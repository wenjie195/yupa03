package com.vidatechft.yupa.friends;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.FriendsRecyclerGridAdapter;
import com.vidatechft.yupa.adapter.ItineraryRecyclerGridAdapter;
import com.vidatechft.yupa.classes.Friend;
import com.vidatechft.yupa.classes.Itinerary;
import com.vidatechft.yupa.classes.User;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FriendListFragment extends Fragment{
    public static final String TAG = FriendListFragment.class.getName();
    private MainActivity parentActivity;

    private ArrayList<Friend> friendsList = new ArrayList<>();
    private ArrayList<Friend> fullFriendsList = new ArrayList<>();
    private ArrayList<Friend> pendingList = new ArrayList<>();
    private ArrayList<Friend> fullPendingList = new ArrayList<>();
    private RecyclerView friendsRV,pendingRV;
    private FriendsRecyclerGridAdapter friendsAdapter,pendingAdapter;

    private ListenerRegistration friendsListener,pendingListener;

    private String loadingTxt,emptyTxt,errorTxt;

    private TextView noFriendsTV;
    private ConstraintLayout pendingCL;
    private EditText searchET;

    private HashMap<String,Friend> friendsMap = new HashMap<>();
    private HashMap<String,Friend> pendingMap = new HashMap<>();
    private HashMap<String,User> userMap = new HashMap<>();

    private int totalFriendCount, totalPendingCount;
    private int friendCount = 0;
    private int pendingCount = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static FriendListFragment newInstance(MainActivity parentActivity) {
        FriendListFragment fragment = new FriendListFragment();

        fragment.parentActivity = parentActivity;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_friends_list, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.showBtmNav();

                loadingTxt = parentActivity.getString(R.string.friend_own_loading);
                emptyTxt = parentActivity.getString(R.string.friend_own_empty);
                errorTxt = parentActivity.getString(R.string.friend_own_loading_error);

                friendsRV = view.findViewById(R.id.friendsRV);
                pendingRV = view.findViewById(R.id.pendingRV);
                noFriendsTV = view.findViewById(R.id.noFriendsTV);
                searchET = view.findViewById(R.id.searchET);
                pendingCL = view.findViewById(R.id.pendingCL);

                setUpGridAdapter();

                if(fullFriendsList.size() <= 0){
                    getFriends();
                }else{
                    noFriendsTV.setVisibility(View.GONE);
                }

                if(fullPendingList.size() <= 0){
                    getPendingFriends();
                }else{
                    pendingCL.setVisibility(View.VISIBLE);
                }

                searchET.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        friendsList.clear();
                        pendingList.clear();
                        friendsAdapter.notifyDataSetChanged();
                        pendingAdapter.notifyDataSetChanged();

                        String keyword = s.toString();

                        for(Friend tempFriend: fullFriendsList){
                            String name = (tempFriend.user.name != null ? tempFriend.user.name : "").toLowerCase();
                            String contactNo = (tempFriend.user.contactNo != null ? tempFriend.user.contactNo : "").toLowerCase();
                            keyword = keyword.toLowerCase();
                            if(name.contains(keyword) || contactNo.contains(keyword)){
                                friendsList.add(tempFriend);
                            }
                        }

                        for(Friend tempPending: fullPendingList){
                            String name = (tempPending.user.name != null ? tempPending.user.name : "").toLowerCase();
                            String contactNo = (tempPending.user.contactNo != null ? tempPending.user.contactNo : "").toLowerCase();
                            keyword = keyword.toLowerCase();
                            if(name.contains(keyword) || contactNo.contains(keyword)){
                                pendingList.add(tempPending);
                            }
                        }

//                        sortChats();
                        friendsAdapter.notifyDataSetChanged();
                        pendingAdapter.notifyDataSetChanged();
                    }
                });
            }
        });

        return view;
    }

    private void setUpGridAdapter(){
        //*********************************PENDING**************************************************
        pendingAdapter = new FriendsRecyclerGridAdapter(parentActivity,this, pendingList);

        pendingRV.setAdapter(pendingAdapter);
        pendingRV.setNestedScrollingEnabled(false);
        pendingRV.setLayoutManager(new GridLayoutManager(parentActivity, 3));

        //*********************************FRIENDS**************************************************
        friendsAdapter = new FriendsRecyclerGridAdapter(parentActivity,this, friendsList);

        friendsRV.setAdapter(friendsAdapter);
        friendsRV.setNestedScrollingEnabled(false);
        friendsRV.setLayoutManager(new GridLayoutManager(parentActivity, 3));
    }

    private void getFriends(){
        //todo the recycler view needs to have lazy load
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                noFriendsTV.setText(loadingTxt);
                noFriendsTV.setVisibility(View.VISIBLE);
            }
        });

        friendsListener = GeneralFunction.getFirestoreInstance()
                .collection(Friend.URL_FRIENDS)
                .whereEqualTo(parentActivity.uid,true)
                .whereEqualTo(Friend.STATUS,Friend.STATUS_ACCEPTED)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot snapshots, @javax.annotation.Nullable final FirebaseFirestoreException e) {
                        if (e != null) {
                            parentActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.w(TAG, "Listen failed.", e);
                                    noFriendsTV.setText(errorTxt);
                                    noFriendsTV.setVisibility(View.VISIBLE);
                                }
                            });
                            return;
                        }

                        if (snapshots != null && !snapshots.isEmpty()) {
                            totalFriendCount = snapshots.size();
                            for(QueryDocumentSnapshot snapshot : snapshots){
                                Friend tempFriend = snapshot.toObject(Friend.class);
                                if(tempFriend.friendList != null){
                                    for(String uid : tempFriend.friendList){
                                        if(!TextUtils.equals(uid,parentActivity.uid)){
                                            friendsMap.put(uid,tempFriend);

                                            //if already got that user's details put it inside, else get the details from database
                                            if(userMap.get(uid) != null){
                                                friendCount++;
                                                friendsMap.get(uid).user = userMap.get(uid);

                                                handleOnFinishFriend();
                                            }else{
                                                getFriendDetails(uid,Friend.TYPE_FRIEND);
                                            }
                                            break;
                                        }
                                    }
                                }
                            }
                        } else {
                            parentActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    friendsList.clear();
                                    friendsAdapter.notifyDataSetChanged();

                                    noFriendsTV.setText(emptyTxt);
                                    noFriendsTV.setVisibility(View.VISIBLE);
                                }
                            });
                        }
                    }
                });
    }

    public void removeFriend(int position){
        fullFriendsList.remove(position);
        friendsList.clear();
        friendsList.addAll(fullFriendsList);
        friendsAdapter.notifyDataSetChanged();
    }

    public void removePending(int position){
        fullPendingList.remove(position);
        pendingList.clear();
        pendingList.addAll(fullPendingList);
        pendingAdapter.notifyDataSetChanged();

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(fullPendingList.isEmpty()){
                    pendingCL.setVisibility(View.GONE);
                }
            }
        });
    }

    private void getPendingFriends(){
        pendingListener = GeneralFunction.getFirestoreInstance()
                .collection(Friend.URL_FRIENDS)
                .whereEqualTo(Friend.TARGET_ID,parentActivity.uid)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot snapshots, @javax.annotation.Nullable final FirebaseFirestoreException e) {
                        if (e != null) {
                            parentActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.w(TAG, "Listen failed.", e);
                                    GeneralFunction.openMessageNotificationDialog(parentActivity,e.getMessage(),R.drawable.notice_bad,TAG);
                                    pendingCL.setVisibility(View.GONE);
                                }
                            });
                            return;
                        }

                        if (snapshots != null && !snapshots.isEmpty()) {
                            totalPendingCount = snapshots.size();
                            for(QueryDocumentSnapshot snapshot : snapshots){
                                Friend tempPending = snapshot.toObject(Friend.class);
                                String uid = tempPending.requestorId;

                                if(tempPending.status != null && TextUtils.equals(tempPending.status,Friend.STATUS_ACCEPTED)){
                                    //add this condtion because don't know why after accepted the requestor id and target id, it is not set to null SOMETIMES, so it appears as pending even though already are friends
                                    HashMap<String,Object> updateFriendMap = new HashMap<>();
                                    updateFriendMap.put(Friend.REQUESTOR_ID,null);
                                    updateFriendMap.put(Friend.TARGET_ID,null);
                                    GeneralFunction.getFirestoreInstance()
                                            .collection(Friend.URL_FRIENDS)
                                            .document(snapshot.getId())
                                            .update(updateFriendMap)
                                            .addOnCompleteListener(GeneralFunction.getOnCompleteListener(parentActivity,TAG));

                                    pendingCount++;
                                    handleOnFinishPending();
                                }else{
                                    pendingMap.put(uid,tempPending);

                                    if(uid != null && !uid.isEmpty()){
                                        if(userMap.get(uid) != null){
                                            pendingMap.get(uid).user = userMap.get(uid);

                                            pendingCount++;
                                            handleOnFinishPending();
                                        }else{
                                            getFriendDetails(uid,Friend.TYPE_PENDING);
                                        }
                                    }else{
                                        pendingCount++;
                                        handleOnFinishPending();
                                    }
                                }
                            }
                        } else {
                            parentActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    pendingList.clear();
                                    pendingAdapter.notifyDataSetChanged();

                                    pendingCL.setVisibility(View.GONE);
                                }
                            });
                        }
                    }
                });
    }

    private void getFriendDetails(String uid, final String type){
        GeneralFunction.getFirestoreInstance()
                .collection(User.URL_USER)
                .document(uid)
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if(task.isSuccessful()){
                            User tempUser = task.getResult().toObject(User.class);

                            if (tempUser != null) {
                                tempUser.id = task.getResult().getId();

                                if(friendsMap.get(tempUser.id) != null){
                                    friendCount++;
                                    friendsMap.get(tempUser.id).user = tempUser;

                                    handleOnFinishFriend();
                                }

                                if(pendingMap.get(tempUser.id) != null){
                                    pendingCount++;
                                    pendingMap.get(tempUser.id).user = tempUser;

                                    handleOnFinishPending();
                                }

                                if(userMap.get(tempUser.id) == null){
                                    userMap.put(tempUser.id,tempUser);
                                }
                            }else{
                                handleIfGotErrorGetFriendDetails(type);
                            }
                        }else{
                            if(task.getException() != null){
                                GeneralFunction.openMessageNotificationDialog(parentActivity,task.getException().toString(),R.drawable.notice_bad,TAG);
                            }
                            handleIfGotErrorGetFriendDetails(type);
                        }
                    }
                });
    }

    private void handleIfGotErrorGetFriendDetails(String type){
        if(TextUtils.equals(Friend.TYPE_FRIEND,type)){
            friendCount++;
            handleOnFinishFriend();
        }else if(TextUtils.equals(Friend.TYPE_PENDING,type)){
            pendingCount++;
            handleOnFinishFriend();
        }
    }

    private void handleOnFinishFriend(){
        if(friendCount >= totalFriendCount){
            friendsList.clear();
            fullFriendsList.clear();
            friendCount = 0;

            for (Map.Entry<String, Friend> entry : friendsMap.entrySet()) {
                friendsList.add(entry.getValue());
            }

            fullFriendsList.addAll(friendsList);

            friendsMap.clear();

            //this is to check if a pending friend is accepted by the other user, so need to remove it from pending since they are now officially friends
            for(Friend tempFriend: fullFriendsList){
                for(int x = 0; x < fullPendingList.size(); x++){
                    Friend tempPending = fullPendingList.get(x);
                    if(tempFriend.user.id != null && tempPending.user.id != null && TextUtils.equals(tempFriend.user.id,tempPending.user.id)){
                        fullPendingList.remove(x);
                    }
                }
            }
            pendingList.clear();
            pendingList.addAll(fullPendingList);
            pendingAdapter.notifyDataSetChanged();

            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(friendsList.isEmpty()){
                        noFriendsTV.setText(emptyTxt);
                        noFriendsTV.setVisibility(View.VISIBLE);
                    }else{
                        noFriendsTV.setVisibility(View.GONE);
                    }
                    friendsAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    private void handleOnFinishPending(){
        if(pendingCount >= totalPendingCount){
            pendingList.clear();
            fullPendingList.clear();
            pendingCount = 0;

            for (Map.Entry<String, Friend> entry : pendingMap.entrySet()) {
                pendingList.add(entry.getValue());
            }

            fullPendingList.addAll(pendingList);

            pendingMap.clear();

            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(pendingList.isEmpty()){
                        pendingCL.setVisibility(View.GONE);
                    }else{
                        pendingCL.setVisibility(View.VISIBLE);
                    }
                    pendingAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.setToolbarTitle(parentActivity.getString(R.string.nav_friend_list));
        parentActivity.selectChat();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(friendsListener != null){
                    friendsListener.remove();
                }

                if(pendingListener != null){
                    pendingListener.remove();
                }
            }
        });
    }

}
