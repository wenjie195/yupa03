package com.vidatechft.yupa.friends;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.utilities.Config;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

public class InviteFriendFragment extends Fragment{
    public static final String TAG = InviteFriendFragment.class.getName();
    private MainActivity parentActivity;

    private TextView inviteLinkTV;
    private String inviteLink;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static InviteFriendFragment newInstance(MainActivity parentActivity) {
        InviteFriendFragment fragment = new InviteFriendFragment();

        fragment.parentActivity = parentActivity;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_invite_friend, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.showBtmNav();

                inviteLinkTV = view.findViewById(R.id.inviteLinkTV);
                Button shareLinkBtn = view.findViewById(R.id.shareLinkBtn);
                Button generateLinkBtn = view.findViewById(R.id.generateLinkBtn);

                inviteLink = GeneralFunction.getSpString(parentActivity,Config.SP_MY_OWN_DEEP_LINK);
                if(inviteLink != null){
                    inviteLinkTV.setText(inviteLink);
                }

                inviteLinkTV.setOnClickListener(new DebouncedOnClickListener(500) {
                    @Override
                    public void onDebouncedClick(View v) {
                        if(inviteLinkTV.getText().toString().trim().isEmpty()){
                            generateLink();
                        }else {
                            ClipboardManager clipboard = (ClipboardManager) parentActivity.getSystemService(Context.CLIPBOARD_SERVICE);
                            ClipData clip = ClipData.newPlainText(null, inviteLinkTV.getText().toString());
                            if (clipboard != null) {
                                clipboard.setPrimaryClip(clip);
                                Toast.makeText(parentActivity,parentActivity.getString(R.string.link_copied),Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });

                shareLinkBtn.setOnClickListener(new DebouncedOnClickListener(500) {
                    @Override
                    public void onDebouncedClick(View v) {
                        if(inviteLink != null){
                            GeneralFunction.shareDeepLink(parentActivity,String.format(parentActivity.getString(R.string.get_this_app_to_see_my_profile),parentActivity.getString(R.string.app_name)),Uri.parse(inviteLink));
                        }else{
                            generateLink();
                        }
                    }
                });

                generateLinkBtn.setOnClickListener(new DebouncedOnClickListener(500) {
                    @Override
                    public void onDebouncedClick(View v) {
                        generateLink();
                    }
                });
            }
        });

        return view;
    }

    private void generateLink(){
        parentActivity.initializeLoadingDialog(parentActivity.getString(R.string.generating_your_profile_link),false);
        parentActivity.controlLoadingDialog(true,parentActivity);

        Uri urlLink = Uri.parse("https://yupa.asia/?" +
                Config.SP_DEEP_LINK_TARGET_ID + "=" + parentActivity.uid + "&" +
                Config.SP_DEEP_LINK_TYPE + "=" + Config.SP_DEEP_LINK_TYPE_PROFILE);
        GeneralFunction.generateDeepLink(parentActivity, urlLink,parentActivity.getString(R.string.deep_link_desc_profile))
                .addOnCompleteListener(new OnCompleteListener<ShortDynamicLink>() {
                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                        parentActivity.controlLoadingDialog(false,parentActivity);
                        if(task.getException() == null && task.getResult() != null && task.getResult().getShortLink() != null){
                            try{
                                String shortDeepLink = task.getResult().getShortLink().toString();
                                GeneralFunction.saveSPStringIntoKey(parentActivity, Config.SP_MY_OWN_DEEP_LINK, shortDeepLink, Config.SP_MY_OWN_DEEP_LINK);

                                inviteLinkTV.setText(shortDeepLink);
                                inviteLink = shortDeepLink;

                                GeneralFunction.shareDeepLink(parentActivity,
                                        String.format(parentActivity.getString(R.string.get_this_app_to_see_my_profile),parentActivity.getString(R.string.app_name)),
                                        task.getResult().getShortLink());
                            }catch (Exception e){
                                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.err_generating_my_profile_deep_link),R.drawable.notice_bad,TAG);
                                e.printStackTrace();
                                Crashlytics.logException(e);
                            }
                        }else{
                            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.err_generating_my_profile_deep_link),R.drawable.notice_bad,TAG);
                        }
                    }
                });
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.nav_invite_friend));
                parentActivity.selectChat();
            }
        });
    }
}
