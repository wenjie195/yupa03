package com.vidatechft.yupa.notificationFragment;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.GlideApp;
import com.vidatechft.yupa.classes.User;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.Map;

//from here https://firebase.google.com/docs/reference/admin/node/admin.messaging.NotificationMessagePayload
//from here https://android.jlelse.eu/cloud-functions-for-firebase-device-to-device-push-notification-f4c548fd9b7d
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getName();

    public static final String TYPE_INBOX = "Inbox";
    public static final String TYPE_CHAT = "Chat";
    public static final String SP_CHAT_KEY = "SubChat";

    public static final String DATA_SOURCE_UID = "sourceUid";
    public static final String DATA_TYPE = "type";
    public static final String DATA_TITLE = "title";
    public static final String DATA_BODY = "body";
//    public static final String DATA_PROFILE_PIC_URL = "profilePicUrl";

    private String notificationTitle,notificationBody,profilePicUrl,channelId;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        notificationTitle = null;
        notificationBody = null;
        profilePicUrl = null;
        channelId = TYPE_INBOX;

        String sourceUid = null;

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {

            if(remoteMessage.getNotification().getTitleLocalizationKey() != null){
                notificationTitle = this.getString(getResources().getIdentifier(remoteMessage.getNotification().getTitleLocalizationKey(), "string", this.getPackageName()));
            }else if(remoteMessage.getNotification().getTitle() != null){
                notificationTitle = remoteMessage.getNotification().getTitle();
            }

            if(remoteMessage.getNotification().getBodyLocalizationKey() != null){
                notificationBody = this.getString(getResources().getIdentifier(remoteMessage.getNotification().getBodyLocalizationKey(), "string", this.getPackageName()));
            }else if(remoteMessage.getNotification().getBody() != null){
                notificationBody = remoteMessage.getNotification().getBody();
            }
        }

        if(remoteMessage.getData() != null){
            Map<String,String> dataMap = remoteMessage.getData();
            if(dataMap.get(DATA_TYPE) != null && !dataMap.get(DATA_TYPE).trim().isEmpty()){
                channelId = dataMap.get(DATA_TYPE);
            }

            if(dataMap.get(DATA_TITLE) != null && !dataMap.get(DATA_TITLE).trim().isEmpty()){
                notificationTitle = dataMap.get(DATA_TITLE);
            }

            if(dataMap.get(DATA_BODY) != null && !dataMap.get(DATA_BODY).trim().isEmpty()){
                notificationBody = dataMap.get(DATA_BODY);
            }

            if(dataMap.get(DATA_SOURCE_UID) != null && !dataMap.get(DATA_SOURCE_UID).trim().isEmpty()){
                sourceUid = dataMap.get(DATA_SOURCE_UID);
            }
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
        // notification will handled by system if app in background (will use onMessageReceived
        // function if app in foreground) but if use data only it will 100% use the onMessageReceived method
        if(channelId == null){
            Log.e(TAG,"channelId is null");
            return;
        }

        switch (channelId){
            case TYPE_INBOX:
                sendNotification(notificationTitle, notificationBody, channelId, null);
                break;
            case TYPE_CHAT:
                getUserProfile(sourceUid);
                break;
            default:
                sendNotification(notificationTitle, notificationBody, channelId, null);
                break;
        }
    }

    private void getUserProfile(String sourceUid){
        if(sourceUid == null){
            Log.e(TAG,"no source uid provided");
            return;
        }

        GeneralFunction.getFirestoreInstance()
                .collection(User.URL_USER)
                .document(sourceUid)
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if(task.getException() != null){
                            Log.e(TAG,task.getException().toString());
                            return;
                        }

                        if(task.getResult() != null && task.getResult().exists()){
                            User tempUser = task.getResult().toObject(User.class);

                            if(tempUser != null){
                                if(tempUser.name != null && !tempUser.name.trim().isEmpty()){
                                    notificationTitle = tempUser.name;
                                }else{
                                    notificationTitle = getApplicationContext().getString(R.string.null_unknown);
                                }

                                if(tempUser.profilePicUrl != null && !tempUser.profilePicUrl.trim().isEmpty()){
                                    profilePicUrl = tempUser.profilePicUrl;
                                }

                                sendNotification(notificationTitle, notificationBody, channelId, profilePicUrl);
                            }else{
                                Log.e(TAG,"no user found");
                            }
                        }else{
                            Log.e(TAG,"snapshot or task is null");
                        }
                    }
                });
    }

    //refer from here to set picture in super big picture mode https://stackoverflow.com/questions/24840282/load-image-from-url-in-notification-android
    private void sendNotification(String notificationTitle, String notificationBody, String channelId, final String largeIconUrl) {
        if(notificationTitle == null || notificationBody == null || channelId == null){
            return;
        }

        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        long[] vibrationPattern = {0, 250, 250, 250};

        final NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            AudioAttributes attributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build();

            NotificationChannel notificationChannel = new NotificationChannel(channelId, channelId + " Notifications", NotificationManager.IMPORTANCE_DEFAULT);

            // Configure the notification channel.
            notificationChannel.setDescription(channelId + " Notifications");
            notificationChannel.setLightColor(Color.WHITE);
            notificationChannel.enableLights(true);
            notificationChannel.setVibrationPattern(vibrationPattern);
            notificationChannel.enableVibration(true);
            notificationChannel.setShowBadge(true);
            notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            notificationChannel.setSound(defaultSoundUri,attributes);
            notificationChannel.setImportance(NotificationManager.IMPORTANCE_DEFAULT);
            if(notificationManager != null){
                notificationManager.createNotificationChannel(notificationChannel);
            }
        }

        final NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId);

        notificationBuilder.setAutoCancel(true)   //Automatically delete the notification
                .setDefaults(Notification.DEFAULT_SOUND)
                .setWhen(System.currentTimeMillis()) // the how many seconds or minutes ago
                .setSmallIcon(R.drawable.ic_notification) //image must be transparent 1
                .setTicker(notificationTitle + notificationBody)//this is for accessibility only, will only show if accessibility is turned on
                .setPriority(Notification.PRIORITY_HIGH)
                .setContentIntent(pendingIntent)
                .setVibrate(vibrationPattern)
                .setLights(Color.WHITE,1000,500)
                .setContentTitle(notificationTitle)
                .setContentText(notificationBody)
                .setContentInfo(channelId)
                .setSound(defaultSoundUri)
                .setChannelId(channelId);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            if(largeIconUrl != null){
//                notificationBuilder.setLargeIcon(GeneralFunction.getDrawableToBitmap(this,R.color.red));
//            }
            notificationBuilder.setColor(getResources().getColor(R.color.colorPrimary));
        }

        if(largeIconUrl != null){
            try{
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public void run() {
                        GlideApp.with(getApplicationContext())
                                .asBitmap()
                                .load(largeIconUrl)
                                .into(new SimpleTarget<Bitmap>() {
                                    @Override
                                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                        showNotification(notificationManager,notificationBuilder,resource);
                                    }
                                });
                    }
                });
            }catch (Exception e){
                Log.e(TAG,e.getMessage());
            }
        }else{
            showNotification(notificationManager,notificationBuilder,null);
        }
    }

    private void showNotification(NotificationManager notificationManager, NotificationCompat.Builder notificationBuilder, Bitmap bitmap){
        if(bitmap != null && android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            notificationBuilder.setLargeIcon(bitmap);
        }

        if(notificationManager != null && notificationBuilder != null){
            notificationManager.notify(/*notification id*/0, notificationBuilder.build());
        }else{
            Log.e(TAG,"notification manager or builder is null");
        }
    }
}