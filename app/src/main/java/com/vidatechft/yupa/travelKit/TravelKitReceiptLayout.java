package com.vidatechft.yupa.travelKit;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.TravelKitReceiptAdapter;
import com.vidatechft.yupa.classes.Payment;
import com.vidatechft.yupa.utilities.GeneralFunction;

//from here https://medium.com/@Sserra90/android-writing-a-compound-view-1eacbf1957fc
public class TravelKitReceiptLayout extends LinearLayout {
    public static final String TAG = TravelKitReceiptLayout.class.getName();
    private MainActivity parentActivity;
    private Fragment parentFragment;
    private String paymentId;
    private Payment payment;
    private TravelKitReceiptLayout tkReceiptLayout;

    private RecyclerView customTKReceiptRV;
    private TravelKitReceiptAdapter tkAdapter;

    public TravelKitReceiptLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init(){
        //if dont have the lines setOrientation and setLayoutParams, the material calendar view wont have any content
        setOrientation(LinearLayout.VERTICAL);
//        setGravity(Gravity.CENTER);
        setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        //Inflate xml resource, pass "this" as the parent, we use <merge> tag in xml to avoid
        //redundant parent, otherwise a LinearLayout will be added to this LinearLayout ending up
        //with two view groups
        inflate(getContext(),R.layout.layout_travel_kit_receipt,this);

        initView();
    }

    private void initView(){
        customTKReceiptRV = findViewById(R.id.customTKReceiptRV);
    }

    public void initLayout(MainActivity parentActivity, Fragment parentFragment, String paymentId, Payment payment, TravelKitReceiptLayout tkReceiptLayout){
        this.parentActivity = parentActivity;
        this.parentFragment = parentFragment;
        this.payment = payment;
        this.tkReceiptLayout = tkReceiptLayout;
        this.paymentId = paymentId;

        if(payment == null){
            getPaymentResult();
        }else{
            setupAdapter();
        }
    }

    private void getPaymentResult(){
        tkReceiptLayout.setVisibility(View.GONE);
        GeneralFunction.getFirestoreInstance()
                .collection(Payment.PAYMENT_RESULT_URL)
                .document(paymentId)
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if(documentSnapshot != null && documentSnapshot.exists()){
                            Payment tempPayment = Payment.getSnapshotToPayment(parentActivity,documentSnapshot);
                            if(tempPayment != null){
                                payment = tempPayment;
                                setupAdapter();
                            }
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        e.printStackTrace();
                        GeneralFunction.openMessageNotificationDialog(parentActivity,e.getMessage(),R.drawable.notice_bad,TAG);
                        tkReceiptLayout.setVisibility(View.GONE);
                    }
                });
    }

    private void setupAdapter(){
        if(parentActivity == null || payment == null || payment.travelKits == null || payment.travelKits.isEmpty()){
            tkReceiptLayout.setVisibility(View.GONE);
            return;
        }else{
            tkReceiptLayout.setVisibility(View.VISIBLE);
        }

        tkAdapter = new TravelKitReceiptAdapter(parentActivity,parentFragment, payment, payment.travelKits);

        customTKReceiptRV.setNestedScrollingEnabled(false);
        customTKReceiptRV.setAdapter(tkAdapter);
        customTKReceiptRV.setLayoutManager(new LinearLayoutManager(parentActivity,LinearLayoutManager.VERTICAL, false));
    }

    public boolean isEmpty(){
        if(parentActivity == null || payment == null || payment.travelKits == null || payment.travelKits.isEmpty()){
            return true;
        }else{
            return false;
        }
    }

    public int size(){
        if(tkAdapter != null){
            return tkAdapter.getItemCount();
        }else{
            return -1;
        }
    }
}