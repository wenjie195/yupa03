package com.vidatechft.yupa.travelKit;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.ItineraryPlanTabPagerAdapter;
import com.vidatechft.yupa.classes.Favourite;
import com.vidatechft.yupa.classes.Itinerary;
import com.vidatechft.yupa.classes.Payment;
import com.vidatechft.yupa.classes.PlaceDetailsGoogleApi.PlaceAddress;
import com.vidatechft.yupa.classes.Plan;
import com.vidatechft.yupa.classes.Search;
import com.vidatechft.yupa.paymentFragment.SelectTravelKitFragment;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;

public class TravelKitDetailFragment extends Fragment{
    public static final String TAG = TravelKitDetailFragment.class.getName();
    private MainActivity parentActivity;
    private Payment payment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static TravelKitDetailFragment newInstance(MainActivity parentActivity, Payment payment) {
        TravelKitDetailFragment fragment = new TravelKitDetailFragment();

        fragment.parentActivity = parentActivity;
        fragment.payment = payment;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_travel_kit_details, container, false);

        TextView departDateTV = view.findViewById(R.id.departDateTV);
        TextView returnDateTV = view.findViewById(R.id.returnDateTV);
        TravelKitReceiptLayout tkReceiptLayout = view.findViewById(R.id.tkReceiptLayout);

        if(payment != null && payment.id != null){
            if(payment.departDate != null){
                departDateTV.setText(GeneralFunction.formatDateToString(payment.departDate));
            }

            if(payment.returnDate != null){
                returnDateTV.setText(GeneralFunction.formatDateToString(payment.returnDate));
            }

            tkReceiptLayout.initLayout(parentActivity, TravelKitDetailFragment.this, payment.id, payment,tkReceiptLayout);
        }

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.showToolbar();
        parentActivity.hideBtmNav();
        parentActivity.setToolbarTitle(parentActivity.getString(R.string.booking_buy_travel_kit_toolbar));
    }

    @Override
    public void onPause() {
        super.onPause();
        parentActivity.showToolbar();
        parentActivity.showBtmNav();
    }
}