//package com.vidatechft.yupa.InboxFragment;
//
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.TextView;
//
//import com.google.android.gms.tasks.OnCompleteListener;
//import com.google.android.gms.tasks.Task;
//import com.google.firebase.firestore.DocumentSnapshot;
//import com.google.firebase.firestore.EventListener;
//import com.google.firebase.firestore.FirebaseFirestoreException;
//import com.google.firebase.firestore.ListenerRegistration;
//import com.google.firebase.firestore.QuerySnapshot;
//import com.vidatechft.yupa.R;
//import com.vidatechft.yupa.activities.MainActivity;
//import com.vidatechft.yupa.adapter.InboxRecyclerAdapter;
//import com.vidatechft.yupa.classes.Accommodation;
//import com.vidatechft.yupa.classes.BookingHistory;
//import com.vidatechft.yupa.classes.Inbox;
//import com.vidatechft.yupa.classes.Room;
//import com.vidatechft.yupa.utilities.GeneralFunction;
//
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.HashMap;
//import java.util.Map;
//
//public class InboxMainFragment_OLD extends Fragment implements View.OnClickListener{
//    public static final String TAG = InboxMainFragment_OLD.class.getName();
//    private MainActivity parentActivity;
//
//    private TextView nothingTV;
//    private String loadingTxt, emptyTxt,errorTxt;
//
//    private RecyclerView inboxRV;
//    private ArrayList<Inbox> inboxList = new ArrayList<>();
//    private HashMap<String,Inbox> inboxMap = new HashMap<>();
//    private InboxRecyclerAdapter inboxAdapter;
//    private ListenerRegistration bookingHistoryListener;
//    private int totalBHCount = 0;
//    private int bHCount = 0;
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//    }
//
//    public static InboxMainFragment_OLD newInstance(MainActivity parentActivity) {
//        InboxMainFragment_OLD fragment = new InboxMainFragment_OLD();
//
//        fragment.parentActivity = parentActivity;
//
//        return fragment;
//    }
//
//    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
//        final View view = inflater.inflate(R.layout.fragment_main_inbox, container, false);
//
//        parentActivity.runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                parentActivity.hideBtmNav();
//
//                loadingTxt = parentActivity.getString(R.string.inbox_loading);
//                emptyTxt = parentActivity.getString(R.string.inbox_empty);
//                errorTxt = parentActivity.getString(R.string.inbox_loading_error);
//
//                nothingTV = view.findViewById(R.id.nothingTV);
//                inboxRV = view.findViewById(R.id.inboxRV);
//
//                inboxAdapter = new InboxRecyclerAdapter(parentActivity, inboxList);
//                inboxRV.setAdapter(inboxAdapter);
//                inboxRV.setLayoutManager(new LinearLayoutManager(parentActivity, LinearLayoutManager.HORIZONTAL, false));
//            }
//        });
//
//        if(inboxList != null && !inboxList.isEmpty()){
//            updateAdapter();
//        }else{
//            getNewBookingHistory();
//        }
//        return view;
//    }
//
//    /*****************************************************BH = Booking History START******************************************************************************/
//    private void getNewBookingHistory(){
//        bookingHistoryListener = GeneralFunction.getFirestoreInstance()
//                .collection(BookingHistory.URL_BOOKING_HISTORY)
//                .whereEqualTo(BookingHistory.USER_ID,parentActivity.uid)
//                .whereEqualTo(BookingHistory.IS_READ_BY_GUEST, false)
//                .whereGreaterThanOrEqualTo(BookingHistory.CHECKIN_DATE,GeneralFunction.getDefaultUtcCalendar().getTimeInMillis())
//                .addSnapshotListener(new EventListener<QuerySnapshot>() {
//                    @Override
//                    public void onEvent(@javax.annotation.Nullable QuerySnapshot snapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
//                        if(GeneralFunction.hasError(parentActivity,e,TAG)){
//                            return;
//                        }
//
//                        if(snapshots != null && !snapshots.isEmpty()){
//                            totalBHCount = snapshots.size();
//                            for(DocumentSnapshot snapshot : snapshots){
//                                final BookingHistory bookingHistory = snapshot.toObject(BookingHistory.class);
//
//                                if(GeneralFunction.isBookingHistoryValid(bookingHistory)){
//                                    bookingHistory.id = snapshot.getId();
//                                    switch (bookingHistory.type){
//                                        case BookingHistory.BOOK_TYPE_HOMESTAY:
//                                            GeneralFunction.getFirestoreInstance()
//                                                    .collection(Accommodation.URL_ACCOMMODATION)
//                                                    .document(bookingHistory.targetId)
//                                                    .get()
//                                                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
//                                                        @Override
//                                                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
//                                                            bHCount++;
//                                                            if(GeneralFunction.hasErrorDoc(parentActivity,task,TAG)){
//                                                                handleBhDownloads();
//                                                                return;
//                                                            }
//
//                                                            Room room = task.getResult().toObject(Room.class);
//
//                                                            if(room != null){
//                                                                room.id = task.getResult().getId();
//                                                                inboxMap.put(bookingHistory.id,new Inbox(parentActivity,room,bookingHistory));
//                                                            }
//
//                                                            handleBhDownloads();
//                                                        }
//                                                    });
//                                            break;
//                                    }
//                                }
//                            }
//                        }else{
//                            parentActivity.runOnUiThread(new Runnable() {
//                                @Override
//                                public void run() {
//                                    nothingTV.setText(emptyTxt);
//                                    nothingTV.setVisibility(View.VISIBLE);
//                                }
//                            });
//                        }
//                    }
//                });
//    }
//
//    private void handleBhDownloads(){
//        if(totalBHCount == bHCount){
//            bHCount = 0;
//            totalBHCount = 0;
//            updateAdapter();
//        }
//    }
//    /*****************************************************BH = Booking History END******************************************************************************/
//
//    private void updateAdapter(){
//        inboxList.clear();
//
//        for (Object o : inboxMap.entrySet()) {
//            Map.Entry appliedPair = (Map.Entry) o;
//            inboxList.add((Inbox) appliedPair.getValue());
////            inboxIterator.remove();
//        }
//
//        Collections.sort(inboxList);
//        inboxAdapter.notifyDataSetChanged();
//
//        parentActivity.runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                if(inboxList.isEmpty()){
//                    nothingTV.setText(emptyTxt);
//                    nothingTV.setVisibility(View.VISIBLE);
//                }else{
//                    nothingTV.setVisibility(View.GONE);
//                }
//            }
//        });
//
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        parentActivity.runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                parentActivity.setToolbarTitle(parentActivity.getString(R.string.inbox));
//            }
//        });
//    }
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//
//        if(bookingHistoryListener != null){
//            bookingHistoryListener.remove();
//        }
//    }
//
//    @Override
//    public void onClick(View v) {
//        switch (v.getId()){
//            case R.id.backIB:
//                parentActivity.onBackPressed();
//                break;
//        }
//    }
//}
