package com.vidatechft.yupa.InboxFragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.InboxRecyclerAdapter;
import com.vidatechft.yupa.classes.Accommodation;
import com.vidatechft.yupa.classes.BookingHistory;
import com.vidatechft.yupa.classes.Inbox;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.classes.User;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

//todo maybe send email notifications to the host and user too in respective action? also need phone notification GCM in future
public class InboxMainFragment extends Fragment implements View.OnClickListener{
    public static final String TAG = InboxMainFragment.class.getName();
    private MainActivity parentActivity;

    private TextView nothingTV;
    private String loadingTxt, emptyTxt,errorTxt;

    private RecyclerView inboxRV;
    private ArrayList<Inbox> inboxList = new ArrayList<>();
    private HashMap<String,Inbox> inboxMap = new HashMap<>();
    private InboxRecyclerAdapter inboxAdapter;
    private ListenerRegistration inboxListener;
    private int totalInboxCount = 0;
    private int inboxCount = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static InboxMainFragment newInstance(MainActivity parentActivity) {
        InboxMainFragment fragment = new InboxMainFragment();

        fragment.parentActivity = parentActivity;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_main_inbox, container, false);

        GeneralFunction.unsubscribeInbox(parentActivity.uid,TAG);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.hideBtmNav();

                loadingTxt = parentActivity.getString(R.string.inbox_loading);
                emptyTxt = parentActivity.getString(R.string.inbox_empty);
                errorTxt = parentActivity.getString(R.string.inbox_loading_error);

                nothingTV = view.findViewById(R.id.nothingTV);
                inboxRV = view.findViewById(R.id.inboxRV);

                inboxAdapter = new InboxRecyclerAdapter(parentActivity, inboxList);
                inboxRV.setAdapter(inboxAdapter);
                inboxRV.setLayoutManager(new LinearLayoutManager(parentActivity, LinearLayoutManager.VERTICAL, false));
            }
        });

        if(inboxList != null && !inboxList.isEmpty()){
            updateAdapter();
        }else{
            getInbox();
        }
        return view;
    }

    /*****************************************************GET INBOX DETAILS START******************************************************************************/
    private void getInbox(){
        inboxListener = GeneralFunction.getFirestoreInstance()
                .collection(User.URL_USER)
                .document(parentActivity.uid)
                .collection(Inbox.URL_INBOX)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot snapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                        if(GeneralFunction.hasError(parentActivity,e,TAG)){
                            nothingTV.setText(errorTxt);
                            nothingTV.setVisibility(View.VISIBLE);
                            return;
                        }

                        if(snapshots != null && !snapshots.isEmpty()){
                            if(snapshots.getMetadata().isFromCache()){
                                inboxCount = 0;
                                totalInboxCount = snapshots.size();
                            }else{
                                inboxCount += inboxCount;
                                totalInboxCount += snapshots.size();
                            }

                            for(DocumentSnapshot snapshot : snapshots){
                                final Inbox inbox = snapshot.toObject(Inbox.class);

                                if(inbox != null && inbox.inboxType != null){
                                    inbox.id = snapshot.getId();

                                    switch (inbox.inboxType){
                                        case Inbox.INBOX_TYPE_HOMESTAY:
                                            getRoom(inbox);
                                            break;
                                        case Inbox.INBOX_TYPE_FRIEND:
                                            inboxCount++;//delete this 2 lines if want to implement the feature because this 2 lines is for skipping
                                            handleInboxInnerDetailsDownload();
//                                            getUser(inbox); //this friend notification feature didnt do
                                            break;
                                        default:
                                            inboxCount++;
                                            handleInboxInnerDetailsDownload();
                                            break;
                                    }
                                }else{
                                    inboxCount++;
                                    handleInboxInnerDetailsDownload();
                                }
                            }
                        }else{
                            parentActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    inboxList.clear();
                                    inboxAdapter.notifyDataSetChanged();
                                    nothingTV.setText(emptyTxt);
                                    nothingTV.setVisibility(View.VISIBLE);
                                }
                            });
                        }
                    }
                });
    }

    private void getRoom(final Inbox inbox){
        GeneralFunction.getFirestoreInstance()
                .collection(Accommodation.URL_ACCOMMODATION)
                .document(inbox.targetId)
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
//                        inboxCount++;
                        if(GeneralFunction.hasErrorDoc(parentActivity,task,TAG)){
                            inboxCount++;
                            handleInboxInnerDetailsDownload();
                            return;
                        }

                        Room room = task.getResult().toObject(Room.class);

                        if(room != null){
                            room.id = task.getResult().getId();
                            inbox.room = room;
                            getUser(inbox);
                        }else{
                            inboxCount++;
                            handleInboxInnerDetailsDownload();
                        }

//                        handleInboxInnerDetailsDownload();
                    }
                });
    }

    private void getUser(final Inbox inbox){
        DocumentReference ref;
        //because if is for host 1 hor, it will go get room details first then only get user details.
        //and because for this inbox the targetId is a accomodation/room id and triggerUid is the user id
        if(inbox.room != null){
            ref = GeneralFunction.getFirestoreInstance()
                    .collection(User.URL_USER)
                    .document(inbox.triggerUId);
        }else{
            ref = GeneralFunction.getFirestoreInstance()
                    .collection(User.URL_USER)
                    .document(inbox.targetId);
        }

        ref.get()
            .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    inboxCount++;
                    if(GeneralFunction.hasErrorDoc(parentActivity,task,TAG)){
                        handleInboxInnerDetailsDownload();
                        return;
                    }

                    User user = task.getResult().toObject(User.class);

                    if(user != null){
                        user.id = task.getResult().getId();
                        inbox.otherUser = user;
                        inboxMap.put(inbox.id,inbox);
                    }

                    handleInboxInnerDetailsDownload();
                }
            });
    }

    //inner details like the other user's info or room info, etc...
    //already fixed the bug --> inbox not being updated on 1st time run, need 2nd time open only got show updated inbox by adding the following code in an if else statement
    // inboxCount += inboxCount;
    // totalInboxCount += snapshots.size();
    private void handleInboxInnerDetailsDownload(){
        if(totalInboxCount == inboxCount){
            updateAdapter();
        }
    }
    /*****************************************************GET INBOX DETAILS END******************************************************************************/

    private void updateAdapter(){
        inboxCount = 0;
        totalInboxCount = 0;
        inboxList.clear();

        for (Object o : inboxMap.entrySet()) {
            Map.Entry appliedPair = (Map.Entry) o;
            inboxList.add((Inbox) appliedPair.getValue());
//            inboxIterator.remove();
        }
        Collections.sort(inboxList);

        inboxAdapter.notifyDataSetChanged();

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(inboxList.isEmpty()){
                    nothingTV.setText(emptyTxt);
                    nothingTV.setVisibility(View.VISIBLE);
                }else{
                    nothingTV.setVisibility(View.GONE);
                }
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.inbox));
                parentActivity.selectHighlights();
            }
        });
        GeneralFunction.unsubscribeInbox(parentActivity.uid,TAG);
    }

    @Override
    public void onPause() {
        super.onPause();
        GeneralFunction.subscribeInbox(parentActivity.uid,TAG);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if(inboxListener != null){
            inboxListener.remove();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.backIB:
                parentActivity.onBackPressed();
                break;
        }
    }
}
