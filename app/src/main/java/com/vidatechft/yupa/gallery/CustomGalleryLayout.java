package com.vidatechft.yupa.gallery;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.Gallery;
import com.vidatechft.yupa.utilities.SlowerLinearLayoutManager;

import java.util.ArrayList;

//from here https://medium.com/@Sserra90/android-writing-a-compound-view-1eacbf1957fc
public class CustomGalleryLayout extends LinearLayout {
    public static final String TAG = CustomGalleryLayout.class.getName();
    private MainActivity parentActivity;
    private Fragment parentFragment;
    private CustomGalleryLayout customGalleryLayout;
    private ArrayList<Gallery> galleryList;

    private ImageView customGalleryLeftArrowIV,customGalleryRightArrowIV;

    private RecyclerView customGalleryRV;
    private CustomGalleryAdapter customGalleryAdapter;

    private boolean isScrollingLeft = false;

    public CustomGalleryLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init(){
        //if dont have the lines setOrientation and setLayoutParams, the material calendar view wont have any content
        setOrientation(LinearLayout.VERTICAL);
//        setGravity(Gravity.CENTER);
        setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        //Inflate xml resource, pass "this" as the parent, we use <merge> tag in xml to avoid
        //redundant parent, otherwise a LinearLayout will be added to this LinearLayout ending up
        //with two view groups
        inflate(getContext(),R.layout.layout_custom_gallery,this);

        initView();
    }

    private void initView(){
        customGalleryRV = findViewById(R.id.customGalleryRV);
        customGalleryLeftArrowIV = findViewById(R.id.customGalleryLeftArrowIV);
        customGalleryRightArrowIV = findViewById(R.id.customGalleryRightArrowIV);
    }

    public void initLayout(MainActivity parentActivity, Fragment parentFragment, CustomGalleryLayout customGalleryLayout, ArrayList<Gallery> galleryList){
        this.parentActivity = parentActivity;
        this.parentFragment = parentFragment;
        this.customGalleryLayout = customGalleryLayout;
        this.galleryList = galleryList;

        setupAdapter();
    }

    private void setupAdapter(){
        final LinearLayoutManager linearLayoutManager = new SlowerLinearLayoutManager(parentActivity,LinearLayoutManager.HORIZONTAL,false);

        //put at least one gallery with a ic_not_found drawable if empty
        if(isEmpty()){
            galleryList = new ArrayList<>();
            galleryList.add(new Gallery());
        }

        customGalleryAdapter = new CustomGalleryAdapter(parentActivity,parentFragment,galleryList);

        customGalleryRV.setNestedScrollingEnabled(false);
        customGalleryRV.setAdapter(customGalleryAdapter);
        customGalleryRV.setLayoutManager(linearLayoutManager);

        //**********************************************CUSTOM SCROLL AND LEFT/RIGHT ARROW LISTENER****************************************************/
        customGalleryLeftArrowIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    customGalleryRV.smoothScrollToPosition(linearLayoutManager.findFirstVisibleItemPosition() - 1);
                }catch (Exception e){
                    customGalleryRV.smoothScrollToPosition(0);
                }

            }
        });

        customGalleryRightArrowIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    customGalleryRV.smoothScrollToPosition(linearLayoutManager.findLastVisibleItemPosition() + 1);
                }catch (Exception e){
                    customGalleryRV.smoothScrollToPosition(customGalleryAdapter.getItemCount() - 1);
                }
            }
        });


        customGalleryRV.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                //if first item is not fully visible then show left arrow
                //if last item is not fully visible show right arrow
                if (linearLayoutManager.findFirstCompletelyVisibleItemPosition() == 0) {
                    customGalleryLeftArrowIV.setVisibility(View.GONE);
                } else {
                    customGalleryLeftArrowIV.setVisibility(View.VISIBLE);
                }

                if (customGalleryAdapter != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == customGalleryAdapter.getItemCount() - 1) {
                    customGalleryRightArrowIV.setVisibility(View.GONE);
                } else {
                    customGalleryRightArrowIV.setVisibility(View.VISIBLE);
                }

                if (dx > 0) {
                    isScrollingLeft = false;
//                    System.out.println("Scrolled Right");
                } else if (dx < 0) {
                    isScrollingLeft = true;
//                    System.out.println("Scrolled Left");
                } else {
                    isScrollingLeft = false;
                }
            }

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                switch (newState) {
                    case RecyclerView.SCROLL_STATE_IDLE:
                        int position = 0;
                        if(isScrollingLeft){
                            position = linearLayoutManager.findFirstVisibleItemPosition();
                        }else{
                            position = linearLayoutManager.findLastVisibleItemPosition();
                        }
                        customGalleryRV.smoothScrollToPosition(position);
                        break;
                    case RecyclerView.SCROLL_STATE_DRAGGING:
//                        System.out.println("Scrolling now");
                        break;
                    case RecyclerView.SCROLL_STATE_SETTLING:
//                        System.out.println("Scroll Settling");
                        break;

                }
            }
        });
    }

    public boolean isEmpty(){
        if(parentActivity == null || galleryList == null || galleryList.isEmpty()){
            return true;
        }else{
            return false;
        }
    }

    public int size(){
        if(customGalleryAdapter != null){
            return customGalleryAdapter.getItemCount();
        }else{
            return -1;
        }
    }
}