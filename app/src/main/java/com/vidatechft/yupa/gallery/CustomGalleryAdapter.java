package com.vidatechft.yupa.gallery;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.Gallery;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;

public class CustomGalleryAdapter extends RecyclerView.Adapter<CustomGalleryAdapter.ViewHolder>  {
    public static final String TAG = CustomGalleryAdapter.class.getName();
    private ArrayList<Gallery> galleryList;
    private MainActivity parentActivity;
    private Fragment parentFragment;

    public CustomGalleryAdapter(MainActivity activity, Fragment parentFragment, ArrayList<Gallery> galleryList) {
        this.parentFragment = parentFragment;
        this.parentActivity = activity;
        this.galleryList = galleryList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public CardView wholeCV;
        public ImageView imageView;

        public ViewHolder(View v) {
            super(v);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    wholeCV = itemView.findViewById(R.id.wholeCV);
                    imageView = itemView.findViewById(R.id.imageView);
                }
            });
        }
    }

    @NonNull
    @Override
    public CustomGalleryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_custom_gallery, parent, false);

        return new CustomGalleryAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int pos) {
        final int position = pos;
        final Gallery gallery = galleryList.get(position);

        if(gallery != null && gallery.url != null){
            GeneralFunction.GlideImageSettingOwnPlaceholder(parentActivity, Uri.parse(gallery.url),holder.imageView,R.drawable.ic_loading_image);
        }else{
//            holder.imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            GeneralFunction.GlideImageSettingOwnDrawable(parentActivity, R.drawable.ic_not_found,holder.imageView);
        }
    }

    @Override
    public int getItemCount() {
        return galleryList.size();
    }
}