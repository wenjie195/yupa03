package com.vidatechft.yupa.activities;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.BuildConfig;
import com.crashlytics.android.core.CrashlyticsCore;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.ListenerRegistration;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.classes.CurrencyRate;
import com.vidatechft.yupa.classes.User;
import com.vidatechft.yupa.dialogFragments.LoadingProgressDialogFragment;
import com.vidatechft.yupa.fragments.LoginFragment;
import com.vidatechft.yupa.fragments.RegisterAccTypeFragment;
import com.vidatechft.yupa.utilities.Config;
import com.vidatechft.yupa.utilities.GeneralFunction;
import com.vidatechft.yupa.utilities.ScreenAdjuster;
import com.vidatechft.yupa.utilities.TypeFaceUtil;

import java.util.HashMap;

import io.fabric.sdk.android.Fabric;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String TAG = LoginActivity.class.getName();
    private LoginActivity parentActivity;
    private ConstraintLayout wholeCL;
    private final int imagesToShow[] = { R.drawable.bg_carousel1,R.drawable.bg_carousel2,
            R.drawable.bg_carousel3, R.drawable.bg_carousel4, R.drawable.bg_carousel5, R.drawable.bg_carousel6,
            R.drawable.bg_carousel7, R.drawable.bg_carousel8};
    private TransitionDrawable td;

    private Handler handler,animTitleHandler;
    private Runnable myRunnable,animTitleRunnable;

    private User registeredUser;
    TextSwitcher travelFreeTS;
    private int currentTitleIndex = 0;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private LoadingProgressDialogFragment loadingProgressDialogFragment;

    private ListenerRegistration currencyRateListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        parentActivity = this;

        //SEARCH WITH THIS -> Set up Crashlytics, disabled for debug builds
        //THIS IS TO DISABLE CRASHLYTICS IN DEBUG MODE BECAUSE IT IS CAUSING BUILD TIME TO BE VERY LONG
        //IT ALSO ADDED IN THE app's build.gradle file. code ->> ext.enableCrashlytics = false
        //and in android manifest. code ->> <meta-data android:name="firebase_crashlytics_collection_enabled" android:value="false" />
//        Crashlytics crashlyticsKit = new Crashlytics.Builder()
//                .core(new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build())
//                .build();
//
//        // Initialize Fabric with the debug-disabled crashlytics.
//        Fabric.with(this, crashlyticsKit);
        Fabric.with(this, new Crashlytics());

        GeneralFunction.checkCurrentAppVersion(parentActivity);

        parentActivity.runOnUiThread(new Runnable() {
            public void run() {
                //from here https://gist.github.com/artem-zinnatullin/7749076
                //make every font become our custom font
                TypeFaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/gill_sans_mt.ttf"); // font from assets: "assets/fonts/gill_sans_mt.ttf

                // to remove status bar then use this before setContentView(layout) in onCreateView method
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                        WindowManager.LayoutParams.FLAG_FULLSCREEN);
                setContentView(R.layout.activity_login);

                ScreenAdjuster.assistActivity(parentActivity);

                //for hiding action bar
                ActionBar myActionBar = getSupportActionBar();
                if (myActionBar != null) {
                    myActionBar.hide();
                }

                travelFreeTS = findViewById(R.id.travelFreeTS);

                travelFreeTS.setFactory(new ViewSwitcher.ViewFactory() {
                    @Override
                    public View makeView () {
                        return LayoutInflater.from(parentActivity).inflate(R.layout.text_view_for_title_text_switcher, null);
                    }
                });

                travelFreeTS.setInAnimation(parentActivity, R.anim.slide_up);
                travelFreeTS.setOutAnimation(parentActivity, R.anim.slide_down);

                wholeCL = findViewById(R.id.wholeCL);
                Button signinBtn = findViewById(R.id.signinBtn);
                Button registerBtn = findViewById(R.id.registerBtn);

                signinBtn.setOnClickListener(LoginActivity.this);
                registerBtn.setOnClickListener(LoginActivity.this);

                setDefaultBgImg();

                animateTitleList();
                
                //get currency rates
                currencyRateListener = CurrencyRate.syncCurrencyFromDB(parentActivity);
            }
        });

        mAuth = FirebaseAuth.getInstance();
        createAuthStateListener();

        //listen for deep linking
        GeneralFunction.listenForDeepLink(parentActivity,TAG);

        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;

            TextView versionTV = findViewById(R.id.versionTV);
            versionTV.setText(String.format(getString(R.string.version),version));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

    }

    private void setDefaultBgImg(){
        parentActivity.runOnUiThread(new Runnable() {
            public void run() {

                if(parentActivity != null){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                        if(!parentActivity.isDestroyed()){
                            GeneralFunction.insertBgImgToOtherLayout(parentActivity,R.drawable.bg_carousel1,wholeCL);
                        }
                    }
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        restartCarousel();
    }

    public void restartCarousel(){
        parentActivity.runOnUiThread(new Runnable() {
            public void run() {
                setDefaultBgImg();

                final int[] i = {0};
                final int[] j = {1};

                handler = new Handler();

                myRunnable = new Runnable() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try{
//                            refered from here https://blog.csdn.net/shaw1994/article/details/46846075
                                    Resources res = getApplicationContext().getResources();
                                    Bitmap bitmap = null;
                                    td = new TransitionDrawable(new Drawable[] {
                                            res.getDrawable(imagesToShow[i[0]]),res.getDrawable(imagesToShow[j[0]]),
                                            new BitmapDrawable(Resources.getSystem(), bitmap)
                                    });

                                    wholeCL.setBackground(td);

                                    td.setCrossFadeEnabled(true);
                                    td.startTransition(4000);

                                    i[0]++;
                                    j[0]++;
                                    if (j[0] == imagesToShow.length) {
                                        j[0] = 0;
                                    }
                                    if (i[0] == imagesToShow.length) {
                                        i[0] = 0;
                                    }
                                    handler.postDelayed(this, 8000);
                                }catch (Exception e){
                                    setDefaultBgImg();
                                }
                            }
                        });
                    }
                };

                handler.postDelayed(myRunnable, 2000);
            }
        });
    }

    private void animateTitleList(){
        final String[] travelTitleList = parentActivity.getResources().getStringArray(R.array.travel_title_list);

        animTitleHandler = new Handler();
        animTitleRunnable = new Runnable(){
            @Override
            public void run() {
                // If index reaches maximum reset it
                if (currentTitleIndex == travelTitleList.length) {
                    currentTitleIndex = 0;
                }

                travelFreeTS.setText(travelTitleList[currentTitleIndex]);
                currentTitleIndex++;

                animTitleHandler.postDelayed(animTitleRunnable, 2000);
            }
        };

        animTitleHandler.postDelayed(animTitleRunnable, 2000);
    }

    private void stopCarousel(){
        if(handler != null){
            handler.removeCallbacksAndMessages(null); // remove everything including pending callbacks and messages
        }
        td = null;
        handler = null;
        myRunnable = null;
    }

    public void setRegisteredUser(User registeredUser){
        this.registeredUser = registeredUser;
    }

    private void createAuthStateListener() {
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                final FirebaseUser user = firebaseAuth.getCurrentUser();

                if (user != null) {

                    if(user.isAnonymous()){
                        goToMainActivity();
                        return;
                    }

                    //manage user online presence
                    GeneralFunction.getFirestoreInstance()
                            .collection(User.URL_USER)
                            .document(user.getUid())
                            .update(User.IS_ONLINE,true);

                    FirebaseDatabase.getInstance().getReference().child(User.IS_ONLINE).child(user.getUid()).setValue(true);

                    //this will set the value of firebase to false when app is disconnected
                    FirebaseDatabase.getInstance().getReference().child(User.IS_ONLINE).child(user.getUid()).onDisconnect().setValue(false);

                    final String emailSentMsg = getString(R.string.email_veri_sent);

                    if(!user.isEmailVerified()){
                        user.sendEmailVerification();

                        if(registeredUser != null){
                            GeneralFunction.getFirestoreInstance().collection(User.URL_USER).document(user.getUid())
                                    .set(registeredUser)
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            closeAndShowMessage( getString(R.string.registered_success)+ "!\n\n" + emailSentMsg,R.drawable.notice_good);
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            closeAndShowMessage(emailSentMsg + "\n\n" + getString(R.string.error) + ": " + e.getMessage(),R.drawable.notice_bad);
                                        }
                                    });
                        }else{
                            closeAndShowMessage(emailSentMsg,R.drawable.notice_bad);
                        }

                        return;
                    }
                    goToMainActivity();
                }
            }
        };
    }

    private void goToMainActivity(){
        Intent intent = new Intent(parentActivity, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    private void closeAndShowMessage(String emailSentMsg,int drawableResource){
        controlProgressDialog(false,parentActivity);
        GeneralFunction.openMessageNotificationDialog(parentActivity,emailSentMsg,drawableResource,"sent_email_verify");

        if(FirebaseAuth.getInstance() != null){
            FirebaseAuth.getInstance().signOut();
        }
    }

    public void controlProgressDialog(boolean isMakeItOpen, AppCompatActivity parentActivity){
        if(parentActivity != null && loadingProgressDialogFragment != null && !parentActivity.isFinishing() && GeneralFunction.isAppInForground(parentActivity.getApplicationContext())){
            if(isMakeItOpen){
                loadingProgressDialogFragment.show(parentActivity.getSupportFragmentManager(),TAG);
            }else{
                loadingProgressDialogFragment.dismiss();
            }
        }
    }

    public void initializeProgressDialog(String message, boolean isAllowCancel){
        loadingProgressDialogFragment = LoadingProgressDialogFragment.newInstance(message,isAllowCancel,parentActivity);
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();

        //this means that there is no more fragment
        if (fm != null && fm.getBackStackEntryCount() == 1) {
            restartCarousel();
        }
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        stopCarousel();
        super.onPause();
    }

    @Override
    public void onStart(){
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop(){
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(currencyRateListener != null){
            currencyRateListener.remove();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.registerBtn:
                stopCarousel();
                Fragment registerAccTypeFragment = RegisterAccTypeFragment.newInstance(parentActivity);
                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.loginFL, registerAccTypeFragment).addToBackStack(RegisterAccTypeFragment.TAG).commit();
                break;
            case R.id.signinBtn:
                stopCarousel();
                Fragment loginFragment = LoginFragment.newInstance(parentActivity);
                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.loginFL, loginFragment).addToBackStack(LoginFragment.TAG).commit();
                break;
        }
    }
}
