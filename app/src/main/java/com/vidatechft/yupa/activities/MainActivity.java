package com.vidatechft.yupa.activities;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.messaging.FirebaseMessaging;
import com.vidatechft.yupa.InboxFragment.InboxMainFragment;
import com.vidatechft.yupa.MainSearchFragment.SearchHostRoomFragment;
import com.vidatechft.yupa.MainSearchFragment.SearchResultFragment;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.adapter.BlogRecyclerGridAdapter;
import com.vidatechft.yupa.adapter.HostStayRecyclerGridAdapter;
import com.vidatechft.yupa.adapter.ItineraryRecyclerGridAdapter;
import com.vidatechft.yupa.adapter.PlaceAutoCompleteAdapter;
import com.vidatechft.yupa.blog.BlogMainFragment;
import com.vidatechft.yupa.blog.BlogViewAllFragment;
import com.vidatechft.yupa.chatFragments.ChatContainerFragment;
import com.vidatechft.yupa.classes.Accommodation;
import com.vidatechft.yupa.classes.Blog;
import com.vidatechft.yupa.classes.BookingHistory;
import com.vidatechft.yupa.classes.Chat;
import com.vidatechft.yupa.classes.CurrencyRate;
import com.vidatechft.yupa.classes.Search;
import com.vidatechft.yupa.classes.SearchPlaceGoogleApi.SearchPlaceResult;
import com.vidatechft.yupa.currency.CurrencyChooserFragment;
import com.vidatechft.yupa.fragments.AboutUsFragment;
import com.vidatechft.yupa.friends.InviteFriendFragment;
import com.vidatechft.yupa.paymentFragment.PaymentHistoryMainFragment;
import com.vidatechft.yupa.paymentFragment.SelectTravelKitFragment;
import com.vidatechft.yupa.retrofitNetwork.PlaceAddressCallback;
import com.vidatechft.yupa.classes.PlaceDetailsGoogleApi.PlaceAddress;
import com.vidatechft.yupa.classes.PlaceDetailsGoogleApi.PlacesDetailsJson;
import com.vidatechft.yupa.classes.Itinerary;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.classes.TravelKit;
import com.vidatechft.yupa.classes.User;
import com.vidatechft.yupa.dialogFragments.LoadingProgressDialogFragment;
import com.vidatechft.yupa.dialogFragments.ProgressIndicatorDialogFragment;
import com.vidatechft.yupa.employerFragments.ApplyEmployerFragment;
import com.vidatechft.yupa.employerFragments.EmployerDashboardContainerFragment;
import com.vidatechft.yupa.employerFragments.PTJobSwipeFragment;
import com.vidatechft.yupa.friends.FriendListFragment;
import com.vidatechft.yupa.hostFragments.ApplyAsHostFragment;
import com.vidatechft.yupa.hostFragments.HostAnAccommodationFragment;
import com.vidatechft.yupa.hostFragments.HostViewAllFragment;
import com.vidatechft.yupa.hostFragments.UploadAccommodationPhotoFragement;
import com.vidatechft.yupa.hostFragments.UploadAccommodationPhotoFragement_2;
import com.vidatechft.yupa.itinerary.ItineraryMainFragment;
import com.vidatechft.yupa.itinerary.ItineraryViewAllFragment;
import com.vidatechft.yupa.merchantShopFragment.ApplyAsMerchantFragment;
import com.vidatechft.yupa.merchantShopFragment.MerchantMapFragment;
import com.vidatechft.yupa.merchantShopFragment.MerchantShopDashboard;
import com.vidatechft.yupa.notificationFragment.MyFirebaseMessagingService;
import com.vidatechft.yupa.profileFragments.ProfileFragment;
import com.vidatechft.yupa.profileSetting.ProfileSettingFragment;
import com.vidatechft.yupa.retrofitNetwork.RetrofitInstance;
import com.vidatechft.yupa.retrofitNetwork.SearchPlaceCallback;
import com.vidatechft.yupa.tourBuddyFragment.TourBuddyProfileFragment;
import com.vidatechft.yupa.tourBuddyFragment.TourBuddyRegisterFragment;
import com.vidatechft.yupa.utilities.Config;
import com.vidatechft.yupa.utilities.CustomMaterialCalendarView;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;
import com.vidatechft.yupa.utilities.KeyboardUtilities;
import com.vidatechft.yupa.utilities.ScreenAdjuster;
import com.vidatechft.yupa.utilities.SlowerLinearLayoutManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.annotation.Nullable;

import io.fabric.sdk.android.Fabric;

//TODO *******************************************
//firestore now supports searching through arrays in it so all those arrays that have been done using the true method should
//change to using an array list instead (now not gonna change it, will change if we needed to query it)
//the search array function only supports up to 1 only, cant search 2 array at the same time
//the name of the function is whereArrayContains
//MAKING BUILD RUN FASTER from here https://medium.com/@wasyl/make-your-gradle-builds-fast-again-ea323ce6a435
//todo lazy load from here https://android.jlelse.eu/recyclerview-with-endlessscroll-2c503008522f   already put the java class in Utilities folder with name EndlessRecyclerOnScrollListener
//todo the toolbar title of each fragments. because of the onDestroy method it will late destroy and set the toolbar name again making it not tally with the current 1
//todo reduce apk size https://riggaroo.co.za/use-android-apk-analyzer-reduce-apk-size/
//todo ask kevin about the part where user can buy itineraries...
//todo maybe listen for inbox here instead and pass to inbox fragment
//todo focus on itinerary and travel buddy chat
//todo all those nearby searchfeature hor maybe don use gps anymore, instead, get the gps from user phone and then query for his current PlaceAddress and then use the placeAddress to query using placeIndex is better

//TODO NOTE ****I CHANGED THE COMPILER TO OFFLINE MODE, IF GOT PROBLEM JUST RESET THE SETTING. REFER FROM HERE https://stackoverflow.com/questions/16775197/building-and-running-app-via-gradle-and-android-studio-is-slower-than-via-eclips

//useful link for increasing recycler view performance from here https://stackoverflow.com/questions/27188536/recyclerview-scrolling-performance
public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        FragmentManager.OnBackStackChangedListener,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks{
    public static final String TAG = MainActivity.class.getName();
    public FirebaseUser firebaseUser;
    public String uid;
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private MainActivity parentActivity;
    private TextView toolbarTitleTV;
// Found solution with change the menu icon when rotate   https://stackoverflow.com/questions/19882443/how-to-change-menuitem-icon-in-actionbar-programmatically
    private Menu menu;
    private ProgressIndicatorDialogFragment progressIndicatorDialogFragment;
    private LoadingProgressDialogFragment loadingProgressDialogFragment, liveLoadingPDF;
    private Boolean firstTime = null;

    //btm navigation image view
    private ConstraintLayout btmNavView;
    private ImageView btmNavNewsfeedIV,btmNavJobIV,btmNavItenaryIV,btmNavChatIV,btmNavHotelIV;
    private int searchViewHeight,searchViewHeight2,notificationHeight;

    //the exit feature (user must click back 2 times to quit)
    private boolean isOkToExit = false;
    private Handler exitHandler = new Handler();
    private Runnable exitRunnable = new Runnable() {
        @Override
        public void run() {
            isOkToExit = false;
        }
    };

    public static boolean isOpen = false;//this is for the custom spinner options

    //all the firestore event listeners
    private ListenerRegistration profileListener,itiListener,travelKitListener,hostAccListener,
                                 bookingNotiListener,currencyRateListener,subscribeChatListener,
                                 blogListener;

    //all the adapters
    private ItineraryRecyclerGridAdapter itiAdapter;
    private HostStayRecyclerGridAdapter hostAccAdapter;
    private BlogRecyclerGridAdapter blogAdapter;

    private boolean isScrollingLeftIti = false;
    private boolean isScrollingLeftHost = false;
    private boolean isScrollingLeftBlog = false;

    //search
    private HashMap<String,LatLng> boundingBox;
    private GoogleApiClient googleApiClient;
    private Search itiSearch,hostSearch;

    private SupportMapFragment mDummyMapInitializer = SupportMapFragment.newInstance();

    //booking notification
    private ArrayList<String> alreadyShownBookingNotiList = new ArrayList<>();

    //check if is host,employer and hotelier or not
    private User thisUser = null;

    private RelativeLayout tutorialBackGradientRL,testRL,testRL2,testRL3;
    private ConstraintLayout tutorialNavBarCL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Fabric.with(this, new Crashlytics());
        parentActivity = this;

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
                if(firebaseUser != null && firebaseUser.getEmail() != null && !firebaseUser.getEmail().isEmpty()){
                    uid = firebaseUser.getUid();
                }else{
//                    GeneralFunction.openMessageNotificationDialog(parentActivity,getString(R.string.error_please_login),R.drawable.notice_bad,TAG);
                    if(!GeneralFunction.isGuest()){
                        new Handler().postDelayed(new Runnable(){
                            public void run(){
                                logout();
                            }
                        }, 350);
                    }

                }

                // to remove status bar then use this before setContentView(layout) in onCreateView method
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                        WindowManager.LayoutParams.FLAG_FULLSCREEN);

                ScreenAdjuster.assistActivity(parentActivity);

                //declare variable here
                drawerLayout = findViewById(R.id.drawer_layout);
                navigationView = findViewById(R.id.nvView);

                //http://v4all123.blogspot.my/2016/02/navigationview-example-in-android.html
                navigationView.setNavigationItemSelectedListener(parentActivity);

                //btm navigation image view
                btmNavNewsfeedIV = findViewById(R.id.btmNavNewsfeedIV);
                btmNavJobIV = findViewById(R.id.btmNavJobIV);
                btmNavItenaryIV = findViewById(R.id.btmNavItenaryIV);
                btmNavChatIV = findViewById(R.id.btmNavChatIV);
                btmNavHotelIV = findViewById(R.id.btmNavHotelIV);
                btmNavView = findViewById(R.id.btmNavView);

                //relative layout
                tutorialBackGradientRL = findViewById(R.id.tutorialBackGradientRL);
                testRL = findViewById(R.id.testRL);
//                testRL2 = findViewById(R.id.testRL2);
                testRL3 = findViewById(R.id.testRL3);

                //Constraint layout
                tutorialNavBarCL = findViewById(R.id.tutorialNavBarCL);

                //function method here
                navigationview();
                customtoolbar();
                setupBtmNavigationView();

                setupSearchFeature();

                getTopItinerary();
                getLatestTravelKit();
                getTopBlog();

                getTopStay();
                //exclude from main activity for now
//                setupShopFeature();
//                setupJobFeature();

                getBookingNotification();
                //TOP BACK BUTTON START
                //Listen for changes in the back stack
                getSupportFragmentManager().addOnBackStackChangedListener(MainActivity.this);
                //Handle when activity is recreated like on orientation Change
                shouldDisplayHomeUp();
                //TOP BACK BUTTON END

                //First time tutorial
                parentActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(isFirstTime()){
                            tutorialBackGradientRL.setVisibility(View.VISIBLE);
                            testRL.setVisibility(View.VISIBLE);
//                            testRL2.setVisibility(View.VISIBLE);
                            testRL3.setVisibility(View.VISIBLE);
                            tutorialNavBarCL.setVisibility(View.VISIBLE);
                            tutorialNavBarCL.setOnClickListener(new DebouncedOnClickListener(500) {
                                @Override
                                public void onDebouncedClick(View v) {
                                    tutorialBackGradientRL.setVisibility(View.GONE);
                                    testRL.setVisibility(View.GONE);
//                                    testRL2.setVisibility(View.GONE);
                                    testRL3.setVisibility(View.GONE);
                                    tutorialNavBarCL.setVisibility(View.GONE);
                                    Toolbar myToolbar = findViewById(R.id.my_toolbar);
                                    myToolbar.findViewById(R.id.inboxMenu).setClickable(true);
                                    myToolbar.findViewById(R.id.hamburgerMenu).setClickable(true);
                                }
                            });
                        }
                        else{
                            tutorialBackGradientRL.setVisibility(View.GONE);
                            testRL.setVisibility(View.GONE);
//                            testRL2.setVisibility(View.GONE);
                            testRL3.setVisibility(View.GONE);
                            tutorialNavBarCL.setVisibility(View.GONE);
                        }
                    }
                });
            }
        });

        // Fixing Later Map loading Delay
        GeneralFunction.makeGoogleMapNoDelay(parentActivity);
        MapsInitializer.initialize(this);
        mDummyMapInitializer.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                Log.d(TAG, "onMapReady");
            }
        });

        //get currency rates
        currencyRateListener = CurrencyRate.syncCurrencyFromDB(parentActivity);
        //subscribe notification
        if(!GeneralFunction.isGuest()){
            //check if is host,employer and hotelier or not
            getUserProfile();

            //listen to firebase messaging push notification service
//                FirebaseMessaging.getInstance().subscribeToTopic(parentActivity.uid + MyFirebaseMessagingService.TYPE_INBOX);
//        FirebaseApp.initializeApp(parentActivity);
//        FirebaseMessaging.getInstance()
//                .subscribeToTopic("noti")
//                .addOnCompleteListener(new OnCompleteListener<Void>() {
//                    @Override
//                    public void onComplete(@NonNull Task<Void> task) {
//                        if (!task.isSuccessful()) {
//                            Log.e(TAG, "failed subscribe");
//                        }
//                    }
//                });

            subscribeAllChat();
            GeneralFunction.subscribeInbox(parentActivity.uid,TAG);

            //listen for deep linking
            GeneralFunction.listenForDeepLink(parentActivity,TAG);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    //i think when it comes to this page too fast and it havent finished storing the sp, so sometimes 1st time wont run this function properly because SP is still null thats why i need slow down this 1
                    handleDeepLink();
                }
            }, 300);
        }else{
            //hide everything if is guest
            Menu navMenu = navigationView.getMenu();
//            navMenu.findItem(R.id.nav_user_profile).setVisible(false);
            navMenu.findItem(R.id.nav_update_user_contact).setVisible(false);
            navMenu.findItem(R.id.nav_update_user_payment).setVisible(false);
            navMenu.findItem(R.id.nav_user_reward).setVisible(false);
            navMenu.findItem(R.id.nav_invite_friend).setVisible(false);
            navMenu.findItem(R.id.nav_friend_list).setVisible(false);
            navMenu.findItem(R.id.nav_apply_employer).setVisible(false);
            navMenu.findItem(R.id.nav_employer_dashboard).setVisible(false);
            navMenu.findItem(R.id.nav_apply_host).setVisible(false);
            navMenu.findItem(R.id.nav_host_dashboard).setVisible(false);
            navMenu.findItem(R.id.nav_apply_hotelier).setVisible(false);
            navMenu.findItem(R.id.nav_hotelier_dashboard).setVisible(false);
            navMenu.findItem(R.id.nav_apply_merchant).setVisible(false);
            navMenu.findItem(R.id.nav_merchant_dashboard).setVisible(false);
            navMenu.findItem(R.id.nav_apply_tour_buddy).setVisible(false);
            navMenu.findItem(R.id.nav_tour_buddy_profile).setVisible(false);
            navMenu.findItem(R.id.nav_setting).setVisible(false);
            navMenu.findItem(R.id.nav_change_password).setVisible(false);

            View headerView = navigationView.getHeaderView(0);
            headerView.setVisibility(View.GONE);
        }

    }

    private void handleDeepLink(){
        final String inviteeUid = GeneralFunction.getSpString(parentActivity,Config.SP_DEEP_LINK_TARGET_ID);
        final String type = GeneralFunction.getSpString(parentActivity,Config.SP_DEEP_LINK_TYPE);

        //clear the sp because this should be one time only
        GeneralFunction.clearSharedPreferencesWithType(parentActivity,Config.SP_DEEP_LINK_TARGET_ID);
        GeneralFunction.clearSharedPreferencesWithType(parentActivity,Config.SP_DEEP_LINK_TYPE);

        if(inviteeUid != null && type != null){
            switch (type){
                case Config.SP_DEEP_LINK_TYPE_PROFILE:
                    initializeLoadingDialog(parentActivity.getString(R.string.downloading_invitee_profile),false);
                    controlLoadingDialog(true,parentActivity);

                    GeneralFunction.getFirestoreInstance()
                            .collection(User.URL_USER)
                            .document(inviteeUid)
                            .get()
                            .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                    controlLoadingDialog(false,parentActivity);
                                    if(GeneralFunction.hasErrorDoc(parentActivity,task,TAG)){
                                        return;
                                    }

                                    if(task.getResult() != null && task.getResult().exists()){
                                        User tempUser = new User(parentActivity,task.getResult());

                                        Fragment profileFragment = ProfileFragment.newInstance(parentActivity,tempUser,false);
                                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, profileFragment).addToBackStack(ProfileFragment.TAG).commit();
                                    }
                                }
                            });
                    break;

                case Config.SP_DEEP_LINK_TYPE_BLOG:
                    initializeLoadingDialog(parentActivity.getString(R.string.downloading_share_blog),false);
                    controlLoadingDialog(true,parentActivity);

                    GeneralFunction.getFirestoreInstance()
                            .collection(Blog.URL_BLOG)
                            .document(inviteeUid)
                            .get()
                            .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                    controlLoadingDialog(false,parentActivity);
                                    if(GeneralFunction.hasErrorDoc(parentActivity,task,TAG)){
                                        return;
                                    }

                                    if(task.getResult() != null && task.getResult().exists()){
                                        Blog tempBlog = Blog.snapshotToBlog(parentActivity,task.getResult());

                                        Fragment blogMainFragment = BlogMainFragment.newInstance(parentActivity,tempBlog);
                                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, blogMainFragment).addToBackStack(BlogMainFragment.TAG).commit();
                                    }
                                }
                            });
                    break;
            }
        }
    }

//    private void retrofitExample(){
//        GetPlaceDetailsService service = RetrofitInstance.getRetrofitInstance().create(GetPlaceDetailsService.class);
//        Call<PlacesDetailsJson> call = service.getPlaceDetails("ChIJN1t_tDeuEmsRUsoyG83frY4",Config.ADDRESS_COMPONENT,parentActivity.getString(R.string.google_geo_api_key));
//        call.enqueue(new Callback<PlacesDetailsJson>() {
//            @Override
//            public void onResponse(@NonNull Call<PlacesDetailsJson> call, @NonNull Response<PlacesDetailsJson> response) {
//                //end progress dialog
//                if (response.errorBody() != null) {
//                    try {
//                        Log.e(TAG,response.errorBody().string());
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_occured_getting_place_details),R.drawable.notice_bad,TAG);
//                    return;
//                }
//
//                boolean hasError = false;
//                PlacesDetailsJson result = response.body();
//                if (result != null) {
//                    if(result.placesDetailsResult != null && result.placesDetailsResult.addressComponents != null){
//                        ArrayList<AddressComponent> addressComponents = result.placesDetailsResult.addressComponents;
//                        for(AddressComponent addressComponent : addressComponents){
//                            Log.e(TAG,addressComponent.longName);
//                            Log.e(TAG,addressComponent.shortName);
//                            Log.e(TAG,addressComponent.types.get(0));
//                        }
//                    }else{
//                        hasError = true;
//                    }
//                }else{
//                    hasError = true;
//                }
//
//                if(hasError){
//                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_occured_getting_place_details),R.drawable.notice_bad,TAG);
//                }
//            }
//
//            @Override
//            public void onFailure(@NonNull Call<PlacesDetailsJson> call, @NonNull Throwable t) {
//                //end progress dialog
//                Log.e("retrofit gg",t.getMessage());
//                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_occured_getting_place_details),R.drawable.notice_bad,TAG);
//            }
//        });
//    }

    private void customCallback(){
        RetrofitInstance.getPlaceAddressCall(parentActivity,"ChIJN1t_tDeuEmsRUsoyG83frY4")
                .enqueue(new PlaceAddressCallback<PlacesDetailsJson>() {
                    @Override
                    public void onPlaceDetailsSuccess(PlaceAddress placeAddress) {
                        GeneralFunction.openMessageNotificationDialog(parentActivity,placeAddress.country,R.drawable.notice_good,TAG);
                        GeneralFunction.openMessageNotificationDialog(parentActivity,placeAddress.placeName,R.drawable.notice_good,TAG);
                    }

                    @Override
                    public void onPlaceDetailsFailure(Exception e) {
                        GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_occured_getting_place_details),R.drawable.notice_bad,TAG);
                        e.printStackTrace();
                    }
                });

        RetrofitInstance.searchPlaceQueryCall(parentActivity,"akihabara")
                .enqueue(new SearchPlaceCallback<SearchPlaceResult>() {
                    @Override
                    public void onSearchPlaceSuccess(String placeId) {
                        GeneralFunction.openMessageNotificationDialog(parentActivity,placeId,R.drawable.notice_good,TAG);
                        Log.e(TAG,placeId);
                    }

                    @Override
                    public void onSearchPlaceFailure(Exception e) {
                        GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_occured_getting_place_details),R.drawable.notice_bad,TAG);
                        e.printStackTrace();
                    }
                });
    }

    private void subscribeAllChat(){
        subscribeChatListener = GeneralFunction.getFirestoreInstance()
                .collection(Chat.URL_CHAT)
                .whereEqualTo(uid,true)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
                        if(GeneralFunction.hasError(parentActivity,e,TAG)){
                            return;
                        }

                        if(snapshots != null && !snapshots.isEmpty()){
                            for (final DocumentChange dc : snapshots.getDocumentChanges()) {
                                switch (dc.getType()) {
                                    case ADDED:
                                        if(dc.getDocument().exists()){
                                            final String subChatId = dc.getDocument().getId() + parentActivity.uid;
                                            FirebaseMessaging.getInstance()
                                                    .subscribeToTopic(subChatId)
                                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            if (!task.isSuccessful()) {
                                                                Log.e(TAG, "failed subscribe: " + dc.getDocument().getId());
                                                            }
                                                            GeneralFunction.saveSPStringIntoKey(parentActivity,subChatId,"*.*",MyFirebaseMessagingService.SP_CHAT_KEY);
                                                        }
                                                    });
                                        }
                                        break;
                                    case MODIFIED:
                                        break;
                                    case REMOVED:
                                        break;
                                }
                            }
                        }
                    }
                });
    }

    private void unsubscribeAllChat(){
        for(final String chatId : GeneralFunction.getSavedSPStringFromKeyAsArray(parentActivity,MyFirebaseMessagingService.SP_CHAT_KEY)){
            FirebaseMessaging.getInstance()
                    .unsubscribeFromTopic(chatId)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (!task.isSuccessful()) {
                                Log.e(TAG, "failed unsubscribe: " + chatId);
                            }
                        }
                    });
        }
    }

    //***********************************************************check if is host,employer and hotelier or not*************************************************************************
    private void getUserProfile(){
        final Menu navMenu = navigationView.getMenu();

        if(uid != null && !uid.isEmpty()){
            profileListener = GeneralFunction.getFirestoreInstance()
                    .collection(User.URL_USER)
                    .document(uid)
                    .addSnapshotListener(new EventListener<DocumentSnapshot>() {
                        @Override
                        public void onEvent(@javax.annotation.Nullable DocumentSnapshot documentSnapshot, @javax.annotation.Nullable final FirebaseFirestoreException e) {
                            if(GeneralFunction.hasError(parentActivity,e,TAG)){
                                return;
                            }

                            if(documentSnapshot != null && documentSnapshot.exists()){
                                thisUser = new User(parentActivity,documentSnapshot);

                                if(thisUser.isEmployer != null && thisUser.isEmployer){
                                    navMenu.findItem(R.id.nav_apply_employer).setVisible(false);
                                    navMenu.findItem(R.id.nav_employer_dashboard).setVisible(true);
                                }else{
                                    navMenu.findItem(R.id.nav_apply_employer).setVisible(true);
                                    navMenu.findItem(R.id.nav_employer_dashboard).setVisible(false);
                                }

                                if(thisUser.isHost != null && thisUser.isHost){
                                    navMenu.findItem(R.id.nav_apply_host).setVisible(false);
                                    navMenu.findItem(R.id.nav_host_dashboard).setVisible(true);
                                }else{
                                    navMenu.findItem(R.id.nav_apply_host).setVisible(true);
                                    navMenu.findItem(R.id.nav_host_dashboard).setVisible(false);
                                }

                                if(thisUser.isHotelier != null && thisUser.isHotelier){
                                    navMenu.findItem(R.id.nav_apply_hotelier).setVisible(false);
                                    navMenu.findItem(R.id.nav_hotelier_dashboard).setVisible(false);
                                }else{
                                    navMenu.findItem(R.id.nav_apply_hotelier).setVisible(false);
                                    navMenu.findItem(R.id.nav_hotelier_dashboard).setVisible(false);
                                }

                                if(thisUser.isMerchant != null && thisUser.isMerchant){
                                    navMenu.findItem(R.id.nav_apply_merchant).setVisible(false);
                                    navMenu.findItem(R.id.nav_merchant_dashboard).setVisible(true);
                                }else{
                                    navMenu.findItem(R.id.nav_apply_merchant).setVisible(true);
                                    navMenu.findItem(R.id.nav_merchant_dashboard).setVisible(false);
                                }

                                if(thisUser.isTourBuddy != null && thisUser.isTourBuddy){
                                    navMenu.findItem(R.id.nav_apply_tour_buddy).setVisible(false);
                                    navMenu.findItem(R.id.nav_tour_buddy_profile).setVisible(true);
                                }else{
                                    navMenu.findItem(R.id.nav_apply_tour_buddy).setVisible(true);
                                    navMenu.findItem(R.id.nav_tour_buddy_profile).setVisible(false);
                                }

                                initiateHeaderView();
                            }

                        }
                    });
        }
    }

    public User getThisUser(){
        return thisUser;
    }

    //***********************************************************SEARCH START*************************************************************************

    public void setBoundingBox(HashMap<String,LatLng> boundingBox){
        this.boundingBox = boundingBox;
    }

    private void setupSearchFeature(){
        //****************************************INITIALIZE THE GOOGLE API CLIENT FOR SEARCH AUTOCOMPLETE TO USE*************************************************/
        googleApiClient = new GoogleApiClient.Builder(parentActivity)
                .addApi(Places.GEO_DATA_API)
                .addConnectionCallbacks(MainActivity.this)
                .addOnConnectionFailedListener(MainActivity.this)
                .build();
        MapsInitializer.initialize(parentActivity);
        googleApiClient.connect();

        final PlaceAutoCompleteAdapter placeAutoCompleteAdapter = new PlaceAutoCompleteAdapter(parentActivity, R.layout.adapter_place_auto_complete,
                googleApiClient, null, null);

        //************************************************THE NORMAL ONE****************************************************************//
        final TextView itineraryTabTV = findViewById(R.id.itineraryTabTV);
        final TextView homestayTabTV = findViewById(R.id.homestayTabTV);
        final Button searchBtn = findViewById(R.id.searchBtn);
        final CustomMaterialCalendarView calendarView = findViewById(R.id.calendarView);

        //************************************************FOR ITINERARY ONE****************************************************************//
        itiSearch = new Search();
        final ConstraintLayout searchItiCL = findViewById(R.id.searchItiCL);
        final ConstraintLayout itiDateSearchCL = findViewById(R.id.itiDateSearchCL);
        final AutoCompleteTextView itiSearchATV = findViewById(R.id.itiSearchATV);
        final ImageView itiSearchIV = findViewById(R.id.itiSearchIV);
        final ImageView itiCloseIV = findViewById(R.id.itiCloseIV);
        final TextView itiStartDateTV = findViewById(R.id.itiStartDateTV);
        final TextView itiEndDateTV = findViewById(R.id.itiEndDateTV);
        final CheckBox itiSearchModeCB = findViewById(R.id.itiSearchModeCB);

        //************************************************FOR HOST ONE****************************************************************//
        hostSearch = new Search();
        final ConstraintLayout searchHostCL = findViewById(R.id.searchHostCL);
        final AutoCompleteTextView hostSearchATV = findViewById(R.id.hostSearchATV);
        final ImageView hostSearchIV = findViewById(R.id.hostSearchIV);
        final ImageView hostCloseIV = findViewById(R.id.hostCloseIV);
        final TextView hostDateTV = findViewById(R.id.hostDateTV);
        final EditText hostGuestET = findViewById(R.id.hostGuestET);
        final CheckBox hostSearchModeCB = findViewById(R.id.hostSearchModeCB);

        //************************************************ON CLICK LISTENER FOR THE NORMAL ONE***************************************************************//
        itineraryTabTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchItiCL.setVisibility(View.VISIBLE);
                searchHostCL.setVisibility(View.GONE);

                itineraryTabTV.setBackground(parentActivity.getResources().getDrawable(R.drawable.square_black_border_btm));
                homestayTabTV.setBackground(parentActivity.getResources().getDrawable(R.drawable.tab_background_itinerary_details));

                itineraryTabTV.setTextColor(parentActivity.getResources().getColor(R.color.black));
                homestayTabTV.setTextColor(parentActivity.getResources().getColor(R.color.dark_gray));
            }
        });

        homestayTabTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchItiCL.setVisibility(View.GONE);
                searchHostCL.setVisibility(View.VISIBLE);

                itineraryTabTV.setBackground(parentActivity.getResources().getDrawable(R.drawable.tab_background_itinerary_details));
                homestayTabTV.setBackground(parentActivity.getResources().getDrawable(R.drawable.square_black_border_btm));

                itineraryTabTV.setTextColor(parentActivity.getResources().getColor(R.color.dark_gray));
                homestayTabTV.setTextColor(parentActivity.getResources().getColor(R.color.black));
            }
        });

        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //i purposely put search.placeAddress = null is because dont know why when fragment updated the search in the fragment, the respoective search variable in activity 1 also kena changed
                if(searchItiCL.getVisibility() == View.VISIBLE){
                    if(itiSearch.placeId != null && !itiSearch.placeId.trim().isEmpty()){
                        itiSearch.placeAddress = null;
                        Fragment searchResultFragment = SearchResultFragment.newInstance(parentActivity,itiSearch,Search.TYPE_ITINERARY,itiSearchModeCB.isChecked());
                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, searchResultFragment).addToBackStack(SearchResultFragment.TAG).commit();
                    }else{
                        GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.landing_page_search_iti_validation_error),R.drawable.notice_bad,TAG);
                    }
                }else if(searchHostCL.getVisibility() == View.VISIBLE){
                    if(hostSearch.placeId != null && !hostSearch.placeId.trim().isEmpty()){
                        hostSearch.placeAddress = null;
                        try{
                            hostSearch.noofGuest = Integer.parseInt(hostGuestET.getText().toString());
                        }catch (Exception e){

                        }
                        Fragment searchResultFragment = SearchResultFragment.newInstance(parentActivity,hostSearch,Search.TYPE_HOMESTAY,hostSearchModeCB.isChecked());
                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, searchResultFragment).addToBackStack(SearchResultFragment.TAG).commit();
                    }else{
                        GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.landing_page_search_host_validation_error),R.drawable.notice_bad,TAG);
                    }
                }else{
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_occured),R.drawable.notice_bad,TAG);
                }
            }
        });

        //***********************************************SEARCH FOR TRAVELLING EXPERIENCE / ITINERARY******************************************************************************//
        itiDateSearchCL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GeneralFunction.slideUpAnimationForView(parentActivity,calendarView);
                MainActivity.isOpen = true;
            }
        });

        itiSearchIV.setBackground(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.ic_search_white_48dp,R.color.black));
        itiCloseIV.setBackground(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.ic_close_black_24dp,R.color.black));

        itiSearchATV.setAdapter(placeAutoCompleteAdapter);

        itiSearchATV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final PlaceAutoCompleteAdapter.PlaceAutocomplete item = placeAutoCompleteAdapter.getItem(position);
                if(item == null){
                    Log.e(TAG, "Place Autocomplete is null");
                    return;
                }

                itiSearch.placeId = item.placeId.toString();
            }
        });

        itiSearchATV.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().isEmpty()){
                    itiCloseIV.setVisibility(View.GONE);
                }else{
                    itiCloseIV.setVisibility(View.VISIBLE);
                }
            }
        });

        itiCloseIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itiSearchATV.setText("");
            }
        });

        //***********************************************SEARCH FOR HOME STAY******************************************************************************//
        hostDateTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GeneralFunction.slideUpAnimationForView(parentActivity,calendarView);
                MainActivity.isOpen = true;
            }
        });

        hostSearchIV.setBackground(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.ic_search_white_48dp,R.color.black));
        hostCloseIV.setBackground(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.ic_close_black_24dp,R.color.black));

        hostSearchATV.setAdapter(placeAutoCompleteAdapter);

        hostSearchATV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final PlaceAutoCompleteAdapter.PlaceAutocomplete item = placeAutoCompleteAdapter.getItem(position);
                if(item == null){
                    Log.e(TAG, "Place Autocomplete is null");
                    return;
                }

                hostSearch.placeId = item.placeId.toString();
            }
        });

        hostSearchATV.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().isEmpty()){
                    hostCloseIV.setVisibility(View.GONE);
                }else{
                    hostCloseIV.setVisibility(View.VISIBLE);
                }
            }
        });

        hostCloseIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hostSearchATV.setText("");
            }
        });

        //************************************************INIT THE CALENDAR*************************************************************************/
//
        calendarView.setContext(parentActivity);
        calendarView.setSelectionMode(CustomMaterialCalendarView.SELECTION_MODE_SINGLE_RANGE);
        calendarView.setOnDateSelected(new CustomMaterialCalendarView.OnDateSelected() {
            @Override
            public void onDateSelected(Date selectedDate) {
                if(searchItiCL.getVisibility() == View.VISIBLE){
                    itiSearch.date = selectedDate.getTime();
                    itiSearch.checkin_date = selectedDate.getTime();
                    itiSearch.checkout_date = selectedDate.getTime();

                    itiStartDateTV.setText(GeneralFunction.formatDateToString(itiSearch.checkin_date));
                    itiEndDateTV.setText(GeneralFunction.formatDateToString(itiSearch.checkout_date));
                }else if(searchHostCL.getVisibility() == View.VISIBLE){
                    hostSearch.date = selectedDate.getTime();
                    hostSearch.checkin_date = selectedDate.getTime();
                    hostSearch.checkout_date = selectedDate.getTime();

                    String date = GeneralFunction.formatDateToString(hostSearch.checkin_date) + "-" + GeneralFunction.formatDateToString(hostSearch.checkout_date);
                    hostDateTV.setText(date);
                }

                GeneralFunction.slideDownAnimationForView(parentActivity,calendarView);
                MainActivity.isOpen = false;
            }

            @Override
            public void onRangeSelected(ArrayList<Date> selectedDates, boolean isMultiRange) {
                if(searchItiCL.getVisibility() == View.VISIBLE){
                    itiSearch.date = selectedDates.get(0).getTime();
                    itiSearch.checkin_date = selectedDates.get(0).getTime();
                    itiSearch.checkout_date = selectedDates.get(selectedDates.size() - 1).getTime();

                    itiStartDateTV.setText(GeneralFunction.formatDateToString(itiSearch.checkin_date));
                    itiEndDateTV.setText(GeneralFunction.formatDateToString(itiSearch.checkout_date));
                }else if(searchHostCL.getVisibility() == View.VISIBLE){
                    hostSearch.date = selectedDates.get(0).getTime();
                    hostSearch.checkin_date = selectedDates.get(0).getTime();
                    hostSearch.checkout_date = selectedDates.get(selectedDates.size() - 1).getTime();

                    String date = GeneralFunction.formatDateToString(hostSearch.checkin_date) + " - " + GeneralFunction.formatDateToString(hostSearch.checkout_date);
                    hostDateTV.setText(date);
                }

                GeneralFunction.slideDownAnimationForView(parentActivity,calendarView);
                MainActivity.isOpen = false;
            }

            @Override
            public void onCanceled() {
                if(searchItiCL.getVisibility() == View.VISIBLE){
                    itiSearch.date = 0;
                    itiSearch.checkin_date = 0;
                    itiSearch.checkout_date = 0;

                    itiStartDateTV.setText("");
                    itiEndDateTV.setText("");
                }else if(searchHostCL.getVisibility() == View.VISIBLE){
                    hostSearch.date = 0;
                    hostSearch.checkin_date = 0;
                    hostSearch.checkout_date = 0;

                    hostDateTV.setText("");
                }

                GeneralFunction.slideDownAnimationForView(parentActivity,calendarView);
                MainActivity.isOpen = false;
            }
        });

        //***********************************************THIS IS FOR TUTORIAL ONLY***********************************************************/
        if(isFirstTime()){
            ViewTreeObserver vto = searchBtn.getViewTreeObserver();
            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                        findViewById(R.id.searchBtn).getViewTreeObserver().removeOnGlobalLayoutListener(this);

                    } else {
                        searchBtn.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                    searchViewHeight = searchBtn.getMeasuredHeight();
                }
            });

            final TextView itiLabelTV = findViewById(R.id.itiLabelTV);
            ViewTreeObserver vto2 = itiLabelTV.getViewTreeObserver();
            vto2.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                        findViewById(R.id.itiLabelTV).getViewTreeObserver().removeOnGlobalLayoutListener(this);

                    } else {
                        itiLabelTV.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                    searchViewHeight2 = itiLabelTV.getMeasuredHeight();
                }
            });
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

        Log.v(TAG,connectionResult.toString());
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    private void setupShopFeature(){
        final ImageView shopIV = findViewById(R.id.shopIV);
        final TextView getNearbyShopTV = findViewById(R.id.getNearbyShopTV);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                GeneralFunction.GlideImageSettingOwnDrawable(parentActivity,R.drawable.bg_nearby_shop,shopIV);
                shopIV.requestLayout();

                getNearbyShopTV.setOnClickListener(new DebouncedOnClickListener(500) {
                    @Override
                    public void onDebouncedClick(View v) {
                        Fragment merchantMapFragment = MerchantMapFragment.newInstance(parentActivity);
                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, merchantMapFragment).addToBackStack(MerchantMapFragment.TAG).commit();
                    }
                });
            }
        });
    }

    private void setupJobFeature(){
        final ImageView jobIV = findViewById(R.id.jobIV);
        final TextView matchJobTV = findViewById(R.id.matchJobTV);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                GeneralFunction.GlideImageSettingOwnDrawable(parentActivity,R.drawable.bg_earn_travel,jobIV);
                jobIV.requestLayout();

                matchJobTV.setOnClickListener(new DebouncedOnClickListener(500) {
                    @Override
                    public void onDebouncedClick(View v) {
                        Fragment ptjobSwipeFragment = PTJobSwipeFragment.newInstance(parentActivity,parentActivity.getString(R.string.app_name),null);
                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, ptjobSwipeFragment).addToBackStack(PTJobSwipeFragment.TAG).commit();
                    }
                });
            }
        });
    }

    //**********************************************SEARCH END**************************************************

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
//            // Do something for lollipop and above versions
//        } else{
//            // do something for phones running an SDK before lollipop
//        }
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(isFirstTime()){
                    TextView imageView =   findViewById(R.id.redotIV);
//                    TextView imageView2 =   findViewById(R.id.redotIV2);
                    TextView notificationCircle =   findViewById(R.id.notificationCircle);
                    TextView notificationTV =   findViewById(R.id.notificationTV);
//                    TextView redotTV2 =   findViewById(R.id.redotTV2);
                    View notificationLine =   findViewById(R.id.notificationLine);
//                    View redotLine2 =   findViewById(R.id.redotLine2);
                    RelativeLayout relativeLayout = findViewById(R.id.testRL);
//                    RelativeLayout relativeLayout2 = findViewById(R.id.testRL2);
                    RelativeLayout relativeLayout3 = findViewById(R.id.testRL3);
                    final Toolbar myToolbar = findViewById(R.id.my_toolbar);

//        RelativeLayout navFeedRL = findViewById(R.id.navFeedRL);
                    int actionBarHeight = (int) parentActivity.getResources().getDimension(R.dimen.custom_action_bar_size);

                    //获取控件的位置和大小
                    final int[] searchLocation = new int[2];
                    final int[] searchLocation2 = new int[2];
                    final int[] searchLocation3 = new int[2];
                    final int[] itiLabelIV_location = new int[2];
                    findViewById(R.id.searchBtn).getLocationOnScreen(searchLocation);
                    findViewById(R.id.itiLabelTV).getLocationOnScreen(searchLocation2);
                    myToolbar.findViewById(R.id.inboxMenu).getLocationOnScreen(searchLocation3);
                    findViewById(R.id.itiLabelIV).getLocationOnScreen(itiLabelIV_location);

                    int searchX = searchLocation[0];
                    int searchY = searchLocation[1];
                    int searchX2 = searchLocation2[0];
                    int searchY2 = searchLocation2[1];
                    int searchX3 = searchLocation3[0];
                    int searchY3 = searchLocation3[1];
                    int itiLabelIV_X = itiLabelIV_location[0];
                    int itiLabelIV_Y = itiLabelIV_location[1];

                    // 控件最终视觉位置(如果有移动则是移动过后的位置)距离手机屏幕屏幕左边、上边的距离Log.i(TAG, "OnScreenX=" + x);
//        GeneralFunction.Toast(parentActivity,"position x : "+String.valueOf(btmNavNewsfeedX)+"position y :" +String.valueOf(btmNavNewsfeedY));
                    // Setting layout params to our RelativeLayout
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    RelativeLayout.LayoutParams redot2Circle = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    RelativeLayout.LayoutParams redotLine2LP = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    RelativeLayout.LayoutParams redotTV2LP = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    RelativeLayout.LayoutParams notificationCircleLP = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    RelativeLayout.LayoutParams notificationLineLP = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    RelativeLayout.LayoutParams notificationTVLP = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    int right =  findViewById(R.id.itiLabelTV).getRight();
                    int left =  findViewById(R.id.itiLabelTV).getLeft();
//                    int redot2CircleRight =  findViewById(R.id.redotIV2).getRight();
//                    int redot2CircleLeft =  findViewById(R.id.redotIV2).getLeft();
                    int notificationCircleRight =   myToolbar.findViewById(R.id.inboxMenu).getRight();
                    int notificationCircleLeft =   myToolbar.findViewById(R.id.inboxMenu).getLeft();
                    int itiLabelIV_right =   findViewById(R.id.itiLabelIV).getRight();
                    int itiLabelIV_left =   findViewById(R.id.itiLabelIV).getLeft();

                    // Setting position of our ImageView
                    //start explore the world
                    layoutParams.topMargin = (int) (searchY - actionBarHeight - (parentActivity.getResources().getDimension(R.dimen.medium_margin)/2));
                    //explore the world
//                    redot2Circle.leftMargin = (int) (itiLabelIV_X - (itiLabelIV_right - itiLabelIV_left) - parentActivity.getResources().getDimension(R.dimen.small_padding)/2 + parentActivity.getResources().getDimension(R.dimen.smaller_padding)/2);
//                    redot2Circle.topMargin = (int) (itiLabelIV_Y - actionBarHeight  - (parentActivity.getResources().getDimension(R.dimen.medium_padding)/2));
//                    redotLine2LP.leftMargin = (int) (itiLabelIV_X - (itiLabelIV_right - itiLabelIV_left)/2 + parentActivity.getResources().getDimension(R.dimen.small_padding)/2 + + parentActivity.getResources().getDimension(R.dimen.smaller_padding)/2);
//                    redotLine2LP.width = 2;
//                    redotLine2LP.height = 40;
//                    redotLine2LP.addRule(RelativeLayout.BELOW, R.id.redotIV2);
//                    redotTV2LP.leftMargin = (int) (itiLabelIV_X - (itiLabelIV_left/2)+ parentActivity.getResources().getDimension(R.dimen.small_padding));
//                    redotTV2LP.addRule(RelativeLayout.BELOW, R.id.redotLine2);
                    //notification
                    notificationCircleLP.leftMargin = (int) (searchX3+ parentActivity.getResources().getDimension(R.dimen.medium_margin)/2);
                    notificationCircleLP.topMargin = searchY3;
                    notificationLineLP.leftMargin = searchX3 + (notificationCircleRight/2);
                    notificationLineLP.width = 2;
                    notificationLineLP.height = 40;
                    notificationLineLP.addRule(RelativeLayout.BELOW, R.id.notificationCircle);
                    notificationTVLP.leftMargin = (int) (searchX3 + (notificationCircleLeft / 2) - parentActivity.getResources().getDimension(R.dimen.small_margin));
                    notificationTVLP.addRule(RelativeLayout.BELOW, R.id.notificationLine);

                    //remove view before add new view
                    if(imageView.getParent()!=null){
                        ((ViewGroup)imageView.getParent()).removeView(imageView); // <- fix
                    }

//                    if(imageView2.getParent()!=null){
//                        ((ViewGroup)imageView2.getParent()).removeView(imageView2); // <- fix
//                    }

//                    if(redotLine2.getParent()!=null){
//                        ((ViewGroup)redotLine2.getParent()).removeView(redotLine2); // <- fix
//                    }

//                    if(redotTV2.getParent()!=null){
//                        ((ViewGroup)redotTV2.getParent()).removeView(redotTV2); // <- fix
//                    }

                    if(notificationCircle.getParent()!=null){
                        ((ViewGroup)notificationCircle.getParent()).removeView(notificationCircle); // <- fix
                    }

                    if(notificationLine.getParent()!=null){
                        ((ViewGroup)notificationLine.getParent()).removeView(notificationLine); // <- fix
                    }

                    if(notificationTV.getParent()!=null){
                        ((ViewGroup)notificationTV.getParent()).removeView(notificationTV); // <- fix
                    }
                    // Finally Adding the imageView to RelativeLayout and its position
                    relativeLayout.addView(imageView, layoutParams);

//                    relativeLayout2.addView(imageView2, redot2Circle);
//                    relativeLayout2.addView(redotLine2, redotLine2LP);
//                    relativeLayout2.addView(redotTV2, redotTV2LP);

                    relativeLayout3.addView(notificationCircle, notificationCircleLP);
                    relativeLayout3.addView(notificationLine, notificationLineLP);
                    relativeLayout3.addView(notificationTV, notificationTVLP);
                    //bring layout to front in android 4.0
                    relativeLayout3.bringToFront();
                }
            }
        });

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }


    //***********************************************************SEARCH*************************************************************************

    private void getTopItinerary(){
        final RecyclerView itiRV = findViewById(R.id.itiRV);
        final ArrayList<Itinerary> itiList = new ArrayList<>();
        final ImageView leftItiArrowIV = findViewById(R.id.leftItiArrowIV);
        final ImageView rightItiArrowIV = findViewById(R.id.rightItiArrowIV);
        final LinearLayoutManager linearLayoutManager = new SlowerLinearLayoutManager(parentActivity,LinearLayoutManager.HORIZONTAL,false);

        itiListener = GeneralFunction.getFirestoreInstance()
                .collection(Itinerary.URL_ITINERARY)
                .whereEqualTo(Itinerary.IS_PUBLIC,true)
                .whereEqualTo(Itinerary.IS_PUBLISH,true)
                .orderBy(Itinerary.LIKE_COUNT, Query.Direction.DESCENDING)
                .orderBy(Itinerary.DATE_UPDATED, Query.Direction.DESCENDING)
                .limit(5)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot snapshots, @javax.annotation.Nullable final FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w(TAG, "Listen failed.", e);
                            return;
                        }

                        itiList.clear();

                        if (snapshots != null && !snapshots.isEmpty()) {
                            for (QueryDocumentSnapshot snapshot : snapshots) {
                                Itinerary tempIti = new Itinerary(parentActivity, snapshot);
                                itiList.add(tempIti);
                            }

                            if(itiAdapter != null && !snapshots.getMetadata().isFromCache() && itiListener != null){
                                itiListener.remove();
                            }
                        } else {
                            itiList.clear();
                        }

                        parentActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                itiAdapter = new ItineraryRecyclerGridAdapter(parentActivity,null, itiList,false,false,true);

                                itiRV.setNestedScrollingEnabled(false);
                                itiRV.setAdapter(itiAdapter);
                                itiRV.setLayoutManager(linearLayoutManager);
                            }
                        });
                    }
                });

        findViewById(R.id.itiViewAllTV).setOnClickListener(new DebouncedOnClickListener(500) {
            @Override
            public void onDebouncedClick(View v) {
                Fragment itineraryViewAllFragment = ItineraryViewAllFragment.newInstance(parentActivity,null,null);
                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, itineraryViewAllFragment).addToBackStack(ItineraryViewAllFragment.TAG).commit();
            }
        });

        leftItiArrowIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    itiRV.smoothScrollToPosition(linearLayoutManager.findFirstVisibleItemPosition() - 1);
                }catch (Exception e){
                    itiRV.smoothScrollToPosition(0);
                }

            }
        });

        rightItiArrowIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    itiRV.smoothScrollToPosition(linearLayoutManager.findLastVisibleItemPosition() + 1);
                }catch (Exception e){
                    itiRV.smoothScrollToPosition(itiAdapter.getItemCount() - 1);
                }
            }
        });


        itiRV.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                //if first item is not fully visible then show left arrow
                //if last item is not fully visible show right arrow
                if (linearLayoutManager.findFirstCompletelyVisibleItemPosition() == 0) {
                    leftItiArrowIV.setVisibility(View.GONE);
                } else {
                    leftItiArrowIV.setVisibility(View.VISIBLE);
                }

                if (itiAdapter != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == itiAdapter.getItemCount() - 1) {
                    rightItiArrowIV.setVisibility(View.GONE);
                } else {
                    rightItiArrowIV.setVisibility(View.VISIBLE);
                }

                if (dx > 0) {
                    isScrollingLeftIti = false;
//                    System.out.println("Scrolled Right");
                } else if (dx < 0) {
                    isScrollingLeftIti = true;
//                    System.out.println("Scrolled Left");
                } else {
                    isScrollingLeftIti = false;
                }
            }

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                switch (newState) {
                    case RecyclerView.SCROLL_STATE_IDLE:
                        int position = 0;
                        if(isScrollingLeftIti){
                            position = linearLayoutManager.findFirstVisibleItemPosition();
                        }else{
                            position = linearLayoutManager.findLastVisibleItemPosition();
                        }
                        itiRV.smoothScrollToPosition(position);
                        break;
                    case RecyclerView.SCROLL_STATE_DRAGGING:
//                        System.out.println("Scrolling now");
                        break;
                    case RecyclerView.SCROLL_STATE_SETTLING:
//                        System.out.println("Scroll Settling");
                        break;

                }
            }
        });
    }

    private void getLatestTravelKit(){
        travelKitListener = GeneralFunction.getFirestoreInstance()
                                .collection(TravelKit.URL_TRAVELKIT)
                                .whereEqualTo(TravelKit.IS_PUBLISH,true)
                                .whereEqualTo(TravelKit.STATUS,TravelKit.STATUS_APPROVED)
                                .orderBy(TravelKit.DATE_UPDATED, Query.Direction.DESCENDING)
                                .limit(1)
                                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                                    @Override
                                    public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
                                        if(GeneralFunction.hasError(parentActivity,e,TAG)){
                                            return;
                                        }

                                        if(snapshots != null && !snapshots.isEmpty()){
                                            ConstraintLayout travelKitCL = findViewById(R.id.travelKitCL);
                                            ImageView travelKitIV = findViewById(R.id.travelKitIV);
                                            TextView travelKitTV = findViewById(R.id.travelKitTV);
                                            TextView buyTravelKitTV = findViewById(R.id.buyTravelKitTV);

                                            for(DocumentSnapshot snapshot : snapshots){
                                                final TravelKit tempTravelKit = TravelKit.getTravelKit(parentActivity,snapshot);

                                                if(tempTravelKit == null){
                                                    travelKitCL.setVisibility(View.GONE);
                                                    return;
                                                }

                                                if(tempTravelKit.coverPhotoUrl != null){
                                                    GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(tempTravelKit.coverPhotoUrl),travelKitIV);
                                                }else{
                                                    travelKitCL.setVisibility(View.GONE);
                                                    return;
                                                }

                                                if(tempTravelKit.name != null){
//                                                    travelKitTV.setText(String.format(parentActivity.getString(R.string.landing_page_travel_kit),tempTravelKit.name));
                                                    //they dont want it to have latest travel kits yet so i just let them choose all travel kits and buy them
                                                    //uncomment the above code for real latest travel kit name
                                                    travelKitTV.setText(parentActivity.getString(R.string.booking_buy_travel_kit_toolbar));
                                                }else{
                                                    travelKitCL.setVisibility(View.GONE);
                                                    return;
                                                }

                                                buyTravelKitTV.setOnClickListener(new DebouncedOnClickListener(500) {
                                                    @Override
                                                    public void onDebouncedClick(View v) {
                                                        BookingHistory tempBookingHistory = null;
                                                        TravelKit travelKit = null;
//                                                        Fragment selectTravelKitFragment = SelectTravelKitFragment.newInstance(parentActivity,tempTravelKit,tempBookingHistory);
                                                        //uncomment the above code to allow user to directly go to the latest travel kit's detail fragment
                                                        Fragment selectTravelKitFragment = SelectTravelKitFragment.newInstance(parentActivity,travelKit,tempBookingHistory);
                                                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, selectTravelKitFragment).addToBackStack(SelectTravelKitFragment.TAG).commit();
                                                    }
                                                });

                                                travelKitCL.setVisibility(View.VISIBLE);

                                            }
                                        }
                                    }
                                });
    }

    private void getTopStay(){
        final RecyclerView hostRV = findViewById(R.id.hostRV);
        final ArrayList<Room> hostList = new ArrayList<>();
        final ArrayList<String> documentPushId = new ArrayList<>();
        final ImageView leftHostArrowIV = findViewById(R.id.leftHostArrowIV);
        final ImageView rightHostArrowIV = findViewById(R.id.rightHostArrowIV);
        final LinearLayoutManager linearLayoutManager = new SlowerLinearLayoutManager(parentActivity, LinearLayoutManager.HORIZONTAL,false);

        hostAccListener = GeneralFunction.getFirestoreInstance()
                .collection(Accommodation.URL_ACCOMMODATION)
                .whereEqualTo(Room.STATUS, Room.STATUS_APPROVED)
                .whereEqualTo(Room.IS_PUBLISH, true)
                .whereEqualTo(Room.IS_DRAFT, false)
                .orderBy(Itinerary.LIKE_COUNT, Query.Direction.DESCENDING)
                .orderBy(Room.DATE_UPDATED, Query.Direction.DESCENDING)
                .limit(5)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot snapshots, @javax.annotation.Nullable final FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w(TAG, "Listen failed.", e);
                            return;
                        }

                        hostList.clear();
                        documentPushId.clear();

                        if (snapshots != null && !snapshots.isEmpty()) {
                            for (QueryDocumentSnapshot snapshot : snapshots) {
                                Room tempHost = Room.snapshotToRoom(parentActivity, snapshot);
                                hostList.add(tempHost);
                                documentPushId.add(snapshot.getId());
                            }

                            if(hostAccAdapter != null && !snapshots.getMetadata().isFromCache() && hostAccListener != null){
                                hostAccListener.remove();
                            }
                        } else {
                            hostList.clear();
                            documentPushId.clear();
                        }

                        parentActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hostAccAdapter = new HostStayRecyclerGridAdapter(parentActivity,null, hostList,documentPushId,false,true);

                                hostRV.setNestedScrollingEnabled(false);
                                hostRV.setAdapter(hostAccAdapter);
                                hostRV.setLayoutManager(linearLayoutManager);
                            }
                        });
                    }
                });

        findViewById(R.id.hostViewAllTV).setOnClickListener(new DebouncedOnClickListener(500) {
            @Override
            public void onDebouncedClick(View v) {
                Fragment hostViewAllFragment = HostViewAllFragment.newInstance(parentActivity,null,null,null);
                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, hostViewAllFragment).addToBackStack(HostViewAllFragment.TAG).commit();
            }
        });

        leftHostArrowIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    hostRV.smoothScrollToPosition(linearLayoutManager.findFirstVisibleItemPosition() - 1);
                }catch (Exception e){
                    hostRV.smoothScrollToPosition(0);
                }

            }
        });

        rightHostArrowIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    hostRV.smoothScrollToPosition(linearLayoutManager.findLastVisibleItemPosition() + 1);
                }catch (Exception e){
                    hostRV.smoothScrollToPosition(hostAccAdapter.getItemCount() - 1);
                }
            }
        });

        hostRV.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                //if first item is not fully visible then show left arrow
                //if last item is not fully visible show right arrow
                if (linearLayoutManager.findFirstCompletelyVisibleItemPosition() == 0) {
                    leftHostArrowIV.setVisibility(View.GONE);
                } else {
                    leftHostArrowIV.setVisibility(View.VISIBLE);
                }

                if (hostAccAdapter != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == hostAccAdapter.getItemCount() - 1) {
                    rightHostArrowIV.setVisibility(View.GONE);
                } else {
                    rightHostArrowIV.setVisibility(View.VISIBLE);
                }

                if (dx > 0) {
                    isScrollingLeftHost = false;
//                    System.out.println("Scrolled Right");
                } else if (dx < 0) {
                    isScrollingLeftHost = true;
//                    System.out.println("Scrolled Left");
                } else {
                    isScrollingLeftHost = false;
                }
            }

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                switch (newState) {
                    case RecyclerView.SCROLL_STATE_IDLE:
                        int position = 0;
                        if(isScrollingLeftHost){
                            position = linearLayoutManager.findFirstVisibleItemPosition();
                        }else{
                            position = linearLayoutManager.findLastVisibleItemPosition();
                        }
                        hostRV.smoothScrollToPosition(position);
                        break;
                    case RecyclerView.SCROLL_STATE_DRAGGING:
//                        System.out.println("Scrolling now");
                        break;
                    case RecyclerView.SCROLL_STATE_SETTLING:
//                        System.out.println("Scroll Settling");
                        break;

                }
            }
        });
    }

    public void updateHostAdapter(){
        hostAccAdapter.notifyDataSetChanged();
    }

    //***********************************************************NOTIFICATION*************************************************************************
    public void getBookingNotification(){
        bookingNotiListener = GeneralFunction.getFirestoreInstance()
                .collection(BookingHistory.URL_BOOKING_HISTORY)
                .whereEqualTo(BookingHistory.USER_ID,parentActivity.uid)
                .whereEqualTo(BookingHistory.IS_READ_BY_GUEST, false)
                .whereGreaterThanOrEqualTo(BookingHistory.CHECKIN_DATE,GeneralFunction.getDefaultUtcCalendar().getTimeInMillis())
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
                        if(GeneralFunction.hasError(parentActivity,e,TAG)){
                            return;
                        }

                        if(snapshots != null && !snapshots.isEmpty()){
                            for(DocumentSnapshot snapshot : snapshots){
                                final BookingHistory bookingHistory = snapshot.toObject(BookingHistory.class);

                                if(GeneralFunction.isBookingHistoryValid(bookingHistory)
                                        && (TextUtils.equals(bookingHistory.actionType,BookingHistory.ACTION_TYPE_ACCEPTED)
                                            || TextUtils.equals(bookingHistory.actionType,BookingHistory.ACTION_TYPE_REJECTED))){
                                    bookingHistory.id = snapshot.getId();
                                    if(!alreadyShownBookingNotiList.contains(bookingHistory.id)){
                                        switch (bookingHistory.type){
                                            case BookingHistory.BOOK_TYPE_HOMESTAY:
                                                GeneralFunction.getFirestoreInstance()
                                                        .collection(Accommodation.URL_ACCOMMODATION)
                                                        .document(bookingHistory.targetId)
                                                        .get()
                                                        .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
//                                                            if(GeneralFunction.hasErrorDoc(parentActivity,task,TAG)){
                                                                if(!task.isSuccessful()){
                                                                    return;
                                                                }

                                                                Room room = task.getResult().toObject(Room.class);

                                                                if(room != null){
                                                                    room.id = task.getResult().getId();
                                                                    alreadyShownBookingNotiList.add(bookingHistory.id);
                                                                    GeneralFunction.openBookingNotificationDialog(parentActivity,room,bookingHistory,TAG);
                                                                }
                                                            }
                                                        });
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                });
    }

    //********************************************************GET TOP BLOGS*******************************************************************/
    private void getTopBlog(){
        final RecyclerView blogRV = findViewById(R.id.blogRV);
        final ArrayList<Blog> blogList = new ArrayList<>();
        final ImageView leftBlogArrowIV = findViewById(R.id.leftBlogArrowIV);
        final ImageView rightBlogArrowIV = findViewById(R.id.rightBlogArrowIV);
        final LinearLayoutManager linearLayoutManager = new SlowerLinearLayoutManager(parentActivity, LinearLayoutManager.HORIZONTAL,false);

        blogListener = GeneralFunction.getFirestoreInstance()
                .collection(Blog.URL_BLOG)
                .whereEqualTo(Blog.IS_PUBLISH, true)
                .whereEqualTo(Blog.IS_DRAFT, false)
                .orderBy(Blog.DATE_UPDATED, Query.Direction.DESCENDING)
                .limit(5)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot snapshots, @javax.annotation.Nullable final FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w(TAG, "Listen failed.", e);
                            return;
                        }

                        blogList.clear();

                        if (snapshots != null && !snapshots.isEmpty()) {
                            for (QueryDocumentSnapshot snapshot : snapshots) {
                                Blog tempBlog = Blog.snapshotToBlog(parentActivity,snapshot);
                                if(tempBlog != null){
                                    blogList.add(tempBlog);
                                }
                            }

                            if(blogAdapter != null && !snapshots.getMetadata().isFromCache() && blogListener != null){
                                blogListener.remove();
                            }
                        } else {
                            blogList.clear();
                        }

                        parentActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                blogAdapter = new BlogRecyclerGridAdapter(parentActivity,blogList, false,true);

                                blogRV.setNestedScrollingEnabled(false);
                                blogRV.setAdapter(blogAdapter);
                                blogRV.setLayoutManager(linearLayoutManager);
                            }
                        });

                        if(blogList.isEmpty()){
                            findViewById(R.id.blogCV).setVisibility(View.GONE);
                        }
                    }
                });

        findViewById(R.id.blogViewAllTV).setOnClickListener(new DebouncedOnClickListener(500) {
            @Override
            public void onDebouncedClick(View v) {
                Fragment blogViewAllFragment = BlogViewAllFragment.newInstance(parentActivity,null,null);
                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, blogViewAllFragment).addToBackStack(BlogViewAllFragment.TAG).commit();
            }
        });

        leftBlogArrowIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    blogRV.smoothScrollToPosition(linearLayoutManager.findFirstVisibleItemPosition() - 1);
                }catch (Exception e){
                    blogRV.smoothScrollToPosition(0);
                }
            }
        });

        rightBlogArrowIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    blogRV.smoothScrollToPosition(linearLayoutManager.findLastVisibleItemPosition() + 1);
                }catch (Exception e){
                    blogRV.smoothScrollToPosition(blogAdapter.getItemCount() - 1);
                }
            }
        });

        blogRV.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                //if first item is not fully visible then show left arrow
                //if last item is not fully visible show right arrow
                if (linearLayoutManager.findFirstCompletelyVisibleItemPosition() == 0) {
                    leftBlogArrowIV.setVisibility(View.GONE);
                } else {
                    leftBlogArrowIV.setVisibility(View.VISIBLE);
                }

                if (blogAdapter != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == blogAdapter.getItemCount() - 1) {
                    rightBlogArrowIV.setVisibility(View.GONE);
                } else {
                    rightBlogArrowIV.setVisibility(View.VISIBLE);
                }

                if (dx > 0) {
                    isScrollingLeftBlog = false;
//                    System.out.println("Scrolled Right");
                } else if (dx < 0) {
                    isScrollingLeftBlog = true;
//                    System.out.println("Scrolled Left");
                } else {
                    isScrollingLeftBlog = false;
                }
            }

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                switch (newState) {
                    case RecyclerView.SCROLL_STATE_IDLE:
                        int position = 0;
                        if(isScrollingLeftBlog){
                            position = linearLayoutManager.findFirstVisibleItemPosition();
                        }else{
                            position = linearLayoutManager.findLastVisibleItemPosition();
                        }
                        blogRV.smoothScrollToPosition(position);
                        break;
                    case RecyclerView.SCROLL_STATE_DRAGGING:
//                        System.out.println("Scrolling now");
                        break;
                    case RecyclerView.SCROLL_STATE_SETTLING:
//                        System.out.println("Scroll Settling");
                        break;

                }
            }
        });
    }

    public void hideBtmNav(){
        btmNavView.setVisibility(View.GONE);
    }

    public void showBtmNav(){
        btmNavView.setVisibility(View.VISIBLE);
    }

    public void showToolbar(){
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(parentActivity.getSupportActionBar() != null){
                    parentActivity.getSupportActionBar().show();
                    findViewById(R.id.appbar).setVisibility(View.VISIBLE);
                }
            }
        });
    }

    public void hideToolbar(){
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(parentActivity.getSupportActionBar() != null){
                    parentActivity.getSupportActionBar().hide();
                    findViewById(R.id.appbar).setVisibility(View.GONE);
                }
            }
        });

    }

    public void selectHighlights(){
        btmNavNewsfeedIV.setActivated(true);
        btmNavJobIV.setActivated(false);
        btmNavItenaryIV.setActivated(false);
        btmNavChatIV.setActivated(false);
        btmNavHotelIV.setActivated(false);
    }

    public void selectJob(){
        btmNavNewsfeedIV.setActivated(false);
        btmNavJobIV.setActivated(true);
        btmNavItenaryIV.setActivated(false);
        btmNavChatIV.setActivated(false);
        btmNavHotelIV.setActivated(false);
    }

    public void selectItinerary(){
        btmNavNewsfeedIV.setActivated(false);
        btmNavJobIV.setActivated(false);
        btmNavItenaryIV.setActivated(true);
        btmNavChatIV.setActivated(false);
        btmNavHotelIV.setActivated(false);
    }

    public void selectChat(){
        btmNavNewsfeedIV.setActivated(false);
        btmNavJobIV.setActivated(false);
        btmNavItenaryIV.setActivated(false);
        btmNavChatIV.setActivated(true);
        btmNavHotelIV.setActivated(false);
    }

    public void selectHotel(){
        btmNavNewsfeedIV.setActivated(false);
        btmNavJobIV.setActivated(false);
        btmNavItenaryIV.setActivated(false);
        btmNavChatIV.setActivated(false);
        btmNavHotelIV.setActivated(true);
    }

    private void setupBtmNavigationView(){
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.fragment_enter, R.anim.fragment_exit, R.anim.fragment_pop_enter, R.anim.fragment_pop_exit);

        btmNavNewsfeedIV.setActivated(true);
        btmNavNewsfeedIV.setOnClickListener(new DebouncedOnClickListener(500) {
            @Override
            public void onDebouncedClick(View v) {
                parentActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        selectHighlights();
                        goToHome();
                    }
                });
            }
        });

        btmNavJobIV.setOnClickListener(new DebouncedOnClickListener(500) {
            @Override
            public void onDebouncedClick(View v) {
                parentActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        selectJob();

                        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mainFL);
                        if(!(fragment instanceof PTJobSwipeFragment)){
                            Fragment ptjobSwipeFragment = PTJobSwipeFragment.newInstance(parentActivity,parentActivity.getString(R.string.app_name),null);
                            GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, ptjobSwipeFragment).addToBackStack(PTJobSwipeFragment.TAG).commit();
                            closeDrawer(true);
                        }else{
                            closeDrawer(false);
                        }
                    }
                });
            }
        });

        btmNavItenaryIV.setOnClickListener(new DebouncedOnClickListener(500) {
            @Override
            public void onDebouncedClick(View v) {
                parentActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        selectItinerary();

                        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mainFL);
                        if(!(fragment instanceof ItineraryMainFragment)){
                            if(GeneralFunction.isGuest()){
                                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_guest_restrict_usage),R.drawable.notice_bad,TAG);
                            }else{
                                Fragment itineraryMainFragment = ItineraryMainFragment.newInstance(parentActivity);
                                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, itineraryMainFragment).addToBackStack(ItineraryMainFragment.TAG).commit();
                            }
                            closeDrawer(true);
                        }else{
                            closeDrawer(false);
                        }
                    }
                });
            }
        });

        btmNavChatIV.setOnClickListener(new DebouncedOnClickListener(500) {
            @Override
            public void onDebouncedClick(View v) {
                parentActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        selectChat();

                        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mainFL);
                        if(!(fragment instanceof ChatContainerFragment)){
                            if(GeneralFunction.isGuest()){
                                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_guest_restrict_usage),R.drawable.notice_bad,TAG);
                            }else{
                                Fragment chatContainerFragment = ChatContainerFragment.newInstance(parentActivity,parentActivity.getString(R.string.chat_toolbar_title));
                                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, chatContainerFragment).addToBackStack(ChatContainerFragment.TAG).commit();
                            }
                            closeDrawer(true);
                        }else{
                            closeDrawer(false);
                        }
                    }
                });
            }
        });

        btmNavHotelIV.setOnClickListener(new DebouncedOnClickListener(500) {
            @Override
            public void onDebouncedClick(View v) {
                parentActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        selectHotel();

                        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mainFL);
                        if(!(fragment instanceof SearchHostRoomFragment)){
                            Fragment searchHostRoomFragment = SearchHostRoomFragment.newInstance(parentActivity);
                            GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, searchHostRoomFragment).addToBackStack(SearchHostRoomFragment.TAG).commit();
                            closeDrawer(true);
                        }else{
                            closeDrawer(false);
                        }
                    }
                });
            }
        });
    }

    private void navigationview(){

        drawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {

            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {
                View v = findViewById(R.id.hamburgerMenu);
                ObjectAnimator animator = ObjectAnimator
                        .ofFloat(v, "rotation", 180);
                animator.start();
                menu.getItem(1).setIcon(ContextCompat.getDrawable(parentActivity, R.drawable.baseline_close_white_18dp));
            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {
                View v = findViewById(R.id.hamburgerMenu);
                ObjectAnimator animator = ObjectAnimator
                        .ofFloat(v, "rotation",  -180);
                animator.start();
                menu.getItem(1).setIcon(ContextCompat.getDrawable(parentActivity, R.drawable.ic_hamburger));
            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
    }

    public void goToHome(){
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(getSupportFragmentManager() != null){
                    getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }
                closeDrawer(false);
                showBtmNav();
                showToolbar();
                selectHighlights();
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.app_name));
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        this.menu = menu;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.fragment_enter, R.anim.fragment_exit, R.anim.fragment_pop_enter, R.anim.fragment_pop_exit);

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mainFL);
        switch (item.getItemId()) {

            case R.id.nav_home:
                goToHome();
                break;
//            case R.id.nav_user_profile:
//                //this 1 dont check because i using same fragment to display other user details
////                if(!(fragment instanceof ProfileFragment)){
//                    Fragment profileFragment = ProfileFragment.newInstance(parentActivity,thisUser,true);
//                    GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, profileFragment).addToBackStack(ProfileFragment.TAG).commit();
//                    closeDrawer(true);
////                }else{
////                    closeDrawer(false);
////                }
//                break;
            case R.id.nav_payment_history:
                if(!(fragment instanceof PaymentHistoryMainFragment)){
                    PaymentHistoryMainFragment paymentHistoryMainFragment = PaymentHistoryMainFragment.newInstance(parentActivity);
                    ft.replace(R.id.mainFL, paymentHistoryMainFragment).addToBackStack(PaymentHistoryMainFragment.TAG);
                    ft.commit();
                    closeDrawer(true);
                }else{
                    closeDrawer(false);
                }
                break;
            case R.id.nav_update_user_contact:
                if(!(fragment instanceof UploadAccommodationPhotoFragement)){
                    UploadAccommodationPhotoFragement uploadAccommodationPhotoFragement = UploadAccommodationPhotoFragement.newInstance(parentActivity);
                    ft.replace(R.id.mainFL, uploadAccommodationPhotoFragement).addToBackStack(UploadAccommodationPhotoFragement.TAG);
                    ft.commit();
                    closeDrawer(true);
                }else{
                    closeDrawer(false);
                }
                break;

            case R.id.nav_update_user_payment:
                if(!(fragment instanceof UploadAccommodationPhotoFragement_2)){
                    UploadAccommodationPhotoFragement_2 uploadAccommodationPhotoFragement_2 = UploadAccommodationPhotoFragement_2.newInstance(parentActivity);
                    ft.replace(R.id.mainFL, uploadAccommodationPhotoFragement_2).addToBackStack(UploadAccommodationPhotoFragement_2.TAG);
                    ft.commit();
                    closeDrawer(true);
                }else{
                    closeDrawer(false);
                }
                break;

            case R.id.nav_user_reward:
//                if(!(fragment instanceof UploadRoomPhotoFragment)){
//                    UploadRoomPhotoFragment uploadRoomPhotoFragment = UploadRoomPhotoFragment.newInstance(parentActivity);
//                    ft.replace(R.id.mainFL, uploadRoomPhotoFragment).addToBackStack(UploadRoomPhotoFragment.TAG);
//                    ft.commit();
//                    closeDrawer(true);
//                }else{
//                    closeDrawer(false);
//                }
                break;

            case R.id.nav_invite_friend:
                if(!(fragment instanceof InviteFriendFragment)){
                    Fragment inviteFriendFragment = InviteFriendFragment.newInstance(parentActivity);
                    GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, inviteFriendFragment).addToBackStack(InviteFriendFragment.TAG).commit();
                    closeDrawer(true);
                }else{
                    closeDrawer(false);
                }
                break;

            case R.id.nav_friend_list:
                if(!(fragment instanceof FriendListFragment)){
                    Fragment friendListFragment = FriendListFragment.newInstance(parentActivity);
                    GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, friendListFragment).addToBackStack(FriendListFragment.TAG).commit();
                    closeDrawer(true);
                }else{
                    closeDrawer(false);
                }
                break;

            case R.id.nav_apply_employer:
                if(!(fragment instanceof ApplyEmployerFragment)){
                    Fragment applyEmployerFragment = ApplyEmployerFragment.newInstance(parentActivity);
                    GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, applyEmployerFragment).addToBackStack(ApplyEmployerFragment.TAG).commit();
                    closeDrawer(true);
                }else{
                    closeDrawer(false);
                }
                break;
            case R.id.nav_employer_dashboard:
                if(!(fragment instanceof EmployerDashboardContainerFragment)){
                    Fragment employerDashboardContainerFragment = EmployerDashboardContainerFragment.newInstance(parentActivity,parentActivity.getString(R.string.app_name));
                    GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, employerDashboardContainerFragment).addToBackStack(EmployerDashboardContainerFragment.TAG).commit();
                    closeDrawer(true);
                }else{
                    closeDrawer(false);
                }
                break;

            case R.id.nav_apply_host:
                if(!(fragment instanceof ApplyAsHostFragment)){
                    Fragment applyAsHostFragment = ApplyAsHostFragment.newInstance(parentActivity);
                    GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, applyAsHostFragment).addToBackStack(ApplyAsHostFragment.TAG).commit();
                    closeDrawer(true);
                }else{
                    closeDrawer(false);
                }
                break;
            case R.id.nav_host_dashboard:
                if(!(fragment instanceof HostAnAccommodationFragment)){
                    Fragment hostAnAccommodationFragment = HostAnAccommodationFragment.newInstance(parentActivity);
                    GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, hostAnAccommodationFragment).addToBackStack(HostAnAccommodationFragment.TAG).commit();
                    closeDrawer(true);
                }else{
                    closeDrawer(false);
                }
                break;

            case R.id.nav_apply_hotelier:

                break;
            case R.id.nav_hotelier_dashboard:

                break;
            case R.id.nav_apply_merchant:
                if(!(fragment instanceof ApplyAsMerchantFragment)){
                    Fragment applyAsMerchantFragment = ApplyAsMerchantFragment.newInstance(parentActivity,null);
                    GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, applyAsMerchantFragment).addToBackStack(ApplyAsMerchantFragment.TAG).commit();
                    closeDrawer(true);
                }else{
                    closeDrawer(false);
                }
                break;
            case R.id.nav_merchant_dashboard:
                if(!(fragment instanceof MerchantShopDashboard)){
                    Fragment merchantShopDashboard = MerchantShopDashboard.newInstance(parentActivity);
                    GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, merchantShopDashboard).addToBackStack(MerchantShopDashboard.TAG).commit();
                    closeDrawer(true);
                }else{
                    closeDrawer(false);
                }
                break;
            case R.id.nav_apply_tour_buddy:
                if(!(fragment instanceof TourBuddyRegisterFragment )){
                    Fragment tourBuddyRegisterFragment = TourBuddyRegisterFragment.newInstance(parentActivity);
                    GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, tourBuddyRegisterFragment).addToBackStack(TourBuddyRegisterFragment.TAG).commit();
                    closeDrawer(true);
                }else{
                    closeDrawer(false);
                }
                break;
            case R.id.nav_tour_buddy_profile:
                if(!(fragment instanceof TourBuddyProfileFragment)){
                    Fragment tourBuddyProfileFragment = TourBuddyProfileFragment.newInstance(parentActivity);
                    GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, tourBuddyProfileFragment).addToBackStack(TourBuddyProfileFragment.TAG).commit();
                    closeDrawer(true);
                }else{
                    closeDrawer(false);
                }
                break;
            case R.id.nav_setting:
                if(!(fragment instanceof ProfileSettingFragment)){
                    Fragment profileSettingFragment = ProfileSettingFragment.newInstance(parentActivity);
                    GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, profileSettingFragment).addToBackStack(ProfileSettingFragment.TAG).commit();
                    closeDrawer(true);
                }else{
                    closeDrawer(false);
                }
                break;
            case R.id.nav_about_us:
                if(!(fragment instanceof AboutUsFragment)){
                    Fragment aboutUsFragment = AboutUsFragment.newInstance(parentActivity);
                    GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, aboutUsFragment).addToBackStack(AboutUsFragment.TAG).commit();
                    closeDrawer(true);
                }else{
                    closeDrawer(false);
                }
                break;

//            case R.id.nav_change_password:
//                if(!(fragment instanceof AddBlogFragment)){
//                    Fragment addBlogFragment = AddBlogFragment.newInstance(parentActivity,null);
//                    GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, addBlogFragment).addToBackStack(AddBlogFragment.TAG).commit();
//                    closeDrawer(true);
//                }else{
//                    closeDrawer(false);
//                }
//                break;

            case R.id.nav_currency_list:
                if(!(fragment instanceof CurrencyChooserFragment)){
                    Fragment currencyChooserFragment = CurrencyChooserFragment.newInstance(parentActivity);
                    GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, currencyChooserFragment).addToBackStack(CurrencyChooserFragment.TAG).commit();
                    closeDrawer(true);
                }else{
                    closeDrawer(false);
                }
                break;

            case R.id.nav_terms:
                Intent browserTermIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://yupa.asia/terms.html"));
                startActivity(browserTermIntent);
                closeDrawer(true);
                break;

            case R.id.nav_privacy_policy:
                Intent browserPrivacyIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://yupa.asia/privacypolicy.html"));
                startActivity(browserPrivacyIntent);
                closeDrawer(true);
                break;

            case R.id.nav_logout:
                logout();
                break;
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.hamburgerMenu:
//                openDrawer();
                break;
        }
        return false;
    }

    public void controlProgressIndicator(boolean isMakeItOpen, Fragment parentFragment){
        if(parentActivity != null && progressIndicatorDialogFragment != null && !parentActivity.isFinishing() && GeneralFunction.isAppInForground(parentActivity.getApplicationContext())){
            if(isMakeItOpen){
                progressIndicatorDialogFragment.show(parentActivity.getSupportFragmentManager(),TAG);
            }else{
                progressIndicatorDialogFragment.dismiss();
            }
        }
    }

    public void initializeProgressIndicator(String message, boolean isAllowCancel, Fragment parentFragment){
        progressIndicatorDialogFragment = ProgressIndicatorDialogFragment.newInstance(message,isAllowCancel,parentActivity);
    }

    //https://stackoverflow.com/questions/37505375/firebase-storage-upload-progress-inaccurate
    //The progress is measured in chunks of 256KB. Since your file is smaller than that,
    // it fits in one chunk and progress thus jumps from 0% to 100% in one go.
    public void updateProgress(float progress, Fragment parentFragment){
        progressIndicatorDialogFragment.updateProgress(progress);
    }

    //normal loading dialog
    public void controlLoadingDialog(boolean isMakeItOpen, AppCompatActivity parentActivity){
        try{
            if(parentActivity != null && loadingProgressDialogFragment != null && !parentActivity.isFinishing() && GeneralFunction.isAppInForground(parentActivity.getApplicationContext())){
                if(isMakeItOpen){
                    loadingProgressDialogFragment.show(parentActivity.getSupportFragmentManager(),TAG);
                }else{
                    loadingProgressDialogFragment.dismiss();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void setLoadingDialogText(String newMessage){
        if(parentActivity != null && loadingProgressDialogFragment != null && !parentActivity.isFinishing() && GeneralFunction.isAppInForground(parentActivity.getApplicationContext())){
            loadingProgressDialogFragment.setLoadingMessage(newMessage);
        }
    }

    public void initializeLoadingDialog(String message, boolean isAllowCancel){
        loadingProgressDialogFragment = LoadingProgressDialogFragment.newInstance(message,isAllowCancel,parentActivity);
    }

    //for realtime events loading dialog
    public void controlLiveLoadingDialog(boolean isMakeItOpen, AppCompatActivity parentActivity){
        if(parentActivity != null && liveLoadingPDF != null && !parentActivity.isFinishing() && GeneralFunction.isAppInForground(parentActivity.getApplicationContext())){
            if(isMakeItOpen){
                liveLoadingPDF.show(parentActivity.getSupportFragmentManager(),TAG);
            }else{
                liveLoadingPDF.dismiss();
            }
        }
    }

    public void initializeLiveLoadingDialog(String message, boolean isAllowCancel){
        liveLoadingPDF = LoadingProgressDialogFragment.newInstance(message,isAllowCancel,parentActivity);
    }

    private void logout(){
        unsubscribeAllChat();
        GeneralFunction.unsubscribeInbox(parentActivity.uid,TAG);

        if(FirebaseAuth.getInstance() != null){
            FirebaseAuth.getInstance().signOut();
        }

        GeneralFunction.clearGlideCacheMemory(parentActivity);

        //clear shared preferences
        GeneralFunction.clearSharedPreferencesWithType(parentActivity, BookingHistory.BOOK_TYPE_HOMESTAY);
        GeneralFunction.clearSharedPreferencesWithType(parentActivity, Config.SP_MY_OWN_DEEP_LINK);
//        GeneralFunction.clearAllSharedPreferences(parentActivity);

        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        Toast.makeText(this,getString(R.string.txt_you_are_logout),Toast.LENGTH_SHORT).show();
        startActivity(intent);
        finish();
    }

    public void openDrawer(){
        if(!drawerLayout.isDrawerOpen(GravityCompat.END)){
            drawerLayout.openDrawer(GravityCompat.END);
        }
    }

    public void closeDrawer(Boolean needDelay){
        if(drawerLayout.isDrawerOpen(GravityCompat.END)){
            if(needDelay){
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        drawerLayout.closeDrawer(GravityCompat.END);
                    }
                }, 500);

            }else{
                drawerLayout.closeDrawer(GravityCompat.END);
            }
        }
    }

    public void setToolbarTitle(String title){
        if(toolbarTitleTV != null){
            toolbarTitleTV.setText(title);
        }
    }

    private void customtoolbar(){
        final Toolbar myToolbar = findViewById(R.id.my_toolbar);
        toolbarTitleTV = findViewById(R.id.toolbarTitleTV);

        setToolbarTitle(getResources().getString(R.string.app_name));
        myToolbar.inflateMenu(R.menu.menu);

                final View item = myToolbar.findViewById(R.id.hamburgerMenu);
                if (item != null) {
                    item.setOnClickListener(new DebouncedOnClickListener(800) {
                        @Override
                        public void onDebouncedClick(View v) {
                            if (drawerLayout.isDrawerOpen(GravityCompat.END)){
//                                ObjectAnimator animator = ObjectAnimator
//                                        .ofFloat(v, "rotation", v.getRotation() + 180);
//                                animator.start();
                                menu.getItem(1).setIcon(ContextCompat.getDrawable(parentActivity, R.drawable.ic_hamburger));

                                drawerLayout.closeDrawer(GravityCompat.END);
                            }
//                            else if (isHomeAsUp){
//                                onBackPressed();
//                            x
                            else {
//                                ObjectAnimator animator = ObjectAnimator
//                                        .ofFloat(v, "rotation", v.getRotation() + 180);
//                                animator.start();
                                menu.getItem(1).setIcon(ContextCompat.getDrawable(parentActivity, R.drawable.baseline_close_white_18dp));
                                openDrawer();
                            }
                        }
                    });
                }

                if(myToolbar.findViewById(R.id.inboxMenu) != null){

                    myToolbar.findViewById(R.id.inboxMenu)
                            .setOnClickListener(new DebouncedOnClickListener(500) {
                                @Override
                                public void onDebouncedClick(View v) {
                                    if(GeneralFunction.isGuest()) {
                                        GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_guest_restrict_usage),R.drawable.notice_bad,TAG);
                                    }else {
                                        Fragment inboxMainFragment = InboxMainFragment.newInstance(parentActivity);
                                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, inboxMainFragment).addToBackStack(InboxMainFragment.TAG).commit();
                                    }

                                }
                            });

                }
                if(isFirstTime()){
                    myToolbar.findViewById(R.id.inboxMenu).setClickable(false);
                    myToolbar.findViewById(R.id.hamburgerMenu).setClickable(false);

                    ViewTreeObserver vto2 = myToolbar.findViewById(R.id.inboxMenu).getViewTreeObserver();
                    vto2.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                        @Override
                        public void onGlobalLayout() {
                            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                                myToolbar.findViewById(R.id.inboxMenu).getViewTreeObserver().removeOnGlobalLayoutListener(this);

                            } else {
                                myToolbar.findViewById(R.id.inboxMenu).getViewTreeObserver().removeOnGlobalLayoutListener(this);
                            }
                            notificationHeight = myToolbar.findViewById(R.id.inboxMenu).getMeasuredHeight();
                        }
                    });

                }
                else{
                    myToolbar.findViewById(R.id.inboxMenu).setClickable(true);
                    myToolbar.findViewById(R.id.hamburgerMenu).setClickable(true);
                }


        setSupportActionBar(myToolbar);
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayShowTitleEnabled(false);
//            getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_back));
            getSupportActionBar().setHomeAsUpIndicator(ContextCompat.getDrawable(parentActivity,R.drawable.ic_back));
        }
    }

    /**************************************************THE TOP BACK BUTTON START****************************************************/
    @Override
    public void onBackStackChanged() {
        shouldDisplayHomeUp();
    }

    public void shouldDisplayHomeUp(){
        //Enable Up button only  if there are entries in the back stack
        boolean canback = getSupportFragmentManager().getBackStackEntryCount() > 0;
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(canback);
            getSupportActionBar().setDisplayShowHomeEnabled(canback);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        //This method is called when the up button is pressed. Just the pop back stack.
//        getSupportFragmentManager().popBackStack();
        parentActivity.onBackPressed();
        return true;
    }
    /**************************************************THE TOP BACK BUTTON END****************************************************/

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        if (fm != null) {
            if (drawerLayout.isDrawerOpen(GravityCompat.END)){
                closeDrawer(false);
                return;
            }
            else{
                //this isOpen is for spinners that is sliding up
                if(isOpen){
                    ConstraintLayout constraintLayout = findViewById(R.id.constraintLayoutSpinnerItem);
                    GeneralFunction.slideDownAnimationForListView(parentActivity, constraintLayout);
                    GeneralFunction.slideDownAnimationForView(parentActivity,findViewById(R.id.calendarView));
                    GeneralFunction.slideDownAnimationForView(parentActivity,findViewById(R.id.suggestionCL));
                   isOpen = false;
                   return;
                }
                else{
                    //because if 1 means that this last one will be disposed of
                    if (fm.getBackStackEntryCount() == 1) {
                        if(getSupportActionBar() != null){
                            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                            getSupportActionBar().setDisplayShowHomeEnabled(false);
                            parentActivity.setToolbarTitle(getResources().getString(R.string.app_name));
                            parentActivity.selectHighlights();
                            showBtmNav();
                        }

                        KeyboardUtilities.hideSoftKeyboard(parentActivity);
                        btmNavNewsfeedIV.setActivated(true);
                        btmNavJobIV.setActivated(false);
                        btmNavItenaryIV.setActivated(false);
                        btmNavChatIV.setActivated(false);
                        btmNavHotelIV.setActivated(false);
                    }else if(fm.getBackStackEntryCount() == 0){
                        if(!isOkToExit){
                            Toast.makeText(parentActivity,parentActivity.getString(R.string.exit_app_msg),Toast.LENGTH_SHORT).show();
                            exitHandler.postDelayed(exitRunnable, 1000);//1sec
                            isOkToExit = true;
                            return;
                        }
                    }
                }
            }
        }
        try{
            super.onBackPressed();
        }catch (Exception e){
            Log.e(TAG,e.getMessage());
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(googleApiClient != null){
            googleApiClient.disconnect();
        }

        if(exitHandler != null && exitRunnable != null){
            exitHandler.removeCallbacks(exitRunnable);
        }

        if(itiListener != null){
            itiListener.remove();
        }

        if(travelKitListener != null){
            travelKitListener.remove();
        }

        if(hostAccListener != null){
            hostAccListener.remove();
        }

        if(bookingNotiListener != null){
            bookingNotiListener.remove();
        }

        if(profileListener != null){
            profileListener.remove();
        }

        if(currencyRateListener != null){
            currencyRateListener.remove();
        }

        if(subscribeChatListener != null){
            subscribeChatListener.remove();
        }

        if(blogListener != null){
            blogListener.remove();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            if(parentActivity.uid != null){
                //manage user online presence
                GeneralFunction.getFirestoreInstance()
                        .collection(User.URL_USER)
                        .document(parentActivity.uid)
                        .update(User.IS_ONLINE,true);

                FirebaseDatabase.getInstance().getReference().child(User.IS_ONLINE).child(parentActivity.uid).setValue(true);

                //this will set the value of firebase to false when app is disconnected
                //since login activity there already put and has quite good performace so no need ba at here
//            FirebaseDatabase.getInstance().getReference().child(User.IS_ONLINE).child(parentActivity.uid).onDisconnect().setValue(false);
            }
        }catch (Exception e){
            Crashlytics.logException(e);
        }
    }

    //TODO THIS IS FOR OPENING GRAB AND UBER APP (OPEN GRAB IF IS SEA, OPEN UBER IF IS NOT) -- GRAB 1 WORKS but uber 1 not yet, it just opens the app only, maybe the my_location need to give gps? like 5.111,100.2222
    //    @Override
//    protected void onResume() {
//        super.onResume();
//        PackageManager pm = getPackageManager();
//        try {
//            pm.getPackageInfo("com.grabtaxi.passenger", PackageManager.GET_ACTIVITIES);
////            String uri = "uber.passenger://?action=setPickup&pickup=my_location";
//            String uri = "grab://open?sourceID=byYQTxMit2&sourceAppName=Fave&screenType=BOOKING&dropOffKeywords=Eternal Beauty & Wellness&dropOffAddress=2-1-30, The-One Terrace Plus, Tingkat Mahsuri 4, Bayan Baru 11950&dropOffLatitude=5.327969&dropOffLongitude=100.28515";
//            Intent intent = new Intent(Intent.ACTION_VIEW);
//            intent.setData(Uri.parse(uri));
//            startActivity(intent);
//        } catch (PackageManager.NameNotFoundException e) {
//            try {
//                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.grabtaxi.passenger")));
//            } catch (android.content.ActivityNotFoundException anfe) {
//                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.grabtaxi.passenger")));
//            }
//        }
//
//        //this 1 is for uber
//        //from here https://stackoverflow.com/questions/35913628/open-uber-app-from-my-app-android
////        PackageManager pm = getPackageManager();
////        try {
////            pm.getPackageInfo("com.ubercab", PackageManager.GET_ACTIVITIES);
////            String uri = "uber://?action=setPickup&pickup=my_location";
////            Intent intent = new Intent(Intent.ACTION_VIEW);
////            intent.setData(Uri.parse(uri));
////            startActivity(intent);
////        } catch (PackageManager.NameNotFoundException e) {
////            try {
////                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.ubercab")));
////            } catch (android.content.ActivityNotFoundException anfe) {
////                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.ubercab")));
////            }
////        }
//    }

    public Rect locateView(View view) {
        Rect loc = new Rect();
        int[] location = new int[2];
        if (view == null) {
            return loc;
        }
        view.getLocationOnScreen(location);

        loc.left = location[0];
        loc.top = location[1];
        loc.right = loc.left + view.getWidth();
        loc.bottom = loc.top + view.getHeight();
        return loc;
    }

    private boolean isFirstTime() {
        if (firstTime == null) {
            SharedPreferences mPreferences = this.getSharedPreferences("first_time", Context.MODE_PRIVATE);
            firstTime = mPreferences.getBoolean("firstTime_tutorial", true);
            if (firstTime) {
                SharedPreferences.Editor editor = mPreferences.edit();
                editor.putBoolean("firstTime_tutorial", false);
                editor.apply();
            }
        }
        return firstTime;
    }

    private void initiateHeaderView(){
        View headerView = navigationView.getHeaderView(0);
        ImageView userHeaderProfileIV = headerView.findViewById(R.id.userProfileIV);
        TextView userHeaderNameTV = headerView.findViewById(R.id.userNameTV);
        TextView userHeaderEmailTV = headerView.findViewById(R.id.userEmailTV);
        TextView userHeaderJoinDateTV = headerView.findViewById(R.id.userJoinDateTV);

        if(thisUser != null){
            if(thisUser.profilePicUrl != null && !thisUser.profilePicUrl.isEmpty()){
                GeneralFunction.GlideCircleImageSetting(parentActivity,Uri.parse(thisUser.profilePicUrl),userHeaderProfileIV);
            }
            else{
                GeneralFunction.GlideImageSettingOwnDrawable(parentActivity,R.drawable.profile_placeholder,userHeaderProfileIV);
            }
            if(thisUser.name != null){
                userHeaderNameTV.setText(thisUser.name);
            }
            if(thisUser.email != null){
                userHeaderEmailTV.setText(thisUser.email);
            }
            if(thisUser.dateCreated != null){
                userHeaderJoinDateTV.setText(String.format(parentActivity.getString(R.string.user_joined_in), GeneralFunction.getSdf().format(thisUser.dateCreated.toDate().getTime())));
            }
        }

        headerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment profileFragment = ProfileFragment.newInstance(parentActivity,thisUser,true);
                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, profileFragment).addToBackStack(ProfileFragment.TAG).commit();
                closeDrawer(true);
            }
        });
    }
}
