package com.vidatechft.yupa.profileFragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.HostStayRecyclerGridAdapter;
import com.vidatechft.yupa.adapter.ItineraryRecyclerGridAdapter;
import com.vidatechft.yupa.blog.BlogDashboardFragment;
import com.vidatechft.yupa.chatFragments.DirectChatFragment;
import com.vidatechft.yupa.classes.Accommodation;
import com.vidatechft.yupa.classes.Chat;
import com.vidatechft.yupa.classes.Favourite;
import com.vidatechft.yupa.classes.Friend;
import com.vidatechft.yupa.classes.Itinerary;
import com.vidatechft.yupa.classes.PlaceDetailsGoogleApi.PlaceAddress;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.classes.TourBuddy;
import com.vidatechft.yupa.classes.User;
import com.vidatechft.yupa.friends.FriendListFragment;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ProfileFragment extends Fragment implements View.OnClickListener{
    public static final String TAG = ProfileFragment.class.getName();
    private MainActivity parentActivity;
    private FriendListFragment parentFragment;
    private User user,findUserCriteria;
    private boolean isOwnProfile,fromTourBuddyPackages;
    private int position = -1;//for removing friends or pending friends

    private ListenerRegistration profileListener,friendsListener,chatListener;
    private Map<String ,Object> checkAllDates = new HashMap<>();
    private TextView nameTV,liveInTV,joinedTV,noTV;
    private ImageView profilePicIV;
    private TextView addFriendTV, chatTV;

    //LIKED ITINERARY LISTS
    private ListenerRegistration itiListener;
    private RecyclerView itiRV;
    private ConstraintLayout itiCL;
    private ItineraryRecyclerGridAdapter itiAdapter;
    private ArrayList<Itinerary> itiList = new ArrayList<>();
    private int itiCount = 0;

    //LIKED HOST ACCOMODATION LISTS
    private ListenerRegistration roomListener;
    private RecyclerView roomRV;
    private ConstraintLayout roomCL;
    private HostStayRecyclerGridAdapter roomAdapter;
    private ArrayList<Room> roomList = new ArrayList<>();
    private int roomCount = 0;

    //the booleans for determining the loading or empty message
    private Boolean isItiEmpty,isRoomEmpty;

    private Friend friendStatus;
    private Button bookTourBuddyBTN,disabledBookedBTN;
    private String queryUid;
    private String loadingTxt,emptyTxt;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static ProfileFragment newInstance(MainActivity parentActivity,User user, boolean isOwnProfile, User findUserCriteria, boolean fromTourBuddyPackages) {
        ProfileFragment fragment = new ProfileFragment();

        fragment.parentActivity = parentActivity;
        fragment.user = user;
        fragment.isOwnProfile = isOwnProfile;
        fragment.findUserCriteria = findUserCriteria;
        fragment.fromTourBuddyPackages = fromTourBuddyPackages;
        return fragment;
    }

    public static ProfileFragment newInstance(MainActivity parentActivity,User user, boolean isOwnProfile) {
        ProfileFragment fragment = new ProfileFragment();

        fragment.parentActivity = parentActivity;
        fragment.user = user;
        fragment.isOwnProfile = isOwnProfile;
        return fragment;
    }

    public static ProfileFragment newInstance(MainActivity parentActivity, FriendListFragment parentFragment, User user, boolean isOwnProfile, int position) {
        ProfileFragment fragment = new ProfileFragment();

        fragment.parentActivity = parentActivity;
        fragment.parentFragment = parentFragment;
        fragment.user = user;
        fragment.isOwnProfile = isOwnProfile;
        fragment.position = position;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_user_profile, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loadingTxt = parentActivity.getString(R.string.profile_loading_fav_any);
                emptyTxt = parentActivity.getString(R.string.profile_no_fav_any);

                nameTV = view.findViewById(R.id.nameTV);
                liveInTV = view.findViewById(R.id.liveInTV);
                joinedTV = view.findViewById(R.id.joinedTV);
                noTV = view.findViewById(R.id.noTV);
                profilePicIV = view.findViewById(R.id.profilePicIV);
                ConstraintLayout shareExpCL = view.findViewById(R.id.shareExpCL);
                itiCL = view.findViewById(R.id.itiCL);
                roomCL = view.findViewById(R.id.roomCL);
                LinearLayout friendBtnLL = view.findViewById(R.id.friendBtnLL);
                addFriendTV = view.findViewById(R.id.addFriendTV);
                chatTV = view.findViewById(R.id.chatTV);
                bookTourBuddyBTN =  view.findViewById(R.id.bookTourBuddyBTN);
                disabledBookedBTN =  view.findViewById(R.id.disabledBookedBTN);

                itiRV = view.findViewById(R.id.itiRV);
                roomRV = view.findViewById(R.id.roomRV);
                TextView editTV = view.findViewById(R.id.editTV);
                ImageView shareExpIV = view.findViewById(R.id.shareExpIV);

                editTV.setOnClickListener(ProfileFragment.this);
                shareExpCL.setOnClickListener(ProfileFragment.this);

                if(fromTourBuddyPackages)
                {
                    checkIfTheyHaveBeenBooked();
                }

                if(user != null && user.id != null){
                    queryUid = user.id;
                }else{
                    queryUid = parentActivity.uid;
                }

                if(isOwnProfile){
                    editTV.setVisibility(View.VISIBLE);
                    friendBtnLL.setVisibility(View.GONE);

                    GeneralFunction.GlideImageSettingOwnDrawable(parentActivity,R.drawable.shareexp_bg,shareExpIV);
                    shareExpCL.setVisibility(View.VISIBLE);
                }else{
                    editTV.setVisibility(View.GONE);
                    shareExpCL.setVisibility(View.GONE);
                    friendBtnLL.setVisibility(View.VISIBLE);

                    getIsFriends();
                }

                setUpGridAdapter();

                if(user == null){
                    getUserProfile();
                }else{
                    populateUserDetails();
                }

                checkIti(true);
                checkHostRoom(true);
                checkAllFavourites();
            }
        });

        return view;
    }

    private void checkIfTheyHaveBeenBooked()
    {
        Query query =  GeneralFunction.getFirestoreInstance().collection(TourBuddy.URL_TOUR_BUDDY_BOOK_CLASS_NAME)
                .whereEqualTo(TourBuddy.TOUR_BUDDY_CURRENTLY_SEARCHING_UID,parentActivity.uid)
                .whereEqualTo(TourBuddy.TOUR_BUDDY_UID,user.id);

        if(findUserCriteria != null)
        {
            checkAllDates = findUserCriteria.dateSearchRange;
            for(Map.Entry<String, Object> dateOneByOne: checkAllDates.entrySet())
            {
                String date = dateOneByOne.getKey();
                Boolean trueFalseValue = (Boolean) dateOneByOne.getValue();
                query = query.whereEqualTo(TourBuddy.TOUR_BUDDY_GET_ALL_AVAILABLE_DATES_INDEX+"."+date,trueFalseValue);
            }
        }

        query.addSnapshotListener(new EventListener<QuerySnapshot>()
        {

            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots , @Nullable FirebaseFirestoreException e)
            {
                if(queryDocumentSnapshots != null && !queryDocumentSnapshots.isEmpty())
                {
                    String approvalStatus = "";
                    boolean isExactRange = false;

                    ArrayList<TourBuddy> tourBuddyBookingList= new ArrayList<>();

                    for(DocumentSnapshot doc: queryDocumentSnapshots)
                    {
                        TourBuddy bookingTB = doc.toObject(TourBuddy.class);

                        if(bookingTB != null && bookingTB.status != null && bookingTB.customDateIndex != null && findUserCriteria != null && findUserCriteria.dateSearchRange != null){
                            tourBuddyBookingList.add(bookingTB);
                        }
                    }

                    for(TourBuddy bookingTB : tourBuddyBookingList){
                        //if any booking dates has been booked successfully within the search range, prompt them booked successful
                        if(TextUtils.equals(bookingTB.status,TourBuddy.TOUR_BUDDY_APPLY_STATUS_ACCEPTED)){
                            approvalStatus = TourBuddy.TOUR_BUDDY_APPLY_STATUS_ACCEPTED;
                            break;
                        }
                    }

                    if(approvalStatus.trim().isEmpty()){
                        for(TourBuddy bookingTB : tourBuddyBookingList){
                            ArrayList<String> dateFromDB = new ArrayList<>();
                            ArrayList<String> dateFromSearch = new ArrayList<>();
                            for (Object dateInMiliObject : bookingTB.customDateIndex.keySet()) {
                                dateFromDB.add(String.valueOf(dateInMiliObject));
                            }

                            for (Object dateInMiliObject : findUserCriteria.dateSearchRange.keySet()) {
                                dateFromSearch.add(String.valueOf(dateInMiliObject));
                            }

                            if(dateFromDB.size() == dateFromSearch.size()){
                                boolean isAllMatched = true;
                                for(String index : dateFromDB){
                                    if(!dateFromSearch.contains(index)){
                                        isAllMatched = false;
                                        break;
                                    }
                                }

                                if(isAllMatched){
                                    isExactRange = true;
                                }
                            }

                            if(TextUtils.equals(bookingTB.status,TourBuddy.TOUR_BUDDY_APPLY_STATUS_PENDING ) && isExactRange){
                                approvalStatus = TourBuddy.TOUR_BUDDY_APPLY_STATUS_PENDING;
                                break;
                            }else if(TextUtils.equals(bookingTB.status,TourBuddy.TOUR_BUDDY_APPLY_STATUS_REJECTED) && isExactRange){
                                approvalStatus = TourBuddy.TOUR_BUDDY_APPLY_STATUS_REJECTED;
                            }
                        }
                    }

                    switch (approvalStatus)
                    {
                        case TourBuddy.TOUR_BUDDY_APPLY_STATUS_ACCEPTED:
                            setVisibilityOfTourBuddyDisabled(true);
                            break;
                        case TourBuddy.TOUR_BUDDY_APPLY_STATUS_PENDING:
                            setVisibilityOfTourBuddyDisabled(false);
                            break;
                        case TourBuddy.TOUR_BUDDY_APPLY_STATUS_REJECTED:
                            setVisibilityOfTourBuddy();
                            break;
                        default:
                            setVisibilityOfTourBuddy();
                            break;
                    }
                }
                else
                {
                    setVisibilityOfTourBuddy();
                }
            }
        });
    }

    private void setVisibilityOfTourBuddyDisabled(boolean isAccepted)
    {
        disabledBookedBTN.setVisibility(View.VISIBLE);
        disabledBookedBTN.setEnabled(false);
        bookTourBuddyBTN.setVisibility(View.GONE);
        if(isAccepted)
        {
            disabledBookedBTN.setText("Booking Accepted");
        }
        else
        {
            disabledBookedBTN.setText("Booking Is Pending ...");
    }
    }
    private void setVisibilityOfTourBuddy()
    {
        disabledBookedBTN.setVisibility(View.GONE);
        bookTourBuddyBTN.setVisibility(View.VISIBLE);
        bookTourBuddyBTN.setOnClickListener(ProfileFragment.this);
    }
    /*************************************************************USER PROFILES AND FRIENDS START******************************************************/
    private void getIsFriends(){
        friendsListener = GeneralFunction.getFirestoreInstance()
                .collection(Friend.URL_FRIENDS)
                .whereEqualTo(parentActivity.uid,true)
                .whereEqualTo(user.id, true)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable final QuerySnapshot snapshots, @javax.annotation.Nullable final FirebaseFirestoreException e) {
                        if (e != null) {
                            parentActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.w(TAG, "Listen failed.", e);
                                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.itinerary_plan_err_loading),R.drawable.notice_bad,TAG);
                                }
                            });
                            return;
                        }

                        parentActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                chatTV.setText(parentActivity.getString(R.string.start_chat));

                                if (snapshots != null && !snapshots.isEmpty()) {

                                    boolean isDeleted = false;
                                    for(DocumentChange dc : snapshots.getDocumentChanges()){
                                        switch (dc.getType()){
                                            case REMOVED:
                                                isDeleted = true;
                                                break;
                                        }
                                    }

                                    if(isDeleted){
                                        addFriendTV.setText(parentActivity.getString(R.string.add_friend));
                                        addFriendTV.setVisibility(View.VISIBLE);
                                        chatTV.setVisibility(View.GONE);
                                        addFriendTV.setOnClickListener(ProfileFragment.this);
                                        return;
                                    }

                                    for(QueryDocumentSnapshot snapshot : snapshots){
                                        friendStatus = snapshot.toObject(Friend.class);
                                        friendStatus.id = snapshot.getId();

                                        if(friendStatus.status == null){
                                            addFriendTV.setText(parentActivity.getString(R.string.add_friend));
                                            addFriendTV.setVisibility(View.VISIBLE);
                                            chatTV.setVisibility(View.GONE);
                                            break;
                                        }

                                        switch (friendStatus.status){
                                            case Friend.STATUS_ACCEPTED:
                                                addFriendTV.setText(parentActivity.getString(R.string.unfriend));
                                                addFriendTV.setVisibility(View.VISIBLE);
                                                chatTV.setVisibility(View.VISIBLE);
                                                break;
                                            case Friend.STATUS_PENDING:
                                                if(friendStatus.targetId != null){
                                                    if(TextUtils.equals(friendStatus.targetId,parentActivity.uid)){
                                                        //this user gets to accept or decline
                                                        addFriendTV.setText(parentActivity.getString(R.string.candid_accept));
                                                        chatTV.setText(parentActivity.getString(R.string.candid_decline));
                                                        addFriendTV.setVisibility(View.VISIBLE);
                                                        chatTV.setVisibility(View.VISIBLE);
                                                    }else{
                                                        //this user is the one sending that request
                                                        addFriendTV.setText(parentActivity.getString(R.string.pending_friends));
                                                        addFriendTV.setVisibility(View.VISIBLE);
                                                        chatTV.setVisibility(View.GONE);
                                                    }
                                                }
                                                break;
                                            case Friend.STATUS_BLOCKED:
                                                if(friendStatus.blockedId != null && TextUtils.equals(friendStatus.blockedId,parentActivity.uid)){
                                                    //this user kena blocked by that user
                                                    addFriendTV.setText(parentActivity.getString(R.string.friend_blocked));
                                                    addFriendTV.setVisibility(View.VISIBLE);
                                                    chatTV.setVisibility(View.GONE);
                                                }else{
                                                    //means that this/current user blocked that user
                                                    addFriendTV.setText(parentActivity.getString(R.string.friend_unblock));
                                                    addFriendTV.setVisibility(View.VISIBLE);
                                                    chatTV.setVisibility(View.GONE);
                                                }
                                                break;
                                            case Friend.STATUS_REJECTED:
                                                //if rejected let both of them add friend again
                                                addFriendTV.setText(parentActivity.getString(R.string.add_friend));
                                                addFriendTV.setVisibility(View.VISIBLE);
                                                chatTV.setVisibility(View.GONE);
                                                break;
                                            default:
                                                addFriendTV.setText(parentActivity.getString(R.string.add_friend));
                                                addFriendTV.setVisibility(View.VISIBLE);
                                                chatTV.setVisibility(View.GONE);
                                                break;
                                        }
                                        break;
                                    }
//                                  if(!snapshots.getMetadata().isFromCache() && friendsListener != null){
//                                      friendsListener.remove();
//                                  }
                                }else{
                                    addFriendTV.setText(parentActivity.getString(R.string.add_friend));
                                    addFriendTV.setVisibility(View.VISIBLE);
                                    chatTV.setVisibility(View.GONE);
                                }

                                addFriendTV.setOnClickListener(ProfileFragment.this);
                                chatTV.setOnClickListener(ProfileFragment.this);
                            }
                        });
                    }
                });
    }

    private void getUserProfile(){
        profileListener = GeneralFunction.getFirestoreInstance()
                            .collection(User.URL_USER)
                            .document(queryUid)
                            .addSnapshotListener(new EventListener<DocumentSnapshot>() {
                                @Override
                                public void onEvent(@javax.annotation.Nullable DocumentSnapshot documentSnapshot, @javax.annotation.Nullable final FirebaseFirestoreException e) {
                                    if (e != null) {
                                        parentActivity.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Log.w(TAG, "Listen failed.", e);
                                                GeneralFunction.openMessageNotificationDialog(parentActivity,e.getMessage(),R.drawable.notice_bad,TAG);
                                            }
                                        });
                                        return;
                                    }

                                    if(documentSnapshot != null && documentSnapshot.exists()){
                                        user = new User(parentActivity,documentSnapshot);
                                        populateUserDetails();
                                    }

                                }
                            });
    }

    private void populateUserDetails(){
        if(user != null){
            if(user.profilePicUrl != null && !TextUtils.isEmpty(user.profilePicUrl)){
                GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(user.profilePicUrl),profilePicIV);
            }
            else{
                profilePicIV.setBackgroundResource(R.drawable.ic_profile_placeholder);
            }

            if(user.name != null){
                nameTV.setText(user.name);
            }

            if(user.country != null){
                if(user.state != null && !user.state.isEmpty()){
                    liveInTV.setText(String.format(parentActivity.getString(R.string.user_live_in_both), user.state, user.country));
                }else{
                    liveInTV.setText(String.format(parentActivity.getString(R.string.user_live_in_country), user.country));
                }
            }

            if(user.dateCreated != null){
                joinedTV.setText(String.format(parentActivity.getString(R.string.user_joined_in), GeneralFunction.getSdf().format(user.dateCreated.toDate().getTime())));
            }

        }
    }

    //*************************************************************USER PROFILES AND FRIENDS END******************************************************/

    /**********************************FUNCTIONS THAT CHECKS WHETHER LIST IS EMPTY OR NOT AND DETERMINING THE noTV's message START******************************************************/
    private void checkIti(final boolean needGet){
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(itiList.size() <= 0){
                    itiCL.setVisibility(View.GONE);
                    if(needGet){
                        getIti();
                    }else{
                        //only here set true because if no need get data again, it means that this method is called after finished getting data from the database, so it really is empty
                        isItiEmpty = true;
                    }
                }else{
                    itiCL.setVisibility(View.VISIBLE);
                    isItiEmpty = false;
                }
            }
        });
    }

    private void checkHostRoom(final boolean needGet){
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(roomList.size() <= 0){
                    roomCL.setVisibility(View.GONE);
                    if(needGet){
                        getRoom();
                    }else{
                        isRoomEmpty = true;
                    }
                }else{
                    roomCL.setVisibility(View.VISIBLE);
                    isRoomEmpty = false;
                }
            }
        });
    }

    private void checkAllFavourites(){
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(isItiEmpty == null && isRoomEmpty == null){
                    noTV.setText(loadingTxt);
                    noTV.setVisibility(View.VISIBLE);
                }else if( (isItiEmpty != null && !isItiEmpty) || (isRoomEmpty != null && !isRoomEmpty) ){
                    noTV.setVisibility(View.GONE);
                }else{
                    noTV.setText(emptyTxt);
                    noTV.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    //**********************************FUNCTIONS THAT CHECKS WHETHER LIST IS EMPTY OR NOT AND DETERMINING THE noTV's message END******************************************************/

    /*************************************************************GET ALL FAVOURITES DATA START******************************************************/
    private void getIti(){
        itiListener = GeneralFunction.getFirestoreInstance()
                .collection(Favourite.URL_FAVOURITE)
                .whereEqualTo(Favourite.USER_ID,queryUid)
                .whereEqualTo(Favourite.TYPE,Favourite.FAV_TYPE_ITI)
                .whereEqualTo(Favourite.IS_LIKED, true)
                .orderBy(Favourite.DATE_UPDATED, Query.Direction.DESCENDING)
                .limit(2)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot snapshots, @javax.annotation.Nullable final FirebaseFirestoreException e) {
                        if (e != null) {
                            parentActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.w(TAG, "Listen failed.", e);
                                    GeneralFunction.openMessageNotificationDialog(parentActivity,e.getMessage(),R.drawable.notice_bad,TAG);
                                }
                            });
                            return;
                        }
                        itiList.clear();

                        if (snapshots != null && !snapshots.isEmpty()) {
                            final int totalFavItiCount = snapshots.getDocuments().size();
                            for (QueryDocumentSnapshot snapshot : snapshots) {
                                Favourite tempFavourite = snapshot.toObject(Favourite.class);

                                if(tempFavourite.targetId != null){
                                    GeneralFunction.getFirestoreInstance()
                                            .collection(Itinerary.URL_ITINERARY)
                                            .document(tempFavourite.targetId)
                                            .get()
                                            .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                                @Override
                                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                    itiCount++;

                                                    if(task.isSuccessful()){
                                                        itiList.add(new Itinerary(parentActivity,task.getResult()));
                                                    }

                                                    if(itiCount == totalFavItiCount){
                                                        itiCount = 0;
                                                        checkIti(false);
                                                        checkAllFavourites();
                                                        itiAdapter.notifyDataSetChanged();
                                                    }
                                                }
                                            });
                                }
                            }

                            if(itiAdapter != null && !snapshots.getMetadata().isFromCache() && itiListener != null){
                                itiListener.remove();
                            }

                        } else {
                            itiList.clear();
                            itiAdapter.notifyDataSetChanged();

                            checkIti(false);
                            checkAllFavourites();
                        }
                    }
                });
    }

    private void getRoom(){
        roomListener = GeneralFunction.getFirestoreInstance()
                .collection(Favourite.URL_FAVOURITE)
                .whereEqualTo(Favourite.USER_ID,queryUid)
                .whereEqualTo(Favourite.TYPE,Favourite.FAV_TYPE_ACC)
                .whereEqualTo(Favourite.IS_LIKED, true)
                .orderBy(Favourite.DATE_UPDATED, Query.Direction.DESCENDING)
                .limit(2)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot snapshots, @javax.annotation.Nullable final FirebaseFirestoreException e) {
                        if (e != null) {
                            parentActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.w(TAG, "Listen failed.", e);
                                    GeneralFunction.openMessageNotificationDialog(parentActivity,e.getMessage(),R.drawable.notice_bad,TAG);
                                }
                            });
                            return;
                        }
                        roomList.clear();

                        if (snapshots != null && !snapshots.isEmpty()) {
                            final int totalFavRoomCount = snapshots.getDocuments().size();
                            for (QueryDocumentSnapshot snapshot : snapshots) {
                                Favourite tempFavourite = snapshot.toObject(Favourite.class);

                                if(tempFavourite.targetId != null){
                                    GeneralFunction.getFirestoreInstance()
                                            .collection(Accommodation.URL_ACCOMMODATION)
                                            .document(tempFavourite.targetId)
                                            .get()
                                            .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                                @Override
                                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                    roomCount++;

                                                    if(task.isSuccessful()){
                                                        roomList.add(Room.snapshotToRoom(parentActivity,task.getResult()));
                                                    }

                                                    if(roomCount == totalFavRoomCount){
                                                        roomCount = 0;
                                                        checkHostRoom(false);
                                                        checkAllFavourites();
                                                        roomAdapter.notifyDataSetChanged();
                                                    }
                                                }
                                            });
                                }
                            }

                            if(roomAdapter != null && !snapshots.getMetadata().isFromCache() && roomListener != null){
                                roomListener.remove();
                            }

                        } else {
                            roomList.clear();
                            roomAdapter.notifyDataSetChanged();

                            checkHostRoom(false);
                            checkAllFavourites();
                        }
                    }
                });
    }

    /*************************************************************GET ALL FAVOURITES DATA END******************************************************/

    private void setUpGridAdapter(){

        itiAdapter = new ItineraryRecyclerGridAdapter(parentActivity,this, itiList,false);

        itiRV.setNestedScrollingEnabled(false);
        itiRV.setAdapter(itiAdapter);
        itiRV.setLayoutManager(new GridLayoutManager(parentActivity, 2));

        roomAdapter = new HostStayRecyclerGridAdapter(parentActivity,this, roomList,null,false);

        roomRV.setNestedScrollingEnabled(false);
        roomRV.setAdapter(roomAdapter);
        roomRV.setLayoutManager(new GridLayoutManager(parentActivity, 2));
    }

    private void addNewFriend(){
        HashMap<String,Object> tempFriendMap = GeneralFunction.addNewFriend(parentActivity,friendStatus,user,TAG);

        if(tempFriendMap == null){
            return;
        }

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                addFriendTV.setText(parentActivity.getString(R.string.pending_friends));
                addFriendTV.setVisibility(View.VISIBLE);
                chatTV.setVisibility(View.GONE);
            }
        });
        addFriendTV.setOnClickListener(null);
        friendStatus = Friend.toClass(tempFriendMap,parentActivity,(String)tempFriendMap.get(Friend.TARGET_ID));
    }

    private void unfriendUser(){
        if(!GeneralFunction.unfriendUser(parentActivity,friendStatus,TAG)){
            return;
        }

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                addFriendTV.setText(parentActivity.getString(R.string.add_friend));
                addFriendTV.requestLayout();//because wrap content wont be refresh if i put new words in it, need to refresh it manually
                addFriendTV.setVisibility(View.VISIBLE);
                chatTV.setVisibility(View.GONE);
            }
        });

        if(parentFragment != null && position != -1){
            parentFragment.removeFriend(position);
        }

        friendStatus = null;
    }

    private void blockOrUnblockUser(String status){
        HashMap<String,Object> tempFriendMap = GeneralFunction.blockOrUnblockUser(parentActivity,friendStatus,user,status,TAG);

        if(tempFriendMap == null){
            return;
        }

        switch (status){
            case Friend.STATUS_BLOCKED:
                parentActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        addFriendTV.setText(parentActivity.getString(R.string.friend_unblock));
                        addFriendTV.setVisibility(View.VISIBLE);
                        chatTV.setVisibility(View.VISIBLE);
                    }
                });

                friendStatus = Friend.toClass(tempFriendMap,parentActivity,(String)tempFriendMap.get(Friend.TARGET_ID));

                if(parentFragment != null && position != -1){
                    parentFragment.removeFriend(position);
                }
                break;
            case Friend.STATUS_UNBLOCKED:
                parentActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        addFriendTV.setText(parentActivity.getString(R.string.add_friend));
                        addFriendTV.setVisibility(View.VISIBLE);
                        chatTV.setVisibility(View.GONE);
                    }
                });

                friendStatus = Friend.toClass(tempFriendMap,parentActivity,(String)tempFriendMap.get(Friend.TARGET_ID));
                break;
        }
    }

    private void acceptOrDeclineUser(String status){
        HashMap<String,Object> tempFriendMap = GeneralFunction.acceptOrDeclineUser(parentActivity,friendStatus,status,TAG);

        if(tempFriendMap == null){
            return;
        }

        //can't return 2 values from function so i write again, and because lazy to write return array list or other long long methods
        ArrayList<String> friendList = new ArrayList<>();
        friendList.add(friendStatus.requestorId);
        friendList.add(friendStatus.targetId);

        switch (status){
            case Friend.STATUS_REJECTED:
                parentActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        addFriendTV.setText(parentActivity.getString(R.string.add_friend));
                        addFriendTV.setVisibility(View.VISIBLE);
                        chatTV.setVisibility(View.GONE);
                    }
                });

                friendStatus = null;
                break;
            case Friend.STATUS_ACCEPTED:
                parentActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        addFriendTV.setText(parentActivity.getString(R.string.unfriend));
                        chatTV.setText(parentActivity.getString(R.string.start_chat));
                        addFriendTV.setVisibility(View.VISIBLE);
                        chatTV.setVisibility(View.VISIBLE);
                    }
                });

                friendStatus = Friend.toClass(tempFriendMap,parentActivity,friendList);
                break;
        }

        if(parentFragment != null && position != -1){
            parentFragment.removePending(position);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(profileListener != null){
                    profileListener.remove();
                }

                if(friendsListener != null){
                    friendsListener.remove();
                }

                if(chatListener != null){
                    chatListener.remove();
                }

                if(itiListener != null){
                    itiListener.remove();
                }

                if(roomListener != null){
                    roomListener.remove();
                }
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        parentActivity.showBtmNav();
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.hideBtmNav();
        parentActivity.setToolbarTitle(parentActivity.getString(R.string.nav_user_profile));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.shareExpCL:
                if(GeneralFunction.isGuest()){
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_guest_restrict_usage),R.drawable.notice_bad,TAG);
                    return;
                }
                Fragment blogDashboardFragment = BlogDashboardFragment.newInstance(parentActivity);
                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, blogDashboardFragment).addToBackStack(BlogDashboardFragment.TAG).commit();
                break;
            case R.id.editTV:
                if(GeneralFunction.isGuest()){
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_guest_restrict_usage),R.drawable.notice_bad,TAG);
                    return;
                }
                //todo well i should pass the user object to the edit profile fragment but no time to do it yet so until next time~
                //hacky way to make user profile updated when go to edit profile fragment
                if(profileListener != null){
                    profileListener.remove();
                }
                getUserProfile();
                Fragment editProfileFragment = EditProfileFragment.newInstance(parentActivity,user);
                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, editProfileFragment).addToBackStack(EditProfileFragment.TAG).commit();
                break;
            case R.id.addFriendTV:
                if(GeneralFunction.isGuest()){
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_guest_restrict_usage),R.drawable.notice_bad,TAG);
                    return;
                }
                if(friendStatus == null || friendStatus.status == null){
                    if(friendStatus != null && friendStatus.targetId != null){
                        if(TextUtils.equals(friendStatus.targetId,parentActivity.uid)){
                            //this user gets to accept or decline
                            //accept user
                            addNewFriend();
                        }else{
                            //this user is the one sending that request
                            //nothing to do here sinc already sent request
                        }
                    }else{
                        //add user
                        addNewFriend();
                    }
                    break;
                }

                switch (friendStatus.status){
                    case Friend.STATUS_ACCEPTED:
                        unfriendUser();
                        break;
                    case Friend.STATUS_PENDING:
                        acceptOrDeclineUser(Friend.STATUS_ACCEPTED);
                        break;
                    case Friend.STATUS_BLOCKED:
                        if(friendStatus.blockedId != null && TextUtils.equals(friendStatus.blockedId,parentActivity.uid)){
                            //this user kena blocked by that user
                            //cant do anything
                            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.friend_you_have_been_blocked),R.drawable.notice_bad,TAG);
                        }else{
                            //means that this/current user blocked that user
                            //unblock the user
                            blockOrUnblockUser(Friend.STATUS_UNBLOCKED);
                        }
                        break;
                    case Friend.STATUS_REJECTED:
                        addNewFriend();
                        break;
                    default:
                        addNewFriend();
                        break;
                }
                break;
            case R.id.chatTV:
                if(GeneralFunction.isGuest()){
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_guest_restrict_usage),R.drawable.notice_bad,TAG);
                    return;
                }
                if(TextUtils.equals(chatTV.getText().toString(),parentActivity.getString(R.string.candid_decline))){
                    //decline user
                    acceptOrDeclineUser(Friend.STATUS_REJECTED);
                }else{
                    //start messaging
                    parentActivity.initializeLiveLoadingDialog(parentActivity.getString(R.string.chat_connecting_chatroom),false);
                    parentActivity.controlLiveLoadingDialog(true,parentActivity);

                    chatListener = GeneralFunction.getFirestoreInstance()
                            .collection(Chat.URL_CHAT)
                            .whereEqualTo(Chat.TYPE,Chat.TYPE_PERSONAL_MSG)
                            .whereEqualTo(parentActivity.uid,true)
                            .whereEqualTo(user.id,true)
                            .addSnapshotListener(new EventListener<QuerySnapshot>() {
                                @Override
                                public void onEvent(@javax.annotation.Nullable QuerySnapshot snapshots, @javax.annotation.Nullable final FirebaseFirestoreException e) {
                                    chatListener.remove();
                                    if (e != null) {
                                        parentActivity.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                parentActivity.controlLiveLoadingDialog(false,parentActivity);
                                                Log.w(TAG, "Listen failed.", e);
                                                GeneralFunction.openMessageNotificationDialog(parentActivity,e.getMessage(),R.drawable.notice_bad,TAG);
                                            }
                                        });
                                        return;
                                    }

                                    if (snapshots != null && !snapshots.isEmpty() && !snapshots.getDocuments().isEmpty()) {
                                        parentActivity.controlLiveLoadingDialog(false,parentActivity);
                                        Fragment directChatFragment = DirectChatFragment.newInstance(
                                                parentActivity,
                                                parentActivity.getString(R.string.nav_user_profile),
                                                user,
                                                snapshots.getDocuments().get(0).getId());
                                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, directChatFragment).addToBackStack(DirectChatFragment.TAG).commit();
                                    }else{
                                        HashMap<String,Object> tempChatMap = Chat.getNewPrivateChatMap(parentActivity.uid,user.id);
                                        final DocumentReference chatRef = GeneralFunction.getFirestoreInstance()
                                                .collection(Chat.URL_CHAT)
                                                .document();

                                        chatRef.set(tempChatMap)
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        parentActivity.controlLiveLoadingDialog(false,parentActivity);
                                                        if(task.isSuccessful()){
                                                            Fragment directChatFragment = DirectChatFragment.newInstance(
                                                                    parentActivity,
                                                                    parentActivity.getString(R.string.nav_user_profile),
                                                                    user,
                                                                    chatRef.getId());
                                                            GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, directChatFragment).addToBackStack(DirectChatFragment.TAG).commit();
                                                        }else{
                                                            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.chat_err_connecting_chatroom),R.drawable.notice_bad,TAG);
                                                        }
                                                    }
                                                });
                                    }
                                }
                            });
                }
                break;
            case R.id.bookTourBuddyBTN:
                bookThisTourBuddy();
                break;
        }
    }

    private void bookThisTourBuddy()
    {
        if(findUserCriteria != null && user != null)
        {
            Map<String , Object> bookThisTourBuddy = new HashMap<>();
            GeoPoint geoPoint ;

            bookThisTourBuddy.put(TourBuddy.TOUR_BUDDY_GET_ALL_AVAILABLE_DATES_INDEX,findUserCriteria.dateSearchRange);
            bookThisTourBuddy.put(TourBuddy.TOUR_BUDDY_DATE_CREATED,FieldValue.serverTimestamp());
            bookThisTourBuddy.put(TourBuddy.TOUR_BUDDY_DATE_UPDATED,FieldValue.serverTimestamp());

            if(findUserCriteria.placeAddress != null)
            {
                geoPoint = new GeoPoint(findUserCriteria.placeAddress.lat,findUserCriteria.placeAddress.lng);
                bookThisTourBuddy.put(TourBuddy.TOUR_BUDDY_GET_CURRENT_LOCATION,geoPoint);
            }

            bookThisTourBuddy.put(TourBuddy.TOUR_BUDDY_PLACE_ADDRESS,PlaceAddress.getHashmap(findUserCriteria.placeAddress,findUserCriteria.placeID));
            bookThisTourBuddy.put(TourBuddy.TOUR_BUDDY_PLACE_INDEX,PlaceAddress.getPlaceIndex(findUserCriteria.placeAddress));
            bookThisTourBuddy.put(TourBuddy.TOUR_BUDDY_APPLY_STATUS,TourBuddy.TOUR_BUDDY_APPLY_STATUS_PENDING);
            bookThisTourBuddy.put(TourBuddy.TOUR_BUDDY_UID,user.id);
            bookThisTourBuddy.put(TourBuddy.TOUR_BUDDY_CURRENTLY_SEARCHING_UID,parentActivity.uid);

            GeneralFunction.getFirestoreInstance().collection(TourBuddy.URL_TOUR_BUDDY_BOOK_CLASS_NAME).document().set(bookThisTourBuddy)
                .addOnSuccessListener(new OnSuccessListener<Void>()
                {
                    @Override
                    public void onSuccess(Void aVoid)
                    {
                        GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.tour_buddy_book_successful),R.drawable.notice_good,TAG);
                    }
                })
                .addOnFailureListener(new OnFailureListener()
                {
                    @Override
                    public void onFailure(@NonNull Exception e)
                    {
                        GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.tour_buddy_book_failed),R.drawable.notice_bad,TAG);
                        Log.e(TAG,e.toString());
                    }
                });
        }
    }
}
