package com.vidatechft.yupa.profileFragments;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.MapsInitializer;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.CheckboxRecyclerGridAdapter;
import com.vidatechft.yupa.adapter.PlaceAutoCompleteAdapter;
import com.vidatechft.yupa.classes.PlaceDetailsGoogleApi.PlaceAddress;
import com.vidatechft.yupa.classes.User;
import com.vidatechft.yupa.retrofitNetwork.UnifyPlaceAddress;
import com.vidatechft.yupa.tourBuddyFragment.TourBuddySearchFragment;
import com.vidatechft.yupa.utilities.GeneralFunction;
import com.vidatechft.yupa.utilities.KeyboardUtilities;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

public class SearchNearbyUserFragment extends Fragment implements View.OnClickListener,DatePickerDialog.OnDateSetListener
{
    public static final String TAG = SearchNearbyUserFragment.class.getName();
    private MainActivity parentActivity;
    private TextView minAgeTV, maxAgeTV, femaleTV, maleTV, returnTV, departureTV,searchTourBuddyTV,nextTV;
    private SeekBar ageSB;
    private ImageView searchCloseRightIV;
    private AutoCompleteTextView getPlaceATV;
    private RecyclerView hobbyRV;
    private ArrayList<CheckBox> getHobby = new ArrayList<>();
    private CheckboxRecyclerGridAdapter hobbyAdapter;
    private boolean isFemale,isMale,isMaxAgeSelected;
    private int minAge, maxAge;
    private GoogleApiClient googleApiClient;
    private Calendar calendar = new GregorianCalendar();
    private Long departDate,returnDate;
    private Boolean isDepart = false;
    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");
    private String placeID;

    public static SearchNearbyUserFragment newInstance(MainActivity parentActivity) {
        SearchNearbyUserFragment fragment = new SearchNearbyUserFragment();

        fragment.parentActivity = parentActivity;

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState)
    {
        final View view = inflater.inflate(R.layout.fragment_search_nearby_user , container , false);

        //todo : findViewById`s
        findView(view);

        //todo : Generating Checkbox for hobby
        checkBoxGenerator(hobbyRV);

        //todo : Generate suggested places
        pickPlace(getPlaceATV);

        //todo : Setup Age
        pickAge();

        //todo : Setup OnClickListeners
        setOnClickListeners();

        //todo : Setup depart and arrival date
        setupDateRange();

        //todo : Setup Age function no 2
        pickAge2();

        //todo : Check Gender
        pickGender();

        return view;
    }

    private void setupDateRange()
    {
        if(departDate != null)
        {
            calendar.setTimeInMillis(departDate);
            departureTV.setText(format.format(calendar.getTime()));
        }
        if(returnDate != null)
        {
            calendar.setTimeInMillis(returnDate);
            returnTV.setText(format.format(calendar.getTime()));
        }
    }

    private void pickPlace(final AutoCompleteTextView getPlaceATV)
    {
        googleApiClient = new GoogleApiClient.Builder(parentActivity)
                .addApi(Places.GEO_DATA_API)
                .addConnectionCallbacks(parentActivity)
                .addOnConnectionFailedListener(parentActivity)
                .build();
        MapsInitializer.initialize(parentActivity);
        googleApiClient.connect();

        final PlaceAutoCompleteAdapter placeAutoCompleteAdapter = new PlaceAutoCompleteAdapter(parentActivity, R.layout.adapter_place_auto_complete,
                googleApiClient, null, null);

        getPlaceATV.setAdapter(placeAutoCompleteAdapter);

        getPlaceATV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                final PlaceAutoCompleteAdapter.PlaceAutocomplete item = placeAutoCompleteAdapter.getItem(position);
                if(item == null)
                {
                    Log.e(TAG, "Place Autocomplete is null");
                    return;
                }
                placeID = item.placeId.toString();
            }
        });

        getPlaceATV.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().isEmpty()){
                    searchCloseRightIV.setVisibility(View.GONE);
                }else{
                    searchCloseRightIV.setVisibility(View.VISIBLE);
                }
            }
        });
        searchCloseRightIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPlaceATV.setText("");
            }
        });
    }

    @Override
    public void onResume()
    {
        super.onResume();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.chat_toolbar_nearby_companion));
                parentActivity.selectChat();
            }
        });
    }

    private void pickAge2()
    {
        if(minAge == 0 && maxAge == 0)
        {
            ageSB.setProgress(18 - User.SEARCH_MIN_AGE);
            minAge = 18;
            maxAge = 25;
            minAgeTV.setText(String.valueOf(minAge));
            maxAgeTV.setText(String.valueOf(maxAge));
        }
        else
            {
            minAgeTV.setText(String.valueOf(minAge));
            maxAgeTV.setText(String.valueOf(maxAge));
        }
    }

    private void pickAge()
    {
        if(isMaxAgeSelected)
        {
            minAgeTV.setActivated(false);
            maxAgeTV.setActivated(true);
        }else{
            minAgeTV.setActivated(true);
            maxAgeTV.setActivated(false);
        }
        setupSeekbar();
    }

    private void setupSeekbar()
    {
        ageSB.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progress = progress + User.SEARCH_MIN_AGE;
                String progressStr = String.valueOf(progress);
                if(minAgeTV.isActivated()){
                    minAgeTV.setText(progressStr);
                    minAge = progress;

                    if(minAge >= maxAge){
                        maxAge = minAge;
                        maxAgeTV.setText(progressStr);
                    }

                }else if(maxAgeTV.isActivated()){
                    maxAgeTV.setText(progressStr);
                    maxAge = progress;

                    if(maxAge <= minAge){
                        minAge = maxAge;
                        minAgeTV.setText(progressStr);
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void pickGender()
    {
        if(isFemale)
        {
            femaleTV.setActivated(true);
        }
        else if (isMale)
        {
            maleTV.setActivated(true);
        }
    }

    private void setOnClickListeners()
    {
        minAgeTV.setOnClickListener(SearchNearbyUserFragment.this);
        maxAgeTV.setOnClickListener(SearchNearbyUserFragment.this);
        femaleTV.setOnClickListener(SearchNearbyUserFragment.this);
        maleTV.setOnClickListener(SearchNearbyUserFragment.this);
        departureTV.setOnClickListener(SearchNearbyUserFragment.this);
        returnTV.setOnClickListener(SearchNearbyUserFragment.this);
        datePickerDialog = GeneralFunction.getDatePickerDialog(parentActivity,SearchNearbyUserFragment.this);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        searchTourBuddyTV.setOnClickListener(SearchNearbyUserFragment.this);
        nextTV.setOnClickListener(SearchNearbyUserFragment.this);
    }

    private void checkBoxGenerator(RecyclerView hobbyRV)
    {
        if(getHobby.size() <= 0)
        {
            final String[] hobbyList = parentActivity.getResources().getStringArray(R.array.editProfileHobby);
            for(String hobby : hobbyList)
            {
                CheckBox hobbyCheckBox = (CheckBox) CheckBox.inflate(parentActivity,R.layout.custom_checkbox,null);
                hobbyCheckBox.setText(hobby.trim());
                getHobby.add(hobbyCheckBox);
            }
        }

        hobbyAdapter = new CheckboxRecyclerGridAdapter(parentActivity,getHobby);

        hobbyRV.setNestedScrollingEnabled(false);
        hobbyRV.setAdapter(hobbyAdapter);
        hobbyRV.setLayoutManager(new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL));
    }

    private void findView(View view)
    {
        minAgeTV = view.findViewById(R.id.minAgeTV);
        maxAgeTV = view.findViewById(R.id.maxAgeTV);
        ageSB = view.findViewById(R.id.ageSB);
        femaleTV = view.findViewById(R.id.femaleTV);
        maleTV = view.findViewById(R.id.maleTV);
        hobbyRV = view.findViewById(R.id.hobbyRV);
        departureTV = view.findViewById(R.id.departureTV);
        returnTV = view.findViewById(R.id.returnTV);
        getPlaceATV = view.findViewById(R.id.getPlaceATV);
        searchCloseRightIV = view.findViewById(R.id.searchCloseRightIV);
        nextTV = view.findViewById(R.id.nextTV);
        searchTourBuddyTV = view.findViewById(R.id.searchTourBuddyTV);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.femaleTV:
                if(femaleTV.isActivated())
                {
                    femaleTV.setActivated(false);
                }
                else
                {
                    femaleTV.setActivated(true);
                    maleTV.setActivated(false);
                }
                break;
            case R.id.maleTV:
                if(maleTV.isActivated())
                {
                    maleTV.setActivated(false);
                }
                else
                {
                    maleTV.setActivated(true);
                    femaleTV.setActivated(false);
                }
                break;
            case R.id.minAgeTV:
                minAgeTV.setActivated(true);
                maxAgeTV.setActivated(false);
                isMaxAgeSelected = false;
                break;
            case R.id.maxAgeTV:
                minAgeTV.setActivated(false);
                maxAgeTV.setActivated(true);
                isMaxAgeSelected = true;
                break;
            case R.id.returnTV:
                isDepart = false;
                datePickerDialog.show();
                break;
            case R.id.departureTV:
                isDepart = true;
                datePickerDialog.show();
                break;

            case R.id.nextTV: //search nearby fragment
                KeyboardUtilities.hideSoftKeyboard(parentActivity);
                getSearchData(false);
                break;

            case R.id.searchTourBuddyTV: //search tour buddy fragment
                KeyboardUtilities.hideSoftKeyboard(parentActivity);
                getSearchData(true);
                break;

        }
    }

    private void getSearchData(final boolean isSearchingForTourBuddy)
    {
        final User user = new User();

        user.ageRangeMin = minAge;
        user.ageRangeMax = maxAge;

        if(isSearchingForTourBuddy)
        {
            if (placeID!= "" && ! TextUtils.equals(placeID , ""))
            {
                new UnifyPlaceAddress(parentActivity ,placeID , new UnifyPlaceAddress.OnUnifyComplete()
                {
                    @Override
                    public void onUnifySuccess(PlaceAddress placeAddress)
                    {
                        user.placeID = placeID;
                        user.placeAddress = placeAddress;

                        if (returnDate != null)
                        {
                            user.returnDate = returnDate;
                        }

                        if (departDate != null)
                        {
                            user.departDate = departDate;
                            setData(user , isSearchingForTourBuddy);
                        }
                        else
                        {
                            GeneralFunction.openMessageNotificationDialog(parentActivity , "Please enter your departure date" , R.drawable.notice_bad , TAG);
                        }
                    }

                    @Override
                    public void onUnifyFailed(Exception e)
                    {
                        GeneralFunction.openMessageNotificationDialog(parentActivity , "Please enter your destination" , R.drawable.notice_bad , TAG);
                    }
                });
            }
            else
            {
                GeneralFunction.openMessageNotificationDialog(parentActivity , "Please enter your destination" , R.drawable.notice_bad , TAG);
            }
        }
        else
        {
            setData(user,isSearchingForTourBuddy);
        }
    }

    private void setData(User user , boolean isSearchingForTourBuddy)
    {
        //todo : get Gender
        if(femaleTV.isActivated())
        {
            user.gender = User.GENDER_FEMALE;
            isFemale = true;
            isMale = false;
        }
        else if(maleTV.isActivated())
        {
            user.gender = User.GENDER_MALE;
            isMale = true;
            isFemale = false;
        }
        else
        {
            user.gender = null;
            isFemale = false;
            isMale = false;
        }

        //todo : get Hobby
        for(CheckBox hobbyScopeCB : hobbyAdapter.getCheckBoxes())
        {
            if(hobbyScopeCB != null && hobbyScopeCB.isChecked())
            {
                String hob = hobbyScopeCB.getText().toString().trim();
                user.hobbyScope.add(hob);
            }
        }

        user.dateSearchRange =  storeDepartUntilReturnDateMap(user);

        if(isSearchingForTourBuddy)
        {
            Fragment searchTourBuddyFragment = TourBuddySearchFragment.newInstance(parentActivity,user);
            GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, searchTourBuddyFragment).addToBackStack(TourBuddySearchFragment.TAG).commit();
        }
        else
        {
            Fragment connectNearbyUserMapFragment = ConnectNearbyUserMapFragment.newInstance(parentActivity,user,false);
            GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, connectNearbyUserMapFragment).addToBackStack(ConnectNearbyUserMapFragment.TAG).commit();
        }

    }

    private Map<String,Object> storeDepartUntilReturnDateMap(User user)
    {
        Map<String,Object> dateSearchRange = new HashMap<>();

        Calendar calendarDepart = GeneralFunction.getDefaultUtcCalendar();
        Calendar calendarReturn = GeneralFunction.getDefaultUtcCalendar();

        if(user.departDate != null)
        {
            calendarDepart.setTimeInMillis(user.departDate);

            if(user.returnDate == null)
            {
                calendarReturn.setTimeInMillis(user.departDate);
            }
            else
            {
                calendarReturn.setTimeInMillis(user.returnDate);
            }
        }

        if(calendarDepart.getTimeInMillis() != calendarReturn.getTimeInMillis())
        {
            while(calendarDepart.getTimeInMillis() <= calendarReturn.getTimeInMillis())
            {
                dateSearchRange.put(String.valueOf(calendarDepart.getTimeInMillis()),true);
                calendarDepart.add(Calendar.DATE,1);
            }
        }
        else
        {
            dateSearchRange.put(String.valueOf(calendarDepart.getTimeInMillis()),true);
        }

        return dateSearchRange;
    }

    @Override
    public void onDateSet(DatePicker view , int year , int month , int dayOfMonth)
    {
        calendar.set(year,month,dayOfMonth);

        long getDate = GeneralFunction.getDefaultUtcCalendar(year,month,dayOfMonth).getTime().getTime();

        if(isDepart)
        {
            departDate = getDate;
            departureTV.setText(format.format(calendar.getTime()));
            datePickerDialog.getDatePicker().setMinDate(departDate);

            if(returnDate != null)
            {
                if(returnDate <= departDate)
                {
                    returnDate = departDate;
                    returnTV.setText(format.format(calendar.getTime()));
                }

            }
        }
        else
        {
            returnDate = getDate;
            returnTV.setText(format.format(calendar.getTime()));
            if(departDate != null)
            {
                if(returnDate <= departDate)
                {
                    departDate = returnDate;
                    departureTV.setText(format.format(calendar.getTime()));
                }
            }
        }
    }
}
