package com.vidatechft.yupa.profileFragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.Job;
import com.vidatechft.yupa.classes.User;
import com.vidatechft.yupa.dialogFragments.ConnectNearbyUserClusterListDialogFragment;
import com.vidatechft.yupa.profileSetting.ProfileSettingFragment;
import com.vidatechft.yupa.utilities.Config;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
public class ConnectNearbyUserMapFragment extends Fragment implements OnMapReadyCallback, View.OnClickListener {
    public static final String TAG = ConnectNearbyUserMapFragment.class.getName();
    private MainActivity parentActivity;
    private User searchedUser;
    private boolean needShowPartTime;

    private GoogleMap googleMap;
    private ClusterManager<User> mClusterManager;
    private TextView noTV;
    private EditText searchET;
    private ImageView clearIV;

    private FusedLocationProviderClient locationProviderClient;
    private LocationCallback locationCallback;
    private HashMap<String,LatLng> boundingBox;
    private int permissionCheckCount = 0;

    //this is the marker's view
    private User selectedUser;
    private ArrayList<User> users = new ArrayList<>();
    private ArrayList<User> fullUsers = new ArrayList<>();

    private String loadingTxt,emptyTxt,errorTxt;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static ConnectNearbyUserMapFragment newInstance(MainActivity parentActivity, User searchedUser, boolean needShowPartTime) {
        ConnectNearbyUserMapFragment fragment = new ConnectNearbyUserMapFragment();

        fragment.parentActivity = parentActivity;
        fragment.searchedUser = searchedUser;
        fragment.needShowPartTime = needShowPartTime;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_connect_nearby_user_map, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.showBtmNav();

                loadingTxt = parentActivity.getString(R.string.chat_nearby_companion_loading);
                emptyTxt = parentActivity.getString(R.string.chat_nearby_companion_empty);
                errorTxt = parentActivity.getString(R.string.chat_nearby_companion_error);

                if(googleMap == null){
                    SupportMapFragment mapFragment = (SupportMapFragment) ConnectNearbyUserMapFragment.this.getChildFragmentManager()
                            .findFragmentById(R.id.nearbyUserGoogleMap);
                    mapFragment.getMapAsync(ConnectNearbyUserMapFragment.this);
                }

                clearIV = view.findViewById(R.id.clearIV);
                noTV = view.findViewById(R.id.noTV);
                searchET = view.findViewById(R.id.searchET);

                clearIV.setOnClickListener(ConnectNearbyUserMapFragment.this);
                searchET.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if(count > 0){
                            clearIV.setVisibility(View.VISIBLE);
                        }else{
                            clearIV.setVisibility(View.GONE);
                        }

                        searchedUser.searchNameOrContactOrEmail = s.toString();

                        addUserMarkers();
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });
            }
        });

        return view;
    }

    private void searchNearbyUser(final LatLng latLng){
        if(locationProviderClient != null && locationCallback != null){
            locationProviderClient.removeLocationUpdates(locationCallback);
        }

        if(fullUsers != null && !fullUsers.isEmpty()){
            noTV.setVisibility(View.GONE);
//            populateMap();
            return;
        }else{
            noTV.setText(loadingTxt);
            noTV.setVisibility(View.VISIBLE);
        }

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                boundingBox = GeneralFunction.getBoundingBoxCoor(latLng,User.JOB_RADIUS);
                GeoPoint btmLeft = new GeoPoint(boundingBox.get(Config.BOUNDING_BOX_BTM_LEFT).latitude,boundingBox.get(Config.BOUNDING_BOX_BTM_LEFT).longitude);
                GeoPoint topRight = new GeoPoint(boundingBox.get(Config.BOUNDING_BOX_TOP_RIGHT).latitude,boundingBox.get(Config.BOUNDING_BOX_TOP_RIGHT).longitude);

                Query ref;
                ref = GeneralFunction.getFirestoreInstance()
                        .collection(User.URL_USER)
                        .whereGreaterThan(User.GPS,btmLeft)
                        .whereLessThan(User.GPS,topRight);

                if(needShowPartTime){
                    ref = ref.whereEqualTo(User.IS_JOB_SEEKER,true);
                }

                if(searchedUser != null){
                    if(searchedUser.gender != null){
                        ref = ref.whereEqualTo(User.GENDER,searchedUser.gender);
                    }
                }

                ref.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if(task.getException() != null){
                            noTV.setText(errorTxt);
                            noTV.setVisibility(View.VISIBLE);
                            return;
                        }

                        if(task.getResult() != null && !task.getResult().isEmpty()){
                            fullUsers.clear();

                            for (DocumentSnapshot snapshot : task.getResult()) {
                                fullUsers.add(new User(parentActivity,snapshot));
                            }

                            populateMap();
                        }else{
                            noTV.setText(emptyTxt);
                            noTV.setVisibility(View.VISIBLE);
                        }
                    }
                });
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        setupInfoWindow();
    }

    private void setupInfoWindow(){
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                final ViewGroup infoWindow = (ViewGroup)getLayoutInflater().inflate(R.layout.info_window_google_map_user_marker, null);
                final ImageView profileIV = infoWindow.findViewById(R.id.profileIV);
                final TextView nameTV = infoWindow.findViewById(R.id.nameTV);
                final TextView liveInTV = infoWindow.findViewById(R.id.liveInTV);

                ConnectNearbyUserMapFragment.this.googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                    @Override
                    public View getInfoWindow(final Marker marker) {
                        return null;
                    }

                    @Override
                    public View getInfoContents(final Marker marker) {
                        parentActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if(selectedUser != null){
                                    if(selectedUser.name != null){
                                        String name;

                                        //if too many characters, dont show all
                                        if(selectedUser.name.length() >= 47){
                                            name = selectedUser.name.substring(0,46) + "...";
                                        }else{
                                            name = selectedUser.name;
                                        }
                                        nameTV.setText(name);
                                    }else{
                                        nameTV.setText(parentActivity.getString(R.string.no_name));
                                    }

                                    if(selectedUser.profilePicUrl != null){
//                                        GeneralFunction.GlideImageSetting(parentActivity,Uri.parse(selectedUser.profilePicUrl),profileIV);
                                        GeneralFunction.GlideImageSettingShrinkImage(parentActivity,Uri.parse(selectedUser.profilePicUrl),profileIV,marker);
                                    }

                                    if(needShowPartTime){
                                        if(selectedUser.jobScope != null && !selectedUser.jobScope.isEmpty()){
                                            liveInTV.setText(GeneralFunction.formatList(selectedUser.jobScope,true));
                                        }
                                    }else{
                                        if(selectedUser.country != null){
                                            String liveIn;
                                            if(selectedUser.state != null && !selectedUser.state.isEmpty()){
                                                liveIn = selectedUser.state + ", " + selectedUser.country;
                                            }else{
                                                liveIn = selectedUser.country;
                                            }
                                            liveInTV.setText(liveIn);
                                        }
                                    }
                                }
                            }
                        });

                        if(selectedUser != null){
                            return infoWindow;
                        }else{
                            return null;
                        }
                    }
                });

                //if want to make a button clickable only instead of the whole window refer to the link below
                //from here https://stackoverflow.com/questions/14123243/google-maps-android-api-v2-interactive-infowindow-like-in-original-android-go/15040761#15040761
                ConnectNearbyUserMapFragment.this.googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                    @Override
                    public void onInfoWindowClick(Marker marker) {
                        if(selectedUser != null){
                            Fragment profileFragment = ProfileFragment.newInstance(parentActivity,selectedUser,false);
                            GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, profileFragment).addToBackStack(TAG).commit();
                        }
                    }
                });

                ConnectNearbyUserMapFragment.this.googleMap.setOnInfoWindowCloseListener(new GoogleMap.OnInfoWindowCloseListener() {
                    @Override
                    public void onInfoWindowClose(Marker marker) {
                        selectedUser = null;
                    }
                });
            }
        });
    }

    private void populateMap(){
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                googleMap.clear();
                mClusterManager = new ClusterManager<>(parentActivity, googleMap);
                mClusterManager.setRenderer(new CustomMapRenderer<>(parentActivity, googleMap, mClusterManager));
                googleMap.setOnCameraIdleListener(mClusterManager);
                googleMap.setOnMarkerClickListener(mClusterManager);
                googleMap.getUiSettings().setZoomControlsEnabled(true);

//                googleMap.addMarker(new MarkerOptions()
//                        .title(parentActivity.getString(R.string.you_are_here))
//                        .position(boundingBox.get(Config.BOUNDING_BOX_CENTER))
//                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_gps)))
//                        .setZIndex(0.1f);

                if(!fullUsers.isEmpty()){
                    noTV.setVisibility(View.GONE);

                    addUserMarkers();

                    CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                            boundingBox.get(Config.BOUNDING_BOX_CENTER), Job.JOB_ZOOM_IN_LEVEL);//the bigger the number, the deeper it goes
                    googleMap.animateCamera(location);

                    googleMap.addCircle(GeneralFunction.getMapCircle(boundingBox.get(Config.BOUNDING_BOX_CENTER),Job.JOB_RADIUS,Config.MAP_COLOUR_GREEN));
                }else{
                    noTV.setText(emptyTxt);
                    noTV.setVisibility(View.VISIBLE);
                }

                mClusterManager.setOnClusterClickListener(new ClusterManager.OnClusterClickListener<User>() {
                    @Override
                    public boolean onClusterClick(Cluster<User> cluster) {

                        if(googleMap.getCameraPosition().zoom >= 20){
                            if(parentActivity != null && !parentActivity.isFinishing() && GeneralFunction.isAppInForground(parentActivity.getApplicationContext())){
                                FragmentManager fm = parentActivity.getSupportFragmentManager();
                                ConnectNearbyUserClusterListDialogFragment connectNearbyUserClusterListDialogFragment = ConnectNearbyUserClusterListDialogFragment.newInstance(parentActivity,new ArrayList<>(cluster.getItems()),needShowPartTime);
                                connectNearbyUserClusterListDialogFragment.show(fm, TAG);
                            }
                        }else{
                            //zoom in the cluster to uncluster it
                            //from here https://stackoverflow.com/questions/25395357/android-how-to-uncluster-on-single-tap-on-a-cluster-marker-maps-v2
                            LatLngBounds.Builder builder = LatLngBounds.builder();
                            for (ClusterItem item : cluster.getItems()) {
                                builder.include(item.getPosition());
                            }
                            final LatLngBounds bounds = builder.build();
                            googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
                        }

                        return true;
                    }
                });

                mClusterManager.setOnClusterItemClickListener(new ClusterManager.OnClusterItemClickListener<User>() {
                    @Override
                    public boolean onClusterItemClick(User user) {
                        selectedUser = user;
                        return false;
                    }
                });
            }
        });
    }

    private void addUserMarkers(){
        users.clear();
        if(mClusterManager == null){
            return;
        }

        mClusterManager.clearItems();

        //this is for filtering all the users in that area with age and hobby as required by the previous search user fragment
        if(searchedUser != null){
            for(User tempUser : fullUsers){
                boolean okToAddToList = true;
                if(tempUser != null){

                    if(TextUtils.equals(tempUser.id,parentActivity.uid)){
                        okToAddToList = false;
                    }

                    if(okToAddToList && searchedUser.ageRangeMin > 0 && searchedUser.ageRangeMax > 0){
                        Calendar currentDate = GeneralFunction.getDefaultUtcCalendar();
                        int age = currentDate.get(Calendar.YEAR) - tempUser.dobYear;
                        if(age < searchedUser.ageRangeMin || age > searchedUser.ageRangeMax){
                            okToAddToList = false;
                        }
                    }

                    if(okToAddToList && searchedUser.hobbyScope != null){
                        for(String searchedHobby : searchedUser.hobbyScope){
                            if(tempUser.hobbyScope != null && !tempUser.hobbyScope.contains(searchedHobby)){
                                okToAddToList = false;
                            }
                        }
                    }

                    if(okToAddToList && searchedUser.searchNameOrContactOrEmail != null){
                        if(needShowPartTime){
                            if(!hasSearchedString(tempUser.name) && !hasSearchedString(tempUser.email) && !hasSearchedString(tempUser.contactNo) && !hasSearchedJobscope(tempUser.jobScope)){
                                okToAddToList = false;
                            }
                        }else{
                            if(!hasSearchedString(tempUser.name) && !hasSearchedString(tempUser.email) && !hasSearchedString(tempUser.contactNo)){
                                okToAddToList = false;
                            }
                        }
                    }
                }else{
                    okToAddToList = false;
                }

                if(okToAddToList){
                    users.add(tempUser);
                }
            }
        }
        mClusterManager.addItems(users);
        mClusterManager.cluster();//after clearing need to use this function to show the new changes

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(users.isEmpty()){
                    noTV.setText(emptyTxt);
                    noTV.setVisibility(View.VISIBLE);
                }else{
                    noTV.setVisibility(View.GONE);
                }
            }
        });
    }

    private boolean hasSearchedJobscope(ArrayList<String> arrayList){
        boolean hasSearchedKeyword = false;
        if(arrayList != null && !arrayList.isEmpty()){
            for(String tempObject : arrayList){
                if(tempObject.toLowerCase().contains(searchedUser.searchNameOrContactOrEmail.toLowerCase())){
                    hasSearchedKeyword = true;
                    break;
                }
            }
        }

        return hasSearchedKeyword;
    }

    private boolean hasSearchedString(String string){
        boolean hasSearchedKeyword = false;
        if(string != null && string.toLowerCase().contains(searchedUser.searchNameOrContactOrEmail.toLowerCase())){
            hasSearchedKeyword = true;
        }

        return hasSearchedKeyword;
    }

    //because the default google map cluster manager will only cluster the marker if there is more than 4 marker on same position
    //so we need to overwrite it ourselves by using the class below
    class CustomMapRenderer<T extends ClusterItem> extends DefaultClusterRenderer<User> {
        private final IconGenerator mClusterIconGenerator = new IconGenerator(parentActivity);
        public CustomMapRenderer(Context context, GoogleMap map, ClusterManager<User> clusterManager) {
            super(context, map, clusterManager);
        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster<User> cluster) {
            //start clustering if at least 2 items overlap
            return cluster.getSize() > 1;
        }

        //for single marker item
        @Override
        protected void onBeforeClusterItemRendered(final User item, final MarkerOptions markerOptions) {
            super.onBeforeClusterItemRendered(item, markerOptions);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //if want to use own icon from drawable then use this
                    if(item.gender == null){
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker_low));
                    }else{
                        switch (item.gender){
                            case User.GENDER_FEMALE:
                                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_female));
                                break;
                            case User.GENDER_MALE:
                                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_male));
                                break;
                            case User.GENDER_OTHER:
                                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker_low));
                                break;
                            default:
                                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker_low));
                                break;
                        }
                    }

                    markerOptions.zIndex(1.0f);
                }
            });
        }

        //for clustered item marker
        //Refer this https://codedump.io/share/JTvSCafSuuf7/1/android-maps-utils-cluster-icon-color
        //and this http://stackoverflow.com/questions/30967961/android-maps-utils-cluster-icon-color
        //for changing icons and colours of clustered markers and single markers with even more control
        @Override
        protected void onBeforeClusterRendered(final Cluster<User> cluster, final MarkerOptions markerOptions){

            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Drawable clusterIcon;

                    if(cluster.getSize() <= 15){
                        clusterIcon = ContextCompat.getDrawable(parentActivity, R.drawable.ic_map_marker_low);
                    }
                    else if(cluster.getSize() > 15){
                            clusterIcon = ContextCompat.getDrawable(parentActivity, R.drawable.ic_map_marker_high);
                    }
                    else{
                            clusterIcon = ContextCompat.getDrawable(parentActivity, R.drawable.ic_map_marker_low);
                    }

                    mClusterIconGenerator.setTextAppearance(R.style.clusterIconText);
                    mClusterIconGenerator.setBackground(clusterIcon);

                    Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
                    markerOptions.zIndex(1.0f);
                }
            });
        }
    }

    /***********************************************************GETTING GPS STUFF START****************************************************/

    private boolean isAllCheckPassed(){
        //if already have data dont refresh it
        if(fullUsers != null && !fullUsers.isEmpty() && boundingBox != null){
            noTV.setVisibility(View.GONE);
//            populateMap();
            return false;
        }

        if (GeneralFunction.dontHaveLocationPermission(parentActivity)) {
            permissionCheckCount++;

            GeneralFunction.requestForLocationPermission(parentActivity,permissionCheckCount);
            noTV.setText(parentActivity.getString(R.string.need_enable_gps));
            noTV.setVisibility(View.VISIBLE);
            return false;
        }

        if (!GeneralFunction.isGpsEnabled(parentActivity)){
            GeneralFunction.showGpsSettingsAlert(parentActivity);
            noTV.setText(parentActivity.getString(R.string.need_on_gps));
            noTV.setVisibility(View.VISIBLE);
            return false;
        }else{
            noTV.setText(parentActivity.getString(R.string.searching_gps));
            noTV.setVisibility(View.VISIBLE);
        }

        return true;
    }

    // Trigger new location updates at interval
    //protected means that this function can only be accessed by the class in the same package only
    protected void startLocationUpdates() {
        parentActivity.runOnUiThread(new Runnable() {
            //added this lint because i actually already checked whether got permission before executung the location updates
            @SuppressLint("MissingPermission")
            @Override
            public void run() {
                if(!isAllCheckPassed()){
                    return;
                }

                // Create the location request to start receiving updates
                LocationRequest locationRequest = new LocationRequest();
                locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                locationRequest.setInterval(Config.GPS_UPDATE_INTERVAL_NEARBY_USER);
                locationRequest.setFastestInterval(Config.GPS_FASTEST_INTERVAL_NEARBY_USER);

                // Create LocationSettingsRequest object using location request
                LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
                builder.addLocationRequest(locationRequest);
                LocationSettingsRequest locationSettingsRequest = builder.build();

                // Check whether location settings are satisfied
                // https://developers.google.com/android/reference/com/google/android/gms/location/SettingsClient
                SettingsClient settingsClient = LocationServices.getSettingsClient(parentActivity);
                settingsClient.checkLocationSettings(locationSettingsRequest);

                // new Google API SDK v11 uses getFusedLocationProviderClient(this)
                locationProviderClient = LocationServices.getFusedLocationProviderClient(parentActivity);
                locationCallback = new LocationCallback(){
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        if(locationResult != null && locationResult.getLastLocation() != null){
                            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(parentActivity);
                            boolean isAllowedTracking = prefs.getBoolean(ProfileSettingFragment.SETTING_TRACKING, false);
                            if(isAllowedTracking){
                                GeneralFunction.getFirestoreInstance()
                                        .collection(User.URL_USER)
                                        .document(parentActivity.uid)
                                        .update(User.GPS,new GeoPoint(locationResult.getLastLocation().getLatitude(),locationResult.getLastLocation().getLongitude()));
                            }
                            searchNearbyUser(new LatLng(locationResult.getLastLocation().getLatitude(),locationResult.getLastLocation().getLongitude()));
                        }else{
                            noTV.setVisibility(View.VISIBLE);
                            noTV.setText(parentActivity.getString(R.string.err_getting_gps));
                        }
                    }

                    @Override
                    public void onLocationAvailability(LocationAvailability locationAvailability) {
                        if(!locationAvailability.isLocationAvailable()){
                            noTV.setVisibility(View.VISIBLE);
                            noTV.setText(parentActivity.getString(R.string.err_getting_gps));
                        }
                    }
                };
                locationProviderClient.requestLocationUpdates(locationRequest,locationCallback, Looper.myLooper());
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Config.REQUEST_GPS_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startLocationUpdates();
                } else {
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.need_enable_gps),R.drawable.notice_bad,TAG);
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.setToolbarTitle(parentActivity.getString(R.string.chat_toolbar_nearby_companion));
        parentActivity.selectChat();
        startLocationUpdates();
    }

    /***********************************************************GETTING GPS STUFF END****************************************************/

    //needs this to prevent duplicate id of map and autocomplete that is making the app crash
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        MapFragment f = (MapFragment) parentActivity.getFragmentManager()
                .findFragmentById(R.id.nearbyUserGoogleMap);
        if (f != null)
            parentActivity.getFragmentManager().beginTransaction().remove(f).commit();
    }

    @Override
    public void onPause() {
        super.onPause();
        if(locationProviderClient != null && locationCallback != null){
            locationProviderClient.removeLocationUpdates(locationCallback);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.clearIV:
                searchET.setText("");
                break;
        }
    }
}
