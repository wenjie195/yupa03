package com.vidatechft.yupa.profileFragments;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.theartofdev.edmodo.cropper.CropImage;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.CheckboxRecyclerGridAdapter;
import com.vidatechft.yupa.adapter.YupaCustomArrayAdapterMethodTwo;
import com.vidatechft.yupa.classes.GlideApp;
import com.vidatechft.yupa.classes.JobSeeker;
import com.vidatechft.yupa.classes.User;
import com.vidatechft.yupa.utilities.Config;
import com.vidatechft.yupa.utilities.CustomMaterialCalendarView;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.FilePath;
import com.vidatechft.yupa.utilities.GeneralFunction;
import com.vidatechft.yupa.utilities.KeyboardUtilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.threeten.bp.Instant;
import org.threeten.bp.ZoneOffset;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static android.app.Activity.RESULT_OK;
import static com.vidatechft.yupa.utilities.CustomMaterialCalendarView.SELECTION_MODE_MULTIPLE_RANGE;
import static com.vidatechft.yupa.utilities.GeneralFunction.GlideImageSetting;

public class EditProfileFragment extends Fragment implements View.OnClickListener, com.tsongkha.spinnerdatepicker.DatePickerDialog.OnDateSetListener
{
    public static final String TAG = EditProfileFragment.class.getName();
    private MainActivity parentActivity;
    private String lastToolbarTitle;

    private ImageButton uploadPicIB;
    private Button uploadPicBTN, sendEditBTN;
    private TextView userNameTV, dobIconTV, genderTV, countryTV, stateTV, hobbyTV, partTimeTV, dobTV;
    private EditText genderET, userNameET;
    private AutoCompleteTextView countryET,stateET,countryCodeATV;
    private ListView listViewEditProfileSpinnerItem;
    private ConstraintLayout constraintLayoutEditProfileSpinnerItem;
    private String filePath,country;
    private CheckboxRecyclerGridAdapter hobbyAdapter,jobAdapter;
    private RecyclerView hobbySetRV,jobSetRV;
    public static final String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

    private ArrayList<CheckBox> getHobby = new ArrayList<>();
    private ArrayList<CheckBox> getJob = new ArrayList<>();
    private SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");
    private Calendar calendar = new GregorianCalendar();
    private int dobDay, dobMonth, dobYear;
    private User user;
    private Switch jobSeekingToggleBtn;
    private ConstraintLayout jobSeekingCL,jobSeekingCalendarCL;
    private CustomMaterialCalendarView jobSeekingCalendarCV;
//    private Button allWeekendsBtn,allWeekdaysBtn,customBtn,everydayBtn;
    private int dateType = 0;
    private int WEEKENDS = 1,
            WEEKDAYS = 2,
            CUSTOM = 3,
            EVERYDAY = 4;
//    private TextView cancelBTN,doneBTN;
    private Map<String, Object> jobSeekerDate = new HashMap<>();
    private Map<String, Object> jobSeekerMap = new HashMap<>();
    private boolean gotCheck = false;
    private Map<String, Object> populateJobSeekerMap = new HashMap<>();
    private Boolean confirmSetDate = null;
    private HashMap<CalendarDay,Integer> populateDateMap;
    private List<String> tempCountryList = new ArrayList<>();
    private View showMoreCountry,showMoreState,showMoreCountryCode;
    private ScrollView editProfileSV;
    private HashMap<String,String> countryAndIdMap = new HashMap<>();
    private HashMap<String,String> countryIdAndStateNameMap = new HashMap<>();
    private ArrayList<String> getStateFromCountryList = new ArrayList<>();
    private ConstraintLayout editProfileRoot;
    private Boolean isOpen = null;
    private EditText contactET;
    private HashMap<String,String> tempCountryCodeMap = new HashMap<>();
    private HashMap<String,String> tempCountrySortNameMap = new HashMap<>();
    private HashMap<String,String> tempCountrySortNameAndCodeMap = new HashMap<>();
    private List<String> tempCountryCodeList = new ArrayList<>();
    private List<String> tempCountrySortNameList = new ArrayList<>();
    private List<String> tempCountrySortNameCodeList = new ArrayList<>();

    FirebaseFirestore db = FirebaseFirestore.getInstance();

    public EditProfileFragment()
    {

    }

    public static EditProfileFragment newInstance(MainActivity parentActivity, User user)
    {
        EditProfileFragment fragment = new EditProfileFragment();
        fragment.parentActivity = parentActivity;
        fragment.user = user;
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState)
    {
        final View view = inflater.inflate(R.layout.fragment_edit_profile, container, false);

        //todo : Get Views
        findView(view);

        //todo : Populate tagEdittext
//        TagEditTextPopulater(hobbyTET, partTimeTET);
        checkBoxGenerator(hobbySetRV,jobSetRV);

        //todo : Get User Personal Data
        getUserPersonalData();

//        buttonClickableAtOnce();
//        doneAndCancel();
        getCurrentDate();

        getCountry();
        getCountryNameAndCode();
        getCountrySortNameAndCode();
        getCountryNameAndSortName();
        getCountryAndID();
//        getCountryIdAndStateName();
        if(getStateFromCountryList.size() == 0){
            getCountryManuallyOnFirstTime();
        }
        initiateCountryAutoCompleteTextView();
        jobSeekingToggleBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(buttonView.isChecked()){
//                    jobSeekingCL.setVisibility(View.VISIBLE);
                    gotCheck = true;
//                    if(dateType == CUSTOM){
//                        jobSeekingCalendarCL.setVisibility(View.VISIBLE);
//                    }
                    jobSeekingCalendarCL.setVisibility(View.VISIBLE);

                }
                else{
                    gotCheck = false;
//                    jobSeekingCL.setVisibility(View.GONE);
                    jobSeekingCalendarCL.setVisibility(View.GONE);
                }
            }
        });


        return view;
    }

    private void checkBoxGenerator(RecyclerView hobbySetRV , RecyclerView jobSetRV)
    {
        final String[] hobbyList = parentActivity.getResources().getStringArray(R.array.editProfileHobby);
        for(String hobby : hobbyList)
        {
            CheckBox hobbyCheckBox = (CheckBox) CheckBox.inflate(parentActivity,R.layout.custom_checkbox,null);
            hobbyCheckBox.setText(hobby.trim());
            getHobby.add(hobbyCheckBox);
        }
        hobbyAdapter = new CheckboxRecyclerGridAdapter(parentActivity,getHobby);

        final String[] jobList = parentActivity.getResources().getStringArray(R.array.jobscope_list);
        for(String job : jobList)
        {
            CheckBox jobCheckBox = (CheckBox) CheckBox.inflate(parentActivity,R.layout.custom_checkbox,null);
            jobCheckBox.setText(job.trim());
            getJob.add(jobCheckBox);
        }
        jobAdapter = new CheckboxRecyclerGridAdapter(parentActivity,getJob);

        hobbySetRV.setNestedScrollingEnabled(false);
        hobbySetRV.setAdapter(hobbyAdapter);
        hobbySetRV.setLayoutManager(new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL));

        jobSetRV.setNestedScrollingEnabled(false);
        jobSetRV.setAdapter(jobAdapter);
        jobSetRV.setLayoutManager(new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL));
    }

    private void setupListener(final Map<String, Object> userPersonalData)
    {
        uploadPicBTN.setOnClickListener(EditProfileFragment.this);
        uploadPicIB.setOnClickListener(EditProfileFragment.this);
        dobIconTV.setOnClickListener(EditProfileFragment.this);
        dobTV.setOnClickListener(EditProfileFragment.this);

        genderET.setOnClickListener(EditProfileFragment.this);
        constraintLayoutEditProfileSpinnerItem.setOnClickListener(this);
//        allWeekendsBtn.setOnClickListener(this);
//        allWeekdaysBtn.setOnClickListener(this);
//        customBtn.setOnClickListener(this);
//        everydayBtn.setOnClickListener(this);
        sendEditBTN.setOnClickListener(new DebouncedOnClickListener(500)
        {
            @Override
            public void onDebouncedClick(View v)
            {
                if(jobSeekingToggleBtn.isChecked()){
                    getAllCustoms();
                    getUserDataInput(userPersonalData);
//                    if(confirmSetDate != null){
//                        getUserDataInput(userPersonalData);
//                    }
//                    else{
//                        GeneralFunction.openMessageNotificationDialog(parentActivity,"Confirm Your dates", R.drawable.notice_bad, TAG);
//                    }
                }
                else{
                    getUserDataInput(userPersonalData);
                }
            }
        });
    }

    private void getUserDataInput(Map<String, Object> userPersonalData)
    {
        Map <String,Object> newUserdata = new HashMap<>();

        String name = userNameET.getText().toString().trim();
        String country = countryET.getText().toString().trim();
        String gender = genderET.getText().toString().trim();
        String countryCode = countryCodeATV.getText().toString().trim();
        String contactNo = contactET.getText().toString().trim();

        if(TextUtils.equals(gender,parentActivity.getString(R.string.search_female)))
        {
            gender = User.GENDER_FEMALE;
        }
        else if(TextUtils.equals(gender,parentActivity.getString(R.string.search_male)))
        {
            gender = User.GENDER_MALE;
        }
        else if
                (TextUtils.equals(gender,parentActivity.getString(R.string.search_prefer_not_to_say)))
        {
            gender = User.GENDER_OTHER;
        }
        else
        {
            gender = User.GENDER_OTHER;
        }

        String state = "";
        if (stateET.getText().toString() != "Please select a state first" && !stateET.getText().toString().equals("Please select a state first"))
        {
            state = stateET.getText().toString().trim();
        }

        if(dobDay == 0 && dobMonth == 0 && dobYear == 0)
        {
            dobDay = ( (Integer) userPersonalData.get(User.DOB_DAY) );
            dobMonth = ( (Integer) userPersonalData.get(User.DOB_MONTH) );
            dobYear = ( (Integer) userPersonalData.get(User.DOB_YEAR) );
        }

        if(isValidInput()){
            newUserdata.put(User.NAME,name);
            newUserdata.put(User.COUNTRY,country);
            newUserdata.put(User.GENDER,gender);
            newUserdata.put(User.STATE,state);
            newUserdata.put(User.DOB_DAY,dobDay);
            newUserdata.put(User.DOB_MONTH,dobMonth);
            newUserdata.put(User.DOB_YEAR,dobYear);

            newUserdata.put(User.ID,userPersonalData.get(User.ID));
            newUserdata.put(User.EMAIL,userPersonalData.get(User.EMAIL));
            newUserdata.put(User.CONTACT,contactNo);
            newUserdata.put(User.COUNTRY_CODE,countryCode);
            newUserdata.put(User.CAN_CONTACT,userPersonalData.get(User.CAN_CONTACT));
            newUserdata.put(User.ACC_TYPE,userPersonalData.get(User.ACC_TYPE));
            newUserdata.put(User.GPS, userPersonalData.get(User.GPS));
            newUserdata.put(User.DATE_CREATED,userPersonalData.get(User.DATE_CREATED));
            newUserdata.put(User.DATE_UPDATED,FieldValue.serverTimestamp());
            newUserdata.put(User.IS_ONLINE,userPersonalData.get(User.IS_ONLINE));
            newUserdata.put(User.LAST_ONLINE,userPersonalData.get(User.LAST_ONLINE));
            newUserdata.put(User.IS_EMPLOYER,userPersonalData.get(User.IS_EMPLOYER));
            newUserdata.put(User.IS_HOST,userPersonalData.get(User.IS_HOST));
            newUserdata.put(User.IS_HOTELIER,userPersonalData.get(User.IS_HOTELIER));
            newUserdata.put(User.IS_MERCHANT,userPersonalData.get(User.IS_MERCHANT));
            newUserdata.put(User.IS_ADMIN,userPersonalData.get(User.IS_ADMIN));

            if(userPersonalData.get(User.URL_USER_TOUR_BUDDY) != null)
            {
                newUserdata.put(User.URL_USER_TOUR_BUDDY,userPersonalData.get(User.URL_USER_TOUR_BUDDY));
            }
            if(userPersonalData.containsKey(User.IS_TOUR_BUDDY))
            {
                newUserdata.put(User.IS_TOUR_BUDDY,userPersonalData.get(User.IS_TOUR_BUDDY));
            }

            for(CheckBox hobbyScopeCB : hobbyAdapter.getCheckBoxes())
            {
                if(hobbyScopeCB != null && hobbyScopeCB.isChecked()){
                    newUserdata.put(hobbyScopeCB.getText().toString().trim(),true);
                }
            }

            for(CheckBox jobScopeCB : jobAdapter.getCheckBoxes())
            {
                if(jobScopeCB != null && jobScopeCB.isChecked()){
                    newUserdata.put(jobScopeCB.getText().toString().trim(),true);
                }
            }
//        for(String tag : hobbyTET.getTaglist())
//        {
//            newUserdata.put(tag.trim(),true);
//        }
//        for(String tag : partTimeTET.getTaglist())
//        {
//            newUserdata.put(tag.trim(),true);
//        }


            if(jobSeekingToggleBtn.isChecked()){
//                switch (dateType){
//                    case 1:
//                        saveIntoFirestore(newUserdata,userPersonalData,false);
//                        break;
//                    case 2:
//                        saveIntoFirestore(newUserdata,userPersonalData,false);
//                        break;
//                    case 3:
//                        saveIntoFirestore(newUserdata,userPersonalData,true);
//                        break;
//                    case 4:
//                        saveIntoFirestore(newUserdata,userPersonalData,false);
//                        break;
//                    case 0:
//                        GeneralFunction.openMessageNotificationDialog(parentActivity,"Please select date", R.drawable.notice_bad,TAG);
//                        break;
//                }
                saveIntoFirestore(newUserdata,userPersonalData,true);
            }
            else{
                saveIntoFirestore(newUserdata,userPersonalData,false);
            }
        }
    }

    private void saveIntoFirestore(final Map<String,Object>newUserdata, final Map<String,Object>userPersonalData, boolean isCustom){

        initializeProgressBar();

        if(!isCustom){
            jobSeekerDate = new JobSeeker().setMap(parentActivity,jobSeekerMap,null,null,dateType,false);
        }

        newUserdata.put(User.JOB_SEEKER,jobSeekerDate);
        userPersonalData.put(User.JOB_SEEKER,jobSeekerDate);
        saveUserProfileDetail(newUserdata,userPersonalData);

    }

    private void saveUserProfileDetail(Map<String,Object>newUserdata,Map<String,Object>userPersonalData){
        if(gotCheck){
            newUserdata.put(User.IS_JOB_SEEKER,true);
            userPersonalData.put(User.IS_JOB_SEEKER,true);
        }
        else{
            newUserdata.put(User.IS_JOB_SEEKER,false);
            userPersonalData.put(User.IS_JOB_SEEKER,false);
        }

        if(filePath != null)
        {
            //todo : Save data with image
            goToStorage(newUserdata);
        }
        else
        {
            if(userPersonalData.get(User.PROFILE_PIC_URL)!= null)
            {
                newUserdata.put(User.PROFILE_PIC_URL,String.valueOf(userPersonalData.get(User.PROFILE_PIC_URL)));
            }
            else
            {
                newUserdata.put(User.PROFILE_PIC_URL,"");
            }
            //todo : Save data with no image
            goToFireStore(newUserdata);
        }
    }

    private void initializeProgressBar()
    {
        parentActivity.initializeProgressIndicator(parentActivity.getString(R.string.uploadEP_picture), false, this);
        parentActivity.controlProgressIndicator(true, this);
    }

//    private void checkCountry(EditText ETcountry,String country)
//    {
//        String newString = country.replace(" ", "_");
//        if(ETcountry.getText().toString() != "" && !ETcountry.getText().toString().equals(""))
//        {
//            slideUpAnimationForListViewCountry(parentActivity,constraintLayoutEditProfileSpinnerItem,listViewEditProfileSpinnerItem,newString,stateET);
//            MainActivity.isOpen = true;
//        }
//        else
//        {
//            stateET.setText("Please select a state first");
//        }
//    }

    private void goToFireStore(Map<String,Object> userPersonalData)
    {
        DocumentReference tRef = db.collection(User.URL_USER).document(parentActivity.uid);

        if(populateDateMap != null){
            tRef.update(userPersonalData)
                    .addOnSuccessListener(new OnSuccessListener<Void>()
                    {
                        @Override
                        public void onSuccess(Void aVoid)
                        {
                            GeneralFunction.handleUploadSuccess(parentActivity, EditProfileFragment.this, R.string.uploadEP_detail_success, TAG);
                        }
                    }).addOnFailureListener(new OnFailureListener()
            {
                @Override
                public void onFailure(@NonNull Exception e)
                {
                    GeneralFunction.handleUploadError(parentActivity, EditProfileFragment.this, R.string.uploadEP_detail_fail, TAG);
                }
            });
        }
        else{
            tRef.set(userPersonalData)
                    .addOnSuccessListener(new OnSuccessListener<Void>()
                    {
                        @Override
                        public void onSuccess(Void aVoid)
                        {
                            GeneralFunction.handleUploadSuccess(parentActivity, EditProfileFragment.this, R.string.uploadEP_detail_success, TAG);
                        }
                    }).addOnFailureListener(new OnFailureListener()
            {
                @Override
                public void onFailure(@NonNull Exception e)
                {
                    GeneralFunction.handleUploadError(parentActivity, EditProfileFragment.this, R.string.uploadEP_detail_fail, TAG);
                }
            });
        }

    }

    private void goToStorage(final Map<String,Object> userPersonalData)
    {
        final StorageReference ref = GeneralFunction.getFirebaseStorageInstance().child(User.URL_USER + "/" + parentActivity.uid + "/" + User.URL_USER_PROFILE_PICTURE);

        UploadTask uploadTask = ref.putBytes(GeneralFunction.getCompressedBitmap(parentActivity, filePath));
        uploadTask.addOnFailureListener(new OnFailureListener()
        {
            @Override
            public void onFailure(@NonNull Exception exception)
            {
                // Handle unsuccessful uploads on all images ...
                GeneralFunction.handleUploadError(parentActivity, EditProfileFragment.this, R.string.employer_err_job_posting_job, TAG);
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>()
        {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot)
            {
                ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>()
                {
                    @Override
                    public void onSuccess(Uri uri)
                    {
                        if(uri != null)
                        {
                            userPersonalData.put(User.PROFILE_PIC_URL, uri.toString());
                            goToFireStore(userPersonalData);
                        }
                    }
                });
            }
        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>()
        {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot)
            {
                // + 10 is because i set the progress to 10 when it finished saving the initial firebase data
                // * 80 is because i want the progress to go to maximum 90% only when the image is finished uploaded
                // so that the final 10% is set when the firebase updates the imageUrl data as the final stepe
                int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * 80) + 10;
                parentActivity.updateProgress((float) totalProgress, EditProfileFragment.this);
            }
        });
    }

    private void getUserPersonalData()
    {
        ///todo : before this i put listener registration
        if (user != null)
        {
            final Map<String,Object> userPersonalData = new HashMap<>();
            User tempUser = user;

            //todo : Put data into map
            putAllDataIntoMap(userPersonalData,tempUser);

            //todo : Display data
            displayData(userPersonalData,user);

            //todo : SetupListener
            setupListener(userPersonalData);

            //todo : Change the cites respective to their country
//            textChangeForCountry(countryET,stateET,tempUser);
        }
        else
        {
            Log.d(TAG, "No such document");
        }

    }

//    private void textChangeForCountry(final EditText countryET ,final EditText stateET , User tempUser)
//    {
//        countryET.addTextChangedListener(new TextWatcher()
//        {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after)
//            {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count)
//            {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s)
//            {
//                country = s.toString().replaceAll(" ","_");
//                GeneralFunction.slideUpAnimationForListViewCountry(parentActivity,constraintLayoutEditProfileSpinnerItem,listViewEditProfileSpinnerItem,country,stateET);
//
//                stateET.setText("");
//            }
//        });
//    }

//    private void TagEditTextPopulater(TagEditText hobby,TagEditText partTime)
//    {
//        hobby.setParentActivity(parentActivity);
//        ArrayAdapter<String> hobbyAdapter = new ArrayAdapter<>(parentActivity,android.R.layout.simple_dropdown_item_1line , getResources().getStringArray(R.array.editProfileHobby));
//        hobby.setAdapter(hobbyAdapter);
//        hobby.setThreshold(1);
//        hobby.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
//
//        partTime.setParentActivity(parentActivity);
//        ArrayAdapter<String> joblistAdapter = new ArrayAdapter<>(parentActivity,android.R.layout.simple_dropdown_item_1line , getResources().getStringArray(R.array.jobscope_list));
//        partTime.setAdapter(joblistAdapter);
//        partTime.setThreshold(1);
//        partTime.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
//    }

    private void displayData(Map<String,Object> userPersonalData, User user) {
        if(user.profilePicUrl != null) {
            GlideApp.with(EditProfileFragment.this).load(user.profilePicUrl).centerCrop().into(uploadPicIB);
        }

        if(user.name != null) {
            userNameET.setText(user.name);
        }

        if(user.gender != null) {
            genderET.setText(user.gender);
        }

        dobIconTV.setText(GeneralFunction.formatDateToString(GeneralFunction.getDefaultUtcCalendar(user.dobYear,user.dobMonth - 1,user.dobDay).getTimeInMillis()));

        if(user.country != null) {
            countryET.setText(user.country);
        }

        if(user.state != null) {
            stateET.setText(user.state);
        }

        if(user.countryCode != null){
            countryCodeATV.setText(user.countryCode);
        }
        if(user.contactNo != null){
            contactET.setText(user.contactNo);
        }

        for(CheckBox compareHobby:hobbyAdapter.getCheckBoxes())
        {
            if(userPersonalData.containsKey(compareHobby.getText().toString()))
            {
                compareHobby.setChecked(true);
            }
        }

        for(CheckBox compareJob:jobAdapter.getCheckBoxes())
        {
            if(userPersonalData.containsKey(compareJob.getText().toString()))
            {
                compareJob.setChecked(true);
            }
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if(Objects.equals(userPersonalData.get(User.IS_JOB_SEEKER), true)){
                jobSeekingToggleBtn.setChecked(true);
                gotCheck = true;
                if(jobSeekingToggleBtn.isChecked()){
//                    jobSeekingCL.setVisibility(View.VISIBLE);
                }
                else{
//                    jobSeekingCL.setVisibility(View.GONE);
                    jobSeekingCalendarCL.setVisibility(View.GONE);
                }
            }
        }
        if(userPersonalData.get(User.JOB_SEEKER) != null){
            populateJobSeekerMap = (Map<String, Object>) userPersonalData.get(User.JOB_SEEKER);
        }
        if(user.isJobSeeker != null && user.isJobSeeker){
            jobSeekingCalendarCL.setVisibility(View.VISIBLE);
        }
        else{
            jobSeekingCalendarCL.setVisibility(View.GONE);
        }
        if(populateJobSeekerMap != null){
//           String availability = String.valueOf(populateJobSeekerMap.get(JobSeeker.AVAILABILITY));
//           switch (availability){
//               case JobSeeker.STATUS_ALL_WEEKEND:
//                   setButtonTrue(allWeekendsBtn);
//
//                   setButtonFalse(allWeekdaysBtn);
//                   setButtonFalse(everydayBtn);
//                   setButtonFalse(customBtn);
//
//                   dateType = WEEKENDS;
//                   setVisibilityOfCalendar(dateType);
//                   break;
//               case JobSeeker.STATUS_ALL_WEEKDAY:
//                   setButtonTrue(allWeekdaysBtn);
//
//                   setButtonFalse(allWeekendsBtn);
//                   setButtonFalse(everydayBtn);
//                   setButtonFalse(customBtn);
//
//                   dateType = WEEKDAYS;
//                   setVisibilityOfCalendar(dateType);
//                   break;
//               case JobSeeker.STATUS_CUSTOM:
//                   setButtonTrue(customBtn);
//
//                   setButtonFalse(allWeekendsBtn);
//                   setButtonFalse(everydayBtn);
//                   setButtonFalse(allWeekdaysBtn);
//
//                   dateType = CUSTOM;
//                   setVisibilityOfCalendar(dateType);
//                   break;
//               case JobSeeker.STATUS_EVERYDAY:
//                   setButtonTrue(everydayBtn);
//
//                   setButtonFalse(allWeekendsBtn);
//                   setButtonFalse(customBtn);
//                   setButtonFalse(allWeekdaysBtn);
//
//                   dateType = EVERYDAY;
//                   setVisibilityOfCalendar(dateType);
//                   break;
//           }

            if(populateJobSeekerMap.get(JobSeeker.GET_ALL_AVAILABLE_DATES) != null){

                populateDateMap = new HashMap<>();
                Map<String, Object> customDateMap = (Map<String, Object>) populateJobSeekerMap.get(JobSeeker.GET_ALL_AVAILABLE_DATES);

                if (customDateMap != null) {
                    for(Map.Entry<String, Object> currentKey : customDateMap.entrySet()){
                        String key = currentKey.getKey();
                        Object value = currentKey.getValue();

//                        switch (Integer.parseInt(value.toString())){
//                            case 0:
//                                populateDateMap.put(CalendarDay.from(Instant.ofEpochMilli(Long.parseLong(key)).atZone(ZoneOffset.UTC).toLocalDate()),parentActivity.getResources().getDrawable(R.drawable.rounded_circle_green));
//                                break;
//                            case 1:
//                                populateDateMap.put(CalendarDay.from(Instant.ofEpochMilli(Long.parseLong(key)).atZone(ZoneOffset.UTC).toLocalDate()),parentActivity.getResources().getDrawable(R.drawable.left_half_circle));
//                                break;
//                            case 2:
//                                populateDateMap.put(CalendarDay.from(Instant.ofEpochMilli(Long.parseLong(key)).atZone(ZoneOffset.UTC).toLocalDate()),parentActivity.getResources().getDrawable(R.drawable.square_calendar));
//                                break;
//                            case 3:
//                                populateDateMap.put(CalendarDay.from(Instant.ofEpochMilli(Long.parseLong(key)).atZone(ZoneOffset.UTC).toLocalDate()),parentActivity.getResources().getDrawable(R.drawable.right_half_circle));
//                                break;
//                            default:
//                                populateDateMap.put(CalendarDay.from(Instant.ofEpochMilli(Long.parseLong(key)).atZone(ZoneOffset.UTC).toLocalDate()),parentActivity.getResources().getDrawable(R.drawable.rounded_circle_green));
//                                break;
//                        }

                        populateDateMap.put(CalendarDay.from(Instant.ofEpochMilli(Long.parseLong(key)).atZone(ZoneOffset.UTC).toLocalDate()),Integer.parseInt(value.toString()));
                    }

                    //need to delay this function because the material calendar view havent initialized yet and he thinks i didnt select the dates from database yet
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            jobSeekingCalendarCV.setNewAllSelectedDate(parentActivity,populateDateMap);
                        }
                    },100);

                }

            }
        }
    }

    private void putAllDataIntoMap(Map<String,Object> userPersonalData , User tempUser)
    {
        userPersonalData.put(User.ID,tempUser.id);
        userPersonalData.put(User.NAME,tempUser.name);
        userPersonalData.put(User.EMAIL,tempUser.email);
        userPersonalData.put(User.PROFILE_PIC_URL,tempUser.profilePicUrl);
        userPersonalData.put(User.DOB_DAY,tempUser.dobDay);
        userPersonalData.put(User.DOB_MONTH,tempUser.dobMonth);
        userPersonalData.put(User.DOB_YEAR,tempUser.dobYear);

        String dateOfYear;
        if(tempUser.dobDay != 0 && tempUser.dobMonth != 0 && tempUser.dobYear != 0)
        {
            String teDay = String.valueOf(tempUser.dobDay);
            String teYear = String.valueOf(tempUser.dobYear);

            dateOfYear = "" + teDay + " " + MONTHS[ tempUser.dobMonth - 1 ] + " " + teYear + "";
        }
        else
        {
            dateOfYear = "Please Enter Your Date";
        }

        userPersonalData.put("dateOfYear",dateOfYear);
        userPersonalData.put(User.COUNTRY,tempUser.country);
        userPersonalData.put(User.COUNTRY_CODE,tempUser.countryCode);
        userPersonalData.put(User.GENDER,tempUser.gender);
        userPersonalData.put(User.STATE,tempUser.state);

        userPersonalData.put(User.CONTACT,tempUser.contactNo);

        if(tempUser.tourBuddy !=null)
        {
            userPersonalData.put(User.URL_USER_TOUR_BUDDY,tempUser.tourBuddy);
        }
        if(tempUser.isTourBuddy != null)
        {
            userPersonalData.put(User.IS_TOUR_BUDDY,tempUser.isTourBuddy);
        }

        userPersonalData.put(User.IS_JOB_SEEKER,tempUser.isJobSeeker);
        if(tempUser.jobSeekerMap != null){
            jobSeekerMap = tempUser.jobSeekerMap;
            userPersonalData.put(User.JOB_SEEKER,tempUser.jobSeekerMap);
        }

        userPersonalData.put(User.CAN_CONTACT,tempUser.canContact);
        userPersonalData.put(User.ACC_TYPE,tempUser.accType);
        userPersonalData.put(User.GPS, tempUser.gps);
        userPersonalData.put(User.DATE_CREATED,tempUser.dateCreated);
        userPersonalData.put(User.DATE_UPDATED,tempUser.dateUpdated);
        userPersonalData.put(User.IS_ONLINE,tempUser.isOnline);
        userPersonalData.put(User.LAST_ONLINE,tempUser.lastOnline);
        userPersonalData.put(User.IS_EMPLOYER,tempUser.isEmployer);
        userPersonalData.put(User.IS_HOST,tempUser.isHost);
        userPersonalData.put(User.IS_HOTELIER,tempUser.isHotelier);
        userPersonalData.put(User.IS_MERCHANT,tempUser.isMerchant);
        userPersonalData.put(User.IS_ADMIN,tempUser.isAdmin);

        if(tempUser.hobbyScope != null)
        {
            for(String hobby:tempUser.hobbyScope)
            {
                userPersonalData.put(hobby,true);
            }
        }

        if(tempUser.jobScope != null)
        {
            for(String job:tempUser.jobScope)
            {
                userPersonalData.put(job,true);
            }
        }
    }

    private void findView(View view)
    {
        uploadPicIB = view.findViewById(R.id.uploadPicIB);
        uploadPicBTN = view.findViewById(R.id.uploadPicBTN);
        sendEditBTN = view.findViewById(R.id.sendEditBTN);
        userNameTV = view.findViewById(R.id.userNameTV);
        genderTV = view.findViewById(R.id.genderTV);
        dobIconTV = view.findViewById(R.id.dobIconTV);
        dobTV = view.findViewById(R.id.dobTV);
        countryTV = view.findViewById(R.id.countryTV);
        stateTV = view.findViewById(R.id.stateTV);
        hobbyTV = view.findViewById(R.id.hobbyTV);
        partTimeTV = view.findViewById(R.id.partTimeTV);
        userNameET = view.findViewById(R.id.userNameET);
        genderET = view.findViewById(R.id.genderET);
        countryET = view.findViewById(R.id.countryET);
        stateET = view.findViewById(R.id.stateET);
        hobbySetRV = view.findViewById(R.id.earnRV);
        jobSetRV = view.findViewById(R.id.earn1RV);
        listViewEditProfileSpinnerItem = view.findViewById(R.id.listViewEditProfileSpinnerItem);
        constraintLayoutEditProfileSpinnerItem = view.findViewById(R.id.constraintLayoutEditProfileSpinnerItem);
        //Constraint Layout
//        jobSeekingCL = view.findViewById(R.id.jobSeekingCL);
        jobSeekingCalendarCL = view.findViewById(R.id.jobSeekingCalendarCL);
        editProfileRoot = view.findViewById(R.id.editProfileRoot);
        //Button
//        allWeekendsBtn = view.findViewById(R.id.allWeekendsBtn);
//        allWeekdaysBtn = view.findViewById(R.id.allWeekdaysBtn);
//        customBtn = view.findViewById(R.id.customBtn);
//        everydayBtn = view.findViewById(R.id.everydayBtn);
        //Custom Calendar View
        jobSeekingCalendarCV = view.findViewById(R.id.jobSeekingCalendarCV);
        //Toggle Button
        jobSeekingToggleBtn = view.findViewById(R.id.jobSeekingToggleBtn);
        //Scroll View
        editProfileSV = view.findViewById(R.id.editProfileSV);
        //View
        showMoreCountry = view.findViewById(R.id.showMoreCountry);
        showMoreState = view.findViewById(R.id.showMoreState);
        showMoreCountryCode = view.findViewById(R.id.showMoreCountryCode);
        //EditText
        contactET = view.findViewById(R.id.contactET);
        //AutoCompleteTextView
        countryCodeATV = view.findViewById(R.id.countryCodeATV);
        //TextView
//        doneBTN = view.findViewById(R.id.doneBTN);
//        cancelBTN = view.findViewById(R.id.cancelBTN);


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE)
            {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                Uri resultUri = result.getUri();
                filePath = FilePath.getPath(parentActivity, resultUri);
                GlideImageSetting(parentActivity, resultUri, uploadPicIB);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void setButtonTrue(Button btnTrue)
    {
        btnTrue.setBackground(getResources().getDrawable(R.drawable.rounded_shape_button_done));
        btnTrue.setTextColor((getResources().getColor(R.color.white)));
        btnTrue.setSelected(true);
    }

    private void setButtonFalse(Button btnFalse)
    {
        btnFalse.setBackground(getResources().getDrawable(R.drawable.rounded_acctype_white_bg_border_main));
        btnFalse.setTextColor((getResources().getColor(R.color.colorPrimary)));
        btnFalse.setSelected(false);
    }
//    private void buttonClickableAtOnce()
//    {
//        allWeekendsBtn.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View v)
//            {
//
//                setButtonTrue(allWeekendsBtn);
//
//                setButtonFalse(allWeekdaysBtn);
//                setButtonFalse(customBtn);
//                setButtonFalse(everydayBtn);
//
//                dateType = WEEKENDS;
//                setVisibilityOfCalendar(dateType);
//            }
//        });
//        allWeekdaysBtn.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View v)
//            {
//                setButtonTrue(allWeekdaysBtn);
//
//                setButtonFalse(allWeekendsBtn);
//                setButtonFalse(customBtn);
//                setButtonFalse(everydayBtn);
//
//                dateType = WEEKDAYS;
//                setVisibilityOfCalendar(dateType);
//            }
//        });
//        customBtn.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View v)
//            {
//                setButtonTrue(customBtn);
//
//                setButtonFalse(allWeekendsBtn);
//                setButtonFalse(allWeekdaysBtn);
//                setButtonFalse(everydayBtn);
//
//                dateType = CUSTOM;
//                setVisibilityOfCalendar(dateType);
//            }
//        });
//        everydayBtn.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View v)
//            {
//                setButtonTrue(everydayBtn);
//
//                setButtonFalse(allWeekendsBtn);
//                setButtonFalse(allWeekdaysBtn);
//                setButtonFalse(customBtn);
//
//                dateType = EVERYDAY;
//                setVisibilityOfCalendar(dateType);
//            }
//        });
//    }
    private void setVisibilityOfCalendar(int visibility)
    {
        if (visibility == CUSTOM && jobSeekingToggleBtn.isChecked())
        {
            jobSeekingCalendarCL.setVisibility(View.VISIBLE);
        }
        else
        {
            jobSeekingCalendarCL.setVisibility(View.GONE);
        }
    }

//    private void doneAndCancel()
//    {
//        doneBTN.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View v)
//            {
//                confirmSetDate = true;
//                if(dateType != 0)
//                {
//                    if (dateType == CUSTOM){
//                        getAllCustoms();
//                        jobSeekingCalendarCL.setVisibility(View.GONE);
//                    }
//                    jobSeekingCalendarCL.setVisibility(View.GONE);
//                }
//                else
//                {
//                    GeneralFunction.openMessageNotificationDialog(parentActivity,"Please choose your available date",R.drawable.notice_bad,TAG);
//                }
//
//            }
//        });
//
//        cancelBTN.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                confirmSetDate = false;
//                jobSeekingCalendarCL.setVisibility(View.GONE);
//            }
//        });
//    }

    private void getAllCustoms()
    {
        final Map <String,Object> setDates = new HashMap<>();
        final Map <String,Object> setDatesBoolean = new HashMap<>();
        HashMap<CalendarDay,Integer> multiRangeDayMap = jobSeekingCalendarCV.getSelectedMultiRangeDay();

        for (Map.Entry<CalendarDay, Integer> entry : multiRangeDayMap.entrySet()) {
            long getFormattedTime = GeneralFunction.getDefaultUtcCalendar(
                    entry.getKey().getYear(),
                    entry.getKey().getMonth() - 1, //need - 1 because the android Calendar class's month default starts at 0
                    entry.getKey().getDay())
                    .getTime().getTime();
            setDates.put(String.valueOf(getFormattedTime),entry.getValue());
            setDatesBoolean.put(String.valueOf(getFormattedTime),true);
        }
        jobSeekerDate = new JobSeeker().setMap(parentActivity,jobSeekerMap,setDates,setDatesBoolean,dateType,true);
    }

    private void getCurrentDate()
    {
        jobSeekingCalendarCV.setContext(parentActivity);
        jobSeekingCalendarCV.setSelectionMode(SELECTION_MODE_MULTIPLE_RANGE);
        jobSeekingCalendarCV.hideButtons();
    }

    private void showDate(int year, int monthOfYear, int dayOfMonth, int spinnerTheme) {
        if(dobDay > 0 && dobMonth > 0 && dobYear > 0){
            year = dobYear;
            monthOfYear = dobMonth - 1;
            dayOfMonth = dobDay;
        }
        new SpinnerDatePickerDialogBuilder()
                .context(parentActivity)
                .callback(EditProfileFragment.this)
                .spinnerTheme(spinnerTheme)
                .defaultDate(year, monthOfYear, dayOfMonth)
                .build()
                .show();
    }

    @Override
    public void onDateSet(com.tsongkha.spinnerdatepicker.DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        dobDay = dayOfMonth;
        dobMonth = monthOfYear + 1;
        dobYear = year;
        calendar.set(year,monthOfYear,dayOfMonth);

        parentActivity.runOnUiThread(new Runnable() {
            public void run() {
                dobIconTV.setText(format.format(calendar.getTime()));
            }
        });
    }

    private void onDobFocused(){
        if(user != null){
            showDate(user.dobYear,user.dobMonth - 1,user.dobDay,R.style.DatePickerSpinner);
        }else{
            Calendar cal = GeneralFunction.getDefaultUtcCalendar();
            showDate(cal.get(Calendar.YEAR),cal.get(Calendar.MONTH),cal.get(Calendar.DAY_OF_MONTH),R.style.DatePickerSpinner);
        }
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.dobIconTV:
                onDobFocused();
                break;
            case R.id.dobTV:
                onDobFocused();
                break;
            case R.id.stateET:
//                checkCountry(countryET,countryET.getText().toString());
                break;
//            case R.id.countryET:
//                GeneralFunction.slideUpAnimationForListView(parentActivity,constraintLayoutEditProfileSpinnerItem, listViewEditProfileSpinnerItem,R.array.country_list,countryET);
//                MainActivity.isOpen = true;
//                break;
            case R.id.genderET:
                GeneralFunction.slideUpAnimationForListView(parentActivity,constraintLayoutEditProfileSpinnerItem, listViewEditProfileSpinnerItem,R.array.editProfileGender,genderET);
                MainActivity.isOpen = true;
                break;
            case R.id.uploadPicIB:
                GeneralFunction.showFileChooser11(parentActivity,this);
                break;
            case R.id.uploadPicBTN:
                GeneralFunction.showFileChooser11(parentActivity,this);
                break;
            case R.id.constraintLayoutEditProfileSpinnerItem:
                GeneralFunction.slideDownAnimationForListView(parentActivity,constraintLayoutEditProfileSpinnerItem);
                break;

        }
    }

    private ArrayList<HashMap<String, String>>  getCountriesFromJSONFile () {
        ArrayList<HashMap<String, String>> countryList = new ArrayList<HashMap<String, String>>();
        try {
            JSONObject obj = new JSONObject(GeneralFunction.loadJSONFromAsset(Config.COUNTRY_JSON_FILE, parentActivity));
            JSONArray countryArray = obj.getJSONArray(Config.COUNTRY_ARRAY);
            HashMap<String, String> countryMap;

            for (int i = 0; i < countryArray.length(); i++) {
                JSONObject jo_inside = countryArray.getJSONObject(i);
//                Log.d("Details-->", jo_inside.getString(Config.COUNTRY_SORT_NAME_JSON));

                String countrySortName = jo_inside.getString(Config.COUNTRY_SORT_NAME_JSON);
                String countryName = jo_inside.getString(Config.COUNTRY_NAME_JSON);
                String countryCode = jo_inside.getString(Config.COUNTRY_CODE_JSON);
                String countryId = jo_inside.getString(Config.COUNTRY_ID_JSON);

                //Add your values in your `ArrayList` as below:
                countryMap = new HashMap<String, String>();
                countryMap.put(Config.COUNTRY_SORT_NAME, countrySortName);
                countryMap.put(Config.COUNTRY_NAME, countryName);
                countryMap.put(Config.COUNTRY_CODE, countryCode);
                countryMap.put(Config.COUNTRY_ID, countryId);

                countryList.add(countryMap);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return countryList;
    }

    private ArrayList<HashMap<String, String>> getStateFromJSONFile () {
        ArrayList<HashMap<String, String>> countryList = new ArrayList<HashMap<String, String>>();
        try {
            JSONObject obj = new JSONObject(GeneralFunction.loadJSONFromAsset(Config.STATE_JSON_FILE, parentActivity));
            JSONArray countryArray = obj.getJSONArray(Config.STATE_ARRAY);
            HashMap<String, String> countryMap;

            for (int i = 0; i < countryArray.length(); i++) {
                JSONObject jo_inside = countryArray.getJSONObject(i);

                String stateName = jo_inside.getString(Config.STATE_NAME_JSON);
                String stateId = jo_inside.getString(Config.STATE_ID_JSON);
                String stateCountryId = jo_inside.getString(Config.STATE_COUNTRY_ID_JSON);

                //Add your values in your `ArrayList` as below:
                countryMap = new HashMap<String, String>();
                countryMap.put(Config.STATE_ID, stateId);
                countryMap.put(Config.STATE_NAME, stateName);
                countryMap.put(Config.STATE_COUNTRY_ID, stateCountryId);

                countryList.add(countryMap);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return countryList;
    }

    private void getCountry(){

        ArrayList<HashMap<String,String>> allCountriesMap = getCountriesFromJSONFile();

        for (HashMap<String, String> map : allCountriesMap){
            for (Map.Entry<String, String> mapEntry : map.entrySet())
            {
                String key = mapEntry.getKey();
                String value = mapEntry.getValue();
                if(key.equals(Config.COUNTRY_NAME)){
                    tempCountryList.add(value);
                }
            }
        }
    }

    private void getCountryAndID(){

        ArrayList<HashMap<String,String>> allCountriesMap = getCountriesFromJSONFile();
        String countryName = null;
        String countryId = null;

        for (HashMap<String, String> map : allCountriesMap){
            for (Map.Entry<String, String> mapEntry : map.entrySet())
            {
                String key = mapEntry.getKey();
                String value = mapEntry.getValue();

                if(key.equals(Config.COUNTRY_NAME)){
                    countryName = value;
                }
                if(key.equals(Config.COUNTRY_ID)){
                    countryId = value;
                }
                if(countryName != null && countryId != null){
                    countryAndIdMap.put(countryName,countryId);
                    countryName = null;
                    countryId = null;
                }
            }
        }
    }

    private String getCountryId(String countryName){
        String countryId = null;
        for (Map.Entry<String, String> mapEntry : countryAndIdMap.entrySet())
        {
            String key = mapEntry.getKey();
            String value = mapEntry.getValue();

            if(key.equals(countryName)){
                countryId = value;
            }
        }
        return countryId;
    }

//    private void getCountryIdAndStateName(){
//        ArrayList<HashMap<String,String>> allStateMap = getStateFromJSONFile();
//
//        String stateCountryId = null;
//        String stateName = null;
//        for (HashMap<String, String> map : allStateMap){
//            for (Map.Entry<String, String> mapEntry : map.entrySet())
//            {
//                String key = mapEntry.getKey();
//                String value = mapEntry.getValue();
//                if(key.equals(Config.STATE_NAME)){
//                    stateName = value;
//                }
//                if(key.equals(Config.STATE_COUNTRY_ID)){
//                    stateCountryId = value;
//                }
//                if(stateName != null && stateCountryId != null){
//                    countryIdAndStateNameMap.put(stateName,stateCountryId);
//                    stateCountryId = null;
//                    stateName = null;
//                }
//            }
//        }
//    }

    //the above function getCountryIdAndStateName can get state but not all state is shown like amazonas in Brazil. use this getStateFromCountry instead
    private void getStateFromCountry(String countryId){
        //some state are missing because they deleted it from the json file. So DONT'T think that the Last ID is equal to the whole state list's size
        //for example 3478 and 3479 is missing from the state list

        ArrayList<HashMap<String, String>> countryList = new ArrayList<HashMap<String, String>>();
        try {
            JSONObject obj = new JSONObject(GeneralFunction.loadJSONFromAsset(Config.STATE_JSON_FILE, parentActivity));
            JSONArray countryArray = obj.getJSONArray(Config.STATE_ARRAY);
            HashMap<String, String> countryMap;

            for (int i = 0; i < countryArray.length(); i++) {
                JSONObject jo_inside = countryArray.getJSONObject(i);

                String stateName = jo_inside.getString(Config.STATE_NAME_JSON);
                String stateId = jo_inside.getString(Config.STATE_ID_JSON);
                String stateCountryId = jo_inside.getString(Config.STATE_COUNTRY_ID_JSON);

                //Add your values in your `ArrayList` as below:
                countryMap = new HashMap<String, String>();
                countryMap.put(Config.STATE_ID, stateId);
                countryMap.put(Config.STATE_NAME, stateName);
                countryMap.put(Config.STATE_COUNTRY_ID, stateCountryId);

                if(stateCountryId.equals(countryId)){
                    countryIdAndStateNameMap.put(stateName,stateCountryId);
                }
                countryList.add(countryMap);
            }
            countryIdAndStateNameMap.size();

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(countryIdAndStateNameMap != null){
            for (Map.Entry<String, String> mapEntry : countryIdAndStateNameMap.entrySet())
            {
                String key = mapEntry.getKey();
                String value = mapEntry.getValue();
                if(value.equals(countryId)){
                    getStateFromCountryList.add(key);
                }
            }
        }
    }

    private void initiateCountryAutoCompleteTextView(){
        final YupaCustomArrayAdapterMethodTwo countryAdapter = new YupaCustomArrayAdapterMethodTwo(parentActivity,android.R.layout.simple_dropdown_item_1line,tempCountryList);
        final YupaCustomArrayAdapterMethodTwo stateAdapter = new YupaCustomArrayAdapterMethodTwo(parentActivity,android.R.layout.simple_dropdown_item_1line,getStateFromCountryList);
        //Programmatically set the auto complete textview drop down height
        countryET.setDropDownHeight(getResources().getDisplayMetrics().widthPixels/2);
        stateET.setDropDownHeight(getResources().getDisplayMetrics().widthPixels/2);
        countryET.setThreshold(1);
        countryET.setAdapter(countryAdapter);

        countryET.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                countryET.setText(tempCountryList.get(position));
                KeyboardUtilities.hideSoftKeyboard(parentActivity);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        // Add Text Change Listener to EditText
        countryET.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // Call back the Adapter with current character to Filter
                countryAdapter.getFilter().filter(s.toString());
                String countryName = s.toString();
                if(isCountryExist(countryName)){
                    //remove the list when the country changes
                    getStateFromCountryList.clear();
                    stateET.setText("");
                    String countryId = getCountryId(countryName);
                    getStateFromCountry(countryId);
                }
                String tempCountryCode = getCountryCodeWithName(countryName);
                String tempSortName = getCountryCodeWithSortName(countryName);
                if(tempSortName != null && tempCountryCode != null){
                    String sortNameCode = tempSortName + " +" + tempCountryCode;
                    countryCodeATV.setText(sortNameCode);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().isEmpty()){
                    //remove the list when the country changes
                    getStateFromCountryList.clear();
                    final YupaCustomArrayAdapterMethodTwo countryAdapter = new YupaCustomArrayAdapterMethodTwo(parentActivity,android.R.layout.simple_dropdown_item_1line,tempCountryList);
                    countryET.setThreshold(1);
                    countryET.setAdapter(countryAdapter);
                    countryET.showDropDown();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,int after) {
            }

        });

        countryET.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                KeyboardUtilities.hideSoftKeyboard(parentActivity);
//                contactET.requestFocus();
//                KeyboardUtilities.showSoftKeyboard(parentActivity,contactET);
            }
        });

        showMoreCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ifSoftKeyboardOpen()){
                    KeyboardUtilities.hideSoftKeyboard(parentActivity);
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        focusOnView(countryET);
                        countryET.requestFocus();
                        KeyboardUtilities.showSoftKeyboard(parentActivity,countryET);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                countryET.showDropDown();
                            }
                        },300);
                    }
                },100);

            }
        });

//////////////////////////////////////////////////////////////////////////////////// initialize state here ////////////////////////////////////////////////////////////////////////////////////

        stateET.setThreshold(1);
        stateET.setAdapter(stateAdapter);
        stateAdapter.setNotifyOnChange(true);

        stateET.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                stateET.setText(getStateFromCountryList.get(position));
                KeyboardUtilities.hideSoftKeyboard(parentActivity);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        // Add Text Change Listener to EditText
        stateET.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // Call back the Adapter with current character to Filter
                stateAdapter.getFilter().filter(s.toString());
                String stateName = s.toString();

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().isEmpty()){
                    final YupaCustomArrayAdapterMethodTwo stateAdapter = new YupaCustomArrayAdapterMethodTwo(parentActivity,android.R.layout.simple_dropdown_item_1line,getStateFromCountryList);
                    stateET.setThreshold(1);
                    stateET.setAdapter(stateAdapter);
                    stateET.showDropDown();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,int after) {
            }

        });

        stateET.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                KeyboardUtilities.hideSoftKeyboard(parentActivity);
            }
        });

        showMoreState.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ifSoftKeyboardOpen()){
                    KeyboardUtilities.hideSoftKeyboard(parentActivity);
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        focusOnView(stateET);
                        stateET.requestFocus();
                        KeyboardUtilities.showSoftKeyboard(parentActivity,stateET);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                stateET.showDropDown();
                            }
                        },300);
                    }
                },100);
            }
        });
        //////////////////////////////////////////////////////////////////////////////////// initialize country code here ////////////////////////////////////////////////////////////////////////////////////

        final YupaCustomArrayAdapterMethodTwo countryCodeAdapter = new YupaCustomArrayAdapterMethodTwo(parentActivity,android.R.layout.simple_dropdown_item_1line,tempCountrySortNameCodeList);
        countryCodeATV.setDropDownHeight(getResources().getDisplayMetrics().widthPixels/2);
        countryCodeATV.setThreshold(1);
        countryCodeATV.setAdapter(countryCodeAdapter);
        showMoreCountryCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        KeyboardUtilities.showSoftKeyboard(parentActivity,countryCodeATV);
                        countryCodeATV.showDropDown();
                    }
                },200);

            }
        });

        countryCodeATV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                KeyboardUtilities.hideSoftKeyboard(parentActivity);
                contactET.requestFocus();
                KeyboardUtilities.showSoftKeyboard(parentActivity,contactET);
            }
        });


        // Add Text Change Listener to EditText
        countryCodeATV.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // Call back the Adapter with current character to Filter
                countryCodeAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().isEmpty()){
                    final YupaCustomArrayAdapterMethodTwo countryAdapter = new YupaCustomArrayAdapterMethodTwo(parentActivity,android.R.layout.simple_dropdown_item_1line,tempCountrySortNameCodeList);
                    countryCodeATV.setThreshold(1);
                    countryCodeATV.setAdapter(countryAdapter);
                    countryCodeATV.showDropDown();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,int after) {
            }

        });

        countryCodeATV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                KeyboardUtilities.hideSoftKeyboard(parentActivity);
                String getCountryCode = countryCodeATV.getText().toString();
                countryCodeATV.setText(getCountryCode);
                contactET.requestFocus();
                KeyboardUtilities.showSoftKeyboard(parentActivity,contactET);

            }
        });
    }

    private final void focusOnView(final AutoCompleteTextView autoCompleteTextView){
        editProfileSV.post(new Runnable() {
            @Override
            public void run() {
                editProfileSV.scrollTo(0, autoCompleteTextView.getBottom());
            }
        });
    }

    private boolean isCountryExist(String countryName){
        Boolean isExist = null;
        if(tempCountryList != null){
           for(String mCountryName : tempCountryList){
               if(countryName.equals(mCountryName)){
                   isExist = true;
               }
           }
        }
        if(isExist != null){
            return true;
        }
        else{
            return false;
        }
    }

    private void getCountryManuallyOnFirstTime(){
        String countryName = countryET.getText().toString();
        if(isCountryExist(countryName)){
            String countryId = getCountryId(countryName);
            getStateFromCountry(countryId);
        }
    }

    private boolean ifSoftKeyboardOpen(){

        editProfileRoot.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int heightDiff = editProfileRoot.getRootView().getHeight() - editProfileRoot.getHeight();
                if (heightDiff > dpToPx(parentActivity, 200)) { // if more than 200 dp, it's probably a keyboard...
                    // ... do something here
                    isOpen = true;
                }
                else{
                    isOpen = false;
                }
            }
        });
        if(isOpen != null && isOpen){
            return true;
        }
        else{
            return false;
        }
    }

    public static float dpToPx(Context context, float valueInDp) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueInDp, metrics);
    }
    public boolean isPhoneNumberValid(String phoneNumber, String countryCode)
    {
        //NOTE: This should probably be a member variable.
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();

        try {
            if(countryCode == null){
                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getResources().getString(R.string.error_country_code_not_found),R.drawable.notice_bad,TAG);
                return false;
            }
            else{
                Phonenumber.PhoneNumber numberProto = phoneUtil.parse(phoneNumber, countryCode);
                if(!phoneUtil.isValidNumber(numberProto)){
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getResources().getString(R.string.error_country_code_not_match_with_country),R.drawable.notice_bad,TAG);
                }
                return phoneUtil.isValidNumber(numberProto);
            }
        }
        catch (NumberParseException e) {
            System.err.println("NumberParseException was thrown: " + e.toString());
            Log.e(TAG,e.toString());
            GeneralFunction.openMessageNotificationDialog(parentActivity,e.toString(),R.drawable.notice_bad,TAG);
        }

        return false;
    }

    private String getCountryCodeWithName(String countryName){

        String countryCode = null;

        if(tempCountryCodeMap != null){
            for (Map.Entry<String, String> mapEntry : tempCountryCodeMap.entrySet()){
                String key = mapEntry.getKey();
                String value = mapEntry.getValue();
                if(key.equals(countryName)){
                    countryCode = value;
                }
            }
            return countryCode;
        }
        else{
            return null;
        }
    }

    private String getCountryCodeWithSortName(String countryName){

        String countryCode = null;

        if(tempCountrySortNameMap != null){
            for (Map.Entry<String, String> mapEntry : tempCountrySortNameMap.entrySet()){
                String key = mapEntry.getKey();
                String value = mapEntry.getValue();
                if(key.equals(countryName)){
                    countryCode = value;
                }
            }
            return countryCode;
        }
        else{
            return null;
        }
    }

    private void getCountryNameAndCode(){

        ArrayList<HashMap<String,String>> allCountriesMap = getCountriesFromJSONFile();
        String name = null;
        String code = null;
        for (HashMap<String, String> map : allCountriesMap){
            for (Map.Entry<String, String> mapEntry : map.entrySet())
            {
                String key = mapEntry.getKey();
                String value = mapEntry.getValue();
                if(key.equals(Config.COUNTRY_NAME)){
                    name = value;
                }
                if(key.equals(Config.COUNTRY_CODE)){
                    code = value;
                    tempCountryCodeList.add(value);
                }
                if(name != null && code != null){
                    tempCountryCodeMap.put(name,code);
                    name = null;
                    code = null;
                }
            }
        }
    }

    private void getCountrySortNameAndCode(){
        ArrayList<HashMap<String,String>> allCountriesMap = getCountriesFromJSONFile();
        String code = null;
        String sortName = null;
        for (HashMap<String, String> map : allCountriesMap){
            for (Map.Entry<String, String> mapEntry : map.entrySet())
            {
                String key = mapEntry.getKey();
                String value = mapEntry.getValue();
                if(key.equals(Config.COUNTRY_CODE)){
                    code = value;
                    tempCountryCodeList.add(value);
                }
                if(key.equals(Config.COUNTRY_SORT_NAME)){
                    sortName = value;
                }
                if(sortName != null && code != null){
                    String sortNameCode = sortName + " +" + code;
                    tempCountrySortNameCodeList.add(sortNameCode);
                    tempCountrySortNameAndCodeMap.put(sortNameCode,sortName);
                    code = null;
                    sortName = null;
                }
            }
        }
    }

    private void getCountryNameAndSortName(){
        ArrayList<HashMap<String,String>> allCountriesMap = getCountriesFromJSONFile();
        String name = null;
        String sortName = null;
        for (HashMap<String, String> map : allCountriesMap){
            for (Map.Entry<String, String> mapEntry : map.entrySet())
            {
                String key = mapEntry.getKey();
                String value = mapEntry.getValue();
                if(key.equals(Config.COUNTRY_NAME)){
                    name = value;
                }
                if(key.equals(Config.COUNTRY_SORT_NAME)){
                    sortName = value;
                }
                if(name != null && sortName != null){
                    tempCountrySortNameMap.put(name,sortName);
                    name = null;
                    sortName = null;
                }
            }
        }
    }

    private String getSortNameByCode(){
        String sortName = null;
        for (Map.Entry<String, String> mapEntry : tempCountrySortNameAndCodeMap.entrySet())
        {
            String key = mapEntry.getKey();
            String value = mapEntry.getValue();
            if(key.equals(countryCodeATV.getText().toString())){
                sortName = value;
            }
        }
        return sortName;
    }

    private boolean isHasThisCountry(String country){
        if(tempCountryList != null){
            if(!tempCountryList.contains(country.trim())){
                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_select_valid_country),R.drawable.notice_bad,TAG);
                return false;
            }else{
                return true;
            }
        }else{
            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_occured),R.drawable.notice_bad,TAG);
            return false;
        }
    }

    private boolean isHasThisCountryCode(String countryCode){
        if(tempCountryCodeList != null){
            if(!countryCode.contains(countryCode.trim())){
                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_select_valid_country_code),R.drawable.notice_bad,TAG);
                return false;
            }else{
                return true;
            }
        }else{
            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_occured),R.drawable.notice_bad,TAG);
            return false;
        }
    }

    private boolean isHasThisState(String state){
        if(getStateFromCountryList != null){
            if(!getStateFromCountryList.contains(state.trim())){
                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_select_valid_state),R.drawable.notice_bad,TAG);
                return false;
            }else{
                return true;
            }
        }else{
            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_occured),R.drawable.notice_bad,TAG);
            return false;
        }
    }

    private boolean isValidInput(){
        return  isHasThisCountry(countryET.getText().toString().trim())
                &&
                isHasThisState(stateET.getText().toString().trim())
                &&
                isHasThisCountryCode(countryCodeATV.getText().toString().trim())
                &&
                isPhoneNumberValid(contactET.getText().toString().trim(),getSortNameByCode());
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.selectHighlights();
        parentActivity.setToolbarTitle(parentActivity.getString(R.string.editProfile));
    }
}