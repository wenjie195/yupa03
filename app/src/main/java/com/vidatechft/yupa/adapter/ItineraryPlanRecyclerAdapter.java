package com.vidatechft.yupa.adapter;

import android.app.TimePickerDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.net.Uri;
import android.provider.CalendarContract;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.Itinerary;
import com.vidatechft.yupa.classes.Plan;
import com.vidatechft.yupa.itinerary.AddNewPlanFragment;
import com.vidatechft.yupa.itinerary.ItineraryPlanTabPager;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

public class ItineraryPlanRecyclerAdapter extends RecyclerView.Adapter<ItineraryPlanRecyclerAdapter.ViewHolder>{
    public static final String TAG = ItineraryPlanRecyclerAdapter.class.getName();
    private MainActivity parentActivity;
    private ItineraryPlanTabPager parentFragment;
    private Itinerary itinerary;
    private int day;
    private ArrayList<Plan> plans;
    private boolean isEditingMode;

    public ItineraryPlanRecyclerAdapter(MainActivity activity, ItineraryPlanTabPager parentFragment, Itinerary itinerary, int day, ArrayList<Plan> plans, boolean isEditingMode) {
        this.parentActivity = activity;
        this.parentFragment = parentFragment;
        this.itinerary = itinerary;
        this.day = day;
        this.plans = plans;
        this.isEditingMode = isEditingMode;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView timeTV,locationTV,distanceTV,activityTV;
        public ConstraintLayout btnCL,wholeCL;
        public ImageView editIV,deleteIV;
        public View timeClickView;

        public ViewHolder(View v) {
            super(v);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    timeTV = itemView.findViewById(R.id.timeTV);
                    locationTV = itemView.findViewById(R.id.locationTV);
                    distanceTV = itemView.findViewById(R.id.distanceTV);
                    activityTV = itemView.findViewById(R.id.activityTV);
                    btnCL = itemView.findViewById(R.id.btnCL);
                    wholeCL = itemView.findViewById(R.id.wholeCL);
                    editIV = itemView.findViewById(R.id.editIV);
                    deleteIV = itemView.findViewById(R.id.deleteIV);
                    timeClickView = itemView.findViewById(R.id.timeClickView);
                }
            });
        }
    }

    @NonNull
    @Override
    public ItineraryPlanRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.itinerary_add_plan_view, parent, false);

        return new ItineraryPlanRecyclerAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int pos) {
        final int position = pos;
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Plan plan = plans.get(position);

                parentActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if(isEditingMode){
                            holder.btnCL.setVisibility(View.VISIBLE);
                        }else{
                            holder.btnCL.setVisibility(View.GONE);
                        }

                        if(plan.activityName != null){
                            holder.activityTV.setText(plan.activityName);
                        }

                        if(plan.startTime != null){
                            Date date = new Date();
                            date.setTime(plan.startTime);
                            holder.timeTV.setText(GeneralFunction.formatDateToTime(date));
                        }

                        if(plan.placeAddress != null && plan.placeAddress.placeName != null){
                            holder.locationTV.setText(plan.placeAddress.placeName);
                        }else{
                            holder.locationTV.setText("");
                        }

                        if(plan.transportMode != null){
                            switch (plan.transportMode){
                                case Plan.MODE_CAR:
                                    holder.distanceTV.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(parentActivity,R.drawable.ic_car),null,null,null);
                                    holder.distanceTV.setText(parentActivity.getString(R.string.itinerary_planning_transport_driving));
                                    break;
                                case Plan.MODE_BUS:
                                    holder.distanceTV.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(parentActivity,R.drawable.ic_bus),null,null,null);
                                    holder.distanceTV.setText(parentActivity.getString(R.string.itinerary_planning_transport_public));
                                    break;
                                case Plan.MODE_WALK:
                                    holder.distanceTV.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(parentActivity,R.drawable.ic_walk),null,null,null);
                                    holder.distanceTV.setText(parentActivity.getString(R.string.itinerary_planning_transport_walking));
                                    break;
                                case Plan.MODE_HAIL:
                                    holder.distanceTV.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(parentActivity,R.drawable.ic_hail),null,null,null);
                                    holder.distanceTV.setText(parentActivity.getString(R.string.itinerary_planning_transport_eHailing));
                                    break;
                                default:
                                    holder.distanceTV.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(parentActivity,R.drawable.ic_walk),null,null,null);
                                    holder.distanceTV.setText(parentActivity.getString(R.string.itinerary_planning_transport_walking));
                                    break;
                            }
                        }

                        holder.wholeCL.setOnClickListener(new DebouncedOnClickListener(500) {
                            @Override
                            public void onDebouncedClick(View v) {
                                if(plan.gps != null){
                                    GeneralFunction.openMapApplicationViaGps(parentActivity,plan.gps.getLatitude(),plan.gps.getLongitude(),plan.placeAddress.placeName);
                                }else{
                                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.no_location_provided),R.drawable.notice_bad,TAG);
                                }
                            }
                        });

                        holder.editIV.setOnClickListener(new DebouncedOnClickListener(500) {
                            @Override
                            public void onDebouncedClick(View v) {
                                if(parentActivity != null && !parentActivity.isFinishing() && GeneralFunction.isAppInForground(parentActivity.getApplicationContext())){
                                    FragmentManager fm = parentActivity.getSupportFragmentManager();
                                    AddNewPlanFragment addNewPlanFragment = AddNewPlanFragment.newInstance(parentActivity,itinerary, day, plan);
                                    addNewPlanFragment.show(fm, TAG);
                                }
                            }
                        });

                        holder.deleteIV.setOnClickListener(new DebouncedOnClickListener(500) {
                            @Override
                            public void onDebouncedClick(View v) {
                                final int eventId = plan.eventId;

                                if(itinerary.quotationStatus != null && TextUtils.equals(itinerary.quotationStatus,Itinerary.STATUS_DONE)){
                                    itinerary.quotationStatus = null;
                                    itinerary.isFromAdmin = null;
                                    HashMap<String,Object> map = new HashMap<>();
                                    map.put(Itinerary.QUOTATION_STATUS,null);
                                    map.put(Itinerary.IS_FROM_ADMIN,null);
                                    GeneralFunction.getFirestoreInstance()
                                            .collection(Itinerary.URL_ITINERARY)
                                            .document(itinerary.id)
                                            .update(map);
                                    parentFragment.disableBuyNowButton();
                                }

                                GeneralFunction.getFirestoreInstance()
                                        .collection(Itinerary.URL_ITINERARY)
                                        .document(itinerary.id)
                                        .collection(Plan.URL_PLAN)
                                        .document(plan.id)
                                        .delete()
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                if(eventId > 0){
                                                    ContentResolver cr = parentActivity.getContentResolver();
                                                    Uri deleteUri;
                                                    deleteUri = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, eventId);
                                                    cr.delete(deleteUri, null, null);
                                                }
                                            }
                                        });
                                plans.remove(position);
                                notifyDataSetChanged();
                            }
                        });

                        holder.timeClickView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showHourPicker(plan);
                            }
                        });
                    }
                });
            }
        });
    }

    public void setEditingMode(boolean isEditingMode){
        this.isEditingMode = isEditingMode;
    }

    private void showHourPicker(final Plan plan) {
        Calendar myCalender = new GregorianCalendar();
        if(plan != null && plan.startTime != null){
            myCalender.setTimeInMillis(plan.startTime);
        }
        int hour = myCalender.get(Calendar.HOUR_OF_DAY);
        int minute = myCalender.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = new TimePickerDialog(parentActivity,
                android.R.style.Theme_Holo_Light_Dialog_NoActionBar,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        Calendar cal = new GregorianCalendar();
                        cal.set(1994,11,13,hourOfDay,minute,0);

                        if(plan != null && plan.id != null){
                            GeneralFunction.getFirestoreInstance()
                                    .collection(Itinerary.URL_ITINERARY)
                                    .document(itinerary.id)
                                    .collection(Plan.URL_PLAN)
                                    .document(plan.id)
                                    .update(Plan.START_TIME,cal.getTimeInMillis());
                        }
                    }
                },
                hour, minute, false);

        if(timePickerDialog.getWindow() != null){
            timePickerDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            timePickerDialog.show();
        }
    }

    @Override
    public int getItemCount() {
        return plans.size();
    }
}