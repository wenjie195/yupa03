package com.vidatechft.yupa.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.Job;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.HashMap;
import java.util.List;

public class PTJobDetailListAdapter extends RecyclerView.Adapter<PTJobDetailListAdapter.ViewHolder> {
    public final String TAG = PTJobDetailListAdapter.class.getName();
    private List<String> descSet;
    private List<Integer> iconSet;
    private String mId;
    private int colour;

    private MainActivity parentActivity;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView descTV;
        public ImageView iconIV;

        public ViewHolder(View v) {
            super(v);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    descTV = itemView.findViewById(R.id.descTV);
                    iconIV = itemView.findViewById(R.id.iconIV);
                }
            });
        }
    }

    public PTJobDetailListAdapter(MainActivity parentActivity, List<String> descSet, List<Integer> iconSet, int colour) {
        this.parentActivity = parentActivity;
        this.descSet = descSet;
        this.iconSet = iconSet;
        this.colour = colour;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_pt_detail_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int pos) {
        final int position = pos;
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!descSet.isEmpty() && descSet.get(position) != null) {
                    holder.descTV.setText(descSet.get(position));
                    holder.descTV.setTextColor(parentActivity.getResources().getColor(colour));
                }

                if (!iconSet.isEmpty() && iconSet.get(position) != null) {
                    holder.iconIV.setImageDrawable(GeneralFunction.getTintedDrawable(parentActivity,iconSet.get(position),colour));
                }else{
                    holder.iconIV.setImageDrawable(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.ic_not_found,colour));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return descSet.size();
    }

}