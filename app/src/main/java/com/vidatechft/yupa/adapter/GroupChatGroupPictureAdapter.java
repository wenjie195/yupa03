package com.vidatechft.yupa.adapter;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
//import com.vidatechft.yupa.chatFragments.GroupChatViewFullImageFragment;
import com.vidatechft.yupa.chatFragments.GroupChatViewImageFragment;
import com.vidatechft.yupa.classes.Message;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GroupChatGroupPictureAdapter extends RecyclerView.Adapter<GroupChatGroupPictureAdapter.ViewHolder>
{
    public static final String TAG = GroupChatGroupPictureAdapter.class.getName();
    private MainActivity parentActivity;
    private Fragment parentFragment;
    private List<Message> groupChatGroupPicList;
    private ArrayList<String> showcasePic;


    public GroupChatGroupPictureAdapter(MainActivity parentActivity, Fragment parentFragment, List<Message> groupChatGroupPicList)
    {
        this.parentActivity = parentActivity;
        this.parentFragment = parentFragment;
        this.groupChatGroupPicList = groupChatGroupPicList;
    }

    @NonNull
    @Override
    public GroupChatGroupPictureAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_grip_group_show_photos, parent, false);
        return new GroupChatGroupPictureAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final GroupChatGroupPictureAdapter.ViewHolder holder, final int position)
    {
        //Todo If the fragment is from GroupChatMediaFragment
        if(groupChatGroupPicList != null)
        {
            parentActivity.runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    final Message tempMessage = groupChatGroupPicList.get(position);
                    //Toast.makeText(parentActivity, "", Toast.LENGTH_SHORT).show();
                    parentActivity.runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            if (tempMessage != null)
                            {
                                if (tempMessage.img != null)
                                {
                                    GeneralFunction.GlideImageSetting(parentActivity,Uri.parse(tempMessage.img),holder.picIV);
                                }
                                Date now = tempMessage.dateCreated.toDate();
                                SimpleDateFormat dateFormatter = new SimpleDateFormat("d-M-y 'at' h:m");
                                holder.timestampTV.setText(dateFormatter.format(now).toString());

                                holder.picIV.setOnClickListener(new View.OnClickListener()
                                {
                                    @Override
                                    public void onClick(View v)
                                    {
                                        Fragment groupChatViewImage = GroupChatViewImageFragment.newInstance(parentActivity,tempMessage,position , groupChatGroupPicList);
                                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, groupChatViewImage).addToBackStack(GroupChatViewImageFragment.TAG).commit();
                                    }
                                });
                            }
                        }
                    });
                }
            });
        }

    }

    @Override
    public int getItemCount()
    {
        return groupChatGroupPicList.size() ;
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public ImageView picIV;
        public TextView timestampTV;

        public ViewHolder(View v)
        {
            super(v);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    picIV = itemView.findViewById(R.id.picIV);
                    timestampTV = itemView.findViewById(R.id.timestampTV);
                }
            });
        }
    }
}
