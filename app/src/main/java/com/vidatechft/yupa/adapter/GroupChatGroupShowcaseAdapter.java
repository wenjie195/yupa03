package com.vidatechft.yupa.adapter;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
//import com.vidatechft.yupa.chatFragments.GroupChatViewFullImageFragment;
import com.vidatechft.yupa.chatFragments.GroupChatViewImageFragment;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;

public class GroupChatGroupShowcaseAdapter extends RecyclerView.Adapter<GroupChatGroupShowcaseAdapter.ViewHolder>
{
    public static final String TAG = GroupChatGroupShowcaseAdapter.class.getName();
    private MainActivity parentActivity;
    private Fragment parentFragment;
    private ArrayList<String> showcasePic;


    public GroupChatGroupShowcaseAdapter(MainActivity parentActivity, Fragment parentFragment,  ArrayList<String> showcasePic)
    {
        this.parentActivity = parentActivity;
        this.parentFragment = parentFragment;
        this.showcasePic = showcasePic;
    }

    @NonNull
    @Override
    public GroupChatGroupShowcaseAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_grip_group_show_photos, parent, false);
        return new GroupChatGroupShowcaseAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final GroupChatGroupShowcaseAdapter.ViewHolder holder, final int position)
    {
        //Todo If the fragment is from GroupChatMediaFragment
        if(showcasePic != null)
        {
            parentActivity.runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    final String pictures = showcasePic.get(position);
                    //Toast.makeText(parentActivity, "", Toast.LENGTH_SHORT).show();
                    parentActivity.runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            if (pictures != null)
                            {
                                GeneralFunction.GlideImageSettingOwnPlaceholder(parentActivity,Uri.parse(pictures),holder.picIV,R.drawable.ic_group_placeholder);
                                holder.timestampTV.setVisibility(View.GONE);
                                holder.picIV.setOnClickListener(new View.OnClickListener()
                                {
                                    @Override
                                    public void onClick(View v)
                                    {
                                        Fragment groupChatViewImage = GroupChatViewImageFragment.newInstance(parentActivity,showcasePic, position);
                                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, groupChatViewImage).addToBackStack(GroupChatViewImageFragment.TAG).commit();
                                    }
                                });
                            }
                        }
                    });
                }
            });
        }

    }

    @Override
    public int getItemCount()
    {
        return showcasePic.size() ;
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public ImageView picIV;
        public TextView timestampTV;

        public ViewHolder(View v)
        {
            super(v);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    picIV = itemView.findViewById(R.id.picIV);
                    timestampTV = itemView.findViewById(R.id.timestampTV);
                }
            });
        }
    }
}

