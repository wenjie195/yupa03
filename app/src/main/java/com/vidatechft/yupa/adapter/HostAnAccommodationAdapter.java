package com.vidatechft.yupa.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.manuelpeinado.multichoiceadapter.MultiChoiceBaseAdapter;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.classes.GlideApp;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.classes.User;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import static com.bumptech.glide.load.DecodeFormat.PREFER_RGB_565;

public class HostAnAccommodationAdapter extends MultiChoiceBaseAdapter {
    public final String TAG = HostAnAccommodationAdapter.class.getName();
    private List<Room> rooms;
    private Activity parentActivity;
    private Context context;
    private Fragment fragment;
    public int num = 0;
    private GridView gridView;
    public static int ROW_NUMBER = 2;


    public HostAnAccommodationAdapter(Context context, Activity activity, Bundle savedInstanceState, List<Room> rooms, GridView gridView) {
        super(savedInstanceState);

        this.fragment = fragment;
        this.parentActivity = activity;
        this.context = context;
        this.rooms = rooms;
        this.gridView = gridView;
    }

    @Override
    protected View getViewImpl(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {

            int layout = R.layout.grid_layout_start_to_host;
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(layout, parent, false);
        }
        if (convertView != null) {
            ImageView startToHostGV = convertView.findViewById(R.id.startToHostGV);
            final TextView startToHostName = convertView.findViewById(R.id.startHostName);
            Room rooms = getItem(position);
            if (rooms.getUrlOutlook() != null && rooms.getAccommodationName() != null) {
                Log.d("asd", rooms.getAccommodationName());
                GlideApp.with(parentActivity).asBitmap().format(PREFER_RGB_565).load(rooms.getUrlOutlook()).diskCacheStrategy(DiskCacheStrategy.RESOURCE).placeholder(R.drawable.ic_placeholder).override(200, 200).centerCrop().error(R.drawable.ic_not_found).into(startToHostGV);
                startToHostName.setText(rooms.getAccommodationName());
            }
            Animation animation;
            if (position >= 2) {
                animation = AnimationUtils.loadAnimation(context, R.anim.up_from_bottom);
                animation.setDuration(340);
                convertView.startAnimation(animation);
            }


        }

        return convertView;
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        return true;
    }

    @Override
    public int getCount() {
        return rooms.size();
    }

    @Override
    public Room getItem(int position) {
        return rooms.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}


