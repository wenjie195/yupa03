package com.vidatechft.yupa.adapter;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.Job;
import com.vidatechft.yupa.employerFragments.PTJobMapFragment;
import com.vidatechft.yupa.employerFragments.PTJobSwipeFragment;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;

public class PTJobSwipeAdapter extends ArrayAdapter<Job> {
    public final String TAG = PTJobSwipeAdapter.class.getName();
    private PTJobSwipeFragment fragment;
    private MainActivity parentActivity;
    private String lastToolbarTitle;

    public PTJobSwipeAdapter(PTJobSwipeFragment fragment, MainActivity parentActivity, String lastToolbarTitle) {
        super(parentActivity, 0);
        this.fragment = fragment;
        this.parentActivity = parentActivity;
        this.lastToolbarTitle = lastToolbarTitle;
    }

    @NonNull
    @Override
    public View getView(int position, View contentView, @NonNull ViewGroup parent) {
        ViewHolder holder;

        if (contentView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            contentView = inflater.inflate(R.layout.adapter_pt_job_card, parent, false);
            holder = new ViewHolder(contentView);
            contentView.setTag(holder);
        } else {
            holder = (ViewHolder) contentView.getTag();
        }

        final ViewHolder finalHolder = holder;

        final Job job = getItem(position);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(job != null){

                    if(job.workspaceName != null){
                        finalHolder.workspaceTV.setText(job.workspaceName);
                    }

                    if(job.location != null){
                        finalHolder.locationTV.setText(job.location);
                    }

                    LinearLayoutManager jobscopeLLM = new LinearLayoutManager(parentActivity, LinearLayoutManager.HORIZONTAL, false);
                    LinearLayoutManager earnlistLLM = new LinearLayoutManager(parentActivity, LinearLayoutManager.HORIZONTAL, false);

                    if(job.jobScope != null && job.jobScopeIcon != null){
                        PTJobDetailListAdapter jobDetailListAdapter = new
                                PTJobDetailListAdapter(parentActivity,job.jobScope,job.jobScopeIcon,R.color.red);

                        finalHolder.workForRV.setAdapter(jobDetailListAdapter);
                        finalHolder.workForRV.setLayoutManager(jobscopeLLM);
                    }

                    if(job.earnList != null && job.earnListIcon != null){
                        PTJobDetailListAdapter jobDetailListAdapter = new
                                PTJobDetailListAdapter(parentActivity,job.earnList,job.earnListIcon,R.color.green);

                        finalHolder.earnRV.setAdapter(jobDetailListAdapter);
                        finalHolder.earnRV.setLayoutManager(earnlistLLM);
                    }

                }else{
                    Log.e(TAG,"error getting job in ptjobadapter");
                }

                finalHolder.dislikeIV.setOnClickListener(new DebouncedOnClickListener(500) {
                    @Override
                    public void onDebouncedClick(View v) {
                        fragment.swipeLeft();
                    }
                });

                finalHolder.likeIV.setOnClickListener(new DebouncedOnClickListener(500) {
                    @Override
                    public void onDebouncedClick(View v) {
                        fragment.swipeRight();
                    }
                });

                finalHolder.viewBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ArrayList<Job> jobs = new ArrayList<>();

                        for(int i = 0; i < getCount(); i++){
                            jobs.add(getItem(i));
                        }

                        Fragment ptjobMapFragment = PTJobMapFragment.newInstance(parentActivity,fragment,lastToolbarTitle,jobs);
                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, ptjobMapFragment).addToBackStack(PTJobMapFragment.TAG).commit();
                    }
                });
            }
        });

        return contentView;
    }

    private static class ViewHolder {
        public TextView locationTV,workspaceTV;
        public RecyclerView earnRV, workForRV;
        public ImageView likeIV, dislikeIV;
        public Button viewBtn;

        public ViewHolder(View view) {
            this.locationTV = view.findViewById(R.id.locationTV);
            this.workspaceTV = view.findViewById(R.id.workspaceTV);
            this.earnRV = view.findViewById(R.id.earnRV);
            this.workForRV = view.findViewById(R.id.workForRV);
            this.likeIV = view.findViewById(R.id.likeIV);
            this.dislikeIV = view.findViewById(R.id.dislikeIV);
            this.viewBtn = view.findViewById(R.id.viewBtn);
        }
    }

}
