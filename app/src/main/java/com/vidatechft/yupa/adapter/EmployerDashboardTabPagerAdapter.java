package com.vidatechft.yupa.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.employerFragments.EmployerCandidateFragment;
import com.vidatechft.yupa.employerFragments.EmployerPostedJobFragment;

public class EmployerDashboardTabPagerAdapter extends FragmentStatePagerAdapter {
    int noOfTabs;
    MainActivity parentActivity;
    String lastToolbarTitle;

    public EmployerDashboardTabPagerAdapter(MainActivity parentActivity, FragmentManager fm, int noOfTabs, String lastToolbarTitle) {
        super(fm);
        this.parentActivity = parentActivity;
        this.noOfTabs = noOfTabs;
        this.lastToolbarTitle = lastToolbarTitle;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return EmployerPostedJobFragment.newInstance(parentActivity,lastToolbarTitle);
            case 1:
                return EmployerCandidateFragment.newInstance(parentActivity,lastToolbarTitle);
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return noOfTabs;
    }
}
