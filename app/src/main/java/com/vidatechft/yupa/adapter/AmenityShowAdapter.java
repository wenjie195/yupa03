package com.vidatechft.yupa.adapter;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;

import java.util.ArrayList;
import java.util.List;

public class AmenityShowAdapter extends RecyclerView.Adapter<AmenityShowAdapter.ViewHolder> {

    private MainActivity parentActivity;
    private Fragment fragment;
    private List<String> amenityDataAll = new ArrayList<>();
    private List<String> limitAmenityData = new ArrayList<>();
    private boolean isViewAll = false;



    public AmenityShowAdapter(MainActivity activity, Fragment parentFragment, List<String>amenityDataAll, List<String>limitAmenityData, boolean isViewAll) {
        this.fragment = parentFragment;
        this.parentActivity = activity;
        this.amenityDataAll = amenityDataAll;
        this.limitAmenityData = limitAmenityData;
        this.isViewAll = isViewAll;

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView amenityTV;
        public LinearLayout wholeLL;

        public ViewHolder(View v) {
            super(v);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    amenityTV = itemView.findViewById(R.id.amenityTV);
                    wholeLL = itemView.findViewById(R.id.wholeLL);
                }
            });
        }
    }

    @NonNull
    @Override
    public AmenityShowAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_grid_amenity_show, parent, false);

        return new AmenityShowAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final AmenityShowAdapter.ViewHolder holder, final int position) {
        //show limit Data
        if(!isViewAll){
            String asd = limitAmenityData.get(position);
            holder.amenityTV.setText(asd);
        }
        else{
            String asd = amenityDataAll.get(position);
            holder.amenityTV.setText(asd);
        }
    }




    @Override
    public int getItemCount() {
        if(!isViewAll){
            return limitAmenityData.size();
        }
        else{
            return amenityDataAll.size();
        }
    }
}
