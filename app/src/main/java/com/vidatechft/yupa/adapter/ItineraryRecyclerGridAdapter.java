package com.vidatechft.yupa.adapter;

import android.content.DialogInterface;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.CurrencyRate;
import com.vidatechft.yupa.classes.Favourite;
import com.vidatechft.yupa.classes.Itinerary;
import com.vidatechft.yupa.classes.Search;
import com.vidatechft.yupa.itinerary.ItineraryDetailFragment;
import com.vidatechft.yupa.itinerary.StartPlanItineraryFragment;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.HashMap;

public class ItineraryRecyclerGridAdapter extends RecyclerView.Adapter<ItineraryRecyclerGridAdapter.ViewHolder>  {
    public static final String TAG = ItineraryRecyclerGridAdapter.class.getName();
    private ArrayList<Itinerary> itiList;
    private MainActivity parentActivity;
    private Fragment parentFragment;
    private boolean isOwnIti;
    private boolean allowUnfavourite;
    private boolean isFromMainActivity;
    private Search search;

    public int num = 0;

    public ItineraryRecyclerGridAdapter(MainActivity activity, Fragment parentFragment, ArrayList<Itinerary> itiList, boolean isOwnIti) {
        this.parentFragment = parentFragment;
        this.parentActivity = activity;
        this.itiList = itiList;
        this.isOwnIti = isOwnIti;
    }

    public ItineraryRecyclerGridAdapter(MainActivity activity, Fragment parentFragment, ArrayList<Itinerary> itiList, boolean isOwnIti, boolean allowUnfavourite) {
        this.parentFragment = parentFragment;
        this.parentActivity = activity;
        this.itiList = itiList;
        this.isOwnIti = isOwnIti;
        this.allowUnfavourite = allowUnfavourite;
    }

    public ItineraryRecyclerGridAdapter(MainActivity activity, Fragment parentFragment, ArrayList<Itinerary> itiList, boolean isOwnIti, boolean allowUnfavourite, boolean isFromMainActivity) {
        this.parentFragment = parentFragment;
        this.parentActivity = activity;
        this.itiList = itiList;
        this.isOwnIti = isOwnIti;
        this.allowUnfavourite = allowUnfavourite;
        this.isFromMainActivity = isFromMainActivity;
    }

    public ItineraryRecyclerGridAdapter(MainActivity activity, Fragment parentFragment, ArrayList<Itinerary> itiList, boolean isOwnIti, Search search) {
        this.parentFragment = parentFragment;
        this.parentActivity = activity;
        this.itiList = itiList;
        this.isOwnIti = isOwnIti;
        this.search = search;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public CardView wholeCV;
        public ImageView imageView;
        public TextView statusTV,textView;

        public ViewHolder(View v) {
            super(v);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    wholeCV = itemView.findViewById(R.id.wholeCV);
                    imageView = itemView.findViewById(R.id.imageView);
                    statusTV = itemView.findViewById(R.id.statusTV);
                    textView = itemView.findViewById(R.id.textView);
                }
            });
        }
    }

    @NonNull
    @Override
    public ItineraryRecyclerGridAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;

        if(isFromMainActivity){
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.adapter_horizontal_grid_itinerary, parent, false);
        }else{
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.adapter_grid_itinerary, parent, false);
        }


        return new ItineraryRecyclerGridAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int pos) {
        final int position = pos;
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Itinerary itinerary = itiList.get(position);

                parentActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(itinerary != null){
                            if(itinerary.itineraryPicUrl != null){
                                GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(itinerary.itineraryPicUrl),holder.imageView);
                            }else{
                                holder.imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                                GeneralFunction.GlideImageSettingOwnDrawable(parentActivity, R.drawable.ic_not_found,holder.imageView);
                            }

                            String statusTitle = "";
                            if(itinerary.isFromAdmin != null && itinerary.isFromAdmin){
                                String totalPrice = GeneralFunction.getItineraryPrice(parentActivity,itinerary,null) + " ";

                                if(itinerary.isPaid != null){
                                    if(itinerary.isPaid){
                                        statusTitle = "<strong><font color=\"" + Color.parseColor("#"+Integer.toHexString(parentActivity.getResources().getColor(R.color.booked_green))) +"\">"
                                                + totalPrice
                                                + parentActivity.getString(R.string.paid)
                                                + "</font></strong> ";
                                    }else{
                                        statusTitle = "<strong><font color=\"" + Color.parseColor("#"+Integer.toHexString(parentActivity.getResources().getColor(R.color.red))) +"\">"
                                                + totalPrice
                                                + parentActivity.getString(R.string.payment_failed)
                                                + "</font></strong> ";
                                    }
                                }else{
                                    if(itinerary.quotationStatus != null){
                                        switch (itinerary.quotationStatus){
                                            case Itinerary.STATUS_PENDING:
                                                statusTitle = "<strong><font color=\"" + Color.parseColor("#"+Integer.toHexString(parentActivity.getResources().getColor(R.color.pending_orange))) +"\">"
                                                        + totalPrice
                                                        + parentActivity.getString(R.string.itinerary_trip_quotation_pending)
                                                        + "</font></strong> ";
                                                break;
                                            case Itinerary.STATUS_REJECTED:
                                                statusTitle = "<strong><font color=\"" + Color.parseColor("#"+Integer.toHexString(parentActivity.getResources().getColor(R.color.red))) +"\">"
                                                        + totalPrice
                                                        + parentActivity.getString(R.string.itinerary_trip_quotation_rejected)
                                                        + "</font></strong> ";
                                                break;
                                            case Itinerary.STATUS_DONE:
                                                statusTitle = "<strong><font color=\"" + Color.parseColor("#"+Integer.toHexString(parentActivity.getResources().getColor(R.color.booked_green))) +"\">"
                                                        + totalPrice
                                                        + parentActivity.getString(R.string.itinerary_trip_quotation_done)
                                                        + "</font></strong> ";
                                                break;
                                            default:
                                                statusTitle = "<strong><font color=\"" + Color.parseColor("#"+Integer.toHexString(parentActivity.getResources().getColor(R.color.pending_orange))) +"\">"
                                                        + totalPrice
                                                        + parentActivity.getString(R.string.payment_pending)
                                                        + "</font></strong> ";
                                                break;
                                        }
                                    }else{
                                        statusTitle = "<strong><font color=\"" + Color.parseColor("#"+Integer.toHexString(parentActivity.getResources().getColor(R.color.pending_orange))) +"\">"
                                                + totalPrice
                                                + parentActivity.getString(R.string.payment_pending)
                                                + "</font></strong> ";
                                    }
                                }
                            }

                            String title = GeneralFunction.getItineraryTitle(itinerary);
                            if(title == null){
                                title = "???";
                            }
                            holder.textView.setText(title);

                            if(holder.statusTV != null){
                                if(!statusTitle.trim().isEmpty()){
                                    holder.statusTV.setText(Html.fromHtml(statusTitle));
                                    holder.statusTV.setVisibility(View.VISIBLE);
                                }else{
                                    holder.statusTV.setVisibility(View.GONE);
                                }
                            }

                            holder.wholeCV.setOnClickListener(new DebouncedOnClickListener(500) {
                                @Override
                                public void onDebouncedClick(View v) {
                                    if(isOwnIti && (itinerary.isFromAdmin == null || !itinerary.isFromAdmin) ){
                                        Itinerary.goToManageItineraryOrPlan(parentActivity,itinerary);
                                    }else{
                                        if( itinerary.quotationStatus != null && (itinerary.isPaid == null || !itinerary.isPaid) ){
                                            Itinerary.goToManageItineraryOrPlan(parentActivity,itinerary);
                                        }else{
                                            Fragment itineraryDetailFragment = ItineraryDetailFragment.newInstance(parentActivity,itinerary,search);
                                            GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, itineraryDetailFragment).addToBackStack(ItineraryDetailFragment.TAG).commit();
                                        }
                                    }
                                }
                            });

                            if(isOwnIti && (itinerary.isPaid == null || !itinerary.isPaid) ){
                                holder.wholeCV.setOnLongClickListener(new View.OnLongClickListener() {
                                    @Override
                                    public boolean onLongClick(View v) {
                                        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(parentActivity);
                                        alertDialogBuilder.setTitle(parentActivity.getString(R.string.itinierary_delete_title));
                                        alertDialogBuilder.setMessage(parentActivity.getString(R.string.itinierary_delete_desc));
                                        alertDialogBuilder.setPositiveButton(parentActivity.getString(R.string.btn_yes), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                GeneralFunction.getFirestoreInstance()
                                                        .collection(Itinerary.URL_ITINERARY)
                                                        .document(itinerary.id)
                                                        .delete();

                                                itiList.remove(position);
                                                notifyItemRemoved(position);

                                                dialog.dismiss();
                                            }
                                        });

                                        alertDialogBuilder.setNegativeButton(parentActivity.getString(R.string.btn_no), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        });

                                        android.support.v7.app.AlertDialog alertDialog  = alertDialogBuilder.create();
                                        if( !( parentActivity.isFinishing() && alertDialog.isShowing() ) ){
                                            alertDialog.show();
                                        }
                                        GeneralFunction.vibratePhone(parentActivity);
                                        return false;
                                    }
                                });
                            }

                            if(allowUnfavourite){
                                holder.wholeCV.setOnLongClickListener(new View.OnLongClickListener() {
                                    @Override
                                    public boolean onLongClick(View v) {
                                        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(parentActivity);
                                        alertDialogBuilder.setTitle(parentActivity.getString(R.string.itinierary_unfav_title));
                                        alertDialogBuilder.setMessage(parentActivity.getString(R.string.itinierary_unfav_desc));
                                        alertDialogBuilder.setPositiveButton(parentActivity.getString(R.string.btn_yes), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                DocumentReference favRef;

                                                if(itinerary.favourite != null && itinerary.favourite.id != null){
                                                    favRef = GeneralFunction.getFirestoreInstance()
                                                            .collection(Favourite.URL_FAVOURITE)
                                                            .document(itinerary.favourite.id);

                                                    itinerary.favourite.isLiked = false;

                                                    HashMap<String,Object> favMap = Favourite.toMap(itinerary.favourite);

                                                    if(itinerary.favourite.dateCreated == null){
                                                        favMap.put(Favourite.DATE_CREATED, FieldValue.serverTimestamp());
                                                    }
                                                    favMap.put(Favourite.DATE_UPDATED, FieldValue.serverTimestamp());

                                                    GeneralFunction.updateFavourite(parentActivity,favRef,favMap,itinerary.favourite,TAG);

                                                    itiList.remove(position);
                                                    notifyItemRemoved(position);
                                                }else{
                                                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_occured),R.drawable.notice_bad,TAG);
                                                }

                                                dialog.dismiss();
                                            }
                                        });

                                        alertDialogBuilder.setNegativeButton(parentActivity.getString(R.string.btn_no), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        });

                                        android.support.v7.app.AlertDialog alertDialog  = alertDialogBuilder.create();
                                        if( !( parentActivity.isFinishing() && alertDialog.isShowing() ) ){
                                            alertDialog.show();
                                        }
                                        GeneralFunction.vibratePhone(parentActivity);
                                        return false;
                                    }
                                });
                            }
                        }
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return itiList.size();
    }
}