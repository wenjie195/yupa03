package com.vidatechft.yupa.adapter;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.blog.AddBlogFragment;
import com.vidatechft.yupa.blog.BlogMainFragment;
import com.vidatechft.yupa.classes.Blog;
import com.vidatechft.yupa.classes.Itinerary;
import com.vidatechft.yupa.classes.PlanSuggestion;
import com.vidatechft.yupa.itinerary.AddNewPlanFragment;
import com.vidatechft.yupa.itinerary.ItineraryPlanTabPager;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;

public class PlanSuggestionRecyclerAdapter extends RecyclerView.Adapter<PlanSuggestionRecyclerAdapter.ViewHolder>  {
    public static final String TAG = PlanSuggestionRecyclerAdapter.class.getName();
    private MainActivity parentActivity;
    private ArrayList<PlanSuggestion> planSuggestions;
    private Itinerary itinerary;
    private int day;

    public PlanSuggestionRecyclerAdapter(MainActivity activity, ArrayList<PlanSuggestion> planSuggestions, Itinerary itinerary, int day) {
        this.parentActivity = activity;
        this.planSuggestions = planSuggestions;
        this.itinerary = itinerary;
        this.day = day;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView activityTV;
        public ConstraintLayout wholeCL;
        public ImageView addIV;
        public View marginTopView,marginBottomView;

        public ViewHolder(View v) {
            super(v);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    wholeCL = itemView.findViewById(R.id.wholeCL);
                    activityTV = itemView.findViewById(R.id.activityTV);
                    addIV = itemView.findViewById(R.id.addIV);
                    marginTopView = itemView.findViewById(R.id.marginTopView);
                    marginBottomView = itemView.findViewById(R.id.marginBottomView);
                }
            });
        }
    }

    @NonNull
    @Override
    public PlanSuggestionRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_plan_suggestion, parent, false);

        return new PlanSuggestionRecyclerAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int pos) {
        final int position = pos;

        if(pos == 0){
            holder.marginTopView.setVisibility(View.GONE);
        }else{
            holder.marginTopView.setVisibility(View.VISIBLE);
        }

        if(pos == getItemCount() - 1){
            holder.marginBottomView.setVisibility(View.GONE);
            holder.wholeCL.setBackgroundColor(parentActivity.getResources().getColor(R.color.white));
        }else{
            holder.marginBottomView.setVisibility(View.VISIBLE);
            holder.wholeCL.setBackground(parentActivity.getResources().getDrawable(R.drawable.square_black_border_btm));
        }

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final PlanSuggestion planSuggestion = planSuggestions.get(position);

                if(planSuggestion != null){
                    if(planSuggestion.activityName != null){
                        holder.activityTV.setText(planSuggestion.activityName);
                    }

//                    if(planSuggestion.time != null){
//                        holder.timeTV.setText(String.format(parentActivity.getString(R.string.itinerary_suggested_time),GeneralFunction.formatMillisecondToXHoursXMinutes(planSuggestion.time.getTime())));
//                    }
                    holder.addIV.setVisibility(View.GONE);
                }else{
                    holder.activityTV.setText(parentActivity.getString(R.string.plan_suggestion_create_own_custom_plan));
                    holder.addIV.setImageDrawable(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.ic_add,R.color.black));
                    holder.addIV.setVisibility(View.VISIBLE);
//                    holder.addIV.setVisibility(View.GONE);
                }

                holder.wholeCL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(planSuggestion != null && planSuggestion.activityName != null){
                            planSuggestion.activityName = planSuggestion.activityName.replaceAll("[\n\r]", "");
                        }

                        if(parentActivity != null && !parentActivity.isFinishing() && GeneralFunction.isAppInForground(parentActivity.getApplicationContext())){
                            final FragmentManager fm = parentActivity.getSupportFragmentManager();
                            AddNewPlanFragment addNewPlanFragment = AddNewPlanFragment.newInstance(parentActivity,itinerary, day, planSuggestion);
                            addNewPlanFragment.show(fm, TAG);
                        }
                    }
                });

            }
        });
    }

    @Override
    public int getItemCount() {
        return planSuggestions.size();
    }

    public void setDay(int day){
        this.day = day;
    }
}