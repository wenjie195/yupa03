package com.vidatechft.yupa.adapter;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.GroupChat;
import com.vidatechft.yupa.classes.User;
import com.vidatechft.yupa.profileFragments.ProfileFragment;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.Map;

import javax.annotation.Nullable;

public class MembershipAdapter extends RecyclerView.Adapter<MembershipAdapter.ViewHolder>
{
    public static final String TAG = MembershipAdapter.class.getName();
    private ArrayList<Map> membershipList;
    private MainActivity parentActivity;
    private String groupID;
    private Fragment parentFragment;
    FirebaseFirestore db = FirebaseFirestore.getInstance();

    public MembershipAdapter(MainActivity parentActivity , ArrayList<Map> membershipList , String groupID,Fragment parentFragment)
    {
        this.membershipList = membershipList;
        this.parentActivity = parentActivity;
        this.parentFragment = parentFragment;
        this.groupID = groupID;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent , int viewType)
    {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_membership, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final @NonNull MembershipAdapter.ViewHolder holder , int pos)
    {
        final int position = pos;
        parentActivity.runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                final Map pendingMembers = membershipList.get(position);
                parentActivity.runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        if(pendingMembers != null)
                        {
                            if (pendingMembers.get("userDetails") != null)
                            {
                                final User user = (User) pendingMembers.get("userDetails");
                                if(user.name != null)
                                {
                                    holder.nameTV.setText(user.name);
                                }
                                if(user.profilePicUrl != null){
                                    GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(user.profilePicUrl),holder.profilePicIV);
                                }
                                buttonListeners(holder,user,pendingMembers,position);

                            }
                            if(pendingMembers.get(GroupChat.GROUP_CHAT_DATE_CREATED) != null)
                            {
                                Timestamp ts = (Timestamp) pendingMembers.get(GroupChat.GROUP_CHAT_DATE_CREATED);
                                //                                holder.dateUpdatedTV.setText(GeneralFunction.formatDateToDateWithTime(ts.toDate()));
                                holder.dateUpdatedTV.setText(GeneralFunction.getTimeAgo(ts.getSeconds() * 1000));
                            }
                        }
                    }
                });
            }
        });
    }
    private void initializeProgressBar()
    {
        parentActivity.initializeProgressIndicator(parentActivity.getString(R.string.uploadEP_picture), false, parentFragment);
        parentActivity.controlProgressIndicator(true, parentFragment);
    }

    private void buttonListeners(ViewHolder holder , final User user , final Map pendingMembers , final int position)
    {

        holder.detailsLL.setOnClickListener(new DebouncedOnClickListener(1500)
        {
            @Override
            public void onDebouncedClick(View v)
            {
                Fragment profileFragment = ProfileFragment.newInstance(parentActivity,user,false);
                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, profileFragment).addToBackStack(ProfileFragment.TAG).commit();
            }
        });
        holder.approveBTN.setOnClickListener(new DebouncedOnClickListener(500)
        {
            @Override
            public void onDebouncedClick(View v)
            {

                initializeProgressBar();
                boolean isAllowed = true;
                pendingMembers.put(GroupChat.GROUP_CHAT_IS_ALLOWED,isAllowed);
                pendingMembers.remove("userDetails");




                db.collection(GroupChat.URL_GROUP_CHAT).document(groupID).collection(GroupChat.GROUP_CHAT_IS_PENDING).
                        document(user.id).set(pendingMembers)
                        .addOnSuccessListener(new OnSuccessListener()
                        {
                            @Override
                            public void onSuccess(Object o)
                            {
                                pendingMembers.clear();
                                membershipList.remove(position);
                                GeneralFunction.handleUploadSuccess(parentActivity, parentFragment, R.string.chat_group_add_user_success, TAG);
                                notifyItemRemoved(position);
                                notifyItemRangeChanged(position,getItemCount());
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e)
                            {
                                GeneralFunction.handleUploadError(parentActivity, parentFragment, R.string.chat_group_approve_error_membership_request, TAG);
                                parentActivity.onBackPressed();
                            }
                        });


            }
        });
        holder.rejectBTN.setOnClickListener(new DebouncedOnClickListener(500)
        {
            @Override
            public void onDebouncedClick(View v)
            {
                initializeProgressBar();
                boolean isAllowed = false;
                pendingMembers.put(GroupChat.GROUP_CHAT_IS_ALLOWED,isAllowed);
                pendingMembers.remove("userDetails");

                db.collection(GroupChat.URL_GROUP_CHAT).document(groupID).collection(GroupChat.GROUP_CHAT_IS_PENDING).
                        document(user.id).set(pendingMembers)
                        .addOnSuccessListener(new OnSuccessListener()
                        {
                            @Override
                            public void onSuccess(Object o)
                            {
                                pendingMembers.clear();
                                membershipList.remove(position);
                                GeneralFunction.handleUploadSuccess(parentActivity, parentFragment, R.string.chat_group_add_user_reject, TAG);
                                notifyItemRemoved(position);
                                notifyItemRangeChanged(position,getItemCount());
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e)
                            {
                                GeneralFunction.handleUploadError(parentActivity, parentFragment, R.string.chat_group_approve_error_membership_request, TAG);
                                parentActivity.onBackPressed();
                            }
                        });
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return membershipList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public ConstraintLayout containerCL;
        public ImageView profilePicIV;
        public LinearLayout detailsLL;
        public TextView nameTV,dateUpdatedTV;
        public Button approveBTN,rejectBTN;

        public ViewHolder(View v)
        {
            super(v);
            parentActivity.runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    containerCL = itemView.findViewById(R.id.containerCL);
                    profilePicIV = itemView.findViewById(R.id.profilePicIV);
                    detailsLL = itemView.findViewById(R.id.detailsLL);
                    nameTV = itemView.findViewById(R.id.nameTV);
                    dateUpdatedTV = itemView.findViewById(R.id.dateUpdatedTV);
                    approveBTN = itemView.findViewById(R.id.approveBTN);
                    rejectBTN = itemView.findViewById(R.id.rejectBTN);
                }
            });
        }
    }
}
