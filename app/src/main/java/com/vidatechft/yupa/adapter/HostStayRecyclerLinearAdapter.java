package com.vidatechft.yupa.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.classes.Search;
import com.vidatechft.yupa.hostFragments.HostStayMainPageFragment;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.List;

public class HostStayRecyclerLinearAdapter extends RecyclerView.Adapter<HostStayRecyclerLinearAdapter.ViewHolder> {
    private List<Room> hostList;
    private List<String> documentPushId;
    private boolean isOwnHost;
    private MainActivity parentActivity;
    private Context context;
    private Fragment fragment;
    private Search search;


    public HostStayRecyclerLinearAdapter(MainActivity activity, Fragment parentFragment, ArrayList<Room> hostList, ArrayList<String>documentPushId, boolean isOwnHost,Search search) {
        this.fragment = parentFragment;
        this.parentActivity = activity;
        this.hostList = hostList;
        this.isOwnHost = isOwnHost;
        this.documentPushId = documentPushId;
        this.search = search;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ConstraintLayout wholeCL;
        public ImageView hostIV;
        public TextView hostTV;

        public ViewHolder(View v) {
            super(v);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    wholeCL = itemView.findViewById(R.id.wholeCL);
                    hostIV = itemView.findViewById(R.id.hostIV);
                    hostTV = itemView.findViewById(R.id.hostTV);
                }
            });
        }
    }

    @NonNull
    @Override
    public HostStayRecyclerLinearAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_linear_host_stay, parent, false);

        return new HostStayRecyclerLinearAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final int pos = position;
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Room room = hostList.get(pos);
                final String descPushId = documentPushId.get(pos);

                parentActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(room != null){
                            if(room.urlOutlook != null){
                                GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(room.urlOutlook),holder.hostIV);
                            }else{
                                holder.hostIV.setScaleType(ImageView.ScaleType.FIT_XY);
                                GeneralFunction.GlideImageSettingOwnDrawable(parentActivity, R.drawable.ic_not_found,holder.hostIV);
                            }

                            GeneralFunction.setHostStayFullTitleWithPrice(parentActivity,room,holder.hostTV);

                            holder.wholeCL.setOnClickListener(new DebouncedOnClickListener(500) {
                                @Override
                                public void onDebouncedClick(View v) {
//                                    Toast.makeText(parentActivity,"asd",Toast.LENGTH_SHORT).show();
                                    Fragment hostStayMainPageFragment = HostStayMainPageFragment.newInstance(parentActivity,parentActivity.getString(R.string.stays),room,descPushId,search);
                                    GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, hostStayMainPageFragment).addToBackStack(HostStayMainPageFragment.TAG).commit();
                                }
                            });
                        }
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return hostList.size();
    }
}
