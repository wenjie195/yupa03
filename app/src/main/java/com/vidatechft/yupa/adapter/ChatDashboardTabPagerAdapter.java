package com.vidatechft.yupa.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.chatFragments.ChatContainerFragment;
import com.vidatechft.yupa.chatFragments.GroupChatFragment;
import com.vidatechft.yupa.chatFragments.PersonalChatListFragment;
import com.vidatechft.yupa.classes.Chat;
import com.vidatechft.yupa.employerFragments.EmployerCandidateFragment;
import com.vidatechft.yupa.employerFragments.EmployerPostedJobFragment;

public class ChatDashboardTabPagerAdapter extends FragmentStatePagerAdapter {
    private int noOfTabs;
    MainActivity parentActivity;
    ChatContainerFragment parentFragment;
    private String lastToolbarTitle;

    public ChatDashboardTabPagerAdapter(MainActivity parentActivity, ChatContainerFragment parentFragment, FragmentManager fm, int noOfTabs, String lastToolbarTitle) {
        super(fm);
        this.parentActivity = parentActivity;
        this.parentFragment = parentFragment;
        this.noOfTabs = noOfTabs;
        this.lastToolbarTitle = lastToolbarTitle;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return PersonalChatListFragment.newInstance(parentActivity,parentFragment,lastToolbarTitle, Chat.TYPE_PERSONAL_MSG);
            case 1:
                return GroupChatFragment.newInstance(parentActivity,parentFragment,lastToolbarTitle);
            case 2:
                return PersonalChatListFragment.newInstance(parentActivity,parentFragment,lastToolbarTitle, Chat.TYPE_JOB_DIRECT);
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return noOfTabs;
    }
}
