package com.vidatechft.yupa.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.BookingHistory;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.hostFragments.AccommodationDetailFragment;
import com.vidatechft.yupa.hostFragments.BookingRequestListFragment;
import com.vidatechft.yupa.hostFragments.HostStayMainPageFragment;
import com.vidatechft.yupa.hostFragments.PublishAccommodationStatusFragment;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.List;

import static com.vidatechft.yupa.classes.Room.TYPE_PUBLISHED;
import static com.vidatechft.yupa.classes.Room.TYPE_REJECTED;
import static com.vidatechft.yupa.classes.Room.TYPE_START_HOST;
import static com.vidatechft.yupa.classes.Room.TYPE_UNPUBLISHED;
import static com.vidatechft.yupa.classes.Room.TYPE_WAIT_VERIFY;

public class HostStayForManageRecyclerGridAdapter extends RecyclerView.Adapter<HostStayForManageRecyclerGridAdapter.ViewHolder> {
    private MainActivity parentActivity;
    private List<Room> rooms;
    private List<String>electricBillPushId;
    private String type;

    public HostStayForManageRecyclerGridAdapter(MainActivity parentActivity, List<Room> rooms,List<String>electricBillPushId, String type) {
        this.parentActivity = parentActivity;
        this.rooms = rooms;
        this.electricBillPushId = electricBillPushId;
        this.type = type;
    }

    public HostStayForManageRecyclerGridAdapter(MainActivity parentActivity, List<Room> rooms, String type) {
        this.parentActivity = parentActivity;
        this.rooms = rooms;
        this.electricBillPushId = electricBillPushId;
        this.type = type;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout wholeLL;
        public ImageView hostIV,notificationIV;
        public TextView hostTV,requestTV;

        public ViewHolder(View v) {
            super(v);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    wholeLL = itemView.findViewById(R.id.wholeLL);
                    hostIV = itemView.findViewById(R.id.hostIV);
                    notificationIV = itemView.findViewById(R.id.notificationIV);
                    hostTV = itemView.findViewById(R.id.hostTV);
                    requestTV = itemView.findViewById(R.id.requestTV);
                }
            });
        }
    }

    @NonNull
    @Override
    public HostStayForManageRecyclerGridAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_grid_host_stay_manage, parent, false);

        return new HostStayForManageRecyclerGridAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final int pos = position;
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Room room = rooms.get(pos);
//                final String electricBillUrl  = electricBillPushId.get(pos);
                parentActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(room != null){
                            if(room.urlOutlook != null){
                                GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(room.urlOutlook),holder.hostIV);
                            }else{
                                holder.hostIV.setScaleType(ImageView.ScaleType.FIT_XY);
                                GeneralFunction.GlideImageSettingOwnDrawable(parentActivity, R.drawable.ic_not_found,holder.hostIV);
                            }

                            GeneralFunction.setHostStayFullTitleWithPrice(parentActivity,room,holder.hostTV);

                            holder.wholeLL.setOnClickListener(new DebouncedOnClickListener(500) {
                                @Override
                                public void onDebouncedClick(View v) {
                                    switch(type){
                                        case TYPE_START_HOST:
                                            if(room.id != null){
                                                AccommodationDetailFragment fragment = AccommodationDetailFragment.newInstance(parentActivity,room,0, TYPE_START_HOST,room.id,null, true, false);
                                                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, fragment).addToBackStack(AccommodationDetailFragment.TAG).commit();
                                            }
                                            break;

                                        case TYPE_WAIT_VERIFY:
                                            Toast.makeText(parentActivity,parentActivity.getString(R.string.host_acc_pending),Toast.LENGTH_LONG).show();
                                            break;
                                        case TYPE_PUBLISHED:
                                            if(room.id != null && parentActivity.uid != null){
                                                Fragment publishAccommodationStatusFragment = PublishAccommodationStatusFragment.newInstance(parentActivity, room.id);
                                                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, publishAccommodationStatusFragment).addToBackStack(PublishAccommodationStatusFragment.TAG).commit();
                                            }
                                            break;
                                        case TYPE_UNPUBLISHED:
                                            if(room.id != null && parentActivity.uid != null){
                                                Fragment publishAccommodationStatusFragment = PublishAccommodationStatusFragment.newInstance(parentActivity, room.id);
                                                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, publishAccommodationStatusFragment).addToBackStack(PublishAccommodationStatusFragment.TAG).commit();
                                            }
                                            break;
                                        case TYPE_REJECTED:
                                            Toast.makeText(parentActivity,parentActivity.getString(R.string.host_acc_rejected),Toast.LENGTH_LONG).show();
                                            break;
                                    }
//                                    Fragment hostStayMainPageFragment = HostStayMainPageFragment.newInstance(parentActivity,parentActivity.getString(R.string.stays),room,room.id);
//                                    GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, hostStayMainPageFragment).addToBackStack(HostStayMainPageFragment.TAG).commit();
                                }
                            });

                            if(room.bookingHistories != null){
                                if(room.bookingHistories.size() > 1){
                                    holder.requestTV.setText(Html.fromHtml("<u>" + String.format(parentActivity.getString(R.string.number_booking_requests),room.bookingHistories.size()) + "</u>"));
                                }else{
                                    holder.requestTV.setText(Html.fromHtml("<u>" + String.format(parentActivity.getString(R.string.number_booking_request),room.bookingHistories.size()) + "</u>"));
                                }

                                holder.requestTV.setOnClickListener(new DebouncedOnClickListener(500) {
                                    @Override
                                    public void onDebouncedClick(View v) {
                                        for(int i = 0; i < room.bookingHistories.size(); i++){
                                            room.bookingHistories.get(i).room = room;
                                        }
                                        Fragment bookingRequestListFragment = BookingRequestListFragment.newInstance(parentActivity,room.bookingHistories);
                                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, bookingRequestListFragment).addToBackStack(BookingRequestListFragment.TAG).commit();
                                    }
                                });

                                holder.requestTV.setVisibility(View.VISIBLE);
                                holder.notificationIV.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return rooms.size();
    }
}
