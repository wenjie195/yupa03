package com.vidatechft.yupa.adapter;

import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.Itinerary;
import com.vidatechft.yupa.classes.Plan;
import com.vidatechft.yupa.itinerary.ItineraryPlanTabPager;
import com.vidatechft.yupa.itinerary.ItineraryPlanTabPagerDetails;
import com.vidatechft.yupa.itinerary.ItineraryPlansContainerFragment;
import com.vidatechft.yupa.utilities.YupaViewPager;

import java.util.ArrayList;

public class ItineraryPlanTabPagerAdapter extends FragmentStatePagerAdapter {
    private int noOfTabs;
    MainActivity parentActivity;
    private Fragment parentFragment;
    private Itinerary itinerary;
    private SparseArray<ArrayList<Plan>> planList;

    private ArrayList<ItineraryPlanTabPager> containerFragments = new ArrayList<>();
    private ArrayList<ItineraryPlanTabPagerDetails> detailsFragments = new ArrayList<>();

    private int mCurrentPosition = -1;

    public ItineraryPlanTabPagerAdapter(MainActivity parentActivity, Fragment parentFragment, FragmentManager fm, int noOfTabs, Itinerary itinerary, SparseArray<ArrayList<Plan>> planList) {
        super(fm);
        this.parentActivity = parentActivity;
        this.parentFragment = parentFragment;
        this.noOfTabs = noOfTabs;
        this.itinerary = itinerary;
        this.planList = planList;

        //to make it only initialize once
        if(parentFragment instanceof ItineraryPlansContainerFragment){
            ItineraryPlansContainerFragment tempFragment = (ItineraryPlansContainerFragment) parentFragment;
            for(int i = 0; i < noOfTabs; i++){
                containerFragments.add(ItineraryPlanTabPager.newInstance(parentActivity,tempFragment,itinerary,i,planList.get(i)));
            }
        }else{
            for(int i = 0; i < noOfTabs; i++){
                detailsFragments.add(ItineraryPlanTabPagerDetails.newInstance(parentActivity,parentFragment,itinerary,i,planList.get(i)));
            }
        }
    }

    @Override
    public Fragment getItem(int position) {
        if(parentFragment instanceof ItineraryPlansContainerFragment){
            containerFragments.get(position).onDatasetChanged(planList.get(position));
            return containerFragments.get(position);
        }else{
            detailsFragments.get(position).onDatasetChanged(planList.get(position));
            return detailsFragments.get(position);
        }
    }

    @Override
    public void setPrimaryItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        //this whole code is only needed for making each fragments in the pager to wrap contents to its contents respectively
        super.setPrimaryItem(container, position, object);
        if(!(parentFragment instanceof ItineraryPlansContainerFragment)){
            if (position != mCurrentPosition) {
                Fragment fragment = (Fragment) object;
                YupaViewPager pager = (YupaViewPager) container;
                if (fragment != null && fragment.getView() != null) {
                    mCurrentPosition = position;
                    pager.measureCurrentView(fragment.getView());
                }
            }
        }
    }

    @Override
    public int getCount() {
        return noOfTabs;
    }

    //the savestate and restore state is needed to preserve the state i think
    @Override
    public Parcelable saveState() {
        return null;
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {

    }
}
