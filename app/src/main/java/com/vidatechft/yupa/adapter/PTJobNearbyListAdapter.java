package com.vidatechft.yupa.adapter;

import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.AppliedCandidate;
import com.vidatechft.yupa.classes.Chat;
import com.vidatechft.yupa.classes.Job;
import com.vidatechft.yupa.dialogFragments.ApplyPTJobDialogFragment;
import com.vidatechft.yupa.employerFragments.PTJobSwipeFragment;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PTJobNearbyListAdapter extends RecyclerView.Adapter<PTJobNearbyListAdapter.ViewHolder>  {
    public static final String TAG = PTJobNearbyListAdapter.class.getName();

    private MainActivity parentActivity;
    private ArrayList<Job> jobDataset;
    private PTJobSwipeFragment parentFragment;
    private String lastToolbarTitle;

    public PTJobNearbyListAdapter(MainActivity parentActivity, PTJobSwipeFragment parentFragment, String lastToolbarTitle, ArrayList<Job> jobDataset) {
        this.parentActivity = parentActivity;
        this.parentFragment = parentFragment;
        this.lastToolbarTitle = lastToolbarTitle;
        this.jobDataset = jobDataset;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView jobscopeTV,earnTV;
        public ConstraintLayout wholeCL;

        public ViewHolder(View v) {
            super(v);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    jobscopeTV = itemView.findViewById(R.id.jobscopeTV);
                    earnTV = itemView.findViewById(R.id.earnTV);
                    wholeCL = itemView.findViewById(R.id.wholeCL);
                }
            });
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_ptjob_nearby_list, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int pos) {
        final int position = pos;
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Job job = jobDataset.get(position);

                holder.wholeCL.setOnClickListener(new DebouncedOnClickListener(500) {
                    @Override
                    public void onDebouncedClick(View v) {
                        if(job != null){
                            AppliedCandidate appliedCandidate = new AppliedCandidate(false,"", Chat.TYPE_JOB_DIRECT + job.employerId + parentActivity.uid);

                            if(parentActivity != null && !parentActivity.isFinishing() && GeneralFunction.isAppInForground(parentActivity.getApplicationContext())){
                                FragmentManager fm = parentActivity.getSupportFragmentManager();
                                ApplyPTJobDialogFragment applyPTJobDialogFragment = ApplyPTJobDialogFragment.newInstance(parentActivity,parentFragment,lastToolbarTitle,appliedCandidate,job,jobDataset,false);
                                applyPTJobDialogFragment.show(fm, TAG);
                            }
                        }
                    }
                });

                if(job != null){
                    if(job.jobScope != null){
                        holder.jobscopeTV.setText(GeneralFunction.formatList(job.jobScope,true));
                    }

                    if(job.earnList != null){
                        holder.earnTV.setText(GeneralFunction.formatList(job.earnList,true));
                    }

                }else{
                    Log.e(TAG,"error getting job in PTJobNearbyListAdapter");
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return jobDataset.size();
    }
}