package com.vidatechft.yupa.adapter;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.firestore.FieldValue;
import com.vidatechft.yupa.MainSearchFragment.SearchHostRoomFragment;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.Inbox;
import com.vidatechft.yupa.classes.Itinerary;
import com.vidatechft.yupa.classes.Plan;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.classes.User;
import com.vidatechft.yupa.hostFragments.HostStayMainPageFragment;
import com.vidatechft.yupa.itinerary.AddNewPlanFragment;
import com.vidatechft.yupa.profileFragments.ProfileFragment;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class InboxRecyclerAdapter extends RecyclerView.Adapter<InboxRecyclerAdapter.ViewHolder>  {
    public static final String TAG = InboxRecyclerAdapter.class.getName();
    private MainActivity parentActivity;
    private ArrayList<Inbox> inboxes;

    public InboxRecyclerAdapter(MainActivity parentActivity, ArrayList<Inbox> inboxes) {
        this.parentActivity = parentActivity;
        this.inboxes = inboxes;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView messageTV,timestampTV;
        public ImageView profileIV,bookingIV;
        public ConstraintLayout wholeCL;

        public ViewHolder(View v) {
            super(v);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    messageTV = itemView.findViewById(R.id.messageTV);
                    wholeCL = itemView.findViewById(R.id.wholeCL);
                    timestampTV = itemView.findViewById(R.id.timestampTV);
                    profileIV = itemView.findViewById(R.id.profileIV);
                    bookingIV = itemView.findViewById(R.id.bookingIV);
                }
            });
        }
    }

    @NonNull
    @Override
    public InboxRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_inbox, parent, false);

        return new InboxRecyclerAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int pos) {
        final int position = pos;
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Inbox inbox = inboxes.get(position);

                if(inbox == null){
//                    inboxes.remove(position);
//                    notifyDataSetChanged();
                    return;
                }

                parentActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if(inbox.inboxType != null){
                            switch (inbox.inboxType){
                                case Inbox.INBOX_TYPE_HOMESTAY:
                                    Room room = inbox.room;

                                    if(room != null){
                                        if(room.urlOutlook != null){
                                            GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(room.urlOutlook),holder.bookingIV);
                                            holder.bookingIV.setVisibility(View.VISIBLE);
                                        }

                                        //if actionType == null, it means pending for host to approve or reject
                                        if(inbox.actionType == null){
                                            if(inbox.otherUser != null && inbox.room != null){
                                                String userName = inbox.otherUser.name != null ? inbox.otherUser.name : "???";
                                                String roomName = inbox.room.accommodationName != null ? inbox.room.accommodationName : "???";
                                                holder.messageTV.setText(String.format(parentActivity.getString(R.string.homestay_booking_confirmation_alert_dialog_pending),userName,roomName));
                                                clickListenerForGoToUserProfile(holder,inbox,inbox.otherUser);

                                                //this should go to bookingRequestFragment instead or to a page where it has a list of request by users?
                                                //then in bookingRequestFragment there only can view that user's profile is better
                                            }else{
//                                                inboxes.remove(position);
//                                                notifyDataSetChanged();
                                            }
                                        }else{
                                            switch (inbox.actionType){
                                                case Inbox.ACTION_TYPE_BOOKING_PAID:
                                                    holder.messageTV.setText(String.format(parentActivity.getString(R.string.homestay_booking_confirmation_alert_dialog_paid), room.accommodationName));
                                                    clickListenerForGoToReceipt(holder,inbox);
                                                    break;
                                                case Inbox.ACTION_TYPE_BOOKING_ACCEPTED:
                                                    holder.messageTV.setText(String.format(parentActivity.getString(R.string.homestay_booking_confirmation_alert_dialog_approved), room.accommodationName));
                                                    clickListenerForGoToMyBookingPage(holder,inbox);
                                                    break;
                                                case Inbox.ACTION_TYPE_BOOKING_REJECTED:
                                                    holder.messageTV.setText(String.format(parentActivity.getString(R.string.homestay_booking_confirmation_alert_dialog_rejected), room.accommodationName));
                                                    clickListenerForGoToAccomDetails(holder,inbox,room);
                                                    break;
                                                case Inbox.ACTION_TYPE_BOOKING_CANCEL:
                                                    holder.messageTV.setText(String.format(parentActivity.getString(R.string.homestay_booking_confirmation_alert_dialog_canceled), room.accommodationName));
                                                    clickListenerForGoToAccomDetails(holder,inbox,room);
                                                    break;
                                                default:
                                                    holder.messageTV.setText(parentActivity.getString(R.string.error_occured));
                                                    clickListenerForGoToAccomDetails(holder,inbox,room);
                                                    break;
                                            }
                                        }
                                    }
                                    break;
                            }
                        }else{
//                            inboxes.remove(position);
//                            notifyDataSetChanged();
                            return;
                        }

                        if(inbox.isRead != null && inbox.isRead){
                            holder.wholeCL.setBackgroundColor(parentActivity.getResources().getColor(R.color.white));
                        }else{
                            holder.wholeCL.setBackgroundColor(parentActivity.getResources().getColor(R.color.unread_notification));
                        }

                        if(inbox.dateCreated != null){
                            //from here https://stackoverflow.com/questions/35858608/how-to-convert-time-to-time-ago-in-android
                            //from here https://stackoverflow.com/questions/25174921/time-ago-for-android-java
                            holder.timestampTV.setText(GeneralFunction.getTimeAgo(inbox.dateCreated.getSeconds() * 1000));
                        }
                    }
                });
            }
        });
    }

    private void clickListenerForGoToReceipt(final ViewHolder holder, final Inbox inbox){
        holder.wholeCL.setOnClickListener(new DebouncedOnClickListener(500) {
            @Override
            public void onDebouncedClick(View v) {
                setReadTrue(inbox,holder);
                Toast.makeText(parentActivity,"Go to receipt",Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void clickListenerForGoToMyBookingPage(final ViewHolder holder, final Inbox inbox){
        holder.wholeCL.setOnClickListener(new DebouncedOnClickListener(500) {
            @Override
            public void onDebouncedClick(View v) {
                setReadTrue(inbox,holder);
                Fragment searchHostRoomFragment = SearchHostRoomFragment.newInstance(parentActivity);
                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, searchHostRoomFragment).addToBackStack(SearchHostRoomFragment.TAG).commit();
            }
        });
    }

    private void clickListenerForGoToAccomDetails(final ViewHolder holder, final Inbox inbox, final Room room){
        holder.wholeCL.setOnClickListener(new DebouncedOnClickListener(500) {
            @Override
            public void onDebouncedClick(View v) {
                setReadTrue(inbox,holder);
                Fragment hostStayMainPageFragment = HostStayMainPageFragment.newInstance(parentActivity,parentActivity.getString(R.string.stays),room,room.id);
                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, hostStayMainPageFragment).addToBackStack(HostStayMainPageFragment.TAG).commit();
            }
        });
    }

    private void clickListenerForGoToUserProfile(final ViewHolder holder, final Inbox inbox, final User user){
        holder.wholeCL.setOnClickListener(new DebouncedOnClickListener(500) {
            @Override
            public void onDebouncedClick(View v) {
                setReadTrue(inbox,holder);
                boolean isOwnProfile = TextUtils.equals(user.id,parentActivity.uid);
                Fragment profileFragment = ProfileFragment.newInstance(parentActivity,user,isOwnProfile);
                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, profileFragment).addToBackStack(ProfileFragment.TAG).commit();
            }
        });
    }

    private void setReadTrue(Inbox inbox, ViewHolder holder){
        if(inbox.isRead){
            return;
        }
        HashMap<String,Object> inboxMap = new HashMap<>();
        inboxMap.put(Inbox.IS_READ,true);
        inboxMap.put(Inbox.DATE_UPDATED, FieldValue.serverTimestamp());

        GeneralFunction.getFirestoreInstance()
                .collection(User.URL_USER)
                .document(parentActivity.uid)
                .collection(Inbox.URL_INBOX)
                .document(inbox.id)
                .update(inboxMap)
                .addOnCompleteListener(GeneralFunction.getOnCompleteListener(parentActivity,TAG));

        holder.wholeCL.setBackgroundColor(parentActivity.getResources().getColor(R.color.white));
    }

    @Override
    public int getItemCount() {
        return inboxes.size();
    }
}