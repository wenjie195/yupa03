package com.vidatechft.yupa.adapter;

import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.Itinerary;
import com.vidatechft.yupa.classes.Plan;
import com.vidatechft.yupa.itinerary.AddNewPlanFragment;
import com.vidatechft.yupa.itinerary.ViewPlanRouteFragment;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.Date;

public class ItineraryPlanRecyclerDetailsAdapter extends RecyclerView.Adapter<ItineraryPlanRecyclerDetailsAdapter.ViewHolder>  {
    public static final String TAG = ItineraryPlanRecyclerDetailsAdapter.class.getName();
    private MainActivity parentActivity;
    private Fragment parentFragment;
    private Itinerary itinerary;
    private int day;
    private ArrayList<Plan> plans;

    public ItineraryPlanRecyclerDetailsAdapter(MainActivity activity, Fragment parentFragment, Itinerary itinerary, int day, ArrayList<Plan> plans) {
        this.parentActivity = activity;
        this.parentFragment = parentFragment;
        this.itinerary = itinerary;
        this.day = day;
        this.plans = plans;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView timeTV,locationTV,activityTV;
        public ConstraintLayout wholeCL;

        public ViewHolder(View v) {
            super(v);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    timeTV = itemView.findViewById(R.id.timeTV);
                    locationTV = itemView.findViewById(R.id.locationTV);
                    activityTV = itemView.findViewById(R.id.activityTV);
                    wholeCL = itemView.findViewById(R.id.wholeCL);
                }
            });
        }
    }

    @NonNull
    @Override
    public ItineraryPlanRecyclerDetailsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.itinerary_add_plan_view_detail, parent, false);

        return new ItineraryPlanRecyclerDetailsAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int pos) {
        final int position = pos;
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Plan plan = plans.get(position);

                parentActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if(plan.activityName != null){
                            holder.activityTV.setText(plan.activityName);
                        }

                        if(plan.startTime != null){
                            Date date = new Date();
                            date.setTime(plan.startTime);
                            holder.timeTV.setText(GeneralFunction.formatDateToTime(date));
                        }

                        if(plan.placeAddress != null && plan.placeAddress.placeName != null){
                            holder.locationTV.setText(plan.placeAddress.placeName);
                        }else{
                            holder.locationTV.setText("");
                        }
                    }
                });

                int plansWithGpsCount = 0;
                for(Plan tempPlan : plans){
                    if(tempPlan != null && tempPlan.gps != null){
                        plansWithGpsCount++;
                    }
                }

                if(plansWithGpsCount >= 2){
                    holder.wholeCL.setOnClickListener(new DebouncedOnClickListener(500) {
                        @Override
                        public void onDebouncedClick(View v) {
                            Fragment viewPlanRouteFragment = ViewPlanRouteFragment.newInstance(parentActivity,plans,day);
                            GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, viewPlanRouteFragment).addToBackStack(ViewPlanRouteFragment.TAG).commit();
                        }
                    });
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return plans.size();
    }
}