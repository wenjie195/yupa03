package com.vidatechft.yupa.adapter;

import android.content.DialogInterface;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.Job;
import com.vidatechft.yupa.employerFragments.EmployerPostJobFragment;
import com.vidatechft.yupa.employerFragments.EmployerPostedJobFragment;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.List;

public class PostedJobRecyclerAdapter extends RecyclerView.Adapter<PostedJobRecyclerAdapter.ViewHolder>  {
    private List<Job>   jobDataset;

    private MainActivity parentActivity;
    private EmployerPostedJobFragment parentFragment;

    public PostedJobRecyclerAdapter(MainActivity parentActivity, EmployerPostedJobFragment parentFragment, List<Job> jobDataset) {
        this.parentActivity = parentActivity;
        this.parentFragment = parentFragment;
        this.jobDataset = jobDataset;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView jobscopeTV,earnTV,storeNameTV,locationTV,earnLbl,storeNameLbl,locationLbl;
        public ImageView dotIV;
        public PopupMenu popupMenu;
        public View menuView;

        public ViewHolder(View v) {
            super(v);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    jobscopeTV = itemView.findViewById(R.id.jobscopeTV);
                    earnTV = itemView.findViewById(R.id.earnTV);
                    storeNameTV = itemView.findViewById(R.id.storeNameTV);
                    locationTV = itemView.findViewById(R.id.locationTV);
                    earnLbl = itemView.findViewById(R.id.earnLbl);
                    storeNameLbl = itemView.findViewById(R.id.storeNameLbl);
                    locationLbl = itemView.findViewById(R.id.locationLbl);
                    dotIV = itemView.findViewById(R.id.dotIV);
                    menuView = itemView.findViewById(R.id.menuView);

                    popupMenu = new PopupMenu(parentActivity,dotIV);
                    popupMenu.getMenuInflater().inflate(R.menu.job_crud, popupMenu.getMenu());

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        popupMenu.setGravity(Gravity.END);
                    }
                }
            });
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_job_posted, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int pos) {
        final int position = pos;
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Job job = jobDataset.get(position);

                holder.jobscopeTV.setText(GeneralFunction.formatList(job.jobScope,true));
                holder.earnTV.setText(GeneralFunction.formatList(job.earnList,true));

                if(job.workspaceName != null){
                    holder.storeNameTV.setText(job.workspaceName);
                }

                if(job.location != null){
                    holder.locationTV.setText(job.location);
                }

                holder.menuView.setOnClickListener(new DebouncedOnClickListener(250) {
                    @Override
                    public void onDebouncedClick(View v) {
                        holder.popupMenu.show();
                    }
                });

                if(job.isAvailable == null || !job.isAvailable){
                    holder.popupMenu.getMenu().findItem(R.id.setAvailability).setTitle(parentActivity.getString(R.string.set_available));
                }

                holder.popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        String title = String.valueOf(item.getTitle());

                        if(TextUtils.equals(title,parentActivity.getString(R.string.edit))){
                            Fragment postJobFragment = EmployerPostJobFragment.newInstance(parentActivity,parentActivity.getString(R.string.app_name),job,parentFragment,true);
                            GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, postJobFragment).addToBackStack(EmployerPostJobFragment.TAG).commit();
                        }
                        else if(TextUtils.equals(title,parentActivity.getString(R.string.set_available))){
                            setAvailability(true,holder.getAdapterPosition(),holder.popupMenu);
                        }
                        else if(TextUtils.equals(title,parentActivity.getString(R.string.set_unavailable))){
                            setAvailability(false,holder.getAdapterPosition(),holder.popupMenu);
                        }
                        else if(TextUtils.equals(title,parentActivity.getString(R.string.remove))){
                            removeJob(holder.getAdapterPosition());
                        }

                        return true;
                    }
                });

                int colorTitle,colorDesc;
                if(job.isAvailable == null || !job.isAvailable){
                    colorTitle = parentActivity.getResources().getColor(R.color.light_gray);
                    colorDesc = parentActivity.getResources().getColor(R.color.lighter_gray);
                }else{
                    colorTitle = parentActivity.getResources().getColor(R.color.black);
                    colorDesc = parentActivity.getResources().getColor(R.color.medium_gray);
                }
                holder.jobscopeTV.setTextColor(colorTitle);
                holder.earnTV.setTextColor(colorDesc);
                holder.earnLbl.setTextColor(colorDesc);
                holder.storeNameLbl.setTextColor(colorDesc);
                holder.storeNameTV.setTextColor(colorDesc);
                holder.locationLbl.setTextColor(colorDesc);
                holder.locationTV.setTextColor(colorDesc);
            }
        });
    }

    @Override
    public int getItemCount() {
        return jobDataset.size();
    }

    private void setAvailability(final boolean isAvailable, int position, final PopupMenu popupMenu){
        GeneralFunction.getFirestoreInstance()
                .collection(Job.URL_JOB)
                .document(jobDataset.get(position).id)
                .update(Job.IS_AVAILABLE, isAvailable);

        jobDataset.get(position).isAvailable = isAvailable;

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(isAvailable){
                    popupMenu.getMenu().findItem(R.id.setAvailability).setTitle(R.string.set_unavailable);
                }else{
                    popupMenu.getMenu().findItem(R.id.setAvailability).setTitle(R.string.set_available);
                }

                notifyDataSetChanged();
            }
        });
    }

    private void removeJob(final int position){
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(parentActivity);
                alertDialogBuilder.setTitle(parentActivity.getString(R.string.employer_confirmation_title));
                alertDialogBuilder.setMessage(parentActivity.getString(R.string.employer_job_confirm_delete_desc));
                alertDialogBuilder.setPositiveButton(parentActivity.getString(R.string.btn_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        GeneralFunction.getFirestoreInstance()
                                .collection(Job.URL_JOB)
                                .document(jobDataset.get(position).id)
                                .delete();

                        jobDataset.remove(position);
                        notifyDataSetChanged();

                        dialog.dismiss();
                    }
                });

                alertDialogBuilder.setNegativeButton(parentActivity.getString(R.string.btn_no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                android.support.v7.app.AlertDialog alertDialog  = alertDialogBuilder.create();
                if( !( parentActivity.isFinishing() && alertDialog.isShowing() ) ){
                    alertDialog.show();
                }

            }
        });
    }
}
