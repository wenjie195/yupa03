package com.vidatechft.yupa.adapter;

import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.TravelKit;
import com.vidatechft.yupa.paymentFragment.BuyTravelKitFragment;
import com.vidatechft.yupa.paymentFragment.SelectTravelKitFragment;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class SelectTravelKtAdapter extends RecyclerView.Adapter<SelectTravelKtAdapter.ViewHolder> {
    public static final String TAG = SelectTravelKtAdapter.class.getName();
    private MainActivity parentActivity;
    private ArrayList<TravelKit> travelKitsList;
    private HashMap<String,Boolean> selectedTravelKitMap = new HashMap<>();
    private HashMap<String,Object> travelKitCheckBoxMap = new HashMap<>();
    private SelectTravelKitFragment selectTravelKitFragment;


    public SelectTravelKtAdapter(MainActivity activity, SelectTravelKitFragment selectTravelKitFragment, ArrayList<TravelKit> travelKitsList, HashMap<String,Boolean> selectedTravelKitMap, HashMap<String,Object> travelKitCheckBoxMap) {
        this.parentActivity = activity;
        this.travelKitsList = travelKitsList;
        this.selectedTravelKitMap = selectedTravelKitMap;
        this.travelKitCheckBoxMap = travelKitCheckBoxMap;
        this.selectTravelKitFragment = selectTravelKitFragment;

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ConstraintLayout wholeLL;
        ImageView selectTravelKitIV,tickIV;
        TextView selectTravelKitTV;

        public ViewHolder(View v) {
            super(v);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    wholeLL = itemView.findViewById(R.id.wholeLL);
                    selectTravelKitIV = itemView.findViewById(R.id.selectTravelKitIV);
                    selectTravelKitTV = itemView.findViewById(R.id.selectTravelKitTV);
                    tickIV = itemView.findViewById(R.id.tickIV);
                }
            });
        }
    }

    @NonNull
    @Override
    public SelectTravelKtAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_select_travel_kit, parent, false);

        return new SelectTravelKtAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final SelectTravelKtAdapter.ViewHolder holder, final int position) {
            final int pos = position;
            final TravelKit tempTravelKit = travelKitsList.get(pos);
            if(tempTravelKit != null){
                holder.tickIV.setImageDrawable(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.baseline_done_black_24,R.color.green));

                if(tempTravelKit.name != null){
                    holder.selectTravelKitTV.setText(tempTravelKit.name);
                    if(selectedTravelKitMap != null){
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                            if(!Objects.equals(selectedTravelKitMap.get(tempTravelKit.name), true)){
                                holder.tickIV.setVisibility(View.GONE);
                            }
                            else{
                                holder.tickIV.setVisibility(View.VISIBLE);
                            }
                        }
                        else{
                            try{
                                if(selectedTravelKitMap.get(tempTravelKit.name) != null && selectedTravelKitMap.get(tempTravelKit.name)){
                                    holder.tickIV.setVisibility(View.VISIBLE);
                                }
                                else{
                                    holder.tickIV.setVisibility(View.GONE);
                                }
                            }
                            catch (Exception e){
                                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_occured),R.drawable.notice_bad,TAG);
                            }
                        }
                    }
                }
                if(tempTravelKit.iconUrl != null && !tempTravelKit.iconUrl.isEmpty()){
                    GeneralFunction.GlideImageSetting(parentActivity,Uri.parse(tempTravelKit.iconUrl),holder.selectTravelKitIV);
                }
                holder.wholeLL.setOnClickListener(new DebouncedOnClickListener(500) {
                    @Override
                    public void onDebouncedClick(View v) {
                        if(tempTravelKit.name != null){
                            holder.selectTravelKitTV.setText(tempTravelKit.name);
                        }
                        if(selectedTravelKitMap == null){
                            HashMap<String,Boolean> mSelectedTravelKitMap = new HashMap<>();
                            if(tempTravelKit.name != null){
                                mSelectedTravelKitMap.put(tempTravelKit.name,false);
                                selectedTravelKitMap = mSelectedTravelKitMap;
                            }
                        }
                        Fragment buyTravelKitFragment = BuyTravelKitFragment.newInstance(parentActivity,selectTravelKitFragment,tempTravelKit,null,selectedTravelKitMap,travelKitCheckBoxMap);
                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, buyTravelKitFragment).addToBackStack(BuyTravelKitFragment.TAG).commit();
                    }
                });

            }

    }




    @Override
    public int getItemCount() {
        return travelKitsList.size();
    }
}
