package com.vidatechft.yupa.adapter;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.Itinerary;
import com.vidatechft.yupa.classes.Job;
import com.vidatechft.yupa.employerFragments.PTJobSwipeFragment;
import com.vidatechft.yupa.itinerary.ItineraryDetailFragment;
import com.vidatechft.yupa.itinerary.StartPlanItineraryFragment;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;

public class JobRecyclerGridAdapter extends RecyclerView.Adapter<JobRecyclerGridAdapter.ViewHolder>  {
    public static final String TAG = JobRecyclerGridAdapter.class.getName();
    private ArrayList<Job> jobList;
    private MainActivity parentActivity;
    private Fragment parentFragment;

    public JobRecyclerGridAdapter(MainActivity activity, Fragment parentFragment, ArrayList<Job> jobList) {
        this.parentFragment = parentFragment;
        this.parentActivity = activity;
        this.jobList = jobList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public CardView wholeCV;
        public ImageView imageView;
        public TextView textView;

        public ViewHolder(View v) {
            super(v);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    wholeCV = itemView.findViewById(R.id.wholeCV);
                    imageView = itemView.findViewById(R.id.imageView);
                    textView = itemView.findViewById(R.id.textView);
                }
            });
        }
    }

    @NonNull
    @Override
    public JobRecyclerGridAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_grid_itinerary, parent, false);

        return new JobRecyclerGridAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int pos) {
        final int position = pos;
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Job job = jobList.get(position);

                parentActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(job != null){
                            if(job.jobPicUrl != null){
                                GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(job.jobPicUrl),holder.imageView);
                            }else{
                                holder.imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                                GeneralFunction.GlideImageSettingOwnDrawable(parentActivity, R.drawable.ic_not_found,holder.imageView);
                            }

                            if(job.jobScope != null && !job.jobScope.isEmpty()){
                                holder.textView.setText(GeneralFunction.formatList(job.jobScope,true));
                                holder.textView.setVisibility(View.VISIBLE);
                            }else{
                                holder.textView.setVisibility(View.GONE);
                            }

                            holder.wholeCV.setOnClickListener(new DebouncedOnClickListener(500) {
                                @Override
                                public void onDebouncedClick(View v) {
                                    ArrayList<Job> sortedJobs = new ArrayList<>();
                                    for(int i = position; i < jobList.size(); i++){
                                        sortedJobs.add(jobList.get(i));
                                    }
                                    Fragment ptJobSwipeFragment = PTJobSwipeFragment.newInstance(parentActivity,parentActivity.getString(R.string.results),sortedJobs);
                                    GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, ptJobSwipeFragment).addToBackStack(PTJobSwipeFragment.TAG).commit();
                                }
                            });
                        }
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return jobList.size();
    }
}