package com.vidatechft.yupa.adapter;

import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.AppliedCandidate;
import com.vidatechft.yupa.classes.Chat;
import com.vidatechft.yupa.classes.Job;
import com.vidatechft.yupa.classes.User;
import com.vidatechft.yupa.dialogFragments.ApplyPTJobDialogFragment;
import com.vidatechft.yupa.dialogFragments.ConnectNearbyUserClusterListDialogFragment;
import com.vidatechft.yupa.employerFragments.PTJobSwipeFragment;
import com.vidatechft.yupa.profileFragments.ProfileFragment;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;

public class ConnectNearbyUserListAdapter extends RecyclerView.Adapter<ConnectNearbyUserListAdapter.ViewHolder>  {
    public static final String TAG = ConnectNearbyUserListAdapter.class.getName();

    private MainActivity parentActivity;
    private ConnectNearbyUserClusterListDialogFragment parentFragment;
    private ArrayList<User> users;
    private boolean needShowPartTime;

    public ConnectNearbyUserListAdapter(MainActivity parentActivity, ConnectNearbyUserClusterListDialogFragment parentFragment, ArrayList<User> users, boolean needShowPartTime) {
        this.parentActivity = parentActivity;
        this.parentFragment = parentFragment;
        this.users = users;
        this.needShowPartTime = needShowPartTime;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView profileIV;
        public TextView nameTV,liveInTV;
        public ConstraintLayout wholeCL;

        public ViewHolder(View v) {
            super(v);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    profileIV = itemView.findViewById(R.id.profileIV);
                    nameTV = itemView.findViewById(R.id.nameTV);
                    liveInTV = itemView.findViewById(R.id.liveInTV);
                    wholeCL = itemView.findViewById(R.id.wholeCL);
                }
            });
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_connect_nearby_user_list, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int pos) {
        final int position = pos;
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final User user = users.get(position);

                holder.wholeCL.setOnClickListener(new DebouncedOnClickListener(500) {
                    @Override
                    public void onDebouncedClick(View v) {
                        if(user != null){
                            if(parentActivity != null && !parentActivity.isFinishing() && GeneralFunction.isAppInForground(parentActivity.getApplicationContext())) {
                                if (parentFragment != null) {
                                    parentFragment.dismiss();
                                }

                                //putting the whole into a handle is to prevent map lag or map bad performance after going into a fragment from dialog and then go back to the map again (doing this makes the map very lag so this post handle fixes this problem)
                                Handler handler = new Handler();
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        Fragment profileFragment = ProfileFragment.newInstance(parentActivity,user,false);
                                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, profileFragment).addToBackStack(ProfileFragment.TAG).commit();
                                    }
                                });
                            }
                        }else{
                            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_goto_user_profile),R.drawable.notice_bad,TAG);
                        }
                    }
                });

                if(user != null){
                    if(user.name != null){
                        holder.nameTV.setText(user.name);
                    }else{
                        holder.nameTV.setText(parentActivity.getString(R.string.no_name));
                    }

                    if(user.profilePicUrl != null){
                        GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(user.profilePicUrl),holder.profileIV);
                    }

                    if(needShowPartTime){
                        if(user.jobScope != null && !user.jobScope.isEmpty()){
                            holder.liveInTV.setText(GeneralFunction.formatList(user.jobScope,true));
                        }
                    }else{
                        if(user.country != null){
                            String liveIn;
                            if(user.state != null && !user.state.isEmpty()){
                                liveIn = user.state + ", " + user.country;
                            }else{
                                liveIn = user.country;
                            }
                            holder.liveInTV.setText(liveIn);
                        }
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return users.size();
    }
}