package com.vidatechft.yupa.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.ortiz.touchview.TouchImageView;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.Blog;
import com.vidatechft.yupa.classes.Message;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.List;

public class GalleryViewPagerAdapter extends PagerAdapter {
    private MainActivity parentActivity;
    private ArrayList<Room> galleryList = new ArrayList<>();
    private int position;
    private Room rooms;
    private Blog blogs;
    private Message tempMessage;
    private LayoutInflater layoutInflater;
    private Context context;
    private ArrayList<String> showcasePic;
    private List<Message> listAllImageMessage;

    public GalleryViewPagerAdapter (MainActivity parentActivity, Context context, Room rooms){
        this.parentActivity = parentActivity;
        this.rooms = rooms;
        layoutInflater = LayoutInflater.from(context);
    }

    public GalleryViewPagerAdapter (MainActivity parentActivity, Context context, Blog blogs){
        this.parentActivity = parentActivity;
        this.blogs = blogs;
        layoutInflater = LayoutInflater.from(context);
    }

    public GalleryViewPagerAdapter (MainActivity parentActivity, Context context,  ArrayList<String> showcasePic){
        this.parentActivity = parentActivity;
        this.showcasePic = showcasePic;
        layoutInflater = LayoutInflater.from(context);
    }

    public GalleryViewPagerAdapter(MainActivity parentActivity , Context context , Message tempMessage , List<Message> listAllImageMessage)
    {
        this.parentActivity = parentActivity;
        this.tempMessage = tempMessage;
        this.listAllImageMessage = listAllImageMessage;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount()
    {
        int size = 0;
        if(rooms != null)
        {
            size = rooms.getRooms().size();
        }
        else if(tempMessage != null)
        {
            size = listAllImageMessage.size();
        }
        else if(showcasePic != null)
        {
            size = showcasePic.size();
        }
        else if(blogs != null){
            size = blogs.getBlogs().size();
        }

        return size;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View viewLayout = layoutInflater.inflate(R.layout.gallery_pager_item, container, false);
        TouchImageView imageView = viewLayout.findViewById(R.id.imageViewPG);
        TextView roomName = viewLayout.findViewById(R.id.roomNameTV);
        ImageButton backIB = viewLayout.findViewById(R.id.backIB);

        if(rooms != null)
        {
            final Room room = rooms.getRooms().get(position);
            if(room != null){
                if(room.urlRoomPic != null){
                    GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(room.urlRoomPic),imageView);
                }
                if(room.roomName != null){
                    roomName.setText(room.roomName);
                }
            }
        }
        else if (tempMessage != null)
        {
            final Message message = listAllImageMessage.get(position);
            if(message != null){
                if(message.img != null){
                    GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(message.img),imageView);
                }
                if(message.dateCreated != null)
                {
                    roomName.setText(GeneralFunction.formatDateToDateWithTime(message.dateCreated.toDate()));
                }
            }
        }
        else if (showcasePic != null)
        {
            final String image = showcasePic.get(position);
            if(image != null){
//                if(message.img != null){
                    GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(image),imageView);
//                }
//                if(message.dateCreated != null)
//                {
//                    roomName.setText(GeneralFunction.formatDateToDateWithTime(message.dateCreated.toDate()));
//                }
            }
        }

        else if(blogs != null){
            final Blog tempBlog = blogs.getBlogs().get(position);
            if(tempBlog != null){
                if(tempBlog.coverPhoto != null){
                    GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(tempBlog.coverPhoto),imageView);
                }
            }
        }




        backIB.setOnClickListener(new DebouncedOnClickListener(500) {
            @Override
            public void onDebouncedClick(View v) {
                parentActivity.onBackPressed();

            }
        });
        //show the view (Must use)
        container.addView(viewLayout);

        return  viewLayout;
    }
}
