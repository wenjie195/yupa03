package com.vidatechft.yupa.adapter;

import android.content.res.ColorStateList;
import android.support.annotation.NonNull;
import android.support.v4.widget.CompoundButtonCompat;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;

import java.util.ArrayList;

public class CheckboxRecyclerGridAdapter extends RecyclerView.Adapter<CheckboxRecyclerGridAdapter.ViewHolder> {
    private MainActivity parentActivity;
    private ArrayList<CheckBox> checkBoxes;

    private int colorRes;

    public CheckboxRecyclerGridAdapter(MainActivity parentActivity, ArrayList<CheckBox> checkBoxes) {
        this.parentActivity = parentActivity;
        this.checkBoxes = checkBoxes;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public AppCompatCheckBox checkBox;
        public TextView textView;

        public ViewHolder(View v) {
            super(v);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    checkBox = itemView.findViewById(R.id.checkBox);
                    textView = itemView.findViewById(R.id.textView);
                }
            });
        }
    }

    @NonNull
    @Override
    public CheckboxRecyclerGridAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_checkbox_with_text, parent, false);

        return new CheckboxRecyclerGridAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final int pos = position;
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final CheckBox checkBox = checkBoxes.get(pos);
                if(checkBox != null && !checkBox.getText().toString().isEmpty()){

                    //set checkbox border colour from here https://stackoverflow.com/questions/34340256/how-to-change-checkbox-checked-color-programmatically
                    ColorStateList colorStateList = new ColorStateList(
                            new int[][] {
                                    new int[] { -android.R.attr.state_checked }, // unchecked
                                    new int[] {  android.R.attr.state_checked }  // checked
                            },
                            new int[] {
                                    parentActivity.getResources().getColor(R.color.colorPrimary),
                                    parentActivity.getResources().getColor(R.color.colorPrimary)
                            }
                    );

                    if(colorRes != 0){
                        colorStateList = new ColorStateList(
                                new int[][] {
                                        new int[] { -android.R.attr.state_checked }, // unchecked
                                        new int[] {  android.R.attr.state_checked }  // checked
                                },
                                new int[] {
                                        colorRes,
                                        colorRes
                                }
                        );

                        holder.textView.setTextColor(colorRes);
                        holder.checkBox.setTextColor(colorRes);
                    }

                    CompoundButtonCompat.setButtonTintList(holder.checkBox, colorStateList);
                    holder.textView.setText(checkBox.getText().toString());
                    if(checkBox.isChecked()){
                        holder.checkBox.setChecked(true);
                    }else{
                        holder.checkBox.setChecked(false);
                    }
                }

                holder.checkBox.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        checkBoxes.get(pos).setChecked(isChecked);
                    }
                });
                holder.textView.setOnClickListener(new DebouncedOnClickListener(500) {
                    @Override
                    public void onDebouncedClick(View v) {
                        if(checkBoxes.get(pos).isChecked()){
                            holder.checkBox.setChecked(false);
                        }
                        else{
                            holder.checkBox.setChecked(true);
                        }
                    }
                });
            }
        });
    }

    public void setColor(int colorRes){
        this.colorRes = colorRes;
    }

    @Override
    public int getItemCount() {
        return checkBoxes.size();
    }

    public ArrayList<CheckBox> getCheckBoxes() {
        return checkBoxes;
    }
}
