package com.vidatechft.yupa.adapter;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.blog.BlogMainFragment;
import com.vidatechft.yupa.classes.Blog;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.hostFragments.GalleryViewPagerFragment;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;

public class RecommendedAdapter extends RecyclerView.Adapter<RecommendedAdapter.ViewHolder> {
    public static final String TAG = RecommendedAdapter.class.getName();
    private MainActivity parentActivity;
    private ArrayList<Blog> blogList;

    public RecommendedAdapter(MainActivity activity, ArrayList<Blog> blogList) {
        this.parentActivity = activity;
        this.blogList = blogList;

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView galleryTV;
        public ImageView galleryIV;
        public ConstraintLayout wholeCL;

        public ViewHolder(View v) {
            super(v);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    galleryTV = itemView.findViewById(R.id.galleryTV);
                    galleryIV = itemView.findViewById(R.id.galleryIV);
                    wholeCL = itemView.findViewById(R.id.wholeCL);
                }
            });
        }
    }

    @NonNull
    @Override
    public RecommendedAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_grid_gallery, parent, false);

        return new RecommendedAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecommendedAdapter.ViewHolder holder, final int position) {
        final Blog blog = blogList.get(position);
        blog.setBlogs(blogList);
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(blog != null){
                    if(blog.title != null){
                        holder.galleryTV.setText(blog.title);
                    }
                    if(blog.coverPhoto != null){
                        GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(blog.coverPhoto),holder.galleryIV);
                    }

                    holder.wholeCL.setOnClickListener(new DebouncedOnClickListener(500) {
                        @Override
                        public void onDebouncedClick(View v) {
//                            Fragment galleryViewPagerFragment = GalleryViewPagerFragment.newInstance(parentActivity,parentActivity.getString(R.string.itinerary_action_toolbar1),blog, position);
//                            GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, galleryViewPagerFragment).addToBackStack(TAG).commit();
                            Fragment blogMainFragment = BlogMainFragment.newInstance(parentActivity,blog);
                            GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, blogMainFragment).addToBackStack(BlogMainFragment.TAG).commit();
                        }
                    });
                }
            }
        });
    }




    @Override
    public int getItemCount() {
     return blogList.size();
    }
}
