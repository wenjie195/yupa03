package com.vidatechft.yupa.adapter;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.hostFragments.GalleryViewPagerFragment;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.List;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> {
    public static final String TAG = GalleryAdapter.class.getName();
    private MainActivity parentActivity;
    private Fragment fragment;
    private ArrayList<Room> galleryList = new ArrayList<>();

    public GalleryAdapter(MainActivity activity, Fragment parentFragment,ArrayList<Room> galleryList) {
        this.fragment = parentFragment;
        this.parentActivity = activity;
        this.galleryList = galleryList;

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView galleryTV;
        public ImageView galleryIV;
        public ConstraintLayout wholeCL;

        public ViewHolder(View v) {
            super(v);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    galleryTV = itemView.findViewById(R.id.galleryTV);
                    galleryIV = itemView.findViewById(R.id.galleryIV);
                    wholeCL = itemView.findViewById(R.id.wholeCL);
                }
            });
        }
    }

    @NonNull
    @Override
    public GalleryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_grid_gallery, parent, false);

        return new GalleryAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final GalleryAdapter.ViewHolder holder, final int position) {
        final Room room = galleryList.get(position);
        room.setRooms(galleryList);
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(room != null){
                    if(room.roomName != null){
                        holder.galleryTV.setText(room.roomName);
                    }
                    if(room.urlRoomPic != null){
                        GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(room.urlRoomPic),holder.galleryIV);
                    }

                    holder.wholeCL.setOnClickListener(new DebouncedOnClickListener(500) {
                        @Override
                        public void onDebouncedClick(View v) {
                            Fragment galleryViewPagerFragment = GalleryViewPagerFragment.newInstance(parentActivity,parentActivity.getString(R.string.itinerary_action_toolbar1),room, position);
                            GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, galleryViewPagerFragment).addToBackStack(TAG).commit();
                        }
                    });
                }
            }
        });
    }




    @Override
    public int getItemCount() {
     return galleryList.size();
    }
}
