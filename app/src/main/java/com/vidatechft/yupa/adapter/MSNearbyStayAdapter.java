package com.vidatechft.yupa.adapter;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.Favourite;
import com.vidatechft.yupa.classes.Host;
import com.vidatechft.yupa.classes.Merchant;
import com.vidatechft.yupa.dialogFragments.MerchantShopDetailsDialogFragment;
import com.vidatechft.yupa.merchantShopFragment.MerchantMapFragment;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MSNearbyStayAdapter extends RecyclerView.Adapter<MSNearbyStayAdapter.ViewHolder> {
    public  static final String TAG = MSNearbyStayAdapter.class.getName();

    private MainActivity parentActivity;
    private MerchantMapFragment parentFragment;
    private List<Merchant> merchants;
    private boolean isViewAll = false;

    public MSNearbyStayAdapter(MainActivity parentActivity, MerchantMapFragment parentFragment, List<Merchant> merchants, boolean isViewAll){
        this.parentActivity = parentActivity;
        this.parentFragment = parentFragment;
        this.merchants = merchants;
        this.isViewAll = isViewAll;
    }

    public MSNearbyStayAdapter(MainActivity parentActivity, List<Merchant> merchants){
        this.parentActivity = parentActivity;
        this.merchants = merchants;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView shopTV;
        public ImageView shopIV;
        public ImageView favIV;

        public ViewHolder(View v) {
            super(v);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    shopTV = itemView.findViewById(R.id.shopTV);
                    shopIV = itemView.findViewById(R.id.shopIV);
                    favIV = itemView.findViewById(R.id.favIV);
                }
            });
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        if(isViewAll){
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.adapter_nearby_shop_list_vertical, parent, false);
        }
        else{
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.adapter_nearby_shop_list, parent, false);
        }

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int pos) {
        final int position = pos;
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Merchant merchant = merchants.get(position);

                if(merchant == null){
                    GeneralFunction.openMessageNotificationDialog(parentActivity,"gg",R.drawable.notice_bad,TAG);
                    Log.e(TAG,"error getting shop in MSNearbyStayAdapter");
                    return;
                }

                if(merchant.workPlace != null){
                    holder.shopTV.setText(merchant.workPlace);
                }
                if(merchant.shopPicUrl != null){
                    GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(merchant.shopPicUrl),holder.shopIV);
                }

                if(merchant.favourite != null && merchant.favourite.isLiked){
//                    holder.favIV.setImageDrawable(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.ic_love,R.color.red));
                    holder.favIV.setImageResource(R.drawable.ic_love_like);
                }else{
//                    holder.favIV.setImageDrawable(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.ic_love,R.color.white));
                    holder.favIV.setImageResource(R.drawable.ic_love);
                }

                holder.shopIV.setOnClickListener(new DebouncedOnClickListener(500) {
                    @Override
                    public void onDebouncedClick(View v) {
                        if(parentActivity != null && !parentActivity.isFinishing() && GeneralFunction.isAppInForground(parentActivity.getApplicationContext())){
                            FragmentManager fm = parentActivity.getSupportFragmentManager();
                            MerchantShopDetailsDialogFragment merchantShopDetailsDialogFragment = MerchantShopDetailsDialogFragment.newInstance(parentActivity, merchants.get(position));
                            merchantShopDetailsDialogFragment.show(fm, TAG);
                        }
                    }
                });

                holder.favIV.setOnClickListener(new DebouncedOnClickListener(500) {
                    @Override
                    public void onDebouncedClick(View v) {
                        if(GeneralFunction.isGuest()){
                            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_guest_restrict_usage),R.drawable.notice_bad,TAG);
                            return;
                        }
                        handleLike(merchant, position, holder);
                    }
                });
            }
        });

    }

    @Override
    public int getItemCount() {
        return merchants.size();
    }

    private void handleLike(Merchant merchant, int position, ViewHolder holder){
        DocumentReference favRef;

        if(merchant.favourite == null ||merchant.id == null){
            favRef = GeneralFunction.getFirestoreInstance()
                    .collection(Favourite.URL_FAVOURITE)
                    .document();

            merchant.favourite = new Favourite();

            merchant.favourite.id = favRef.getId();

            merchant.favourite.isLiked = true;
        }else{
            favRef = GeneralFunction.getFirestoreInstance()
                    .collection(Favourite.URL_FAVOURITE)
                    .document(merchant.favourite.id);

            merchant.favourite.isLiked = !merchant.favourite.isLiked;
        }

        merchant.favourite.userId = parentActivity.uid;
        merchant.favourite.targetId = merchant.id;
        merchant.favourite.type = Favourite.FAV_TYPE_SHOP;
        HashMap<String,Object> favMap = Favourite.toMap(merchant.favourite);

        if(merchant.favourite.dateCreated == null){
            favMap.put(Favourite.DATE_CREATED, FieldValue.serverTimestamp());
        }
        favMap.put(Favourite.DATE_UPDATED, FieldValue.serverTimestamp());

        favRef.set(favMap);

//        if(merchant.favourite.isLiked){
//            holder.favIV.setImageResource(R.drawable.ic_love_like);
////            holder.favIV.setImageDrawable(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.ic_love,R.color.red));
//        }else{
//            holder.favIV.setImageResource(R.drawable.ic_love);
////            holder.favIV.setImageDrawable(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.ic_love,R.color.white));
//        }

        if(merchants.get(position) != null){
            merchants.set(position,merchant);
            notifyItemChanged(position);

            if(parentFragment != null){
                parentFragment.updateMerchantFavouriteFromViewAllDialogFragment(position,merchant);
            }
        }
    }
}
