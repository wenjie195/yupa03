package com.vidatechft.yupa.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.Accommodation;
import com.vidatechft.yupa.classes.Favourite;
import com.vidatechft.yupa.classes.Host;
import com.vidatechft.yupa.classes.Rating;
import com.vidatechft.yupa.classes.Report;
import com.vidatechft.yupa.classes.User;
import com.vidatechft.yupa.dialogFragments.ApplyPTJobDialogFragment;
import com.vidatechft.yupa.dialogFragments.ReviewReportFragment;
import com.vidatechft.yupa.hostFragments.HostStayMainPageFragment;
import com.vidatechft.yupa.profileFragments.ProfileFragment;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.ViewHolder> {
    public final String TAG = ReviewAdapter.class.getName();
    private MainActivity parentActivity;
    private Context context;
    private Fragment fragment;
    private Map<String,Object> reviewList = new HashMap<>();
    private List<Rating> cc;
    private Accommodation accommodation;
    private ReviewAdapter reviewAdapter;


    public ReviewAdapter(MainActivity activity, List<Rating> cc, Accommodation accommodation){
        this.parentActivity = activity;
        this.cc = cc;
        this.accommodation = accommodation;

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView reviewProfileNameTV,reviewDateTV,reviewRateTV,reviewDescTV,likeCountTV;
        public ImageView reviewProfilePicIV;
        public ImageButton likeIB,moreOptionsIB;

        public ViewHolder(View v) {
            super(v);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    reviewProfileNameTV = itemView.findViewById(R.id.reviewProfileNameTV);
                    reviewDateTV = itemView.findViewById(R.id.reviewDateTV);
                    reviewRateTV = itemView.findViewById(R.id.reviewRateTV);
                    reviewProfilePicIV = itemView.findViewById(R.id.reviewProfilePicIV);
                    likeIB = itemView.findViewById(R.id.likeIB);
                    moreOptionsIB = itemView.findViewById(R.id.moreOptionsIB);
                    reviewDescTV = itemView.findViewById(R.id.reviewDescTV);
                    likeCountTV = itemView.findViewById(R.id.likeCountTV);
                }
            });
        }
    }

    @NonNull
    @Override
    public ReviewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_review, parent, false);

        return new ReviewAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ReviewAdapter.ViewHolder holder, final int position) {
        if(cc != null) {
            final Rating rating = cc.get(position);
            final Host host = new Host();
            final Favourite favourite = new Favourite();

            if(rating.user.profilePicUrl != null && !rating.user.profilePicUrl.trim().isEmpty()){
                GeneralFunction.GlideImageSetting(parentActivity,Uri.parse(rating.user.profilePicUrl),holder.reviewProfilePicIV);
            }
            if(rating.user.name != null){
                holder.reviewProfileNameTV.setText(rating.user.name);
            }
            if(rating.rating != null){
                String symbol = String.valueOf(Html.fromHtml(parentActivity.getResources().getString(R.string.symbol_star)));
                String mRating = String.valueOf(rating.rating) + symbol;
                holder.reviewRateTV.setText(mRating);
            }
            if(rating.dateCreated != null){
                SimpleDateFormat formatter = new SimpleDateFormat("d MMM yyyy");
                String dateString = formatter.format(new Date(rating.dateCreated.getSeconds() * 1000L));
                holder.reviewDateTV.setText(dateString);
            }
            if(rating.review != null){
                holder.reviewDescTV.setText(rating.review);
            }

            if(rating.favourite == null){
                holder.likeIB.setImageResource(R.drawable.ic_like_grey);
            }
            else{
                if(rating.favourite.isLiked != null && rating.favourite.isLiked){
                    holder.likeIB.setImageResource(R.drawable.ic_like_color_primary);
                }
                else{
                    holder.likeIB.setImageResource(R.drawable.ic_like_grey);
                }
            }
            if(rating.likeCount != null){
                holder.likeCountTV.setText(String.valueOf(rating.likeCount));
            }

            if(rating.favourite == null){
                rating.favourite = new Favourite();
                rating.favourite.accommodationId = accommodation.id;
                rating.favourite.targetId = accommodation.id;
            }

//            try{
//                if(rating.favourite.isLiked != null){
//
//                    if(rating.favourite.isLiked){
//                        holder.likeIB.setImageResource(R.drawable.ic_like_color_primary);
//                    }
//                    else{
//                        holder.likeIB.setImageResource(R.drawable.ic_like_grey);
//                    }
//                }
//            }
//            catch(Exception e){
//                Log.d(TAG,"This is not important, it prevent crash because rating.favourite.isliked is null of object reference");
//            }


            holder.reviewProfilePicIV.setOnClickListener(new DebouncedOnClickListener(500) {
                @Override
                public void onDebouncedClick(View v) {
                    if(rating.user.id.equals(parentActivity.uid)){
                        Fragment profileFragment = ProfileFragment.newInstance(parentActivity,rating.user,true);
                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, profileFragment).addToBackStack(TAG).commit();
                    }
                    else{
                        Fragment profileFragment = ProfileFragment.newInstance(parentActivity,rating.user,false);
                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, profileFragment).addToBackStack(TAG).commit();
                    }
                }
            });
//            getliked(rating, holder);
            holder.likeIB.setOnClickListener(new DebouncedOnClickListener(1000) {
                @Override
                public void onDebouncedClick(View v) {
                    if(GeneralFunction.isGuest()){
                        GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_guest_restrict_usage),R.drawable.notice_bad,TAG);
                        return;
                    }
                    check_like(rating,holder,false, host,favourite);
//                    handleLikeAction(rating,holder,false, host,favourite);
                }
            });

            holder.moreOptionsIB.setOnClickListener(new DebouncedOnClickListener(500) {
                @Override
                public void onDebouncedClick(View v) {
                    if(GeneralFunction.isGuest()){
                        GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_guest_restrict_usage),R.drawable.notice_bad,TAG);
                        return;
                    }
                    showPopMenu(v, rating, favourite);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return cc.size();
    }

    public String stampToDate(long timeMillis){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date(timeMillis);
        return simpleDateFormat.format(date);
    }

    private void handleLikeAction(Rating rating, ReviewAdapter.ViewHolder holder, boolean forceLike, Host host, Favourite favourite, boolean gotLike){
        DocumentReference likeRef;
        if(accommodation == null ){
            GeneralFunction.openMessageNotificationDialog(parentActivity,"Current accommodation is null",R.drawable.notice_bad,TAG);
            return;
        }

        if(rating == null){
            GeneralFunction.openMessageNotificationDialog(parentActivity,"rating is null",R.drawable.notice_bad,TAG);
            return;
        }
        if(rating.favourite == null ){
            rating.favourite = new Favourite();
        }

        if(rating.favourite.isLiked == null || rating.favourite.targetId == null && !gotLike){

            likeRef = GeneralFunction.getFirestoreInstance()
                    .collection(Favourite.URL_FAVOURITE)
                    .document();

            favourite = new Favourite();

            rating.favourite.id = likeRef.getId();

            rating.favourite.isLiked = true;

        }else{
            likeRef = GeneralFunction.getFirestoreInstance()
                    .collection(Favourite.URL_FAVOURITE)
                    .document(rating.favourite.id);

            rating.favourite.isLiked = !rating.favourite.isLiked;

        }

        if(forceLike){
            rating.favourite.isLiked = true;
        }



        favourite.userId = parentActivity.uid;
        favourite.targetId = rating.user.id;
        favourite.type = Favourite.FAV_TYPE_RATING;
        favourite.isLiked = rating.favourite.isLiked;
        favourite.dateCreated = rating.favourite.dateCreated;
        HashMap<String,Object> likeMap = Favourite.toMap(favourite);

        if(favourite.dateCreated == null){
            likeMap.put(Favourite.DATE_CREATED, FieldValue.serverTimestamp());
        }
        likeMap.put(Favourite.DATE_UPDATED, FieldValue.serverTimestamp());
        likeMap.put(Favourite.ACCOMMODATION_ID,accommodation.id);

        likeRef.set(likeMap)
        .addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                GeneralFunction.Toast(parentActivity,e.toString());
            }
        });

        if(rating.favourite.isLiked){
            holder.likeIB.setImageResource(R.drawable.ic_like_color_primary);
            if(TextUtils.isEmpty(holder.likeCountTV.getText())){
                holder.likeCountTV.setText(String.valueOf(0));
            }
            int likecount = Integer.parseInt(holder.likeCountTV.getText().toString()) + 1;
            holder.likeCountTV.setText(String.valueOf(likecount));
        }else{
            holder.likeIB.setImageResource(R.drawable.ic_like_grey);
            int likecount = Integer.parseInt(holder.likeCountTV.getText().toString()) - 1;
            holder.likeCountTV.setText(String.valueOf(likecount));
        }
    }

    private void check_like(final Rating rating, final ViewHolder holder, boolean forceLike, final Host host, final Favourite favourite){
        GeneralFunction.getFirestoreInstance()
                .collection(Favourite.URL_FAVOURITE)
                .whereEqualTo(Favourite.ACCOMMODATION_ID, accommodation.id)
                .whereEqualTo(Favourite.TYPE, Favourite.FAV_TYPE_RATING)
                .whereEqualTo(Favourite.USER_ID, parentActivity.uid)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if(task.isSuccessful()){
                            Favourite favourite1 = new Favourite(parentActivity,task.getResult());
                            if(favourite1.isLiked == null){
                                handleLikeAction(rating,holder,false, host,favourite,false);
                            }
                            else{
                                handleLikeAction(rating,holder,false, host,favourite,true);
                            }

                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        if(e != null){
                            GeneralFunction.Toast(parentActivity, e.toString());
                        }
                    }
                });
    }

    private void showPopMenu(final View v, final Rating rating, final Favourite favourite){
        final PopupMenu popupMenu = new PopupMenu(parentActivity, v);
        // 获取布局文件
        //popmenu need to show first if not it cant find the view
        //found from here https://blog.csdn.net/qq_24867873/article/details/73180007
        popupMenu.getMenuInflater().inflate(R.menu.review_menu, popupMenu.getMenu());
        popupMenu.show();
        if(rating.favourite == null || !rating.user.id.equals(parentActivity.uid)) {
            popupMenu.getMenu().findItem(R.id.review_menu_delete).setVisible(false);
        }
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.review_menu_delete:
                        DocumentReference deleleRef = GeneralFunction.getFirestoreInstance()
                                .collection(Accommodation.URL_ACCOMMODATION)
                                .document(accommodation.id)
                                .collection(Rating.URL_RATING)
                                .document(parentActivity.uid);
                        deleleRef.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                GeneralFunction.Toast(parentActivity,parentActivity.getString(R.string.delete_rating));
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                GeneralFunction.Toast(parentActivity,e.toString());
                            }
                        });
                        break;
                    case R.id.review_menu_report:
                        checkReportSubmited(rating, favourite);
                        break;
                }
                return false;
            }
        });
    }

    private void checkReportSubmited(final Rating rating, final Favourite favourite){
        if(rating.favourite == null){
            rating.favourite = favourite;
        }
        GeneralFunction.getFirestoreInstance()
                .collection(Report.URL_REPORT)
                .whereEqualTo(Report.USER_ID,parentActivity.uid)
                .whereEqualTo(Report.ID,rating.user.id)
                .whereEqualTo(Report.ACCOMMODATION_ID, rating.favourite.accommodationId)
                .whereEqualTo(Report.IS_REVIEWED,false)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if(task.isSuccessful()){
                            if(!task.getResult().isEmpty()){
                                for(QueryDocumentSnapshot queryDocumentSnapshot : task.getResult()){
                                    Report report = queryDocumentSnapshot.toObject(Report.class);
                                    if(report.isReviewed != null && !report.isReviewed){
                                        GeneralFunction.Toast(parentActivity,parentActivity.getString(R.string.submitted_report));
                                    }
                                }
                            }
                            else{
                                DocumentReference reportRef = GeneralFunction.getFirestoreInstance().collection(Report.URL_REPORT).document();
                                FragmentManager fm = parentActivity.getSupportFragmentManager();
                                ReviewReportFragment reviewReportFragment = ReviewReportFragment.newInstance(parentActivity,rating);
                                reviewReportFragment.show(fm, TAG);
                            }
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if(e != null){
                    GeneralFunction.Toast(parentActivity,e.toString());
                }
            }
        });
    }
}
