package com.vidatechft.yupa.adapter;

import android.content.DialogInterface;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.chatFragments.DirectChatFragment;
import com.vidatechft.yupa.classes.AppliedCandidate;
import com.vidatechft.yupa.classes.CandidateDetails;
import com.vidatechft.yupa.classes.Chat;
import com.vidatechft.yupa.classes.Job;
import com.vidatechft.yupa.employerFragments.EmployerCandidateFragment;
import com.vidatechft.yupa.profileFragments.ProfileFragment;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.HashMap;
import java.util.List;

public class JobCandidateAdapter extends RecyclerView.Adapter<JobCandidateAdapter.ViewHolder>  {
    private List<CandidateDetails> candidDetailsDataset;

    private MainActivity parentActivity;
    private EmployerCandidateFragment parentFragment;
    private String lastToolbarTitle;

    public JobCandidateAdapter(MainActivity parentActivity, String lastToolbarTitle, EmployerCandidateFragment parentFragment, List<CandidateDetails> candidDetailsDataset) {
        this.parentActivity = parentActivity;
        this.lastToolbarTitle = lastToolbarTitle;
        this.parentFragment = parentFragment;
        this.candidDetailsDataset = candidDetailsDataset;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView jobscopeTV,earnTV,storeNameTV,locationTV,candidNameTV,candidDescTV,dateAppliedTV,acceptedTV;
        public ConstraintLayout wholeCL;
        public View acceptedCL;
        public ImageView profilePicIV,dotIV,onlineIV;
        public PopupMenu popupMenu;
        public View menuView;

        public ViewHolder(View v) {
            super(v);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    jobscopeTV = itemView.findViewById(R.id.jobscopeTV);
                    earnTV = itemView.findViewById(R.id.earnTV);
                    storeNameTV = itemView.findViewById(R.id.storeNameTV);
                    locationTV = itemView.findViewById(R.id.locationTV);
                    candidNameTV = itemView.findViewById(R.id.candidNameTV);
                    candidDescTV = itemView.findViewById(R.id.candidDescTV);
                    dateAppliedTV = itemView.findViewById(R.id.dateAppliedTV);
                    dotIV = itemView.findViewById(R.id.dotIV);
                    profilePicIV = itemView.findViewById(R.id.profilePicIV);
                    menuView = itemView.findViewById(R.id.menuView);
                    onlineIV = itemView.findViewById(R.id.onlineIV);
                    acceptedTV = itemView.findViewById(R.id.acceptedTV);
                    acceptedCL = itemView.findViewById(R.id.acceptedCL);
                    wholeCL = itemView.findViewById(R.id.wholeCL);

                    popupMenu = new PopupMenu(parentActivity,dotIV);
                    popupMenu.getMenuInflater().inflate(R.menu.candid_crud, popupMenu.getMenu());

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        popupMenu.setGravity(Gravity.END);
                    }
                }
            });
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_job_candidate, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int pos) {
        final int position = pos;

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final CandidateDetails candidateDetails = candidDetailsDataset.get(position);

                holder.jobscopeTV.setText(GeneralFunction.formatList(candidateDetails.job.jobScope,true));
                holder.earnTV.setText(GeneralFunction.formatList(candidateDetails.job.earnList,true));

                if(candidateDetails.job != null){
                    if(candidateDetails.job.location != null){
                        holder.locationTV.setText(candidateDetails.job.location);
                    }

                    if(candidateDetails.job.workspaceName != null){
                        holder.storeNameTV.setText(candidateDetails.job.workspaceName);
                    }
                }

                if(candidateDetails.appliedCandidate != null){
                    if(candidateDetails.appliedCandidate.desc != null){
                        holder.candidDescTV.setText(candidateDetails.appliedCandidate.desc);
                    }

                    if(candidateDetails.appliedCandidate.dateCreated != null){
                        holder.dateAppliedTV.setText(GeneralFunction.formatDateToDateWithTime(candidateDetails.appliedCandidate.dateCreated.toDate()));
                    }

                    if(candidateDetails.appliedCandidate.user != null){
                        if(candidateDetails.appliedCandidate.user.name != null){
                            holder.candidNameTV.setText(candidateDetails.appliedCandidate.user.name);

                            holder.wholeCL.setOnClickListener(new DebouncedOnClickListener(500) {
                                @Override
                                public void onDebouncedClick(View v) {
                                    if(TextUtils.equals(candidateDetails.appliedCandidate.user.id,parentActivity.uid)){
                                        Fragment profileFragment = ProfileFragment.newInstance(parentActivity,candidateDetails.appliedCandidate.user,true);
                                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, profileFragment).addToBackStack(ProfileFragment.TAG).commit();
                                    }else{
                                        Fragment profileFragment = ProfileFragment.newInstance(parentActivity,candidateDetails.appliedCandidate.user,false);
                                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, profileFragment).addToBackStack(ProfileFragment.TAG).commit();
                                    }
                                }
                            });
                        }

                        if(candidateDetails.appliedCandidate.user.profilePicUrl != null && !candidateDetails.appliedCandidate.user.profilePicUrl.isEmpty()){
                            GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(candidateDetails.appliedCandidate.user.profilePicUrl),holder.profilePicIV);
                        }else{
                            GeneralFunction.GlideImageSettingOwnDrawable(parentActivity, R.drawable.profile_placeholder,holder.profilePicIV);
                        }

                        if(candidateDetails.appliedCandidate.user.isOnline != null){
                            if(candidateDetails.appliedCandidate.user.isOnline){
                                holder.onlineIV.setImageDrawable(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.rounded_circle,R.color.green));
                            }else{
                                holder.onlineIV.setImageDrawable(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.rounded_circle,R.color.light_gray));
                            }
                        }else{
                            holder.onlineIV.setImageDrawable(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.rounded_circle,R.color.light_gray));
                        }
                    }

                    if(candidateDetails.appliedCandidate.isAccepted != null && candidateDetails.appliedCandidate.isAccepted){
                        holder.acceptedCL.setVisibility(View.VISIBLE);
                        holder.acceptedTV.setVisibility(View.VISIBLE);
                    }else{
                        holder.acceptedCL.setVisibility(View.GONE);
                        holder.acceptedTV.setVisibility(View.GONE);
                    }
                }

                holder.menuView.setOnClickListener(new DebouncedOnClickListener(250) {
                    @Override
                    public void onDebouncedClick(View v) {
                        holder.popupMenu.show();
                    }
                });

                holder.popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        String title = String.valueOf(item.getTitle());

                        if(TextUtils.equals(title,parentActivity.getString(R.string.candid_accept))){
                            acceptOrDecline(true,holder.getAdapterPosition(),holder.popupMenu);
                        }
                        else if(TextUtils.equals(title,parentActivity.getString(R.string.candid_decline))){
                            acceptOrDecline(false,holder.getAdapterPosition(),holder.popupMenu);
                        }
                        else if(TextUtils.equals(title,parentActivity.getString(R.string.candid_send_msg))){

                            String chatId;

                            if(candidateDetails.appliedCandidate.chatId == null){
                                chatId = Chat.TYPE_JOB_DIRECT + parentActivity.uid + candidateDetails.appliedCandidate.user.id;
                            }else{
                                chatId = candidateDetails.appliedCandidate.chatId;
                            }

                            Fragment directChatFragment = DirectChatFragment.newInstance(
                                    parentActivity,
                                    lastToolbarTitle,
                                    candidateDetails.appliedCandidate.user.id,
                                    chatId);
                            //always put type -> employer id -> staff id
                            //this is employer's fragment so put current login uid is correct lo
                            GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, directChatFragment).addToBackStack(DirectChatFragment.TAG).commit();
                        }

                        return true;
                    }
                });

                if(candidateDetails.appliedCandidate.isAccepted != null && candidateDetails.appliedCandidate.isAccepted){
                    removeAcceptDeclineOption(holder.popupMenu);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return candidDetailsDataset.size();
    }

    private void acceptOrDecline(final boolean isAccepted, final int position, final PopupMenu popupMenu){
        final DocumentReference jobRef = GeneralFunction.getFirestoreInstance()
                .collection(Job.URL_JOB)
                .document(candidDetailsDataset.get(position).job.id);

        final DocumentReference candidRef = jobRef
                .collection(AppliedCandidate.URL_CANDIDATE)
                .document(candidDetailsDataset.get(position).appliedCandidate.user.id);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(isAccepted){
                    //inside job
                    HashMap<String,Object> acceptedMap = new HashMap<>();
                    acceptedMap.put(AppliedCandidate.IS_ACCEPTED, isAccepted);
                    acceptedMap.put(AppliedCandidate.DATE_UPDATED, FieldValue.serverTimestamp());
                    candidRef.update(acceptedMap);

                    //outside job
                    jobRef.update(candidDetailsDataset.get(position).appliedCandidate.uid,true);

                    candidDetailsDataset.get(position).appliedCandidate.isAccepted = isAccepted;

                    removeAcceptDeclineOption(popupMenu);

                    notifyDataSetChanged();
                }else{
                    android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(parentActivity);
                    alertDialogBuilder.setTitle(parentActivity.getString(R.string.employer_confirmation_title));
                    alertDialogBuilder.setMessage(parentActivity.getString(R.string.employer_candidate_confirm_delete_desc));
                    alertDialogBuilder.setPositiveButton(parentActivity.getString(R.string.btn_yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            candidRef.delete();
                            jobRef.update(candidDetailsDataset.get(position).appliedCandidate.uid, FieldValue.delete());

                            candidDetailsDataset.remove(position);

                            notifyDataSetChanged();

                            dialog.dismiss();
                        }
                    });

                    alertDialogBuilder.setNegativeButton(parentActivity.getString(R.string.btn_no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    android.support.v7.app.AlertDialog alertDialog  = alertDialogBuilder.create();
                    if( !( parentActivity.isFinishing() && alertDialog.isShowing() ) ){
                        alertDialog.show();
                    }
                }
            }
        });
    }

    private void removeAcceptDeclineOption(final PopupMenu popupMenu){
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                popupMenu.getMenu().findItem(R.id.accept).setVisible(false);
            }
        });
    }
}
