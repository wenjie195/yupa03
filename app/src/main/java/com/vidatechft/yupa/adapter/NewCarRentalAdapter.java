package com.vidatechft.yupa.adapter;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.CurrencyRate;
import com.vidatechft.yupa.classes.SelectableItem;
import com.vidatechft.yupa.classes.Transport;
import com.vidatechft.yupa.holder.SelectableViewHolder;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.List;

public class NewCarRentalAdapter extends RecyclerView.Adapter implements SelectableViewHolder.OnItemSelectedListener {

    private final List<SelectableItem> mValues;
    private boolean isMultiSelectionEnabled = false;
    private MainActivity parentActivity;
    SelectableViewHolder.OnItemSelectedListener listener;
    private String hr = "/hr";
    private String day = "/day";
    private String month = "/month";


    public NewCarRentalAdapter(SelectableViewHolder.OnItemSelectedListener listener,
                             List<Transport> items, boolean isMultiSelectionEnabled) {
        this.listener = listener;
        this.isMultiSelectionEnabled = isMultiSelectionEnabled;

        mValues = new ArrayList<>();
        for (Transport item : items) {
            mValues.add(new SelectableItem(item, false));
        }
    }

    @Override
    public SelectableViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_car_rental, parent, false);

        return new SelectableViewHolder(itemView, this);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        SelectableViewHolder holder = (SelectableViewHolder) viewHolder;
        SelectableItem selectableItem = mValues.get(position);
        String name = selectableItem.id;
        if(selectableItem != null){
            selectableItem.tempId = position;

            if(selectableItem.photoUrl != null){
                GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(selectableItem.photoUrl),holder.carRentalIV);
            }
            if(selectableItem.make != null && selectableItem.model != null){
                String carName = selectableItem.make + " " + selectableItem.model;
                holder.carNameTV.setText(carName);
            }
            if(selectableItem.manufactureDate != null){
                holder.carYearTV.setText(String.valueOf(selectableItem.manufactureDate));
            }
            if(selectableItem.currencyCode != null){
                String localCurrencyCode = GeneralFunction.getCurrencyCode(parentActivity);
                String templatePrice = localCurrencyCode + " ";
                if(selectableItem.perHour != null){
                    String perHourPrice = templatePrice + GeneralFunction.getDecimalFormat(2).format(CurrencyRate.getUserLocalPrice(parentActivity,localCurrencyCode,selectableItem.currencyCode,selectableItem.perHour))+ hr;
                    holder.carRentalPerHourTV.setText(perHourPrice);
                }
                if(selectableItem.perDay != null){
                    String perDayPrice = templatePrice + GeneralFunction.getDecimalFormat(2).format(CurrencyRate.getUserLocalPrice(parentActivity,localCurrencyCode,selectableItem.currencyCode,selectableItem.perDay))+ day;
                    holder.carRentalPerDayTV.setText(perDayPrice);
                }
                if(selectableItem.perMonth != null){
                    String perMonthPrice = templatePrice + GeneralFunction.getDecimalFormat(2).format(CurrencyRate.getUserLocalPrice(parentActivity,localCurrencyCode,selectableItem.currencyCode,selectableItem.perMonth))+ month;
                    holder.carRentalPerMonthTV.setText(perMonthPrice);
                }
            }
        }
//        holder.textView.setText(name);
//        if (isMultiSelectionEnabled) {
//            TypedValue value = new TypedValue();
//            holder.textView.getContext().getTheme().resolveAttribute(android.R.attr.listChoiceIndicatorMultiple, value, true);
//            int checkMarkDrawableResId = value.resourceId;
//            holder.textView.setCheckMarkDrawable(checkMarkDrawableResId);
//        } else {
//            TypedValue value = new TypedValue();
//            holder.textView.getContext().getTheme().resolveAttribute(android.R.attr.listChoiceIndicatorSingle, value, true);
//            int checkMarkDrawableResId = value.resourceId;
//            holder.textView.setCheckMarkDrawable(checkMarkDrawableResId);
//        }
//
//        holder.mItem = selectableItem;
//        holder.setChecked(holder.mItem.isSelected());
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public List<Transport> getSelectedItems() {

        List<Transport> selectedItems = new ArrayList<>();
        for (SelectableItem item : mValues) {
            if (item.isSelected()) {
                selectedItems.add(item);
            }
        }
        return selectedItems;
    }

    @Override
    public int getItemViewType(int position) {
        if(isMultiSelectionEnabled){
            return SelectableViewHolder.MULTI_SELECTION;
        }
        else{
            return SelectableViewHolder.SINGLE_SELECTION;
        }
    }

    @Override
    public void onItemSelected(SelectableItem item) {
        if (!isMultiSelectionEnabled) {

            for (SelectableItem selectableItem : mValues) {
                if (!selectableItem.equals(item)
                        && selectableItem.isSelected()) {
                    selectableItem.setSelected(false);
                } else if (selectableItem.equals(item)
                        && item.isSelected()) {
                    selectableItem.setSelected(true);
                }
            }
            notifyDataSetChanged();
        }
        listener.onItemSelected(item);
    }
}