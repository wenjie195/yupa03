package com.vidatechft.yupa.adapter;

import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.BookingHistory;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.dialogFragments.SubmitMessageToHostDialogFragment;
import com.vidatechft.yupa.hostFragments.HostStayMainPageFragment;
import com.vidatechft.yupa.paymentFragment.SelectTravelKitFragment;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.List;

public class HostStayMyBookingRecyclerGridAdapter extends RecyclerView.Adapter<HostStayMyBookingRecyclerGridAdapter.ViewHolder> {
    public static final String TAG = HostStayMyBookingRecyclerGridAdapter.class.getName();

    private MainActivity parentActivity;
    private List<BookingHistory> bookingHistoryList;


    public HostStayMyBookingRecyclerGridAdapter(MainActivity activity, ArrayList<BookingHistory>bookingHistoryList) {
        this.parentActivity = activity;
        this.bookingHistoryList = bookingHistoryList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout wholeLL;
        public ImageView hostIV;
        public TextView statusTV,hostTV;

        public ViewHolder(View v) {
            super(v);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    wholeLL = itemView.findViewById(R.id.wholeLL);
                    hostIV = itemView.findViewById(R.id.hostIV);
                    statusTV = itemView.findViewById(R.id.statusTV);
                    hostTV = itemView.findViewById(R.id.hostTV);
                }
            });
        }
    }

    @NonNull
    @Override
    public HostStayMyBookingRecyclerGridAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_grid_host_stay_my_booking, parent, false);

        return new HostStayMyBookingRecyclerGridAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final int pos = position;
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final BookingHistory bookingHistory = bookingHistoryList.get(pos);
                if(bookingHistory != null){
                    final Room room = bookingHistory.room;

                    parentActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(room != null){
                                if(room.urlOutlook != null){
                                    GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(room.urlOutlook),holder.hostIV);
                                }else{
                                    holder.hostIV.setScaleType(ImageView.ScaleType.FIT_XY);
                                    GeneralFunction.GlideImageSettingOwnDrawable(parentActivity, R.drawable.ic_not_found,holder.hostIV);
                                }

                                String statusTitle;

                                //from here https://stackoverflow.com/questions/32409964/get-color-resource-as-string/32410035
                                if(bookingHistory.actionType != null){
                                    switch (bookingHistory.actionType){
                                        case BookingHistory.ACTION_TYPE_ACCEPTED:
                                            if(bookingHistory.isPaid != null && bookingHistory.isPaid){
                                                statusTitle = "<strong><font color=\"" + Color.parseColor("#"+Integer.toHexString(parentActivity.getResources().getColor(R.color.booked_green))) +"\">"
                                                        + parentActivity.getString(R.string.booked)
                                                        + "</font></strong> ";
                                            }else{
                                                statusTitle = "<strong><font color=\"" + Color.parseColor("#"+Integer.toHexString(parentActivity.getResources().getColor(R.color.pending_orange))) +"\">"
                                                        + parentActivity.getString(R.string.waiting_for_payment)
                                                        + "</font></strong> ";
                                            }
                                            break;
                                        case BookingHistory.ACTION_TYPE_REJECTED:
                                            statusTitle = "<strong><font color=\"" + Color.parseColor("#"+Integer.toHexString(parentActivity.getResources().getColor(R.color.red))) +"\">"
                                                    + parentActivity.getString(R.string.rejected)
                                                    + "</font></strong> ";
                                            break;
                                        case BookingHistory.ACTION_TYPE_PENDING:
                                            statusTitle = "<strong><font color=\"" + Color.parseColor("#"+Integer.toHexString(parentActivity.getResources().getColor(R.color.pending_orange))) +"\">"
                                                    + parentActivity.getString(R.string.pending)
                                                    + "</font></strong> ";
                                            break;
                                        default:
                                            statusTitle = "<strong><font color=\"" + Color.parseColor("#"+Integer.toHexString(parentActivity.getResources().getColor(R.color.pending_orange))) +"\">"
                                                    + parentActivity.getString(R.string.pending)
                                                    + "</font></strong> ";
                                            break;
                                    }
                                }else{
                                    statusTitle = "<strong><font color=\"" + Color.parseColor("#"+Integer.toHexString(parentActivity.getResources().getColor(R.color.pending_orange))) +"\">"
                                            + parentActivity.getString(R.string.pending)
                                            + "</font></strong> ";
                                }

                                String title = "???";
                                if(room.accommodationName != null){
                                    title = room.accommodationName;
                                }
                                holder.hostTV.setText(title);

                                if(holder.statusTV != null){
                                    if(!statusTitle.trim().isEmpty()){
                                        holder.statusTV.setText(Html.fromHtml(statusTitle));
                                        holder.statusTV.setVisibility(View.VISIBLE);
                                    }else{
                                        holder.statusTV.setVisibility(View.GONE);
                                    }
                                }

                                holder.wholeLL.setOnClickListener(new DebouncedOnClickListener(500) {
                                    @Override
                                    public void onDebouncedClick(View v) {
                                        if(bookingHistory.actionType != null){
                                            Fragment hostStayMainPageFragment = HostStayMainPageFragment.newInstance(parentActivity,parentActivity.getString(R.string.stays),room,room.id);
                                            switch (bookingHistory.actionType){
                                                case BookingHistory.ACTION_TYPE_ACCEPTED:
                                                    if(bookingHistory.isPaid != null && bookingHistory.isPaid && bookingHistory.paymentId != null){
                                                        Fragment receiptHostStayFragment = HostStayMainPageFragment.newInstance(parentActivity,parentActivity.getString(R.string.stays),room,room.id,bookingHistory,null,true);
                                                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, receiptHostStayFragment).addToBackStack(HostStayMainPageFragment.TAG).commit();
                                                    }else{
                                                        Fragment selectTravelKitFragment = SelectTravelKitFragment.newInstance(parentActivity,null,bookingHistory);
                                                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, selectTravelKitFragment).addToBackStack(SelectTravelKitFragment.TAG).commit();
                                                    }
                                                    break;
                                                case BookingHistory.ACTION_TYPE_REJECTED:
                                                    GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, hostStayMainPageFragment).addToBackStack(HostStayMainPageFragment.TAG).commit();
                                                    break;
                                                case BookingHistory.ACTION_TYPE_PENDING:
                                                    FragmentManager fm = parentActivity.getSupportFragmentManager();
                                                    SubmitMessageToHostDialogFragment submitMessageToHostDialogFragment1 = SubmitMessageToHostDialogFragment.newInstance(parentActivity,bookingHistory,room);
                                                    submitMessageToHostDialogFragment1.show(fm,TAG);
                                                    break;
                                                default:
                                                    GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, hostStayMainPageFragment).addToBackStack(HostStayMainPageFragment.TAG).commit();
                                                    break;
                                            }

                                        }else{
                                            Fragment hostStayMainPageFragment = HostStayMainPageFragment.newInstance(parentActivity,parentActivity.getString(R.string.stays),room,room.id);
                                            GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, hostStayMainPageFragment).addToBackStack(HostStayMainPageFragment.TAG).commit();
                                        }
                                    }
                                });

                            }
                        }
                    });
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return bookingHistoryList.size();
    }
}
