package com.vidatechft.yupa.adapter;

import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.Merchant;
import com.vidatechft.yupa.dialogFragments.MerchantShopDetailsDialogFragment;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.List;

public class MSNearbyStayListAdapter extends RecyclerView.Adapter<MSNearbyStayListAdapter.ViewHolder> {
    public  static final String TAG = MSNearbyStayListAdapter.class.getName();

    private MainActivity parentActivity;
    private List<Merchant> merchants = new ArrayList<>();


    public MSNearbyStayListAdapter(MainActivity parentActivity, List<Merchant> merchants){
        this.parentActivity = parentActivity;
        this.merchants = merchants;

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView shopNameTV, descTV;
        public ConstraintLayout wholeCL;

        public ViewHolder(View v) {
            super(v);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    shopNameTV = itemView.findViewById(R.id.shopNameTV);
                    descTV = itemView.findViewById(R.id.descTV);
                    wholeCL = itemView.findViewById(R.id.wholeCL);

                }
            });
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_merchantshop_nearby_clusterlist, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int pos) {
        final int position = pos;
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Merchant merchant = merchants.get(position);

                holder.wholeCL.setOnClickListener(new DebouncedOnClickListener(500) {
                    @Override
                    public void onDebouncedClick(View v) {
                        if(merchant != null){

                            if(parentActivity != null && !parentActivity.isFinishing() && GeneralFunction.isAppInForground(parentActivity.getApplicationContext())){
                                FragmentManager fm = parentActivity.getSupportFragmentManager();
                                MerchantShopDetailsDialogFragment merchantShopDetailsDialogFragment = MerchantShopDetailsDialogFragment.newInstance(parentActivity, merchants.get(position));
                                merchantShopDetailsDialogFragment.show(fm, TAG);
                            }
                        }
                    }
                });

                if(merchant != null){
                    if(merchant.workPlace != null){
                        holder.shopNameTV.setText(merchant.workPlace);
                    }
                    if (merchant.description != null){
                        holder.descTV.setText(merchant.description);
                    }

                }else{
                    Log.e(TAG,"error getting shop in MSNearbyStayListAdapter");
                }
            }
        });

    }

    @Override
    public int getItemCount() {

        return merchants.size();
    }

}
