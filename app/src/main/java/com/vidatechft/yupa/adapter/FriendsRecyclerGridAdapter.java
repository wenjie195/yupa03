package com.vidatechft.yupa.adapter;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.Friend;
import com.vidatechft.yupa.classes.User;
import com.vidatechft.yupa.friends.FriendListFragment;
import com.vidatechft.yupa.itinerary.ItineraryDetailFragment;
import com.vidatechft.yupa.itinerary.StartPlanItineraryFragment;
import com.vidatechft.yupa.profileFragments.ProfileFragment;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;

public class FriendsRecyclerGridAdapter extends RecyclerView.Adapter<FriendsRecyclerGridAdapter.ViewHolder>  {
    public static final String TAG = FriendsRecyclerGridAdapter.class.getName();
    private ArrayList<Friend> friendList;
    private MainActivity parentActivity;
    private FriendListFragment parentFragment;

    public FriendsRecyclerGridAdapter(MainActivity activity, FriendListFragment parentFragment, ArrayList<Friend> friendList) {
        this.parentActivity = activity;
        this.friendList = friendList;
        this.parentFragment = parentFragment;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout wholeLL;
        public ImageView friendIV;
        public TextView friendTV;

        public ViewHolder(View v) {
            super(v);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    wholeLL = itemView.findViewById(R.id.wholeLL);
                    friendIV = itemView.findViewById(R.id.friendIV);
                    friendTV = itemView.findViewById(R.id.friendTV);
                }
            });
        }
    }

    @NonNull
    @Override
    public FriendsRecyclerGridAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_grid_friend, parent, false);

        return new FriendsRecyclerGridAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int pos) {
        final int position = pos;
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Friend friend = friendList.get(position);

                parentActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(friend != null && friend.user != null){
                            final User user = friend.user;
                            if(user.profilePicUrl != null && !user.profilePicUrl.isEmpty()){
                                GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(user.profilePicUrl),holder.friendIV);
                            }else{
                                holder.friendIV.setScaleType(ImageView.ScaleType.FIT_XY);
                                GeneralFunction.GlideImageSettingOwnDrawable(parentActivity, R.drawable.ic_profile_placeholder,holder.friendIV);
                            }

                            if(user.name != null){
                                holder.friendTV.setText(user.name);
                            }else{
                                holder.friendTV.setText(parentActivity.getString(R.string.no_name));
                            }

                            holder.wholeLL.setOnClickListener(new DebouncedOnClickListener(500) {
                                @Override
                                public void onDebouncedClick(View v) {
                                    if(TextUtils.equals(user.id,parentActivity.uid)){
                                        Fragment profileFragment = ProfileFragment.newInstance(parentActivity,parentFragment,user,true,position);
                                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, profileFragment).addToBackStack(ProfileFragment.TAG).commit();
                                    }else{
                                        Fragment profileFragment = ProfileFragment.newInstance(parentActivity,parentFragment,user,false,position);
                                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, profileFragment).addToBackStack(ProfileFragment.TAG).commit();
                                    }
                                }
                            });
                        }
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return friendList.size();
    }
}