package com.vidatechft.yupa.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TextView;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.Payment;
import com.vidatechft.yupa.classes.TravelKit;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

public class TravelKitReceiptAdapter extends RecyclerView.Adapter<TravelKitReceiptAdapter.ViewHolder>  {
    public static final String TAG = TravelKitReceiptAdapter.class.getName();
    private MainActivity parentActivity;
    private Fragment parentFragment;
    private Payment payment;
    private ArrayList<TravelKit> tkList;

    public TravelKitReceiptAdapter(MainActivity parentActivity, Fragment parentFragment, Payment payment, ArrayList<TravelKit> tkList) {
        this.parentActivity = parentActivity;
        this.parentFragment = parentFragment;
        this.payment = payment;
        this.tkList = tkList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tkNameTV,tkPriceTV,tkProductPriceLabelTV;
        public TableLayout productTL;

        public ViewHolder(View v) {
            super(v);
            tkNameTV = itemView.findViewById(R.id.tkNameTV);
            tkPriceTV = itemView.findViewById(R.id.tkPriceTV);
            tkProductPriceLabelTV = itemView.findViewById(R.id.tkProductPriceLabelTV);
            productTL = itemView.findViewById(R.id.productTL);
        }
    }

    @NonNull
    @Override
    public TravelKitReceiptAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_travelkit_receipt, parent, false);

        return new TravelKitReceiptAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int pos) {
        final int position = pos;
        final TravelKit travelKit = tkList.get(position);

        if(payment != null && travelKit != null){
            if(travelKit.name != null){
                holder.tkNameTV.setText(travelKit.name);

                if(payment.amount != null && payment.paymentAmount != null && payment.paymentAmount.travelKitMap != null && payment.paymentAmount.travelKitMap.get(travelKit.name) != null){
                    String totalPriceText = payment.paymentAmount.currencyCode + GeneralFunction.getDecimalFormat(2).format(payment.paymentAmount.travelKitMap.get(travelKit.name));
                    holder.tkPriceTV.setText(totalPriceText);
                }
            }

            if(travelKit.productList != null && !travelKit.productList.isEmpty()){
                LayoutInflater layoutInflater = (LayoutInflater) parentActivity.getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                String currencyCode = "";
                String priceLabel = holder.tkProductPriceLabelTV.getText().toString();
                Iterator it = travelKit.productList.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry)it.next();
                    String productName = (String) pair.getKey();
                    Double productPrice = (Double) pair.getValue();
                    String productRemark = "";

                    if(travelKit.currencyCode != null){
                        currencyCode = " (" + travelKit.currencyCode + ")";
                    }

                    if(travelKit.productRemark != null && travelKit.productRemark.get(productName) != null){
                        productRemark = travelKit.productRemark.get(productName);
                    }

                    if(travelKit.quantityMap != null && travelKit.quantityMap.get(productName) != null){
                        productName += " x" + String.valueOf(travelKit.quantityMap.get(productName));
                    }

                    View productView = layoutInflater.inflate(R.layout.layout_receipt_travel_kit_product, null);
                    TextView productNameTV = productView.findViewById(R.id.productNameTV);
                    TextView productPriceTV = productView.findViewById(R.id.productPriceTV);
                    TextView productRemarkTV = productView.findViewById(R.id.productRemarkTV);

                    productNameTV.setText(productName);
                    productPriceTV.setText(GeneralFunction.getDecimalFormat(2).format(productPrice));
                    productRemarkTV.setText(productRemark);
                    holder.productTL.addView(productView);
                }

                priceLabel += currencyCode;
                holder.tkProductPriceLabelTV.setText(priceLabel);
            }
        }
    }

    @Override
    public int getItemCount() {
        return tkList.size();
    }
}