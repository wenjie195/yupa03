package com.vidatechft.yupa.adapter;


import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.chatFragments.GroupChatCreateFragment;
import com.vidatechft.yupa.chatFragments.GroupChatHomeFragment;
import com.vidatechft.yupa.chatFragments.GroupChatMessageFragment;
import com.vidatechft.yupa.classes.GroupChat;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class GroupChatAdapter extends RecyclerView.Adapter<GroupChatAdapter.ViewHolder>
{
    private MainActivity parentActivity;
    private Fragment parentFragment;
    private ArrayList<GroupChat> groupChatGroupList;
    private String lastToolbarTitle;

    public GroupChatAdapter(MainActivity parentActivity, Fragment parentFragment, ArrayList<GroupChat> groupChatGroupList, String lastToolbarTitle)
    {
        this.parentActivity = parentActivity;
        this.parentFragment = parentFragment;
        this.groupChatGroupList = groupChatGroupList;
        this.lastToolbarTitle = lastToolbarTitle;
    }

    @NonNull
    @Override
    public GroupChatAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_grid_groupchat, parent, false);
        return new GroupChatAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final GroupChatAdapter.ViewHolder holder , int pos)
    {
        final int position = pos;

        parentActivity.runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                final GroupChat temporaryGC = groupChatGroupList.get(position);

                parentActivity.runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        if(temporaryGC != null)
                        {
                            //todo : To prevent picture error getting null
                            if (temporaryGC.groupChatPic == null)
                            {
                                temporaryGC.groupChatPic = "";
                            }
                        }
                        //todo : Create new group for chat
                        if(temporaryGC.isCreategroup && temporaryGC.isCreategroup != null)
                        {
                            holder.groupNameTV.setText("Add New Group");
                            holder.groupMemberSizeTV.setVisibility(View.GONE);
                            holder.groupIV.setScaleType(ImageView.ScaleType.FIT_XY);
                            holder.groupIV.setColorFilter(Color.argb(255, 255, 255, 255));
                            holder.wholeLL.setBackgroundColor(parentActivity.getResources().getColor(R.color.group_chat_add));
                            GeneralFunction.GlideCircleImageSettingGC(parentActivity, 2,holder.groupIV);
                        }
                        //todo : Get existing chat group
                        else
                        {
                            if(temporaryGC.groupChatPic != null || temporaryGC.groupChatPic != "")
                            {
                                GeneralFunction.GlideCircleImageSetting(parentActivity, Uri.parse(temporaryGC.groupChatPic),holder.groupIV);
                            }
                            if(temporaryGC.groupChatName != null || temporaryGC.groupChatName != "")
                            {
                                holder.groupNameTV.setText(temporaryGC.groupChatName);
                            }
                            holder.groupMemberSizeTV.setText(temporaryGC.groupMemberUid.size()+" Members");
                            holder.addNewGroupIV.setVisibility(View.GONE);
                        }

                        holderListener(holder,temporaryGC);
                    }
                });
            }
        });
    }

    private void holderListener(ViewHolder holder , final GroupChat temporaryGC)
    {
        holder.wholeLL.setOnClickListener(new DebouncedOnClickListener(500)
        {
            @Override
            public void onDebouncedClick(View v)
            {
                //todo : If the user create new group for chat
                if(temporaryGC.isCreategroup)
                {
                    Map<String, Object> currentUserMembershipDetailsTemp = new HashMap<>();
                    //todo : Create a new group fragment
                    Fragment groupChatCreateFragment = GroupChatCreateFragment.newInstance(parentActivity,temporaryGC.isCreategroup,currentUserMembershipDetailsTemp,null);
                    GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, groupChatCreateFragment).addToBackStack(GroupChatCreateFragment.TAG).commit();
                }
                //todo : Get existing chat group
                else
                {
                    //todo : If the user is already a member in the group
                    if(temporaryGC.groupMemberUid.contains(parentActivity.uid))
                    {
                        if(temporaryGC.userMembershipDetails != null)
                        {
                            temporaryGC.currentUserMembershipDetails = temporaryGC.userMembershipDetails.get(parentActivity.uid);

                            //todo : Navigate to GroupChatMessageFragment(OLD : DirectGroupChatFragment)

                            Fragment groupChatMessageFragment = GroupChatMessageFragment.newInstance(parentActivity,temporaryGC.groupChatId,temporaryGC.currentUserMembershipDetails);
                            GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, groupChatMessageFragment).addToBackStack(GroupChatMessageFragment.TAG).commit();
                        }
                    }
                    //todo : Go to Group Homepage if the user is not approved yet
                    //todo : OR
                    //todo : If the user is no a member in the group
                    else
                    {
                        //todo : if the user is not a member of that group and he wants to join the group manually
                        boolean isMember = false;

                        Fragment groupChatHomeFragment = GroupChatHomeFragment.newInstance(parentActivity,isMember,temporaryGC.groupChatId);
                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, groupChatHomeFragment).addToBackStack(GroupChatHomeFragment.TAG).commit();
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return groupChatGroupList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public LinearLayout wholeLL;
        public ImageView groupIV;
        public ImageView addNewGroupIV;
        public TextView groupNameTV,groupMemberSizeTV;

        public ViewHolder(View v)
        {
            super(v);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    wholeLL = itemView.findViewById(R.id.wholeLL);
                    groupIV = itemView.findViewById(R.id.groupIV);
                    addNewGroupIV = itemView.findViewById(R.id.addNewGroupIV);
                    groupMemberSizeTV = itemView.findViewById(R.id.groupMemberSizeTV);
                    groupNameTV = itemView.findViewById(R.id.groupNameTV);
                }
            });
        }
    }
}
