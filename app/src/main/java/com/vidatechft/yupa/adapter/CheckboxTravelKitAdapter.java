package com.vidatechft.yupa.adapter;

import android.annotation.SuppressLint;
import android.content.res.ColorStateList;
import android.support.annotation.NonNull;
import android.support.v4.widget.CompoundButtonCompat;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.CurrencyRate;
import com.vidatechft.yupa.classes.TravelKit;
import com.vidatechft.yupa.paymentFragment.BuyTravelKitFragment;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.HashMap;

import static com.vidatechft.yupa.utilities.GeneralFunction.getDecimalFormat;

public class CheckboxTravelKitAdapter extends RecyclerView.Adapter<CheckboxTravelKitAdapter.ViewHolder> {
    public static final String TAG = CheckboxTravelKitAdapter.class.getName();
    private MainActivity parentActivity;
    private ArrayList<CheckBox> checkBoxes;
    private ArrayList<Double> productList;
    private TravelKit mTravelKit;
    private ArrayList<TravelKit>allTravelKitList;
    private Double mPrice;
    private BuyTravelKitFragment buyTravelKitFragment;
    private String localCurrencyCode;

    private int colorRes;

    public CheckboxTravelKitAdapter(MainActivity parentActivity, ArrayList<CheckBox> checkBoxes, ArrayList<Double> productList, TravelKit mTravelKit, BuyTravelKitFragment buyTravelKitFragment, String localCurrencyCode) {
        this.parentActivity = parentActivity;
        this.checkBoxes = checkBoxes;
        this.productList = productList;
        this.mTravelKit = mTravelKit;
        this.buyTravelKitFragment = buyTravelKitFragment;
        this.localCurrencyCode = localCurrencyCode;
    }

    public CheckboxTravelKitAdapter(MainActivity parentActivity, ArrayList<CheckBox> checkBoxes, ArrayList<Double> productList, ArrayList<TravelKit>allTravelKitList, BuyTravelKitFragment buyTravelKitFragment, String localCurrencyCode) {
        this.parentActivity = parentActivity;
        this.checkBoxes = checkBoxes;
        this.productList = productList;
        this.allTravelKitList = allTravelKitList;
        this.buyTravelKitFragment = buyTravelKitFragment;
        this.localCurrencyCode = localCurrencyCode;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public AppCompatCheckBox checkBox;
        public TextView textView,priceTV,quantityTV;
        private ImageView helpIV,addQuantityIV,removeQuantityIV;

        public ViewHolder(View v) {
            super(v);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    checkBox = itemView.findViewById(R.id.checkBox);
                    textView = itemView.findViewById(R.id.textView);
                    priceTV = itemView.findViewById(R.id.priceTV);
                    quantityTV = itemView.findViewById(R.id.quantityTV);
                    helpIV = itemView.findViewById(R.id.helpIV);
                    addQuantityIV = itemView.findViewById(R.id.addQuantityIV);
                    removeQuantityIV = itemView.findViewById(R.id.removeQuantityIV);
                }
            });
        }
    }

    @NonNull
    @Override
    public CheckboxTravelKitAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_travel_kit_checkbox, parent, false);

        return new CheckboxTravelKitAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final int pos = position;
        parentActivity.runOnUiThread(new Runnable() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public void run() {
                final CheckBox checkBox = checkBoxes.get(pos);
                Double travelKitPrice = null;
                if(productList != null){
                    travelKitPrice = productList.get(pos);
                }

                TravelKit tempTK;
                if(allTravelKitList != null && allTravelKitList.get(pos) != null){
                    tempTK = allTravelKitList.get(pos);
                }else{
                    tempTK = mTravelKit;
                }

                final TravelKit travelKit = tempTK;

                if(travelKit == null || checkBox == null){
                    return;
                }

                if(!checkBox.getText().toString().isEmpty()){

                    HashMap<String, HashMap<String,Long>> tempQuantityMap = buyTravelKitFragment.getQuantityMap();
                    if(tempQuantityMap.get(travelKit.name) != null && tempQuantityMap.get(travelKit.name).get(checkBox.getText().toString()) != null){
                        holder.quantityTV.setText(String.valueOf(tempQuantityMap.get(travelKit.name).get(checkBox.getText().toString())));
                    }

                    //set checkbox border colour from here https://stackoverflow.com/questions/34340256/how-to-change-checkbox-checked-color-programmatically
                    ColorStateList colorStateList = new ColorStateList(
                            new int[][] {
                                    new int[] { -android.R.attr.state_checked }, // unchecked
                                    new int[] {  android.R.attr.state_checked }  // checked
                            },
                            new int[] {
                                    parentActivity.getResources().getColor(R.color.colorPrimary),
                                    parentActivity.getResources().getColor(R.color.colorPrimary)
                            }
                    );

                    if(colorRes != 0){
                        colorStateList = new ColorStateList(
                                new int[][] {
                                        new int[] { -android.R.attr.state_checked }, // unchecked
                                        new int[] {  android.R.attr.state_checked }  // checked
                                },
                                new int[] {
                                        colorRes,
                                        colorRes
                                }
                        );

                        holder.textView.setTextColor(colorRes);
                        holder.checkBox.setTextColor(colorRes);
                    }

                    CompoundButtonCompat.setButtonTintList(holder.checkBox, colorStateList);
                    holder.textView.setText(checkBox.getText().toString());
                    if(checkBox.isChecked()){
                        holder.checkBox.setChecked(true);
                    }else{
                        holder.checkBox.setChecked(false);
                    }
                }

                if(holder.checkBox.isChecked()){
                    updateQuantityMap(holder,travelKit.name,checkBox.getText().toString(),travelKitPrice,true,null);
                }

                final Double finalTravelKitPrice = travelKitPrice;
                holder.checkBox.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        checkBoxes.get(pos).setChecked(isChecked);
                        if(checkBoxes.get(pos).isChecked()){
                            if(finalTravelKitPrice != null){
                                updateQuantityMap(holder,travelKit.name,checkBox.getText().toString(),finalTravelKitPrice,true,null);
                            }
                        }
                        else{
                            if(finalTravelKitPrice != null){
                                updateQuantityMap(holder,travelKit.name,checkBox.getText().toString(),finalTravelKitPrice,false,null);
                            }
                        }
                    }
                });
                holder.textView.setOnClickListener(new DebouncedOnClickListener(500) {
                    @Override
                    public void onDebouncedClick(View v) {
                        if(checkBoxes.get(pos).isChecked()){
                            holder.checkBox.setChecked(false);
                        }
                        else{
                            holder.checkBox.setChecked(true);
                        }

                        if(holder.checkBox.isChecked()){

                        }
                    }
                });

                if(travelKit.productRemark != null && !travelKit.productRemark.isEmpty() && travelKit.productRemark.get(checkBox.getText().toString()) != null){
                    holder.helpIV.setVisibility(View.VISIBLE);
                    holder.helpIV.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            GeneralFunction.openMessageNotificationDialog(parentActivity,travelKit.productRemark.get(checkBox.getText().toString()),R.drawable.notice_good,TAG);
                        }
                    });
                }else{
                    holder.helpIV.setVisibility(View.GONE);
                }

                if(travelKitPrice != null){
                    String convertedPrice = getDecimalFormat(2).format(CurrencyRate.getUserLocalPrice(parentActivity,localCurrencyCode,travelKit.currencyCode,travelKitPrice));
//                    String mCurrencyValue = localCurrencyCode + " " + convertedPrice;
                    holder.priceTV.setText(convertedPrice);
                }
//                if(travelKitPrice != null && mTravelKit != null){
//                    String convertedPrice = getDecimalFormat(2).format(CurrencyRate.getUserLocalPrice(parentActivity,localCurrencyCode,mTravelKit.currencyCode,travelKitPrice));
////                    String mCurrencyValue = localCurrencyCode + " " + convertedPrice;
//                    holder.priceTV.setText(convertedPrice);
//                }

                holder.removeQuantityIV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int quantity = Integer.parseInt(holder.quantityTV.getText().toString()) - 1;
                        if(quantity < 1){
                            holder.quantityTV.setText("1");
                        }else{
                            holder.quantityTV.setText(String.valueOf(quantity));
                            if(checkBox.isChecked()){
                                updateQuantityMap(holder,travelKit.name,checkBox.getText().toString(),finalTravelKitPrice,checkBox.isChecked(),false);
                            }
                        }
                    }
                });

                holder.addQuantityIV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int quantity = Integer.parseInt(holder.quantityTV.getText().toString()) + 1;
                        if(quantity > 99){
                            holder.quantityTV.setText("99");
                        }else{
                            holder.quantityTV.setText(String.valueOf(quantity));
                            if(checkBox.isChecked()){
                                updateQuantityMap(holder,travelKit.name,checkBox.getText().toString(),finalTravelKitPrice,checkBox.isChecked(),true);
                            }
                        }
                    }
                });
            }
        });
    }

    private void updateQuantityMap(CheckboxTravelKitAdapter.ViewHolder holder, String travelKitName, String productName, Double finalTravelKitPrice, boolean isAdd, Boolean isIncreased){
        int quantity = Integer.parseInt(holder.quantityTV.getText().toString());
        double price;

        if(isIncreased == null){
            price = ( CurrencyRate.getUserLocalPrice(parentActivity,localCurrencyCode,mTravelKit.currencyCode, finalTravelKitPrice) ) * quantity;
            String convertedPrice = getDecimalFormat(2).format(price);
            buyTravelKitFragment.setTravelKitTotalPrice(Double.parseDouble(convertedPrice), isAdd);
        }else{
            if(isIncreased){
                //minus the original 1 to 0
                price = ( CurrencyRate.getUserLocalPrice(parentActivity,localCurrencyCode,mTravelKit.currencyCode, finalTravelKitPrice) ) * (quantity - 1);
                String convertedPrice = getDecimalFormat(2).format(price);
                buyTravelKitFragment.setTravelKitTotalPrice(Double.parseDouble(convertedPrice), false);

                //add back with the newest final price
                price = ( CurrencyRate.getUserLocalPrice(parentActivity,localCurrencyCode,mTravelKit.currencyCode, finalTravelKitPrice) ) * quantity;
                convertedPrice = getDecimalFormat(2).format(price);
                buyTravelKitFragment.setTravelKitTotalPrice(Double.parseDouble(convertedPrice), true);
            }else{
                //minus the original 1 to 0
                price = ( CurrencyRate.getUserLocalPrice(parentActivity,localCurrencyCode,mTravelKit.currencyCode, finalTravelKitPrice) ) * (quantity + 1);
                String convertedPrice = getDecimalFormat(2).format(price);
                buyTravelKitFragment.setTravelKitTotalPrice(Double.parseDouble(convertedPrice), false);

                //add back with the newest final price
                price = ( CurrencyRate.getUserLocalPrice(parentActivity,localCurrencyCode,mTravelKit.currencyCode, finalTravelKitPrice) ) * quantity;
                convertedPrice = getDecimalFormat(2).format(price);
                buyTravelKitFragment.setTravelKitTotalPrice(Double.parseDouble(convertedPrice), true);
            }
        }

        HashMap<String, HashMap<String,Long>> tempQuantityMap = buyTravelKitFragment.getQuantityMap();
        if(tempQuantityMap.get(travelKitName) == null){
            tempQuantityMap.put(travelKitName,new HashMap<String, Long>());
        }
        tempQuantityMap.get(travelKitName).put(productName, (long) quantity);
        buyTravelKitFragment.setQuantityMap(tempQuantityMap);
    }

    public void setColor(int colorRes){
        this.colorRes = colorRes;
    }

    @Override
    public int getItemCount() {
        return checkBoxes.size();
    }

    public ArrayList<CheckBox> getCheckBoxes() {
        return checkBoxes;
    }

}
