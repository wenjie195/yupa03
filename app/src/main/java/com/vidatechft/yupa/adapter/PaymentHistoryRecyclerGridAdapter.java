package com.vidatechft.yupa.adapter;

import android.content.DialogInterface;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.BookingHistory;
import com.vidatechft.yupa.classes.Itinerary;
import com.vidatechft.yupa.classes.Payment;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.classes.TravelKit;
import com.vidatechft.yupa.hostFragments.HostStayMainPageFragment;
import com.vidatechft.yupa.itinerary.ItineraryDetailFragment;
import com.vidatechft.yupa.paymentFragment.SelectTravelKitFragment;
import com.vidatechft.yupa.travelKit.TravelKitDetailFragment;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;

public class PaymentHistoryRecyclerGridAdapter extends RecyclerView.Adapter<PaymentHistoryRecyclerGridAdapter.ViewHolder>  {
    public static final String TAG = PaymentHistoryRecyclerGridAdapter.class.getName();
    private ArrayList<Payment> paymentList;
    private MainActivity parentActivity;
    private Fragment parentFragment;

    public PaymentHistoryRecyclerGridAdapter(MainActivity activity, Fragment parentFragment, ArrayList<Payment> paymentList) {
        this.parentFragment = parentFragment;
        this.parentActivity = activity;
        this.paymentList = paymentList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public CardView wholeCV;
        public ImageView imageView;
        public TextView statusTV,textView;

        public ViewHolder(View v) {
            super(v);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    wholeCV = itemView.findViewById(R.id.wholeCV);
                    imageView = itemView.findViewById(R.id.imageView);
                    statusTV = itemView.findViewById(R.id.statusTV);
                    textView = itemView.findViewById(R.id.textView);
                }
            });
        }
    }

    @NonNull
    @Override
    public PaymentHistoryRecyclerGridAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_grid_itinerary, parent, false);

        return new PaymentHistoryRecyclerGridAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int pos) {
        final int position = pos;
        final Payment payment = paymentList.get(position);

        if(payment == null){
            return;
        }

        String statusTitle;
        boolean isPaid;
        if(payment.status != null && TextUtils.equals(payment.status,Payment.STATUS_SUCCESS)){
            statusTitle = "<strong><font color=\"" + Color.parseColor("#"+Integer.toHexString(parentActivity.getResources().getColor(R.color.booked_green))) +"\">"
                    + parentActivity.getString(R.string.paid)
                    + "</font></strong> ";
            isPaid = true;
        }else{
            statusTitle = "<strong><font color=\"" + Color.parseColor("#"+Integer.toHexString(parentActivity.getResources().getColor(R.color.red))) +"\">"
                    + parentActivity.getString(R.string.payment_failed)
                    + "</font></strong> ";
            isPaid = false;
        }

        if(payment.itinerary != null){
            setAsItinerary(holder,payment.itinerary, payment,isPaid);
        }else if(payment.bookingHistory != null){
            setAsHomestay(holder,payment.bookingHistory,payment,isPaid);
        }else if(payment.travelKits != null && !payment.travelKits.isEmpty()){
            setAsTravelKit(holder,payment.travelKits,isPaid,payment);
        }else{
            holder.textView.setText("???");
            return;
        }

        if(holder.statusTV != null){
            holder.statusTV.setVisibility(View.VISIBLE);
            holder.statusTV.setText(Html.fromHtml(statusTitle));
        }

    }

    private void setAsItinerary(ViewHolder holder, final Itinerary itinerary, final Payment payment, boolean isPaid){
        if(itinerary.itineraryPicUrl != null){
            GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(itinerary.itineraryPicUrl),holder.imageView);
        }else{
            holder.imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            GeneralFunction.GlideImageSettingOwnDrawable(parentActivity, R.drawable.ic_not_found,holder.imageView);
        }

        String title = GeneralFunction.getItineraryTitle(itinerary);
        if(title == null){
            title = "???";
        }
        holder.textView.setText(title);

        //because in payment data the itinerary payment id, isfromadmin and ispaid wont be updated, so manually do here
        itinerary.paymentId = payment.id;
        itinerary.isFromAdmin = true;
        itinerary.isPaid = isPaid;

        holder.wholeCV.setOnClickListener(new DebouncedOnClickListener(500) {
            @Override
            public void onDebouncedClick(View v) {
                Fragment itineraryDetailFragment = ItineraryDetailFragment.newInstance(parentActivity,itinerary,payment);
                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, itineraryDetailFragment).addToBackStack(ItineraryDetailFragment.TAG).commit();
            }
        });
    }

    private void setAsHomestay(ViewHolder holder, final BookingHistory bookingHistory, final Payment payment, final boolean isPaid){
        if(bookingHistory == null){
            return;
        }
        //because in payment data the booking history's payment id and ispaid wont be updated, so manually do here
        bookingHistory.paymentId = payment.id;
        bookingHistory.isPaid = isPaid;

        if(bookingHistory.paymentId != null){
            final Room room = bookingHistory.room;

            if(room == null){
                holder.textView.setText("???");
                return;
            }

            if(room.urlOutlook != null){
                GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(room.urlOutlook),holder.imageView);
            }else{
                holder.imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                GeneralFunction.GlideImageSettingOwnDrawable(parentActivity, R.drawable.ic_not_found,holder.imageView);
            }

            String title = "???";
            if(room.accommodationName != null){
                title = room.accommodationName;
            }
            holder.textView.setText(title);

            holder.wholeCV.setOnClickListener(new DebouncedOnClickListener(500) {
                @Override
                public void onDebouncedClick(View v) {
                    if(isPaid){
                        Fragment receiptHostStayFragment = HostStayMainPageFragment.newInstance(parentActivity,parentActivity.getString(R.string.stays),room,room.id,bookingHistory,payment,true);
                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, receiptHostStayFragment).addToBackStack(HostStayMainPageFragment.TAG).commit();
                    }else{
                        Fragment selectTravelKitFragment = SelectTravelKitFragment.newInstance(parentActivity,null,bookingHistory);
                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, selectTravelKitFragment).addToBackStack(SelectTravelKitFragment.TAG).commit();
                    }
                }
            });
        }
    }

    private void setAsTravelKit(ViewHolder holder, final ArrayList<TravelKit> travelKits, final boolean isPaid, final Payment payment){
        TravelKit tempTk = travelKits.get(0);
        if(tempTk.coverPhotoUrl != null){
            GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(tempTk.coverPhotoUrl),holder.imageView);
        }else{
            holder.imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            GeneralFunction.GlideImageSettingOwnDrawable(parentActivity, R.drawable.ic_not_found,holder.imageView);
        }

        if(tempTk.name != null){
            holder.textView.setText(tempTk.name);
        }else{
            holder.textView.setText("???");
        }

        holder.wholeCV.setOnClickListener(new DebouncedOnClickListener(500) {
            @Override
            public void onDebouncedClick(View v) {
                if(isPaid){
                    Fragment travelKitDetailFragment = TravelKitDetailFragment.newInstance(parentActivity,payment);
                    GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, travelKitDetailFragment).addToBackStack(TravelKitDetailFragment.TAG).commit();
                }else{
                    BookingHistory tempBookingHistory = null;
                    TravelKit travelKit = null;
                    Fragment selectTravelKitFragment = SelectTravelKitFragment.newInstance(parentActivity,travelKit,tempBookingHistory);
                    GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, selectTravelKitFragment).addToBackStack(SelectTravelKitFragment.TAG).commit();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return paymentList.size();
    }
}