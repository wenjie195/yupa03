package com.vidatechft.yupa.adapter;

import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.AppliedCandidate;
import com.vidatechft.yupa.classes.Chat;
import com.vidatechft.yupa.classes.CurrencyRate;
import com.vidatechft.yupa.classes.Job;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.classes.Search;
import com.vidatechft.yupa.dialogFragments.ApplyPTJobDialogFragment;
import com.vidatechft.yupa.dialogFragments.HostRoomClusterListDialogFragment;
import com.vidatechft.yupa.employerFragments.PTJobSwipeFragment;
import com.vidatechft.yupa.hostFragments.HostStayMainPageFragment;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;

public class HostRoomMapClusterListAdapter extends RecyclerView.Adapter<HostRoomMapClusterListAdapter.ViewHolder>  {
    public static final String TAG = HostRoomMapClusterListAdapter.class.getName();

    private MainActivity parentActivity;
    private HostRoomClusterListDialogFragment parentFragment;
    private ArrayList<Room> roomDataset;
    private Search search;

    public HostRoomMapClusterListAdapter(MainActivity parentActivity, HostRoomClusterListDialogFragment parentFragment, ArrayList<Room> roomDataset, Search search) {
        this.parentActivity = parentActivity;
        this.parentFragment = parentFragment;
        this.roomDataset = roomDataset;
        this.search = search;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView roomPriceTV,roomNameTV;
        public ImageView roomIV;
        public ConstraintLayout wholeCL;

        public ViewHolder(View v) {
            super(v);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    roomIV = itemView.findViewById(R.id.roomIV);
                    roomPriceTV = itemView.findViewById(R.id.roomPriceTV);
                    roomNameTV = itemView.findViewById(R.id.roomNameTV);
                    wholeCL = itemView.findViewById(R.id.wholeCL);
                }
            });
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_host_room_cluster_list, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int pos) {
        final int position = pos;
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Room room = roomDataset.get(position);

                holder.wholeCL.setOnClickListener(new DebouncedOnClickListener(500) {
                    @Override
                    public void onDebouncedClick(View v) {
                        if(room != null){
                            if(parentActivity != null && !parentActivity.isFinishing() && GeneralFunction.isAppInForground(parentActivity.getApplicationContext())){
                                if(parentFragment != null){
                                    parentFragment.dismiss();
                                }

                                Handler handler = new Handler();
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        Fragment hostStayMainPageFragment = HostStayMainPageFragment.newInstance(parentActivity,parentActivity.getString(R.string.itinerary_action_toolbar1),room,room.id,search);
                                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, hostStayMainPageFragment).addToBackStack(HostStayMainPageFragment.TAG).commit();
                                    }
                                });
                            }
                        }else{
                            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.no_host_room_found),R.drawable.notice_bad,TAG);
                        }
                    }
                });

                if(room != null){
                    if(room.currencyValue != null && room.currencyType != null){
                        String localCurrencyCode = GeneralFunction.getCurrencyCode(parentActivity);
                        String price;
                        try{
                            price = localCurrencyCode + GeneralFunction.getDecimalFormat(2).format(CurrencyRate.getUserLocalPrice(parentActivity,localCurrencyCode,room.currencyType,room.currencyValue));
                        }catch (Exception e){
                            Log.e(TAG,e.getMessage());
                            price = room.currencyType + room.currencyValue;
                        }

                        holder.roomPriceTV.setText(price);
                    }

                    if(room.accommodationName != null){
                        holder.roomNameTV.setText(room.accommodationName);
                    }

                    if(room.urlOutlook != null){
                        GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(room.urlOutlook),holder.roomIV);
                    }
                }else{
                    Log.e(TAG,"error getting room in HostRoomMapClusterListAdapter");
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return roomDataset.size();
    }
}