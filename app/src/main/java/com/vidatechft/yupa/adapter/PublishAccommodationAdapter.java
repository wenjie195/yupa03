package com.vidatechft.yupa.adapter;

import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.BookHomestayDetails;
import com.vidatechft.yupa.classes.BookingHistory;
import com.vidatechft.yupa.classes.User;
import com.vidatechft.yupa.profileFragments.ProfileFragment;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;
import com.vidatechft.yupa.utilities.MillisecondsToDate;

import java.util.List;

public class PublishAccommodationAdapter extends RecyclerView.Adapter<PublishAccommodationAdapter.ViewHolder> {
    public static final String TAG = PublishAccommodationAdapter.class.getName();
    private MainActivity parentActivity;
    private Fragment fragment;
    private List<BookingHistory> bookHomestayDetailsOri;
    private String guestType;

    public PublishAccommodationAdapter(MainActivity activity, Fragment parentFragment, List<BookingHistory> bookHomestayDetailsOri, String guestType) {
        this.fragment = parentFragment;
        this.parentActivity = activity;
        this.bookHomestayDetailsOri = bookHomestayDetailsOri;
        this.guestType = guestType;

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView currentGuestName, isPaidTV,currentGuestState,currentGuestCounry,currentGuestContactNumber,currentGuest_checkDateTV;
        public ImageView currentGuestProfileImg;
        public LinearLayout currentGuestProfileLL;
        public Button btn_currentGuestViewDetail,btn_upcomingGuestCancleRequest;

        public ViewHolder(View v) {
            super(v);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    currentGuestName = itemView.findViewById(R.id.currentGuestName);
                    currentGuestProfileImg = itemView.findViewById(R.id.currentGuestProfileImg);
                    isPaidTV = itemView.findViewById(R.id.isPaidTV);
                    currentGuestState = itemView.findViewById(R.id.currentGuestState);
                    currentGuestCounry = itemView.findViewById(R.id.currentGuestCounry);
                    currentGuestContactNumber = itemView.findViewById(R.id.currentGuestContactNumber);
                    currentGuest_checkDateTV = itemView.findViewById(R.id.currentGuest_checkDateTV);
                    btn_currentGuestViewDetail = itemView.findViewById(R.id.btn_currentGuestViewDetail);
                    btn_upcomingGuestCancleRequest = itemView.findViewById(R.id.btn_upcomingGuestCancleRequest);
                }
            });
        }
    }

    @NonNull
    @Override
    public PublishAccommodationAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_publish_accommodation, parent, false);

        return new PublishAccommodationAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final PublishAccommodationAdapter.ViewHolder holder, final int position) {

       final BookingHistory bookHomestayDetails = bookHomestayDetailsOri.get(position);
       //Todo button cancel request button
//        if(guestType.equals(BookHomestayDetails.UPCOMING_GUEST)){
//            if(holder.btn_upcomingGuestCancleRequest.getVisibility() == View.GONE ){
//                holder.btn_upcomingGuestCancleRequest.setVisibility(View.VISIBLE);
//            }
//        }

       if(bookHomestayDetails != null){
           if(bookHomestayDetails.isPaid == null || !bookHomestayDetails.isPaid){
               String statusTitle = "<strong><font color=\"" + Color.parseColor("#"+Integer.toHexString(parentActivity.getResources().getColor(R.color.red))) +"\">"
                       + parentActivity.getString(R.string.havent_paid_yet)
                       + "</font></strong> ";
               holder.isPaidTV.setText(Html.fromHtml(statusTitle));
               holder.isPaidTV.setVisibility(View.VISIBLE);
           }

           if(bookHomestayDetails.userId != null){
               GeneralFunction.getFirestoreInstance()
                       .collection(User.URL_USER)
                       .document(bookHomestayDetails.userId)
                       .get()
                       .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                           @Override
                           public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                               if(task.isSuccessful() && task.getResult().exists()){
                                   User user = new User(parentActivity,task.getResult());
                                   if(user.profilePicUrl!= null){
                                       GeneralFunction.GlideCircleImageSetting(parentActivity, Uri.parse(user.profilePicUrl),holder.currentGuestProfileImg);
                                   }
                                   if(user.name != null){
                                       holder.currentGuestName.setText(user.name);
                                   }
                                   if(user.contactNo != null){
                                        holder.currentGuestContactNumber.setText(user.contactNo);
                                   }
                                   if(user.state != null && !TextUtils.isEmpty(user.state)){
                                       String live = "Lived in ";
                                       String comma = ",";
                                       String state = live+user.state + comma;
                                       holder.currentGuestState.setText(state);
                                   }
                                   if(user.country != null){
                                       holder.currentGuestCounry.setText(user.country);
                                   }
                               }
                           }
                       })
                       .addOnFailureListener(new OnFailureListener() {
                           @Override
                           public void onFailure(@NonNull Exception e) {
                               if(e != null){
                                   GeneralFunction.Toast(parentActivity,e.toString());
                               }
                           }
                       });
           }
           if(bookHomestayDetails.checkinDate != null && bookHomestayDetails.checkoutDate != null){
               String checkIn = MillisecondsToDate.convert(bookHomestayDetails.checkinDate);
               String checkOut = MillisecondsToDate.convert(bookHomestayDetails.checkoutDate);
               String checkDate = checkIn +" - "+ checkOut;
               holder.currentGuest_checkDateTV.setText(checkDate);
           }

           GeneralFunction.getFirestoreInstance()
                   .collection(User.URL_USER)
                   .document(bookHomestayDetails.userId)
                   .get()
                   .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                       @Override
                       public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                           if(task.isSuccessful()){
                               final User user = new User(parentActivity, task.getResult());
                               holder.currentGuestProfileImg.setOnClickListener(new DebouncedOnClickListener(500) {
                                   @Override
                                   public void onDebouncedClick(View v) {
                                       if(bookHomestayDetails.userId.equals( parentActivity.uid)){
                                           Fragment profileFragment = ProfileFragment.newInstance(parentActivity,user,true);
                                           GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, profileFragment).addToBackStack(TAG).commit();
                                       }
                                       else{
                                           Fragment profileFragment = ProfileFragment.newInstance(parentActivity,user,false);
                                           GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, profileFragment).addToBackStack(TAG).commit();
                                       }
                                   }
                               });
                               holder.btn_currentGuestViewDetail.setOnClickListener(new DebouncedOnClickListener(500) {
                                   @Override
                                   public void onDebouncedClick(View v) {
                                       if(bookHomestayDetails.userId.equals( parentActivity.uid)){
                                           Fragment profileFragment = ProfileFragment.newInstance(parentActivity,user,true);
                                           GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, profileFragment).addToBackStack(TAG).commit();
                                       }
                                       else{
                                           Fragment profileFragment = ProfileFragment.newInstance(parentActivity,user,false);
                                           GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, profileFragment).addToBackStack(TAG).commit();
                                       }
                                   }
                               });
                           }
                       }
                   });

       }
    }




    @Override
    public int getItemCount() {
     return bookHomestayDetailsOri.size();
    }
}
