package com.vidatechft.yupa.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.Job;
import com.vidatechft.yupa.employerFragments.PTJobMapFragment;
import com.vidatechft.yupa.employerFragments.PTJobSwipeFragment;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;
import com.yuyakaido.android.cardstackview.CardStackView;

import java.util.ArrayList;
import java.util.List;

public class PTJobSwiperAdapterNew extends RecyclerView.Adapter<PTJobSwiperAdapterNew.ViewHolder> {
    public final String TAG = PTJobSwiperAdapterNew.class.getName();
    private LayoutInflater inflater;
    private ArrayList<Job> jobsList;
    private PTJobSwipeFragment fragment;
    private MainActivity parentActivity;
    private String lastToolbarTitle;

    public PTJobSwiperAdapterNew(PTJobSwipeFragment fragment, MainActivity parentActivity, ArrayList<Job> jobsList, String lastToolbarTitle) {
        this.inflater = LayoutInflater.from(parentActivity);
        this.jobsList = jobsList;
        this.fragment = fragment;
        this.parentActivity = parentActivity;
        this.lastToolbarTitle = lastToolbarTitle;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(inflater.inflate(R.layout.adapter_pt_job_card, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final Job jobs = jobsList.get(position);
        parentActivity.runOnUiThread(new Runnable() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public void run() {

                holder.cardView.setOnTouchListener(new View.OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
//                        Log.v(TAG, "PARENT TOUCH");

                        holder.earnRV.getParent().requestDisallowInterceptTouchEvent(false);
                        holder.workForRV.getParent().requestDisallowInterceptTouchEvent(false);
                        return false;
                    }
                });

                holder.earnRV.setOnTouchListener(new View.OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
//                        Log.v(TAG, "CHILD TOUCH");

                        // Disallow the touch request for parent scroll on touch of  child view
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        return false;
                    }
                });

                holder.workForRV.setOnTouchListener(new View.OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
//                        Log.v(TAG, "CHILD TOUCH");

                        // Disallow the touch request for parent scroll on touch of  child view
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        return false;
                    }
                });

                if(jobs != null){

                    if(jobs.workspaceName != null){
                        holder.workspaceTV.setText(jobs.workspaceName);
                    }

                    if(jobs.location != null){
                        holder.locationTV.setText(jobs.location);
                    }

                    LinearLayoutManager jobscopeLLM = new LinearLayoutManager(parentActivity, LinearLayoutManager.HORIZONTAL, false);
                    LinearLayoutManager earnlistLLM = new LinearLayoutManager(parentActivity, LinearLayoutManager.HORIZONTAL, false);

                    if(jobs.jobScope != null && jobs.jobScopeIcon != null){
                        PTJobDetailListAdapter jobDetailListAdapter = new
                                PTJobDetailListAdapter(parentActivity,jobs.jobScope,jobs.jobScopeIcon,R.color.red);

                        holder.workForRV.setAdapter(jobDetailListAdapter);
                        holder.workForRV.setLayoutManager(jobscopeLLM);
                    }

                    if(jobs.earnList != null && jobs.earnListIcon != null){
                        PTJobDetailListAdapter jobDetailListAdapter = new
                                PTJobDetailListAdapter(parentActivity,jobs.earnList,jobs.earnListIcon,R.color.green);

                        holder.earnRV.setAdapter(jobDetailListAdapter);
                        holder.earnRV.setLayoutManager(earnlistLLM);
                    }

                }else{
                    Log.e(TAG,"error getting job in ptjobadapter");
                }

                holder.dislikeIV.setOnClickListener(new DebouncedOnClickListener(500) {
                    @Override
                    public void onDebouncedClick(View v) {
                        fragment.swipeLeft();
                    }
                });

                holder.likeIV.setOnClickListener(new DebouncedOnClickListener(500) {
                    @Override
                    public void onDebouncedClick(View v) {
                        fragment.swipeRight();
                    }
                });

                holder.viewBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

//                        ArrayList<Job> tempJobList = new ArrayList<>(jobsList);

                        Fragment ptjobMapFragment = PTJobMapFragment.newInstance(parentActivity,fragment,lastToolbarTitle,jobsList);
                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, ptjobMapFragment).addToBackStack(PTJobMapFragment.TAG).commit();
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return jobsList.size();
    }

    public ArrayList<Job> getJobs() {
        return jobsList;
    }

    public void setJobs(ArrayList<Job> jobsList) {
        this.jobsList = jobsList;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
         TextView locationTV,workspaceTV;
         RecyclerView earnRV, workForRV;
         ImageView likeIV, dislikeIV;
         Button viewBtn;
//        found here https://stackoverflow.com/questions/4490821/scrollview-inside-scrollview/11554823#11554823
//        Allow the recycle view to scroll inside the swipe able card view
         CardView cardView;
        ViewHolder(View view) {
            super(view);
            locationTV = view.findViewById(R.id.locationTV);
            workspaceTV = view.findViewById(R.id.workspaceTV);
            earnRV = view.findViewById(R.id.earnRV);
            workForRV = view.findViewById(R.id.workForRV);
            likeIV = view.findViewById(R.id.likeIV);
            dislikeIV = view.findViewById(R.id.dislikeIV);
            viewBtn = view.findViewById(R.id.viewBtn);
            cardView = view.findViewById(R.id.cardView);
        }
    }

    public Job getJobsFromAdapter(int getJobPostion){
        // minus 1 because it start from 1, becuz normally is start from 0
        return jobsList.get(getJobPostion - 1);
    }

}
