package com.vidatechft.yupa.adapter;

import android.content.DialogInterface;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.blog.BlogMainFragment;
import com.vidatechft.yupa.classes.Blog;
import com.vidatechft.yupa.blog.AddBlogFragment;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;

public class BlogRecyclerGridAdapter extends RecyclerView.Adapter<BlogRecyclerGridAdapter.ViewHolder>  {
    public static final String TAG = BlogRecyclerGridAdapter.class.getName();
    private MainActivity parentActivity;
    private ArrayList<Blog> blogs;
    private boolean isOwnBlog;
    private boolean isFromMainActivity;

    public BlogRecyclerGridAdapter(MainActivity activity, ArrayList<Blog> blogs, boolean isOwnBlog) {
        this.parentActivity = activity;
        this.blogs = blogs;
        this.isOwnBlog = isOwnBlog;
    }

    public BlogRecyclerGridAdapter(MainActivity activity, ArrayList<Blog> blogs, boolean isOwnBlog, boolean isFromMainActivity) {
        this.parentActivity = activity;
        this.blogs = blogs;
        this.isOwnBlog = isOwnBlog;
        this.isFromMainActivity = isFromMainActivity;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public CardView wholeCV;
        public ImageView imageView;
        public TextView textView;

        public ViewHolder(View v) {
            super(v);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    wholeCV = itemView.findViewById(R.id.wholeCV);
                    imageView = itemView.findViewById(R.id.imageView);
                    textView = itemView.findViewById(R.id.textView);
                }
            });
        }
    }

    @NonNull
    @Override
    public BlogRecyclerGridAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        if(isFromMainActivity){
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.adapter_horizontal_grid_itinerary, parent, false);
        }else{
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.adapter_grid_itinerary, parent, false);
        }

        return new BlogRecyclerGridAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int pos) {
        final int position = pos;
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Blog blog = blogs.get(position);

                parentActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(blog != null){
                            if(blog.coverPhoto != null){
                                GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(blog.coverPhoto),holder.imageView);
                            }else{
                                holder.imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                                GeneralFunction.GlideImageSettingOwnDrawable(parentActivity, R.drawable.ic_not_found,holder.imageView);
                            }

                            if(blog.title != null){
                                holder.textView.setText(blog.title);
                            }

                            holder.wholeCV.setOnClickListener(new DebouncedOnClickListener(500) {
                                @Override
                                public void onDebouncedClick(View v) {
                                    if(isOwnBlog){
                                        Fragment addBlogFragment = AddBlogFragment.newInstance(parentActivity,blog);
                                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, addBlogFragment).addToBackStack(AddBlogFragment.TAG).commit();
                                    }else{
                                        Fragment blogMainFragment = BlogMainFragment.newInstance(parentActivity,blog);
                                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, blogMainFragment).addToBackStack(BlogMainFragment.TAG).commit();
                                    }
                                }
                            });

                            holder.wholeCV.setOnLongClickListener(new View.OnLongClickListener() {
                                @Override
                                public boolean onLongClick(View v) {
                                    android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(parentActivity);
                                    alertDialogBuilder.setTitle(parentActivity.getString(R.string.blog_delete_title));
                                    alertDialogBuilder.setMessage(parentActivity.getString(R.string.blog_delete_desc));
                                    alertDialogBuilder.setPositiveButton(parentActivity.getString(R.string.btn_yes), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            GeneralFunction.getFirestoreInstance()
                                                    .collection(Blog.URL_BLOG)
                                                    .document(blog.id)
                                                    .delete();

                                            blogs.remove(position);
                                            notifyItemRemoved(position);

                                            dialog.dismiss();
                                        }
                                    });

                                    alertDialogBuilder.setNegativeButton(parentActivity.getString(R.string.btn_no), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });

                                    android.support.v7.app.AlertDialog alertDialog  = alertDialogBuilder.create();
                                    if( !( parentActivity.isFinishing() && alertDialog.isShowing() ) ){
                                        alertDialog.show();
                                    }
                                    GeneralFunction.vibratePhone(parentActivity);
                                    return false;
                                }
                            });
                        }
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return blogs.size();
    }
}