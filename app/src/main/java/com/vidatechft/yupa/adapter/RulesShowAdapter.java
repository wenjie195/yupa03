package com.vidatechft.yupa.adapter;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.Rules;
import com.vidatechft.yupa.classes.Setting;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RulesShowAdapter extends RecyclerView.Adapter<RulesShowAdapter.ViewHolder> {

    private MainActivity parentActivity;
    private Fragment fragment;
    private List<String> rulesListBoolTrue = new ArrayList<>();
    private List<String> rulesListBoolFalse = new ArrayList<>();
    private HashMap<String,Object> rulesListString = new HashMap<>();
    private boolean isBooleanMethod = false;
    private Rules rules;
    private Boolean isAllow = false;
    private boolean isPromotion = false;
    private List<String> rulesDataStringFinal;



    public RulesShowAdapter(MainActivity activity, Fragment parentFragment, List<String>rulesListBoolTrue, List<String>rulesListBoolFalse, HashMap<String,Object> rulesDataString, boolean isBooleanMethod, Boolean isAllow,boolean isPromotion) {
        this.fragment = parentFragment;
        this.parentActivity = activity;
        this.rulesListBoolTrue = rulesListBoolTrue;
        this.rulesListBoolFalse = rulesListBoolFalse;
        this.rulesListString = rulesDataString;
        this.isBooleanMethod = isBooleanMethod;
        this.isAllow = isAllow;
        this.isPromotion = isPromotion;


    }
    public RulesShowAdapter(MainActivity activity, Fragment parentFragment, List<String>rulesListBoolTrue, List<String>rulesListBoolFalse, List<String> rulesDataStringFinal, boolean isBooleanMethod, Boolean isAllow,boolean isPromotion) {
        this.fragment = parentFragment;
        this.parentActivity = activity;
        this.rulesListBoolTrue = rulesListBoolTrue;
        this.rulesListBoolFalse = rulesListBoolFalse;
        this.rulesDataStringFinal = rulesDataStringFinal;
        this.isBooleanMethod = isBooleanMethod;
        this.isAllow = isAllow;
        this.isPromotion = isPromotion;


    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView rulesTV,rulesSymbol;
        public LinearLayout wholeLL;

        public ViewHolder(View v) {
            super(v);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    rulesTV = itemView.findViewById(R.id.rulesTV);
                    rulesSymbol = itemView.findViewById(R.id.rulesSymbol);
                    wholeLL = itemView.findViewById(R.id.wholeLL);
                }
            });
        }
    }

    @NonNull
    @Override
    public RulesShowAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_grid_rules_show, parent, false);

        return new RulesShowAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final RulesShowAdapter.ViewHolder holder, final int position) {

        if(isBooleanMethod){
            if(isAllow){
                if(rulesListBoolTrue != null){
                    String rule = rulesListBoolTrue.get(position);
                    holder.rulesSymbol.setText("✔ ");
                    convertValue(rule,holder.rulesTV);
                }
            }
            else{
                if(rulesListBoolFalse != null){
                    String rule = rulesListBoolFalse.get(position);
                    holder.rulesSymbol.setText("✖ ");
                    convertValue(rule,holder.rulesTV);
                }
            }
        }
        else{
            if(rulesListString != null && !isPromotion ){
                List<String> data = new ArrayList<>();
                for(Map.Entry<String, Object> entry: rulesListString.entrySet()){
                    String key = entry.getKey();
                    Object value = entry.getValue();
                    if(value == null || value.toString().trim().isEmpty()){
                        continue;
                    }
                    if(key.equals(Rules.CHECK_IN)){
                        String checkIn ="Check in after "+value.toString();
                        data.add(checkIn);
                    }
                    if(key.equals(Rules.CHECK_OUT)){
                        String checkOut ="Check out after "+value.toString();
                        data.add(checkOut);
                    }
                    if(key.equals(Rules.MAXIMUM_NIGHTS)){
                        String maxNight ="Maximum Stay "+value.toString()+" night (s)";
                        data.add(maxNight);
                    }
                    if(key.equals(Rules.MINIMIUM_NIGHTS)){
                        String minNight ="Minimum Stay "+value.toString()+" night (s)";
                        data.add(minNight);
                    }
                    if(key.equals(Setting.APPROVAL_TYPE)){
                        if(value.equals(Setting.myApproval)){
                            String bookingReq ="Booking request needs to be approve by the host";
                            data.add(bookingReq);
                        }
                        else{
                            String bookingReq ="Booking request needs to be approve by the host after payment";
                            data.add(bookingReq);
                        }
                    }
                    if(key.equals(Setting.GUEST_GENDER)){
                        if(value.equals(Setting.guestFemale)){
                            String gender ="Only female guest (s)";
                            data.add(gender);
                        }
                        else if(value.equals(Setting.guestMale)){
                            String gender ="Only male guest (s)";
                            data.add(gender);
                        }
                    }

                }
                if(position < data.size() && data.get(position) != null && !data.get(position).trim().isEmpty()){
                    String rulesString = data.get(position);
                    holder.rulesTV.setText(rulesString);
                }else{
                    holder.wholeLL.setVisibility(View.GONE);
                }
            }
            if(isPromotion && rulesDataStringFinal != null){
                holder.rulesSymbol.setText("");
                String promotion = rulesDataStringFinal.get(position);
                holder.rulesTV.setText(promotion);
            }
        }
    }


    @Override
    public int getItemCount() {
        if(isBooleanMethod){
            if(isAllow){
                return rulesListBoolTrue.size();
            }
            else{
                return rulesListBoolFalse.size();
            }
        }
        else{
            if(isPromotion){
                return rulesDataStringFinal.size();
            }
            else{
                return rulesListString.size();
            }
        }
    }

    private void convertValue(String text, TextView textView){

        if (text.equals(Rules.PET_ALLOWED)) {
            textView.setText("Pet");
        }
        if (text.equals(Rules.COOKING_ALLOWED)) {
            textView.setText("Cooking");
        }

        if (text.equals(Rules.LAUNDRY_ALLOWED)) {
            textView.setText("Laundry");
        }

        if (text.equals(Rules.EVENT_PARTY_ALLOWED)) {
            textView.setText("Event/Parties");
        }

        if (text.equals(Rules.SMOKE_ALLOWED)) {
            textView.setText("Smoking");
        }

        if (text.equals(Rules.NOISY_ALLOWED)) {
            textView.setText("Making noise after 10 pm");
        }

        if (text.equals(Rules.SHOE_ALLOWED)) {
            textView.setText("Wear shoes inside the house");
        }
    }
}
