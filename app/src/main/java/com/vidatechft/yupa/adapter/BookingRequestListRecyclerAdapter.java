package com.vidatechft.yupa.adapter;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.firestore.FieldValue;
import com.vidatechft.yupa.MainSearchFragment.SearchHostRoomFragment;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.BookingHistory;
import com.vidatechft.yupa.classes.Inbox;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.classes.User;
import com.vidatechft.yupa.hostFragments.BookingRequestFragment;
import com.vidatechft.yupa.hostFragments.BookingRequestListFragment;
import com.vidatechft.yupa.hostFragments.HostStayMainPageFragment;
import com.vidatechft.yupa.profileFragments.ProfileFragment;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.HashMap;

public class BookingRequestListRecyclerAdapter extends RecyclerView.Adapter<BookingRequestListRecyclerAdapter.ViewHolder>  {
    public static final String TAG = BookingRequestListRecyclerAdapter.class.getName();
    private MainActivity parentActivity;
    private BookingRequestListFragment parentFragment;
    private ArrayList<BookingHistory> bookingHistories;

    public BookingRequestListRecyclerAdapter(MainActivity parentActivity, BookingRequestListFragment parentFragment, ArrayList<BookingHistory> bookingHistories) {
        this.parentActivity = parentActivity;
        this.parentFragment = parentFragment;
        this.bookingHistories = bookingHistories;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nameTV,liveInTV,dateRangeTV;
        public ImageView profileIV;
        public LinearLayout wholeLL;

        public ViewHolder(View v) {
            super(v);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    nameTV = itemView.findViewById(R.id.nameTV);
                    liveInTV = itemView.findViewById(R.id.liveInTV);
                    dateRangeTV = itemView.findViewById(R.id.dateRangeTV);
                    profileIV = itemView.findViewById(R.id.profileIV);
                    wholeLL = itemView.findViewById(R.id.wholeLL);
                }
            });
        }
    }

    @NonNull
    @Override
    public BookingRequestListRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_booking_request_list, parent, false);

        return new BookingRequestListRecyclerAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int pos) {
        final int position = pos;
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final BookingHistory bookingHistory = bookingHistories.get(position);

                if(bookingHistory == null){
//                    bookingHistories.remove(position);
//                    notifyDataSetChanged();
                    return;
                }

                parentActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        String name = "";
                        if(bookingHistory.user != null){
                            if(bookingHistory.user.name != null){
                                name = bookingHistory.user.name;
                            }else{
                                if(bookingHistory.firstName != null){
                                    name = bookingHistory.firstName;
                                }
                                if(bookingHistory.lastName != null){
                                    name += " " + bookingHistory.lastName;
                                }
                            }

                            if(bookingHistory.user.profilePicUrl != null && !bookingHistory.user.profilePicUrl.isEmpty()){
                                GeneralFunction.GlideImageSettingNoPlaceholder(parentActivity,Uri.parse(bookingHistory.user.profilePicUrl),holder.profileIV);
                            }else{
                                GeneralFunction.GlideImageSettingOwnDrawable(parentActivity,R.drawable.profile_placeholder,holder.profileIV);
                            }

                            if(bookingHistory.user.country != null){
                                if(bookingHistory.user.state != null && !bookingHistory.user.state.isEmpty()){
                                    holder.liveInTV.setText(String.format(parentActivity.getString(R.string.user_live_in_both), bookingHistory.user.state, bookingHistory.user.country));
                                }else{
                                    holder.liveInTV.setText(String.format(parentActivity.getString(R.string.user_live_in_country), bookingHistory.user.country));
                                }
                            }
                        }

                        if(bookingHistory.checkinDate != null && bookingHistory.checkoutDate != null){
                            String dateRange = "";

                            if(bookingHistory.checkinDate.equals(bookingHistory.checkoutDate)){
                                dateRange = GeneralFunction.getSdfWithSlash().format(bookingHistory.checkinDate);
                            }else{
                                dateRange = GeneralFunction.getSdfWithSlash().format(bookingHistory.checkinDate) + " - " + GeneralFunction.getSdfWithSlash().format(bookingHistory.checkoutDate);
                            }

                            holder.dateRangeTV.setText(dateRange);
                        }

                        holder.nameTV.setText(name);

                        holder.wholeLL.setOnClickListener(new DebouncedOnClickListener(500) {
                            @Override
                            public void onDebouncedClick(View v) {
                                Fragment bookingRequestFragment = BookingRequestFragment.newInstance(parentActivity,parentFragment,bookingHistory,position);
                                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, bookingRequestFragment).addToBackStack(BookingRequestFragment.TAG).commit();
                            }
                        });
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return bookingHistories.size();
    }
}