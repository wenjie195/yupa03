package com.vidatechft.yupa.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.InputType;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.Accommodation;
import com.vidatechft.yupa.classes.Blog;
import com.vidatechft.yupa.classes.BlogDiscussion;
import com.vidatechft.yupa.classes.Favourite;
import com.vidatechft.yupa.classes.Host;
import com.vidatechft.yupa.classes.Rating;
import com.vidatechft.yupa.classes.Report;
import com.vidatechft.yupa.classes.User;
import com.vidatechft.yupa.dialogFragments.ReviewReportFragment;
import com.vidatechft.yupa.profileFragments.ProfileFragment;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;
import com.vidatechft.yupa.utilities.KeyboardUtilities;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DiscussionAdapter extends RecyclerView.Adapter<DiscussionAdapter.ViewHolder> {
    public final String TAG = DiscussionAdapter.class.getName();
    private MainActivity parentActivity;
    private Report reports;
    private Blog blogs;
    private Favourite favourite;
    private ArrayList<BlogDiscussion> blogDiscussionArrayList = new ArrayList<>();


    public DiscussionAdapter(MainActivity activity, ArrayList<BlogDiscussion> blogDiscussionArrayList, Blog blogs){
        this.parentActivity = activity;
        this.blogDiscussionArrayList = blogDiscussionArrayList;
        this.blogs = blogs;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView profilePicIV,likeButton,dotIV;
        TextView nameTV,dateTV,discussionTV,replyTV,likeCountTV;
        View menuView;
        PopupMenu popupMenu;

        public ViewHolder(View v) {
            super(v);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //ImageView
                    profilePicIV = itemView.findViewById(R.id.profilePicIV);
                    likeButton = itemView.findViewById(R.id.likeButton);
                    //TextView
                    nameTV = itemView.findViewById(R.id.nameTV);
                    dateTV = itemView.findViewById(R.id.dateTV);
                    discussionTV = itemView.findViewById(R.id.discussionTV);
                    replyTV = itemView.findViewById(R.id.replyTV);
                    likeCountTV = itemView.findViewById(R.id.likeCountTV);
                    dotIV = itemView.findViewById(R.id.dotIV);
                    menuView = itemView.findViewById(R.id.menuView);

                    popupMenu = new PopupMenu(parentActivity,dotIV);
                    popupMenu.getMenuInflater().inflate(R.menu.blog_menu, popupMenu.getMenu());

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        popupMenu.setGravity(Gravity.END);
                    }
                }
            });
        }
    }

    @NonNull
    @Override
    public DiscussionAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_discussion, parent, false);

        return new DiscussionAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final DiscussionAdapter.ViewHolder holder, final int position) {
        final BlogDiscussion tempBlogDiscussion = blogDiscussionArrayList.get(position);
        if (tempBlogDiscussion != null) {
            if(tempBlogDiscussion.descDiscussion != null){
                holder.discussionTV.setText(tempBlogDiscussion.descDiscussion);
            }

            if(tempBlogDiscussion.user != null && tempBlogDiscussion.user.profilePicUrl != null){
                GeneralFunction.GlideImageSetting(parentActivity,Uri.parse(tempBlogDiscussion.user.profilePicUrl),holder.profilePicIV);
            }

            if(tempBlogDiscussion.user != null && tempBlogDiscussion.user.name != null){
                holder.nameTV.setText(tempBlogDiscussion.user.name);
            }

            if(tempBlogDiscussion.dateCreated != null){
                holder.dateTV.setText(GeneralFunction.formatDateToDateWithTime(tempBlogDiscussion.dateCreated.toDate()));
            }

            holder.likeButton.setOnClickListener(new DebouncedOnClickListener(500) {
                @Override
                public void onDebouncedClick(View v) {
                    check_like(blogs,holder,false, tempBlogDiscussion,favourite);
                }
            });

            if(tempBlogDiscussion.likeCount != null){
                String totalLikeCount = GeneralFunction.shortenNumbersFormat((long) tempBlogDiscussion.likeCount);
                holder.likeCountTV.setText(totalLikeCount);
            }

            holder.replyTV.setOnClickListener(new DebouncedOnClickListener(500) {
                @Override
                public void onDebouncedClick(View v) {
                    FragmentManager fm = parentActivity.getSupportFragmentManager();
                    ReviewReportFragment reviewReportFragment = ReviewReportFragment.newInstance(parentActivity,blogs, tempBlogDiscussion);
                    reviewReportFragment.show(fm, TAG);
                }
            });

            holder.likeButton.setBackground(parentActivity.getResources().getDrawable(R.drawable.ic_like_grey));
            if(tempBlogDiscussion.favourite != null){
                if(tempBlogDiscussion.favourite.isLiked != null && tempBlogDiscussion.favourite.isLiked){
                    holder.likeButton.setBackground(parentActivity.getResources().getDrawable(R.drawable.ic_like_color_primary));
                }
            }

            if(tempBlogDiscussion.userId != null && TextUtils.equals(tempBlogDiscussion.userId,parentActivity.uid)){
                holder.menuView.setOnClickListener(new DebouncedOnClickListener(250) {
                    @Override
                    public void onDebouncedClick(View v) {
                        holder.popupMenu.show();
                    }
                });

                holder.popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        String title = String.valueOf(item.getTitle());

                        if(TextUtils.equals(title,parentActivity.getString(R.string.blog_discuss_menu_edit))){
                            editBlogDiscussion(holder.getAdapterPosition());
                        }
                        else if(TextUtils.equals(title,parentActivity.getString(R.string.blog_discuss_menu_delete))){
                            removeBlogDiscussion(holder.getAdapterPosition());
                        }

                        return true;
                    }
                });

                holder.dotIV.setVisibility(View.VISIBLE);
                holder.menuView.setVisibility(View.VISIBLE);
            }else{
                holder.dotIV.setVisibility(View.GONE);
                holder.menuView.setVisibility(View.GONE);
            }

        }

    }

    private void editBlogDiscussion(final int position){
        final BlogDiscussion tempBlogDiscussion = blogDiscussionArrayList.get(position);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(parentActivity);
                LayoutInflater inflater = parentActivity.getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.dialog_edit_blog_discussion, null);

                alertDialogBuilder.setView(dialogView);

                alertDialogBuilder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        KeyboardUtilities.hideSoftKeyboard(parentActivity);
                    }
                });

                alertDialogBuilder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        KeyboardUtilities.hideSoftKeyboard(parentActivity);
                    }
                });

                final android.support.v7.app.AlertDialog alertDialog  = alertDialogBuilder.create();

                final EditText commentET = dialogView.findViewById(R.id.commentET);
                Button cancelBtn = dialogView.findViewById(R.id.cancelBtn);
                Button updateBtn = dialogView.findViewById(R.id.updateBtn);

                if(tempBlogDiscussion.descDiscussion != null){
                    commentET.setText(tempBlogDiscussion.descDiscussion);
                }

                cancelBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                        KeyboardUtilities.hideSoftKeyboard(parentActivity);
                    }
                });

                updateBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Map<String,Object> newDiscussion = new HashMap<>();
                        newDiscussion.put(BlogDiscussion.DESCRIPTION_DISCUSSION,commentET.getText().toString());
                        newDiscussion.put(BlogDiscussion.DATE_UPDATED,FieldValue.serverTimestamp());

                        GeneralFunction.getFirestoreInstance()
                                .collection(BlogDiscussion.URL_BLOG_DISCUSSION)
                                .document(blogDiscussionArrayList.get(position).id)
                                .update(newDiscussion);

//                        blogDiscussionArrayList.get(position).descDiscussion = input.getText().toString();
//                        notifyItemChanged(position);

                        alertDialog.dismiss();
                        KeyboardUtilities.hideSoftKeyboard(parentActivity);
                    }
                });

                if( !( parentActivity.isFinishing() && alertDialog.isShowing() ) ){
                    commentET.setSelection(commentET.getText().toString().length());
                    alertDialog.show();
                    KeyboardUtilities.showSoftKeyboard(parentActivity,commentET);
                }

            }
        });
    }

    private void removeBlogDiscussion(final int position){
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(parentActivity);
                alertDialogBuilder.setTitle(parentActivity.getString(R.string.blog_discuss_confirmation_title));
                alertDialogBuilder.setMessage(parentActivity.getString(R.string.blog_discuss_confirm_delete_desc));
                alertDialogBuilder.setPositiveButton(parentActivity.getString(R.string.btn_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        GeneralFunction.getFirestoreInstance()
                                .collection(BlogDiscussion.URL_BLOG_DISCUSSION)
                                .document(blogDiscussionArrayList.get(position).id)
                                .delete();

                        blogDiscussionArrayList.remove(position);
                        notifyItemRemoved(position);

                        dialog.dismiss();
                    }
                });

                alertDialogBuilder.setNegativeButton(parentActivity.getString(R.string.btn_no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                android.support.v7.app.AlertDialog alertDialog  = alertDialogBuilder.create();
                if( !( parentActivity.isFinishing() && alertDialog.isShowing() ) ){
                    alertDialog.show();
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return blogDiscussionArrayList.size();
    }

    public String stampToDate(long timeMillis){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date(timeMillis);
        return simpleDateFormat.format(date);
    }

    private void handleLikeAction(Blog blog, DiscussionAdapter.ViewHolder holder, boolean forceLike, final BlogDiscussion blogDiscussion, Favourite favourite, boolean gotLike){

        DocumentReference likeRef;
        if(blogs == null ){
            GeneralFunction.openMessageNotificationDialog(parentActivity,"Current blog is null",R.drawable.notice_bad,TAG);
            return;
        }

        if(blogDiscussion == null){
            GeneralFunction.openMessageNotificationDialog(parentActivity,"Report is null",R.drawable.notice_bad,TAG);
            return;
        }
        if(blogDiscussion.favourite == null ){
            blogDiscussion.favourite = new Favourite();
        }

        if(blogDiscussion.favourite.isLiked == null || blogDiscussion.favourite.targetId == null && !gotLike){

            likeRef = GeneralFunction.getFirestoreInstance()
                    .collection(Favourite.URL_FAVOURITE)
                    .document();

            favourite = new Favourite();

            blogDiscussion.favourite.id = likeRef.getId();

            blogDiscussion.favourite.isLiked = true;

        }else{
            likeRef = GeneralFunction.getFirestoreInstance()
                    .collection(Favourite.URL_FAVOURITE)
                    .document(blogDiscussion.favourite.id);

            blogDiscussion.favourite.isLiked = !blogDiscussion.favourite.isLiked;

            favourite = blogDiscussion.favourite;

        }

        if(forceLike){
            blogDiscussion.favourite.isLiked = true;
        }



        favourite.userId = parentActivity.uid;
        favourite.targetId = blogDiscussion.id;
        favourite.targetUid = blogDiscussion.userId;
        favourite.type = Favourite.FAV_TYPE_BLOG_DISCUSSION;
        favourite.isLiked = blogDiscussion.favourite.isLiked;
//        favourite.dateCreated = report.favourite.dateCreated;
        HashMap<String,Object> likeMap = Favourite.toMap(favourite);

        if(favourite.dateCreated == null){
            likeMap.put(Favourite.DATE_CREATED, FieldValue.serverTimestamp());
        }
        likeMap.put(Favourite.DATE_UPDATED, FieldValue.serverTimestamp());
        likeMap.put(Favourite.TARGET_ID,blogDiscussion.id);

        likeRef.set(likeMap)
        .addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                GeneralFunction.Toast(parentActivity,e.toString());
            }
        });
    }

    private void check_like(final Blog blog, final ViewHolder holder, boolean forceLike, final BlogDiscussion blogDiscussion, final Favourite favourite){

        if(blogDiscussion.favourite != null){
            if(blogDiscussion.favourite.isLiked){
                holder.likeButton.setBackground(parentActivity.getResources().getDrawable(R.drawable.ic_like_grey));
//                int likecount = Integer.parseInt(holder.likeCountTV.getText().toString()) - 1;
//                holder.likeCountTV.setText(String.valueOf(likecount));
            }
            else{
                holder.likeButton.setBackground(parentActivity.getResources().getDrawable(R.drawable.ic_like_color_primary));
                if(!holder.likeCountTV.getText().toString().equals("")){
//                    int likecount = Integer.parseInt(holder.likeCountTV.getText().toString()) + 1;
//                    holder.likeCountTV.setText(String.valueOf(likecount));
                }
                else{
//                    holder.likeCountTV.setText("1");
                }
            }
        }
        else{
            holder.likeButton.setBackground(parentActivity.getResources().getDrawable(R.drawable.ic_like_color_primary));
            if(!holder.likeCountTV.getText().toString().equals("")){
//                int likecount = Integer.parseInt(holder.likeCountTV.getText().toString()) + 1;
//                holder.likeCountTV.setText(String.valueOf(likecount));
            }
            else{
//                holder.likeCountTV.setText("1");
            }
        }


        GeneralFunction.getFirestoreInstance()
                .collection(Favourite.URL_FAVOURITE)
                .whereEqualTo(Favourite.TARGET_ID, blogDiscussion.id)
                .whereEqualTo(Favourite.TYPE, Favourite.FAV_TYPE_BLOG_DISCUSSION)
                .whereEqualTo(Favourite.TARGET_UID,blogDiscussion.userId)
                .whereEqualTo(Favourite.USER_ID,parentActivity.uid)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if(task.isSuccessful()){
                            if(task.getResult() != null){
                                Favourite favourite1 = new Favourite(parentActivity,task.getResult());
                                blogDiscussion.favourite = favourite1;
                                if(favourite1.isLiked == null){
                                    handleLikeAction(blog,holder,false, blogDiscussion,favourite1,false);
                                }
                                else{
                                    handleLikeAction(blog,holder,false, blogDiscussion,favourite1,true);
                                }
                            }
                            else{
                                handleLikeAction(blog,holder,false, blogDiscussion,favourite,false);
                            }
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        if(e != null){
                            GeneralFunction.Toast(parentActivity, e.toString());
                        }
                    }
                });
    }

}
