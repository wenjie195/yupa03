package com.vidatechft.yupa.adapter;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.chatFragments.DirectChatFragment;
import com.vidatechft.yupa.classes.Chat;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.List;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder>  {
    public static final String TAG = ChatAdapter.class.getName();

    private List<Chat> chatDataset;
    private MainActivity parentActivity;
    private String lastToolbarTitle;

    public ChatAdapter(MainActivity parentActivity, String lastToolbarTitle, List<Chat> chatDataset) {
        this.parentActivity = parentActivity;
        this.chatDataset = chatDataset;
        this.lastToolbarTitle = lastToolbarTitle;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nameTV,lastMsgTV,dateUpdatedTV;
        public ImageView deleteIV,onlineIV,profilePicIV;
        public ConstraintLayout containerCL;

        public ViewHolder(View v) {
            super(v);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    nameTV = itemView.findViewById(R.id.nameTV);
                    lastMsgTV = itemView.findViewById(R.id.lastMsgTV);
                    dateUpdatedTV = itemView.findViewById(R.id.dateUpdatedTV);
                    deleteIV = itemView.findViewById(R.id.deleteIV);
                    onlineIV = itemView.findViewById(R.id.onlineIV);
                    profilePicIV = itemView.findViewById(R.id.profilePicIV);
                    containerCL = itemView.findViewById(R.id.containerCL);
                }
            });
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_chat_list, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int pos) {
        final int position = pos;
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Chat chat = chatDataset.get(position);

                parentActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(chat != null){
                            if(chat.user != null){
                                if(chat.user.name != null){
                                    holder.nameTV.setText(chat.user.name);
                                }

                                if(chat.user.isOnline != null && chat.user.isOnline){
                                    holder.onlineIV.setImageDrawable(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.rounded_circle,R.color.green));
                                }

                                if(chat.user.profilePicUrl != null && !chat.user.profilePicUrl.isEmpty()){
                                    GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(chat.user.profilePicUrl),holder.profilePicIV);
                                }else{
                                    GeneralFunction.GlideImageSettingOwnDrawable(parentActivity, R.drawable.profile_placeholder,holder.profilePicIV);
                                }
                            }

                            if(chat.lastMsg != null)
                            {
                                if(TextUtils.equals(chat.lastType,"text"))
                                {
                                    holder.lastMsgTV.setText(chat.lastMsg);
                                }
                                else if (TextUtils.equals(chat.lastType,"image"))
                                {
                                    if (chat.lastMsg.isEmpty())
                                    {
                                        holder.lastMsgTV.setText("(photo) ");
                                    }
                                    else
                                    {
                                        holder.lastMsgTV.setText("(photo) "+ chat.lastMsg);
                                    }
                                }
                            }

                            if(chat.dateUpdated != null){
                                holder.dateUpdatedTV.setText(GeneralFunction.formatDateToDateWithTime(chat.dateUpdated.toDate()));
                            }

                        }
                    }
                });

                holder.deleteIV.setOnClickListener(new DebouncedOnClickListener(500) {
                    @Override
                    public void onDebouncedClick(View v) {

                    }
                });

                holder.containerCL.setOnClickListener(new DebouncedOnClickListener(500) {
                    @Override
                    public void onDebouncedClick(View v) {
                        parentActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if(chat != null && chat.id != null && chat.user != null){
                                    Fragment directChatFragment = DirectChatFragment.newInstance(
                                            parentActivity,
                                            lastToolbarTitle,
                                            chat.user,
                                            chat.id);
                                    //always put type -> employer id -> staff id
                                    //this is employer's fragment so put current login uid is correct lo
                                    GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, directChatFragment).addToBackStack(DirectChatFragment.TAG).commit();
                                }else{
                                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.chat_err_entering),R.drawable.notice_bad,TAG);
                                }
                            }
                        });
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return chatDataset.size();
    }
}
