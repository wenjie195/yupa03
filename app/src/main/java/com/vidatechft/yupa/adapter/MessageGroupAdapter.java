package com.vidatechft.yupa.adapter;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.firestore.FirebaseFirestore;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
//import com.vidatechft.yupa.chatFragments.DirectGroupChatFragment;
//import com.vidatechft.yupa.chatFragments.GroupChatViewImageFragment;
import com.vidatechft.yupa.chatFragments.GroupChatViewImageFragment;
import com.vidatechft.yupa.classes.Message;
import com.vidatechft.yupa.classes.User;
import com.vidatechft.yupa.profileFragments.ProfileFragment;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MessageGroupAdapter extends RecyclerView.Adapter<MessageGroupAdapter.ViewHolder> {
    public final String TAG = MessageGroupAdapter.class.getName();
    private List<Message> mDataSet;
    private List<Message> pictureDataset;
    private String mId;
    FirebaseFirestore db = FirebaseFirestore.getInstance();

    private static final int MSG_RIGHT = 1;
    private static final int MSG_LEFT = 2;
    private static final int IMG_MSG_RIGHT = 3;
    private static final int IMG_MSG_LEFT = 4;

    private MainActivity parentActivity;
    private Map<String, User> usersDetails;


    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView msgTV,msgNameTV;
        public ImageView profilePicIV,imageIV;

        public ViewHolder(View v) {
            super(v);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    msgTV = itemView.findViewById(R.id.msgTV);
                    msgNameTV = itemView.findViewById(R.id.msgNameTV);
                    profilePicIV = itemView.findViewById(R.id.profilePicIV);
                    imageIV = itemView.findViewById(R.id.imageIV);
                }
            });
        }
    }

    public MessageGroupAdapter(List<Message> messageDataset , MainActivity parentActivity , List<Message> pictureDataset)// Map<String, User> usersDetails
    {
        mDataSet = messageDataset;
        this.pictureDataset = pictureDataset;
        this.parentActivity = parentActivity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        View v;

        switch (viewType){
            case MSG_RIGHT://todo : Current user message
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_chat_msg_right, parent, false);
                break;
            case MSG_LEFT://todo : Other user message
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_chat_group_msg_left, parent, false);
                break;
            case IMG_MSG_RIGHT://todo : Current user message with pictures
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_chat_img_msg_right, parent, false);
                break;
            case IMG_MSG_LEFT://todo : Other user message with pictures
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_chat_img_msg_left, parent, false);
                break;
            default://todo : Default user message
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_chat_msg_right, parent, false);
                break;
        }

        return new ViewHolder(v);
    }

    @Override
    public int getItemViewType(int position) {
        Message thisMessage = mDataSet.get(position);
        if (thisMessage.isThisUser){
            if(thisMessage.isImg){
                return IMG_MSG_RIGHT;
            }else{
                return MSG_RIGHT;
            }
        }else{
            if(thisMessage.isImg){
                return IMG_MSG_LEFT;
            }else{
                return MSG_LEFT;
            }
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int pos) {
        final int position = pos;
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(mDataSet.get(position).groupChat != null)
                {
                    final Message message = mDataSet.get(position);

                    if(!message.msg.equals("") || message.msg != null)
                    {
                        holder.msgTV.setText(message.msg);
                    }
                    if((message.msg.equals("") || message.msg == null))
                    {
                        holder.msgTV.setVisibility(View.GONE);
                    }


                    //todo : If the user`s messages is not from the current user
                    if(getItemViewType(position) == MSG_LEFT)
                    {
                        if(message.user.name != null)
                        {
                            holder.msgNameTV.setText(message.user.name);
                        }
                        if(message.user.profilePicUrl != null)
                        {
                            GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(message.user.profilePicUrl),holder.profilePicIV);
                        }
                    }
                    //todo : If the user`s messages is image (IMAGES)
                    else if(getItemViewType(position) == IMG_MSG_LEFT || getItemViewType(position) == IMG_MSG_RIGHT)
                    {
                        if(getItemViewType(position) == IMG_MSG_LEFT)
                        {
                            holder.msgNameTV.setText(message.user.name);
                            GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(message.user.profilePicUrl),holder.profilePicIV);
                        }
                        GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(message.img),holder.imageIV);
                        //todo : If the user clicks on the image onto the message layout, go to GroupChatViewImageFragment
                        holder.imageIV.setOnClickListener(new DebouncedOnClickListener(500)
                        {
                            @Override
                            public void onDebouncedClick(View v)
                            {
                                if(message.isImg)
                                {
                                    Fragment groupChatViewImageFragment = GroupChatViewImageFragment.newInstance(parentActivity,message,position,pictureDataset);
                                    GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, groupChatViewImageFragment).addToBackStack(GroupChatViewImageFragment.TAG).commit();
                                }
                            }
                        });
                    }
                    if(holder.profilePicIV != null)
                    {
                        //todo : If the user clicks on the other user profile image , go to ProfileFragment
                        holder.profilePicIV.setOnClickListener(new DebouncedOnClickListener(500) {
                            @Override
                            public void onDebouncedClick(View v) {
                                message.user.id = message.uid;
                                Fragment profileFragment = ProfileFragment.newInstance(parentActivity,message.user,false);
                                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, profileFragment).addToBackStack(ProfileFragment.TAG).commit();
                            }
                        });
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

}
