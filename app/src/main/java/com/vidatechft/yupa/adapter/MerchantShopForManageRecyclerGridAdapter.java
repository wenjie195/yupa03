package com.vidatechft.yupa.adapter;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.Merchant;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.hostFragments.BookingRequestListFragment;
import com.vidatechft.yupa.hostFragments.HostStayMainPageFragment;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;

public class MerchantShopForManageRecyclerGridAdapter extends RecyclerView.Adapter<MerchantShopForManageRecyclerGridAdapter.ViewHolder> {
    private MainActivity parentActivity;
    private ArrayList<Merchant> merchants;
    private final OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(Merchant merchant, int pos);
    }

    public MerchantShopForManageRecyclerGridAdapter(MainActivity parentActivity, ArrayList<Merchant> merchants, OnItemClickListener listener) {
        this.parentActivity = parentActivity;
        this.merchants = merchants;
        this.listener = listener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout wholeLL;
        public ImageView shopIV,notificationIV;
        public TextView shopTV,requestTV;

        public ViewHolder(View v) {
            super(v);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    wholeLL = itemView.findViewById(R.id.wholeLL);
                    shopIV = itemView.findViewById(R.id.shopIV);
                    notificationIV = itemView.findViewById(R.id.notificationIV);
                    shopTV = itemView.findViewById(R.id.shopTV);
                    requestTV = itemView.findViewById(R.id.requestTV);
                }
            });
        }

        public void bind(final Merchant merchant,final int pos, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(merchant, pos);
                }
            });
        }
    }

    @NonNull
    @Override
    public MerchantShopForManageRecyclerGridAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_grid_manage_merchant_shop, parent, false);

        return new MerchantShopForManageRecyclerGridAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final int pos = position;
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Merchant merchant = merchants.get(pos);
                if(merchant != null){
                    if(merchant.shopPicUrl != null){
                        GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(merchant.shopPicUrl),holder.shopIV);
                    }else{
                        holder.shopIV.setScaleType(ImageView.ScaleType.FIT_XY);
                        GeneralFunction.GlideImageSettingOwnDrawable(parentActivity, R.drawable.ic_not_found,holder.shopIV);
                    }

                    if(merchant.workPlace != null){
                        holder.shopTV.setText(merchant.workPlace);
                    }

                    holder.bind(merchant, pos, listener);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return merchants.size();
    }
}
