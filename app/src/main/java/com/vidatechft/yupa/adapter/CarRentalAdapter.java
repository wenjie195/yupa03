package com.vidatechft.yupa.adapter;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.BookingHistory;
import com.vidatechft.yupa.classes.CurrencyRate;
import com.vidatechft.yupa.classes.Transport;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CarRentalAdapter extends RecyclerView.Adapter<CarRentalAdapter.ViewHolder> {
    public final String TAG = CarRentalAdapter.class.getName();
    private MainActivity parentActivity;
    private BookingHistory bookingHistory;
    private ArrayList<Transport> transportsList = new ArrayList<>();
    private String hr = "/hr";
    private String day = "/day";
    private String month = "/month";
    private int lastCheckedPosition = -1;
    private final OnItemClickListener listener;
    private SparseArray<ConstraintLayout> wholeLLMap = new SparseArray<>();

    public interface OnItemClickListener {
        void onItemClick(Transport transport, int pos, ConstraintLayout wholeLL);
    }

    public CarRentalAdapter(MainActivity activity, BookingHistory bookingHistory, ArrayList<Transport> transportsList, OnItemClickListener listener){
        this.parentActivity = activity;
        this.bookingHistory = bookingHistory;
        this.transportsList = transportsList;
        this.listener = listener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView carRentalIV;
        TextView carNameTV,carYearTV,carRentalPerHourTV,carRentalPerDayTV,carRentalPerMonthTV;
        View viewLine;
        ConstraintLayout wholeLL;

        public ViewHolder(View v) {
            super(v);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    carRentalIV = itemView.findViewById(R.id.carRentalIV);
                    carNameTV = itemView.findViewById(R.id.carNameTV);
                    carYearTV = itemView.findViewById(R.id.carYearTV);
                    carRentalPerHourTV = itemView.findViewById(R.id.carRentalPerHourTV);
                    carRentalPerDayTV = itemView.findViewById(R.id.carRentalPerDayTV);
                    carRentalPerMonthTV = itemView.findViewById(R.id.carRentalPerMonthTV);
                    viewLine = itemView.findViewById(R.id.viewLine);
                    wholeLL = itemView.findViewById(R.id.wholeLL);
                }
            });
        }

        public void bind(final Transport transport,final int pos, final OnItemClickListener listener, final ConstraintLayout wholeLL) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(transport, pos, wholeLL);
                }
            });
        }
    }

    @NonNull
    @Override
    public CarRentalAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_car_rental, parent, false);

        return new CarRentalAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final CarRentalAdapter.ViewHolder holder, final int pos) {
        final int position = pos;

        if(position == transportsList.size() - 1){
            holder.viewLine.setVisibility(View.GONE);
        }

        final Transport transport = transportsList.get(position);
        if(transport != null){
            transport.tempId = position;

            if(transport.photoUrl != null){
                GeneralFunction.GlideImageSetting(parentActivity,Uri.parse(transport.photoUrl),holder.carRentalIV);
            }
            if(transport.make != null && transport.model != null){
                String carName = transport.make + " " + transport.model;
                holder.carNameTV.setText(carName);
            }
            if(transport.manufactureDate != null){
                holder.carYearTV.setText(String.valueOf(transport.manufactureDate));
            }
            if(transport.currencyCode != null){
                String localCurrencyCode = GeneralFunction.getCurrencyCode(parentActivity);
                String templatePrice = localCurrencyCode + " ";
                if(transport.perHour != null){
                    String perHourPrice = templatePrice + GeneralFunction.getDecimalFormat(2).format(CurrencyRate.getUserLocalPrice(parentActivity,localCurrencyCode,transport.currencyCode,transport.perHour))+ hr;
                    holder.carRentalPerHourTV.setText(perHourPrice);
                }
                if(transport.perDay != null){
                    String perDayPrice = templatePrice + GeneralFunction.getDecimalFormat(2).format(CurrencyRate.getUserLocalPrice(parentActivity,localCurrencyCode,transport.currencyCode,transport.perDay))+ day;
                    holder.carRentalPerDayTV.setText(perDayPrice);
                }
                if(transport.perMonth != null){
                    String perMonthPrice = templatePrice + GeneralFunction.getDecimalFormat(2).format(CurrencyRate.getUserLocalPrice(parentActivity,localCurrencyCode,transport.currencyCode,transport.perMonth))+ month;
                    holder.carRentalPerMonthTV.setText(perMonthPrice);
                }
            }

            wholeLLMap.put(pos,holder.wholeLL);
            holder.bind(transport, pos, listener, holder.wholeLL);

        }
    }

    public SparseArray<ConstraintLayout> getWholeLLMap(){
        return wholeLLMap;
    }

    @Override
    public int getItemCount() {
        return transportsList.size();
    }

    private void buttonClickableAtOnce(ConstraintLayout btnTrue, ConstraintLayout btnFalse, boolean isChecked){

        if(!isChecked){
            //set selection
            btnFalse.setSelected(true);
            btnTrue.setSelected(false);
            //set false background
            btnTrue.setBackgroundColor(parentActivity.getResources().getColor(R.color.unread_notification));
            btnFalse.setBackgroundColor(parentActivity.getResources().getColor(R.color.white));
        }
        else{
            //set selection
            btnTrue.setSelected(true);
            btnFalse.setSelected(false);
            //set true background
            btnFalse.setBackgroundColor(parentActivity.getResources().getColor(R.color.unread_notification));
            btnTrue.setBackgroundColor(parentActivity.getResources().getColor(R.color.white));
        }
    }

    private void setButton(ViewHolder holder, int position){
        if(transportsList != null){

            for(Transport transport1 : transportsList){
                if(transport1.tempId == position){
                    holder.wholeLL.setBackgroundColor(parentActivity.getResources().getColor(R.color.unread_notification));
                }
                else{
                    holder.wholeLL.setBackgroundColor(parentActivity.getResources().getColor(R.color.white));
                }
            }
            notifyDataSetChanged();
        }
    }
}
