package com.vidatechft.yupa.adapter;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.User;
import com.vidatechft.yupa.profileFragments.ProfileFragment;
import com.vidatechft.yupa.utilities.GeneralFunction;
import com.vidatechft.yupa.utilities.KeyboardUtilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class GetAllAvailableTourBuddyAdapter extends RecyclerView.Adapter<GetAllAvailableTourBuddyAdapter.ViewHolder>
{
    private MainActivity parentActivity;
    private Fragment parentFragment;
    private ArrayList<User> getTourBuddy;
    private User findUserCriteria;

    public GetAllAvailableTourBuddyAdapter(MainActivity parentActivity , Fragment parentFragment , ArrayList<User> getTourBuddy , User findUserCriteria)
    {
        this.parentActivity = parentActivity;
        this.parentFragment = parentFragment;
        this.getTourBuddy = getTourBuddy;
        this.findUserCriteria = findUserCriteria;
    }

    @NonNull
    @Override
    public GetAllAvailableTourBuddyAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent , int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_grid_search_tour_buddy, parent, false);
        return new GetAllAvailableTourBuddyAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final GetAllAvailableTourBuddyAdapter.ViewHolder holder , final int pos)
    {
        final int position = pos;
        parentActivity.runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                final User searchedUser = getTourBuddy.get(position);
                parentActivity.runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        if(searchedUser != null)
                        {
                            if (searchedUser.profilePicUrl == null)
                            {
                                searchedUser.profilePicUrl = "";
                            }
                            if (searchedUser.profilePicUrl != null)
                            {
                                GeneralFunction.GlideImageSetting(parentActivity,Uri.parse(searchedUser.profilePicUrl),holder.tourBuddyUserIV);
                            }
                            if (searchedUser.name != "")
                            {
                                holder.tourBuddyNameTV.setText(searchedUser.name);
                            }
                            if(searchedUser.hobbyScope != null)
                            {
                                int hobbySize = searchedUser.hobbyScope.size();
                                StringBuilder stringBuilder = new StringBuilder();
                                String text = "";
                                if(hobbySize >= 2)
                                {
                                    for(int i = 0;i < 2;i++)
                                    {
                                        if(i == 1)
                                        {
                                            stringBuilder.append(searchedUser.hobbyScope.get(i));
                                        }
                                        else
                                        {
                                            stringBuilder.append(searchedUser.hobbyScope.get(i)+", ");
                                        }

                                    }

                                    text = stringBuilder.toString();

                                }
                                else if(hobbySize == 1)
                                {
                                    stringBuilder.append(searchedUser.hobbyScope.get(0));
                                    text = stringBuilder.toString();
                                }

                                holder.tourBuddyHobbyTV.setText(text);

                                holder.wholeLL.setOnClickListener(new View.OnClickListener()
                                {
                                    @Override
                                    public void onClick(View v)
                                    {
                                        KeyboardUtilities.hideSoftKeyboard(parentActivity);
                                        Fragment profileFragment = ProfileFragment.newInstance(parentActivity,searchedUser,false,findUserCriteria,true);
                                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, profileFragment).addToBackStack(ProfileFragment.TAG).commit();
                                    }
                                });
                            }
                        }
                        else
                        {

                        }
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return getTourBuddy.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public LinearLayout wholeLL;
        public TextView tourBuddyNameTV,tourBuddyHobbyTV;
        public ImageView tourBuddyUserIV;

        public ViewHolder(View v)
        {
            super(v);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    wholeLL = itemView.findViewById(R.id.wholeLL);
                    tourBuddyNameTV = itemView.findViewById(R.id.tourBuddyNameTV);
                    tourBuddyHobbyTV = itemView.findViewById(R.id.tourBuddyHobbyTV);
                    tourBuddyUserIV = itemView.findViewById(R.id.tourBuddyUserIV);
                }
            });
        }
    }
}

