package com.vidatechft.yupa.fragments;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.tsongkha.spinnerdatepicker.DatePickerDialog;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.LoginActivity;
import com.vidatechft.yupa.adapter.YupaCustomArrayAdapterMethodTwo;
import com.vidatechft.yupa.classes.User;
import com.vidatechft.yupa.utilities.Config;
import com.vidatechft.yupa.utilities.GeneralFunction;
import com.vidatechft.yupa.utilities.KeyboardUtilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//countries, state, city, phone codes from here https://github.com/hiiamrohit/Countries-States-Cities-database
//great library for detecting whether phone number is valid or not based on country from here https://github.com/googlei18n/libphonenumber
public class RegisterFragment extends Fragment implements View.OnClickListener, DatePickerDialog.OnDateSetListener{
    public static final String TAG = RegisterFragment.class.getName();
    private AppCompatActivity parentActivity;
    private EditText nameET, emailET, passwordET, contactET;
    private AutoCompleteTextView countryET,countryCodeATV;
    private TextView dobTV;
    private AppCompatCheckBox acknowCB;
    private int dobDay, dobMonth, dobYear;
    private SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");
    private String accType;
    private List<String> tempCountryList = new ArrayList<>();
    private List<String> tempCountryCodeList = new ArrayList<>();
    private List<String> tempCountrySortNameList = new ArrayList<>();
    private List<String> tempCountrySortNameCodeList = new ArrayList<>();
    private HashMap<String,String> tempCountryCodeMap = new HashMap<>();
    private HashMap<String,String> tempCountrySortNameMap = new HashMap<>();
    private HashMap<String,String> tempCountrySortNameAndCodeMap = new HashMap<>();
    private View showMoreCountry,showMoreCountryCode;
    private TextView termTV;
    private boolean isShowingPassword = false;

    View view;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static RegisterFragment newInstance(AppCompatActivity parentActivity, String accType) {
        RegisterFragment fragment = new RegisterFragment();
        fragment.parentActivity = parentActivity;
        fragment.accType = accType;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container, Bundle savedInstanceState) {
        parentActivity.runOnUiThread(new Runnable() {
            public void run() {
                view = inflater.inflate(R.layout.fragment_register, container, false);
                TextView registerTV = view.findViewById(R.id.registerTV);
                ImageButton backIB = view.findViewById(R.id.backIB);
                backIB.setImageDrawable(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.ic_back,R.color.black));
                nameET = view.findViewById(R.id.nameET);
                emailET = view.findViewById(R.id.emailET);
                passwordET = view.findViewById(R.id.passwordET);
                final ImageView showPasswordIV = view.findViewById(R.id.showPasswordIV);
                dobTV = view.findViewById(R.id.dobTV);
                countryET = view.findViewById(R.id.countryET);
                contactET = view.findViewById(R.id.contactET);
                acknowCB = view.findViewById(R.id.acknowCB);
                showMoreCountry = view.findViewById(R.id.showMoreCountry);
                showMoreCountryCode = view.findViewById(R.id.showMoreCountryCode);
                countryCodeATV = view.findViewById(R.id.countryCodeATV);
                termTV = view.findViewById(R.id.termTV);

                dobTV.setOnClickListener(RegisterFragment.this);
                backIB.setOnClickListener(RegisterFragment.this);
                registerTV.setOnClickListener(RegisterFragment.this);

                setupCountry();

                passwordET.setOnEditorActionListener(new EditText.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_NEXT) {
                            Calendar cal = GeneralFunction.getDefaultUtcCalendar();
                            showDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), R.style.DatePickerSpinner);
                            return true;
                        }
                        return false;
                    }
                });

                showPasswordIV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(isShowingPassword){
                            showPasswordIV.setImageDrawable(parentActivity.getResources().getDrawable(R.drawable.ic_show_password));
                            passwordET.setTransformationMethod(new PasswordTransformationMethod());
                        }else{
                            showPasswordIV.setImageDrawable(parentActivity.getResources().getDrawable(R.drawable.ic_hide_password));
                            passwordET.setTransformationMethod(null);
                        }
                        isShowingPassword = !isShowingPassword;
                        passwordET.setSelection(passwordET.length());
                    }
                });

                contactET.setOnEditorActionListener(new EditText.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_DONE) {
                            registerUser();
                            return true;
                        }
                        return false;
                    }
                });
//                String zipCode = GeneralFunction.GetCountryZipCode(parentActivity);
//                Toast.makeText(parentActivity, zipCode, Toast.LENGTH_SHORT).show();

                countryCodeATV.setDropDownWidth(getResources().getDisplayMetrics().widthPixels/2);
                termTV.setMovementMethod(LinkMovementMethod.getInstance());
                termTV.setLinkTextColor(parentActivity.getResources().getColor(R.color.pure_blue));
                getCountriesFromJSONFile();

            }
        });

        return view;
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setupCountry(){
        getCountry();
        getCountryNameAndCode();
        getCountrySortNameAndCode();
        getCountryNameAndSortName();
        if(tempCountryList == null || tempCountryCodeList == null){
            return;
        }
//        countryList = Arrays.asList(getResources().getStringArray(R.array.country_list));
        final YupaCustomArrayAdapterMethodTwo countryAdapter = new YupaCustomArrayAdapterMethodTwo(parentActivity,android.R.layout.simple_dropdown_item_1line,tempCountryList);
//        final ArrayAdapter<String> countryCodeAdapter = new ArrayAdapter<>(parentActivity,android.R.layout.simple_dropdown_item_1line,tempCountrySortNameCodeList);
        final YupaCustomArrayAdapterMethodTwo countryCodeAdapter = new YupaCustomArrayAdapterMethodTwo(parentActivity,android.R.layout.simple_dropdown_item_1line,tempCountrySortNameCodeList);
        //Programmatically set the auto complete textview drop down height
        countryET.setDropDownHeight(getResources().getDisplayMetrics().widthPixels/2);
        countryCodeATV.setDropDownHeight(getResources().getDisplayMetrics().widthPixels/2);
        countryET.setThreshold(1);
        countryET.setAdapter(countryAdapter);

        countryCodeATV.setThreshold(1);
        countryCodeATV.setAdapter(countryCodeAdapter);

        countryET.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                countryET.setText(tempCountryList.get(position));
                KeyboardUtilities.hideSoftKeyboard(parentActivity);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        countryCodeATV.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                countryCodeATV.setText(tempCountryCodeList.get(position));
//                KeyboardUtilities.hideSoftKeyboard(parentActivity);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        showMoreCountryCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        KeyboardUtilities.showSoftKeyboard(parentActivity,countryCodeATV);
                        countryCodeATV.showDropDown();
                    }
                },200);

            }
        });


        showMoreCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        KeyboardUtilities.showSoftKeyboard(parentActivity,countryET);
                        countryET.showDropDown();
                    }
                },200);
            }
        });



        // Add Text Change Listener to EditText
        countryET.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // Call back the Adapter with current character to Filter
                countryAdapter.getFilter().filter(s.toString());
                String countryName = s.toString();
                String tempCountryCode = getCountryCodeWithName(countryName);
                String tempSortName = getCountryCodeWithSortName(countryName);
                if(tempSortName != null && tempCountryCode != null){
                    String sortNameCode = tempSortName + " +" + tempCountryCode;
                    countryCodeATV.setText(sortNameCode);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().isEmpty()){
                    final YupaCustomArrayAdapterMethodTwo countryAdapter = new YupaCustomArrayAdapterMethodTwo(parentActivity,android.R.layout.simple_dropdown_item_1line,tempCountryList);
                    countryET.setThreshold(1);
                    countryET.setAdapter(countryAdapter);
                    countryET.showDropDown();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,int after) {
            }

        });

        countryET.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                KeyboardUtilities.hideSoftKeyboard(parentActivity);
                contactET.requestFocus();
                KeyboardUtilities.showSoftKeyboard(parentActivity,contactET);
            }
        });

        countryCodeATV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                KeyboardUtilities.hideSoftKeyboard(parentActivity);
                contactET.requestFocus();
                KeyboardUtilities.showSoftKeyboard(parentActivity,contactET);
            }
        });


        // Add Text Change Listener to EditText
        countryCodeATV.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // Call back the Adapter with current character to Filter
                countryCodeAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().isEmpty()){
                    final YupaCustomArrayAdapterMethodTwo countryAdapter = new YupaCustomArrayAdapterMethodTwo(parentActivity,android.R.layout.simple_dropdown_item_1line,tempCountrySortNameCodeList);
                    countryCodeATV.setThreshold(1);
                    countryCodeATV.setAdapter(countryAdapter);
                    countryCodeATV.showDropDown();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,int after) {
            }

        });

        countryCodeATV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                KeyboardUtilities.hideSoftKeyboard(parentActivity);
                String getCountryCode = countryCodeATV.getText().toString();
                countryCodeATV.setText(getCountryCode);
                contactET.requestFocus();
                KeyboardUtilities.showSoftKeyboard(parentActivity,contactET);

            }
        });
    }

    private boolean isValidInput(String name, String email, String password, String contactNo){
        return  GeneralFunction.checkAndReturnString(name,parentActivity.getString(R.string.error_please_enter_name),parentActivity)
                &&
                GeneralFunction.checkAndReturnString(email,parentActivity.getString(R.string.error_please_enter_email),parentActivity)
                &&
                GeneralFunction.isValidEmail(email,parentActivity)
                &&
                GeneralFunction.checkAndReturnPassword(password, parentActivity.getString(R.string.error_please_enter_password_with_6),parentActivity)
                &&
                GeneralFunction.checkAndReturnNum(dobYear,parentActivity.getString(R.string.error_please_enter_dob),parentActivity)
                &&
                GeneralFunction.checkAndReturnString(countryET.getText().toString(),parentActivity.getString(R.string.error_please_select_country),parentActivity)
                &&
                isHasThisCountry(countryET.getText().toString())
                &&
                isHasThisCountryCode(countryCodeATV.getText().toString())
                &&
                isPhoneNumberValid(contactET.getText().toString().trim(),getSortNameByCode())
                &&
                GeneralFunction.checkAndReturnString(contactNo,parentActivity.getString(R.string.error_please_enter_contact),parentActivity);
    }

    private boolean isHasThisCountry(String country){
        if(tempCountryList != null){
            if(!tempCountryList.contains(country.trim())){
                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_select_valid_country),R.drawable.notice_bad,TAG);
                return false;
            }else{
                return true;
            }
        }else{
            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_occured),R.drawable.notice_bad,TAG);
            return false;
        }
    }

    private boolean isHasThisCountryCode(String countryCode){
        if(tempCountryCodeList != null){
            if(!countryCode.contains(countryCode.trim())){
                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_select_valid_country_code),R.drawable.notice_bad,TAG);
                return false;
            }else{
                return true;
            }
        }else{
            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_occured),R.drawable.notice_bad,TAG);
            return false;
        }
    }

    private void registerUser(){
        String name = nameET.getText().toString().trim();
        String email = emailET.getText().toString().trim();
        String password = passwordET.getText().toString();
        String country = countryET.getText().toString().trim();
        String countryCode = countryCodeATV.getText().toString().trim();
        String contactNo = contactET.getText().toString().trim();
        String state = "";
        String gender = "";

        if(isValidInput(name,email,password,contactNo)){
            ((LoginActivity)parentActivity).initializeProgressDialog(parentActivity.getString(R.string.creating_account),false);
            ((LoginActivity)parentActivity).controlProgressDialog(true, parentActivity);
//            ((LoginActivity)parentActivity).setRegisteredUser(new User(name,email,gender,dobDay,dobMonth,dobYear,country,state,contactNo,acknowCB.isChecked(),accType) );
            ((LoginActivity)parentActivity).setRegisteredUser(new User(name,email,gender,dobDay,dobMonth,dobYear,country,countryCode,state,contactNo,acknowCB.isChecked(),accType) );

            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(parentActivity, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(!task.isSuccessful()){
                                ((LoginActivity)parentActivity).controlProgressDialog(false,parentActivity);
                                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_create_account),R.drawable.notice_bad,"register_user_fail");
                                if(task.getException() != null){
                                    task.getException().printStackTrace();
                                }
                            }else{
                                if(parentActivity.getSupportFragmentManager() != null){
                                    ((LoginActivity)parentActivity).restartCarousel();
                                    parentActivity.getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                }
                            }
                        }
                    });
            //the place where it updates the database is inside LoginActivity onAuthStateChanged function
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.dobTV:
                Calendar cal = GeneralFunction.getDefaultUtcCalendar();
                showDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), R.style.DatePickerSpinner);
                break;
            case R.id.backIB:
                parentActivity.onBackPressed();
                break;
            case R.id.registerTV:
                registerUser();
                break;
        }
    }

    public boolean isPhoneNumberValid(String phoneNumber, String countryCode)
    {
        //NOTE: This should probably be a member variable.
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();

        try {
            if(countryCode == null){
                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getResources().getString(R.string.error_country_code_not_found),R.drawable.notice_bad,TAG);
                return false;
            }
            else{
                Phonenumber.PhoneNumber numberProto = phoneUtil.parse(phoneNumber, countryCode);
                if(!phoneUtil.isValidNumber(numberProto)){
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getResources().getString(R.string.error_country_code_not_match_with_country),R.drawable.notice_bad,TAG);
                }
                return phoneUtil.isValidNumber(numberProto);
            }
        }
        catch (NumberParseException e) {
            System.err.println("NumberParseException was thrown: " + e.toString());
            Log.e(TAG,e.toString());
            GeneralFunction.openMessageNotificationDialog(parentActivity,e.toString(),R.drawable.notice_bad,TAG);
        }

        return false;
    }

    void showDate(int year, int monthOfYear, int dayOfMonth, int spinnerTheme) {
        if(dobDay > 0 && dobMonth > 0 && dobYear > 0){
            year = dobYear;
            monthOfYear = dobMonth - 1;
            dayOfMonth = dobDay;
        }
        new SpinnerDatePickerDialogBuilder()
                .context(parentActivity)
                .callback(RegisterFragment.this)
                .spinnerTheme(spinnerTheme)
                .defaultDate(year, monthOfYear, dayOfMonth)
                .build()
                .show();
    }

    @Override
    public void onDateSet(com.tsongkha.spinnerdatepicker.DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        final Calendar calendar = new GregorianCalendar(year, monthOfYear, dayOfMonth);
        dobDay = dayOfMonth;
        dobMonth = monthOfYear + 1;
        dobYear = year;
        dobTV.setText(format.format(calendar.getTime()));

        parentActivity.runOnUiThread(new Runnable() {
            public void run() {
                dobTV.setText(format.format(calendar.getTime()));
                countryET.requestFocus();
                KeyboardUtilities.showSoftKeyboard(parentActivity,countryET);
            }
        });
    }


    private ArrayList<HashMap<String, String>>  getCountriesFromJSONFile () {
        ArrayList<HashMap<String, String>> countryList = new ArrayList<HashMap<String, String>>();
        try {
            JSONObject obj = new JSONObject(GeneralFunction.loadJSONFromAsset(Config.COUNTRY_JSON_FILE, parentActivity));
            JSONArray countryArray = obj.getJSONArray(Config.COUNTRY_ARRAY);
            HashMap<String, String> countryMap;

            for (int i = 0; i < countryArray.length(); i++) {
                JSONObject jo_inside = countryArray.getJSONObject(i);
                Log.d("Details-->", jo_inside.getString(Config.COUNTRY_SORT_NAME_JSON));

                String countrySortName = jo_inside.getString(Config.COUNTRY_SORT_NAME_JSON);
                String countryName = jo_inside.getString(Config.COUNTRY_NAME_JSON);
                String countryCode = jo_inside.getString(Config.COUNTRY_CODE_JSON);
                String countryId = jo_inside.getString(Config.COUNTRY_ID_JSON);

                //Add your values in your `ArrayList` as below:
                countryMap = new HashMap<String, String>();
                countryMap.put(Config.COUNTRY_SORT_NAME, countrySortName);
                countryMap.put(Config.COUNTRY_NAME, countryName);
                countryMap.put(Config.COUNTRY_CODE, countryCode);
                countryMap.put(Config.COUNTRY_ID, countryId);

                countryList.add(countryMap);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return countryList;
    }

    private void getCountry(){

        ArrayList<HashMap<String,String>> allCountriesMap = getCountriesFromJSONFile();

        for (HashMap<String, String> map : allCountriesMap){
            for (Map.Entry<String, String> mapEntry : map.entrySet())
            {
                String key = mapEntry.getKey();
                String value = mapEntry.getValue();
                if(key.equals(Config.COUNTRY_NAME)){
                    tempCountryList.add(value);
                }
                if(key.equals(Config.COUNTRY_SORT_NAME)){
                    tempCountrySortNameList.add(value);
                }
            }
        }
    }


    private String getCountryCodeWithName(String countryName){

        String countryCode = null;

        if(tempCountryCodeMap != null){
            for (Map.Entry<String, String> mapEntry : tempCountryCodeMap.entrySet()){
                String key = mapEntry.getKey();
                String value = mapEntry.getValue();
                if(key.equals(countryName)){
                    countryCode = value;
                }
            }
            return countryCode;
        }
        else{
            return null;
        }
    }

    private String getCountryCodeWithSortName(String countryName){

        String countryCode = null;

        if(tempCountrySortNameMap != null){
            for (Map.Entry<String, String> mapEntry : tempCountrySortNameMap.entrySet()){
                String key = mapEntry.getKey();
                String value = mapEntry.getValue();
                if(key.equals(countryName)){
                    countryCode = value;
                }
            }
            return countryCode;
        }
        else{
            return null;
        }
    }

    private void getCountryNameAndCode(){

        ArrayList<HashMap<String,String>> allCountriesMap = getCountriesFromJSONFile();
        String name = null;
        String code = null;
        for (HashMap<String, String> map : allCountriesMap){
            for (Map.Entry<String, String> mapEntry : map.entrySet())
            {
                String key = mapEntry.getKey();
                String value = mapEntry.getValue();
                if(key.equals(Config.COUNTRY_NAME)){
                    name = value;
                }
                if(key.equals(Config.COUNTRY_CODE)){
                    code = value;
                    tempCountryCodeList.add(value);
                }
                if(name != null && code != null){
                    tempCountryCodeMap.put(name,code);
                    name = null;
                    code = null;
                }
            }
        }
    }

    private void getCountrySortNameAndCode(){
        ArrayList<HashMap<String,String>> allCountriesMap = getCountriesFromJSONFile();
        String code = null;
        String sortName = null;
        for (HashMap<String, String> map : allCountriesMap){
            for (Map.Entry<String, String> mapEntry : map.entrySet())
            {
                String key = mapEntry.getKey();
                String value = mapEntry.getValue();
                if(key.equals(Config.COUNTRY_CODE)){
                    code = value;
                    tempCountryCodeList.add(value);
                }
                if(key.equals(Config.COUNTRY_SORT_NAME)){
                    sortName = value;
                }
                if(sortName != null && code != null){
                    String sortNameCode = sortName + " +" + code;
                    tempCountrySortNameCodeList.add(sortNameCode);
                    tempCountrySortNameAndCodeMap.put(sortNameCode,sortName);
                    code = null;
                    sortName = null;
                }
            }
        }
    }

    private void getCountryNameAndSortName(){
        ArrayList<HashMap<String,String>> allCountriesMap = getCountriesFromJSONFile();
        String name = null;
        String sortName = null;
        for (HashMap<String, String> map : allCountriesMap){
            for (Map.Entry<String, String> mapEntry : map.entrySet())
            {
                String key = mapEntry.getKey();
                String value = mapEntry.getValue();
                if(key.equals(Config.COUNTRY_NAME)){
                    name = value;
                }
                if(key.equals(Config.COUNTRY_SORT_NAME)){
                    sortName = value;
                }
                if(name != null && sortName != null){
                    tempCountrySortNameMap.put(name,sortName);
                    name = null;
                    sortName = null;
                }
            }
        }
    }

    private String getSortNameByCode(){
        String sortName = null;
        for (Map.Entry<String, String> mapEntry : tempCountrySortNameAndCodeMap.entrySet())
        {
            String key = mapEntry.getKey();
            String value = mapEntry.getValue();
            if(key.equals(countryCodeATV.getText().toString())){
                sortName = value;
            }
        }
        return sortName;
    }
}
