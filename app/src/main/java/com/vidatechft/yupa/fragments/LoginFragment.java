package com.vidatechft.yupa.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.LoginActivity;
import com.vidatechft.yupa.utilities.GeneralFunction;
import com.vidatechft.yupa.utilities.KeyboardUtilities;

import java.util.HashMap;
import java.util.Map;

public class LoginFragment extends Fragment implements View.OnClickListener{
    public static final String TAG = LoginFragment.class.getName();
    private AppCompatActivity parentActivity;
    private EditText emailET, passwordET;

    private boolean isShowingPassword = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static LoginFragment newInstance(AppCompatActivity parentActivity) {
        LoginFragment fragment = new LoginFragment();
        fragment.parentActivity = parentActivity;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_login, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ConstraintLayout wholeCL = view.findViewById(R.id.wholeCL);
                TextView loginTV = view.findViewById(R.id.loginTV);
                TextView forgotPwTV = view.findViewById(R.id.forgotPwTV);
                ImageButton backIB = view.findViewById(R.id.backIB);
                backIB.setImageDrawable(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.ic_back,R.color.white));
                emailET = view.findViewById(R.id.emailET);
                passwordET = view.findViewById(R.id.passwordET);
                final ImageView showPasswordIV = view.findViewById(R.id.showPasswordIV);

                forgotPwTV.setOnClickListener(LoginFragment.this);
                backIB.setOnClickListener(LoginFragment.this);
                loginTV.setOnClickListener(LoginFragment.this);
                GeneralFunction.insertBgImgToOtherLayout(parentActivity,R.drawable.bg_signin,wholeCL);

                passwordET.setOnEditorActionListener(new EditText.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_DONE) {
                            loginUser();
                            return true;
                        }
                        return false;
                    }
                });

                showPasswordIV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(isShowingPassword){
                            showPasswordIV.setImageDrawable(parentActivity.getResources().getDrawable(R.drawable.ic_show_password));
                            passwordET.setTransformationMethod(new PasswordTransformationMethod());
                        }else{
                            showPasswordIV.setImageDrawable(parentActivity.getResources().getDrawable(R.drawable.ic_hide_password));
                            passwordET.setTransformationMethod(null);
                        }
                        isShowingPassword = !isShowingPassword;
                        passwordET.setSelection(passwordET.length());
                    }
                });
            }
        });

        return view;
    }

    private boolean isValidInput(String email, String password){
        return  GeneralFunction.checkAndReturnString(email,parentActivity.getString(R.string.error_please_enter_email),parentActivity)
                &&
                GeneralFunction.checkAndReturnString(password,parentActivity.getString(R.string.error_please_enter_password),parentActivity);
    }

    private void loginUser(){
        String email = emailET.getText().toString().trim();
        String password = passwordET.getText().toString();
        if(isValidInput(email, password)){
            ((LoginActivity)parentActivity).initializeProgressDialog(parentActivity.getString(R.string.logging_in),false);
            ((LoginActivity)parentActivity).controlProgressDialog(true, parentActivity);

            FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(parentActivity, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            ((LoginActivity)parentActivity).controlProgressDialog(false,parentActivity);
                            if (!task.isSuccessful()) {
                                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_incorrect_email_or_pw),R.drawable.notice_bad,TAG);
                                if(task.getException() != null){
                                    task.getException().printStackTrace();
                                }
                            }
                        }
                    });
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        KeyboardUtilities.hideSoftKeyboard(parentActivity);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.backIB:
                parentActivity.onBackPressed();
                break;
            case R.id.loginTV:
                loginUser();
                break;
            case R.id.forgotPwTV:
                Fragment forgotPasswordFragment = ForgotPasswordFragment.newInstance(parentActivity);
                parentActivity.getSupportFragmentManager().beginTransaction().replace(R.id.loginFL, forgotPasswordFragment).addToBackStack(ForgotPasswordFragment.TAG).commit();
                break;
        }
    }
}
