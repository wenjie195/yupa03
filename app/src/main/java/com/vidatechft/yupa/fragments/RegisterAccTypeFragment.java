package com.vidatechft.yupa.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.LoginActivity;
import com.vidatechft.yupa.utilities.Config;
import com.vidatechft.yupa.utilities.GeneralFunction;

public class RegisterAccTypeFragment extends Fragment implements View.OnClickListener{
    public static final String TAG = RegisterAccTypeFragment.class.getName();
    private AppCompatActivity parentActivity;
    private View guestView, onetimeView, freqView;
    private TextView guestLblTV, onetimeLblTV, freqLblTV;

    View view;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static RegisterAccTypeFragment newInstance(AppCompatActivity parentActivity) {
        RegisterAccTypeFragment fragment = new RegisterAccTypeFragment();
        fragment.parentActivity = parentActivity;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container, Bundle savedInstanceState) {
        parentActivity.runOnUiThread(new Runnable() {
            public void run() {
                view = inflater.inflate(R.layout.fragment_register_type, container, false);
                guestView = view.findViewById(R.id.guestView);
                onetimeView = view.findViewById(R.id.onetimeView);
                freqView = view.findViewById(R.id.freqView);
                guestLblTV = view.findViewById(R.id.guestLblTV);
                onetimeLblTV = view.findViewById(R.id.onetimeLblTV);
                freqLblTV = view.findViewById(R.id.freqLblTV);
                ImageButton backIB = view.findViewById(R.id.backIB);
                backIB.setImageDrawable(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.ic_back,R.color.white));
                ConstraintLayout wholeCL = view.findViewById(R.id.wholeCL);

                backIB.setOnClickListener(RegisterAccTypeFragment.this);
                guestView.setOnClickListener(RegisterAccTypeFragment.this);
                onetimeView.setOnClickListener(RegisterAccTypeFragment.this);
                freqView.setOnClickListener(RegisterAccTypeFragment.this);

                GeneralFunction.insertBgImgToOtherLayout(parentActivity,R.drawable.bg_acctype,wholeCL);
            }
        });

        return view;
    }

    private void goToRegisterFragment(String type){
        Fragment registerFragment = RegisterFragment.newInstance(parentActivity,type);
        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.loginFL, registerFragment).addToBackStack(RegisterFragment.TAG).commit();
    }

    private void signInAnonymously(){
        ((LoginActivity)parentActivity).initializeProgressDialog(parentActivity.getString(R.string.creating_guest_account),false);
        ((LoginActivity)parentActivity).controlProgressDialog(true, parentActivity);
        FirebaseAuth.getInstance().signInAnonymously()
                .addOnCompleteListener(parentActivity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        ((LoginActivity)parentActivity).controlProgressDialog(false,parentActivity);
                        if (!task.isSuccessful()) {
                            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_create_account_anonymously),R.drawable.notice_bad,TAG);
                        }
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.backIB:
                parentActivity.onBackPressed();
                break;
            case R.id.guestView:
                guestView.setSelected(true);
                onetimeView.setSelected(false);
                freqView.setSelected(false);

                guestLblTV.setSelected(true);
                onetimeLblTV.setSelected(false);
                freqLblTV.setSelected(false);

                signInAnonymously();
                break;
            case R.id.onetimeView:
                guestView.setSelected(false);
                onetimeView.setSelected(true);
                freqView.setSelected(false);

                guestLblTV.setSelected(false);
                onetimeLblTV.setSelected(true);
                freqLblTV.setSelected(false);
                break;
            case R.id.freqView:
                guestView.setSelected(false);
                onetimeView.setSelected(false);
                freqView.setSelected(true);

                guestLblTV.setSelected(false);
                onetimeLblTV.setSelected(false);
                freqLblTV.setSelected(true);

                goToRegisterFragment(Config.ACC_TYPE_FREQ);
                break;
//            case R.id.nextTV:
//                //no need this next liao
//                if(guestView.isSelected()){
//                    signInAnonymously();
//                }else if(onetimeView.isSelected()){
//                    goToRegisterFragment(Config.ACC_TYPE_ONETIME);
//                }else if(freqView.isSelected()){
//                    goToRegisterFragment(Config.ACC_TYPE_FREQ);
//                }else{
//                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.email_choose_acc_type),R.drawable.notice_bad,TAG);
//                }
//                break;
        }
    }
}
