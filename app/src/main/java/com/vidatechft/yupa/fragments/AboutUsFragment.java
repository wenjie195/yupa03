package com.vidatechft.yupa.fragments;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.utilities.Config;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

public class AboutUsFragment extends Fragment{
    public static final String TAG = AboutUsFragment.class.getName();
    private MainActivity parentActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static AboutUsFragment newInstance(MainActivity parentActivity) {
        AboutUsFragment fragment = new AboutUsFragment();

        fragment.parentActivity = parentActivity;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_about_us, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.showBtmNav();

                TextView appVersionTV = view.findViewById(R.id.appVersionTV);
                TextView addressTV = view.findViewById(R.id.addressTV);

                try {
                    PackageInfo pInfo = parentActivity.getPackageManager().getPackageInfo(parentActivity.getPackageName(), 0);
                    String version = pInfo.versionName;

                    appVersionTV.setText(String.format(getString(R.string.version),version));
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }

                addressTV.setPaintFlags(addressTV.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);
                addressTV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        GeneralFunction.openMapApplicationViaGps(parentActivity,5.4312379,100.31070690000001, "YuPa Discovery Sdn Bhd");
                    }
                });
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.nav_about_us));
                parentActivity.selectHighlights();
            }
        });
    }
}
