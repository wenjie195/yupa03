package com.vidatechft.yupa.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.LoginActivity;
import com.vidatechft.yupa.utilities.GeneralFunction;

public class ForgotPasswordFragment extends Fragment implements View.OnClickListener{
    private EditText emailET;

    public static final String TAG = ForgotPasswordFragment.class.getName();
    private AppCompatActivity parentActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static ForgotPasswordFragment newInstance(AppCompatActivity parentActivity) {
        ForgotPasswordFragment fragment = new ForgotPasswordFragment();

        fragment.parentActivity = parentActivity;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_forgot_password, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                emailET = view.findViewById(R.id.emailET);
                Button resetBtn = view.findViewById(R.id.resetBtn);
                ImageButton backIB = view.findViewById(R.id.backIB);
                backIB.setImageDrawable(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.ic_back,R.color.black));

                backIB.setOnClickListener(ForgotPasswordFragment.this);
                resetBtn.setOnClickListener(ForgotPasswordFragment.this);
            }
        });
        return view;
    }

    private boolean isValidInput(String email){
        return  GeneralFunction.checkAndReturnString(email,parentActivity.getString(R.string.error_please_enter_email),parentActivity)
                &&
                GeneralFunction.isValidEmail(email,parentActivity);
    }

    private void resetPassword(){
        String email = emailET.getText().toString().trim();

        if(isValidInput(email)){

            ((LoginActivity)parentActivity).initializeProgressDialog(parentActivity.getString(R.string.sending_reset_email),true);
            ((LoginActivity)parentActivity).controlProgressDialog(true, parentActivity);

            FirebaseAuth.getInstance().sendPasswordResetEmail(email)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            ((LoginActivity)parentActivity).controlProgressDialog(false, parentActivity);
                            if (task.isSuccessful()) {
                                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.check_email_to_reset_pw), R.drawable.notice_good, TAG);
                            }else{
                                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_sending_reset_email), R.drawable.notice_bad, TAG);
                                if(task.getException() != null){
                                    task.getException().printStackTrace();
                                }
                            }
                        }
                    });
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.backIB:
                parentActivity.onBackPressed();
                break;
            case R.id.resetBtn:
                resetPassword();
                break;
        }
    }
}
