package com.vidatechft.yupa.CalendaUtilities;

import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.vidatechft.yupa.R;

import java.util.HashMap;
import java.util.List;

public class CustomMCVSelectorDecorator implements DayViewDecorator {
    public static final int SELECTION_TYPE_ROUND = 0;
    public static final int SELECTION_TYPE_LEFT = 1;
    public static final int SELECTION_TYPE_MIDDLE = 2;
    public static final int SELECTION_TYPE_RIGHT = 3;

    private HashMap<CalendarDay,Integer> calendarDayMap;
    private AppCompatActivity parentActivity;
    private DayViewFacade dayViewFacade;
    private CalendarDay minDay, maxDay;


    public CustomMCVSelectorDecorator(AppCompatActivity parentActivity, HashMap<CalendarDay,Integer> calendarDayMap ) {
        this.parentActivity = parentActivity;
        this.calendarDayMap = calendarDayMap;
    }

    @Override
    public boolean shouldDecorate(CalendarDay day) {
        if(calendarDayMap.get(day) != null){

            switch (calendarDayMap.get(day)){
                case 0:
                    dayViewFacade.setSelectionDrawable(parentActivity.getResources().getDrawable(R.drawable.rounded_circle_green));
                    break;
                case 1:
                    dayViewFacade.setSelectionDrawable(parentActivity.getResources().getDrawable(R.drawable.left_half_circle));
                    break;
                case 2:
                    dayViewFacade.setSelectionDrawable(parentActivity.getResources().getDrawable(R.drawable.square_calendar));
                    break;
                case 3:
                    dayViewFacade.setSelectionDrawable(parentActivity.getResources().getDrawable(R.drawable.right_half_circle));
                    break;
                default:
                    dayViewFacade.setSelectionDrawable(parentActivity.getResources().getDrawable(R.drawable.rounded_circle_green));
                    break;
            }

//            return day.isInRange(minDay,maxDay);
            return true;
        }else{
            return false;
        }
//        return calendarDayMap.get(day) != null;
    }

    @Override
    public void decorate(DayViewFacade view) {
        view.setSelectionDrawable(parentActivity.getResources().getDrawable(R.drawable.left_half_circle));
        dayViewFacade = view;
    }
}
