package com.vidatechft.yupa.CalendaUtilities;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.widget.CalendarView;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Highlight Saturdays and Sundays with a background
 */
public class HighlightWeekendsDecorator implements DayViewDecorator {

    private final Calendar calendar = Calendar.getInstance();
    private final Drawable highlightDrawable, leftCircle, rightCircle;
    private Context context;
    private CalendarDay minDay, maxDay;
    private Calendar minDate, maxDate;
    private MaterialCalendarView materialCalendarView;
    private int count = 0;
    private boolean first, last;
    private MainActivity parentActivity;
//    private static final int color = getResources().getColor(R.color.colorPrimary);

    public HighlightWeekendsDecorator(MainActivity parentActivity, int color, CalendarDay maxDay, CalendarDay minDay, Calendar maxDate, Calendar minDate, Drawable leftCircle, Drawable rightCircle) {
        highlightDrawable = new ColorDrawable(color);
        this.parentActivity = parentActivity;
        this.maxDay = maxDay;
        this.minDay = minDay;
        this.leftCircle = leftCircle;
        this.rightCircle = rightCircle;
        this.maxDate = maxDate;
        this.minDate = minDate;

    }

    @Override
    public boolean shouldDecorate(CalendarDay day) {
//        day.copyTo(calendar);
        CalendarDay minday = CalendarDay.today();
        final Calendar instance2 = Calendar.getInstance();
        instance2.add(Calendar.DAY_OF_MONTH, 10);
//        int weekDay = calendar.get(Calendar.DAY_OF_WEEK);
//        return weekDay == Calendar.SATURDAY || weekDay == Calendar.SUNDAY;
        return day.isInRange(minDay,maxDay);
    }

    @Override
    public void decorate(DayViewFacade view) {
//        view.setBackgroundDrawable(highlightDrawable);
//        view.setSelectionDrawable(highlightDrawable);

        if(minDate.getTimeInMillis() < maxDate.getTimeInMillis()){
            count++;
            if(count == 1){
                first = true;
            }
            else{
                last = true;
            }
            if(first){
                view.setSelectionDrawable(leftCircle);
            }
            if(last){
                view.setSelectionDrawable(rightCircle);

            }

        }
        else{
            view.setSelectionDrawable(highlightDrawable);
        }
    }
}