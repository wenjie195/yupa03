package com.vidatechft.yupa.CalendaUtilities;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.classes.CustomCalendarDay;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MySelectorDecorator implements DayViewDecorator {

    private Drawable leftCircle, rightCircle;
    private List<CalendarDay> dates;
    private HashMap<CalendarDay,Drawable> calendarDayMap;
    private AppCompatActivity parentActivity;
    private DayViewFacade dayViewFacade;
    private CalendarDay minDay, maxDay;


    public MySelectorDecorator(AppCompatActivity parentActivity,HashMap<CalendarDay,Drawable> calendarDayMap ) {
//        drawable = context.getResources().getDrawable(R.drawable.circle);
        this.parentActivity = parentActivity;
        this.calendarDayMap = calendarDayMap;
    }

    public MySelectorDecorator(AppCompatActivity parentActivity,HashMap<CalendarDay,Drawable> calendarDayMap,CalendarDay minDay, CalendarDay maxDay ) {
//        drawable = context.getResources().getDrawable(R.drawable.circle);
        this.parentActivity = parentActivity;
        this.calendarDayMap = calendarDayMap;
        this.minDay = minDay;
        this.maxDay = maxDay;
    }


    @Override
    public boolean shouldDecorate(CalendarDay day) {
        if(calendarDayMap.get(day) != null){

            dayViewFacade.setSelectionDrawable(calendarDayMap.get(day));
//            dayViewFacade.setBackgroundDrawable(calendarDayMap.get(day));
            return day.isInRange(minDay,maxDay);
        }else{
            return false;
        }
//        return calendarDayMap.get(day) != null;
    }

    @Override
    public void decorate(DayViewFacade view) {
//        if(myDay != null){
//            view.setSelectionDrawable(calendarDayMap.get(myDay));
//        }
        view.setSelectionDrawable(parentActivity.getResources().getDrawable(R.drawable.left_half_circle));
//        view.setBackgroundDrawable(parentActivity.getResources().getDrawable(R.drawable.left_half_circle));
        dayViewFacade = view;
    }
}
