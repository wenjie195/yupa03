package com.vidatechft.yupa.employerFragments;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.CheckboxRecyclerGridAdapter;
import com.vidatechft.yupa.classes.Job;
import com.vidatechft.yupa.classes.PlaceDetailsGoogleApi.PlaceAddress;
import com.vidatechft.yupa.retrofitNetwork.UnifyPlaceAddress;
import com.vidatechft.yupa.utilities.Config;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.FilePath;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.HashMap;

import static android.app.Activity.RESULT_OK;
import static com.vidatechft.yupa.utilities.GeneralFunction.GlideImageSetting;
import static com.vidatechft.yupa.utilities.GeneralFunction.handleUploadError;

public class EmployerPostJobFragment extends Fragment implements View.OnClickListener{
    public static final String TAG = EmployerPostJobFragment.class.getName();

    private MainActivity parentActivity;
    private EmployerPostedJobFragment parentFragment;
    private boolean needUpdate = false;

    private AppCompatEditText otherET;
    private AppCompatTextView locationTV,workspaceTV;
    private String lastToolbarTitle, filePath;
    private Uri fileResultUri;
    private ImageButton uploadPhotoIB;
    private Place place;
    private CheckBox remoteCB;
    private DocumentReference jobRef;

    private CheckboxRecyclerGridAdapter workscopeCBAdapter,earnCBAdapter;

    //for edit
    private Job oriJob, editedJob;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static EmployerPostJobFragment newInstance(MainActivity parentActivity, String lastToolbarTitle) {
        EmployerPostJobFragment fragment = new EmployerPostJobFragment();

        fragment.parentActivity = parentActivity;
        fragment.lastToolbarTitle = lastToolbarTitle;

        return fragment;
    }

    public static EmployerPostJobFragment newInstance(MainActivity parentActivity, String lastToolbarTitle, EmployerPostedJobFragment parentFragment, boolean needUpdate) {
        EmployerPostJobFragment fragment = new EmployerPostJobFragment();

        fragment.parentActivity = parentActivity;
        fragment.lastToolbarTitle = lastToolbarTitle;
        fragment.parentFragment = parentFragment;
        fragment.needUpdate = needUpdate;

        return fragment;
    }

    public static EmployerPostJobFragment newInstance(MainActivity parentActivity, String lastToolbarTitle, Job oriJob, EmployerPostedJobFragment parentFragment, boolean needUpdate) {
        EmployerPostJobFragment fragment = new EmployerPostJobFragment();

        fragment.parentActivity = parentActivity;
        fragment.lastToolbarTitle = lastToolbarTitle;
        fragment.oriJob = oriJob;
        fragment.parentFragment = parentFragment;
        fragment.needUpdate = needUpdate;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_employer_post_job, container, false);
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.hideBtmNav();
                uploadPhotoIB = view.findViewById(R.id.uploadPhotoIB);
                AppCompatTextView submitTV = view.findViewById(R.id.submitTV);
                RecyclerView workscopeRV = view.findViewById(R.id.workscopeRV);
                RecyclerView earnRV = view.findViewById(R.id.earnRV);
                workspaceTV = view.findViewById(R.id.workspaceTV);
                otherET = view.findViewById(R.id.otherET);
                locationTV = view.findViewById(R.id.locationTV);
                remoteCB = view.findViewById(R.id.remoteCB);

                setupJobList(workscopeRV);
                setupEarnList(earnRV);

                workspaceTV.setOnClickListener(new DebouncedOnClickListener(500) {
                    @Override
                    public void onDebouncedClick(View v) {
                        GeneralFunction.openPlacePicker(parentActivity,EmployerPostJobFragment.this);
                    }
                });

                locationTV.setOnClickListener(new DebouncedOnClickListener(500) {
                    @Override
                    public void onDebouncedClick(View v) {
                        GeneralFunction.openPlacePicker(parentActivity,EmployerPostJobFragment.this);
                    }
                });

                uploadPhotoIB.setOnClickListener(EmployerPostJobFragment.this);
                submitTV.setOnClickListener(EmployerPostJobFragment.this);

                populateOriginalJob();
            }
        });
        return view;
    }

    private void populateOriginalJob(){
        Job populateJob = null;
        if(oriJob != null){
            populateJob = oriJob;
        }

        if(editedJob != null){
            populateJob = editedJob;
        }

        if(populateJob == null){
            return;
        }

        if(populateJob.jobScope != null && !populateJob.jobScope.isEmpty()){
            for(String workscope : populateJob.jobScope){
                for(int i = 0; i < workscopeCBAdapter.getCheckBoxes().size(); i++){
                    CheckBox workscopeCB = workscopeCBAdapter.getCheckBoxes().get(i);
                    if(TextUtils.equals(workscopeCB.getText().toString().trim(),workscope.trim())){
                        workscopeCBAdapter.getCheckBoxes().get(i).setChecked(true);
                        workscopeCBAdapter.notifyItemChanged(i);
                    }
                }
            }
        }

        if(populateJob.earnList != null && !populateJob.earnList.isEmpty()){
            for(String earn : populateJob.earnList){
                for(int i = 0; i < earnCBAdapter.getCheckBoxes().size(); i++){
                    CheckBox earnCB = earnCBAdapter.getCheckBoxes().get(i);
                    if(TextUtils.equals(earnCB.getText().toString().trim(),earn.trim())){
                        earnCBAdapter.getCheckBoxes().get(i).setChecked(true);
                        earnCBAdapter.notifyItemChanged(i);
                    }
                }
            }
        }

        otherET.setText(populateJob.othersDesc);
        workspaceTV.setText(populateJob.workspaceName);
        locationTV.setText(populateJob.location);
        remoteCB.setChecked(populateJob.isRemote);
        locationTV.requestLayout();
        workspaceTV.requestLayout();

        if(populateJob.jobPicUrl != null){
            GeneralFunction.GlideImageSetting(parentActivity,Uri.parse(populateJob.jobPicUrl),uploadPhotoIB);
        }

    }

    private void setupJobList(RecyclerView workscopeRV){
        ArrayList<CheckBox> workscopeCBs = new ArrayList<>();
        final String[] workscopeList = parentActivity.getResources().getStringArray(R.array.jobscope_list);
        for(String workscope : workscopeList){
            CheckBox checkBox = (CheckBox) CheckBox.inflate(parentActivity,R.layout.custom_checkbox,null);
            checkBox.setText(workscope.trim());
            workscopeCBs.add(checkBox);
        }
        workscopeCBAdapter = new CheckboxRecyclerGridAdapter(parentActivity,workscopeCBs);

        workscopeRV.setNestedScrollingEnabled(false);
        workscopeRV.setAdapter(workscopeCBAdapter);
        workscopeRV.setLayoutManager(new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL));
    }

    private void setupEarnList(RecyclerView earnRV){
        ArrayList<CheckBox> earnCBs = new ArrayList<>();
        final String[] earnList = parentActivity.getResources().getStringArray(R.array.job_earning_list);
        for(String earn : earnList){
            CheckBox checkBox = (CheckBox) CheckBox.inflate(parentActivity,R.layout.custom_checkbox,null);
            checkBox.setText(earn.trim());
            earnCBs.add(checkBox);
        }
        earnCBAdapter = new CheckboxRecyclerGridAdapter(parentActivity,earnCBs);

        earnCBAdapter.getCheckBoxes()
                .get(earnList.length - 1)
                .setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if(isChecked){
                            otherET.setVisibility(View.VISIBLE);
                        }else{
                            otherET.setVisibility(View.GONE);
                        }
                    }
                });

        earnRV.setNestedScrollingEnabled(false);
        earnRV.setAdapter(earnCBAdapter);
        earnRV.setLayoutManager(new LinearLayoutManager(parentActivity, LinearLayoutManager.VERTICAL, false));
    }


    private boolean isValidInput(){
        return  GeneralFunction.checkAndReturnCheckboxes(workscopeCBAdapter.getCheckBoxes(),parentActivity.getString(R.string.employer_err_job_jobscope),parentActivity)
                &&
                GeneralFunction.checkAndReturnCheckboxes(earnCBAdapter.getCheckBoxes(),parentActivity.getString(R.string.employer_err_job_earning),parentActivity)
                &&
                GeneralFunction.checkAndReturnString(workspaceTV.getText().toString(),parentActivity.getString(R.string.employer_err_job_workplace),parentActivity);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            if (requestCode == Config.REQUEST_PLACE_PICKER) {
                place = PlacePicker.getPlace(parentActivity, data);

                if (place != null && place.getAddress() != null && !place.getAddress().equals("")) {
                    locationTV.setText(place.getAddress().toString());
                    GeneralFunction.setLocationNameForPlace(parentActivity, place, workspaceTV);
                    locationTV.requestLayout();
                    workspaceTV.requestLayout();
                } else {
                    GeneralFunction.openMessageNotificationDialog(parentActivity, parentActivity.getString(R.string.error_place_picker), R.drawable.notice_bad, TAG);
                }
            }else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                Uri resultUri = result.getUri();
                fileResultUri = resultUri;
                filePath = FilePath.getPath(parentActivity, resultUri);
                GlideImageSetting(parentActivity, resultUri, uploadPhotoIB);
            }else if (requestCode == CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE) {
                if (ContextCompat.checkSelfPermission(parentActivity, Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_DENIED) {
                    ActivityCompat.requestPermissions(parentActivity, new String[]{Manifest.permission.CAMERA}, requestCode);
                }
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void startToSetJobDetails(){
        HashMap<String,Object> jobMap = new HashMap<>();

        if(earnCBAdapter.getCheckBoxes().get(earnCBAdapter.getCheckBoxes().size() - 1).isChecked() && !GeneralFunction.checkAndReturnString(otherET.getText().toString(),parentActivity.getString(R.string.employer_err_job_earning_other),parentActivity)){
            return;
        }else{
            jobMap.put(Job.OTHERS_DESC,otherET.getText().toString());
        }
        parentActivity.initializeProgressIndicator(parentActivity.getString(R.string.employer_job_posting),false,this);
        parentActivity.controlProgressIndicator(true,this);

        for(CheckBox workscopeCB : workscopeCBAdapter.getCheckBoxes()){
            if(workscopeCB != null && workscopeCB.isChecked()){
                jobMap.put(workscopeCB.getText().toString().trim(),true);
            }
        }

        for(CheckBox earnCBs : earnCBAdapter.getCheckBoxes()){
            if(earnCBs != null && earnCBs.isChecked()){
                jobMap.put(earnCBs.getText().toString().trim(),true);
            }
        }

        jobMap.put(Job.WORKSPACE_NAME, workspaceTV.getText().toString());
        jobMap.put(Job.LOCATION, locationTV.getText().toString());
        jobMap.put(Job.IS_REMOTE, remoteCB.isChecked());
        jobMap.put(Job.EMPLOYER_ID, parentActivity.uid);
        jobMap.put(Job.DATE_UPDATED, FieldValue.serverTimestamp());//because i did not use the class so must manually update the timestamp

        //use original GPS
        if(oriJob != null && place == null){
            jobMap.put(Job.GPS, oriJob.gps);
            if(oriJob.placeAddress != null && oriJob.placeAddress.placeId != null){
                jobMap.put(Job.PLACE_ADDRESS, PlaceAddress.getHashmap(oriJob.placeAddress,oriJob.placeAddress.placeId));
            }
            if(oriJob.placeIndex != null){
                jobMap.put(Job.PLACE_INDEX,oriJob.placeIndex);
            }
        }else{ //use new GPS
            jobMap.put(Job.GPS, new GeoPoint(place.getLatLng().latitude,place.getLatLng().longitude));
        }

        //update
        if(oriJob != null){
            jobMap.put(Job.IS_AVAILABLE, oriJob.isAvailable);
            jobMap.put(Job.DATE_CREATED, oriJob.dateCreated);
            jobMap.put(Job.JOB_PIC_URL, oriJob.jobPicUrl);
        }else{ //insert
            jobMap.put(Job.IS_AVAILABLE, true);
            jobMap.put(Job.DATE_CREATED, FieldValue.serverTimestamp());
        }

        preparingUpload(jobMap);
    }

    private void preparingUpload(final HashMap<String,Object> jobMap){
        if(oriJob == null){
            //means need insert
            jobRef = GeneralFunction.getFirestoreInstance()
                    .collection(Job.URL_JOB)
                    .document();
        }else{
            //means need edit
            jobRef = GeneralFunction.getFirestoreInstance()
                    .collection(Job.URL_JOB)
                    .document(oriJob.id);
        }

        if(place != null && place.getId() != null){
            getPlaceDetails(jobRef,jobMap);
        }else{
            uploadJobPhoto(jobRef,jobMap);
        }
    }

    private void getPlaceDetails(final DocumentReference jobRef, final HashMap<String,Object> jobMap){
        new UnifyPlaceAddress(parentActivity, place.getId(), new UnifyPlaceAddress.OnUnifyComplete() {
            @Override
            public void onUnifySuccess(PlaceAddress placeAddress) {
                jobMap.put(Job.PLACE_ADDRESS,PlaceAddress.getHashmap(placeAddress,place.getId()));
                jobMap.put(Job.PLACE_INDEX,PlaceAddress.getPlaceIndex(placeAddress));
                parentActivity.updateProgress(40,EmployerPostJobFragment.this);
                if(filePath == null || filePath.trim().length() <= 0){
                    setJobDetails(jobRef,jobMap);
                }else{
                    uploadJobPhoto(jobRef,jobMap);
                }
            }

            @Override
            public void onUnifyFailed(Exception e) {
                GeneralFunction.handleUploadError(parentActivity,EmployerPostJobFragment.this,R.string.error_occured_getting_place_details,TAG);
                e.printStackTrace();
            }
        });
    }

    private void uploadJobPhoto(final DocumentReference jobRef, final HashMap<String,Object> jobMap){
        if(filePath == null || filePath.trim().isEmpty()){
            setJobDetails(jobRef,jobMap);
            return;
        }
        
        final StorageReference storageReference = GeneralFunction.getFirebaseStorageInstance()
                .child(Job.URL_JOB)
                .child(parentActivity.uid)
                .child(jobRef.getId())
                .child(Job.JOB_PIC_URL);

        UploadTask uploadTask = storageReference.putBytes(GeneralFunction.getCompressedBitmap(parentActivity,filePath));

        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                GeneralFunction.handleUploadError(parentActivity,EmployerPostJobFragment.this,R.string.employer_err_job_posting_job,TAG);
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri downloadUri) {
                        if(downloadUri != null){
                            jobMap.put(Job.JOB_PIC_URL, downloadUri.toString());
                            setJobDetails(jobRef,jobMap);
                        }else{
                            handleUploadError(parentActivity,EmployerPostJobFragment.this,R.string.employer_err_job_posting_job,TAG);
                        }
                    }
                });
            }
        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                // + 10 is because i set the progress to 10 when it finished saving the initial firebase data
                // * 80 is because i want the progress to go to maximum 90% only when the image is finished uploaded
                // so that the final 10% is set when the firebase updates the imageUrl data as the final step
                int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * 50) + 40;
                parentActivity.updateProgress((float) totalProgress,EmployerPostJobFragment.this);
            }
        });
    }

    private void setJobDetails(DocumentReference jobRef, HashMap<String,Object> jobMap){
        jobRef.set(jobMap).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                //before using real time on event listener
//                if(needUpdate && parentActivity != null && parentFragment != null && !parentActivity.isFinishing() && !parentFragment.isRemoving() && !parentFragment.isDetached()){
//                    parentFragment.getThisUserPostedJob();
//                }
                GeneralFunction.handleUploadSuccess(parentActivity,EmployerPostJobFragment.this,R.string.employer_job_upload_success,TAG);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                GeneralFunction.handleUploadError(parentActivity,EmployerPostJobFragment.this,R.string.employer_err_job_posting_job,TAG);
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        editedJob = new Job();

        ArrayList<String> workscopeList = new ArrayList<>();
        for(CheckBox workscopeCB : workscopeCBAdapter.getCheckBoxes()){
            if(workscopeCB != null && workscopeCB.isChecked()){
                workscopeList.add(workscopeCB.getText().toString().trim());
            }
        }
        editedJob.jobScope = workscopeList;

        ArrayList<String> earnList = new ArrayList<>();
        for(CheckBox earnCB : earnCBAdapter.getCheckBoxes()){
            if(earnCB != null && earnCB.isChecked()){
                earnList.add(earnCB.getText().toString().trim());
            }
        }
        editedJob.earnList = earnList;

        editedJob.othersDesc = otherET.getText().toString();
        editedJob.workspaceName = workspaceTV.getText().toString();
        editedJob.location = locationTV.getText().toString();
        editedJob.isRemote = remoteCB.isChecked();
        if(fileResultUri != null){
            editedJob.jobPicUrl = fileResultUri.toString();
        }else{
            if(oriJob != null){
                editedJob.jobPicUrl = oriJob.jobPicUrl;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.frag_employer_post_job));
                parentActivity.selectJob();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.submitTV:
                if(isValidInput()){
                    startToSetJobDetails();
                }
                break;
            case R.id.uploadPhotoIB:
                GeneralFunction.showFileChooser(parentActivity,this);
                break;
        }
    }
}