package com.vidatechft.yupa.employerFragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.EmployerDashboardTabPagerAdapter;

public class EmployerDashboardContainerFragment extends Fragment{
    public static final String TAG = EmployerDashboardContainerFragment.class.getName();
    private MainActivity parentActivity;
    private String lastToolbarTitle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static EmployerDashboardContainerFragment newInstance(MainActivity parentActivity, String lastToolbarTitle) {
        EmployerDashboardContainerFragment fragment = new EmployerDashboardContainerFragment();

        fragment.parentActivity = parentActivity;
        fragment.lastToolbarTitle = lastToolbarTitle;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_employer_dashboard_container, container, false);
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.hideBtmNav();
                TabLayout tabLayout = view.findViewById(R.id.tabLayout);

                tabLayout.addTab(tabLayout.newTab().setText(parentActivity.getString(R.string.employer_posted_job_title)));
                tabLayout.addTab(tabLayout.newTab().setText(parentActivity.getString(R.string.employer_candidate_title)));

                final ViewPager viewPager = view.findViewById(R.id.pager);

                final EmployerDashboardTabPagerAdapter adapter = new EmployerDashboardTabPagerAdapter(parentActivity,EmployerDashboardContainerFragment.this.getChildFragmentManager(), tabLayout.getTabCount(),lastToolbarTitle);
                viewPager.setAdapter(adapter);
                viewPager.setOffscreenPageLimit(1);
                viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

                tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        viewPager.setCurrentItem(tab.getPosition());
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {

                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {

                    }
                });
            }
        });
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.employer_dashboard_toolbar));
                parentActivity.selectJob();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(lastToolbarTitle);
            }
        });
    }
}