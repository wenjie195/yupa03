package com.vidatechft.yupa.employerFragments;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.PTJobNearbyListAdapter;
import com.vidatechft.yupa.adapter.PTJobSwipeAdapter;
import com.vidatechft.yupa.classes.AppliedCandidate;
import com.vidatechft.yupa.classes.Chat;
import com.vidatechft.yupa.classes.Job;
import com.vidatechft.yupa.dialogFragments.ApplyPTJobDialogFragment;
import com.vidatechft.yupa.utilities.Config;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;
import com.vidatechft.yupa.utilities.KeyboardUtilities;
import com.yuyakaido.android.cardstackview.CardStackView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PTJobNearbyListFragment extends Fragment {
    public static final String TAG = PTJobNearbyListFragment.class.getName();

    private MainActivity parentActivity;
    private String lastToolbarTitle;
    private ArrayList<Job> jobs;
    private PTJobSwipeFragment parentFragment;
    private HashMap<String,LatLng> boundingBox;

    private PTJobNearbyListAdapter ptJobNearbyListAdapter;
    private RecyclerView jobRV;
    private TextView noJobTV, locationTV;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static PTJobNearbyListFragment newInstance(MainActivity parentActivity, PTJobSwipeFragment parentFragment, String lastToolbarTitle, ArrayList<Job> jobs,HashMap<String,LatLng> boundingBox) {
        PTJobNearbyListFragment fragment = new PTJobNearbyListFragment();

        fragment.parentActivity = parentActivity;
        fragment.parentFragment = parentFragment;
        fragment.lastToolbarTitle = lastToolbarTitle;
        fragment.boundingBox = boundingBox;
        fragment.jobs = jobs;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_ptjob_list, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.showBtmNav();

                PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                        parentActivity.getFragmentManager().findFragmentById(R.id.nearbyJobListPAF);
                locationTV = view.findViewById(R.id.locationTV);
                noJobTV = view.findViewById(R.id.noJobTV);
                jobRV = view.findViewById(R.id.jobRV);
                Button viewBtn = view.findViewById(R.id.viewBtn);

                ptJobNearbyListAdapter = new PTJobNearbyListAdapter(parentActivity,parentFragment,lastToolbarTitle,jobs);

                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(parentActivity);
                jobRV.setAdapter(ptJobNearbyListAdapter);
                jobRV.setLayoutManager(linearLayoutManager);

                if(!jobs.isEmpty()){
                    ptJobNearbyListAdapter.notifyDataSetChanged();

                }else{
                    noJobTV.setText(parentActivity.getString(R.string.pt_no_jobs_available_map));
                    noJobTV.setVisibility(View.VISIBLE);
                }


                autocompleteFragment.setHint(parentActivity.getString(R.string.pt_map_search_hint));
                autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
                    @Override
                    public void onPlaceSelected(Place place) {
                        searchSelectedNearbyJob(place);
                    }

                    @Override
                    public void onError(Status status) {
                        GeneralFunction.openMessageNotificationDialog(parentActivity,status.getStatusMessage(),R.drawable.notice_bad,TAG);
                    }
                });

                viewBtn.setOnClickListener(new DebouncedOnClickListener(500) {
                    @Override
                    public void onDebouncedClick(View v) {
                        Fragment ptjobMapFragment = PTJobMapFragment.newInstance(parentActivity,lastToolbarTitle,jobs,boundingBox);
                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, ptjobMapFragment).addToBackStack(PTJobMapFragment.TAG).commit();
                    }
                });
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                locationTV = view.findViewById(R.id.locationTV);
                if(!jobs.isEmpty()){
                    locationTV.setText(String.valueOf(jobs.get(0).location));
                }
            }
        });


        new GetCityName(parentActivity,locationTV,boundingBox.get(Config.BOUNDING_BOX_CENTER)).execute();
    }

    //explanation from here https://stackoverflow.com/questions/25647881/android-asynctask-example-and-explanation
    private class GetCityName extends AsyncTask<Void, Integer, String> {

        MainActivity parentActivity;
        TextView locationTV;
        LatLng currentGps;

        public GetCityName(MainActivity parentActivity, TextView locationTV, LatLng currentGps){
            this.parentActivity = parentActivity;
            this.locationTV = locationTV;
            this.currentGps = currentGps;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Do something like display a progress bar
        }

        @Override
        protected String doInBackground(Void... params) {
            // get the string from params, which is an array

            return GeneralFunction.getCityNameByCoordinates(parentActivity,currentGps);
        }

        // This is called from background thread but runs in UI
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            // Do things like update the progress bar
        }

        // This runs in UI when background thread finishes
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(result == null){
                if(!jobs.isEmpty()){
                    locationTV.setText(String.valueOf(jobs.get(0).location));
                }
            }else{
                locationTV.setText(result);
            }
        }
    }

    private void searchSelectedNearbyJob(final Place place){
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                boundingBox = GeneralFunction.getBoundingBoxCoor(place.getLatLng(),Job.JOB_RADIUS);
                GeoPoint btmLeft = new GeoPoint(boundingBox.get(Config.BOUNDING_BOX_BTM_LEFT).latitude,boundingBox.get(Config.BOUNDING_BOX_BTM_LEFT).longitude);
                GeoPoint topRight = new GeoPoint(boundingBox.get(Config.BOUNDING_BOX_TOP_RIGHT).latitude,boundingBox.get(Config.BOUNDING_BOX_TOP_RIGHT).longitude);

                new GetCityName(parentActivity,locationTV,boundingBox.get(Config.BOUNDING_BOX_CENTER)).execute();

                jobs.clear();
                ptJobNearbyListAdapter.notifyDataSetChanged();
                noJobTV.setText(parentActivity.getString(R.string.pt_loading_jobs_list));
                noJobTV.setVisibility(View.VISIBLE);

                GeneralFunction.getFirestoreInstance()
                        .collection(Job.URL_JOB)
                        .whereGreaterThan(Job.GPS,btmLeft)
                        .whereLessThan(Job.GPS,topRight)
                        .whereEqualTo(Job.IS_AVAILABLE,true)
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                if(task.isSuccessful()){
                                    if(!task.getResult().isEmpty()){

                                        for (QueryDocumentSnapshot snapshot : task.getResult()) {
                                            jobs.add(GeneralFunction.getJobscopeAndWorkIcons(parentActivity,new Job(parentActivity,snapshot)));
                                        }

                                        ptJobNearbyListAdapter.notifyDataSetChanged();

                                        noJobTV.setVisibility(View.GONE);
                                    }else{
                                        noJobTV.setText(parentActivity.getString(R.string.pt_no_jobs_available_map));
                                        noJobTV.setVisibility(View.VISIBLE);
                                    }
                                }else{
                                    noJobTV.setText(parentActivity.getString(R.string.pt_err_loading_jobs_map));
                                    noJobTV.setVisibility(View.VISIBLE);
                                }
                            }
                        });
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.setToolbarTitle(parentActivity.getString(R.string.pt_job_map_toolbar));
        parentActivity.selectJob();
    }

    //needs this to prevent duplicate id of map and autocomplete that is making the app crash
    @Override
    public void onDestroyView() {
        super.onDestroyView();

        PlaceAutocompleteFragment p = (PlaceAutocompleteFragment) parentActivity.getFragmentManager()
                .findFragmentById(R.id.nearbyJobListPAF);
        if (p != null)
            parentActivity.getFragmentManager().beginTransaction().remove(p).commit();
    }
}
