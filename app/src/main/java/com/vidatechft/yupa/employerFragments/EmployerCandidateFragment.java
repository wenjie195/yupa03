package com.vidatechft.yupa.employerFragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.JobCandidateAdapter;
import com.vidatechft.yupa.classes.AppliedCandidate;
import com.vidatechft.yupa.classes.Candidate;
import com.vidatechft.yupa.classes.CandidateDetails;
import com.vidatechft.yupa.classes.Job;
import com.vidatechft.yupa.classes.User;
import com.vidatechft.yupa.profileFragments.ConnectNearbyUserMapFragment;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

//TODO USE THE EXISTING POSTED JOB INSTEAD ***********************************
public class EmployerCandidateFragment extends Fragment implements View.OnClickListener{
    public static final String TAG = EmployerCandidateFragment.class.getName();
    private MainActivity parentActivity;
    private String lastToolbarTitle;

    private RecyclerView candidateRV;
    private List<CandidateDetails> candidDetailsDataset = new ArrayList<>();
    private HashMap<String,Candidate> tempCandidMap = new HashMap<>();
    private JobCandidateAdapter candidAdapter;
    private TextView noCandidTV;
    private HashMap<String,User> userDetailsMap = new HashMap<>();
    private ArrayList<String> totalUserList = new ArrayList<>();
    private int jobCount, jobSize, userCount;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static EmployerCandidateFragment newInstance(MainActivity parentActivity, String lastToolbarTitle) {
        EmployerCandidateFragment fragment = new EmployerCandidateFragment();

        fragment.parentActivity = parentActivity;
        fragment.lastToolbarTitle = lastToolbarTitle;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_employer_candidate, container, false);
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                candidateRV = view.findViewById(R.id.candidateRV);
                noCandidTV = view.findViewById(R.id.noCandidTV);
                TextView searchForNearbyPTTV = view.findViewById(R.id.searchForNearbyPTTV);

                searchForNearbyPTTV.setOnClickListener(EmployerCandidateFragment.this);

                candidAdapter = new JobCandidateAdapter(parentActivity,lastToolbarTitle,EmployerCandidateFragment.this, candidDetailsDataset);

                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(parentActivity);
                candidateRV.setAdapter(candidAdapter);
                candidateRV.setLayoutManager(linearLayoutManager);
            }
        });

        if(candidDetailsDataset.size() <= 0){
            getThisUserPostedJob();
        }else{
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    noCandidTV.setVisibility(View.GONE);
                }
            });
        }
        return view;
    }

    private void getThisUserPostedJob(){
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                noCandidTV.setVisibility(View.VISIBLE);
                noCandidTV.setText(parentActivity.getString(R.string.employer_loading_job_candidate));
            }
        });
        candidDetailsDataset.clear();

        GeneralFunction.getFirestoreInstance().collection(Job.URL_JOB)
                .whereEqualTo(Job.EMPLOYER_ID,parentActivity.uid)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if(task.isSuccessful()){
                            jobSize = task.getResult().size();

                            for (QueryDocumentSnapshot snapshot : task.getResult()) {
                                tempCandidMap.put(snapshot.getId(),new Candidate());
                                tempCandidMap.get(snapshot.getId()).job = new Job(parentActivity,snapshot);
                                getAppliedCandidate(snapshot.getReference().collection(AppliedCandidate.URL_CANDIDATE), snapshot.getId());
                            }

                            if(task.getResult().isEmpty()){
                                parentActivity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        noCandidTV.setText(parentActivity.getString(R.string.employer_no_job_posted));
                                        noCandidTV.setVisibility(View.VISIBLE);
                                    }
                                });
                            }
                        }else{
                            parentActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    noCandidTV.setText(parentActivity.getString(R.string.employer_err_loading_job_candidate));
                                    noCandidTV.setVisibility(View.VISIBLE);
                                }
                            });
                        }
                    }
                });
    }

    private void getAppliedCandidate(CollectionReference ref, final String jobId){
        ref.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if(task.isSuccessful()){
                            for (QueryDocumentSnapshot candidSnapshot : task.getResult()) {
                                tempCandidMap.get(jobId).appliedCandidates.put(candidSnapshot.getId(),new AppliedCandidate(candidSnapshot));
                                totalUserList.add(candidSnapshot.getId());
                            }

                            if(task.getResult().isEmpty()){
                                tempCandidMap.remove(jobId);
                            }

                            jobCount++;
                            if(jobCount == jobSize){
                                getUserDetails();
                            }

                        }else{
                            noCandidTV.setVisibility(View.VISIBLE);
                            noCandidTV.setText(parentActivity.getString(R.string.employer_err_loading_job_candidate));
                        }
                    }
                });
    }

    private void getUserDetails(){
        if(totalUserList.size() == 0){
            //all jobs dont have any candiddates
            noCandidTV.setVisibility(View.VISIBLE);
            noCandidTV.setText(parentActivity.getString(R.string.employer_no_job_candid));
        }else{
            //1 shot get all the user details using query where (many where...)
            //the linkedHashedSet will filter out duplicate user id (and the order is maintained)
            Set<String> filteredTotalUserList = new LinkedHashSet<>(totalUserList);
            final int userSize = filteredTotalUserList.size();
            for(String candidId : filteredTotalUserList){
                GeneralFunction.getFirestoreInstance()
                        .collection(User.URL_USER)
                        .document(candidId)
                .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if(task.isSuccessful()){
                            User tempUser = task.getResult().toObject(User.class);
                            if(tempUser != null && tempUser.name != null){
                                tempUser.id = task.getResult().getId();
                                userDetailsMap.put(task.getResult().getId(),tempUser);
                            }//todo maybe add change listener onto the user's isOnline?
                        }

                        userCount++;

                        if(userCount == userSize){
                            updateAdapter();
                        }

                    }
                });
            }
        }

    }

    private void updateAdapter(){
        Iterator jobIterator = tempCandidMap.entrySet().iterator();
        candidDetailsDataset.clear();
        while (jobIterator.hasNext()){
            Map.Entry jobPair = (Map.Entry)jobIterator.next();
            Candidate candidate = (Candidate) jobPair.getValue();

            Iterator appliedCandidIterator = candidate.appliedCandidates.entrySet().iterator();
            while (appliedCandidIterator.hasNext()){
                Map.Entry appliedPair = (Map.Entry)appliedCandidIterator.next();

                CandidateDetails candidateDetails = new CandidateDetails();
                candidateDetails.job = candidate.job;
                candidateDetails.appliedCandidate = (AppliedCandidate) appliedPair.getValue();

                if(userDetailsMap.get(candidateDetails.appliedCandidate.uid) != null){
                    candidateDetails.appliedCandidate.user = userDetailsMap.get(candidateDetails.appliedCandidate.uid);
                    candidDetailsDataset.add(candidateDetails);
                }

                appliedCandidIterator.remove();
            }
                jobIterator.remove();
        }

        if(candidDetailsDataset.size() == 0){
            noCandidTV.setVisibility(View.VISIBLE);
            noCandidTV.setText(parentActivity.getString(R.string.employer_no_job_candid));
        }else{
            candidAdapter.notifyDataSetChanged();
            noCandidTV.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(lastToolbarTitle);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.employer_dashboard_toolbar));
                parentActivity.selectJob();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.searchForNearbyPTTV:
                Fragment connectNearbyUserMapFragment = ConnectNearbyUserMapFragment.newInstance(parentActivity,new User(),true);
                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, connectNearbyUserMapFragment).addToBackStack(ConnectNearbyUserMapFragment.TAG).commit();
                break;
        }
    }
}