package com.vidatechft.yupa.employerFragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.PostedJobRecyclerAdapter;
import com.vidatechft.yupa.classes.Job;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.List;

public class EmployerPostedJobFragment extends Fragment implements View.OnClickListener{
    public static final String TAG = EmployerPostedJobFragment.class.getName();
    private MainActivity parentActivity;
    private String lastToolbarTitle;

    private RecyclerView jobRV;
    private List<String> jobIDDataset = new ArrayList<>();
    private List<Job> jobDataset = new ArrayList<>();
    private PostedJobRecyclerAdapter jobAdapter;
    private TextView noJobTV;

    private String loadingTxt,emptyTxt,errorTxt;
    private ListenerRegistration jobListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static EmployerPostedJobFragment newInstance(MainActivity parentActivity, String lastToolbarTitle) {
        EmployerPostedJobFragment fragment = new EmployerPostedJobFragment();

        fragment.parentActivity = parentActivity;
        fragment.lastToolbarTitle = lastToolbarTitle;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_employer_posted_job, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loadingTxt = parentActivity.getString(R.string.employer_loading_job_posted);
                emptyTxt = parentActivity.getString(R.string.employer_no_job_posted);
                errorTxt = parentActivity.getString(R.string.employer_err_loading_job_posted);

                jobRV = view.findViewById(R.id.jobRV);
                noJobTV = view.findViewById(R.id.noJobTV);
                TextView postJobTV = view.findViewById(R.id.postJobTV);

                postJobTV.setOnClickListener(EmployerPostedJobFragment.this);

                jobAdapter = new PostedJobRecyclerAdapter(parentActivity,EmployerPostedJobFragment.this,jobDataset);

                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(parentActivity);
                jobRV.setAdapter(jobAdapter);
                jobRV.setLayoutManager(linearLayoutManager);

                if(jobDataset.size() <= 0){
                    getThisUserPostedJob();
                }else{
                    noJobTV.setVisibility(View.GONE);
                }

            }
        });
        return view;
    }

    private void getThisUserPostedJob(){
        noJobTV.setVisibility(View.VISIBLE);
        noJobTV.setText(loadingTxt);
        jobDataset.clear();

        jobListener = GeneralFunction.getFirestoreInstance().collection(Job.URL_JOB)
                .whereEqualTo(Job.EMPLOYER_ID,parentActivity.uid)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot snapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w(TAG, "Listen failed.", e);
                            noJobTV.setVisibility(View.VISIBLE);
                            noJobTV.setText(errorTxt);
                            return;
                        }

                        if(snapshots != null && !snapshots.isEmpty()){
                            for(DocumentChange dc : snapshots.getDocumentChanges()){
                                switch (dc.getType()){
                                    case ADDED:
                                        //dont break it. let it go to modified because the first time open this fragment it goes into here instead
                                    case MODIFIED:
                                        for (QueryDocumentSnapshot snapshot : snapshots) {
                                            Job tempJob = new Job(parentActivity,snapshot);

                                            if(jobIDDataset.contains(tempJob.id)){
                                                int position = jobIDDataset.indexOf(tempJob.id);
                                                if(jobDataset.get(position) != null){
                                                    jobDataset.set(position,tempJob);
                                                }else{
                                                    jobDataset.add(tempJob);
                                                }
                                            }else{
                                                jobDataset.add(tempJob);
                                                jobIDDataset.add(tempJob.id);
                                            }
                                        }

                                        jobAdapter.notifyDataSetChanged();
                                        noJobTV.setVisibility(View.GONE);
                                        break;
                                    case REMOVED:
                                        //todo remove something
                                        break;
                                }
                            }
                        }else{
                            noJobTV.setVisibility(View.VISIBLE);
                            noJobTV.setText(emptyTxt);
                        }
                    }
                });
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.employer_dashboard_toolbar));
                parentActivity.selectJob();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(lastToolbarTitle);
                if(jobListener != null){
                    jobListener.remove();
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.postJobTV:
                Fragment postJobFragment = EmployerPostJobFragment.newInstance(parentActivity,parentActivity.getString(R.string.employer_dashboard_toolbar),this,true);
                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, postJobFragment).addToBackStack(EmployerPostJobFragment.TAG).commit();
                break;
        }
    }
}