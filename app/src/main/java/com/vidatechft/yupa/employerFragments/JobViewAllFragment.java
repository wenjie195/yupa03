package com.vidatechft.yupa.employerFragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.ItineraryRecyclerGridAdapter;
import com.vidatechft.yupa.adapter.JobRecyclerGridAdapter;
import com.vidatechft.yupa.classes.Itinerary;
import com.vidatechft.yupa.classes.Job;
import com.vidatechft.yupa.utilities.Config;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.HashMap;

public class JobViewAllFragment extends Fragment{
    public static final String TAG = JobViewAllFragment.class.getName();
    private MainActivity parentActivity;
    private ArrayList<Job> jobList = new ArrayList<>();
    private Query queryRef;

    private RecyclerView jobRV;
    private JobRecyclerGridAdapter jobAdapter;

    private ListenerRegistration jobListener;

    private String loadingTxt,emptyTxt,errorTxt;
    private TextView noJobTV;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static JobViewAllFragment newInstance(MainActivity parentActivity, ArrayList<Job> jobList, Query queryRef) {
        JobViewAllFragment fragment = new JobViewAllFragment();

        fragment.parentActivity = parentActivity;
        if(jobList == null){
            fragment.jobList = new ArrayList<>();
        }else{
            fragment.jobList = jobList;
        }
        fragment.queryRef = queryRef;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_itinerary_view_all, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.showBtmNav();

                loadingTxt = parentActivity.getString(R.string.job_loading_other);
                emptyTxt = parentActivity.getString(R.string.job_empty_other);
                errorTxt = parentActivity.getString(R.string.job_err_loading_other);

                jobRV = view.findViewById(R.id.itiRV);
                noJobTV = view.findViewById(R.id.noItiTV);

                setUpGridAdapter();

                if(jobList.size() <= 0){
                    getJob();
                }else{
                    noJobTV.setVisibility(View.GONE);
                }
            }
        });

        return view;
    }

    private void setUpGridAdapter(){

        jobAdapter = new JobRecyclerGridAdapter(parentActivity,this, jobList);

        jobRV.setAdapter(jobAdapter);
        jobRV.setLayoutManager(new GridLayoutManager(parentActivity, 2));
    }

    private void getJob(){
        //todo the recycler view needs to have lazy load

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                noJobTV.setText(loadingTxt);
                noJobTV.setVisibility(View.VISIBLE);
            }
        });

        Query ref;
        if(queryRef != null){
            ref = queryRef;
        }else{
            //this is for showing all nearby jobs of grid version (probably not needed since design dont have and is only used for search results)
            noJobTV.setText(emptyTxt);
            noJobTV.setVisibility(View.VISIBLE);
            return;
//            ref = GeneralFunction.getFirestoreInstance()
//                    .collection(Job.URL_JOB)
//                    .whereGreaterThan(Job.GPS,boundingBox.get(Config.BOUNDING_BOX_BTM_LEFT))
//                    .whereLessThan(Job.GPS,boundingBox.get(Config.BOUNDING_BOX_TOP_RIGHT))
//                    .whereEqualTo(Job.IS_AVAILABLE,true)
//                    .orderBy(Job.GPS)
//                    .orderBy(Job.DATE_UPDATED, Query.Direction.DESCENDING);
        }

        jobListener = ref
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot snapshots, @javax.annotation.Nullable final FirebaseFirestoreException e) {
                        if (e != null) {
                            parentActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.w(TAG, "Listen failed.", e);
                                    noJobTV.setText(errorTxt);
                                    noJobTV.setVisibility(View.VISIBLE);
                                }
                            });
                            return;
                        }

                        jobList.clear();

                        if (snapshots != null && !snapshots.isEmpty()) {
                            for (QueryDocumentSnapshot snapshot : snapshots) {
                                Job tempJob = new Job(parentActivity, snapshot);
                                jobList.add(tempJob);
                            }

                            parentActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    jobAdapter.notifyDataSetChanged();
                                    noJobTV.setVisibility(View.GONE);
                                }
                            });
                        } else {
                            parentActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    jobList.clear();
                                    jobAdapter.notifyDataSetChanged();

                                    noJobTV.setText(emptyTxt);
                                    noJobTV.setVisibility(View.VISIBLE);
                                }
                            });
                        }
                    }
                });
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.setToolbarTitle(parentActivity.getString(R.string.job_all));
        parentActivity.selectJob();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(jobListener != null){
                    jobListener.remove();
                }
            }
        });
    }

}
