package com.vidatechft.yupa.employerFragments;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.Employer;
import com.vidatechft.yupa.utilities.FilePath;
import com.vidatechft.yupa.utilities.GeneralFunction;

import static android.app.Activity.RESULT_OK;
import static com.vidatechft.yupa.utilities.GeneralFunction.GlideImageSetting;

public class ApplyEmployerFragment extends Fragment implements View.OnClickListener{
    public static final String TAG = ApplyEmployerFragment.class.getName();

    private MainActivity parentActivity;
    private AppCompatEditText nameET, nricET, emailET, contactET;
    private String filePath;
    private ImageButton uploadLicenseIB;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static ApplyEmployerFragment newInstance(MainActivity parentActivity) {
        ApplyEmployerFragment fragment = new ApplyEmployerFragment();

        fragment.parentActivity = parentActivity;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_apply_as_employer, container, false);
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.hideBtmNav();

                uploadLicenseIB = view.findViewById(R.id.uploadLicenseIB);
                AppCompatTextView submitTV = view.findViewById(R.id.submitTV);
                nameET = view.findViewById(R.id.nameET);
                nricET = view.findViewById(R.id.nricET);
                emailET = view.findViewById(R.id.emailET);
                contactET = view.findViewById(R.id.contactET);

                uploadLicenseIB.setOnClickListener(ApplyEmployerFragment.this);
                submitTV.setOnClickListener(ApplyEmployerFragment.this);
            }
        });
        return view;
    }

    private boolean isValidInput(String name, String nric, String email, String contactNo){
        return  GeneralFunction.checkAndReturnString(name,parentActivity.getString(R.string.employer_err_enter_name),parentActivity)
                &&
                GeneralFunction.checkAndReturnString(nric,parentActivity.getString(R.string.employer_err_enter_nric),parentActivity)
                &&
                GeneralFunction.isValidEmail(email,parentActivity)
                &&
                GeneralFunction.checkAndReturnString(contactNo,parentActivity.getString(R.string.employer_err_enter_contact_no),parentActivity)
                &&
                GeneralFunction.checkAndReturnString(filePath,parentActivity.getString(R.string.employer_err_upload_license),parentActivity);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                Uri resultUri = result.getUri();
                filePath = FilePath.getPath(parentActivity, resultUri);
                GlideImageSetting(parentActivity, resultUri,uploadLicenseIB);
            }else if (requestCode == CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE) {
                if (ContextCompat.checkSelfPermission(parentActivity, Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_DENIED) {
                    ActivityCompat.requestPermissions(parentActivity, new String[]{Manifest.permission.CAMERA}, requestCode);
                }
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void setEmployerDetails(Employer employer){
        final DocumentReference employerRef = GeneralFunction.getFirestoreInstance().collection(Employer.URL_EMPLOYER)
                .document(parentActivity.uid);

        //todo make it 1 shot save everything to save api cost?
        employerRef.set(employer)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        parentActivity.updateProgress(10f,ApplyEmployerFragment.this);
                        uploadLicense(employerRef);
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        GeneralFunction.handleUploadError(parentActivity,ApplyEmployerFragment.this,R.string.employer_err_uploading_license,TAG);
                    }
                });
    }

    private void uploadLicense(final DocumentReference employerRef){
        final StorageReference storageReference = GeneralFunction.getFirebaseStorageInstance()
                .child(Employer.URL_EMPLOYER)
                .child(parentActivity.uid)
                .child(Employer.URL_EMPLOYER_DETAILS)
                .child(Employer.URL_EMPLOYER_LICENSE);

        UploadTask uploadTask = storageReference.putBytes(GeneralFunction.getCompressedBitmap(parentActivity,filePath));

        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                GeneralFunction.handleUploadError(parentActivity,ApplyEmployerFragment.this,R.string.employer_err_uploading_license,TAG);
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri downloadUri) {
                        if(downloadUri != null){
                            employerRef.update(Employer.LICENSE_URL, downloadUri.toString())
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            GeneralFunction.handleUploadSuccess(parentActivity,ApplyEmployerFragment.this,R.string.employer_upload_success,TAG);
                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            GeneralFunction.handleUploadError(parentActivity,ApplyEmployerFragment.this,R.string.employer_err_uploading_license,TAG);
                                        }
                            });
                        }else{
                            GeneralFunction.handleUploadError(parentActivity,ApplyEmployerFragment.this,R.string.employer_err_uploading_license,TAG);
                        }
                    }
                });
            }
        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                // + 10 is because i set the progress to 10 when it finished saving the initial firebase data
                // * 80 is because i want the progress to go to maximum 90% only when the image is finished uploaded
                // so that the final 10% is set when the firebase updates the imageUrl data as the final step
                int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * 80) + 10;
                parentActivity.updateProgress((float) totalProgress,ApplyEmployerFragment.this);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.nav_apply_employer));
                parentActivity.selectJob();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.submitTV:
                String name = nameET.getText().toString().trim();
                String nric = nricET.getText().toString().trim();
                String email = emailET.getText().toString().trim();
                String contactNo = contactET.getText().toString().trim();

                if(isValidInput(name,nric,email,contactNo)){
                    parentActivity.initializeProgressIndicator(parentActivity.getString(R.string.employer_uploading_license),false,this);
                    parentActivity.controlProgressIndicator(true,this);
                    setEmployerDetails(new Employer(null,name,nric,email,contactNo,null));
                }
                break;
            case R.id.uploadLicenseIB:
                GeneralFunction.showFileChooser(parentActivity,this);
                break;
        }
    }
}