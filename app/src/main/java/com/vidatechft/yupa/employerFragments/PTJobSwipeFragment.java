package com.vidatechft.yupa.employerFragments;

import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.PTJobSwiperAdapterNew;
import com.vidatechft.yupa.classes.AppliedCandidate;
import com.vidatechft.yupa.classes.Chat;
import com.vidatechft.yupa.classes.Job;
import com.vidatechft.yupa.dialogFragments.ApplyPTJobDialogFragment;
import com.vidatechft.yupa.utilities.Config;
import com.vidatechft.yupa.utilities.GeneralFunction;
import com.yuyakaido.android.cardstackview.CardStackLayoutManager;
import com.yuyakaido.android.cardstackview.CardStackListener;
import com.yuyakaido.android.cardstackview.CardStackView;
import com.yuyakaido.android.cardstackview.Direction;
import com.yuyakaido.android.cardstackview.StackFrom;
import com.yuyakaido.android.cardstackview.SwipeAnimationSetting;

import java.util.ArrayList;
import java.util.HashMap;

//gps refer from here https://github.com/codepath/android_guides/wiki/Retrieving-Location-with-LocationServices-API
//todo add GPS listener so that if the user used the top bar to open gps will auto connect
public class PTJobSwipeFragment extends Fragment implements CardStackListener {
    public static final String TAG = PTJobSwipeFragment.class.getName();
    private MainActivity parentActivity;
    private String lastToolbarTitle;
    private ArrayList<Job> jobList;

    private TextView noJobTV;

    private FusedLocationProviderClient locationProviderClient;
    private LocationCallback locationCallback;

    private HashMap<String,GeoPoint> currentBoundingBox = null;
    private int permissionCheckCount = 0;
    private CardStackLayoutManager manager;
    private PTJobSwiperAdapterNew ptJobSwipeAdapter;
    private CardStackView cardStackView;

    public static PTJobSwipeFragment newInstance(MainActivity parentActivity, String lastToolbarTitle, ArrayList<Job> jobList) {
        PTJobSwipeFragment fragment = new PTJobSwipeFragment();
        fragment.parentActivity = parentActivity;
        fragment.lastToolbarTitle = lastToolbarTitle;
        fragment.jobList = jobList;

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.ptjob_swipe_fragment, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.hideBtmNav();

                cardStackView = view.findViewById(R.id.job_card_stack_view);
                noJobTV = view.findViewById(R.id.noJobTV);

                //todo FOR_NAVIGATION_PURPOSE_ONLY: uncomment initializeCardView() below;
//                initializeCardView();




                //todo FOR_NAVIGATION_PURPOSE_ONLY: delete code below after reviving this feature and uncomment initializeCardView() above;
                noJobTV.setText(parentActivity.getString(R.string.upcoming_new_feature));
                cardStackView.setVisibility(View.GONE);
            }
        });

        return view;
    }

    public HashMap<String,LatLng> getCurrentBoundingBox(){
        if(this.currentBoundingBox != null){
            return GeneralFunction.convertGeoPointToLatLng(this.currentBoundingBox);
        }else{
            return null;
        }
    }

    private void setCurrentBoundingBox(HashMap<String,GeoPoint> currentBoundingBox){
        this.currentBoundingBox = currentBoundingBox;
    }

    private void getNearbyPartTimeJob(final Location currentLocation){
        //stop getting updates
        if(locationProviderClient != null && locationCallback != null){
            locationProviderClient.removeLocationUpdates(locationCallback);
        }

        noJobTV.setText(parentActivity.getString(R.string.pt_loading_jobs));
        noJobTV.setVisibility(View.VISIBLE);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(currentLocation.getLatitude() == 0 && currentLocation.getLongitude() == 0){
                    noJobTV.setVisibility(View.VISIBLE);
                    noJobTV.setText(parentActivity.getString(R.string.err_getting_gps));
                    return;
                }

                HashMap<String,GeoPoint> boundingBox = GeneralFunction.getBoundingBoxCoor(currentLocation,Job.JOB_RADIUS);
                final ArrayList<Job>tempJobList = new ArrayList<>();
                setCurrentBoundingBox(boundingBox);

                GeneralFunction.getFirestoreInstance()
                        .collection(Job.URL_JOB)
                        .whereGreaterThan(Job.GPS,boundingBox.get(Config.BOUNDING_BOX_BTM_LEFT))
                        .whereLessThan(Job.GPS,boundingBox.get(Config.BOUNDING_BOX_TOP_RIGHT))
                        .whereEqualTo(Job.IS_AVAILABLE,true)
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                if(task.isSuccessful()){
                                    if(!task.getResult().isEmpty()){

                                        tempJobList.clear();

                                        for (QueryDocumentSnapshot snapshot : task.getResult()) {
                                            Job tempJob = new Job(parentActivity,snapshot);
                                            tempJobList.add(GeneralFunction.getJobscopeAndWorkIcons(parentActivity,new Job(parentActivity,snapshot)));
//                                            ptJobSwipeAdapter.add(GeneralFunction.getJobscopeAndWorkIcons(parentActivity,new Job(parentActivity,snapshot)));
                                        }
//                                        if(jobList == null){
//                                            jobList = tempJobList;
//                                        }

                                        ptJobSwipeAdapter = new PTJobSwiperAdapterNew(PTJobSwipeFragment.this,parentActivity,tempJobList,lastToolbarTitle);
                                        initializeCardView();
                                        ptJobSwipeAdapter.notifyDataSetChanged();
                                        noJobTV.setVisibility(View.GONE);
                                    }else{
                                        noJobTV.setText(parentActivity.getString(R.string.pt_no_jobs_available));
                                        noJobTV.setVisibility(View.VISIBLE);
                                    }
                                }else{
                                    noJobTV.setVisibility(View.VISIBLE);
                                    noJobTV.setText(parentActivity.getString(R.string.pt_err_loading_jobs));
                                }
                            }
                        });
            }
        });
    }

    /***********************************************************GETTING GPS STUFF START****************************************************/

    private boolean isAllCheckPassed(){
        //if already have data dont refresh it
        if(ptJobSwipeAdapter != null){
            noJobTV.setVisibility(View.GONE);
            return false;
        }

        if (GeneralFunction.dontHaveLocationPermission(parentActivity)) {
            permissionCheckCount++;

            GeneralFunction.requestForLocationPermission(parentActivity,permissionCheckCount);
            noJobTV.setText(parentActivity.getString(R.string.need_enable_gps));
            noJobTV.setVisibility(View.VISIBLE);
            return false;
        }

        if (!GeneralFunction.isGpsEnabled(parentActivity)){
            GeneralFunction.showGpsSettingsAlert(parentActivity);
            noJobTV.setText(parentActivity.getString(R.string.need_on_gps));
            noJobTV.setVisibility(View.VISIBLE);
            return false;
        }else{
            noJobTV.setText(parentActivity.getString(R.string.searching_gps));
            noJobTV.setVisibility(View.VISIBLE);
        }

        return true;
    }

    // Trigger new location updates at interval
    //protected means that this function can only be accessed by the class in the same package only
    protected void startLocationUpdates() {
        parentActivity.runOnUiThread(new Runnable() {
            //added this lint because i actually already checked whether got permission before executung the location updates
            @SuppressLint("MissingPermission")
            @Override
            public void run() {
                if(!isAllCheckPassed()){
                    return;
                }

                ptJobSwipeAdapter = new PTJobSwiperAdapterNew (PTJobSwipeFragment.this,parentActivity,jobList,lastToolbarTitle);

                // Create the location request to start receiving updates
                LocationRequest locationRequest = new LocationRequest();
                locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                locationRequest.setInterval(Config.GPS_UPDATE_INTERVAL);
                locationRequest.setFastestInterval(Config.GPS_FASTEST_INTERVAL);

                // Create LocationSettingsRequest object using location request
                LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
                builder.addLocationRequest(locationRequest);
                LocationSettingsRequest locationSettingsRequest = builder.build();

                // Check whether location settings are satisfied
                // https://developers.google.com/android/reference/com/google/android/gms/location/SettingsClient
                SettingsClient settingsClient = LocationServices.getSettingsClient(parentActivity);
                settingsClient.checkLocationSettings(locationSettingsRequest);

                // new Google API SDK v11 uses getFusedLocationProviderClient(this)
                locationProviderClient = LocationServices.getFusedLocationProviderClient(parentActivity);
                locationCallback = new LocationCallback(){
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        if(locationResult != null && locationResult.getLastLocation() != null){
                            getNearbyPartTimeJob(locationResult.getLastLocation());
                        }else{
                            noJobTV.setVisibility(View.VISIBLE);
                            noJobTV.setText(parentActivity.getString(R.string.err_getting_gps));
                        }
                    }

                    @Override
                    public void onLocationAvailability(LocationAvailability locationAvailability) {
                        if(!locationAvailability.isLocationAvailable()){
                            noJobTV.setVisibility(View.VISIBLE);
                            noJobTV.setText(parentActivity.getString(R.string.err_getting_gps));
                        }
                    }
                };
                locationProviderClient.requestLocationUpdates(locationRequest,locationCallback,Looper.myLooper());
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Config.REQUEST_GPS_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startLocationUpdates();
                } else {
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.need_enable_gps),R.drawable.notice_bad,TAG);
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(locationProviderClient != null && locationCallback != null){
            locationProviderClient.removeLocationUpdates(locationCallback);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        parentActivity.setToolbarTitle(parentActivity.getString(R.string.pt_job_swipe_toolbar));
        parentActivity.selectJob();

        //todo FOR_NAVIGATION_PURPOSE_ONLY: uncomment the whole code below
//        if(jobList != null && !jobList.isEmpty()){
//            ArrayList<Job> tempJobList = new ArrayList<>();
//            for (Job job : jobList) {
//                tempJobList.add(GeneralFunction.getJobscopeAndWorkIcons(parentActivity,job));
//            }
//            ptJobSwipeAdapter = new PTJobSwiperAdapterNew(PTJobSwipeFragment.this,parentActivity,tempJobList,lastToolbarTitle);
//            if(lastToolbarTitle.equals(parentActivity.getString(R.string.results))){
//                cardStackView.setAdapter(ptJobSwipeAdapter);
//            }
//            noJobTV.setVisibility(View.GONE);
//            ptJobSwipeAdapter.notifyDataSetChanged();
//        }else{
//            startLocationUpdates();
//        }
    }

    /***********************************************************GETTING GPS STUFF END****************************************************/

    public void swipeLeft(){
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                SwipeAnimationSetting setting = new SwipeAnimationSetting.Builder()
                        .setDirection(Direction.Left)
                        .setDuration(200)
                        .setInterpolator(new AccelerateInterpolator())
                        .build();
                manager.setSwipeAnimationSetting(setting);
                cardStackView.swipe();
            }
        });
    }

    public void swipeRight(){
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                SwipeAnimationSetting setting = new SwipeAnimationSetting.Builder()
                        .setDirection(Direction.Right)
                        .setDuration(200)
                        .setInterpolator(new AccelerateInterpolator())
                        .build();
                manager.setSwipeAnimationSetting(setting);
                cardStackView.swipe();
            }
        });
    }

    @Override
    public void onCardDragging(Direction direction, float ratio) {

    }

    @Override
    public void onCardSwiped(final Direction direction) {
//        Log.d("CardStackView", "onCardSwiped: p = " + manager.getTopPosition() + ", d = " + direction);
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (manager.getTopPosition() == ptJobSwipeAdapter.getItemCount()) {
                    parentActivity.onBackPressed();
                    String msg = parentActivity.getString(R.string.pt_no_more_jobs);
                    if(jobList != null && !jobList.isEmpty()){
                        msg = parentActivity.getString(R.string.pt_no_more_jobs_other);
                    }
                    GeneralFunction.openMessageNotificationDialog(parentActivity,msg,R.drawable.notice_bad,TAG);
                }

                try{
                    if(ptJobSwipeAdapter.getJobsFromAdapter(manager.getTopPosition()) != null){
                        final Job tempJob = ptJobSwipeAdapter.getJobsFromAdapter(manager.getTopPosition());
                        if(direction.toString().equals(Job.SWIPE_RIGHT) && tempJob != null){
                            AppliedCandidate appliedCandidate = new AppliedCandidate(false,"", Chat.TYPE_JOB_DIRECT + tempJob.employerId + parentActivity.uid);

                            if(parentActivity != null && !parentActivity.isFinishing() && GeneralFunction.isAppInForground(parentActivity.getApplicationContext())){
                                ArrayList<Job> jobs = new ArrayList<>();

                                for(int i = 0; i < ptJobSwipeAdapter.getItemCount(); i++){
                                    jobs.add(ptJobSwipeAdapter.getJobs().get(i));
                                }

                                if(parentActivity != null && !parentActivity.isFinishing() && GeneralFunction.isAppInForground(parentActivity.getApplicationContext())){
                                    FragmentManager fm = parentActivity.getSupportFragmentManager();
                                    ApplyPTJobDialogFragment applyPTJobDialogFragment = ApplyPTJobDialogFragment.newInstance(parentActivity,PTJobSwipeFragment.this,lastToolbarTitle,appliedCandidate,tempJob,jobs,true);
                                    applyPTJobDialogFragment.show(fm, TAG);
                                }
                            }
                        }
                    }
                }catch (Exception e){
                    Log.e(TAG,e.toString());
                }
            }
        });
    }

    @Override
    public void onCardRewound() {

    }

    @Override
    public void onCardCanceled() {

    }

//    @Override
//    public void onCardAppeared(View view, int position) {
//
//    }
//
//    @Override
//    public void onCardDisappeared(View view, int position) {
//
//    }

    private void initializeCardView() {
        manager = new CardStackLayoutManager(parentActivity, this);
        manager.setStackFrom(StackFrom.None);
        manager.setVisibleCount(3);
        manager.setTranslationInterval(8.0f);
        manager.setScaleInterval(0.95f);
        manager.setSwipeThreshold(0.3f);
        manager.setMaxDegree(20.0f);
        manager.setDirections(Direction.HORIZONTAL);
        manager.setCanScrollHorizontal(true);
        manager.setCanScrollVertical(true);
        cardStackView.setLayoutManager(manager);
        cardStackView.setNestedScrollingEnabled(false);
        cardStackView.setAdapter(ptJobSwipeAdapter);
    }
}
