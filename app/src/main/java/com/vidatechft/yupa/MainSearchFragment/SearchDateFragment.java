package com.vidatechft.yupa.MainSearchFragment;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.Search;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.TimeZone;

public class SearchDateFragment extends Fragment implements View.OnClickListener, DatePickerDialog.OnDateSetListener{
    public static final String TAG = SearchDateFragment.class.getName();
    private MainActivity parentActivity;
    Search search;

    private TextView locationTV,dateTV;
    private DatePickerDialog datePickerDialog;
    private long selectedDateInMili;

    private SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        parentActivity.setBoundingBox(null);
    }

    public static SearchDateFragment newInstance(MainActivity parentActivity, Search search) {
        SearchDateFragment fragment = new SearchDateFragment();

        fragment.parentActivity = parentActivity;
        fragment.search = search;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_main_search_date, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.hideBtmNav();

                ImageButton backIB = view.findViewById(R.id.backIB);
                locationTV = view.findViewById(R.id.locationTV);
                dateTV = view.findViewById(R.id.dateTV);
                View emptyView = view.findViewById(R.id.emptyView);
                TextView startExploreTV = view.findViewById(R.id.startExploreTV);

                locationTV.setText(search.title);
                datePickerDialog = GeneralFunction.getDatePickerDialog(parentActivity,SearchDateFragment.this);
//                datePickerDialog.getDatePicker().setMinDate(new Date().getTime());

                if(selectedDateInMili > 0){
                    dateTV.setText(format.format(selectedDateInMili));
                }

                backIB.setOnClickListener(SearchDateFragment.this);
                locationTV.setOnClickListener(SearchDateFragment.this);
                dateTV.setOnClickListener(SearchDateFragment.this);
                emptyView.setOnClickListener(SearchDateFragment.this);
                startExploreTV.setOnClickListener(SearchDateFragment.this);
            }
        });

        return view;
    }

    @Override
    public void onDateSet(DatePicker view, final int year, final int month, final int dayOfMonth) {
        parentActivity.runOnUiThread(new Runnable() {
            public void run() {
                Calendar calendar = GeneralFunction.getDefaultUtcCalendar(year,month,dayOfMonth);

                selectedDateInMili = calendar.getTimeInMillis();
                dateTV.setText(format.format(selectedDateInMili));

            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        parentActivity.showToolbar();
    }

    @Override
    public void onResume() {
        super.onResume();

        parentActivity.hideToolbar();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.backIB:
                parentActivity.onBackPressed();
                break;
            case R.id.locationTV:
                parentActivity.onBackPressed();
                break;
            case R.id.dateTV:
                datePickerDialog.show();
                break;
            case R.id.emptyView:
                parentActivity.setBoundingBox(search.boundingBox);
                parentActivity.goToHome();
                break;
            case R.id.startExploreTV:
                search.date = selectedDateInMili;
                Fragment searchResultFragment = SearchResultFragment.newInstance(parentActivity,search,Search.TYPE_HOMESTAY,false);
                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, searchResultFragment).addToBackStack(SearchResultFragment.TAG).commit();
                break;
        }
    }
}
