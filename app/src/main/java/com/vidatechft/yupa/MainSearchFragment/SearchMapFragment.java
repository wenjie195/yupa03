package com.vidatechft.yupa.MainSearchFragment;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatSeekBar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.Search;
import com.vidatechft.yupa.utilities.Config;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.HashMap;

public class SearchMapFragment extends Fragment implements OnMapReadyCallback, View.OnClickListener {
    public static final String TAG = SearchMapFragment.class.getName();
    private MainActivity parentActivity;
    private int searchType;

    private GoogleMap googleMap;

    private TextView radiusTV;
    private int radius = 500;
    private AppCompatSeekBar radiusSB;

    private FusedLocationProviderClient locationProviderClient;
    private LocationCallback locationCallback;
    private LatLng currentLocation;
    private Place place;
    private HashMap<String,LatLng> boundingBox;

    private String title = "???";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static SearchMapFragment newInstance(MainActivity parentActivity,HashMap<String,LatLng> boundingBox, int searchType) {
        SearchMapFragment fragment = new SearchMapFragment();

        fragment.parentActivity = parentActivity;
        fragment.boundingBox = boundingBox;
        fragment.searchType = searchType;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_main_search_map, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.hideBtmNav();

                SupportMapFragment mapFragment = (SupportMapFragment) SearchMapFragment.this.getChildFragmentManager()
                        .findFragmentById(R.id.searchItiGoogleMap);
                mapFragment.getMapAsync(SearchMapFragment.this);

                PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                        parentActivity.getFragmentManager().findFragmentById(R.id.searchItiPAF);
                autocompleteFragment.setHint(parentActivity.getString(R.string.pt_map_search_hint));

                View touchView = view.findViewById(R.id.touchView);
                TextView nextTV = view.findViewById(R.id.nextTV);
                radiusTV = view.findViewById(R.id.radiusTV);
                radiusSB = view.findViewById(R.id.radiusSB);

                final View autoCompleteFragmentView = autocompleteFragment.getView();
                touchView.setOnClickListener(new DebouncedOnClickListener(500) {
                    @Override
                    public void onDebouncedClick(View v) {
                        if (autoCompleteFragmentView != null) {
                            //why so mahuan is because the map is distorted when keyboard goes up so i put adjust nothing here then when resume it reverts back to normal
                            parentActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
                            autoCompleteFragmentView.findViewById(R.id.place_autocomplete_search_input).performClick();
                        }else{
                            Toast.makeText(parentActivity,"Error opening auto complete fragment",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                nextTV.setOnClickListener(SearchMapFragment.this);

                autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
                    @Override
                    public void onPlaceSelected(Place place) {
                        if(googleMap != null){
                            SearchMapFragment.this.place = place;
                            boundingBox = GeneralFunction.getBoundingBoxCoor(place.getLatLng(),radius);
                            populateMap(place.getLatLng());
                        }
                    }

                    @Override
                    public void onError(Status status) {
                        GeneralFunction.openMessageNotificationDialog(parentActivity,status.getStatusMessage(),R.drawable.notice_bad,TAG);
                    }
                });
            }
        });

        setupSeekbar();

        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;

        this.googleMap.getUiSettings().setMapToolbarEnabled(false);

        this.googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                place = null;
                boundingBox = GeneralFunction.getBoundingBoxCoor(latLng,radius);
                populateMap(latLng);
            }
        });

        if(boundingBox != null && !boundingBox.isEmpty()){
            populateMap(boundingBox.get(Config.BOUNDING_BOX_CENTER));
        }else{
            populateMap(currentLocation);
        }
    }

    private void setupSeekbar(){
        radiusSB.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(progress == 0){
                    progress = 1;
                }

                progress = progress * Config.METER_MULTIPLIER;
                radius = progress;
                radiusTV.setText(parentActivity.getString(R.string.radius_in_meter,progress));

                if(googleMap != null){
                    if(boundingBox != null && !boundingBox.isEmpty()){
                        populateMap(boundingBox.get(Config.BOUNDING_BOX_CENTER));
                    }else{
                        populateMap(currentLocation);
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        radiusSB.setProgress(5);
    }

    private void populateMap(final LatLng centerPoint){
        if(googleMap == null){
            return;
        }
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                googleMap.clear();

                if(centerPoint != null){
                    title = String.valueOf(centerPoint.latitude) + "," + String.valueOf(centerPoint.longitude);
                    if(place != null){
                        title = GeneralFunction.getCountryStateFromPlace(place);

                        if(TextUtils.equals(title,"???")){
                            title = String.valueOf(centerPoint.latitude) + "," + String.valueOf(centerPoint.longitude);
                        }

                        googleMap.addMarker(new MarkerOptions()
                                .title(title)
                                .position(centerPoint)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_gps)))
                                .setZIndex(0.1f);
                    }else{
                        new GetCountryStateName(parentActivity,googleMap,centerPoint).execute();
                    }

                    //these codes(until animate camera) will zoom in google map based on how large the radius is
                    boundingBox = GeneralFunction.getBoundingBoxCoor(centerPoint,radius);
                    LatLngBounds.Builder builder = LatLngBounds.builder();
                    builder.include(boundingBox.get(Config.BOUNDING_BOX_TOP_LEFT));
                    builder.include(boundingBox.get(Config.BOUNDING_BOX_BTM_RIGHT));
                    final LatLngBounds bounds = builder.build();
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));

                    googleMap.addCircle(GeneralFunction.getMapCircle(centerPoint,radius,Config.MAP_COLOUR_GREEN));
                }
            }
        });
    }

    private class GetCountryStateName extends AsyncTask<Void, Integer, String> {

        MainActivity parentActivity;
        GoogleMap googleMap;
        LatLng currentGps;

        public GetCountryStateName(MainActivity parentActivity, GoogleMap googleMap, LatLng currentGps){
            this.parentActivity = parentActivity;
            this.googleMap = googleMap;
            this.currentGps = currentGps;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Do something like display a progress bar
        }

        @Override
        protected String doInBackground(Void... params) {
            // get the string from params, which is an array

            return GeneralFunction.getCountryStateByCoordinates(parentActivity,currentGps);
        }

        // This is called from background thread but runs in UI
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            // Do things like update the progress bar
        }

        // This runs in UI when background thread finishes
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(result == null){
                title = String.valueOf(currentGps.latitude) + "," + String.valueOf(currentGps.longitude);
            }else{
                title = result;
            }

            if(googleMap != null){
                googleMap.addMarker(new MarkerOptions()
                        .title(title)
                        .position(currentGps)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_gps)))
                        .setZIndex(0.1f);
            }
        }
    }

    /***********************************************************GETTING GPS STUFF START****************************************************/

    private boolean isAllCheckPassed() {
        return !GeneralFunction.dontHaveLocationPermission(parentActivity) && GeneralFunction.isGpsEnabled(parentActivity);

    }

    // Trigger new location updates at interval
    //protected means that this function can only be accessed by the class in the same package only
    protected void startLocationUpdates() {

        if(boundingBox != null && !boundingBox.isEmpty()){
            return;
        }

        parentActivity.runOnUiThread(new Runnable() {
            //added this lint because i actually already checked whether got permission before executung the location updates
            @SuppressLint("MissingPermission")
            @Override
            public void run() {
                if(!isAllCheckPassed()){
                    return;
                }

                // Create the location request to start receiving updates
                LocationRequest locationRequest = new LocationRequest();
                locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                locationRequest.setInterval(Config.GPS_UPDATE_INTERVAL);
                locationRequest.setFastestInterval(Config.GPS_FASTEST_INTERVAL);

                // Create LocationSettingsRequest object using location request
                LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
                builder.addLocationRequest(locationRequest);
                LocationSettingsRequest locationSettingsRequest = builder.build();

                // Check whether location settings are satisfied
                // https://developers.google.com/android/reference/com/google/android/gms/location/SettingsClient
                SettingsClient settingsClient = LocationServices.getSettingsClient(parentActivity);
                settingsClient.checkLocationSettings(locationSettingsRequest);

                // new Google API SDK v11 uses getFusedLocationProviderClient(this)
                locationProviderClient = LocationServices.getFusedLocationProviderClient(parentActivity);
                locationCallback = new LocationCallback(){
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        if(locationProviderClient != null && locationCallback != null){
                            locationProviderClient.removeLocationUpdates(locationCallback);
                        }

                        if(locationResult != null && locationResult.getLastLocation() != null){
                            currentLocation = new LatLng(locationResult.getLastLocation().getLatitude(),locationResult.getLastLocation().getLongitude());
                            populateMap(currentLocation);
                        }
                    }

                    @Override
                    public void onLocationAvailability(LocationAvailability locationAvailability) {
                        super.onLocationAvailability(locationAvailability);
                        if(locationProviderClient != null && locationCallback != null && !locationAvailability.isLocationAvailable()){
                            locationProviderClient.removeLocationUpdates(locationCallback);
                        }
                    }
                };
                locationProviderClient.requestLocationUpdates(locationRequest,locationCallback, Looper.myLooper());
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        if(locationProviderClient != null && locationCallback != null){
            locationProviderClient.removeLocationUpdates(locationCallback);
        }
    }

    /***********************************************************GETTING GPS STUFF END****************************************************/

    @Override
    public void onResume() {
        super.onResume();
        startLocationUpdates();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.map_result));
                parentActivity.selectHotel();
                parentActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
            }
        });

    }

    //needs this to prevent duplicate id of map and autocomplete that is making the app crash
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        MapFragment f = (MapFragment) parentActivity.getFragmentManager()
                .findFragmentById(R.id.searchItiGoogleMap);
        if (f != null)
            parentActivity.getFragmentManager().beginTransaction().remove(f).commit();

        PlaceAutocompleteFragment p = (PlaceAutocompleteFragment) parentActivity.getFragmentManager()
                .findFragmentById(R.id.searchItiPAF);
        if (p != null)
            parentActivity.getFragmentManager().beginTransaction().remove(p).commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.nextTV:
                if(boundingBox == null || boundingBox.isEmpty()){
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_search_iti_map),R.drawable.notice_bad, TAG);
                }else{
                    if(searchType == Config.SEARCH_TYPE_HOST_ROOM){
                        Fragment searchHostRoomDateFragment = SearchHostRoomDateFragment.newInstance(parentActivity, new Search(title,boundingBox, Long.valueOf(radius)));
                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, searchHostRoomDateFragment).addToBackStack(SearchHostRoomDateFragment.TAG).commit();
                    }else{
                        Fragment searchDateFragment = SearchDateFragment.newInstance(parentActivity,new Search(title,boundingBox, Long.valueOf(radius)));
                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, searchDateFragment).addToBackStack(SearchDateFragment.TAG).commit();
                    }
                }

                break;
        }
    }
}
