package com.vidatechft.yupa.MainSearchFragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.HostStayRecyclerLinearAdapter;
import com.vidatechft.yupa.classes.Accommodation;
import com.vidatechft.yupa.classes.BookingHistory;
import com.vidatechft.yupa.classes.CurrencyRate;
import com.vidatechft.yupa.classes.Job;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.classes.Search;
import com.vidatechft.yupa.dialogFragments.HostRoomClusterListDialogFragment;
import com.vidatechft.yupa.hostFragments.HostStayMainPageFragment;
import com.vidatechft.yupa.utilities.Config;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;

public class SearchResultHostRoomMapFragment extends Fragment implements OnMapReadyCallback {
    public static final String TAG = SearchResultHostRoomMapFragment.class.getName();
    private MainActivity parentActivity;
    private Search search;

    private GoogleMap googleMap;
    private ClusterManager<Room> mClusterManager;

    private Room selectedRoom;
    private ArrayList<Room> rooms = new ArrayList<>();
    private ArrayList<String> roomsId = new ArrayList<>();

    private RecyclerView hostRoomRV;
    private HostStayRecyclerLinearAdapter hostRoomAdapter;

    private int downloadedRoomCount = 0;

    //this is the marker's view
    private ViewGroup infoWindow;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static SearchResultHostRoomMapFragment newInstance(MainActivity parentActivity, Search search) {
        SearchResultHostRoomMapFragment fragment = new SearchResultHostRoomMapFragment();

        fragment.parentActivity = parentActivity;
        fragment.search = search;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_main_host_room_search_result_map, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.hideBtmNav();

                hostRoomRV = view.findViewById(R.id.hostRoomRV);

                SupportMapFragment mapFragment = (SupportMapFragment) SearchResultHostRoomMapFragment.this.getChildFragmentManager()
                        .findFragmentById(R.id.hostRoomResultMap);
                mapFragment.getMapAsync(SearchResultHostRoomMapFragment.this);

                setupAdapter();
                if(rooms == null || rooms.isEmpty()){
                    getHostRoom();
                }

            }
        });

        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        setupInfoWindow();
        populateMap();
    }

    private void setupAdapter(){
        hostRoomAdapter = new HostStayRecyclerLinearAdapter(parentActivity,null, rooms, roomsId,false,search);

        hostRoomRV.setNestedScrollingEnabled(false);
        hostRoomRV.setAdapter(hostRoomAdapter);
        hostRoomRV.setLayoutManager(new LinearLayoutManager(parentActivity, LinearLayoutManager.HORIZONTAL, false));
    }

    private void setupInfoWindow(){
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(infoWindow == null){
                    infoWindow = (ViewGroup)getLayoutInflater().inflate(R.layout.info_window_google_map_marker_with_image, infoWindow);
                }

                final ImageView infoIV = infoWindow.findViewById(R.id.infoIV);
                final TextView infoAccName = infoWindow.findViewById(R.id.infoAccName);
                final TextView infoAdd = infoWindow.findViewById(R.id.infoAdd);

                googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                    @Override
                    public View getInfoWindow(Marker marker) {
                        if(selectedRoom != null){
                            if(selectedRoom.currencyValue != null && selectedRoom.currencyType != null){
                                infoAccName.setText(GeneralFunction.getHostStayPrice(parentActivity,selectedRoom));
                                infoAccName.requestLayout();
                            }

                            if(selectedRoom.accommodationName != null){
                                infoAdd.setText(selectedRoom.accommodationName);
                                infoAdd.requestLayout();
                            }

                            if(selectedRoom.urlOutlook != null){
                                GeneralFunction.GlideImageSettingShrinkImage(parentActivity, Uri.parse(selectedRoom.urlOutlook),infoIV,marker);
                            }
                            return infoWindow;
                        }else{
                            return null;
                        }
                    }

                    @Override
                    public View getInfoContents(final Marker marker) {
                        return null;
                    }
                });

                googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                    @Override
                    public void onInfoWindowClick(Marker marker) {
                        if(selectedRoom != null){
                            Fragment hostStayMainPageFragment = HostStayMainPageFragment.newInstance(parentActivity,parentActivity.getString(R.string.results),selectedRoom,selectedRoom.id,search);
                            GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, hostStayMainPageFragment).addToBackStack(HostStayMainPageFragment.TAG).commit();
                        }
                    }
                });

                googleMap.setOnInfoWindowCloseListener(new GoogleMap.OnInfoWindowCloseListener() {
                    @Override
                    public void onInfoWindowClose(Marker marker) {
                        selectedRoom = null;
                    }
                });
            }
        });
    }

    private void getHostRoom(){
        Query hostRef = GeneralFunction.getFirestoreInstance()
                .collection(Accommodation.URL_ACCOMMODATION)
                .whereEqualTo(Room.STATUS, Room.STATUS_APPROVED)
                .whereEqualTo(Room.IS_PUBLISH, true)
                .whereEqualTo(Room.IS_DRAFT, false);

        if(search != null){
            if(search.boundingBox != null && search.boundingBox.get(Config.BOUNDING_BOX_BTM_LEFT) != null && search.boundingBox.get(Config.BOUNDING_BOX_TOP_RIGHT) != null){
                hostRef = hostRef.whereGreaterThan(Room.ACCOMMODATION_GEOPOINT,new GeoPoint(search.boundingBox.get(Config.BOUNDING_BOX_BTM_LEFT).latitude,search.boundingBox.get(Config.BOUNDING_BOX_BTM_LEFT).longitude))
                        .whereLessThan(Room.ACCOMMODATION_GEOPOINT,new GeoPoint(search.boundingBox.get(Config.BOUNDING_BOX_TOP_RIGHT).latitude,search.boundingBox.get(Config.BOUNDING_BOX_TOP_RIGHT).longitude));
            }

            if(search.roomType != null){
                hostRef = hostRef.whereEqualTo(Room.ROOM_TYPE,search.roomType);
            }
        }

         hostRef.orderBy(Room.ACCOMMODATION_GEOPOINT)
                 .orderBy(Room.DATE_UPDATED, Query.Direction.DESCENDING)
                 .get()
                 .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if(GeneralFunction.hasError(parentActivity,task,TAG)){
                            return;
                        }

                        rooms.clear();
                        roomsId.clear();

                        final QuerySnapshot snapshots = task.getResult();

                        if (snapshots != null && !snapshots.isEmpty()) {
                            for (final QueryDocumentSnapshot snapshot : snapshots) {
                                final Room tempHost = Room.snapshotToRoom(parentActivity, snapshot);

                                boolean allowBook = true;
                                if(search.minBudget > 0 && search.maxBudget > 0 ){
                                    if(!filterPrice(tempHost)){
                                        allowBook = false;
                                    }
                                }

                                if(search.noofBed > 0){
                                    if(!filterNoofBed(tempHost)){
                                        allowBook = false;
                                    }
                                }

                                if(search.noofGuest > 0){
                                    if(!filterNoofGuest(tempHost)){
                                        allowBook = false;
                                    }
                                }

                                if(search.amenityList != null && !search.amenityList.isEmpty()){
                                    if(!filterAmenities(tempHost)){
                                        allowBook = false;
                                    }
                                }

                                if(allowBook){
                                    checkBookingDateGotClashed(tempHost,snapshot,snapshots);
                                }else{
                                    downloadedRoomCount++;
                                    handleAllRoomBookingRequestDataDownloaded(snapshots);
                                }
                            }
                        } else {
                            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.no_result_found),R.drawable.notice_bad,TAG);
                            populateMap();
                            rooms.clear();
                            roomsId.clear();
                        }
                    }
                });
    }

    //returns true if fulfills the price range else false
    private boolean filterPrice(Room tempHost){
        if(tempHost.currencyType != null && tempHost.currencyValue != null){
            try{
                String localCurrencyCode = GeneralFunction.getCurrencyCode(parentActivity);
                double convertedPrice = CurrencyRate.getUserLocalPrice(parentActivity,localCurrencyCode,tempHost.currencyType,tempHost.currencyValue);
                if(convertedPrice >= search.minBudget && convertedPrice <= search.maxBudget){
                    return true;
                }else{
                    return false;
                }
            }catch (Exception e){
                Log.e(TAG,e.getMessage());
                return false;
            }
        }else{
            return false;
        }
    }

    private boolean filterNoofBed(Room tempHost){
        if(tempHost.bedroomQuantity != null){
            try{
                if(tempHost.bedroomQuantity >= search.noofBed){
                    return true;
                }else{
                    return false;
                }
            }catch (Exception e){
                Log.e(TAG,e.getMessage());
                return false;
            }
        }else{
            return false;
        }
    }

    private boolean filterNoofGuest(Room tempHost){
        if(tempHost.guestQuantity != null){
            try{
                if(tempHost.guestQuantity >= search.noofGuest){
                    return true;
                }else{
                    return false;
                }
            }catch (Exception e){
                Log.e(TAG,e.getMessage());
                return false;
            }
        }else{
            return false;
        }
    }

    private boolean filterAmenities(Room tempHost){
        if(tempHost.amenityList != null && !tempHost.amenityList.isEmpty()){
            for(String filterAmenity : search.amenityList){
                if(!tempHost.amenityList.contains(filterAmenity)){
                    return false;
                }
            }
            return true;
        }else{
            return false;
        }
    }

    private void checkBookingDateGotClashed(final Room tempHost, final QueryDocumentSnapshot snapshot, final QuerySnapshot snapshots){
        //this is to get all booking history of each room that starts from the searched checkin date until to the VERY LAST
        GeneralFunction.getFirestoreInstance()
                .collection(BookingHistory.URL_BOOKING_HISTORY)
                .whereEqualTo(BookingHistory.TARGET_ID,tempHost.id)
                .whereEqualTo(BookingHistory.ACTION_TYPE,BookingHistory.ACTION_TYPE_ACCEPTED)
                .whereGreaterThanOrEqualTo(BookingHistory.CHECKOUT_DATE,search.checkin_date)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if(GeneralFunction.hasError(parentActivity,task,TAG)){
                            return;
                        }

                        boolean isClashed = false;

                        if(task.getResult() != null && !task.getResult().isEmpty()){
                            for(DocumentSnapshot bkHistorySnapshot : task.getResult().getDocuments()){
                                BookingHistory bookingHistory = bkHistorySnapshot.toObject(BookingHistory.class);
                                if (bookingHistory != null && bookingHistory.checkinDate != null && bookingHistory.checkoutDate != null) {
                                    isClashed = GeneralFunction.isSearchedDateClashedWithOtherBookedDates(
                                            bookingHistory.checkinDate,bookingHistory.checkoutDate,search.checkin_date,search.checkout_date);
                                    if(isClashed){
                                        break;
                                    }
                                }
                            }
                        }

                        if(!isClashed){
                            tempHost.id = snapshot.getId();
                            rooms.add(tempHost);
                            roomsId.add(snapshot.getId());
                        }

                        downloadedRoomCount++;
                        handleAllRoomBookingRequestDataDownloaded(snapshots);
                    }
                });
    }

    private void handleAllRoomBookingRequestDataDownloaded(QuerySnapshot snapshots){
        //if all room already got their booking history, proceed
        if(downloadedRoomCount == snapshots.size() || snapshots.isEmpty()){
            //reset back to zero
            downloadedRoomCount = 0;
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(rooms.isEmpty()){
                        GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.no_result_found),R.drawable.notice_bad,TAG);
                        populateMap();
                    }else{
                        populateMap();
                    }
                    hostRoomAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    private void populateMap(){
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                googleMap.clear();
                mClusterManager = new ClusterManager<>(parentActivity, googleMap);
                mClusterManager.setRenderer(new CustomMapRenderer<>(parentActivity, googleMap, mClusterManager));
                googleMap.setOnCameraIdleListener(mClusterManager);
                googleMap.setOnMarkerClickListener(mClusterManager);
                googleMap.getUiSettings().setZoomControlsEnabled(true);

                if(search != null){
                    int radius = Job.JOB_RADIUS;
                    if(search.radius != null){
                        radius = search.radius.intValue();
                    }

                    if(search.boundingBox != null){
                        googleMap.addMarker(new MarkerOptions()
                                .title(parentActivity.getString(R.string.you_are_here))
                                .position(search.boundingBox.get(Config.BOUNDING_BOX_CENTER))
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_gps)))
                                .setZIndex(0.1f);

                        CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                                search.boundingBox.get(Config.BOUNDING_BOX_CENTER), Job.JOB_ZOOM_IN_LEVEL);//the bigger the number, the deeper it goes
                        googleMap.animateCamera(location);

                        googleMap.addCircle(GeneralFunction.getMapCircle(search.boundingBox.get(Config.BOUNDING_BOX_CENTER),radius,Config.MAP_COLOUR_GREEN));
                    }
                }

                if(!rooms.isEmpty()){
                    mClusterManager.addItems(rooms);
                }

                mClusterManager.setOnClusterClickListener(new ClusterManager.OnClusterClickListener<Room>() {
                    @Override
                    public boolean onClusterClick(Cluster<Room> cluster) {

                        if(googleMap.getCameraPosition().zoom >= 20){
                            if(parentActivity != null && !parentActivity.isFinishing() && GeneralFunction.isAppInForground(parentActivity.getApplicationContext())){
                                FragmentManager fm = parentActivity.getSupportFragmentManager();
                                HostRoomClusterListDialogFragment hostRoomClusterListDialogFragment = HostRoomClusterListDialogFragment.newInstance(parentActivity,new ArrayList<>(cluster.getItems()),search);
                                hostRoomClusterListDialogFragment.show(fm, TAG);
                            }
                        }else{
                            //zoom in the cluster to uncluster it
                            //from here https://stackoverflow.com/questions/25395357/android-how-to-uncluster-on-single-tap-on-a-cluster-marker-maps-v2
                            LatLngBounds.Builder builder = LatLngBounds.builder();
                            for (ClusterItem item : cluster.getItems()) {
                                builder.include(item.getPosition());
                            }
                            final LatLngBounds bounds = builder.build();
                            googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
                        }

                        return true;
                    }
                });

                mClusterManager.setOnClusterItemClickListener(new ClusterManager.OnClusterItemClickListener<Room>() {
                    @Override
                    public boolean onClusterItemClick(Room room) {
                        selectedRoom = room;
                        return false;
                    }
                });
            }
        });
    }

    //because the default google map cluster manager will only cluster the marker if there is more than 4 marker on same position
    //so we need to overwrite it ourselves by using the class below
    class CustomMapRenderer<T extends ClusterItem> extends DefaultClusterRenderer<Room> {
        private final IconGenerator mClusterIconGenerator = new IconGenerator(parentActivity);
        public CustomMapRenderer(Context context, GoogleMap map, ClusterManager<Room> clusterManager) {
            super(context, map, clusterManager);
        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster<Room> cluster) {
            //start clustering if at least 2 items overlap
            return cluster.getSize() > 1;
        }

        //for single marker item
        @Override
        protected void onBeforeClusterItemRendered(final Room item, final MarkerOptions markerOptions) {
            super.onBeforeClusterItemRendered(item, markerOptions);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //if want to use own icon from drawable then use this
                    markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker_low));
                    markerOptions.zIndex(1.0f);
                }
            });
        }

        //for clustered item marker
        //Refer this https://codedump.io/share/JTvSCafSuuf7/1/android-maps-utils-cluster-icon-color
        //and this http://stackoverflow.com/questions/30967961/android-maps-utils-cluster-icon-color
        //for changing icons and colours of clustered markers and single markers with even more control
        @Override
        protected void onBeforeClusterRendered(final Cluster<Room> cluster, final MarkerOptions markerOptions){

            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Drawable clusterIcon;

                    if(cluster.getSize() <= 15){
                        clusterIcon = ContextCompat.getDrawable(parentActivity, R.drawable.ic_map_marker_low);
                    }
                    else if(cluster.getSize() > 15){
                            clusterIcon = ContextCompat.getDrawable(parentActivity, R.drawable.ic_map_marker_high);
                    }
                    else{
                            clusterIcon = ContextCompat.getDrawable(parentActivity, R.drawable.ic_map_marker_low);
                    }

                    mClusterIconGenerator.setTextAppearance(R.style.clusterIconText);
                    mClusterIconGenerator.setBackground(clusterIcon);

                    Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
                    markerOptions.zIndex(1.0f);
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.search_results));
                parentActivity.selectHotel();
            }
        });
    }

    //needs this to prevent duplicate id of map and autocomplete that is making the app crash
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        MapFragment f = (MapFragment) parentActivity.getFragmentManager()
                .findFragmentById(R.id.hostRoomResultMap);
        if (f != null)
            parentActivity.getFragmentManager().beginTransaction().remove(f).commit();
    }
}
