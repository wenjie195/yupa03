package com.vidatechft.yupa.MainSearchFragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.HostStayMyBookingRecyclerGridAdapter;
import com.vidatechft.yupa.adapter.HostStayRecyclerGridAdapter;
import com.vidatechft.yupa.classes.Accommodation;
import com.vidatechft.yupa.classes.BookHomestayDetails;
import com.vidatechft.yupa.classes.BookingHistory;
import com.vidatechft.yupa.classes.Favourite;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.classes.Setting;
import com.vidatechft.yupa.hostFragments.HostViewAllFragment;
import com.vidatechft.yupa.utilities.Config;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.Collections;

public class SearchHostRoomFragment extends Fragment implements View.OnClickListener{
    public static final String TAG = SearchHostRoomFragment.class.getName();
    private MainActivity parentActivity;

    private TextView nothingTV;
    private String loadingTxt, emptyTxt;

    //my booking history
    private RecyclerView myBookingRV;
    private LinearLayout myBookingLL;
    private ArrayList<BookingHistory> myBookingHistoryMainList = new ArrayList<>();
    private ArrayList<BookingHistory> myBookingHistoryNotPendingList = new ArrayList<>();
    private ArrayList<BookingHistory> myBookingHistoryPendingList = new ArrayList<>();
    private HostStayMyBookingRecyclerGridAdapter myBookingAdapter;
    private ListenerRegistration myBookingListener,myBookingPendingListener;
    private int totalBHCount = 0;
    private int bHCount = 0;
    private int totalPendingBHCount = 0;
    private int pendingpBHCount = 0;

    //promotion
    private RecyclerView promoRV;
    private LinearLayout promoLL;
    private ArrayList<Room> fullPromoList = new ArrayList<>();
    private ArrayList<Room> promoList = new ArrayList<>();
    private ArrayList<String> fullPromoPushId = new ArrayList<>();
    private ArrayList<String> promoPushId = new ArrayList<>();
    private HostStayRecyclerGridAdapter promoAdapter;
    private ListenerRegistration promoListener;
    private Query promoRef;

    //stays for you
    private RecyclerView hostRV;
    private LinearLayout hostLL;
    private ArrayList<Room> fullHostList = new ArrayList<>();
    private ArrayList<Room> hostList = new ArrayList<>();
    private ArrayList<String> fullHostPushId = new ArrayList<>();
    private ArrayList<String> hostPushId = new ArrayList<>();
    private HostStayRecyclerGridAdapter hostAccAdapter;
    private ListenerRegistration hostAccListener;
    private Query hostRef;

    //recommended by travellers
    private RecyclerView recomRV;
    private LinearLayout recomLL;
    private ArrayList<Room> fullRecomList = new ArrayList<>();
    private ArrayList<Room> recomList = new ArrayList<>();
    private ArrayList<String> fullRecomPushId = new ArrayList<>();
    private ArrayList<String> recomPushId = new ArrayList<>();
    private HostStayRecyclerGridAdapter recomAccAdapter;
    private ListenerRegistration recomAccListener;
    private Query recomRef;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static SearchHostRoomFragment newInstance(MainActivity parentActivity) {
        SearchHostRoomFragment fragment = new SearchHostRoomFragment();

        fragment.parentActivity = parentActivity;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_main_host_room_search, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.showBtmNav();

                loadingTxt = parentActivity.getString(R.string.loading_result);
                emptyTxt = parentActivity.getString(R.string.no_result_found);

                nothingTV = view.findViewById(R.id.nothingTV);

                //My Booking
                myBookingRV = view.findViewById(R.id.myBookingRV);
                myBookingLL = view.findViewById(R.id.myBookingLL);

                //Promotion
                promoRV = view.findViewById(R.id.promoRV);
                promoLL = view.findViewById(R.id.promoLL);
                view.findViewById(R.id.promoViewAllTV).setOnClickListener(new DebouncedOnClickListener(500) {
                    @Override
                    public void onDebouncedClick(View v) {
                        Fragment hostViewAllFragment = HostViewAllFragment.newInstance(parentActivity,fullPromoList, fullPromoPushId,promoRef);
                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, hostViewAllFragment).addToBackStack(HostViewAllFragment.TAG).commit();
                    }
                });

                //Accomodation
                hostRV = view.findViewById(R.id.hostRV);
                hostLL = view.findViewById(R.id.hostLL);
                view.findViewById(R.id.hostViewAllTV).setOnClickListener(new DebouncedOnClickListener(500) {
                    @Override
                    public void onDebouncedClick(View v) {
                        Fragment hostViewAllFragment = HostViewAllFragment.newInstance(parentActivity,fullHostList, fullHostPushId,hostRef);
                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, hostViewAllFragment).addToBackStack(HostViewAllFragment.TAG).commit();
                    }
                });

                //Recommended by travellers
                recomRV = view.findViewById(R.id.recomRV);
                recomLL = view.findViewById(R.id.recomLL);
                view.findViewById(R.id.recomViewAllTV).setOnClickListener(new DebouncedOnClickListener(500) {
                    @Override
                    public void onDebouncedClick(View v) {
                        Fragment hostViewAllFragment = HostViewAllFragment.newInstance(parentActivity,fullRecomList, fullRecomPushId,recomRef);
                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, hostViewAllFragment).addToBackStack(HostViewAllFragment.TAG).commit();
                    }
                });
            }
        });

        setupSearchFeature(view);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                myBookingAdapter = new HostStayMyBookingRecyclerGridAdapter(parentActivity, myBookingHistoryMainList);
                myBookingRV.setNestedScrollingEnabled(false);
                myBookingRV.setAdapter(myBookingAdapter);
                myBookingRV.setLayoutManager(new GridLayoutManager(parentActivity, 2));
            }
        });

        if(!GeneralFunction.isGuest()){
            if((myBookingHistoryPendingList != null && !myBookingHistoryPendingList.isEmpty()) || (myBookingHistoryNotPendingList != null && !myBookingHistoryNotPendingList.isEmpty())){
                updateMyBookingAdapter();
            }else{
                getMyBookingPending();
                getMyBooking();
            }
        }

        if(promoList != null && !promoList.isEmpty()){
            setPromoAdapter();
        }else{
            getPromo();
        }

        if(hostList != null && !hostList.isEmpty()){
            setHostAdapter();
        }else{
            getStay();
        }

        if(recomList != null && !recomList.isEmpty()){
            setRecomAdapter();
        }else{
            getRecom();
        }


        return view;
    }

    private void setupSearchFeature(View view){
        final ImageView searchIV = view.findViewById(R.id.searchIV);
        TextView searchTV = view.findViewById(R.id.searchTV);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                GeneralFunction.GlideImageSettingOwnDrawable(parentActivity,R.drawable.bg_homestay,searchIV);
                searchIV.requestLayout();
            }
        });

        searchTV.setOnClickListener(new DebouncedOnClickListener(1000) {
            @Override
            public void onDebouncedClick(View v) {
                Fragment searchMapFragment = SearchMapFragment.newInstance(parentActivity,null,Config.SEARCH_TYPE_HOST_ROOM);
                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, searchMapFragment).addToBackStack(SearchMapFragment.TAG).commit();
            }
        });
    }

    /*****************************************************MY BOOKING******************************************************************************/
    private void getMyBookingPending(){
        myBookingPendingListener = GeneralFunction.getFirestoreInstance()
                .collection(BookHomestayDetails.URL_BOOK_GUEST)
                .whereEqualTo(BookingHistory.USER_ID,parentActivity.uid)
                .whereGreaterThanOrEqualTo(BookingHistory.CHECKIN_DATE,GeneralFunction.getDefaultUtcCalendar().getTimeInMillis())
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot snapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                        if(GeneralFunction.hasError(parentActivity,e,TAG)){
                            return;
                        }

                        myBookingHistoryPendingList.clear();

                        if(snapshots != null && !snapshots.isEmpty()){
                            totalPendingBHCount = snapshots.size();
                            for(DocumentSnapshot snapshot : snapshots){
                                final BookHomestayDetails bookHomestayDetails = snapshot.toObject(BookHomestayDetails.class);

                                if(GeneralFunction.isBookingHomestayDetailsValid(bookHomestayDetails)){
                                    bookHomestayDetails.id = snapshot.getId();
                                    GeneralFunction.getFirestoreInstance()
                                            .collection(Accommodation.URL_ACCOMMODATION)
                                            .document(bookHomestayDetails.targetId)
                                            .get()
                                            .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                                @Override
                                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                    pendingpBHCount++;
                                                    if(GeneralFunction.hasErrorDoc(parentActivity,task,TAG)){
                                                        handlePendingBhDownloads();
                                                        return;
                                                    }

                                                    Room room = task.getResult().toObject(Room.class);

                                                    if(room != null){
                                                        room.id = task.getResult().getId();
                                                        BookingHistory tempBookingHistory = new BookingHistory();
                                                        tempBookingHistory.id = bookHomestayDetails.id;
                                                        tempBookingHistory.actionType = BookingHistory.ACTION_TYPE_PENDING;
                                                        tempBookingHistory.dateUpdated = bookHomestayDetails.dateUpdated;
                                                        tempBookingHistory.room = room;
                                                        myBookingHistoryPendingList.add(tempBookingHistory);
                                                    }

                                                    handlePendingBhDownloads();
                                                }
                                            });
                                }
                            }
                        }else{
                            parentActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    handlePendingBhDownloads();
                                    if(isAllListEmpty()){
                                        nothingTV.setText(emptyTxt);
                                        nothingTV.setVisibility(View.VISIBLE);
                                    }
                                }
                            });
                        }
                    }
                });
    }

    private void getMyBooking(){

        myBookingListener = GeneralFunction.getFirestoreInstance()
                .collection(BookingHistory.URL_BOOKING_HISTORY)
                .whereEqualTo(BookingHistory.TYPE,BookingHistory.BOOK_TYPE_HOMESTAY)
                .whereEqualTo(BookingHistory.USER_ID,parentActivity.uid)
                .whereGreaterThanOrEqualTo(BookingHistory.CHECKIN_DATE,GeneralFunction.getDefaultUtcCalendarFifteenDaysAgo().getTimeInMillis())
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot snapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                        if(GeneralFunction.hasError(parentActivity,e,TAG)){
                            return;
                        }

                        myBookingHistoryNotPendingList.clear();

                        if(snapshots != null && !snapshots.isEmpty()){
                            totalBHCount = snapshots.size();
                            for(DocumentSnapshot snapshot : snapshots){
                                final BookingHistory bookingHistory = snapshot.toObject(BookingHistory.class);
                                if(bookingHistory != null){
                                    bookingHistory.id = snapshot.getId();
                                }

                                if(GeneralFunction.isBookingHistoryValid(bookingHistory)){
                                    bookingHistory.id = snapshot.getId();
                                    switch (bookingHistory.type){
                                        case BookingHistory.BOOK_TYPE_HOMESTAY:
                                            GeneralFunction.getFirestoreInstance()
                                                    .collection(Accommodation.URL_ACCOMMODATION)
                                                    .document(bookingHistory.targetId)
                                                    .get()
                                                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                            bHCount++;
//                                                            if(GeneralFunction.hasErrorDoc(parentActivity,task,TAG)){
                                                            if(!task.isSuccessful()){
                                                                handleBhDownloads();
                                                                return;
                                                            }

                                                            Room room = task.getResult().toObject(Room.class);

                                                            if(room != null && (TextUtils.equals(bookingHistory.actionType,BookingHistory.ACTION_TYPE_ACCEPTED) || TextUtils.equals(bookingHistory.actionType,BookingHistory.ACTION_TYPE_REJECTED))){
                                                                room.id = task.getResult().getId();
                                                                bookingHistory.room = room;
                                                                myBookingHistoryNotPendingList.add(bookingHistory);
                                                            }

                                                            handleBhDownloads();
                                                        }
                                                    });
                                            break;
                                    }
                                }
                            }
                        }else{
                            parentActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    handleBhDownloads();
                                    if(isAllListEmpty()){
                                        nothingTV.setText(emptyTxt);
                                        nothingTV.setVisibility(View.VISIBLE);
                                    }
                                }
                            });
                        }
                    }
                });
    }

    private void handleBhDownloads(){
        if(totalBHCount == bHCount){
            bHCount = 0;
            totalBHCount = 0;
            updateMyBookingAdapter();
        }
    }

    private void handlePendingBhDownloads(){
        if(pendingpBHCount == totalPendingBHCount){
            totalPendingBHCount = 0;
            pendingpBHCount = 0;
            updateMyBookingAdapter();
        }
    }

    private void updateMyBookingAdapter(){
        myBookingHistoryMainList.clear();
        myBookingHistoryMainList.addAll(myBookingHistoryPendingList);
        myBookingHistoryMainList.addAll(myBookingHistoryNotPendingList);

        Collections.sort(myBookingHistoryMainList);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                myBookingAdapter.notifyDataSetChanged();

                if(myBookingHistoryMainList.isEmpty()){
                    myBookingLL.setVisibility(View.GONE);
                }else{
                    myBookingLL.setVisibility(View.VISIBLE);

                    nothingTV.setVisibility(View.GONE);
                }

                if(isAllListEmpty()){
                    nothingTV.setText(emptyTxt);
                    nothingTV.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    /*****************************************************PROMOTION******************************************************************************/
    private void getPromo(){
        promoRef = GeneralFunction.getFirestoreInstance()
                .collection(Accommodation.URL_ACCOMMODATION)
                .whereEqualTo(Room.STATUS, Room.STATUS_APPROVED)
                .whereEqualTo(Room.IS_PUBLISH, true)
                .whereEqualTo(Room.IS_DRAFT, false);

        promoListener = promoRef
                .orderBy(Room.DATE_CREATED, Query.Direction.DESCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot snapshots, @javax.annotation.Nullable final FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w(TAG, "Listen failed.", e);
                            return;
                        }

                        fullPromoList.clear();
                        fullPromoPushId.clear();

                        if (snapshots != null && !snapshots.isEmpty()) {
                            for (QueryDocumentSnapshot snapshot : snapshots) {
                                Room tempPromo = Room.snapshotToRoom(parentActivity, snapshot);
                                fullPromoList.add(tempPromo);
                                fullPromoPushId.add(snapshot.getId());
                            }

                            if(promoAdapter != null && !snapshots.getMetadata().isFromCache() && promoListener != null){
                                promoListener.remove();
                            }
                        } else {
                            fullPromoList.clear();
                            fullPromoPushId.clear();
                        }

                        setPromoAdapter();
                    }
                });
    }

    private void setPromoAdapter(){
        promoList.clear();
        promoPushId.clear();
        int count = 0;
        for(Room tempRoom : fullPromoList){
            if(count >= 2){
                break;
            }else{
                promoList.add(tempRoom);
            }
            count++;
        }
        count = 0;

        for(String pushedId : fullPromoPushId){
            if(count >= 2){
                break;
            }else{
                promoPushId.add(pushedId);
            }
            count++;
        }

        promoAdapter = new HostStayRecyclerGridAdapter(parentActivity,null, promoList, promoPushId,false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                promoRV.setNestedScrollingEnabled(false);
                promoRV.setAdapter(promoAdapter);
                promoRV.setLayoutManager(new GridLayoutManager(parentActivity, 2));

                if(promoList.isEmpty()){
                    promoLL.setVisibility(View.GONE);
                }else{
                    promoLL.setVisibility(View.VISIBLE);

                    nothingTV.setVisibility(View.GONE);
                }

                if(isAllListEmpty()){
                    nothingTV.setText(emptyTxt);
                    nothingTV.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    /*****************************************************ACCOMODATION******************************************************************************/
    private void getStay(){
        hostRef = GeneralFunction.getFirestoreInstance()
                .collection(Accommodation.URL_ACCOMMODATION)
                .whereEqualTo(Room.STATUS, Room.STATUS_APPROVED)
                .whereEqualTo(Room.IS_PUBLISH, true)
                .whereEqualTo(Room.IS_DRAFT, false);

        hostAccListener = hostRef
                .orderBy(Room.DATE_UPDATED, Query.Direction.DESCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot snapshots, @javax.annotation.Nullable final FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w(TAG, "Listen failed.", e);
                            return;
                        }

                        fullHostList.clear();
                        fullHostPushId.clear();

                        if (snapshots != null && !snapshots.isEmpty()) {
                            for (QueryDocumentSnapshot snapshot : snapshots) {
                                Room tempHost = Room.snapshotToRoom(parentActivity, snapshot);
                                fullHostList.add(tempHost);
                                fullHostPushId.add(snapshot.getId());
                            }

                            if(hostAccAdapter != null && !snapshots.getMetadata().isFromCache() && hostAccListener != null){
                                hostAccListener.remove();
                            }
                        } else {
                            fullHostList.clear();
                            fullHostPushId.clear();
                        }

                        setHostAdapter();
                    }
                });
    }

    private void setHostAdapter(){
        hostList.clear();
        hostPushId.clear();
        int count = 0;
        for(Room tempRoom : fullHostList){
            if(count >= 2){
                break;
            }else{
                hostList.add(tempRoom);
            }
            count++;
        }

        count = 0;
        for(String pushedId : fullHostPushId){
            if(count >= 2){
                break;
            }else{
                hostPushId.add(pushedId);
            }
            count++;
        }

        hostAccAdapter = new HostStayRecyclerGridAdapter(parentActivity,null, hostList, hostPushId,false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hostRV.setNestedScrollingEnabled(false);
                hostRV.setAdapter(hostAccAdapter);
                hostRV.setLayoutManager(new GridLayoutManager(parentActivity, 2));

                if(hostList.isEmpty()){
                    hostLL.setVisibility(View.GONE);
                }else{
                    hostLL.setVisibility(View.VISIBLE);

                    nothingTV.setVisibility(View.GONE);
                }

                if(isAllListEmpty()){
                    nothingTV.setText(emptyTxt);
                    nothingTV.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    /*****************************************************RECOMMENDED BY FRIENDS******************************************************************************/
    private void getRecom(){
        recomRef = GeneralFunction.getFirestoreInstance()
                .collection(Accommodation.URL_ACCOMMODATION)
                .whereEqualTo(Room.STATUS, Room.STATUS_APPROVED)
                .whereEqualTo(Room.IS_PUBLISH, true)
                .whereEqualTo(Room.IS_DRAFT, false);

        recomAccListener = recomRef
                .orderBy(Favourite.LIKE_COUNT, Query.Direction.DESCENDING)
                .orderBy(Room.DATE_UPDATED, Query.Direction.DESCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot snapshots, @javax.annotation.Nullable final FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w(TAG, "Listen failed.", e);
                            return;
                        }

                        fullRecomList.clear();
                        fullRecomPushId.clear();

                        if (snapshots != null && !snapshots.isEmpty()) {
                            for (QueryDocumentSnapshot snapshot : snapshots) {
                                Room tempRecom = Room.snapshotToRoom(parentActivity, snapshot);
                                fullRecomList.add(tempRecom);
                                fullRecomPushId.add(snapshot.getId());
                            }

                            if(recomAccAdapter != null && !snapshots.getMetadata().isFromCache() && recomAccListener != null){
                                recomAccListener.remove();
                            }
                        } else {
                            fullRecomList.clear();
                            fullRecomPushId.clear();
                        }

                        setRecomAdapter();
                    }
                });
    }

    private void setRecomAdapter(){
        recomList.clear();
        recomPushId.clear();
        int count = 0;
        for(Room tempRoom : fullRecomList){
            if(count >= 2){
                break;
            }else{
                recomList.add(tempRoom);
            }
            count++;
        }
        count = 0;

        for(String pushedId : fullRecomPushId){
            if(count >= 2){
                break;
            }else{
                recomPushId.add(pushedId);
            }
            count++;
        }

        recomAccAdapter = new HostStayRecyclerGridAdapter(parentActivity,null, recomList, recomPushId,false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                recomRV.setNestedScrollingEnabled(false);
                recomRV.setAdapter(recomAccAdapter);
                recomRV.setLayoutManager(new GridLayoutManager(parentActivity, 2));

                if(recomList.isEmpty()){
                    recomLL.setVisibility(View.GONE);
                }else{
                    recomLL.setVisibility(View.VISIBLE);

                    nothingTV.setVisibility(View.GONE);
                }

                if(isAllListEmpty()){
                    nothingTV.setText(emptyTxt);
                    nothingTV.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private boolean isAllListEmpty(){
        return ((myBookingHistoryNotPendingList == null || myBookingHistoryNotPendingList.isEmpty()) || myBookingHistoryPendingList == null || myBookingHistoryPendingList.isEmpty()) && (promoList == null || promoList.isEmpty()) && (hostList == null || hostList.isEmpty()) && (recomList == null || recomList.isEmpty());
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.landing_page_stays_for_you));
                parentActivity.selectHotel();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if(myBookingListener != null){
            myBookingListener.remove();
        }

        if(myBookingPendingListener != null){
            myBookingPendingListener.remove();
        }

        if(promoListener != null){
            promoListener.remove();
        }

        if(hostAccListener != null){
            hostAccListener.remove();
        }

        if(recomAccListener != null){
            recomAccListener.remove();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.backIB:
                parentActivity.onBackPressed();
                break;
        }
    }
}
