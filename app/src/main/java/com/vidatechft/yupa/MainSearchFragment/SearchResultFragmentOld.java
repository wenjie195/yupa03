package com.vidatechft.yupa.MainSearchFragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.HostStayRecyclerGridAdapter;
import com.vidatechft.yupa.adapter.ItineraryRecyclerGridAdapter;
import com.vidatechft.yupa.adapter.JobRecyclerGridAdapter;
import com.vidatechft.yupa.classes.Accommodation;
import com.vidatechft.yupa.classes.Host;
import com.vidatechft.yupa.classes.Itinerary;
import com.vidatechft.yupa.classes.Job;
import com.vidatechft.yupa.classes.Merchant;
import com.vidatechft.yupa.classes.PlaceDetailsGoogleApi.PlaceAddress;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.classes.Search;
import com.vidatechft.yupa.classes.Setting;
import com.vidatechft.yupa.employerFragments.JobViewAllFragment;
import com.vidatechft.yupa.hostFragments.HostViewAllFragment;
import com.vidatechft.yupa.itinerary.ItineraryViewAllFragment;
import com.vidatechft.yupa.merchantShopFragment.ApplyAsMerchantFragment;
import com.vidatechft.yupa.retrofitNetwork.UnifyPlaceAddress;
import com.vidatechft.yupa.utilities.Config;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
public class SearchResultFragmentOld extends Fragment implements View.OnClickListener{
    public static final String TAG = SearchResultFragmentOld.class.getName();
    private MainActivity parentActivity;
    private Search search;

    private TextView nothingTV;
    private String loadingTxt, emptyTxt;

    //itinerary
    private RecyclerView itiRV;
    private ConstraintLayout itiCL;
    private ArrayList<Itinerary> itiList = new ArrayList<>();
    private ArrayList<Itinerary> fullItiList = new ArrayList<>();
    private ItineraryRecyclerGridAdapter itiAdapter;
    private ListenerRegistration itiListener;
    private Query itiRef;

    //accomodation
    private RecyclerView hostRV;
    private ConstraintLayout hostCL;
    private ArrayList<Room> fullHostList = new ArrayList<>();
    private ArrayList<Room> hostList = new ArrayList<>();
    private HostStayRecyclerGridAdapter hostAccAdapter;
    private ListenerRegistration hostAccListener;
    private Query hostRef;

    //job
    private RecyclerView jobRV;
    private ConstraintLayout jobCL;
    private ArrayList<Job> jobList = new ArrayList<>();
    private ArrayList<Job> fullJobList = new ArrayList<>();
    private JobRecyclerGridAdapter jobAdapter;
    private ListenerRegistration jobListener;
    private Query jobRef;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static SearchResultFragmentOld newInstance(MainActivity parentActivity, Search search) {
        SearchResultFragmentOld fragment = new SearchResultFragmentOld();

        fragment.parentActivity = parentActivity;
        fragment.search = search;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_main_search_result_old, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.hideBtmNav();

                loadingTxt = parentActivity.getString(R.string.loading_result);
                emptyTxt = parentActivity.getString(R.string.no_result_found);

                nothingTV = view.findViewById(R.id.nothingTV);

                //Iti
                itiRV = view.findViewById(R.id.itiRV);
                itiCL = view.findViewById(R.id.itiCL);
                view.findViewById(R.id.itiViewAllTV).setOnClickListener(new DebouncedOnClickListener(500) {
                    @Override
                    public void onDebouncedClick(View v) {
                        Fragment itineraryViewAllFragment = ItineraryViewAllFragment.newInstance(parentActivity,fullItiList,itiRef);
                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, itineraryViewAllFragment).addToBackStack(ItineraryViewAllFragment.TAG).commit();
                    }
                });

                //Accomodation
                hostRV = view.findViewById(R.id.hostRV);
                hostCL = view.findViewById(R.id.hostCL);
                view.findViewById(R.id.hostViewAllTV).setOnClickListener(new DebouncedOnClickListener(500) {
                    @Override
                    public void onDebouncedClick(View v) {
                        ArrayList<String> fullDocumentPushId = new ArrayList<>();
                        for(Room tempRoom : fullHostList){
                            fullDocumentPushId.add(tempRoom.id);
                        }
                        Fragment hostViewAllFragment = HostViewAllFragment.newInstance(parentActivity,fullHostList,fullDocumentPushId,hostRef);
                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, hostViewAllFragment).addToBackStack(HostViewAllFragment.TAG).commit();
                    }
                });

                //Job
                jobRV = view.findViewById(R.id.jobRV);
                jobCL = view.findViewById(R.id.jobCL);
                view.findViewById(R.id.jobViewAllTV).setOnClickListener(new DebouncedOnClickListener(500) {
                    @Override
                    public void onDebouncedClick(View v) {
                        Fragment jobViewAllFragment = JobViewAllFragment.newInstance(parentActivity,fullJobList,jobRef);
                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, jobViewAllFragment).addToBackStack(JobViewAllFragment.TAG).commit();
                    }
                });
            }
        });

        if(search != null && search.placeId != null && search.placeAddress == null){
            getPlaceDetails();
        }else{
            getResults();
        }

        return view;
    }

    /*****************************************************GET UNIFIED PLACE ADDRESS******************************************************************************/
    private void getPlaceDetails(){
        parentActivity.initializeLoadingDialog(parentActivity.getString(R.string.place_details_donwloading),true);
        parentActivity.controlLoadingDialog(true,parentActivity);

        new UnifyPlaceAddress(parentActivity, search.placeId, new UnifyPlaceAddress.OnUnifyComplete() {
            @Override
            public void onUnifySuccess(PlaceAddress placeAddress) {
                parentActivity.controlLoadingDialog(false,parentActivity);
                search.placeAddress = placeAddress;
                getResults();
            }

            @Override
            public void onUnifyFailed(Exception e) {
                parentActivity.controlLoadingDialog(false,parentActivity);
                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_occured_getting_place_details),R.drawable.notice_bad,TAG);
                e.printStackTrace();
                Crashlytics.logException(e);
                nothingTV.setText(emptyTxt);
                nothingTV.setVisibility(View.VISIBLE);
            }
        });
    }

    private void getResults(){
        if(itiList != null && !itiList.isEmpty()){
            setItiAdapter();
        }else{
            getItinerary();
        }

        if(jobList != null && !jobList.isEmpty()){
            setJobAdapter();
        }else{
            getJob();
        }

        if(hostList != null && !hostList.isEmpty()){
            setHostAdapter();
        }else{
            getStay();
        }
    }

    /*****************************************************ITINERARY******************************************************************************/
    private void getItinerary(){
        itiRef = GeneralFunction.getFirestoreInstance()
                .collection(Itinerary.URL_ITINERARY)
                .whereEqualTo(Itinerary.IS_PUBLIC,true)
                .whereEqualTo(Itinerary.IS_PUBLISH,true);

        if(search != null){
//            if(search.boundingBox != null && search.boundingBox.get(Config.BOUNDING_BOX_BTM_LEFT) != null && search.boundingBox.get(Config.BOUNDING_BOX_TOP_RIGHT) != null){
//                itiRef = itiRef.whereGreaterThan(Itinerary.GPS,new GeoPoint(search.boundingBox.get(Config.BOUNDING_BOX_BTM_LEFT).latitude,search.boundingBox.get(Config.BOUNDING_BOX_BTM_LEFT).longitude))
//                        .whereLessThan(Itinerary.GPS,new GeoPoint(search.boundingBox.get(Config.BOUNDING_BOX_TOP_RIGHT).latitude,search.boundingBox.get(Config.BOUNDING_BOX_TOP_RIGHT).longitude));
//            }
            itiRef = PlaceAddress.getQueriedPlaceAddress(itiRef,search.placeAddress,null);
        }

        itiListener = itiRef
//                .orderBy(Itinerary.GPS)
//                .orderBy(Itinerary.LIKE_COUNT, Query.Direction.DESCENDING)
//                .orderBy(Itinerary.DATE_UPDATED, Query.Direction.DESCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot snapshots, @javax.annotation.Nullable final FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w(TAG, "Listen failed.", e);
                            if(isAllListEmpty()){
                                nothingTV.setText(emptyTxt);
                                nothingTV.setVisibility(View.VISIBLE);
                            }
                            return;
                        }

                        fullItiList.clear();

                        if (snapshots != null && !snapshots.isEmpty()) {
                            for (QueryDocumentSnapshot snapshot : snapshots) {
                                boolean shouldAddIti = true;

                                Itinerary tempIti = new Itinerary(parentActivity, snapshot);
                                if(search.checkin_date > 0 && search.checkout_date > 0){
                                    if( ((search.checkin_date >= tempIti.departDate && search.checkin_date <= tempIti.returnDate)
                                            ||
                                        (search.checkout_date <= tempIti.returnDate && search.checkout_date >= tempIti.departDate))
                                            ||
                                        ((tempIti.departDate >= search.checkin_date && tempIti.departDate <= search.checkout_date)
                                            ||
                                        (tempIti.returnDate <= search.checkout_date && tempIti.returnDate >= search.checkin_date)) ){

                                    }else{
                                        shouldAddIti = false;
                                    }
                                }

                                if(shouldAddIti && search.activityList != null && !search.activityList.isEmpty()){
                                    boolean shouldAddActivity = false;
                                    for(String activity : search.activityList){
                                        if(tempIti.activityList.contains(activity)){
                                            shouldAddActivity = true;
                                            break;
                                        }
                                    }

                                    if(!shouldAddActivity){
                                        shouldAddIti = false;
                                    }
                                }

                                if(shouldAddIti){
                                    fullItiList.add(tempIti);
                                }

                                //this 1 is old filter (checks whether the searched month is within the itinerary month, if is in range then add into list)
//                                if(search.date > 0){
//                                    Calendar cal1 = Calendar.getInstance();
//                                    Calendar cal2 = Calendar.getInstance();
//                                    Date date1 = new Date();
//                                    date1.setTime(tempIti.departDate);
//                                    Date date2 = new Date();
//                                    date1.setTime(search.date);
//                                    cal1.setTime(date1);
//                                    cal2.setTime(date2);
//                                    if(cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH)){
//                                        fullItiList.add(tempIti);
//                                    }
//                                }else{
//                                    fullItiList.add(tempIti);
//                                }
                            }

                            if(itiAdapter != null && !snapshots.getMetadata().isFromCache() && itiListener != null){
                                itiListener.remove();
                            }
                        } else {
                            fullItiList.clear();
                        }

                        Collections.sort(fullItiList);
                        setItiAdapter();
                    }
                });
    }

    private void setItiAdapter(){
        itiList.clear();
        int count = 0;
        for(Itinerary tempIti : fullItiList){
            if(count >= 2){
                break;
            }else{
                itiList.add(tempIti);
            }
            count++;
        }

        itiAdapter = new ItineraryRecyclerGridAdapter(parentActivity,SearchResultFragmentOld.this, itiList,false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                itiRV.setNestedScrollingEnabled(false);
                itiRV.setAdapter(itiAdapter);
                itiRV.setLayoutManager(new GridLayoutManager(parentActivity, 2));

                if(itiList.isEmpty()){
                    itiCL.setVisibility(View.GONE);
                }else{
                    itiCL.setVisibility(View.VISIBLE);

                    nothingTV.setVisibility(View.GONE);
                }

                if(isAllListEmpty()){
                    nothingTV.setText(emptyTxt);
                    nothingTV.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    /*****************************************************ACCOMODATION******************************************************************************/
    private void getStay(){
        hostRef = GeneralFunction.getFirestoreInstance()
                .collection(Accommodation.URL_ACCOMMODATION)
                .whereEqualTo(Room.STATUS, Room.STATUS_APPROVED)
                .whereEqualTo(Room.IS_PUBLISH, true)
                .whereEqualTo(Room.IS_DRAFT, false);

        if(search != null){
//            if(search.boundingBox != null && search.boundingBox.get(Config.BOUNDING_BOX_BTM_LEFT) != null && search.boundingBox.get(Config.BOUNDING_BOX_TOP_RIGHT) != null){
//                hostRef = hostRef.whereGreaterThan(Room.ACCOMMODATION_GEOPOINT,new GeoPoint(search.boundingBox.get(Config.BOUNDING_BOX_BTM_LEFT).latitude,search.boundingBox.get(Config.BOUNDING_BOX_BTM_LEFT).longitude))
//                        .whereLessThan(Room.ACCOMMODATION_GEOPOINT,new GeoPoint(search.boundingBox.get(Config.BOUNDING_BOX_TOP_RIGHT).latitude,search.boundingBox.get(Config.BOUNDING_BOX_TOP_RIGHT).longitude));
//            }

            hostRef = PlaceAddress.getQueriedPlaceAddress(hostRef,search.placeAddress,null);
        }

        hostAccListener = hostRef
//                .orderBy(Room.ACCOMMODATION_GEOPOINT)
//                .orderBy(Room.DATE_UPDATED, Query.Direction.DESCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot snapshots, @javax.annotation.Nullable final FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w(TAG, "Listen failed.", e);
                            if(isAllListEmpty()){
                                nothingTV.setText(emptyTxt);
                                nothingTV.setVisibility(View.VISIBLE);
                            }
                            return;
                        }

                        fullHostList.clear();

                        if (snapshots != null && !snapshots.isEmpty()) {
                            for (QueryDocumentSnapshot snapshot : snapshots) {
                                Room tempHost = Room.snapshotToRoom(parentActivity, snapshot);
                                fullHostList.add(tempHost);
                            }

                            if(hostAccAdapter != null && !snapshots.getMetadata().isFromCache() && hostAccListener != null){
                                hostAccListener.remove();
                            }
                        } else {
                            fullHostList.clear();
                        }

                        Collections.sort(fullHostList);
                        setHostAdapter();
                    }
                });
    }

    private void setHostAdapter(){
        hostList.clear();
        int count = 0;

        ArrayList<String> documentPushId = new ArrayList<>();
        for(Room tempRoom : fullHostList){
            if(count >= 2){
                break;
            }else{
                hostList.add(tempRoom);
                documentPushId.add(tempRoom.id);
            }
            count++;
        }

        hostAccAdapter = new HostStayRecyclerGridAdapter(parentActivity,null, hostList,documentPushId,false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hostRV.setNestedScrollingEnabled(false);
                hostRV.setAdapter(hostAccAdapter);
                hostRV.setLayoutManager(new GridLayoutManager(parentActivity, 2));

                if(hostList.isEmpty()){
                    hostCL.setVisibility(View.GONE);
                }else{
                    hostCL.setVisibility(View.VISIBLE);

                    nothingTV.setVisibility(View.GONE);
                }

                if(isAllListEmpty()){
                    nothingTV.setText(emptyTxt);
                    nothingTV.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    /*****************************************************JOB******************************************************************************/
    private void getJob(){
        jobRef = GeneralFunction.getFirestoreInstance()
                .collection(Job.URL_JOB)
                .whereEqualTo(Job.IS_AVAILABLE,true);

        if(search != null){
//            if(search.boundingBox != null && search.boundingBox.get(Config.BOUNDING_BOX_BTM_LEFT) != null && search.boundingBox.get(Config.BOUNDING_BOX_TOP_RIGHT) != null){
//                jobRef = jobRef.whereGreaterThan(Job.GPS,new GeoPoint(search.boundingBox.get(Config.BOUNDING_BOX_BTM_LEFT).latitude,search.boundingBox.get(Config.BOUNDING_BOX_BTM_LEFT).longitude))
//                        .whereLessThan(Job.GPS,new GeoPoint(search.boundingBox.get(Config.BOUNDING_BOX_TOP_RIGHT).latitude,search.boundingBox.get(Config.BOUNDING_BOX_TOP_RIGHT).longitude));
//            }
            jobRef = PlaceAddress.getQueriedPlaceAddress(jobRef,search.placeAddress,null);
        }

        jobListener = jobRef
//                .orderBy(Job.GPS)
//                .orderBy(Job.DATE_UPDATED, Query.Direction.DESCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot snapshots, @javax.annotation.Nullable final FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w(TAG, "Listen failed.", e);
                            if(isAllListEmpty()){
                                nothingTV.setText(emptyTxt);
                                nothingTV.setVisibility(View.VISIBLE);
                            }
                            return;
                        }

                        fullJobList.clear();

                        if (snapshots != null && !snapshots.isEmpty()) {
                            for (QueryDocumentSnapshot snapshot : snapshots) {
                                Job tempJob = new Job(parentActivity, snapshot);
                                fullJobList.add(tempJob);
                            }

                            if(jobAdapter != null && !snapshots.getMetadata().isFromCache() && jobListener != null){
                                jobListener.remove();
                            }
                        } else {
                            fullJobList.clear();
                        }

                        Collections.sort(fullJobList);
                        setJobAdapter();
                    }
                });
    }

    private void setJobAdapter(){
        jobList.clear();
        int count = 0;
        for(Job tempJob : fullJobList){
            if(count >= 2){
                break;
            }else{
                jobList.add(tempJob);
            }
            count++;
        }

        jobAdapter = new JobRecyclerGridAdapter(parentActivity,SearchResultFragmentOld.this, jobList);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                jobRV.setNestedScrollingEnabled(false);
                jobRV.setAdapter(jobAdapter);
                jobRV.setLayoutManager(new GridLayoutManager(parentActivity, 2));

                if(jobList.isEmpty()){
                    jobCL.setVisibility(View.GONE);
                }else{
                    jobCL.setVisibility(View.VISIBLE);

                    nothingTV.setVisibility(View.GONE);
                }
                if(isAllListEmpty()){
                    nothingTV.setText(emptyTxt);
                    nothingTV.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    private boolean isAllListEmpty(){
        return (itiList == null || itiList.isEmpty()) && (hostList == null || hostList.isEmpty()) && (jobList == null || jobList.isEmpty());
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.search_results));
                parentActivity.selectHotel();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(itiListener != null){
            itiListener.remove();
        }

        if(hostAccListener != null){
            hostAccListener.remove();
        }

        if(jobListener != null){
            jobListener.remove();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.backIB:
                parentActivity.onBackPressed();
                break;
        }
    }
}
