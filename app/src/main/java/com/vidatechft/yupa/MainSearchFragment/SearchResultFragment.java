package com.vidatechft.yupa.MainSearchFragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.HostStayRecyclerGridAdapter;
import com.vidatechft.yupa.adapter.ItineraryRecyclerGridAdapter;
import com.vidatechft.yupa.adapter.JobRecyclerGridAdapter;
import com.vidatechft.yupa.classes.Accommodation;
import com.vidatechft.yupa.classes.Host;
import com.vidatechft.yupa.classes.Itinerary;
import com.vidatechft.yupa.classes.Job;
import com.vidatechft.yupa.classes.Merchant;
import com.vidatechft.yupa.classes.PlaceDetailsGoogleApi.PlaceAddress;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.classes.Search;
import com.vidatechft.yupa.classes.Setting;
import com.vidatechft.yupa.employerFragments.JobViewAllFragment;
import com.vidatechft.yupa.hostFragments.HostViewAllFragment;
import com.vidatechft.yupa.itinerary.ItineraryViewAllFragment;
import com.vidatechft.yupa.merchantShopFragment.ApplyAsMerchantFragment;
import com.vidatechft.yupa.retrofitNetwork.UnifyPlaceAddress;
import com.vidatechft.yupa.utilities.Config;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

//THIS SEARCH FEATURE CODE HAS BEEN UPDATED TO INCLUDE ADMINISTRATIVE AREA ONLY SEARCH, means if search balik pulau, bayan lepas place will also show
//i think it is better to provide a checkbox to let user select how strict it wants the search to be
//because currently it is searching for every place that has the administrative only
public class SearchResultFragment extends Fragment{
    public static final String TAG = SearchResultFragment.class.getName();
    private MainActivity parentActivity;
    private Search search;
    private int searchType;
    private boolean isStrictMode;

    private TextView nothingTV,labelTV,viewAllTV;
    private ConstraintLayout resultCL;
    private RecyclerView resultRV;

    private String loadingTxt, emptyTxt,errorTxt;
    private Query searchRef;
    private ListenerRegistration searchListener;

    //itinerary
    private ArrayList<Itinerary> itiList = new ArrayList<>();
    private ItineraryRecyclerGridAdapter itiAdapter;

    //accomodation
    private ArrayList<Room> hostList = new ArrayList<>();
    private HostStayRecyclerGridAdapter hostAccAdapter;

    //job
    private ArrayList<Job> jobList = new ArrayList<>();
    private JobRecyclerGridAdapter jobAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static SearchResultFragment newInstance(MainActivity parentActivity, Search search, int searchType, boolean isStrictMode) {
        SearchResultFragment fragment = new SearchResultFragment();

        fragment.parentActivity = parentActivity;
        fragment.search = search;
        fragment.searchType = searchType;
        fragment.isStrictMode = isStrictMode;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_main_search_result, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.hideBtmNav();

                nothingTV = view.findViewById(R.id.nothingTV);
                labelTV = view.findViewById(R.id.labelTV);
                viewAllTV = view.findViewById(R.id.viewAllTV);
                resultCL = view.findViewById(R.id.resultCL);
                resultRV = view.findViewById(R.id.resultRV);

                switch (searchType){
                    case Search.TYPE_ITINERARY:

                        break;
                    case Search.TYPE_HOMESTAY:

                        break;
                    default:

                        break;
                }

                //the old view all button which is gone now
//                view.findViewById(R.id.itiViewAllTV).setOnClickListener(new DebouncedOnClickListener(500) {
//                    @Override
//                    public void onDebouncedClick(View v) {
//                        Fragment itineraryViewAllFragment = ItineraryViewAllFragment.newInstance(parentActivity,fullItiList,itiRef);
//                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, itineraryViewAllFragment).addToBackStack(ItineraryViewAllFragment.TAG).commit();
//                    }
//                });

                //Accomodation
            }
        });

        if(search != null && search.placeId != null && search.placeAddress == null){
            getPlaceDetails();
        }else{
            getResults();
        }

        return view;
    }

    /*****************************************************GET UNIFIED PLACE ADDRESS******************************************************************************/
    private void getPlaceDetails(){
        parentActivity.initializeLoadingDialog(parentActivity.getString(R.string.place_details_donwloading),true);
        parentActivity.controlLoadingDialog(true,parentActivity);

        new UnifyPlaceAddress(parentActivity, search.placeId, new UnifyPlaceAddress.OnUnifyComplete() {
            @Override
            public void onUnifySuccess(PlaceAddress placeAddress) {
                parentActivity.controlLoadingDialog(false,parentActivity);
                search.placeAddress = placeAddress;
                getResults();
            }

            @Override
            public void onUnifyFailed(Exception e) {
                parentActivity.controlLoadingDialog(false,parentActivity);
                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_occured_getting_place_details),R.drawable.notice_bad,TAG);
                e.printStackTrace();
                Crashlytics.logException(e);
                nothingTV.setText(emptyTxt);
                nothingTV.setVisibility(View.VISIBLE);
            }
        });
    }

    private void getResults(){
        switch (searchType){
            case Search.TYPE_ITINERARY:
                loadingTxt = parentActivity.getString(R.string.itinerary_search_loading);
                emptyTxt = parentActivity.getString(R.string.itinerary_search_empty);
                errorTxt = parentActivity.getString(R.string.itinerary_search_err);

                labelTV.setText(parentActivity.getString(R.string.itinerary_action_toolbar));
                nothingTV.setText(loadingTxt);
                nothingTV.setVisibility(View.VISIBLE);

                if(itiList != null && !itiList.isEmpty()){
                    setItiAdapter();
                }else{
                    getItinerary();
                }
                break;
            case Search.TYPE_HOMESTAY:
                loadingTxt = parentActivity.getString(R.string.homestay_search_loading);
                emptyTxt = parentActivity.getString(R.string.homestay_search_empty);
                errorTxt = parentActivity.getString(R.string.homestay_search_err);

                labelTV.setText(parentActivity.getString(R.string.landing_page_homestay));
                nothingTV.setText(loadingTxt);
                nothingTV.setVisibility(View.VISIBLE);

                if(hostList != null && !hostList.isEmpty()){
                    setHostAdapter();
                }else{
                    getStay();
                }
                break;
            case Search.TYPE_JOB:
                loadingTxt = parentActivity.getString(R.string.job_search_loading);
                emptyTxt = parentActivity.getString(R.string.job_search_empty);
                errorTxt = parentActivity.getString(R.string.job_search_err);

                labelTV.setText(parentActivity.getString(R.string.landing_page_job));
                nothingTV.setText(loadingTxt);
                nothingTV.setVisibility(View.VISIBLE);

                if(jobList != null && !jobList.isEmpty()){
                    setJobAdapter();
                }else{
                    getJob();
                }
                break;
            default:
                loadingTxt = parentActivity.getString(R.string.error_occured);
                emptyTxt = parentActivity.getString(R.string.error_occured);
                errorTxt = parentActivity.getString(R.string.error_occured);
                nothingTV.setText(errorTxt);
                nothingTV.setVisibility(View.VISIBLE);
                break;
        }
    }

    /*****************************************************ITINERARY******************************************************************************/
    private void getItinerary(){
        searchRef = GeneralFunction.getFirestoreInstance()
                .collection(Itinerary.URL_ITINERARY)
                .whereEqualTo(Itinerary.IS_PUBLIC,true)
                .whereEqualTo(Itinerary.IS_PUBLISH,true);

        if(search != null){
//            if(search.boundingBox != null && search.boundingBox.get(Config.BOUNDING_BOX_BTM_LEFT) != null && search.boundingBox.get(Config.BOUNDING_BOX_TOP_RIGHT) != null){
//                itiRef = itiRef.whereGreaterThan(Itinerary.GPS,new GeoPoint(search.boundingBox.get(Config.BOUNDING_BOX_BTM_LEFT).latitude,search.boundingBox.get(Config.BOUNDING_BOX_BTM_LEFT).longitude))
//                        .whereLessThan(Itinerary.GPS,new GeoPoint(search.boundingBox.get(Config.BOUNDING_BOX_TOP_RIGHT).latitude,search.boundingBox.get(Config.BOUNDING_BOX_TOP_RIGHT).longitude));
//            }
//            searchRef = PlaceAddress.getQueriedPlaceAddress(searchRef,search.placeAddress,null);
            if(isStrictMode){
                searchRef = PlaceAddress.getQueriedPlaceAddressSublocality(searchRef,search.placeAddress,null);
            }else{
                searchRef = PlaceAddress.getQueriedPlaceAddressAdministrativeArea(searchRef,search.placeAddress,null);
            }
        }

        searchListener = searchRef
//                .orderBy(Itinerary.GPS)
//                .orderBy(Itinerary.LIKE_COUNT, Query.Direction.DESCENDING)
//                .orderBy(Itinerary.DATE_UPDATED, Query.Direction.DESCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot snapshots, @javax.annotation.Nullable final FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w(TAG, "Listen failed.", e);
                            resultCL.setVisibility(View.GONE);
                            nothingTV.setText(errorTxt);
                            nothingTV.setVisibility(View.VISIBLE);
                            return;
                        }

                        itiList.clear();

                        if (snapshots != null && !snapshots.isEmpty()) {
                            for (QueryDocumentSnapshot snapshot : snapshots) {
                                boolean shouldAddIti = true;

                                Itinerary tempIti = new Itinerary(parentActivity, snapshot);
                                if(search.checkin_date > 0 && search.checkout_date > 0){
                                    if( ((search.checkin_date >= tempIti.departDate && search.checkin_date <= tempIti.returnDate)
                                            ||
                                        (search.checkout_date <= tempIti.returnDate && search.checkout_date >= tempIti.departDate))
                                            ||
                                        ((tempIti.departDate >= search.checkin_date && tempIti.departDate <= search.checkout_date)
                                            ||
                                        (tempIti.returnDate <= search.checkout_date && tempIti.returnDate >= search.checkin_date)) ){

                                    }else{
                                        shouldAddIti = false;
                                    }
                                }

                                if(shouldAddIti && search.activityList != null && !search.activityList.isEmpty()){
                                    boolean shouldAddActivity = false;
                                    for(String activity : search.activityList){
                                        if(tempIti.activityList.contains(activity)){
                                            shouldAddActivity = true;
                                            break;
                                        }
                                    }

                                    if(!shouldAddActivity){
                                        shouldAddIti = false;
                                    }
                                }

                                if(shouldAddIti){
                                    itiList.add(tempIti);
                                }

                                //this 1 is old filter (checks whether the searched month is within the itinerary month, if is in range then add into list)
//                                if(search.date > 0){
//                                    Calendar cal1 = Calendar.getInstance();
//                                    Calendar cal2 = Calendar.getInstance();
//                                    Date date1 = new Date();
//                                    date1.setTime(tempIti.departDate);
//                                    Date date2 = new Date();
//                                    date1.setTime(search.date);
//                                    cal1.setTime(date1);
//                                    cal2.setTime(date2);
//                                    if(cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH)){
//                                        fullItiList.add(tempIti);
//                                    }
//                                }else{
//                                    fullItiList.add(tempIti);
//                                }
                            }

                            if(itiAdapter != null && !snapshots.getMetadata().isFromCache() && searchListener != null){
                                searchListener.remove();
                            }
                        } else {
                            itiList.clear();
                        }

                        Collections.sort(itiList);
                        setItiAdapter();
                    }
                });
    }

    private void setItiAdapter(){
        itiAdapter = new ItineraryRecyclerGridAdapter(parentActivity,SearchResultFragment.this, itiList,false, search);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                resultRV.setAdapter(itiAdapter);
                resultRV.setLayoutManager(new GridLayoutManager(parentActivity, 2));

                if(itiList.isEmpty()){
                    resultCL.setVisibility(View.GONE);
                    nothingTV.setText(emptyTxt);
                    nothingTV.setVisibility(View.VISIBLE);
                }else{
                    resultCL.setVisibility(View.VISIBLE);
                    nothingTV.setVisibility(View.GONE);
                }
            }
        });
    }

    /*****************************************************ACCOMODATION******************************************************************************/
    private void getStay(){
        searchRef = GeneralFunction.getFirestoreInstance()
                .collection(Accommodation.URL_ACCOMMODATION)
                .whereEqualTo(Room.STATUS, Room.STATUS_APPROVED)
                .whereEqualTo(Room.IS_PUBLISH, true)
                .whereEqualTo(Room.IS_DRAFT, false);

        if(search != null){
//            if(search.boundingBox != null && search.boundingBox.get(Config.BOUNDING_BOX_BTM_LEFT) != null && search.boundingBox.get(Config.BOUNDING_BOX_TOP_RIGHT) != null){
//                hostRef = hostRef.whereGreaterThan(Room.ACCOMMODATION_GEOPOINT,new GeoPoint(search.boundingBox.get(Config.BOUNDING_BOX_BTM_LEFT).latitude,search.boundingBox.get(Config.BOUNDING_BOX_BTM_LEFT).longitude))
//                        .whereLessThan(Room.ACCOMMODATION_GEOPOINT,new GeoPoint(search.boundingBox.get(Config.BOUNDING_BOX_TOP_RIGHT).latitude,search.boundingBox.get(Config.BOUNDING_BOX_TOP_RIGHT).longitude));
//            }

//            searchRef = PlaceAddress.getQueriedPlaceAddress(searchRef,search.placeAddress,null);
            if(isStrictMode){
                searchRef = PlaceAddress.getQueriedPlaceAddressSublocality(searchRef,search.placeAddress,null);
            }else{
                searchRef = PlaceAddress.getQueriedPlaceAddressAdministrativeArea(searchRef,search.placeAddress,null);
            }
        }

        searchListener = searchRef
//                .orderBy(Room.ACCOMMODATION_GEOPOINT)
//                .orderBy(Room.DATE_UPDATED, Query.Direction.DESCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot snapshots, @javax.annotation.Nullable final FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w(TAG, "Listen failed.", e);
                            resultCL.setVisibility(View.GONE);
                            nothingTV.setText(errorTxt);
                            nothingTV.setVisibility(View.VISIBLE);
                            return;
                        }

                        hostList.clear();

                        if (snapshots != null && !snapshots.isEmpty()) {
                            for (QueryDocumentSnapshot snapshot : snapshots) {
                                boolean shouldAddHost = true;

                                Room tempHost = Room.snapshotToRoom(parentActivity, snapshot);
                                if(tempHost != null){
                                    if(search.noofGuest > 0 && tempHost.guestQuantity < search.noofGuest){
                                        shouldAddHost = false;
                                    }
                                }else{
                                    shouldAddHost = false;
                                }

                                if(shouldAddHost){
                                    hostList.add(tempHost);
                                }
                            }

                            if(hostAccAdapter != null && !snapshots.getMetadata().isFromCache() && searchListener != null){
                                searchListener.remove();
                            }
                        } else {
                            hostList.clear();
                        }

                        Collections.sort(hostList);
                        setHostAdapter();
                    }
                });
    }

    private void setHostAdapter(){
        ArrayList<String> documentPushId = new ArrayList<>();
        for(Room room : hostList){
            documentPushId.add(room.id);
        }

        hostAccAdapter = new HostStayRecyclerGridAdapter(parentActivity,null, hostList,documentPushId,false,search);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                resultRV.setAdapter(hostAccAdapter);
                resultRV.setLayoutManager(new GridLayoutManager(parentActivity, 2));

                if(hostList.isEmpty()){
                    resultCL.setVisibility(View.GONE);
                    nothingTV.setText(emptyTxt);
                    nothingTV.setVisibility(View.VISIBLE);
                }else{
                    resultCL.setVisibility(View.VISIBLE);
                    nothingTV.setVisibility(View.GONE);
                }
            }
        });
    }

    /*****************************************************JOB******************************************************************************/
    private void getJob(){
        searchRef = GeneralFunction.getFirestoreInstance()
                .collection(Job.URL_JOB)
                .whereEqualTo(Job.IS_AVAILABLE,true);

        if(search != null){
//            if(search.boundingBox != null && search.boundingBox.get(Config.BOUNDING_BOX_BTM_LEFT) != null && search.boundingBox.get(Config.BOUNDING_BOX_TOP_RIGHT) != null){
//                jobRef = jobRef.whereGreaterThan(Job.GPS,new GeoPoint(search.boundingBox.get(Config.BOUNDING_BOX_BTM_LEFT).latitude,search.boundingBox.get(Config.BOUNDING_BOX_BTM_LEFT).longitude))
//                        .whereLessThan(Job.GPS,new GeoPoint(search.boundingBox.get(Config.BOUNDING_BOX_TOP_RIGHT).latitude,search.boundingBox.get(Config.BOUNDING_BOX_TOP_RIGHT).longitude));
//            }
//            searchRef = PlaceAddress.getQueriedPlaceAddress(searchRef,search.placeAddress,null);
            if(isStrictMode){
                searchRef = PlaceAddress.getQueriedPlaceAddressSublocality(searchRef,search.placeAddress,null);
            }else{
                searchRef = PlaceAddress.getQueriedPlaceAddressAdministrativeArea(searchRef,search.placeAddress,null);
            }
        }

        searchListener = searchRef
//                .orderBy(Job.GPS)
//                .orderBy(Job.DATE_UPDATED, Query.Direction.DESCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot snapshots, @javax.annotation.Nullable final FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w(TAG, "Listen failed.", e);
                            resultCL.setVisibility(View.GONE);
                            nothingTV.setText(errorTxt);
                            nothingTV.setVisibility(View.VISIBLE);
                            return;
                        }

                        jobList.clear();

                        if (snapshots != null && !snapshots.isEmpty()) {
                            for (QueryDocumentSnapshot snapshot : snapshots) {
                                Job tempJob = new Job(parentActivity, snapshot);
                                jobList.add(tempJob);
                            }

                            if(jobAdapter != null && !snapshots.getMetadata().isFromCache() && searchListener != null){
                                searchListener.remove();
                            }
                        } else {
                            jobList.clear();
                        }

                        Collections.sort(jobList);
                        setJobAdapter();
                    }
                });
    }

    private void setJobAdapter(){
        jobAdapter = new JobRecyclerGridAdapter(parentActivity,SearchResultFragment.this, jobList);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                resultRV.setAdapter(jobAdapter);
                resultRV.setLayoutManager(new GridLayoutManager(parentActivity, 2));

                if(jobList.isEmpty()){
                    resultCL.setVisibility(View.GONE);
                    nothingTV.setText(emptyTxt);
                    nothingTV.setVisibility(View.VISIBLE);
                }else{
                    resultCL.setVisibility(View.VISIBLE);
                    nothingTV.setVisibility(View.GONE);
                }
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.search_results));

                switch (searchType){
                    case Search.TYPE_ITINERARY:
                        parentActivity.selectItinerary();
                        break;
                    case Search.TYPE_HOMESTAY:
                        parentActivity.selectHotel();
                        break;
                    case Search.TYPE_JOB:
                        parentActivity.selectJob();
                        break;
                    default:
                        parentActivity.selectItinerary();
                        break;
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(searchListener != null){
            searchListener.remove();
        }
    }
}
