package com.vidatechft.yupa.MainSearchFragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.classes.Search;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class SearchHostRoomAdvanceFragment extends Fragment implements View.OnClickListener{
    public static final String TAG = SearchHostRoomAdvanceFragment.class.getName();
    private MainActivity parentActivity;
    private Search search,oriSearch;

    private TextView minBudgetTV, maxBudgetTV;
    private EditText noofGuestET, noofBedET;
    private SeekBar budgetSB;
    private RadioButton entireRB,privateRB,sharedRB;

    private String currencyCode;
    private double minBudget, maxBudget;
    private boolean isMinBudgetLastSelected = true;
    private ArrayList<CheckBox> checkBoxes;

    private ConstraintLayout constraintLayoutRuleSpinnerItem;
    private ListView listViewRuleSpinnerItem;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static SearchHostRoomAdvanceFragment newInstance(MainActivity parentActivity, Search search) {
        SearchHostRoomAdvanceFragment fragment = new SearchHostRoomAdvanceFragment();

        fragment.parentActivity = parentActivity;
        fragment.search = search;
        fragment.oriSearch = search;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_main_host_room_search_advanced, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.hideBtmNav();

                currencyCode = GeneralFunction.getCurrencyCode(parentActivity);

                TextView nextTV = view.findViewById(R.id.nextTV);
                TextView skipTV = view.findViewById(R.id.skipTV);
                noofGuestET = view.findViewById(R.id.noofGuestET);
                noofBedET = view.findViewById(R.id.noofBedET);
                budgetSB = view.findViewById(R.id.budgetSB);
                minBudgetTV = view.findViewById(R.id.minBudgetTV);
                maxBudgetTV = view.findViewById(R.id.maxBudgetTV);
                entireRB = view.findViewById(R.id.entireRB);
                privateRB = view.findViewById(R.id.privateRB);
                sharedRB = view.findViewById(R.id.sharedRB);
                LinearLayout checkboxesLL = view.findViewById(R.id.checkboxesLL);

                setupSeekbar();
                setupAmenityList(checkboxesLL);

                constraintLayoutRuleSpinnerItem = view.findViewById(R.id.constraintLayoutSpinnerItem);
                listViewRuleSpinnerItem = view.findViewById(R.id.listViewRuleSpinnerItem);

                nextTV.setOnClickListener(SearchHostRoomAdvanceFragment.this);
                skipTV.setOnClickListener(SearchHostRoomAdvanceFragment.this);
                noofGuestET.setOnClickListener(SearchHostRoomAdvanceFragment.this);
                noofBedET.setOnClickListener(SearchHostRoomAdvanceFragment.this);
                minBudgetTV.setOnClickListener(SearchHostRoomAdvanceFragment.this);
                maxBudgetTV.setOnClickListener(SearchHostRoomAdvanceFragment.this);
                entireRB.setOnClickListener(SearchHostRoomAdvanceFragment.this);
                privateRB.setOnClickListener(SearchHostRoomAdvanceFragment.this);
                sharedRB.setOnClickListener(SearchHostRoomAdvanceFragment.this);

                if(isMinBudgetLastSelected){
                    minBudgetTV.setActivated(true);
                    maxBudgetTV.setActivated(false);
                }else{
                    minBudgetTV.setActivated(false);
                    maxBudgetTV.setActivated(true);
                }
                String minBudgetStr = currencyCode + String.valueOf((int)search.minBudget);
                minBudgetTV.setText(minBudgetStr);
                String maxBudgetStr = currencyCode + String.valueOf((int)search.maxBudget);
                maxBudgetTV.setText(maxBudgetStr);
            }
        });

        return view;
    }

    private void setupSeekbar(){
        budgetSB.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(fromUser){
                    progress = progress * Search.PROGRESS_MULTIPLIER;

                    String budget = currencyCode + String.valueOf(progress);
                    if(minBudgetTV.isActivated()){
                        minBudgetTV.setText(budget);
                        minBudget = progress;

                        if(minBudget >= maxBudget){
                            maxBudget = minBudget;
                            maxBudgetTV.setText(budget);
                        }

                    }else if(maxBudgetTV.isActivated()){
                        maxBudgetTV.setText(budget);
                        maxBudget = progress;

                        if(maxBudget <= minBudget){
                            minBudget = maxBudget;
                            minBudgetTV.setText(budget);
                        }
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void setupAmenityList(LinearLayout checkboxesLL){
        checkBoxes = new ArrayList<>();
        final String[] earnList = parentActivity.getResources().getStringArray(R.array.host_select_amenity_list);
        for(String earning : earnList){
            CheckBox checkBox = (CheckBox) CheckBox.inflate(parentActivity,R.layout.custom_checkbox,null);
            checkBox.setText(earning.trim());
            if(search.amenityList != null && search.amenityList.contains(earning.trim())){
                checkBox.setChecked(true);
            }
            checkboxesLL.addView(checkBox);
            checkBoxes.add(checkBox);
        }
    }

    private void goToRoomResult(Search search){
        Fragment searchResultHostRoomMapFragment = SearchResultHostRoomMapFragment.newInstance(parentActivity,search);
        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, searchResultHostRoomMapFragment).addToBackStack(SearchResultHostRoomMapFragment.TAG).commit();
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.homestay_booking_action_bar3));
                parentActivity.selectHotel();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.nextTV:
                search.minBudget = minBudget;
                search.maxBudget = maxBudget;
                if(entireRB.isChecked()){
                    search.roomType = Room.TYPE_ROOM_ENTIRE_PLACE;
                }else if(privateRB.isChecked()){
                    search.roomType = Room.TYPE_ROOM_PRIVATE;
                }else if(sharedRB.isChecked()){
                    search.roomType = Room.TYPE_ROOM_SHARE;
                }
                if(!noofGuestET.getText().toString().trim().isEmpty()){
                    search.noofGuest = Integer.parseInt(noofGuestET.getText().toString());
                }

                if(!noofBedET.getText().toString().trim().isEmpty()){
                    search.noofBed = Integer.parseInt(noofBedET.getText().toString());
                }

                search.amenityList = new ArrayList<>();
                for(CheckBox checkBox : checkBoxes){
                    if(checkBox.isChecked()){
                        search.amenityList.add(checkBox.getText().toString());
                    }
                }

                goToRoomResult(search);
                break;
            case R.id.skipTV:
                goToRoomResult(oriSearch);
                break;
            case R.id.noofBedET:
                GeneralFunction.slideUpAnimationForListView(parentActivity,constraintLayoutRuleSpinnerItem, listViewRuleSpinnerItem,R.array.bedroom_array,noofBedET);
                MainActivity.isOpen = true;
                break;
            case R.id.noofGuestET:
                GeneralFunction.slideUpAnimationForListView(parentActivity,constraintLayoutRuleSpinnerItem, listViewRuleSpinnerItem,R.array.guestAllow_array,noofGuestET);
                MainActivity.isOpen = true;
                break;
            case R.id.minBudgetTV:
                minBudgetTV.setActivated(true);
                maxBudgetTV.setActivated(false);
                isMinBudgetLastSelected = minBudgetTV.isActivated();
                break;
            case R.id.maxBudgetTV:
                minBudgetTV.setActivated(false);
                maxBudgetTV.setActivated(true);
                isMinBudgetLastSelected = minBudgetTV.isActivated();
                break;
            case R.id.entireRB:
                entireRB.setChecked(true);
                privateRB.setChecked(false);
                sharedRB.setChecked(false);
                break;
            case R.id.privateRB:
                entireRB.setChecked(false);
                privateRB.setChecked(true);
                sharedRB.setChecked(false);
                break;
            case R.id.sharedRB:
                entireRB.setChecked(false);
                privateRB.setChecked(false);
                sharedRB.setChecked(true);
                break;
        }
    }
}
