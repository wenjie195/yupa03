package com.vidatechft.yupa.MainSearchFragment;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.OnRangeSelectedListener;
import com.vidatechft.yupa.CalendaUtilities.MySelectorDecorator;
import com.vidatechft.yupa.CalendaUtilities.TodayDecorator;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.bookHostRoomFragment.EnterGuestDetailsFragment;
import com.vidatechft.yupa.classes.BookingHistory;
import com.vidatechft.yupa.classes.Itinerary;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.classes.Search;
import com.vidatechft.yupa.classes.TravelKit;
import com.vidatechft.yupa.paymentFragment.PaymentSummaryFragment;
import com.vidatechft.yupa.utilities.GeneralFunction;

import org.threeten.bp.LocalDate;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class SearchHostRoomDateFragment extends Fragment implements View.OnClickListener{
    public static final String TAG = SearchHostRoomDateFragment.class.getName();
    private MainActivity parentActivity;
    private Search search;
    private Room room;

    private TextView checkinDateTV, checkoutDateTV, noofNightTV;
    private MaterialCalendarView checkinCV;
    private LinearLayout checkinLL, checkoutLL;
    private boolean isLongClick = false;
    private int countClick = 0;
    private List<CalendarDay> mDates = new ArrayList<>();
    private List<CalendarDay> tempDates = new ArrayList<>();

    //from buying itinerary or travel kits only
    private BookingHistory bookingHistory;
    private Itinerary itinerary;
    private ArrayList<TravelKit> selectedTKs;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static SearchHostRoomDateFragment newInstance(MainActivity parentActivity, Search search) {
        SearchHostRoomDateFragment fragment = new SearchHostRoomDateFragment();

        fragment.parentActivity = parentActivity;
        fragment.search = search;

        return fragment;
    }

    public static SearchHostRoomDateFragment newInstance(MainActivity parentActivity, Search search, Room room) {
        SearchHostRoomDateFragment fragment = new SearchHostRoomDateFragment();

        fragment.parentActivity = parentActivity;
        fragment.search = search;
        fragment.room = room;

        return fragment;
    }

    public static SearchHostRoomDateFragment newInstance(MainActivity parentActivity, BookingHistory bookingHistory, Itinerary itinerary, ArrayList<TravelKit> selectedTKs, Search search) {
        SearchHostRoomDateFragment fragment = new SearchHostRoomDateFragment();

        fragment.parentActivity = parentActivity;
        fragment.bookingHistory = bookingHistory;
        fragment.itinerary = itinerary;
        fragment.selectedTKs = selectedTKs;
        if(search == null){
            fragment.search = new Search();
        }else{
            fragment.search = search;
        }

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_main_host_room_search_date, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.hideBtmNav();

                checkinLL = view.findViewById(R.id.checkinLL);
                checkoutLL = view.findViewById(R.id.checkoutLL);
                checkinDateTV = view.findViewById(R.id.startBookingDateTV);
                checkoutDateTV = view.findViewById(R.id.endBookingDateTV);
                noofNightTV = view.findViewById(R.id.noOfDayTV);
                checkinCV = view.findViewById(R.id.checkinCV);
                TextView confirmTV = view.findViewById(R.id.confirmTV);
                confirmTV.setOnClickListener(SearchHostRoomDateFragment.this);

                initCustomCalendar();

                if(itinerary != null || (selectedTKs != null && !selectedTKs.isEmpty())){
                    TextView startBookingLabelTV = view.findViewById(R.id.startBookingLabelTV);
                    TextView endBookingLabelTV = view.findViewById(R.id.endBookingLabelTV);

                    startBookingLabelTV.setText(parentActivity.getString(R.string.search_depart_date));
                    endBookingLabelTV.setText(parentActivity.getString(R.string.search_return_date));
                }
            }
        });

        return view;
    }

    private void initCustomCalendar(){
        //this is to fix min date not precede toDate(current date), the min date for kitkat version must not equal to the current calendar time
        final Calendar currentDate = GeneralFunction.getDefaultUtcCalendar();
        final LocalDate localDate = LocalDate.now();
        checkinCV.state().edit()
                .setMinimumDate(localDate)
                .commit();

        checkinCV.addDecorators(new TodayDecorator());
        //to get today date and auto select
        checkinCV.setSelectedDate(localDate);
        checkinCV.setSelectionMode(MaterialCalendarView.SELECTION_MODE_RANGE);

        mDates.clear();
        checkinCV.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView materialCalendarView, @NonNull CalendarDay calendarDay, boolean b) {
                List<CalendarDay>dates = new ArrayList<>();
                if(b){
                    dates.add(calendarDay);
                    mDates.clear();
                    mDates.add(calendarDay);
                    materialCalendarView.setSelectedDate(calendarDay);
                    handleOnDayChanged(dates);
                }
            }
        });

        checkinCV.setOnRangeSelectedListener(new OnRangeSelectedListener() {
            @Override
            public void onRangeSelected(@NonNull MaterialCalendarView widget, @NonNull final List<CalendarDay> dates) {

                CalendarDay firstDay = dates.get(0);
                CalendarDay lastDay = dates.get(dates.size()-1);
//                        final Calendar mCalendarFirstDay = GeneralFunction.getDefaultUtcCalendar(firstDay.getYear(),firstDay.getMonth(),firstDay.getDay());
//                        final Calendar mCalendarLastDay = GeneralFunction.getDefaultUtcCalendar(lastDay.getYear(),lastDay.getMonth(),lastDay.getDay());

                final HashMap<CalendarDay,Drawable> customCalendarDayMap = new HashMap<>();
                //highlight the calendar date
                for(int i = 0; i< dates.size(); i++){
                    if(dates.size() == 1){
                        customCalendarDayMap.put(dates.get(i),parentActivity.getResources().getDrawable(R.drawable.square_single_calendar));
                    }
                    else{
                        if(dates.get(i).equals(firstDay)){
                            customCalendarDayMap.put(dates.get(i),parentActivity.getResources().getDrawable(R.drawable.left_half_circle));
                        }
                        if(dates.get(i).equals(lastDay)){
                            customCalendarDayMap.put(dates.get(i),parentActivity.getResources().getDrawable(R.drawable.right_half_circle));
                        }
                        if(!dates.get(i).equals(firstDay) && !dates.get(i).equals(lastDay)){
                            customCalendarDayMap.put(dates.get(i),parentActivity.getResources().getDrawable(R.drawable.square_calendar));
                        }
                    }
                }
                mDates.addAll(dates);
                //solve the reverse date
                if(mDates.get(0).equals(firstDay)||mDates.get(0).equals(lastDay)){
                    mDates.remove(mDates.get(0));
                }

                final MySelectorDecorator mySelectorDecorator =new MySelectorDecorator(parentActivity,customCalendarDayMap,firstDay, lastDay);
                checkinCV.addDecorators(mySelectorDecorator);

                widget.setOnDateChangedListener(new OnDateSelectedListener() {
                    @Override
                    public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                        List<CalendarDay>dates = new ArrayList<>();
                        if(selected){
                            isLongClick = false;
//                                    GeneralFunction.Toast(parentActivity,"you selected" + date.toString());
                            checkinDateTV.setText("");
                            checkoutDateTV.setText("");
                            noofNightTV.setText("");
                            checkinCV.removeDecorator(mySelectorDecorator);

                            //to select the date in onRangeSelect
                            dates.add(date);
                            handleOnDayChanged(dates);

                            //clear mDate
                            mDates.clear();
                            mDates.add(date);
                        }
                        if(!selected){
//                                    GeneralFunction.Toast(parentActivity," not select");
                            checkinDateTV.setText("");
                            checkoutDateTV.setText("");
                            noofNightTV.setText("");
                            checkinCV.removeDecorator(mySelectorDecorator);
                            //to programmatically set selected day
                            widget.setSelectedDate(date);
                            dates.add(date);
                            mDates.clear();
                            handleOnDayChanged(dates);

                        }

                    }
                });

                checkinLL.setActivated(false);
                checkoutLL.setActivated(false);

                handleOnDayChanged(dates);
            }
        });
    }

    private void handleOnDayChanged(List<CalendarDay>dates){
        //getmonthvalue - 1 because the JAN default value is 0 LOL bobo one
        LocalDate mCheckInDate = dates.get(0).getDate();
        LocalDate mCheckOutDate = dates.get(dates.size()-1).getDate();
        Calendar checkInDate = GeneralFunction.getDefaultUtcCalendar(mCheckInDate.getYear(),mCheckInDate.getMonthValue()-1,mCheckInDate.getDayOfMonth());
        Calendar checkOutDate = GeneralFunction.getDefaultUtcCalendar(mCheckOutDate.getYear(),mCheckOutDate.getMonthValue()-1,mCheckOutDate.getDayOfMonth());
        search.checkin_date = checkInDate.getTimeInMillis();
        checkinDateTV.setText(GeneralFunction.formatDateToString(search.checkin_date));
        search.checkout_date = checkOutDate.getTimeInMillis();
        checkoutDateTV.setText(GeneralFunction.formatDateToString(search.checkout_date));

        if(search.checkin_date > search.checkout_date){
            search.checkout_date = search.checkin_date;
            checkoutDateTV.setText(GeneralFunction.formatDateToString(search.checkout_date));
        }
        if(search.checkout_date < search.checkin_date){
            search.checkin_date = search.checkout_date;
            checkinDateTV.setText(GeneralFunction.formatDateToString(search.checkin_date));
        }

        long noofNight = GeneralFunction.getNoofDaysInDate(search.checkin_date,search.checkout_date);
        if(noofNight > 1){
            noofNight = noofNight - 1;
        }
        if(noofNight <= 1){
            noofNightTV.setText(String.format(parentActivity.getString(R.string.noof_night),noofNight));
        }else{
            noofNightTV.setText(String.format(parentActivity.getString(R.string.noof_nights),noofNight));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.confirmTV:
                if(search == null || search.checkin_date == 0 || search.checkout_date == 0){
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_search_checkin_out),R.drawable.notice_bad,TAG);
                }else{
                    if(itinerary != null || (selectedTKs != null && !selectedTKs.isEmpty())){
                        Fragment paymentSummaryFragmentNo = PaymentSummaryFragment.newInstance(parentActivity,bookingHistory,itinerary,selectedTKs,search);
                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, paymentSummaryFragmentNo).addToBackStack(PaymentSummaryFragment.TAG).commit();
                        return;
                    }

                    if(room != null){
                        parentActivity.initializeLoadingDialog(parentActivity.getString(R.string.checking_availability),false);
                        parentActivity.controlLoadingDialog(true,parentActivity);

                        GeneralFunction.getFirestoreInstance()
                                .collection(BookingHistory.URL_BOOKING_HISTORY)
                                .whereEqualTo(BookingHistory.TARGET_ID,room.id)
                                .whereEqualTo(BookingHistory.ACTION_TYPE,BookingHistory.ACTION_TYPE_ACCEPTED)
                                .whereGreaterThanOrEqualTo(BookingHistory.CHECKOUT_DATE,search.checkin_date)
                                .get()
                                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                        if(GeneralFunction.hasError(parentActivity,task,TAG)){
                                            return;
                                        }

                                        boolean isClashed = false;

                                        if(task.getResult() != null && !task.getResult().isEmpty()){
                                            for(DocumentSnapshot bkHistorySnapshot : task.getResult().getDocuments()){
                                                BookingHistory tempBookingHistory = bkHistorySnapshot.toObject(BookingHistory.class);
                                                if (tempBookingHistory != null && tempBookingHistory.checkinDate != null && tempBookingHistory.checkoutDate != null) {
                                                    isClashed = GeneralFunction.isSearchedDateClashedWithOtherBookedDates(
                                                            tempBookingHistory.checkinDate,tempBookingHistory.checkoutDate,search.checkin_date,search.checkout_date);
                                                    if(isClashed){
                                                        break;
                                                    }
                                                }
                                            }
                                        }

                                        parentActivity.controlLoadingDialog(false,parentActivity);
                                        if(!isClashed){
                                            Fragment enterGuestDetailsFragment = EnterGuestDetailsFragment.newInstance(parentActivity,search,room);
                                            GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, enterGuestDetailsFragment).addToBackStack(EnterGuestDetailsFragment.TAG).commit();
                                        }else{
                                            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.date_clashed_please_select_another_date),R.drawable.notice_bad,TAG);
                                        }
                                    }
                                });
                    }else{
                        Fragment searchHostRoomAdvanceFragment = SearchHostRoomAdvanceFragment.newInstance(parentActivity, search);
                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, searchHostRoomAdvanceFragment).addToBackStack(SearchHostRoomAdvanceFragment.TAG).commit();
                    }

                }
                break;
            case R.id.checkinLL:
                checkinLL.setActivated(false);
                checkoutLL.setActivated(false);
                break;
            case R.id.checkoutLL:
                checkinLL.setActivated(false);
                checkoutLL.setActivated(false);
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(mDates!= null && mDates.size() != 0){
            tempDates.addAll(mDates);
            mDates.clear();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.homestay_booking_action_bar2));
                parentActivity.selectHotel();
                if(tempDates!= null && tempDates.size() != 0){
                    mDates.addAll(tempDates);
                    if(mDates != null && mDates.size()!=0){
                        final HashMap<CalendarDay,Drawable> customCalendarDayMap = new HashMap<>();
                        CalendarDay firstDay = mDates.get(0);
                        CalendarDay lastDay = mDates.get(mDates.size()-1);
                        //highlight the calendar date
                        if(mDates.size() == 1){
                            checkinCV.setSelectedDate(mDates.get(0));
                            handleOnDayChanged(mDates);
                        }
                        else{
                            for(int i = 0; i< mDates.size(); i++){
                                if(mDates.size() == 1){
                                    customCalendarDayMap.put(mDates.get(i),parentActivity.getResources().getDrawable(R.drawable.square_single_calendar));
                                }
                                else{
                                    if(mDates.get(i).equals(firstDay)){
                                        customCalendarDayMap.put(mDates.get(i),parentActivity.getResources().getDrawable(R.drawable.left_half_circle));
                                    }
                                    if(mDates.get(i).equals(lastDay)){
                                        customCalendarDayMap.put(mDates.get(i),parentActivity.getResources().getDrawable(R.drawable.right_half_circle));
                                    }
                                    if(!mDates.get(i).equals(firstDay) && !mDates.get(i).equals(lastDay)){
                                        customCalendarDayMap.put(mDates.get(i),parentActivity.getResources().getDrawable(R.drawable.square_calendar));
                                    }
                                }
                            }
                            handleOnDayChanged(mDates);

                            final MySelectorDecorator mySelectorDecorator =new MySelectorDecorator(parentActivity,customCalendarDayMap,firstDay, lastDay);
                            checkinCV.addDecorators(mySelectorDecorator);

                            checkinCV.setOnDateChangedListener(new OnDateSelectedListener() {
                                @Override
                                public void onDateSelected(@NonNull MaterialCalendarView materialCalendarView, @NonNull CalendarDay calendarDay, boolean b) {
                                    List<CalendarDay>dates = new ArrayList<>();
                                    if(b){
                                        dates.add(calendarDay);
                                        checkinCV.removeDecorator(mySelectorDecorator);
                                        mDates.clear();
                                        mDates.add(calendarDay);
                                        materialCalendarView.setSelectedDate(calendarDay);
                                        handleOnDayChanged(dates);
                                    }
                                }
                            });
                        }
                    }
                    tempDates.clear();
                }
            }
        });
    }

}
