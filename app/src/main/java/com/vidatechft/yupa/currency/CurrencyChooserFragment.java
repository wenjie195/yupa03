package com.vidatechft.yupa.currency;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;

public class CurrencyChooserFragment extends Fragment{
    public static final String TAG = CurrencyChooserFragment.class.getName();
    private MainActivity parentActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static CurrencyChooserFragment newInstance(MainActivity parentActivity) {
        CurrencyChooserFragment fragment = new CurrencyChooserFragment();

        fragment.parentActivity = parentActivity;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_currency_chooser, container, false);

        CurrencyChooserLayout customCurrencyChooserLayout = view.findViewById(R.id.customCurrencyChooserLayout);

        customCurrencyChooserLayout.initLayout(parentActivity, customCurrencyChooserLayout);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.showToolbar();
        parentActivity.hideBtmNav();
        parentActivity.setToolbarTitle(parentActivity.getString(R.string.nav_currency_list));
    }

    @Override
    public void onPause() {
        super.onPause();
        parentActivity.showToolbar();
        parentActivity.showBtmNav();
    }
}