package com.vidatechft.yupa.currency;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.TravelKitReceiptAdapter;
import com.vidatechft.yupa.classes.CurrencyRate;
import com.vidatechft.yupa.classes.Payment;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.HashMap;

//from here https://medium.com/@Sserra90/android-writing-a-compound-view-1eacbf1957fc
public class CurrencyChooserLayout extends LinearLayout {
    public static final String TAG = CurrencyChooserLayout.class.getName();
    private MainActivity parentActivity;
    private CurrencyChooserLayout layout;

    private ArrayList<CurrencyRate> currencyList = new ArrayList<>();
    private HashMap<String,String> currenyNameMap = new HashMap<>();
    private RecyclerView customCurrencyChooserRV;
    private CurrencyChooserAdapter adapter;

    public CurrencyChooserLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init(){
        //if dont have the lines setOrientation and setLayoutParams, the material calendar view wont have any content
        setOrientation(LinearLayout.VERTICAL);
//        setGravity(Gravity.CENTER);
        setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        //Inflate xml resource, pass "this" as the parent, we use <merge> tag in xml to avoid
        //redundant parent, otherwise a LinearLayout will be added to this LinearLayout ending up
        //with two view groups
        inflate(getContext(),R.layout.layout_currency_chooser,this);

        initView();
    }

    private void initView(){
        customCurrencyChooserRV = findViewById(R.id.customCurrencyChooserRV);
    }

    public void initLayout(MainActivity parentActivity,CurrencyChooserLayout layout){
        this.parentActivity = parentActivity;
        this.layout = layout;

        currencyList.clear();
        currencyList = CurrencyRate.getAllRatesFromSP(parentActivity);

        //START this removes everthing from list except MYR and USD
        ArrayList<CurrencyRate> tempCurrencyList = new ArrayList<>();
        for(CurrencyRate tempCurrencyRate : currencyList){
            if(TextUtils.equals(tempCurrencyRate.code,"MYR") || TextUtils.equals(tempCurrencyRate.code,"USD")){
                tempCurrencyList.add(tempCurrencyRate);
            }
        }
        currencyList.clear();
        currencyList.addAll(tempCurrencyList);
        //END this removes everthing from list except MYR and USD

        currenyNameMap = CurrencyRate.getCurrencyName(parentActivity);
        setupAdapter();
    }

    private void setupAdapter(){
        adapter = new CurrencyChooserAdapter(parentActivity,currencyList,currenyNameMap);

        adapter.setCurrentCurrency(GeneralFunction.getCurrencyCode(parentActivity));
        customCurrencyChooserRV.setNestedScrollingEnabled(false);
        customCurrencyChooserRV.setAdapter(adapter);
        customCurrencyChooserRV.setLayoutManager(new LinearLayoutManager(parentActivity,LinearLayoutManager.VERTICAL, false));
    }

    public boolean isEmpty(){
        if(parentActivity == null || currencyList == null || currencyList.isEmpty()){
            return true;
        }else{
            return false;
        }
    }

    public int size(){
        if(adapter != null){
            return adapter.getItemCount();
        }else{
            return -1;
        }
    }
}