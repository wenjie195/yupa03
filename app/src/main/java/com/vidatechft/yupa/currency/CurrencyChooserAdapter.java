package com.vidatechft.yupa.currency;

import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.CurrencyRate;

import java.util.ArrayList;
import java.util.HashMap;

public class CurrencyChooserAdapter extends RecyclerView.Adapter<CurrencyChooserAdapter.ViewHolder>  {
    public static final String TAG = CurrencyChooserAdapter.class.getName();
    private MainActivity parentActivity;
    private ArrayList<CurrencyRate> currencyList;
    private HashMap<String,String> currencyNameMap;

    private String currentCurrency = "MYR";

    public CurrencyChooserAdapter(MainActivity parentActivity, ArrayList<CurrencyRate> currencyList, HashMap<String,String> currencyNameMap) {
        this.parentActivity = parentActivity;
        this.currencyList = currencyList;
        this.currencyNameMap = currencyNameMap;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ConstraintLayout wholeCL;
        public TextView textView;
        public RadioButton radioButton;

        public ViewHolder(View v) {
            super(v);
            wholeCL = itemView.findViewById(R.id.wholeCL);
            textView = itemView.findViewById(R.id.textView);
            radioButton = itemView.findViewById(R.id.radioButton);
        }
    }

    public void setCurrentCurrency(String currentCurrency){
        this.currentCurrency = currentCurrency;
    }

    public String getCurrentCurrency(){
        return currentCurrency;
    }

    @NonNull
    @Override
    public CurrencyChooserAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_currency_chooser, parent, false);

        return new CurrencyChooserAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int pos) {
        final int position = pos;
        final CurrencyRate currency = currencyList.get(position);

        if(currency != null){
            StringBuilder sb = new StringBuilder();
            if(currency.code != null){
                sb.append(currency.code);

                if(currencyNameMap != null && currencyNameMap.get(currency.code) != null){
                    sb.append(" - ");
                    sb.append(currencyNameMap.get(currency.code));
                }
            }
            holder.textView.setText(sb.toString());

            if(TextUtils.equals(currency.code,getCurrentCurrency())){
                holder.radioButton.setChecked(true);
                holder.wholeCL.setBackgroundColor(parentActivity.getResources().getColor(R.color.green));
            }else{
                holder.radioButton.setChecked(false);
                holder.wholeCL.setBackgroundColor(parentActivity.getResources().getColor(R.color.white));
            }

            holder.wholeCL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for(CurrencyRate tempCurrency : currencyList){
                        if(TextUtils.equals(tempCurrency.code,getCurrentCurrency())){
                            tempCurrency.isSelected = false;
                        }

                        if(TextUtils.equals(tempCurrency.code,currency.code)){
                            tempCurrency.isSelected = true;
                        }
                    }
                    setCurrentCurrency(currency.code);
                    CurrencyRate.savePreferredCurrencyCodeToSP(parentActivity,getCurrentCurrency());
                    parentActivity.updateHostAdapter();
                    notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return currencyList.size();
    }
}