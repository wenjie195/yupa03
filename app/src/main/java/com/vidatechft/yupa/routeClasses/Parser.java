package com.vidatechft.yupa.routeClasses;

import java.util.List;

public interface Parser {
    List<Route> parse() throws RouteException;
}