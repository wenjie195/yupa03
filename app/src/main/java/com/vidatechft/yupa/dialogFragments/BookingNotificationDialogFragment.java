package com.vidatechft.yupa.dialogFragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.firestore.ListenerRegistration;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.BookingHistory;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.paymentFragment.SelectTravelKitFragment;
import com.vidatechft.yupa.utilities.GeneralFunction;


public class BookingNotificationDialogFragment extends DialogFragment implements View.OnClickListener {
    public static final String TAG = BookingNotificationDialogFragment.class.getName();

    private MainActivity parentActivity;
    private Room room;
    private BookingHistory bookingHistory;

    private ListenerRegistration listener;

    private Button skipBtn,proceedBtn;

    public BookingNotificationDialogFragment() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    public static BookingNotificationDialogFragment newInstance(MainActivity parentActivity, Room room, BookingHistory bookingHistory) {
        BookingNotificationDialogFragment frag = new BookingNotificationDialogFragment();
        frag.parentActivity = parentActivity;
        frag.room = room;
        frag.bookingHistory = bookingHistory;
        return frag;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(true);
        getDialog().setCancelable(true);

        return inflater.inflate(R.layout.dialog_booking_notification, container);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final TextView messageTV =  view.findViewById(R.id.messageTV);
                final ImageView noticeIV = view.findViewById(R.id.noticeIV);
                final TextView nameTV =  view.findViewById(R.id.nameTV);
                proceedBtn = view.findViewById(R.id.proceedBtn);
                skipBtn = view.findViewById(R.id.skipBtn);
                ImageButton closeIB = view.findViewById(R.id.closeIB);

                if(bookingHistory != null && bookingHistory.type != null){
                    switch (bookingHistory.type){
                        case BookingHistory.BOOK_TYPE_HOMESTAY:
                            populateWithRoom(messageTV,noticeIV,nameTV);
                            break;
                        default:
                            nameTV.setText("???");
                            noticeIV.setImageResource(R.drawable.ic_not_found);
                            messageTV.setText(parentActivity.getString(R.string.error_occured));
                            break;
                    }
                }else{
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_occured),R.drawable.notice_bad,TAG);
                    getDialog().dismiss();
                }

                proceedBtn.setOnClickListener(BookingNotificationDialogFragment.this);
                skipBtn.setOnClickListener(BookingNotificationDialogFragment.this);
                closeIB.setOnClickListener(BookingNotificationDialogFragment.this);
            }
        });
    }

    private void populateWithRoom(TextView messageTV, ImageView noticeIV, TextView nameTV){
        if(room != null){
            String accommodationName = "???";
            if(room.accommodationName != null){
                accommodationName = room.accommodationName;
            }

            if(room.urlOutlook != null){
                GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(room.urlOutlook),noticeIV);
            }else{
                GeneralFunction.GlideImageSettingOwnDrawable(parentActivity,R.drawable.ic_not_found,noticeIV);
            }

            nameTV.setText(accommodationName);

            if(bookingHistory != null && bookingHistory.actionType != null){
                switch (bookingHistory.actionType){
                    case BookingHistory.ACTION_TYPE_ACCEPTED:
                        messageTV.setText(String.format(parentActivity.getString(R.string.homestay_booking_confirmation_alert_dialog_approved),accommodationName));
                        break;
                    case BookingHistory.ACTION_TYPE_REJECTED:
                        messageTV.setText(String.format(parentActivity.getString(R.string.homestay_booking_confirmation_alert_dialog_rejected),accommodationName));
                        skipBtn.setText(parentActivity.getString(android.R.string.ok));
                        proceedBtn.setVisibility(View.GONE);
                        break;
                    default:
                        GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_occured),R.drawable.notice_bad,TAG);
                        getDialog().dismiss();
                        break;
                }
            }else{
                messageTV.setText(parentActivity.getString(R.string.error_occured));
            }
        }
    }

    private void setReadTrue(){
        if(bookingHistory != null && bookingHistory.id != null){
            GeneralFunction.getFirestoreInstance()
                    .collection(BookingHistory.URL_BOOKING_HISTORY)
                    .document(bookingHistory.id)
                    .update(BookingHistory.IS_READ_BY_GUEST, true)
                    .addOnCompleteListener(GeneralFunction.getOnCompleteListener(parentActivity,TAG));
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        Window window = getDialog().getWindow();
//        window.setLayout(1000, 650);
        window.setGravity(Gravity.CENTER);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(listener != null){
            listener.remove();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.closeIB:
                setReadTrue();
                getDialog().dismiss();
                break;
            case R.id.proceedBtn:
                setReadTrue();
                bookingHistory.room = room;
                Fragment selectTravelKitFragment = SelectTravelKitFragment.newInstance(parentActivity,null,bookingHistory);
                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, selectTravelKitFragment).addToBackStack(SelectTravelKitFragment.TAG).commit();
                getDialog().dismiss();
                break;
            case R.id.skipBtn:
                setReadTrue();
                getDialog().dismiss();
                break;
        }
    }
}
