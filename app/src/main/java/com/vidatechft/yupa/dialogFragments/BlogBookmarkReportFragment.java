package com.vidatechft.yupa.dialogFragments;

import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FieldValue;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.Blog;
import com.vidatechft.yupa.classes.Rating;
import com.vidatechft.yupa.classes.Report;
import com.vidatechft.yupa.hostFragments.HostStayMainPageFragment;
import com.vidatechft.yupa.utilities.GeneralFunction;
import com.vidatechft.yupa.utilities.KeyboardUtilities;

import java.util.HashMap;
import java.util.Map;

public class BlogBookmarkReportFragment extends DialogFragment implements View.OnClickListener {
    public static final String TAG = BlogBookmarkReportFragment.class.getName();
    private MainActivity parentActivity;
    private Blog blog;
    private BlogBookmarkReportFragment parentFragment;
    private String lastToolbarTitle;
    private ImageView reviewProfilePicIV;
    private TextView reviewProfileNameTV,titleTV;
    private EditText reportET;
    private Button btn_submitReport;
    private BlogBookmarkReportFragment fragment;

    public BlogBookmarkReportFragment() {

    }

    public static BlogBookmarkReportFragment newInstance(MainActivity parentActivity, Blog blog) {
        BlogBookmarkReportFragment frag = new BlogBookmarkReportFragment();
        frag.parentActivity = parentActivity;
        frag.blog = blog;

        return frag;
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(true);
        getDialog().setCancelable(true);
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.rounded_white);
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        return inflater.inflate(R.layout.dialog_blog_bookmark_report, container);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //Image View
                reviewProfilePicIV = view.findViewById(R.id.reviewProfilePicIV);
                //TextView
                reviewProfileNameTV = view.findViewById(R.id.reviewProfileNameTV);
                titleTV = view.findViewById(R.id.titleTV);
                //EditText
                reportET = view.findViewById(R.id.reportET);
                //Button
                btn_submitReport = view.findViewById(R.id.btn_submitReport);
                btn_submitReport.setOnClickListener(BlogBookmarkReportFragment.this);

                titleTV.setText(parentActivity.getResources().getString(R.string.blog_report_title));

            }
        });
    }


    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        KeyboardUtilities.hideSoftKeyboard(parentActivity);
    }

    @Override
    public void onResume() {
        super.onResume();
        Window window = getDialog().getWindow();
//        window.setLayout(1000, 650);
        if (window != null) {
            window.setGravity(Gravity.CENTER);
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_submitReport:
                submitReport();
                break;
        }
    }

    private void submitReport(){
        String report = reportET.getText().toString().trim();
        if(isValidateInput(report)){
            Map<String,Object> reportMap = new HashMap<>();
//            reportMap.put(Report.TARGET_UID, blog.userId);
            reportMap.put(Report.DESCRIPTION_REPORT, report);
            reportMap.put(Report.TARGET_ID, blog.id);
            reportMap.put(Report.USER_ID, parentActivity.uid);
            reportMap.put(Report.REPORT_TYPE, Report.REPORT_TYPE_BLOG);
            reportMap.put(Report.IS_REVIEWED, false);
            reportMap.put(Report.DATE_CREATED, FieldValue.serverTimestamp());
            reportMap.put(Report.DATE_UPDATED,FieldValue.serverTimestamp());

            GeneralFunction.getFirestoreInstance()
                    .collection(Report.URL_REPORT)
                    .document()
                    .set(reportMap)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                dismiss();
                                GeneralFunction.Toast(parentActivity, parentActivity.getString(R.string.successful_submit_report));
                            }
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    if(e!= null){
                        GeneralFunction.Toast(parentActivity,e.toString());
                    }
                }
            });

        }

    }

    private boolean isValidateInput(String report){
        return GeneralFunction.checkAndReturnString(report,parentActivity.getString(R.string.input_report_edit_text),parentActivity);
    }
}
