package com.vidatechft.yupa.dialogFragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.ConnectNearbyUserListAdapter;
import com.vidatechft.yupa.classes.User;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;

public class ConnectNearbyUserClusterListDialogFragment extends DialogFragment {
    public static final String TAG = ConnectNearbyUserClusterListDialogFragment.class.getName();

    private MainActivity parentActivity;
    private ArrayList<User> users;
    private boolean needShowPartTime;

    public ConnectNearbyUserClusterListDialogFragment() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    public static ConnectNearbyUserClusterListDialogFragment newInstance(MainActivity parentActivity, ArrayList<User> users, boolean needShowPartTime) {
        ConnectNearbyUserClusterListDialogFragment frag = new ConnectNearbyUserClusterListDialogFragment();
        frag.parentActivity = parentActivity;
        frag.users = users;
        frag.needShowPartTime = needShowPartTime;
        return frag;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(true);
        getDialog().setCancelable(true);
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.rounded_acctype_white_bg_border_black);

        return inflater.inflate(R.layout.dialog_connect_nearby_user_map_cluster_list, container);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView userRV =  view.findViewById(R.id.userRV);

        ConnectNearbyUserListAdapter connectNearbyUserListAdapter = new ConnectNearbyUserListAdapter(parentActivity,this,users,needShowPartTime);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(parentActivity);
        userRV.setAdapter(connectNearbyUserListAdapter);
        userRV.setLayoutManager(linearLayoutManager);

        if(!users.isEmpty()){
            connectNearbyUserListAdapter.notifyDataSetChanged();
        }else{
            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.chat_nearby_companion_error),R.drawable.notice_bad,TAG);
            getDialog().dismiss();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        Window window = getDialog().getWindow();
//        window.setLayout(1000, 650);
        window.setGravity(Gravity.CENTER);
    }
}
