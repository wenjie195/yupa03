package com.vidatechft.yupa.dialogFragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.classes.GlideApp;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;

import static com.bumptech.glide.load.DecodeFormat.PREFER_RGB_565;

public class MessageNotificationDialogFragment extends DialogFragment {

    private String message;
    private int drawableResource;
    private AppCompatActivity parentActivity;

    public MessageNotificationDialogFragment() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    public static MessageNotificationDialogFragment newInstance(AppCompatActivity parentActivity, String message, int drawableResource) {
        MessageNotificationDialogFragment frag = new MessageNotificationDialogFragment();
        frag.message = message;
        frag.drawableResource = drawableResource;
        frag.parentActivity = parentActivity;
        return frag;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(true);
        getDialog().setCancelable(true);
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.rounded_white);

        return inflater.inflate(R.layout.dialog_message_notification, container);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView messageTV =  view.findViewById(R.id.messageTV);
        Button okBTN = view.findViewById(R.id.okBTN);
        ImageButton closeIB = view.findViewById(R.id.closeIB);
        ImageView noticeIV = view.findViewById(R.id.noticeIV);

//        octoSDV.setImageResource(drawableResource);
        GlideApp.with(getDialog().getContext())
                .asBitmap().format(PREFER_RGB_565)
                .load(drawableResource)
                .placeholder(R.drawable.progress_animation)
                .error(R.drawable.ic_not_found)
                .into(noticeIV);
        messageTV.setText(message);
        okBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().dismiss();
            }
        });

        closeIB.setOnClickListener(new DebouncedOnClickListener(1000) {
            @Override
            public void onDebouncedClick(View v) {
                getDialog().dismiss();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        Window window = getDialog().getWindow();
//        window.setLayout(1000, 650);
        window.setGravity(Gravity.CENTER);
    }
}
