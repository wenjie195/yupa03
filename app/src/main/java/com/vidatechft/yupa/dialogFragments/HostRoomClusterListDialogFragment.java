package com.vidatechft.yupa.dialogFragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.HostRoomMapClusterListAdapter;
import com.vidatechft.yupa.adapter.PTJobNearbyListAdapter;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.classes.Search;
import com.vidatechft.yupa.employerFragments.PTJobSwipeFragment;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;

public class HostRoomClusterListDialogFragment extends DialogFragment {
    public static final String TAG = HostRoomClusterListDialogFragment.class.getName();

    private MainActivity parentActivity;
    private ArrayList<Room> rooms;
    private Search search;

    public HostRoomClusterListDialogFragment() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    public static HostRoomClusterListDialogFragment newInstance(MainActivity parentActivity, ArrayList<Room> rooms, Search search) {
        HostRoomClusterListDialogFragment frag = new HostRoomClusterListDialogFragment();
        frag.parentActivity = parentActivity;
        frag.rooms = rooms;
        frag.search = search;
        return frag;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(true);
        getDialog().setCancelable(true);
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.rounded_acctype_white_bg_border_black);

        return inflater.inflate(R.layout.dialog_host_room_map_cluster_list, container);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final RecyclerView roomRV =  view.findViewById(R.id.roomRV);

        final HostRoomMapClusterListAdapter hostRoomMapClusterListAdapter = new HostRoomMapClusterListAdapter(parentActivity,this,rooms,search);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(parentActivity);
                roomRV.setAdapter(hostRoomMapClusterListAdapter);
                roomRV.setLayoutManager(linearLayoutManager);

                if(!rooms.isEmpty()){
                    hostRoomMapClusterListAdapter.notifyDataSetChanged();
                }else{
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.host_room_err_loading_other),R.drawable.notice_bad,TAG);
                    getDialog().dismiss();
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        Window window = getDialog().getWindow();
//        window.setLayout(1000, 650);
        window.setGravity(Gravity.CENTER);
    }
}
