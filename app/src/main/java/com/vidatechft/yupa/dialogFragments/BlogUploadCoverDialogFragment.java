package com.vidatechft.yupa.dialogFragments;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.blog.AddBlogFragment;
import com.vidatechft.yupa.blog.BlogPreviewFragment;
import com.vidatechft.yupa.classes.Blog;
import com.vidatechft.yupa.classes.Chat;
import com.vidatechft.yupa.classes.GroupChat;
import com.vidatechft.yupa.classes.Message;
import com.vidatechft.yupa.utilities.FilePath;
import com.vidatechft.yupa.utilities.GeneralFunction;

import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.UUID;

import kotlin.Function;

import static android.app.Activity.RESULT_OK;

public class BlogUploadCoverDialogFragment extends DialogFragment implements View.OnClickListener {

    public static final String TAG = BlogUploadCoverDialogFragment.class.getName();
    private MainActivity parentActivity;
    private ImageView blogUploadIV;
    private Button btn_upload,btn_cancel;
    private String coverUrl;
    private ImageButton coverPhotoOption;
    private Blog blog;
    private BlogUploadCoverDialogFragment blogUploadCoverDialogFragment;

    public BlogUploadCoverDialogFragment()
    {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static BlogUploadCoverDialogFragment newInstance(MainActivity parentActivity, Blog blog)
    {
        BlogUploadCoverDialogFragment fragment = new BlogUploadCoverDialogFragment();
        fragment.parentActivity = parentActivity;
        fragment.blog = blog;

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater , ViewGroup container ,
            Bundle savedInstanceState)
    {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(true);
        getDialog().setCancelable(true);
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.rounded_white);

        return inflater.inflate(R.layout.fragment_blog_upload_image_dialog, container);
    }

    @Override
    public void onViewCreated(@NonNull final View view , @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view , savedInstanceState);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run()
            {
                //ImageView
                blogUploadIV = view.findViewById(R.id.blogUploadIV );
                //Button
                btn_upload = view.findViewById(R.id.btn_upload);
                btn_cancel = view.findViewById(R.id.btn_cancel);
                coverPhotoOption = view.findViewById(R.id.coverPhotoOption);
                //Onclick Listener
                btn_upload.setOnClickListener(BlogUploadCoverDialogFragment.this);
                btn_cancel.setOnClickListener(BlogUploadCoverDialogFragment.this);
                coverPhotoOption.setOnClickListener(BlogUploadCoverDialogFragment.this);

                if(blog.coverPhoto != null){
                    GeneralFunction.GlideImageSetting(parentActivity,Uri.parse(blog.coverPhoto),blogUploadIV);
                    coverPhotoOption.setBackground(parentActivity.getResources().getDrawable(R.drawable.ic_edit));
                }
                else{
                    coverPhotoOption.setBackground(parentActivity.getResources().getDrawable(R.drawable.ic_add));
                }
            }
        });
    }

    @Override
    public void onResume()
    {
        super.onResume();
        Window window = getDialog().getWindow();
        //https://guides.codepath.com/android/using-dialogfragment
        Point size = new Point();
        // Store dimensions of the screen in `size`
        Display display = window.getWindowManager().getDefaultDisplay();
        display.getSize(size);
        // Set the width of the dialog proportional to 75% of the screen width
        window.setLayout((int) (size.x * 0.8),WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.blogUploadIV:

                break;
            case R.id.btn_upload:
//                uploadCoverImage();
                if(checkImage()){
                    blog.coverPhoto = coverUrl;
                    getDialog().dismiss();
                }
                break;
            case R.id.btn_cancel:
                getDialog().dismiss();
                break;
            case R.id.coverPhotoOption:
                if (GeneralFunction.isStorageAndCameraPermitted(parentActivity.getApplicationContext(), parentActivity)) {
                    GeneralFunction.showFileChooserRatio169(parentActivity, this);
                }
                break;
        }
    }

    private boolean checkImage(){
        if(coverUrl != null && !TextUtils.isEmpty(coverUrl) && GeneralFunction.checkImageViewHasDrawable(parentActivity,blogUploadIV, R.id.blogUploadIV, coverUrl)){
            return true;
        }
        else{
            GeneralFunction.openMessageNotificationDialog(parentActivity,
                    parentActivity.getResources().getString(R.string.blog_error_upload_cover_image),
                    R.drawable.notice_bad, TAG);
            return false;
        }
    }


}
