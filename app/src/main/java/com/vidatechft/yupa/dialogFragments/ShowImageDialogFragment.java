package com.vidatechft.yupa.dialogFragments;


import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.chatFragments.GroupChatMessageFragment;
import com.vidatechft.yupa.classes.Chat;
import com.vidatechft.yupa.classes.GroupChat;
import com.vidatechft.yupa.classes.Message;
import com.vidatechft.yupa.utilities.FilePath;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.HashMap;

public class ShowImageDialogFragment extends DialogFragment {

    public static final String TAG = ShowImageDialogFragment.class.getName();
    private MainActivity parentActivity;
    private Uri filePath;
    private String fp;
    private ImageView sendIV;
    private ImageView viewImageUploadedIV;
    private EditText msgET;
    private String id;
    private Boolean isGroupChat;

    public ShowImageDialogFragment()
    {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static ShowImageDialogFragment newInstance(MainActivity parentActivity , Uri filePath , String id,Boolean isGroupChat)
    {
        ShowImageDialogFragment fragment = new ShowImageDialogFragment();
        fragment.parentActivity = parentActivity ;
        fragment.filePath = filePath;
        fragment.id = id;
        fragment.isGroupChat = isGroupChat;

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater , ViewGroup container ,
            Bundle savedInstanceState)
    {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(true);
        getDialog().setCancelable(true);
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.rounded_white);

        return inflater.inflate(R.layout.fragment_show_image_dialog, container);
    }

    @Override
    public void onViewCreated(@NonNull final View view , @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view , savedInstanceState);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run()
            {
                viewImageUploadedIV = view.findViewById(R.id.viewImageUploadedIV);
                msgET = view.findViewById(R.id.msgET);
                sendIV = view.findViewById(R.id.sendIV);
                GeneralFunction.GlideImageSetting(parentActivity, filePath, viewImageUploadedIV);


                    sendIV.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v)
                        {
                            final DocumentReference ref;

                            if(isGroupChat)
                            {
                                ref = GeneralFunction.getFirestoreInstance()
                                        .collection(GroupChat.URL_GROUP_CHAT)
                                        .document(id)
                                        .collection(Message.URL_MSG)
                                        .document();
                            }
                            else
                            {
                               ref = GeneralFunction.getFirestoreInstance()
                                        .collection(Chat.URL_CHAT)
                                        .document(id)
                                        .collection(Message.URL_MSG)
                                        .document();
                            }

                            parentActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run()
                                {
                                    Message tempMsg;

                                    fp = FilePath.getPath(parentActivity, filePath);

                                    if(msgET.getText() != null && TextUtils.equals("",msgET.getText().toString().trim()))
                                    {
                                        if(fp != null)
                                        {
                                            initializeProgressBar();
                                            tempMsg = new Message(parentActivity.uid,msgET.getText().toString().trim(),true);
                                            tempMsg.img = fp;

                                            savePicInsideFireStore(tempMsg,id,ref);
                                        }
                                        else
                                        {
                                            Toast.makeText(parentActivity , "Please Insert Your Message" , Toast.LENGTH_LONG).show();
                                        }
                                    }
                                    //Todo if there is message together with picture
                                    else if(msgET.getText() != null && !TextUtils.equals("",msgET.getText().toString().trim()))
                                    {
                                        if(fp != null)
                                        {
                                            initializeProgressBar();
                                            tempMsg = new Message(parentActivity.uid,msgET.getText().toString().trim(),true);
                                            tempMsg.img = fp;

                                            savePicInsideFireStore(tempMsg,id,ref);
                                        }
                                        else
                                        {
                                            tempMsg = new Message(parentActivity.uid,msgET.getText().toString().trim(),false);
                                            updateFireStore(id,tempMsg,ref);
                                        }
                                    }

                                    else
                                    {
                                        GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.chat_blank_msg),R.drawable.notice_bad,TAG);
                                    }
                                }
                            });
                        }
                    });


            }
        });
    }
    private void initializeProgressBar()
    {
        parentActivity.initializeProgressIndicator(parentActivity.getString(R.string.uploadEP_picture), false, this);
        parentActivity.controlProgressIndicator(true, this);
    }

    private void updateFireStore(String id, Message tempMsg, DocumentReference ref)
    {

        ref.set(tempMsg)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.getException() != null){
                            GeneralFunction.openMessageNotificationDialog(parentActivity,task.getException().getMessage(),R.drawable.notice_bad,TAG);
                        }
                    }
                });



        HashMap<String, Object> tempChatMap = new HashMap<>();

        if(isGroupChat)
        {
            tempChatMap.put(GroupChat.GROUP_CHAT_LAST_UPDATED , FieldValue.serverTimestamp());
            tempChatMap.put(GroupChat.GROUP_CHAT_LAST_MESSAGE , tempMsg.msg);
            tempChatMap.put(GroupChat.LAST_TYPE,GroupChat.MESSAGE_TYPE_IMG);
            tempChatMap.put(GroupChat.LAST_UID, parentActivity.uid);
            GeneralFunction.getFirestoreInstance()
                    .collection(GroupChat.URL_GROUP_CHAT)
                    .document(id)
                    .update(tempChatMap);
        }
        else
        {
            tempChatMap.put(Chat.DATE_UPDATED,FieldValue.serverTimestamp());
            tempChatMap.put(Chat.LAST_MESSAGE,tempMsg.msg);
            tempChatMap.put(Chat.LAST_TYPE,Chat.MESSAGE_TYPE_IMG);
            tempChatMap.put(Chat.LAST_UID, parentActivity.uid);
            GeneralFunction.getFirestoreInstance()
                    .collection(Chat.URL_CHAT)
                    .document(id)
                    .update(tempChatMap);
        }

            msgET.setText("");
            if(filePath != null )
            {
                GeneralFunction.handleUploadSuccess(parentActivity , ShowImageDialogFragment.this , R.string.chat_group_successfully_upload_image , TAG);
                filePath = null;
            }
            getDialog().dismiss();
    }

    private void savePicInsideFireStore(final Message tempMsg, final String id,final DocumentReference ref)
    {

        if(isGroupChat)
        {
            final StorageReference storeRef = GeneralFunction.getFirebaseStorageInstance().child(GroupChat.URL_GROUP_CHAT + "/" + id + "/groupMedia/"+parentActivity.uid+"/"+ref.getId());
            UploadTask uploadTask = storeRef.putBytes(GeneralFunction.getCompressedBitmap(parentActivity, FilePath.getPath(parentActivity, filePath)));
            uploadTask.addOnFailureListener(new OnFailureListener()
            {
                @Override
                public void onFailure(@NonNull Exception exception)
                {
                    GeneralFunction.openMessageNotificationDialog(parentActivity,"Error",R.drawable.notice_bad,TAG);

                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>()
            {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot)
                {
                    storeRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>()
                    {
                        @Override
                        public void onSuccess(Uri uri)
                        {
                            if(uri != null){
                                tempMsg.img = uri.toString();
                                updateFireStore(id,tempMsg,ref);
                            }
                        }
                    });
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>()
            {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot)
                {
                    int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * 80) + 10;
                    parentActivity.updateProgress((float) totalProgress, ShowImageDialogFragment.this);
                }
            });
        }
        else
        {
            final StorageReference storeRef1 = GeneralFunction.getFirebaseStorageInstance().child(Chat.URL_CHAT + "/" + id + "/"+parentActivity.uid+"/"+ref.getId());
            UploadTask uploadTask1 = storeRef1.putBytes(GeneralFunction.getCompressedBitmap(parentActivity, FilePath.getPath(parentActivity, filePath)));
            uploadTask1.addOnFailureListener(new OnFailureListener()
            {
                @Override
                public void onFailure(@NonNull Exception exception)
                {
                    GeneralFunction.openMessageNotificationDialog(parentActivity,"Error",R.drawable.notice_bad,TAG);

                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>()
            {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot)
                {
                    storeRef1.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>()
                    {
                        @Override
                        public void onSuccess(Uri uri)
                        {
                            if(uri != null){
                                tempMsg.img = uri.toString();
                                updateFireStore(id,tempMsg,ref);
                            }
                        }
                    });
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>()
            {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot)
                {
                    int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * 80) + 10;
                    parentActivity.updateProgress((float) totalProgress, ShowImageDialogFragment.this);
                }
            });
        }
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        if(filePath != null){
            filePath = null;
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
        Window window = getDialog().getWindow();
        //        window.setLayout(1000, 650);
        window.setGravity(Gravity.CENTER);
    }
}
