package com.vidatechft.yupa.dialogFragments;

import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextClock;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.core.Repo;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.AppliedCandidate;
import com.vidatechft.yupa.classes.Blog;
import com.vidatechft.yupa.classes.BlogDiscussion;
import com.vidatechft.yupa.classes.Job;
import com.vidatechft.yupa.classes.Rating;
import com.vidatechft.yupa.classes.Report;
import com.vidatechft.yupa.classes.User;
import com.vidatechft.yupa.employerFragments.PTJobMapFragment;
import com.vidatechft.yupa.employerFragments.PTJobSwipeFragment;
import com.vidatechft.yupa.hostFragments.HostStayMainPageFragment;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;
import com.vidatechft.yupa.utilities.KeyboardUtilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ReviewReportFragment extends DialogFragment implements View.OnClickListener {
    public static final String TAG = ReviewReportFragment.class.getName();
    private MainActivity parentActivity;
    private HostStayMainPageFragment parentFragment;
    private String lastToolbarTitle;
    private ImageView reviewProfilePicIV;
    private TextView reviewProfileNameTV;
    private EditText reportET;
    private Button btn_submitReport;
    private Rating rating;
    private Blog blogs;
    private ReviewReportFragment fragment;
    private BlogDiscussion blogDiscussion;
    private DocumentReference reportRef;

    public ReviewReportFragment() {

    }

    public static ReviewReportFragment newInstance(MainActivity parentActivity, HostStayMainPageFragment parentFragment) {
        ReviewReportFragment frag = new ReviewReportFragment();
        frag.parentActivity = parentActivity;
        frag.parentFragment = parentFragment;

        return frag;
    }

    public static ReviewReportFragment newInstance(MainActivity parentActivity, Rating rating) {
        ReviewReportFragment frag = new ReviewReportFragment();
        frag.parentActivity = parentActivity;
        frag.rating = rating;
        return frag;
    }

    public static ReviewReportFragment newInstance(MainActivity parentActivity, Blog blogs, BlogDiscussion blogDiscussion) {
        ReviewReportFragment frag = new ReviewReportFragment();
        frag.parentActivity = parentActivity;
        frag.blogs = blogs;
        frag.blogDiscussion = blogDiscussion;
        return frag;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(true);
        getDialog().setCancelable(true);
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.rounded_white);
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        return inflater.inflate(R.layout.dialog_review_report, container);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //Image View
                reviewProfilePicIV = view.findViewById(R.id.reviewProfilePicIV);
                //TextView
                reviewProfileNameTV = view.findViewById(R.id.reviewProfileNameTV);
                //EditText
                reportET = view.findViewById(R.id.reportET);
                //Button
                btn_submitReport = view.findViewById(R.id.btn_submitReport);
                btn_submitReport.setOnClickListener(ReviewReportFragment.this);

                if(rating != null){
                    if(rating.user.profilePicUrl != null){
                        GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(rating.user.profilePicUrl),reviewProfilePicIV);
                    }
                    if(rating.user.name != null){
                        reviewProfileNameTV.setText(rating.user.name);
                    }
                }
                else if(blogs != null){
                    btn_submitReport.setText(parentActivity.getResources().getString(R.string.btn_submit));
                    if(blogs.userId != null){
                        GeneralFunction.getFirestoreInstance()
                                .collection(User.URL_USER)
                                .document(blogs.userId)
                                .get()
                                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                    @Override
                                    public void onSuccess(DocumentSnapshot snapshot) {
                                        if(snapshot != null){
                                            if (snapshot.exists()) {
                                                User tempUser = snapshot.toObject(User.class);
                                                if(tempUser != null){
                                                    if(tempUser.profilePicUrl != null){
                                                        GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(tempUser.profilePicUrl),reviewProfilePicIV);
                                                    }
                                                    if(tempUser.name != null){
                                                        reviewProfileNameTV.setText(tempUser.name);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                GeneralFunction.openMessageNotificationDialog(parentActivity, e.toString(),R.drawable.notice_bad, TAG);
                            }
                        });
                    }
                }
            }
        });
    }


    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        KeyboardUtilities.hideSoftKeyboard(parentActivity);
    }

    @Override
    public void onResume() {
        super.onResume();
        Window window = getDialog().getWindow();
//        window.setLayout(1000, 650);
        if (window != null) {
            window.setGravity(Gravity.CENTER);
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_submitReport:
                submitReport();
                break;
        }
    }

    private void submitReport(){
        String report = reportET.getText().toString().trim();
        if(isValidateInput(report)){
            Map<String,Object> reportMap = new HashMap<>();
            if(rating != null){
                reportMap.put(Report.TARGET_UID, rating.user.id);
                reportMap.put(Report.DESCRIPTION_REPORT, report);
                reportMap.put(Report.TARGET_ID, rating.favourite.accommodationId);
                reportMap.put(Report.USER_ID, parentActivity.uid);
                reportMap.put(Report.REPORT_TYPE, Report.REPORT_TYPE_ROOM);
            }
            else if(blogs != null){
                reportMap.put(BlogDiscussion.DISCUSSION_ID, blogDiscussion.id);
                reportMap.put(BlogDiscussion.DESCRIPTION_DISCUSSION, report);
                reportMap.put(BlogDiscussion.BLOG_ID, blogs.id);
                reportMap.put(BlogDiscussion.USER_ID, parentActivity.uid);
                reportMap.put(BlogDiscussion.TYPE, BlogDiscussion.TYPE_REPLY);
            }

            reportMap.put(Report.IS_REVIEWED, false);
            reportMap.put(Report.DATE_CREATED, FieldValue.serverTimestamp());
            reportMap.put(Report.DATE_UPDATED,FieldValue.serverTimestamp());

            if(rating != null){
                reportRef = GeneralFunction.getFirestoreInstance()
                        .collection(Report.URL_REPORT)
                        .document();
            }
            else if (blogs != null){
                reportRef = GeneralFunction.getFirestoreInstance()
                        .collection(BlogDiscussion.URL_BLOG_DISCUSSION)
                        .document();
            }
            reportRef.set(reportMap)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                dismiss();
                                if(rating != null){
                                    GeneralFunction.Toast(parentActivity, parentActivity.getString(R.string.successful_submit_report));
                                }
                                else if(blogs != null){
                                    GeneralFunction.Toast(parentActivity, parentActivity.getString(R.string.blog_successful_submit_discussion));
                                }
                            }
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    if(e!= null){
                        GeneralFunction.Toast(parentActivity,e.toString());
                    }
                }
            });

        }

    }

    private boolean isValidateInput(String report){
        return GeneralFunction.checkAndReturnString(report,parentActivity.getString(R.string.input_report_edit_text),parentActivity);
    }
}
