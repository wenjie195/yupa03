package com.vidatechft.yupa.dialogFragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.GridView;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.MSNearbyStayAdapter;
import com.vidatechft.yupa.classes.Merchant;
import com.vidatechft.yupa.merchantShopFragment.MerchantMapFragment;
import com.vidatechft.yupa.utilities.GeneralFunction;
import com.vidatechft.yupa.utilities.RecycleViewGridView;

import java.util.ArrayList;
import java.util.List;

public class MSNearbyViewAllDialogFragment extends DialogFragment {
    public static final String TAG = MSNearbyViewAllDialogFragment.class.getName();

    private MainActivity parentActivity;
    private MerchantMapFragment parentFragment;
    private List<Merchant> merchants = new ArrayList<>();
    private boolean isViewAll;

    public MSNearbyViewAllDialogFragment() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    public static MSNearbyViewAllDialogFragment newInstance(MainActivity parentActivity, MerchantMapFragment parentFragment, ArrayList<Merchant> merchants, boolean isViewAll) {
        MSNearbyViewAllDialogFragment frag = new MSNearbyViewAllDialogFragment();
        frag.parentActivity = parentActivity;
        frag.parentFragment = parentFragment;
        frag.merchants = merchants;
        frag.isViewAll = isViewAll;
        return frag;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(true);
        getDialog().setCancelable(true);
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.rounded_acctype_white_bg_border_black);

        return inflater.inflate(R.layout.dialog_msnearby_view_all, container);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecycleViewGridView shopRV =  view.findViewById(R.id.shopRV);
        Button closeBtn = view.findViewById(R.id.closeBtn);

        MSNearbyStayAdapter msNearbyStayAdapter = new MSNearbyStayAdapter(parentActivity, parentFragment, merchants, isViewAll);

//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(parentActivity);
        shopRV.setAdapter(msNearbyStayAdapter);
        shopRV.setLayoutManager(new GridLayoutManager(parentActivity,2));
        msNearbyStayAdapter.notifyDataSetChanged();
//        shopRV.setHasFixedSize(false);


        if(!merchants.isEmpty()){
            msNearbyStayAdapter.notifyDataSetChanged();
        }else{
            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.merchant_err_loading_shops),R.drawable.notice_bad,TAG);
            getDialog().dismiss();
        }

        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        Window window = getDialog().getWindow();
//        window.setLayout(1000, 650);
        window.setGravity(Gravity.CENTER);
    }
}
