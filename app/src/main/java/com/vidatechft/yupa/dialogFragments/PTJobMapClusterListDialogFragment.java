package com.vidatechft.yupa.dialogFragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.PTJobNearbyListAdapter;
import com.vidatechft.yupa.classes.GlideApp;
import com.vidatechft.yupa.classes.Job;
import com.vidatechft.yupa.employerFragments.PTJobSwipeFragment;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.List;

import static com.bumptech.glide.load.DecodeFormat.PREFER_RGB_565;

public class PTJobMapClusterListDialogFragment extends DialogFragment {
    public static final String TAG = PTJobMapClusterListDialogFragment.class.getName();

    private MainActivity parentActivity;
    private ArrayList<Job> jobs;
    private PTJobSwipeFragment parentFragment;
    private String lastToolbarTitle;

    public PTJobMapClusterListDialogFragment() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    public static PTJobMapClusterListDialogFragment newInstance(MainActivity parentActivity, PTJobSwipeFragment parentFragment, String lastToolbarTitle, ArrayList<Job> jobs) {
        PTJobMapClusterListDialogFragment frag = new PTJobMapClusterListDialogFragment();
        frag.parentActivity = parentActivity;
        frag.parentFragment = parentFragment;
        frag.lastToolbarTitle = lastToolbarTitle;
        frag.jobs = jobs;
        return frag;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(true);
        getDialog().setCancelable(true);
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.rounded_acctype_white_bg_border_black);

        return inflater.inflate(R.layout.dialog_ptjob_map_cluster_list, container);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView jobRV =  view.findViewById(R.id.jobRV);

        PTJobNearbyListAdapter ptJobNearbyListAdapter = new PTJobNearbyListAdapter(parentActivity,parentFragment,lastToolbarTitle,jobs);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(parentActivity);
        jobRV.setAdapter(ptJobNearbyListAdapter);
        jobRV.setLayoutManager(linearLayoutManager);

        if(!jobs.isEmpty()){
            ptJobNearbyListAdapter.notifyDataSetChanged();
        }else{
            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.pt_err_loading_jobs_map),R.drawable.notice_bad,TAG);
            getDialog().dismiss();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        Window window = getDialog().getWindow();
//        window.setLayout(1000, 650);
        window.setGravity(Gravity.CENTER);
    }
}
