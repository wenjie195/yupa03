package com.vidatechft.yupa.dialogFragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import com.google.firebase.firestore.FieldValue;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.BookHomestayDetails;
import com.vidatechft.yupa.classes.BookingHistory;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.hostFragments.HostStayMainPageFragment;
import com.vidatechft.yupa.utilities.GeneralFunction;
import com.vidatechft.yupa.utilities.KeyboardUtilities;

import java.util.HashMap;

public class SubmitMessageToHostDialogFragment extends DialogFragment {
    public static final String TAG = SubmitMessageToHostDialogFragment.class.getName();
    private MainActivity parentActivity;
    private BookingHistory bookingHistory;
    private Room room;

    public SubmitMessageToHostDialogFragment() {

    }

    public static SubmitMessageToHostDialogFragment newInstance(MainActivity parentActivity, BookingHistory bookingHistory, Room room) {
        SubmitMessageToHostDialogFragment frag = new SubmitMessageToHostDialogFragment();
        frag.parentActivity = parentActivity;
        frag.bookingHistory = bookingHistory;
        frag.room = room;

        return frag;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(true);
        getDialog().setCancelable(true);
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.rounded_white);
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        return inflater.inflate(R.layout.dialog_submit_msg_to_host, container);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final EditText msgET = view.findViewById(R.id.msgET);
                Button viewBtn = view.findViewById(R.id.viewBtn);
                Button submitBtn = view.findViewById(R.id.submitBtn);

                viewBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Fragment hostStayMainPageFragment = HostStayMainPageFragment.newInstance(parentActivity,parentActivity.getString(R.string.stays),room,room.id);
                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, hostStayMainPageFragment).addToBackStack(HostStayMainPageFragment.TAG).commit();
                        dismiss();
                    }
                });

                submitBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        HashMap<String,Object> bookHomestayMap = new HashMap<>();
                        bookHomestayMap.put(BookingHistory.PURPOSE,msgET.getText().toString().trim());
                        bookHomestayMap.put(BookingHistory.DATE_UPDATED,FieldValue.serverTimestamp());

                        if(bookingHistory != null && bookingHistory.id != null){
                            GeneralFunction.getFirestoreInstance()
                                    .collection(BookHomestayDetails.URL_BOOK_GUEST)
                                    .document(bookingHistory.id)
                                    .update(bookHomestayMap);
                        }

                        GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.message_sent),R.drawable.notice_good,TAG);
                        dismiss();
                    }
                });
            }
        });
    }


    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        KeyboardUtilities.hideSoftKeyboard(parentActivity);
    }

    @Override
    public void onResume() {
        super.onResume();
        Window window = getDialog().getWindow();
//        window.setLayout(1000, 650);
        if (window != null) {
            window.setGravity(Gravity.CENTER);
        }
    }
}
