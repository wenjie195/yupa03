package com.vidatechft.yupa.dialogFragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.vidatechft.yupa.R;

public class LoadingProgressDialogFragment extends DialogFragment {

    private String message;
    private Boolean isAllowCancel;
    private AppCompatActivity parentActivity;
    private int noOfDots = 0;
    private Handler handler;
    private Runnable runnable;
    private TextView messageTV;

    public LoadingProgressDialogFragment() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    public static LoadingProgressDialogFragment newInstance(String message, Boolean isAllowCancel, AppCompatActivity parentActivity) {
        LoadingProgressDialogFragment frag = new LoadingProgressDialogFragment();
        frag.message = message;
        frag.isAllowCancel = isAllowCancel;
        frag.parentActivity = parentActivity;
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

                if(isAllowCancel){
                    getDialog().setCanceledOnTouchOutside(true);
                }else{
                    getDialog().setCanceledOnTouchOutside(false);
                }
            }
        });
        return inflater.inflate(R.layout.dialog_progress_bar, container);
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                messageTV =  view.findViewById(R.id.messageTV);
                Button cancelBtn = view.findViewById(R.id.cancelBtn);

                messageTV.setText(message + "\n .");

                if(isAllowCancel){
                    cancelBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            getDialog().dismiss();
                        }
                    });
                }else{
                    cancelBtn.setVisibility(View.GONE);
                }
            }
        });
    }

    private void startLoading(){
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                noOfDots++;

                switch (noOfDots){
                    case 0: messageTV.setText(message + "\n ."); break;
                    case 1: messageTV.setText(message + "\n . ."); break;
                    case 2: messageTV.setText(message + "\n . . ."); break;
                    case 3: messageTV.setText(message + "\n . . . ."); break;
                    case 4: messageTV.setText(message + "\n . . . . ."); break;
                    default: messageTV.setText(message + "\n ."); noOfDots = 0; break;
                }

                handler.postDelayed(this,1000);
            }
        };
        handler.postDelayed(runnable, 1000);
    }

    private void stopLoading(){
        if(handler != null){
            handler.removeCallbacksAndMessages(null); // remove everything including pending callbacks and messages
        }
        handler = null;
        runnable = null;
    }

    public void setLoadingMessage(String newMessage){
        message = newMessage;
        messageTV.setText(message + "\n .");
    }

    @Override
    public void onPause() {
        stopLoading();
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Window window = getDialog().getWindow();
//        window.setLayout(1000, 650);
                window.setGravity(Gravity.CENTER);

                startLoading();
            }
        });
    }
}
