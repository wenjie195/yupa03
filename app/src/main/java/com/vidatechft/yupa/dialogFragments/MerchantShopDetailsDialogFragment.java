package com.vidatechft.yupa.dialogFragments;

import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Html;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.BusinessHourTime;
import com.vidatechft.yupa.classes.GlideApp;
import com.vidatechft.yupa.classes.Merchant;
import com.vidatechft.yupa.merchantShopFragment.ApplyAsMerchantFragment;
import com.vidatechft.yupa.merchantShopFragment.MerchantMapFragment;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.vidatechft.yupa.merchantShopFragment.BusinessHourDialogFragment.NUM_OF_WEEKDAYS;

public class MerchantShopDetailsDialogFragment extends DialogFragment {
    public static final String TAG = MerchantShopDetailsDialogFragment.class.getName();
    private MainActivity parentActivity;
    private Merchant merchant;
    private ImageView shopImg;
    private TextView shopNameTV,shopAddTV,shopDescTV,shopBHTV;

    public MerchantShopDetailsDialogFragment(){
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    public static MerchantShopDetailsDialogFragment newInstance(MainActivity parentActivity,Merchant merchant){
        MerchantShopDetailsDialogFragment fragment = new MerchantShopDetailsDialogFragment();

        fragment.parentActivity = parentActivity;
        fragment.merchant = merchant;
        return fragment;
    }

//    public static MerchantShopDetailsDialogFragment newInstance(MainActivity parentActivity, MerchantMapFragment parentFragment, String lastToolbarTitle, Merchant merchant) {
//        MerchantShopDetailsDialogFragment fragment = new MerchantShopDetailsDialogFragment();
//
//        fragment.parentActivity = parentActivity;
//        fragment.parentFragment = parentFragment;
//        fragment.merchant = merchant;
//        return fragment;
//    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(true);
        getDialog().setCancelable(true);
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.rounded_white);

        return inflater.inflate(R.layout.dialog_merchant_shop_details, container);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Button closeBtn = view.findViewById(R.id.closeBtn);
                shopImg = view.findViewById(R.id.shopImg);
                shopNameTV = view.findViewById(R.id.shopNameTV);
                shopAddTV = view.findViewById(R.id.shopAddTV);
                shopDescTV = view.findViewById(R.id.shopDescTV);
                shopBHTV = view.findViewById(R.id.shopBHTV);

                if(merchant != null){
                    if(merchant.workPlace != null){
                        shopNameTV.setText(String.valueOf(merchant.workPlace));
                    }

                    if(merchant.workPlaceAdd != null){
                        shopAddTV.setText(String.valueOf(merchant.workPlaceAdd));
                    }

                    if(merchant.workPlace == null && merchant.workPlaceAdd == null){
                        shopNameTV.setText(parentActivity.getString(R.string.none));
                    }

                    if(merchant.shopPicUrl != null) {
                        GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(merchant.shopPicUrl),shopImg);
//                        GlideApp.with(MerchantShopDetailsDialogFragment.this).load(merchant.shopPicUrl).centerCrop().into(shopImg);
                    }

                    if (merchant.description != null){
                        shopDescTV.setText(String.valueOf(merchant.description));
                    }

                    if (merchant.businessHourTimes !=null){
                        StringBuilder stringBuilder = new StringBuilder();
                        for (int i = 0; i < NUM_OF_WEEKDAYS; i++){
                            switch (i){
                                case 0:
                                    stringBuilder.append("<b>Monday</b> </br>");
                                    break;
                                case 1:
                                    stringBuilder.append("<b> <br> Tuesday </b> </br> ");
                                    break;
                                case 2:
                                    stringBuilder.append("<b> <br> Wednesday </b> </br>");
                                    break;
                                case 3:
                                    stringBuilder.append("<b> <br> Thursday </b> </br>");
                                    break;
                                case 4:
                                    stringBuilder.append("<b> <br> Friday </b> </br>");
                                    break;
                                case 5:
                                    stringBuilder.append("<b> <br> Saturday </b> </br>");
                                    break;
                                case 6:
                                    stringBuilder.append("<b> <br> Sunday </b> </br>");
                                    break;
                            }

                            if(merchant.businessHourTimes.get(i) != null && !merchant.businessHourTimes.get(i).isEmpty()){
                                for(BusinessHourTime tempBHT: merchant.businessHourTimes.get(i)){
                                    stringBuilder.append("<p>");
                                    int tempOpenHour = tempBHT.getOpenHour();
                                    int tempOpenMin = tempBHT.getOpenMin();
                                    stringBuilder.append(String.format(Locale.US,"%02d:%02d %s", tempOpenHour == 12 || tempOpenHour == 0 ? 12 : tempOpenHour % 12, tempOpenMin, tempOpenHour < 12 ? "am" : "pm")+ "&#160;&#160");

                                    stringBuilder.append(" - " );
                                    stringBuilder.append(" &#160 " );
                                    int tempCloseHour = tempBHT.getCloseHour();
                                    int tempCloseMin = tempBHT.getCloseMin();
                                    stringBuilder.append(String.format(Locale.US,"%02d:%02d %s", tempCloseHour == 12 || tempCloseHour == 0 ? 12 : tempCloseHour % 12, tempCloseMin, tempCloseHour < 12 ? "am" : "pm")+ "&#160;&#160");
                                    stringBuilder.append(" <br> " );

                                }
                            }else{
                                stringBuilder.append("<br> Closed <br>");
                            }


                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                shopBHTV.setText(Html.fromHtml(stringBuilder.toString(), Html.FROM_HTML_MODE_COMPACT));
                            }else{
                                shopBHTV.setText(Html.fromHtml(stringBuilder.toString()));
                            }
                        }
                    }


                }

                closeBtn.setOnClickListener(new DebouncedOnClickListener(500) {
                    @Override
                    public void onDebouncedClick(View v) {
                        getDialog().dismiss();
                    }
                });

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        Window window = getDialog().getWindow();
//        window.setLayout(1000, 650);
        window.setGravity(Gravity.CENTER);
    }
}
