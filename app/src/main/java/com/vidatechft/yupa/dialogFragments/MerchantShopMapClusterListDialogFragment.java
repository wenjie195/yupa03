package com.vidatechft.yupa.dialogFragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.MSNearbyStayAdapter;
import com.vidatechft.yupa.adapter.MSNearbyStayListAdapter;
import com.vidatechft.yupa.adapter.PTJobNearbyListAdapter;
import com.vidatechft.yupa.classes.Job;
import com.vidatechft.yupa.classes.Merchant;
import com.vidatechft.yupa.employerFragments.PTJobSwipeFragment;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.List;

public class MerchantShopMapClusterListDialogFragment extends DialogFragment {
    public static final String TAG = MerchantShopMapClusterListDialogFragment.class.getName();

    private MainActivity parentActivity;
    private List<Merchant> merchants = new ArrayList<>();

    public MerchantShopMapClusterListDialogFragment() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    public static MerchantShopMapClusterListDialogFragment newInstance(MainActivity parentActivity,ArrayList<Merchant> merchants) {
        MerchantShopMapClusterListDialogFragment frag = new MerchantShopMapClusterListDialogFragment();
        frag.parentActivity = parentActivity;
        frag.merchants = merchants;
        return frag;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(true);
        getDialog().setCancelable(true);
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.rounded_acctype_white_bg_border_black);

        return inflater.inflate(R.layout.dialog_merchantshop_map_cluster_list, container);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView shopRV =  view.findViewById(R.id.shopRV);

        MSNearbyStayListAdapter msNearbyStayListAdapter = new MSNearbyStayListAdapter(parentActivity,merchants);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(parentActivity);
        shopRV.setAdapter(msNearbyStayListAdapter);
        shopRV.setLayoutManager(linearLayoutManager);

        if(!merchants.isEmpty()){
            msNearbyStayListAdapter.notifyDataSetChanged();
        }else{
            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.pt_err_loading_jobs_map),R.drawable.notice_bad,TAG);
            getDialog().dismiss();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        Window window = getDialog().getWindow();
//        window.setLayout(1000, 650);
        window.setGravity(Gravity.CENTER);
    }
}
