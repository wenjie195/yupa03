package com.vidatechft.yupa.dialogFragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.AppliedCandidate;
import com.vidatechft.yupa.classes.GlideApp;
import com.vidatechft.yupa.classes.Job;
import com.vidatechft.yupa.employerFragments.PTJobMapFragment;
import com.vidatechft.yupa.employerFragments.PTJobSwipeFragment;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;
import com.vidatechft.yupa.utilities.KeyboardUtilities;

import java.util.ArrayList;

import static com.bumptech.glide.load.DecodeFormat.PREFER_RGB_565;

public class ApplyPTJobDialogFragment extends DialogFragment {
    public static final String TAG = ApplyPTJobDialogFragment.class.getName();

    private MainActivity parentActivity;
    private PTJobSwipeFragment parentFragment;
    private AppliedCandidate appliedCandidate;
    private Job job;
    private String lastToolbarTitle;
    private ArrayList<Job> jobs;
    private boolean needViewNearby;

    private EditText descET;

    public ApplyPTJobDialogFragment() {

    }

    public static ApplyPTJobDialogFragment newInstance(MainActivity parentActivity, PTJobSwipeFragment parentFragment, String lastToolbarTitle, AppliedCandidate appliedCandidate, Job job, ArrayList<Job> jobs, boolean needViewNearby) {
        ApplyPTJobDialogFragment frag = new ApplyPTJobDialogFragment();
        frag.parentActivity = parentActivity;
        frag.parentFragment = parentFragment;
        frag.lastToolbarTitle = lastToolbarTitle;
        frag.appliedCandidate = appliedCandidate;
        frag.job = job;
        frag.jobs = jobs;
        frag.needViewNearby = needViewNearby;
        return frag;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(true);
        getDialog().setCancelable(true);
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.rounded_white);
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        return inflater.inflate(R.layout.dialog_apply_for_ptjob, container);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final TextView wordCountTV =  view.findViewById(R.id.wordCountTV);
                descET = view.findViewById(R.id.descET);
                Button sendBtn = view.findViewById(R.id.sendBtn);
                Button viewBtn = view.findViewById(R.id.viewBtn);

                TextView jobscopeTV = view.findViewById(R.id.jobscopeTV);
                TextView earnTV = view.findViewById(R.id.earnTV);
                TextView workspaceTV = view.findViewById(R.id.workspaceTV);
                TextView locationTV = view.findViewById(R.id.locationTV);

                if(job != null){
                    if(job.jobScope != null){
                        jobscopeTV.setText(GeneralFunction.formatList(job.jobScope,true));
                    }else{
                        jobscopeTV.setText(parentActivity.getString(R.string.none));
                    }

                    if(job.earnList != null){
                        earnTV.setText(GeneralFunction.formatList(job.earnList,true));
                    }else{
                        earnTV.setText(parentActivity.getString(R.string.none));
                    }

                    if(job.workspaceName != null){
                        workspaceTV.setText(String.valueOf(job.workspaceName));
                    }

                    if(job.location != null){
                        locationTV.setText(String.valueOf(job.location));
                    }

                    if(job.workspaceName == null && job.location == null){
                        workspaceTV.setText(parentActivity.getString(R.string.none));
                    }

                }

                descET.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        wordCountTV.setText(String.valueOf(s.length()));
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

                sendBtn.setOnClickListener(new DebouncedOnClickListener(500) {
                    @Override
                    public void onDebouncedClick(View v) {
                        setApplyCandidateDetails();
                        KeyboardUtilities.hideSoftKeyboard(parentActivity);
                        getDialog().dismiss();
                    }
                });

                if(needViewNearby){
                    viewBtn.setVisibility(View.VISIBLE);
                    viewBtn.setOnClickListener(new DebouncedOnClickListener(500) {
                        @Override
                        public void onDebouncedClick(View v) {
                            KeyboardUtilities.hideSoftKeyboard(parentActivity);
                            getDialog().dismiss();
                            Fragment ptjobMapFragment = PTJobMapFragment.newInstance(parentActivity,parentFragment,lastToolbarTitle,jobs);
                            GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, ptjobMapFragment).addToBackStack(PTJobMapFragment.TAG).commit();
                        }
                    });
                }else{
                    viewBtn.setVisibility(View.GONE);
                }

            }
        });
    }

    private void setApplyCandidateDetails(){
        if(GeneralFunction.isGuest()){
            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_guest_restrict_usage),R.drawable.notice_bad,TAG);
            return;
        }
        if(!descET.getText().toString().trim().isEmpty()){
            appliedCandidate.desc = descET.getText().toString().replaceAll("\n+"," ").trim();
        }else{
            appliedCandidate.desc = "";
        }

        GeneralFunction.getFirestoreInstance()
                .collection(Job.URL_JOB)
                .document(job.id)
                .collection(AppliedCandidate.URL_CANDIDATE)
                .document(parentActivity.uid)
                .set(appliedCandidate);

        //didnt want to use the dialog fragment to prevent user getting annoyed and not putting this toast
        //inside onComplete is because i want let user know instantly
        Toast.makeText(parentActivity,parentActivity.getString(R.string.pt_application_sent),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        KeyboardUtilities.hideSoftKeyboard(parentActivity);
    }

    @Override
    public void onResume() {
        super.onResume();

        Window window = getDialog().getWindow();
//        window.setLayout(1000, 650);
        window.setGravity(Gravity.CENTER);
    }
}
