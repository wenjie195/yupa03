package com.vidatechft.yupa.holder;

import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.SelectableItem;

public class SelectableViewHolder extends RecyclerView.ViewHolder {

    public static final int MULTI_SELECTION = 2;
    public static final int SINGLE_SELECTION = 1;
    public MainActivity parentActivity;
    public SelectableItem mItem;
    public final OnItemSelectedListener itemSelectedListener;
    public final ConstraintLayout wholeLL;
    public final ImageView carRentalIV;
    public final TextView carNameTV,carYearTV,carRentalPerHourTV,carRentalPerDayTV,carRentalPerMonthTV;
    public final View viewLine;


    public SelectableViewHolder(View view, OnItemSelectedListener listener) {
        super(view);
        itemSelectedListener = listener;
        wholeLL = itemView.findViewById(R.id.wholeLL);
        carRentalIV = itemView.findViewById(R.id.carRentalIV);
        carNameTV = itemView.findViewById(R.id.carNameTV);
        carYearTV = itemView.findViewById(R.id.carYearTV);
        carRentalPerHourTV = itemView.findViewById(R.id.carRentalPerHourTV);
        carRentalPerDayTV = itemView.findViewById(R.id.carRentalPerDayTV);
        carRentalPerMonthTV = itemView.findViewById(R.id.carRentalPerMonthTV);
        viewLine = itemView.findViewById(R.id.viewLine);
        wholeLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (mItem.isSelected() && getItemViewType() == SINGLE_SELECTION) {
                    setChecked(false);
                } else {
                    setChecked(true);
                }
                itemSelectedListener.onItemSelected(mItem);

            }
        });
    }

    public void setChecked(boolean value) {
        if (value) {
            wholeLL.setBackgroundColor(parentActivity.getResources().getColor(R.color.unread_notification));
        } else {
            wholeLL.setBackgroundColor(parentActivity.getResources().getColor(R.color.white));
        }
        mItem.setSelected(value);
    }

    public interface OnItemSelectedListener {

        void onItemSelected(SelectableItem item);
    }

}