package com.vidatechft.yupa.profileSetting;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;

//from here https://medium.com/@JakobUlbrich/building-a-settings-screen-for-android-part-2-2ba63e2d7d1d
//and use the shared preference value from here https://medium.com/@JakobUlbrich/you-can-always-check-the-current-value-with-the-following-ed650489c7fd
public class ProfileSettingFragment extends PreferenceFragmentCompat {
    //todo <!--this is a temporary thing to fix the bug that the PreferenceScreen has allocated a default icon space to it. (will change to androidx in the future since it is fixed in latest androidx library) from here https://stackoverflow.com/questions/51518758/preferencefragmentcompat-has-padding-on-preferencecategory-that-i-cant-get-rid-->
    public static final String TAG = ProfileSettingFragment.class.getName();

    public static final String SETTING_TRACKING = "setting_allow_tracking";

    private MainActivity parentActivity;

    public static ProfileSettingFragment newInstance(MainActivity parentActivity) {
        ProfileSettingFragment fragment = new ProfileSettingFragment();

        fragment.parentActivity = parentActivity;

        return fragment;
    }

    @Override
    public void onCreatePreferences(Bundle bundle, String s) {
        // Load the Preferences from the XML file
        addPreferencesFromResource(R.xml.app_preferences);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if(view != null){
            view.setBackgroundColor(getResources().getColor(android.R.color.white));
        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.selectHighlights();
        parentActivity.setToolbarTitle(parentActivity.getString(R.string.nav_setting));
    }

    //    todo get the value using line below
//    SharedPreferences prefs =
//            PreferenceManager.getDefaultSharedPreferences(this);
//    boolean myString = prefs.getBoolean("setting_allow_tracking", false);
//    Toast.makeText(parentActivity,String.valueOf(myString),Toast.LENGTH_SHORT).show();
}
