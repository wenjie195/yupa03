package com.vidatechft.yupa.utilities;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Selection;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;

import com.vidatechft.yupa.R;

import java.util.ArrayList;

//from here https://stackoverflow.com/questions/11318551/creating-a-custom-edittext-with-tag-like-feature
public class TagEditText extends android.support.v7.widget.AppCompatMultiAutoCompleteTextView {
    /*************************************************EXAMPLE USAGE*****************************************************/
//    private void setupTagEditText(){
//        parentActivity.runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                cuisineTET.setParentActivity(parentActivity);
//
//                ArrayAdapter<String> cuisinelistAdapter = new ArrayAdapter<>(parentActivity,
//                        android.R.layout.simple_dropdown_item_1line , getResources().getStringArray(R.array.cuisine_list));
//
//                cuisineTET.setAdapter(cuisinelistAdapter);
//                cuisineTET.setThreshold(1);
//                cuisineTET.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
//                cuisineTET.setLimit(10);
//
//
//                activityTET.setParentActivity(parentActivity);
//
//                ArrayAdapter<String> activitylistAdapter = new ArrayAdapter<>(parentActivity,
//                        android.R.layout.simple_dropdown_item_1line , getResources().getStringArray(R.array.editProfileHobby));
//
//                activityTET.setAdapter(activitylistAdapter);
//                activityTET.setThreshold(1);
//                activityTET.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
//                activityTET.setLimit(10);
//            }
//        });
//    }

    //************************THEN TO USE IT JUST DO LIKE THIS*************************
//    for(String tag : cuisineTET.getTaglist()){
//        itiMap.put(tag.trim(),true);
//    }
//
//    for(String tag : activityTET.getTaglist()){
//        itiMap.put(tag.trim(),true);
//    }

    //************************THIS IS TO POPULATE EXISTING STRINGS INTO THIS TAG EDIT TEXT AND FORMAT IT INTO A TAG*************************
//    cuisineTET.setText(GeneralFunction.formatList(oriItinerary.cuisineList,false));
//    cuisineTET.formatIntoTag(false);
    /*************************************************EXAMPLE USAGE*****************************************************/

    TextWatcher textWatcher;

    String lastString;

    String separator = ",";

    int limit = 5;
    int lastCharacterPosition = 0;

    AppCompatActivity parentActivity;

    ArrayList<String> taglist = new ArrayList<>();

    public TagEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        setMovementMethod(LinkMovementMethod.getInstance());

        textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(count == 0){
                    formatIntoTag(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
//                String thisString = s.toString();
//                if (thisString.length() > 0 && !thisString.equals(lastString)) {
//                    formatIntoTag();
//                }
            }
        };

        addTextChangedListener(textWatcher);

        setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                formatIntoTag(false);
            }
        });

        setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    fixCursorNotShowingBug();
                }
            }
        });

        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                fixCursorNotShowingBug();
            }
        });
    }

    private void fixCursorNotShowingBug(){
        if(length() == 0){
            setText(" ");
            setSelection(1);
//            Selection.removeSelection(getText()); //doesnt work
        }
    }

    public void formatIntoTag(boolean isDeleteClicked) {
        fixCursorNotShowingBug();
        if(!isDeleteClicked && isExceededTagLimit() && getText().length() > lastCharacterPosition + 1){
            getText().delete(lastCharacterPosition + 1, getText().length());
            return;
        }

        taglist.clear();

        SpannableStringBuilder sb = new SpannableStringBuilder();
        String fullString = getText().toString();

        if(fullString.length() <= 0){
            return;
        }

        String[] strings = fullString.split(separator);


        for (int i = 0; i < strings.length; i++) {

            String string = strings[i];
            sb.append(string);

            if (fullString.charAt(fullString.length() - 1) != separator.charAt(0) && i == strings.length - 1) {
                break;
            }

            BitmapDrawable bd = (BitmapDrawable) convertViewToDrawable(createTokenView(string));
            bd.setBounds(0, 0, bd.getIntrinsicWidth(), bd.getIntrinsicHeight());

            int startIdx = sb.length() - (string.length());
            int endIdx = sb.length();

            sb.setSpan(new ImageSpan(bd), startIdx, endIdx, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            MyClickableSpan myClickableSpan = new MyClickableSpan(startIdx, endIdx, taglist.size());
            sb.setSpan(myClickableSpan, Math.max(endIdx-2, startIdx), endIdx, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            if (i < strings.length - 1) {
                sb.append(separator);
            } else if (fullString.charAt(fullString.length() - 1) == separator.charAt(0)) {
                sb.append(separator);
            }

            taglist.add(string);
            lastCharacterPosition = getText().length() - 1;
        }


        lastString = sb.toString();

        setText(sb);
        setSelection(sb.length());

    }

    private boolean isExceededTagLimit(){
        if(parentActivity == null){
            return true;
        }

        if(taglist.size() >= limit){
            GeneralFunction.openMessageNotificationDialog(
                    parentActivity
                    ,parentActivity.getString(R.string.error_exceed_tag_limit,limit)
                    ,R.drawable.notice_bad
                    ,"TAG_EDIT_TEXT");
            return true;
        }else{
            return false;
        }
    }

    public void setParentActivity(AppCompatActivity parentActivity) {
        this.parentActivity = parentActivity;
    }

    public View createTokenView(String text) {
//        LinearLayout l = (LinearLayout)
//        l.setOrientation(LinearLayout.HORIZONTAL);
//        l.setBackgroundResource(R.drawable.rounded_acctype_blue_bg);
//
//        TextView tv = new TextView(getContext());
//        l.addView(tv);
//        tv.setPadding(15,15,15,15);
//        tv.setText(text);
//        tv.setGravity(Gravity.CENTER);
//        tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);
//
//        ImageView im = new ImageView(getContext());
//        l.addView(im);
//        im.setImageResource(R.drawable.ic_close_black_24dp);
//        im.setScaleType(ImageView.ScaleType.CENTER_CROP);

        View view = LinearLayout.inflate(getContext(),R.layout.layout_tag_edit_text,null);
        TextView tv = view.findViewById(R.id.tagET);
        tv.setText(text);

        return view;
    }

    public Object convertViewToDrawable(View view) {
        int spec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        view.measure(spec, spec);
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());

        Bitmap b = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas c = new Canvas(b);

        c.translate(-view.getScrollX(), -view.getScrollY());
        view.draw(c);
        view.setDrawingCacheEnabled(true);
        Bitmap cacheBmp = view.getDrawingCache();
        Bitmap viewBmp = cacheBmp.copy(Bitmap.Config.ARGB_8888, true);
        view.destroyDrawingCache();
        return new BitmapDrawable(getContext().getResources(), viewBmp);
    }

    public ArrayList<String> getTaglist() {
        return taglist;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    private class MyClickableSpan extends ClickableSpan {

        int startIdx;
        int endIdx;
        int position;

        public MyClickableSpan(int startIdx, int endIdx, int position) {
            super();
            this.startIdx = startIdx;
            this.endIdx = endIdx;
            this.position = position;
        }

        @Override
        public void onClick(View widget) {
            String s = getText().toString();

            String s1 = s.substring(0, startIdx);
            String s2 = s.substring(Math.min(endIdx+1, s.length()-1) + 1, s.length() );

            TagEditText.this.setText(s1 + s2);

            formatIntoTag(true);
        }

    }
}
