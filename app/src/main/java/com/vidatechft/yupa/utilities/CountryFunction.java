package com.vidatechft.yupa.utilities;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.vidatechft.yupa.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CountryFunction {
    public static final String TAG = CountryFunction.class.getName();
    public static ArrayList<HashMap<String, String>> getCountriesFromJSONFile(AppCompatActivity parentActivity) {
        ArrayList<HashMap<String, String>> countryList = new ArrayList<HashMap<String, String>>();
        try {
            JSONObject obj = new JSONObject(GeneralFunction.loadJSONFromAsset(Config.COUNTRY_JSON_FILE, parentActivity));
            JSONArray countryArray = obj.getJSONArray(Config.COUNTRY_ARRAY);
            HashMap<String, String> countryMap;

            for (int i = 0; i < countryArray.length(); i++) {
                JSONObject jo_inside = countryArray.getJSONObject(i);
//                Log.d("Details-->", jo_inside.getString(Config.COUNTRY_SORT_NAME_JSON));

                String countrySortName = jo_inside.getString(Config.COUNTRY_SORT_NAME_JSON);
                String countryName = jo_inside.getString(Config.COUNTRY_NAME_JSON);
                String countryCode = jo_inside.getString(Config.COUNTRY_CODE_JSON);
                String countryId = jo_inside.getString(Config.COUNTRY_ID_JSON);

                //Add your values in your `ArrayList` as below:
                countryMap = new HashMap<String, String>();
                countryMap.put(Config.COUNTRY_SORT_NAME, countrySortName);
                countryMap.put(Config.COUNTRY_NAME, countryName);
                countryMap.put(Config.COUNTRY_CODE, countryCode);
                countryMap.put(Config.COUNTRY_ID, countryId);

                countryList.add(countryMap);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return countryList;
    }

    public static ArrayList<String> getCountry(AppCompatActivity parentActivity){

        ArrayList<HashMap<String,String>> allCountriesMap = getCountriesFromJSONFile(parentActivity);
        ArrayList<String> tempCountryList = new ArrayList<>();

        for (HashMap<String, String> map : allCountriesMap){
            for (Map.Entry<String, String> mapEntry : map.entrySet())
            {
                String key = mapEntry.getKey();
                String value = mapEntry.getValue();
                if(key.equals(Config.COUNTRY_NAME)){
                    tempCountryList.add(value);
                }
            }
        }
        return tempCountryList;
    }

    public static boolean isCountryExist(String countryName, AppCompatActivity parentActivity){
        Boolean isExist = null;
        if(getCountry(parentActivity) != null){
            for(String mCountryName : getCountry(parentActivity)){
                if(countryName.equals(mCountryName)){
                    isExist = true;
                }
            }
        }
        if(isExist != null){
            return true;
        }
        else{
            return false;
        }
    }

    public static String getCountryCodeWithName(String countryName, AppCompatActivity parentActivity){

        String countryCode = null;

        if(getCountryNameAndCode(parentActivity) != null){
            for (Map.Entry<String, String> mapEntry : getCountryNameAndCode(parentActivity).entrySet()){
                String key = mapEntry.getKey();
                String value = mapEntry.getValue();
                if(key.equals(countryName)){
                    countryCode = value;
                }
            }
            return countryCode;
        }
        else{
            return null;
        }
    }

    public static String getCountryCodeWithSortName(String countryName, AppCompatActivity parentActivity){

        String countryCode = null;

        if(getCountry(parentActivity) != null){
            for (Map.Entry<String, String> mapEntry : getCountryNameAndSortName(parentActivity).entrySet()){
                String key = mapEntry.getKey();
                String value = mapEntry.getValue();
                if(key.equals(countryName)){
                    countryCode = value;
                }
            }
            return countryCode;
        }
        else{
            return null;
        }
    }

    public static HashMap<String,String> getCountryNameAndCode(AppCompatActivity parentActivity){

        ArrayList<HashMap<String,String>> allCountriesMap = getCountriesFromJSONFile(parentActivity);
        HashMap<String,String> tempCountryCodeMap = new HashMap<>();
        String name = null;
        String code = null;
        for (HashMap<String, String> map : allCountriesMap){
            for (Map.Entry<String, String> mapEntry : map.entrySet())
            {
                String key = mapEntry.getKey();
                String value = mapEntry.getValue();
                if(key.equals(Config.COUNTRY_NAME)){
                    name = value;
                }
                if(key.equals(Config.COUNTRY_CODE)){
                    code = value;
                }
                if(name != null && code != null){
                    tempCountryCodeMap.put(name,code);
                    name = null;
                    code = null;
                }
            }
        }
        return tempCountryCodeMap;
    }

    public static ArrayList<String> getCountryCode(AppCompatActivity parentActivity){

        ArrayList<HashMap<String,String>> allCountriesMap = getCountriesFromJSONFile(parentActivity);
        ArrayList<String> tempCountryCodeList = new ArrayList<>();
        for (HashMap<String, String> map : allCountriesMap){
            for (Map.Entry<String, String> mapEntry : map.entrySet())
            {
                String key = mapEntry.getKey();
                String value = mapEntry.getValue();
                if(key.equals(Config.COUNTRY_CODE)){
                    tempCountryCodeList.add(value);
                }
            }
        }
        return tempCountryCodeList;
    }

    public static ArrayList<String> getCountrySortNameAndCodeList(AppCompatActivity parentActivity){
        ArrayList<HashMap<String,String>> allCountriesMap = getCountriesFromJSONFile(parentActivity);
        ArrayList<String> tempCountrySortNameCodeList = new ArrayList<>();
        String code = null;
        String sortName = null;
        for (HashMap<String, String> map : allCountriesMap){
            for (Map.Entry<String, String> mapEntry : map.entrySet())
            {
                String key = mapEntry.getKey();
                String value = mapEntry.getValue();
                if(key.equals(Config.COUNTRY_CODE)){
                    code = value;
                }
                if(key.equals(Config.COUNTRY_SORT_NAME)){
                    sortName = value;
                }
                if(sortName != null && code != null){
                    String sortNameCode = sortName + " +" + code;
                    tempCountrySortNameCodeList.add(sortNameCode);
                    code = null;
                    sortName = null;
                }
            }
        }
        return tempCountrySortNameCodeList;
    }

    public static HashMap<String,String> getCountrySortNameAndCodeMap(AppCompatActivity parentActivity){
        ArrayList<HashMap<String,String>> allCountriesMap = getCountriesFromJSONFile(parentActivity);
        HashMap<String,String> tempCountrySortNameAndCodeMap = new HashMap<>();
        String code = null;
        String sortName = null;
        for (HashMap<String, String> map : allCountriesMap){
            for (Map.Entry<String, String> mapEntry : map.entrySet())
            {
                String key = mapEntry.getKey();
                String value = mapEntry.getValue();
                if(key.equals(Config.COUNTRY_CODE)){
                    code = value;
                }
                if(key.equals(Config.COUNTRY_SORT_NAME)){
                    sortName = value;
                }
                if(sortName != null && code != null){
                    String sortNameCode = sortName + " +" + code;
                    tempCountrySortNameAndCodeMap.put(sortNameCode,sortName);
                    code = null;
                    sortName = null;
                }
            }
        }
        return tempCountrySortNameAndCodeMap;
    }

    public static HashMap<String,String> getCountryNameAndSortName(AppCompatActivity parentActivity){
        ArrayList<HashMap<String,String>> allCountriesMap = getCountriesFromJSONFile(parentActivity);
        HashMap<String,String>tempCountrySortNameMap = new HashMap<>();
        String name = null;
        String sortName = null;
        for (HashMap<String, String> map : allCountriesMap){
            for (Map.Entry<String, String> mapEntry : map.entrySet())
            {
                String key = mapEntry.getKey();
                String value = mapEntry.getValue();
                if(key.equals(Config.COUNTRY_NAME)){
                    name = value;
                }
                if(key.equals(Config.COUNTRY_SORT_NAME)){
                    sortName = value;
                }
                if(name != null && sortName != null){
                    tempCountrySortNameMap.put(name,sortName);
                    name = null;
                    sortName = null;
                }
            }
        }
        return tempCountrySortNameMap;
    }

    public static boolean isHasThisCountry(AppCompatActivity parentActivity, String country){
        if(getCountry(parentActivity) != null){
            if(!getCountry(parentActivity).contains(country.trim())){
                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_select_valid_country),R.drawable.notice_bad,TAG);
                return false;
            }else{
                return true;
            }
        }else{
            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_occured),R.drawable.notice_bad,TAG);
            return false;
        }
    }

    public static boolean isHasThisCountryCode(AppCompatActivity parentActivity, String countryCode){
        if(getCountryCode(parentActivity) != null){
            if(!countryCode.contains(countryCode.trim())){
                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_select_valid_country_code),R.drawable.notice_bad,TAG);
                return false;
            }else{
                return true;
            }
        }else{
            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_occured),R.drawable.notice_bad,TAG);
            return false;
        }
    }

    public static boolean isPhoneNumberValid(AppCompatActivity parentActivity, String phoneNumber, String countryCode)
    {
        //NOTE: This should probably be a member variable.
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();

        try {
            if(countryCode == null){
                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getResources().getString(R.string.error_country_code_not_found),R.drawable.notice_bad,TAG);
                return false;
            }
            else{
                Phonenumber.PhoneNumber numberProto = phoneUtil.parse(phoneNumber, countryCode);
                if(!phoneUtil.isValidNumber(numberProto)){
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getResources().getString(R.string.error_country_code_not_match_with_country),R.drawable.notice_bad,TAG);
                }
                return phoneUtil.isValidNumber(numberProto);
            }
        }
        catch (NumberParseException e) {
            System.err.println("NumberParseException was thrown: " + e.toString());
            Log.e(TAG,e.toString());
            GeneralFunction.openMessageNotificationDialog(parentActivity,e.toString(),R.drawable.notice_bad,TAG);
        }

        return false;
    }

    public static String getSortNameByCode(String countryCodeATV, AppCompatActivity parentActivity){
        String sortName = null;
        HashMap<String,String> tempCountrySortNameAndCodeMap = getCountrySortNameAndCodeMap(parentActivity);
        for (Map.Entry<String, String> mapEntry : tempCountrySortNameAndCodeMap.entrySet())
        {
            String key = mapEntry.getKey();
            String value = mapEntry.getValue();
            if(key.equals(countryCodeATV)){
                sortName = value;
            }
        }
        return sortName;
    }

//    private boolean isHasThisState(AppCompatActivity parentActivity, String state){
//        if(getStateFromCountryList != null){
//            if(!getStateFromCountryList.contains(state.trim())){
//                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_select_valid_state),R.drawable.notice_bad,TAG);
//                return false;
//            }else{
//                return true;
//            }
//        }else{
//            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_occured),R.drawable.notice_bad,TAG);
//            return false;
//        }
//    }
}
