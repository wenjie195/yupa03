package com.vidatechft.yupa.utilities;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.InputType;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BaseTarget;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.SizeReadyCallback;
import com.bumptech.glide.request.transition.Transition;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.Accommodation;
import com.vidatechft.yupa.classes.BookHomestayDetails;
import com.vidatechft.yupa.classes.BookingHistory;
import com.vidatechft.yupa.classes.CurrencyRate;
import com.vidatechft.yupa.classes.Favourite;
import com.vidatechft.yupa.classes.Friend;
import com.vidatechft.yupa.classes.GlideApp;
import com.vidatechft.yupa.classes.Itinerary;
import com.vidatechft.yupa.classes.Job;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.classes.TravelKit;
import com.vidatechft.yupa.classes.User;
import com.vidatechft.yupa.dialogFragments.BookingNotificationDialogFragment;
import com.vidatechft.yupa.dialogFragments.MessageNotificationDialogFragment;
import com.vidatechft.yupa.notificationFragment.MyFirebaseMessagingService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import static com.bumptech.glide.load.DecodeFormat.PREFER_RGB_565;

public class GeneralFunction {

    private static boolean not_first_time_showing_info_window = false;
/************************************************************PERMISSION***************************************************************************************/

    public static Boolean isStorageAndCameraPermitted(Context context, AppCompatActivity activity){
        boolean isPermitted = true;

        if(ContextCompat.checkSelfPermission(context,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == 0){
            isPermitted = false;
        }
        if(ContextCompat.checkSelfPermission(context,
                Manifest.permission.READ_EXTERNAL_STORAGE) == 0){
            isPermitted = false;
        }
        if (ContextCompat.checkSelfPermission(context,
                Manifest.permission.CAMERA)
                == 0){
            isPermitted = false;
//            ActivityCompat.requestPermissions(context, new String[] {Manifest.permission.CAMERA}, CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE);

        }

        if(isPermitted){
            String[] permissions = {
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA
            };

            ActivityCompat.requestPermissions(activity,
                    permissions,
                    Config.REQUEST_CALENDAR_PERMISSION);

            openMessageNotificationDialog(activity,activity.getString(R.string.need_camera_storage_permission), R.drawable.notice_bad,"storage_camera_permission");
            return false;
        }else{
            return true;
        }
    }

    public static Boolean isCalendarPermitted(Context context, AppCompatActivity activity){
        boolean isPermitted = true;

        if(ContextCompat.checkSelfPermission(context,
                Manifest.permission.WRITE_CALENDAR) == 0){
            isPermitted = false;
        }
        if(ContextCompat.checkSelfPermission(context,
                Manifest.permission.READ_CALENDAR) == 0){
            isPermitted = false;
        }

        if(isPermitted){
            String[] permissions = {
                    Manifest.permission.WRITE_CALENDAR,
                    Manifest.permission.READ_CALENDAR
            };

            ActivityCompat.requestPermissions(activity,
                    permissions,
                    Config.REQUEST_CAMERA_AND_STORAGE_PERMISSION);

            openMessageNotificationDialog(activity,activity.getString(R.string.need_calendar_permission), R.drawable.notice_bad,"storage_camera_permission");
            return false;
        }else{
            return true;
        }
    }

    public static boolean dontHaveLocationPermission(AppCompatActivity parentActivity) {
        return ActivityCompat.checkSelfPermission(parentActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(parentActivity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED;
    }

    public static void requestForLocationPermission(final AppCompatActivity parentActivity, int checkCount) {
        //dont show again bug on my phone because it is in a onResume method
        //maybe put check count if more than 3 times dont show again
        //then show alert dialog to direct them to the setting page
        //explanations of usage of shouldShowRequestPermissionRationale function from here https://stackoverflow.com/questions/32347532/android-m-permissions-confused-on-the-usage-of-shouldshowrequestpermissionrati

//        if (!ActivityCompat.shouldShowRequestPermissionRationale(parentActivity, Manifest.permission.ACCESS_FINE_LOCATION)) {
//            ActivityCompat.requestPermissions(parentActivity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Config.REQUEST_GPS_PERMISSION);
//        }

        if(checkCount <= 3){
            ActivityCompat.requestPermissions(parentActivity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Config.REQUEST_GPS_PERMISSION);
        }
        else if(checkCount == 4){
            //if is the 4th time, only try for 3 times
            //after the 4th time, not doing anything anymore
            AlertDialog alertDialog = new AlertDialog.Builder(parentActivity).setMessage(parentActivity.getString(R.string.need_enable_gps)).setPositiveButton(parentActivity.getString(R.string.go_to_settings), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // navigate to settings
                    dialog.dismiss();
                    try{
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", parentActivity.getPackageName(), null);
                        intent.setData(uri);
                        parentActivity.startActivity(intent);
                    }catch (Exception e){
                        parentActivity.startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                    }
                }
            }).setNegativeButton(parentActivity.getString(R.string.go_back), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    parentActivity.onBackPressed();
                }
            }).create();

            alertDialog.show();
            alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setAllCaps(false);
            alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setAllCaps(false);
            alertDialog.getButton(DialogInterface.BUTTON_NEUTRAL).setAllCaps(false);
            alertDialog.show();
        }
    }

    public static void showCameraSettingAlert(final AppCompatActivity parentActivity) {
        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(parentActivity);
        alertDialogBuilder.setTitle(parentActivity.getString(R.string.alert_camera_title));
        alertDialogBuilder.setMessage(parentActivity.getString(R.string.alert_camera_desc));
        alertDialogBuilder.setPositiveButton(parentActivity.getString(R.string.go_to_settings), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//                parentActivity.startActivity(intent);

                Uri packageUri = Uri.fromParts( "package", parentActivity.getPackageName(), null );

                Intent applicationDetailsSettingsIntent = new Intent();

                applicationDetailsSettingsIntent.setAction( Settings.ACTION_APPLICATION_DETAILS_SETTINGS );
                applicationDetailsSettingsIntent.setData( packageUri );
                applicationDetailsSettingsIntent.addFlags( Intent.FLAG_ACTIVITY_NEW_TASK );

                parentActivity.startActivity( applicationDetailsSettingsIntent );
            }
        });

        alertDialogBuilder.setNegativeButton(parentActivity.getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        android.support.v7.app.AlertDialog alertDialog  = alertDialogBuilder.create();
        if( !( parentActivity.isFinishing() && alertDialog.isShowing() ) ){
            alertDialog.show();
        }
    }

    public static void showGpsSettingsAlert(final AppCompatActivity parentActivity) {
        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(parentActivity);
        alertDialogBuilder.setTitle(parentActivity.getString(R.string.alert_gps_title));
        alertDialogBuilder.setMessage(parentActivity.getString(R.string.alert_gps_desc));
        alertDialogBuilder.setPositiveButton(parentActivity.getString(R.string.gps_settings), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                parentActivity.startActivity(intent);
            }
        });

        alertDialogBuilder.setNegativeButton(parentActivity.getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        android.support.v7.app.AlertDialog alertDialog  = alertDialogBuilder.create();
        if( !( parentActivity.isFinishing() && alertDialog.isShowing() ) ){
            alertDialog.show();
        }
    }

/************************************************************GPS AND LOCATIONS AND GOOGLE MAPS**************************************************************************************/

    public static HashMap<String,GeoPoint> getBoundingBoxCoor(Location currentLocation, final int RADIUS){
        //from here https://stackoverflow.com/questions/2839533/adding-distance-to-a-gps-coordinate
        //in meter //the final calculation is actually larger than the intended radius because it is a square not a circle
        final double currentLat = currentLocation.getLatitude();
        final double currentLng = currentLocation.getLongitude();

        final double latRadian = Math.toRadians(currentLat);

        final double degLatKm = 110.574235;
        final double degLongKm = 110.572833 * Math.cos(latRadian);
        final double deltaLat = RADIUS / 1000.0 / degLatKm;
        final double deltaLong = RADIUS / 1000.0 / degLongKm;

        final double minLat = currentLat - deltaLat;
        final double minLong = currentLng - deltaLong;
        final double maxLat = currentLat + deltaLat;
        final double maxLong = currentLng + deltaLong;

        GeoPoint btmLeft = new GeoPoint(minLat,minLong);//lat is north and south, long is east and west
        GeoPoint btmRight = new GeoPoint(minLat,maxLong);
        GeoPoint topLeft = new GeoPoint(maxLat,minLong);
        GeoPoint topRight = new GeoPoint(maxLat,maxLong);
        GeoPoint center = new GeoPoint(currentLat,currentLng);

        HashMap<String,GeoPoint> boundingBox = new HashMap<>();
        boundingBox.put(Config.BOUNDING_BOX_BTM_LEFT,btmLeft);
        boundingBox.put(Config.BOUNDING_BOX_BTM_RIGHT,btmRight);
        boundingBox.put(Config.BOUNDING_BOX_TOP_RIGHT,topRight);
        boundingBox.put(Config.BOUNDING_BOX_TOP_LEFT,topLeft);
        boundingBox.put(Config.BOUNDING_BOX_CENTER,center);

        return boundingBox;
    }

    public static HashMap<String,LatLng> getBoundingBoxCoor(LatLng currentLocation, final int RADIUS){
        //from here https://stackoverflow.com/questions/2839533/adding-distance-to-a-gps-coordinate
        //in meter //the final calculation is actually larger than the intended radius because it is a square not a circle
        final double currentLat = currentLocation.latitude;
        final double currentLng = currentLocation.longitude;

        final double latRadian = Math.toRadians(currentLat);

        final double degLatKm = 110.574235;
        final double degLongKm = 110.572833 * Math.cos(latRadian);
        final double deltaLat = RADIUS / 1000.0 / degLatKm;
        final double deltaLong = RADIUS / 1000.0 / degLongKm;

        final double minLat = currentLat - deltaLat;
        final double minLong = currentLng - deltaLong;
        final double maxLat = currentLat + deltaLat;
        final double maxLong = currentLng + deltaLong;

        LatLng btmLeft = new LatLng(minLat,minLong);//lat is north and south, long is east and west
        LatLng btmRight = new LatLng(minLat,maxLong);
        LatLng topLeft = new LatLng(maxLat,minLong);
        LatLng topRight = new LatLng(maxLat,maxLong);
        LatLng center = new LatLng(currentLat,currentLng);

        HashMap<String,LatLng> boundingBox = new HashMap<>();
        boundingBox.put(Config.BOUNDING_BOX_BTM_LEFT,btmLeft);
        boundingBox.put(Config.BOUNDING_BOX_BTM_RIGHT,btmRight);
        boundingBox.put(Config.BOUNDING_BOX_TOP_RIGHT,topRight);
        boundingBox.put(Config.BOUNDING_BOX_TOP_LEFT,topLeft);
        boundingBox.put(Config.BOUNDING_BOX_CENTER,center);

        return boundingBox;
    }

    public static HashMap<String, LatLng> getBoundingBoxCoor(GeoPoint currentLocation, final int RADIUS){
        //from here https://stackoverflow.com/questions/2839533/adding-distance-to-a-gps-coordinate
        //in meter //the final calculation is actually larger than the intended radius because it is a square not a circle
        final double currentLat = currentLocation.getLatitude();
        final double currentLng = currentLocation.getLongitude();

        final double latRadian = Math.toRadians(currentLat);

        final double degLatKm = 110.574235;
        final double degLongKm = 110.572833 * Math.cos(latRadian);
        final double deltaLat = RADIUS / 1000.0 / degLatKm;
        final double deltaLong = RADIUS / 1000.0 / degLongKm;

        final double minLat = currentLat - deltaLat;
        final double minLong = currentLng - deltaLong;
        final double maxLat = currentLat + deltaLat;
        final double maxLong = currentLng + deltaLong;

        LatLng btmLeft = new LatLng(minLat,minLong);//lat is north and south, long is east and west
        LatLng btmRight = new LatLng(minLat,maxLong);
        LatLng topLeft = new LatLng(maxLat,minLong);
        LatLng topRight = new LatLng(maxLat,maxLong);
        LatLng center = new LatLng(currentLat,currentLng);

        HashMap<String,LatLng> boundingBox = new HashMap<>();
        boundingBox.put(Config.BOUNDING_BOX_BTM_LEFT,btmLeft);
        boundingBox.put(Config.BOUNDING_BOX_BTM_RIGHT,btmRight);
        boundingBox.put(Config.BOUNDING_BOX_TOP_RIGHT,topRight);
        boundingBox.put(Config.BOUNDING_BOX_TOP_LEFT,topLeft);
        boundingBox.put(Config.BOUNDING_BOX_CENTER,center);

        return boundingBox;
    }

    public static HashMap<String,LatLng> convertGeoPointToLatLng(HashMap<String,GeoPoint> boundingBox){
        HashMap<String,LatLng> tempBox = new HashMap<>();
        tempBox.put(Config.BOUNDING_BOX_TOP_RIGHT,new LatLng(boundingBox.get(Config.BOUNDING_BOX_TOP_RIGHT).getLatitude(),boundingBox.get(Config.BOUNDING_BOX_TOP_RIGHT).getLongitude()));
        tempBox.put(Config.BOUNDING_BOX_TOP_LEFT,new LatLng(boundingBox.get(Config.BOUNDING_BOX_TOP_LEFT).getLatitude(),boundingBox.get(Config.BOUNDING_BOX_TOP_LEFT).getLongitude()));
        tempBox.put(Config.BOUNDING_BOX_BTM_LEFT,new LatLng(boundingBox.get(Config.BOUNDING_BOX_BTM_LEFT).getLatitude(),boundingBox.get(Config.BOUNDING_BOX_BTM_LEFT).getLongitude()));
        tempBox.put(Config.BOUNDING_BOX_BTM_RIGHT,new LatLng(boundingBox.get(Config.BOUNDING_BOX_BTM_RIGHT).getLatitude(),boundingBox.get(Config.BOUNDING_BOX_BTM_RIGHT).getLongitude()));
        tempBox.put(Config.BOUNDING_BOX_CENTER,new LatLng(boundingBox.get(Config.BOUNDING_BOX_CENTER).getLatitude(),boundingBox.get(Config.BOUNDING_BOX_CENTER).getLongitude()));
        return tempBox;
    }

    //from here https://stackoverflow.com/questions/35977204/how-to-draw-a-polygon-rectangle-on-google-maps-with-defined-distance-meters
    public static PolygonOptions getSquarePolygon(HashMap<String,LatLng> boundingBox, String colourType){
        int strokeColour = Config.COLOUR_ARGB_LIGHT_TRANSPARENT;
        int fillColour = Config.COLOUR_ARGB_LIGHT_TRANSPARENT_GREEN;

        if(TextUtils.equals(Config.MAP_COLOUR_GREEN,colourType)){
            strokeColour = Config.COLOUR_ARGB_LIGHT_TRANSPARENT;
            fillColour = Config.COLOUR_ARGB_LIGHT_TRANSPARENT_GREEN;
        }

        return new PolygonOptions()
                .add(boundingBox.get(Config.BOUNDING_BOX_TOP_RIGHT))
                .add(boundingBox.get(Config.BOUNDING_BOX_BTM_RIGHT))
                .add(boundingBox.get(Config.BOUNDING_BOX_BTM_LEFT))
                .add(boundingBox.get(Config.BOUNDING_BOX_TOP_LEFT))
                .strokeColor(strokeColour)
                .fillColor(fillColour)
                .strokeWidth(2f);
    }

    public static CircleOptions getMapCircle(LatLng currentGps,final int RADIUS, String colourType){
        int strokeColour = Config.COLOUR_ARGB_LIGHT_TRANSPARENT;
        int fillColour = Config.COLOUR_ARGB_LIGHT_TRANSPARENT_GREEN;

        if(TextUtils.equals(Config.MAP_COLOUR_GREEN,colourType)){
            strokeColour = Config.COLOUR_ARGB_LIGHT_TRANSPARENT;
            fillColour = Config.COLOUR_ARGB_LIGHT_TRANSPARENT_GREEN;
        }

        return new CircleOptions()
                .center(currentGps)
                .radius(RADIUS)
                .strokeColor(strokeColour)
                .fillColor(fillColour);
    }

    public static void makeGoogleMapNoDelay(final Activity parentActivity){
        //Found from https://stackoverflow.com/questions/26265526/what-makes-my-map-fragment-loading-slow
        // Fixing Later Map loading Delay
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    MapView mv = new MapView(parentActivity.getApplicationContext());
                    mv.onCreate(null);
                    mv.onPause();
                    mv.onDestroy();
                    mv.onLowMemory();
                    mv.onResume();
                }catch (Exception ignored){

                }
            }
        }).start();
    }

    public static boolean openMapApplicationViaGps(MainActivity parentActivity, double lat, double lng, String name){
//        String uri = "http://maps.google.com/"+oriRoom.accommodationGeoPoint.getLatitude()+","+oriRoom.accommodationGeoPoint.getLongitude();
//        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
//        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
//        startActivity(intent);
//        Uri gmmIntentUri = Uri.parse("geo:"+oriRoom.accommodationGeoPoint.getLatitude()+","+oriRoom.accommodationGeoPoint.getLongitude()+"?q=");
//        Uri gmmIntentUri = Uri.parse("geo:0,0?q="+oriRoom.accommodationGeoPoint.getLatitude()+","+oriRoom.accommodationGeoPoint.getLongitude()+"("+oriRoom.address+")");

        Uri gmmIntentUri = Uri.parse("geo:0,0?q="+lat+","+lng + " (" + name + ")");
//        Uri gmmIntentUri = Uri.parse("geo:0,0?q="+lat+","+lng);
//        Uri gmmIntentUri = Uri.parse("https://waze.com/ul?ll="+oriRoom.accommodationGeoPoint.getLatitude()+","+oriRoom.accommodationGeoPoint.getLongitude()+"&z=10");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
//        mapIntent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        if (mapIntent.resolveActivity(parentActivity.getPackageManager()) != null) {
            parentActivity.startActivity(mapIntent);
            return true;
//          startActivity(Intent.createChooser(mapIntent, "Select an application"));
        }
        else{
            GeneralFunction.openMessageNotificationDialog(parentActivity, Config.DONT_HAVE_ANY_MAP_APPLICATION,R.drawable.notice_bad,"openMapApplicationViaGps");
            return false;
        }
    }


/************************************************************PLAY STORE AND APP VERSION***************************************************************************************/

    public static int getCurrentAppVersion(AppCompatActivity parentActivity){
        PackageManager manager = parentActivity.getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(parentActivity.getPackageName(), 0);
            return info.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return 0;
        }
    }

    private static void openPlayStoreAlertDialog(final AppCompatActivity parentActivity){
        if(!parentActivity.isFinishing() && isAppInForground(parentActivity.getApplicationContext())){
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(parentActivity);
                    LayoutInflater inflater = parentActivity.getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.dialog_new_app, null);
                    Button goPlayStoreBtn = dialogView.findViewById(R.id.goPlayStoreBtn);
                    ImageView mascotIV = dialogView.findViewById(R.id.mascotIV);

                    GlideApp.with(parentActivity)
                            .asBitmap().format(PREFER_RGB_565)
                            .load(R.drawable.notice_bad)
                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                            .placeholder(R.drawable.progress_animation)
                            .error(R.drawable.ic_not_found)
                            .into(mascotIV);

                    builder.setView(dialogView);
                    builder.setCancelable(false);
                    builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            final String appPackageName = parentActivity.getPackageName(); // getPackageName() from Context or Activity object
                            try {
                                parentActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                            } catch (android.content.ActivityNotFoundException anfe) {
                                parentActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                            }
                        }
                    });
                    final AlertDialog alertDialog = builder.create();

                    goPlayStoreBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.cancel();
                        }
                    });

                    alertDialog.show();
                }
            });
        }
    }

    public static void checkCurrentAppVersion(final AppCompatActivity parentActivity){
        getFirestoreInstance().collection(Config.URL_APP_VERSION).document(Config.URL_APP_VERSION)
                .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists() && document.get(Config.URL_APP_VERSION) != null) {
                        int currentAppVersion = getCurrentAppVersion(parentActivity);
                        if(currentAppVersion != 0){
                            if(currentAppVersion < (Long) document.get(Config.URL_APP_VERSION) ){
                                openPlayStoreAlertDialog(parentActivity);
                            }
                        }else{
                            Log.e(parentActivity.getLocalClassName(),"error getting app version from phone");
                        }

                        Log.d(Config.URL_APP_VERSION + ":- ",String.valueOf(currentAppVersion));
                    }
                }
            }
        });
    }




/************************************************************DIALOG FRAGMENTS AND POPUPS***************************************************************************************/
    public static void openMessageNotificationDialog(AppCompatActivity parentActivity, String message, int drawableResource, String TAG){
        if(parentActivity != null && !parentActivity.isFinishing() && isAppInForground(parentActivity.getApplicationContext())){
            FragmentManager fm = parentActivity.getSupportFragmentManager();
            MessageNotificationDialogFragment messageNotificationDialogFragment = MessageNotificationDialogFragment.newInstance(parentActivity, message,drawableResource);
            messageNotificationDialogFragment.show(fm, TAG);
        }
    }

    public static void openBookingNotificationDialog(MainActivity parentActivity, Room room, BookingHistory bookingHistory, String TAG){
        if(parentActivity != null && !parentActivity.isFinishing() && isAppInForground(parentActivity.getApplicationContext())){
            FragmentManager fm = parentActivity.getSupportFragmentManager();
            BookingNotificationDialogFragment bookingNotificationDialogFragment = BookingNotificationDialogFragment.newInstance(parentActivity, room, bookingHistory);
            bookingNotificationDialogFragment.show(fm, TAG);
        }
    }

    public static DatePickerDialog getDatePickerDialog(AppCompatActivity parentActivity, DatePickerDialog.OnDateSetListener dateSetListener){
        Date date = new Date();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);

        return new DatePickerDialog(
                parentActivity, dateSetListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
    }

    public static TimePickerDialog getTimePickerDialog(AppCompatActivity parentActivity, TimePickerDialog.OnTimeSetListener timeSetListener){
        Date date = new Date();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);

        return new TimePickerDialog(
                parentActivity, timeSetListener, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);
    }

    public static void openPlacePicker(AppCompatActivity parentActivity,Fragment parentFragment){
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

        try {
            parentFragment.startActivityForResult(builder.build(parentActivity), Config.REQUEST_PLACE_PICKER);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    public static void setLocationNameForPlace(final AppCompatActivity parentActivity, Place place, final TextView locationTV){
        final String address[] = place.getAddress().toString().split("\\,");
        final String[] location = new String[1];
        if(address.length-2 < 0 || address.length-1 < 0){

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(parentActivity);
            alertDialogBuilder.setCancelable(false);
            alertDialogBuilder.setTitle("Your Location Name:");
//                    alertDialogBuilder.setMessage("Alert message to be shown");
            final EditText input = new EditText(parentActivity);
            input.setInputType(InputType.TYPE_CLASS_TEXT);
            String final_address = place.getName().toString();
            input.setText(final_address);
            input.setSelection(input.getText().length());
            alertDialogBuilder.setView(input);

            alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
//                            location[0] = place.getName() + "," + input.getText().toString() + "," + address[address.length-1];

                    KeyboardUtilities.hideSoftKeyboard(parentActivity);
                    location[0] = input.getText().toString();
                    locationTV.setText(location[0]);
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    KeyboardUtilities.hideSoftKeyboard(parentActivity);
                    dialog.dismiss();
                }
            });
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                alertDialogBuilder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        KeyboardUtilities.hideSoftKeyboard(parentActivity);
                    }
                });
            }else {
                alertDialogBuilder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        KeyboardUtilities.hideSoftKeyboard(parentActivity);
                    }
                });
            }
            AlertDialog dialog = alertDialogBuilder.create();
            //show the keyboard for user convenience to enter location
            if(dialog.getWindow() != null){
                dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
            }
            dialog.show();

        }
        else{
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(parentActivity);
            alertDialogBuilder.setCancelable(false);
            alertDialogBuilder.setTitle("Your Location Name:");
//                    alertDialogBuilder.setMessage("Alert message to be shown");
            final EditText input = new EditText(parentActivity);
            input.setInputType(InputType.TYPE_CLASS_TEXT);
            String final_address = place.getName().toString()   ;
            input.setText(final_address);
            input.setSelection(input.getText().length()); //To set the blinking cursor to the end of the word
            alertDialogBuilder.setView(input);

            alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
//                            location[0] = place.getName() + "," + input.getText().toString() + "," + address[address.length-1];

                    location[0] = input.getText().toString();
                    locationTV.setText(location[0]);
                }
            });
            alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            AlertDialog dialog = alertDialogBuilder.create();
            //show the keyboard for user convenience to enter location
            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
            dialog.show();
//                    locationAlertDialog();
        }
    }

    public static void setLocationCountryForPlace(Place place,TextView locationTV){
        String address[] = place.getAddress().toString().split("\\,");
        if(address.length > 0){
            String country = null;
            String state = null;
            if(address.length - 1 >= 0){
                country = address[address.length - 1];
            }

            if(address.length - 2 >= 0){
                state = address[address.length - 2];
            }
            String fullAddress = "";

            if(state != null){
                fullAddress += state + ",";
            }

            fullAddress += country;

            locationTV.setText(fullAddress);
        }else{
            locationTV.setText(place.getAddress().toString());
        }
    }

    public static String getCountryStateFromPlace(Place place){
        String fullAddress = "";
        String address[] = place.getAddress().toString().split("\\,");

        if(address.length > 0){
            String country = null;
            String state = null;
            if(address.length - 1 >= 0){
                country = address[address.length - 1];
            }

            if(address.length - 2 >= 0){
                state = address[address.length - 2];
            }

            if(state != null){
                fullAddress += state + ",";
            }

            fullAddress += country;
        }else{
            if(place.getName() != null){
                fullAddress = place.getName().toString();
            }else{
                fullAddress = "???";
            }
        }

        return fullAddress;
    }

    public static void showFileChooser(AppCompatActivity parentActivity, Fragment fragment){
        if (CropImage.isExplicitCameraPermissionRequired(parentActivity)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if(!parentActivity.shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)){
                    showCameraSettingAlert(parentActivity);
                }//from here https://stackoverflow.com/questions/34573627/how-to-know-user-check-never-ask-again-before-calling-requestpermissions
                //the answer that says to use shouldShowRequestPermissionRationale to detect whether user has ticked the never ask again checkbox
            }
            fragment.requestPermissions(new String[]{Manifest.permission.CAMERA}, CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE);
        } else {
            CropImage.activity()
                    .setAspectRatio(16, 9)
                    .setRequestedSize(650, 650)
                    .setFixAspectRatio(true)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setScaleType(CropImageView.ScaleType.FIT_CENTER)
                    .start(parentActivity, fragment);
        }
    }

    public static void showFileChooser11(AppCompatActivity parentActivity, Fragment fragment){
        if (CropImage.isExplicitCameraPermissionRequired(parentActivity)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if(!parentActivity.shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)){
                    showCameraSettingAlert(parentActivity);
                }
            }
            fragment.requestPermissions(new String[]{Manifest.permission.CAMERA}, CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE);
        } else {
            CropImage.activity()
                    .setAspectRatio(1, 1)
                    .setRequestedSize(650, 650)
                    .setFixAspectRatio(true)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setScaleType(CropImageView.ScaleType.FIT_CENTER)
                    .start(parentActivity, fragment);
        }
    }

    public static void showFileChooserRatio169(AppCompatActivity parentActivity, Fragment fragment){
        if (CropImage.isExplicitCameraPermissionRequired(parentActivity)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if(!parentActivity.shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)){
                    showCameraSettingAlert(parentActivity);
                }
            }
            fragment.requestPermissions(new String[]{Manifest.permission.CAMERA}, CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE);
        } else {
            CropImage.activity()
                    .setAspectRatio(16, 9)
                    .setRequestedSize(650, 650)
                    .setFixAspectRatio(true)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setScaleType(CropImageView.ScaleType.FIT_CENTER)
                    .start(parentActivity, fragment);
        }
    }




/************************************************************ANIMATIONS**************************************************************************************/
    public static void slideUpAnimation(AppCompatActivity parentActivity, ConstraintLayout constraintLayout){
        Animation slideUp = AnimationUtils.loadAnimation(parentActivity, R.anim.slide_up);

        if (constraintLayout.getVisibility() == View.GONE) {
            constraintLayout.setVisibility(View.VISIBLE);
            constraintLayout.startAnimation(slideUp);
        }
    }

    public static void slideDownAnimation(AppCompatActivity parentActivity, ConstraintLayout constraintLayout){
        Animation slideUp = AnimationUtils.loadAnimation(parentActivity, R.anim.slide_down);

        if (constraintLayout.getVisibility() == View.GONE) {
            constraintLayout.setVisibility(View.VISIBLE);
            constraintLayout.startAnimation(slideUp);
        }
    }

    public static void fadeInAnimation(AppCompatActivity parentActivity, ConstraintLayout constraintLayout){
        Animation fadeIn = AnimationUtils.loadAnimation(parentActivity, R.anim.fade_in);

        if (constraintLayout.getVisibility() == View.GONE) {
            constraintLayout.setVisibility(View.VISIBLE);
            constraintLayout.startAnimation(fadeIn);
        }
    }

    public static void fadeOutAnimation(AppCompatActivity parentActivity, ConstraintLayout constraintLayout){
        Animation fadeOut = AnimationUtils.loadAnimation(parentActivity, R.anim.fade_out);

        if (constraintLayout.getVisibility() == View.VISIBLE) {
            constraintLayout.setVisibility(View.GONE);
            constraintLayout.startAnimation(fadeOut);
        }
    }

    public static void slideUpAnimationForListViewCountry(final AppCompatActivity parentActivity, final ConstraintLayout constraintLayout, final ListView listView, final String country, final EditText editText){

        int arryid = parentActivity.getResources().getIdentifier(country, "array",
                parentActivity.getPackageName());

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(parentActivity,
                arryid, android.R.layout.simple_list_item_1);

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // ListView Clicked item index
                int itemPosition     = position;

                // ListView Clicked item value
                String  itemValue    = (String) listView.getItemAtPosition(position);

                if(editText.getId() == R.id.currencyTV){
                    StringTokenizer tokens = new StringTokenizer(itemValue, "-");
                    String first = tokens.nextToken();// this will contain "Fruit"
                    String second = tokens.nextToken();// this will contain " they taste good"
                    if(editText.getText().toString().equals("Select_your_origin_country")){
                        editText.setText("");
                    }
                    editText.setText(first.trim());
                    MainActivity.isOpen = false;
                    slideDownAnimationForListView(parentActivity,constraintLayout);
                }
                else{
                    // Show Alert
                    //                Toast.makeText(parentActivity.getApplicationContext(),
                    //                        "Position :"+itemPosition+"  ListItem : " +itemValue , Toast.LENGTH_LONG)
                    //                        .show();
                    editText.setText(itemValue);
                    MainActivity.isOpen = false;
                    slideDownAnimationForListView(parentActivity,constraintLayout);
                }

            }
        });
        Animation slideUp = AnimationUtils.loadAnimation(parentActivity, R.anim.slide_up);

        if (constraintLayout.getVisibility() == View.GONE) {
            constraintLayout.setVisibility(View.VISIBLE);
            constraintLayout.startAnimation(slideUp);
        }
    }

    public static void slideUpAnimationForListView(final AppCompatActivity parentActivity, final ConstraintLayout constraintLayout, final ListView listView, final int myArray, final EditText editText){
        KeyboardUtilities.hideSoftKeyboard(parentActivity);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(parentActivity,
                myArray, android.R.layout.simple_list_item_1);

        //        int arryid = parentActivity.getResources().getIdentifier(getCountryList, "array",
        //                parentActivity.getPackageName());

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // ListView Clicked item index
                int itemPosition     = position;

                // ListView Clicked item value
                String  itemValue    = (String) listView.getItemAtPosition(position);

                if(editText.getId() == R.id.currencyTV){
                    StringTokenizer tokens = new StringTokenizer(itemValue, "-");
                    String first = tokens.nextToken();// this will contain "Fruit"
                    String second = tokens.nextToken();// this will contain " they taste good"
                    editText.setText(first.trim());
                    MainActivity.isOpen = false;
                    slideDownAnimationForListView(parentActivity,constraintLayout);
                }
                else{
                    // Show Alert
                    //                Toast.makeText(parentActivity.getApplicationContext(),
                    //                        "Position :"+itemPosition+"  ListItem : " +itemValue , Toast.LENGTH_LONG)
                    //                        .show();
                    editText.setText(itemValue);
                    MainActivity.isOpen = false;
                    slideDownAnimationForListView(parentActivity,constraintLayout);
                }

            }
        });

        Animation slideUp = AnimationUtils.loadAnimation(parentActivity, R.anim.slide_up);

        if (constraintLayout.getVisibility() == View.GONE) {
            constraintLayout.setVisibility(View.VISIBLE);
            constraintLayout.startAnimation(slideUp);
        }
    }

    public static void slideDownAnimationForListView(final AppCompatActivity parentActivity, final ConstraintLayout constraintLayout){
        Animation slideDown = AnimationUtils.loadAnimation(parentActivity, R.anim.slide_down);
        try{
            if (constraintLayout.getVisibility() == View.VISIBLE) {
                constraintLayout.setVisibility(View.GONE);
                constraintLayout.startAnimation(slideDown);
            }
            constraintLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Animation slideDown = AnimationUtils.loadAnimation(parentActivity, R.anim.slide_down);
                    if (constraintLayout.getVisibility() == View.VISIBLE) {
                        constraintLayout.setVisibility(View.GONE);
                        constraintLayout.startAnimation(slideDown);
                    }
                }
            });
        }
        catch(Exception e){
            Log.e(",",e.toString());
        }
    }

    public static void slideUpAnimationForView(final AppCompatActivity parentActivity, final View view){
        KeyboardUtilities.hideSoftKeyboard(parentActivity);
        Animation slideUp = AnimationUtils.loadAnimation(parentActivity, R.anim.slide_up);
        if (view != null && view.getVisibility() == View.GONE) {
            view.setVisibility(View.VISIBLE);
            view.startAnimation(slideUp);
        }
    }

    public static void slideDownAnimationForView(final AppCompatActivity parentActivity, final View view){
        KeyboardUtilities.hideSoftKeyboard(parentActivity);
        Animation slideDown = AnimationUtils.loadAnimation(parentActivity, R.anim.slide_down);
        if (view != null && view.getVisibility() == View.VISIBLE) {
            view.setVisibility(View.GONE);
            view.startAnimation(slideDown);
        }
    }


/************************************************************ERROR HANDLING POPUPS***************************************************************************************/
    public static void handleUploadError(MainActivity parentActivity, Fragment parentFragment, int errorMsgId, String TAG){
        if(parentActivity != null && !parentActivity.isFinishing() && isAppInForground(parentActivity.getApplicationContext()) ){
            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(errorMsgId),R.drawable.notice_bad,TAG);
            parentActivity.controlProgressIndicator(false,parentFragment);
        }
    }

    public static void handleUploadSuccess(MainActivity parentActivity, Fragment parentFragment, int errorMsgId, String TAG){
        if(parentActivity != null && !parentActivity.isFinishing() && isAppInForground(parentActivity.getApplicationContext())){
            parentActivity.updateProgress(100f,parentFragment);
            parentActivity.controlProgressIndicator(false,parentFragment);
            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(errorMsgId),R.drawable.notice_good,TAG);
        }
    }
    public static void handleUploadSuccessTommy(MainActivity parentActivity, Fragment parentFragment, int errorMsgId, String TAG){
        parentActivity.updateProgress(100f,parentFragment);
        parentActivity.controlProgressIndicator(false,parentFragment);
    }




/************************************************************SHARED PREFERENCES***************************************************************************************/
    public static ArrayList<String> getSavedSPStringFromKeyAsArray(AppCompatActivity parentActivity, String spType){
        ArrayList<String> contents = new ArrayList<>();

        final Gson gson = new Gson();
        SharedPreferences sharedPreferences = parentActivity.getSharedPreferences(spType, Context.MODE_PRIVATE);

        Map<String, ?> allStrings = sharedPreferences.getAll();

        for (Map.Entry<String, ?> entry : allStrings.entrySet()) {
            contents.add(entry.getKey());
        }
        return contents;
    }

    public static String getSpString(Activity parentActivity, String spType){
        SharedPreferences sharedPreferences = parentActivity.getSharedPreferences(spType, Context.MODE_PRIVATE);
        return sharedPreferences.getString(spType,null);
    }

    public static void saveSPStringIntoKey(AppCompatActivity parentActivity, String contentId, String content, String spType){
        SharedPreferences sharedPreferences = parentActivity.getSharedPreferences(spType, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();

        prefsEditor.putString(contentId, content);
        prefsEditor.apply();
    }

    //inner
    public static void clearSharedPreferencesWithKey(Activity parentActivity, String spType, String key){
        SharedPreferences preferences = parentActivity.getSharedPreferences(spType, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(key);
        editor.apply();
    }

    //outer
    public static void clearSharedPreferencesWithType(Activity parentActivity,String spType){
        SharedPreferences preferences = parentActivity.getSharedPreferences(spType, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
    }

    //from here https://stackoverflow.com/questions/18348744/delete-all-sharedpreferences-for-my-app-when-user-signs-out
    //seems like it needs to be restarted for it to take effect. so macam not practical la
    public static void clearAllSharedPreferences(Activity parentActivity){
        try{
            File sharedPreferenceFile = new File("/data/data/"+ parentActivity.getPackageName()+ "/shared_prefs/");
            File[] listFiles = sharedPreferenceFile.listFiles();
            for (File file : listFiles) {
                file.delete();
            }
        }catch (Exception e){
            Crashlytics.logException(e);
        }
    }

    public static void saveGuestDetailsToSP(MainActivity parentActivity, BookHomestayDetails bookHomestayDetails, String spType){
        final Gson gson = new Gson();
        SharedPreferences sharedPreferences = parentActivity.getSharedPreferences(spType, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();

        String json = gson.toJson(bookHomestayDetails);
        prefsEditor.putString(parentActivity.uid, json);
        prefsEditor.apply();
    }

    public static BookHomestayDetails getBookGuestDetailsFromSharedPreferences(Activity parentActivity, String spType){
        BookHomestayDetails bookHomestayDetails = null;

        final Gson gson = new Gson();
        SharedPreferences sharedPreferences = parentActivity.getSharedPreferences(spType, Context.MODE_PRIVATE);

        TreeMap<String, ?> allBookGuestDetails = new TreeMap<String, Object>(sharedPreferences.getAll());

        for (Map.Entry<String, ?> entry : allBookGuestDetails.entrySet()) {
            bookHomestayDetails = gson.fromJson((String) entry.getValue(), BookHomestayDetails.class);
        }
        return bookHomestayDetails;
    }

    public static void saveHostSharedPreferences(Activity parentActivity, Room room, String spType, String key) {
        SharedPreferences appSharedPrefs = parentActivity.getSharedPreferences(spType, Context.MODE_PRIVATE);

        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(room); //tasks is an ArrayList instance variable
        prefsEditor.putString(key, json);
        prefsEditor.apply();
    }

    public static void saveDocumentPath(Activity parentActivity, String key, String value){
        SharedPreferences appSharedPrefs = parentActivity.getSharedPreferences(key, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = appSharedPrefs.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static String getDocumentPath(Activity parentActivity,String spType, String key, String value){
        SharedPreferences appSharedPrefs = parentActivity.getSharedPreferences(spType, Context.MODE_PRIVATE);
        return appSharedPrefs.getString(key,value);
    }

    public static void saveAccommodationSharedPreferences(Activity parentActivity, Accommodation accommodation, String spType, String key) {
        SharedPreferences appSharedPrefs = parentActivity.getSharedPreferences(spType, Context.MODE_PRIVATE);

        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(accommodation); //tasks is an ArrayList instance variable
        prefsEditor.putString(key, json);
        prefsEditor.apply();
    }

    public static void saveAccommodationHashMapSharedPreferences(Activity parentActivity,HashMap<String,Object> accommodationMap,String spType, String key) {
        SharedPreferences appSharedPrefs = parentActivity.getSharedPreferences(spType, Context.MODE_PRIVATE);

        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(accommodationMap); //tasks is an ArrayList instance variable
        prefsEditor.putString(key, json);
        prefsEditor.apply();
    }

    public static void saveAccommodationHashMapSharedPreferencesBoolean(Activity parentActivity, Map<String, Object> accommodationMap, String spType, String key) {
        SharedPreferences appSharedPrefs = parentActivity.getSharedPreferences(spType, Context.MODE_PRIVATE);

        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();

        Gson gson = new Gson();
        String json = gson.toJson(accommodationMap); //tasks is an ArrayList instance variable
        prefsEditor.putString(key, json);
        prefsEditor.apply();
    }

    public static void saveDocumentPath(Activity parentActivity,String spType, String key, String value){
        SharedPreferences appSharedPrefs = parentActivity.getSharedPreferences(spType, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = appSharedPrefs.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static void getCheckBox(Activity parentActivity,String spType, String key, String value){
        SharedPreferences appSharedPrefs = parentActivity.getSharedPreferences(spType, Context.MODE_PRIVATE);
        Type type = new TypeToken<Map<String, Boolean>>(){}.getType();
        Gson gson = new Gson();
        Map<String, String> myMap = gson.fromJson(appSharedPrefs.getString(key,""), type);
    }

    public void saveInfo(Context context, String key, List<Map<String, String>> datas) {
        JSONArray mJsonArray = new JSONArray();
        for (int i = 0; i < datas.size(); i++) {
            Map<String, String> itemMap = datas.get(i);
            Iterator<Map.Entry<String, String>> iterator = itemMap.entrySet().iterator();

            JSONObject object = new JSONObject();

            while (iterator.hasNext()) {
                Map.Entry<String, String> entry = iterator.next();
                try {
                    object.put(entry.getKey(), entry.getValue());
                } catch (JSONException e) {

                }
            }
            mJsonArray.put(object);
        }

        SharedPreferences sp = context.getSharedPreferences("finals", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(key, mJsonArray.toString());
        editor.apply();
    }

    public List<Map<String, String>> getInfo(Context context, String key) {
        List<Map<String, String>> datas = new ArrayList<Map<String, String>>();
        SharedPreferences sp = context.getSharedPreferences("finals", Context.MODE_PRIVATE);
        String result = sp.getString(key, "");
        try {
            JSONArray array = new JSONArray(result);
            for (int i = 0; i < array.length(); i++) {
                JSONObject itemObject = array.getJSONObject(i);
                Map<String, String> itemMap = new HashMap<String, String>();
                JSONArray names = itemObject.names();
                if (names != null) {
                    for (int j = 0; j < names.length(); j++) {
                        String name = names.getString(j);
                        String value = itemObject.getString(name);
                        itemMap.put(name, value);
                    }
                }
                datas.add(itemMap);
            }
        } catch (JSONException e) {

        }

        return datas;
    }




/************************************************************GLIDE, BITMAPS AND ANY OTHER DRAWABLES***************************************************************************************/

    public static Bitmap getDrawableToBitmap(Context context, int color) {
        Drawable drawable = ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_gps, null);
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);

        //set round background on lolipop+ and square before
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Paint p = new Paint();
            p.setAntiAlias(true);
            p.setColor(ContextCompat.getColor(context, color));
            canvas.drawCircle(canvas.getHeight()/2, canvas.getHeight()/2, canvas.getHeight()/2, p);
        }
        else {
            canvas.drawColor(ContextCompat.getColor(context, color));
        }
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    public static void insertBgImgToOtherLayout(Activity parentActivity, int drawable, final View otherLayout){
        GlideApp.with(parentActivity)
                .load(drawable)
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .fitCenter()
                .into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, Transition<? super Drawable> transition) {
                        otherLayout.setBackground(resource);
                    }
                });
    }

    //make those drawable have different colours
    public static Drawable getTintedDrawable(AppCompatActivity parentActivity,
                                                  int drawableResId, int colorResId) {
        Drawable drawable = ContextCompat.getDrawable(parentActivity, drawableResId);
        int color = ContextCompat.getColor(parentActivity,colorResId);
        if (drawable != null) {
            drawable.setColorFilter(color, PorterDuff.Mode.SRC_IN);
        }
        return drawable;
    }

    public static Drawable getTintedRandomDrawable(AppCompatActivity parentActivity,
                                             int drawableResId, int colour) {
        Drawable drawable = ContextCompat.getDrawable(parentActivity, drawableResId);
        if (drawable != null) {
            drawable.setColorFilter(colour, PorterDuff.Mode.SRC_IN);
        }
        return drawable;
    }

    public static void GlideImageSetting(final Activity parentActivity, final Uri uri , final ImageView imageView){
        try{
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    GlideApp.with(parentActivity)
                            .asBitmap().format(PREFER_RGB_565)
                            .load(uri)
                            .placeholder(R.drawable.ic_loading_image)
                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                            .error(R.drawable.ic_not_found)
                            .into(imageView);
                }
            });
        }catch (Exception e){
            Crashlytics.logException(e);
        }
    }

    public static void GlideImageSettingBitmap(final Activity parentActivity, final Bitmap bitmap, final ImageView imageView){
        try{
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    GlideApp.with(parentActivity)
                            .asBitmap()
                            .load(bitmap)
                            .placeholder(R.drawable.ic_loading_image)
                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                            .error(R.drawable.ic_not_found)
                            .into(imageView);
                }
            });
        }catch (Exception e){
            Crashlytics.logException(e);
        }
    }

    public static void GlideCircleImageSetting(final Activity parentActivity, final Uri uri , final ImageView imageView){
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                GlideApp.with(parentActivity)
                        .asBitmap().format(PREFER_RGB_565)
                        .load(uri)
                        .circleCrop()
                        .placeholder(R.drawable.ic_loading_image)
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .error(R.drawable.ic_not_found)
                        .into(imageView);
            }
        });
    }
    public static void GlideCircleImageSettingGC(final Activity parentActivity, int setImageGroupAdd , final ImageView imageView){
        final Uri uri = null;
        uri.parse("");
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                GlideApp.with(parentActivity)
                        .asBitmap().format(PREFER_RGB_565)
                        .load(uri)
                        .circleCrop()
                        .placeholder(R.drawable.ic_groupchat_add)
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .error(R.drawable.bg_circle_bg)
                        .into(imageView);
            }
        });
    }

    public static void GlideImageSettingShrinkImage(final Activity parentActivity, final Uri uri, final ImageView imageView, final Marker marker){
        //has to be shrinked in google map marker because if not the image cant be shown
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //from here  https://mrfu.me/2016/02/27/Glide_Callbacks_SimpleTarget_and_ViewTarget_for_Custom_View_Classes/
                BaseTarget target = new BaseTarget<BitmapDrawable>() {
                    @Override
                    public void onResourceReady(BitmapDrawable bitmap, Transition<? super BitmapDrawable> transition) {
                        // do something with the bitmap
                        // for demonstration purposes, let's set it to an imageview
                        if(not_first_time_showing_info_window){
                            imageView.setImageDrawable(bitmap);
                            not_first_time_showing_info_window = false;
                        }
                        else{
                            not_first_time_showing_info_window = true;
                            imageView.setImageDrawable(bitmap);
                            marker.showInfoWindow();
                        }
                    }

                    @Override
                    public void getSize(SizeReadyCallback cb) {
                        cb.onSizeReady(SIZE_ORIGINAL, SIZE_ORIGINAL);
                    }

                    @Override
                    public void removeCallback(SizeReadyCallback cb) {}
                };

                GlideApp.with(parentActivity)
                        .load(uri)
                        .placeholder(R.drawable.ic_loading_image)
                        .into(target);
            }
        });

    }

    public static void GlideImageSettingOwnPlaceholder(final Activity parentActivity, final Uri uri , final ImageView imageView, final int drawable){
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                GlideApp.with(parentActivity)
                        .asBitmap().format(PREFER_RGB_565)
                        .load(uri)
                        .placeholder(drawable)
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .error(drawable)
                        .into(imageView);
            }
        });

    }

    public static void GlideImageSettingNoPlaceholder(final Activity parentActivity, final Uri uri , final ImageView imageView){
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                GlideApp.with(parentActivity)
                        .asBitmap().format(PREFER_RGB_565)
                        .load(uri)
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .into(imageView);
            }
        });

    }

    public static void GlideImageSettingOwnDrawable(final Activity parentActivity, final int drawable , final ImageView imageView){
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                GlideApp.with(parentActivity)
                        .asBitmap().format(PREFER_RGB_565)
                        .load(drawable)
                        .error(R.drawable.ic_not_found)
                        .into(imageView);
            }
        });
    }

    public static byte[] getCompressedBitmap(AppCompatActivity parentActivity, String filePath){
        // Get the data from an ImageView as bytes
        Bitmap bitmap  = BitmapFactory.decodeFile(filePath);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, baos);
        return baos.toByteArray();
    }

    public static byte[] getCompressedBitmapWithWatermark(AppCompatActivity parentActivity, String filePath){
        // Get the data from an ImageView as bytes
        Bitmap bitmap  = BitmapFactory.decodeFile(filePath);
        bitmap = addWatermark(parentActivity.getResources(),bitmap);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, baos);
        return baos.toByteArray();
    }

    public static Bitmap addWatermark(Resources res, Bitmap source) {
        int w, h;
        Canvas c;
        Paint paint;
        Bitmap bmp, watermark;
        Matrix matrix;
        float scale;
        RectF r;
        w = source.getWidth();
        h = source.getHeight();
        // Create the new bitmap
        bmp = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        paint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.DITHER_FLAG | Paint.FILTER_BITMAP_FLAG);
        // Copy the original bitmap into the new one
        c = new Canvas(bmp);
        c.drawBitmap(source, 0, 0, paint);
        // Load the watermark
        watermark = BitmapFactory.decodeResource(res, R.drawable.ic_watermark);
        // Scale the watermark to be approximately 40% of the source image height
        scale = (float) (((float) h * 0.4) / (float) watermark.getHeight());
        //this is extra from me to fix the scale too big bug
        scale *= 0.6;
        // Create the matrix
        matrix = new Matrix();
        matrix.postScale(scale, scale);
        // Determine the post-scaled size of the watermark
        r = new RectF(0, 0, watermark.getWidth(), watermark.getHeight());
        matrix.mapRect(r);
        // Move the watermark to the bottom right corner
        matrix.postTranslate(w - r.width(), h - r.height());
        // Draw the watermark
        c.drawBitmap(watermark, matrix, paint);
        // Free up the bitmap memory
        watermark.recycle();
        return bmp;
    }

    public static void showPicFromFilePath(ImageView imageView, String filePath){
        File imgFile = new File(filePath);

        if(imgFile.exists()){

            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            imageView.setImageBitmap(myBitmap);
        }
    }

    public static void clearGlideCacheMemory(final Activity parentActivity){
        new AsyncTask<Void, Void, String>() {
            @Override
            protected void onPreExecute() {
                //...
            }

            @Override
            protected String doInBackground(Void... params) {
                GlideApp.get(parentActivity).clearDiskCache();
                return null;
            }

            @Override
            protected void onPostExecute (String result)    {
                GlideApp.get(parentActivity).clearMemory();
            }

        }.execute();
    }

/************************************************************FORMS**************************************************************************************/
    public static void clearForm(ViewGroup group) {
        for (int i = 0, count = group.getChildCount(); i < count; ++i) {
            View view = group.getChildAt(i);
            if (view instanceof EditText) {
                ((EditText)view).setText("");
            }

            if(view instanceof ViewGroup && (((ViewGroup)view).getChildCount() > 0))
                clearForm((ViewGroup)view);
        }
    }

    public static void clearFormTV(ViewGroup group) {
        for (int i = 0, count = group.getChildCount(); i < count; ++i) {
            View view = group.getChildAt(i);
            if (view instanceof TextView) {
                ((TextView)view).setText("");
            }

            if(view instanceof ViewGroup && (((ViewGroup)view).getChildCount() > 0))
                clearFormTV((ViewGroup)view);
        }
    }

    public static void editRoomNameFunc(EditText editText, AppCompatActivity parentActivity){
        editText.requestFocus();
        editText.setSelectAllOnFocus(true);
        editText.selectAll();
        KeyboardUtilities.showSoftKeyboard(parentActivity,editText);
    }




/************************************************************ERROR CHECKING***************************************************************************************/
    public static Boolean checkAndReturnNum(int i, String errorMsg, AppCompatActivity parentActivity){
        if(i <= 0){
            openMessageNotificationDialog(parentActivity,errorMsg,R.drawable.notice_bad,"invalid_num");
            return false;
        }

        return true;
    }

    public static Boolean checkAndReturnNum(long i, String errorMsg, AppCompatActivity parentActivity){
        if(i <= 0){
            openMessageNotificationDialog(parentActivity,errorMsg,R.drawable.notice_bad,"invalid_num");
            return false;
        }

        return true;
    }

    public static Boolean checkAndReturnBedQTY(long i, String errorMsg, AppCompatActivity parentActivity){
        if(i > 16){
            openMessageNotificationDialog(parentActivity,errorMsg,R.drawable.notice_bad,"invalid_num");
            return false;
        }

        return true;
    }

    public static Boolean checkAndReturnRoomQTY(long i, String errorMsg, AppCompatActivity parentActivity){
        if(i > 8){
            openMessageNotificationDialog(parentActivity,errorMsg,R.drawable.notice_bad,"invalid_num");
            return false;
        }

        return true;
    }

    //2nd number should be more than or equals to 1st number
    public static Boolean compareAndReturnNum(double x, double y, String errorMsg, AppCompatActivity parentActivity){
        if(y < x){
            openMessageNotificationDialog(parentActivity,errorMsg,R.drawable.notice_bad,"invalid_num_range");
            return false;
        }

        return true;
    }

    public static Boolean checkAndReturnDateRange(long startDate, long endDate, String errorMsg, AppCompatActivity parentActivity){
        //start date should not be more than end date
        if(startDate > endDate){
            openMessageNotificationDialog(parentActivity,errorMsg,R.drawable.notice_bad,"invalid_date_range");
            return false;
        }

        return true;
    }

    public static Boolean checkAndReturnString(String s, String errorMsg, AppCompatActivity parentActivity){
        if(s == null || TextUtils.isEmpty(s.trim())){
            if(parentActivity != null && errorMsg != null){
                openMessageNotificationDialog(parentActivity,errorMsg,R.drawable.notice_bad,"invalid_string");
            }
            return false;
        }else{
            return true;
        }
    }

    public static Boolean checkAndReturnDouble(Double s, String errorMsg, AppCompatActivity parentActivity){
        if(s == null || (s <= 0.0)){
            if(parentActivity != null && errorMsg != null){
                openMessageNotificationDialog(parentActivity,errorMsg,R.drawable.notice_bad,"invalid_double");
            }
            return false;
        }else{
            return true;
        }
    }

    public static Boolean checkAndReturnSpinner(Spinner spinner, String errorMsg, AppCompatActivity parentActivity){
        if(spinner == null || TextUtils.isEmpty(spinner.getSelectedItem().toString().trim()) || spinner.getSelectedItemPosition() == 0){
            if(parentActivity != null){
                openMessageNotificationDialog(parentActivity,errorMsg,R.drawable.notice_bad,"invalid_string");
            }
            return false;
        }else{
            return true;
        }
    }

    public static Boolean checkAndReturnPassword(String s, String errorMsg, AppCompatActivity parentActivity){
        if(s == null || TextUtils.isEmpty(s.trim()) || s.trim().length() < 6){
            if(parentActivity != null){
                openMessageNotificationDialog(parentActivity,errorMsg,R.drawable.notice_bad,"invalid_string");
            }
            return false;
        }else{
            return true;
        }
    }

    public static Boolean checkAndReturnListSize(int size, String errorMsg, AppCompatActivity parentActivity){
        if(size <= 0){
            if(parentActivity != null && errorMsg != null){
                openMessageNotificationDialog(parentActivity,errorMsg,R.drawable.notice_bad,"invalid_array_list");
            }
            return false;
        }else{
            return true;
        }
    }

    public static Boolean checkAndReturnCheckboxes(ArrayList<CheckBox> checkBoxes, String errorMsg, AppCompatActivity parentActivity){
        for(CheckBox checkBox : checkBoxes){
            if(checkBox.isChecked()){
                return true;
            }
        }

        if(parentActivity != null && errorMsg != null){
            openMessageNotificationDialog(parentActivity,errorMsg,R.drawable.notice_bad,"invalid_checkbox");
        }
        return false;
    }

    public static boolean isValidEmail(String email, AppCompatActivity parentActivity) {
        boolean isValid = email != null && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();

        if(!isValid){
            openMessageNotificationDialog(parentActivity,parentActivity.getResources().getString(R.string.error_please_enter_valid_email),R.drawable.notice_bad,"invalid_email");
        }

        return isValid;
    }

    public static boolean checkAndReturnImageView(AppCompatActivity parentActivity, ImageView imageView, int resId, int stringId, boolean isFromDatabase){
        if(!isFromDatabase){
            if(imageView.getDrawable() == null){
                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(stringId),R.drawable.notice_bad,"invalid_image");
                return false;
            }else{
                return true;
            }
        }
        else{
            return true;
        }
    }

    public static boolean checkImageViewHasDrawable(AppCompatActivity parentActivity, ImageView imageView, int stringId, String photoPath){
            if(imageView.getDrawable() == null && photoPath == null){
                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(stringId),R.drawable.notice_bad,"invalid_image");
                return false;
            }else{
                return true;
            }


    }

    public static boolean checkAndReturnImageButton(AppCompatActivity parentActivity, ImageButton imageButton, int resId, int stringId, boolean isFromDatabase){
        if(!isFromDatabase){
            if(imageButton.getDrawable().getConstantState() == parentActivity.getResources().getDrawable(resId).getConstantState()){
                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(stringId),R.drawable.notice_bad,"invalid_image");
                return false;
            }else{
                return true;
            }
        }
        else{
            return true;
        }
    }

    public static boolean isGpsEnabled(AppCompatActivity parentActivity) {
        LocationManager locationManager = (LocationManager) parentActivity.getSystemService(Context.LOCATION_SERVICE);
        return locationManager != null && locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public static boolean isNetworkEnabled(AppCompatActivity parentActivity){
        LocationManager locationManager = (LocationManager)parentActivity.getSystemService(Context.LOCATION_SERVICE);
        return locationManager != null && locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    public static boolean isBookingHistoryValid(BookingHistory bookingHistory){
        return bookingHistory != null && bookingHistory.type != null && bookingHistory.targetId != null && bookingHistory.actionType != null;
    }

    public static boolean isBookingHomestayDetailsValid(BookHomestayDetails bookHomestayDetails){
        return bookHomestayDetails != null && bookHomestayDetails.targetId != null;
    }


/************************************************************FRAGMENTS***************************************************************************************/
    public static FragmentTransaction getFragmentTransaction(AppCompatActivity parentActivity){
        FragmentTransaction ft = parentActivity.getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.fragment_enter, R.anim.fragment_exit, R.anim.fragment_pop_enter, R.anim.fragment_pop_exit);
        return ft;
    }




/************************************************************ADAPTERS SUCH AS SPINNERS AND OTHERS***************************************************************************************/
    public static ArrayAdapter<String> getCustomSpinnerAdapter(final AppCompatActivity parentActivity, Spinner spinner, int stringArrayId, final View imeToNext){
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                parentActivity,R.layout.spinner_item,parentActivity.getResources().getStringArray(stringArrayId)){
            @Override
            public boolean isEnabled(int position){
                if(position == 0){
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                }
                else{
                    return true;
                }
            }
            @Override
            public View getDropDownView(final int position, final View convertView,
                                        @NonNull final ViewGroup parent) {
                final View view = super.getDropDownView(position, convertView, parent);
                parentActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        TextView tv = (TextView) view;
                        if(position == 0){
                            // Set the hint text color gray
                            tv.setTextColor(parentActivity.getResources().getColor(R.color.default_hint));
                            tv.setAlpha(0.75f);
                        }
                        else {
                            tv.setTextColor(Color.BLACK);
                        }
                    }
                });

                return view;
            }
        };

        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_dropdown);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(final AdapterView<?> parent, View view, final int position, long id) {
                parentActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        TextView tv = (TextView) parent.getChildAt(0);
                        if(position != 0){
                            tv.setTextColor(Color.BLACK);
                            tv.setAlpha(1f);
                            imeToNext.requestFocus();
                            imeToNext.performClick();
                        }
                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return spinnerArrayAdapter;
    }




/************************************************************FIRESTORE***************************************************************************************/
    public static FirebaseFirestore getFirestoreInstance(){
        FirebaseFirestore firestore = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setTimestampsInSnapshotsEnabled(true)
                .build();
        firestore.setFirestoreSettings(settings);

        //            Timestamp timestamp = snapshot.getTimestamp("created_at");
//            java.util.Date date = timestamp.toDate();
        return firestore;
    }

    //this is for getting error message only
    public static OnCompleteListener<Void> getOnCompleteListener(final AppCompatActivity parentActivity, final String TAG) {
        return new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.getException() != null){
                    GeneralFunction.openMessageNotificationDialog(parentActivity,task.getException().toString(),R.drawable.notice_bad,TAG);
                    Log.w(TAG, "Get failed.", task.getException());
                }
            }
        };
    }

    public static boolean hasErrorDocOnComplete(AppCompatActivity parentActivity,Task<Void> task,String TAG){
        if(task.getException() != null){
            GeneralFunction.openMessageNotificationDialog(parentActivity,task.getException().toString(),R.drawable.notice_bad,TAG);
            Log.w(TAG, "Get failed.", task.getException());
            return true;
        }
        return false;
    }

    public static boolean hasError(AppCompatActivity parentActivity, Task<QuerySnapshot> task, String TAG){
        if(!task.isSuccessful()){
            if(task.getException() != null){
                GeneralFunction.openMessageNotificationDialog(parentActivity,task.getException().toString(),R.drawable.notice_bad,TAG);
            }
            Log.w(TAG, "Get failed.", task.getException());
            return true;
        }else{
            return false;
        }
    }

    public static boolean hasErrorDoc(AppCompatActivity parentActivity, Task<DocumentSnapshot> task, String TAG){
        if(!task.isSuccessful()){
            if(task.getException() != null){
                GeneralFunction.openMessageNotificationDialog(parentActivity,task.getException().toString(),R.drawable.notice_bad,TAG);
            }
            Log.w(TAG, "Get failed.", task.getException());
            return true;
        }else{
            return false;
        }
    }

    public static boolean hasError(AppCompatActivity parentActivity, @javax.annotation.Nullable FirebaseFirestoreException e, String TAG){
        if(e != null){
            GeneralFunction.openMessageNotificationDialog(parentActivity,e.toString(),R.drawable.notice_bad,TAG);
            Log.e(TAG,e.toString());
            return true;
        }else{
            return false;
        }
    }


/************************************************************FIREBASE STORAGE***************************************************************************************/
    public static StorageReference getFirebaseStorageInstance(){
        return FirebaseStorage.getInstance().getReference();
    }




/************************************************************PROGRESS BAR**************************************************************************************/
    public static Double getCurrentProgress(UploadTask.TaskSnapshot taskSnapshot){
        return (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount() ;
    }

    public static void focusOnView(final ScrollView scrollView, final EditText editText){
        scrollView.post(new Runnable() {
            @Override
            public void run() {

                scrollView.scrollTo(0, editText.getBottom());

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        editText.performClick();
                    }
                }, 500);
            }
        });
    }


/************************************************************STRING/ARRAY MANIPULATION**************************************************************************************/
    //put false if is Tag Edit Text (when you want to format it as a Tag) else put true
    public static String formatList(ArrayList<String> arrayList, boolean needDeleteLastCharacter){
        StringBuilder allList = new StringBuilder();

        for(String aText : arrayList){
            allList.append(aText);
            allList.append(", ");
        }

        if(needDeleteLastCharacter){
            //(CRASH PROOFED) delete the last 2 characters (because i put ", ") by decreasing the string builder length and if its size is > 0
            allList.setLength(Math.max(allList.length() - 2, 0));
        }

        return String.valueOf(allList);
    }

    public static ArrayList<String> copyListIfMatchValue(AppCompatActivity parentActivity, HashMap<String,Object> selectedList, int defaultList){
        ArrayList<String> finalList = new ArrayList<>();

        for(String aValue : parentActivity.getResources().getStringArray(defaultList)){
            if(selectedList.containsKey(aValue)){
                finalList.add(aValue);
            }
        }

        return finalList;
    }

    public static ArrayList<String> copyListIfMatchValueQuerySnapshot(AppCompatActivity parentActivity, QueryDocumentSnapshot snapshot, int defaultList){
        ArrayList<String> finalList = new ArrayList<>();

        for(String aValue : parentActivity.getResources().getStringArray(defaultList)){
            if(snapshot.contains(aValue)){
                finalList.add(aValue);
            }
        }

        return finalList;
    }

    public static ArrayList<String> copyListIfMatchValueQuerySnapshot(AppCompatActivity parentActivity, DocumentSnapshot snapshot, int defaultList){
        ArrayList<String> finalList = new ArrayList<>();

        for(String aValue : parentActivity.getResources().getStringArray(defaultList)){
            if(snapshot.contains(aValue)){
                finalList.add(aValue);
            }
        }

        return finalList;
    }

    public static Job getJobscopeAndWorkIcons(MainActivity parentActivity, Job job){
        job.jobScopeIcon = new ArrayList<>();
        job.earnListIcon = new ArrayList<>();

        //jobscope list icons
        List<String> resJobscopeList = new ArrayList<>(Arrays.asList(parentActivity.getResources().getStringArray(R.array.jobscope_list)));
//        int[] resJobscopeIconList = parentActivity.getResources().getIntArray(R.array.jobscope_icon_list);
        TypedArray resJobscopeIconList = parentActivity.getResources().obtainTypedArray(R.array.jobscope_icon_list);

        if(job.jobScope != null){
            for(int i = 0; i < job.jobScope.size(); i++){
                if(resJobscopeList.contains(job.jobScope.get(i))){
                    int position = resJobscopeList.indexOf(job.jobScope.get(i));
                    if(position == -1){
                        job.jobScopeIcon.add(i,R.drawable.ic_not_found);
                    }else{
                        job.jobScopeIcon.add(i,resJobscopeIconList.getResourceId(position,R.drawable.ic_not_found));
                    }
                }else{
                    job.jobScopeIcon.add(i,R.drawable.ic_not_found);
                }
            }
        }

        resJobscopeIconList.recycle();

        //earning list icons
        List<String> resEarnList = new ArrayList<>(Arrays.asList(parentActivity.getResources().getStringArray(R.array.job_earning_list)));
        TypedArray resEarnIconList = parentActivity.getResources().obtainTypedArray(R.array.job_earning_icon_list);

        if(job.earnList != null){
            for(int i = 0; i < job.earnList.size(); i++){
                if(resEarnList.contains(job.earnList.get(i))){
                    int position = resEarnList.indexOf(job.earnList.get(i));
                    if(position == -1){
                        job.earnListIcon.add(i,R.drawable.ic_not_found);
                    }else{
                        job.earnListIcon.add(i,resEarnIconList.getResourceId(position,R.drawable.ic_not_found));
                    }

                    //if is other set to use othersDesc instead
                    if(TextUtils.equals(parentActivity.getString(R.string.job_earn_other),job.earnList.get(i))){
                        job.earnList.set(i,job.othersDesc);
                    }
                }else{
                    job.earnListIcon.add(i,R.drawable.ic_not_found);
                }
            }
        }
        resEarnIconList.recycle();

        return job;
    }

    public static String getCityNameByCoordinates(AppCompatActivity parentActivity, LatLng currentGps){
        Geocoder geocoder = new Geocoder(parentActivity, Locale.getDefault());
        List<Address> addresses;
        try {
            addresses = geocoder.getFromLocation(currentGps.latitude, currentGps.longitude, 1);

            if (addresses != null && addresses.size() > 0) {
                return addresses.get(0).getLocality();
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }


        return null;
    }

    public static String getStateNameByCoordinates(AppCompatActivity parentActivity, GeoPoint geoPoint){
        Geocoder geocoder = new Geocoder(parentActivity, Locale.getDefault());
        LatLng current = new LatLng(geoPoint.getLatitude(),geoPoint.getLongitude());
        List<Address> addresses;
        try {
            addresses = geocoder.getFromLocation(current.latitude, current.longitude, 1);

            if (addresses != null && addresses.size() > 0) {

                return addresses.get(0).getLocality()+", "+addresses.get(0).getAdminArea();
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }


        return null;
    }

    public static String getCountryStateByCoordinates(AppCompatActivity parentActivity, LatLng latLng){
        Geocoder geocoder = new Geocoder(parentActivity, Locale.getDefault());
        LatLng current = new LatLng(latLng.latitude,latLng.longitude);
        List<Address> addresses;
        try {
            addresses = geocoder.getFromLocation(current.latitude, current.longitude, 1);

            if (addresses != null && addresses.size() > 0) {

                return addresses.get(0).getAdminArea() + ", " + addresses.get(0).getCountryName();
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }


        return null;
    }


//    public static void getStateNameByCoordinates(final AppCompatActivity parentActivity, GeoPoint geoPoint, final VolleyCallback volleyCallback){
//        LatLng current = new LatLng(geoPoint.getLatitude(),geoPoint.getLongitude());
//        final HashMap<Integer, Result.AddressComponent[]> address = new HashMap<>();
//
//            new ReverseGeocoding(current.latitude, current.longitude, Config.GOOGLE_MAP_APY_KEY)
//                    .setLanguage("en")
//                    .setResultTypes(AddressTypes.LOCALITY,AddressTypes.LOCALITY)
//                    .setLocationTypes(LocationTypes.APPROXIMATE)
//                    .isSensor(true)
//                    .fetch(new Callback() {
//                        @Override
//                        public void onResponse(Response response) {
//                            if(response.getResults() != null){
//                                for (Result result : response.getResults()) {
//                                    Log.d(HostStayMainPageFragment.TAG, result.getFormattedAddress());
//                                    address.put(0, result.getAddressComponents(AddressTypes.LOCALITY));
//                                    address.put(1,  result.getAddressComponents(AddressTypes.ADMINISTRATIVE_AREA_LEVEL_1));
//                                }
//                                volleyCallback.onSuccessResponse(address);
////                                ((GeneralCallbacks)parentActivity).VolleyResponse(address); // This will make a callback to activity.
//                            }
//                        }
//
//                        @Override
//                        public void onFailed(Response response, IOException e) {
//                            Log.d(HostStayMainPageFragment.TAG, "Something went wrong");
//                        }
//                    });
//
//    }


    public interface VolleyCallback {
        void onSuccessResponse(HashMap result);
    }

    public static String getItineraryTitle(Itinerary itinerary){
        String title = null;

        if(itinerary.tripName != null && !itinerary.tripName.isEmpty()){
            title = itinerary.tripName;
        }else{
            if(itinerary.locationName != null){
                title = itinerary.locationName;
            }
        }

        return title;
    }

    public static String getHostStayTitle(Room room){
        String title = "";

        if(room.accommodationName != null && !room.accommodationName.isEmpty()){
            title = room.accommodationName;
        }else{
//            if(room.country != null){
//                title = room.country;
//            }
//
//            if(room.state != null && !room.state.isEmpty()){
//                if(title != null){
//                    title += " - ";
//                    title += room.state;
//                }else{
//                    title = room.state;
//                }
//            }
        }

        return title;
    }

    //this 1 shows price value
    public static String getHostStayPrice(AppCompatActivity parentActivity, Room room){
        String title = "";
        if(room.currencyValue != null && room.currencyType != null){
            Double foreignPrice = 0.00;
            try {
                foreignPrice = room.currencyValue;
            }catch (Exception e){
                Log.e("getHostStayPrice: ",e.getMessage());
            }
            String localCurrencyCode = getCurrencyCode(parentActivity);
            String convertedPrice = getDecimalFormat(2).format(CurrencyRate.getUserLocalPrice(parentActivity,localCurrencyCode,room.currencyType,foreignPrice));
            title = localCurrencyCode + " " + convertedPrice;
        }
        return title;
    }


    //this 1 shows price value
    public static String getTravelKitPrice(AppCompatActivity parentActivity, TravelKit travelKit, Double travelkitPrice){
        String title = "";
        if(travelkitPrice != null && travelKit.currencyCode != null){
            Double foreignPrice = 0.00;
            try {
                foreignPrice = travelkitPrice;
            }catch (Exception e){
                Log.e("getHostStayPrice: ",e.getMessage());
            }
            String localCurrencyCode = getCurrencyCode(parentActivity);
            String convertedPrice = getDecimalFormat(2).format(CurrencyRate.getUserLocalPrice(parentActivity,localCurrencyCode,travelKit.currencyCode,foreignPrice));
            title = localCurrencyCode + convertedPrice;
        }
        return title;
    }

    //this 1 shows price value
    public static String getItineraryPrice(AppCompatActivity parentActivity, Itinerary itinerary, Double useThisPriceInstead){
        if(itinerary == null || itinerary.currencyCode == null){
            return "";
        }

        Double price = null;
        if(itinerary.price != null){
            price = itinerary.price;
        }

        if(useThisPriceInstead != null){
            price = useThisPriceInstead;
        }

        if(price == null){
            return "";
        }

        String localCurrencyCode = getCurrencyCode(parentActivity);
        String convertedPrice = getDecimalFormat(2).format(CurrencyRate.getUserLocalPrice(parentActivity,localCurrencyCode,itinerary.currencyCode,price));
        return localCurrencyCode + convertedPrice;
    }

    public static void setHostStayFullTitleWithPrice(AppCompatActivity parentActivity, Room room, TextView textView){
        String price = GeneralFunction.getHostStayPrice(parentActivity,room);

        String title = GeneralFunction.getHostStayTitle(room);
        if(title.isEmpty()){
            title = "???";
        }

        if(!price.isEmpty()){
            price = price + " ";
            SpannableStringBuilder priceSpanBuilder = new SpannableStringBuilder(price + title);
            priceSpanBuilder.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, price.length() - 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            textView.setText(priceSpanBuilder);
        }else{
            textView.setText(title);
        }
    }

    //*****************READ MORE Feature on TextView Start********************/

    //BUTTON METHOD
    public static void makeTextViewResizableWithButtonMethod(final AppCompatActivity parentActivity, final TextView tv,
                                                             final int maxLine, final int oriMaxLine,
                                                             final String expandText, final boolean viewMore, final TextView viewMoreTV) {
        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {

                ViewTreeObserver obs = tv.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);

                final int tvLineCount = tv.getLayout().getLineCount();
                if (maxLine == 0) {
                    int lineEndIndex = tv.getLayout().getLineEnd(0);
                    String text = tv.getText().subSequence(0, lineEndIndex) + " ";
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                } else if (maxLine > 0 && tvLineCount > maxLine) {
                    int lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex - 3) + "...";
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                } else {
                    int lineEndIndex = tv.getLayout().getLineEnd(tvLineCount - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex).toString();
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                }

                if(maxLine >= tvLineCount){
                    viewMoreTV.setVisibility(View.GONE);
                }else{
                    viewMoreTV.setVisibility(View.VISIBLE);
                }

                viewMoreTV.setText(expandText);

                viewMoreTV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (viewMore) {
                            tv.setLayoutParams(tv.getLayoutParams());
                            tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                            makeTextViewResizableWithButtonMethod(parentActivity, tv, -1,oriMaxLine, parentActivity.getString(R.string.view_less), false,viewMoreTV);
                        } else {
                            tv.setLayoutParams(tv.getLayoutParams());
                            tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                            makeTextViewResizableWithButtonMethod(parentActivity, tv, oriMaxLine,oriMaxLine, parentActivity.getString(R.string.view_more), true,viewMoreTV);
                        }
                    }
                });
            }
        });

    }

    //HTML METHOD
    //from here https://stackoverflow.com/questions/19099296/set-text-view-ellipsize-and-add-view-more-at-end
    //can use this if want to use HTML version, because it won't go to next line in HTML mode since no <p> or something, so it looks ugly, so cant use this yet. Maybe can use on blog 1

    public static void makeTextViewResizable(final AppCompatActivity parentActivity, final TextView tv, final int maxLine, final String expandText, final boolean viewMore) {
        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {

                ViewTreeObserver obs = tv.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
                if (maxLine == 0) {
                    int lineEndIndex = tv.getLayout().getLineEnd(0);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(parentActivity, Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
                    int lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(parentActivity, Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else {
                    int lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(parentActivity, Html.fromHtml(tv.getText().toString()), tv, lineEndIndex, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                }
            }
        });

    }

    private static SpannableStringBuilder addClickablePartTextViewResizable(final AppCompatActivity parentActivity, final Spanned strSpanned, final TextView tv,
                                                                            final int maxLine, final String spanableText, final boolean viewMore) {
        String str = strSpanned.toString();
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);

        if (str.contains(spanableText)) {


            ssb.setSpan(new CustomClickableSpan(false){
                @Override
                public void onClick(@NonNull View widget) {
                    if (viewMore) {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        makeTextViewResizable(parentActivity, tv, -1, parentActivity.getString(R.string.view_less), false);
                    } else {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        makeTextViewResizable(parentActivity, tv, 3, parentActivity.getString(R.string.view_more), true);
                    }
                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), 0);

        }
        return ssb;

    }

    //*****************READ MORE Feature on TextView End**********************/

/************************************************************DATE TIME MANIPULATION**************************************************************************************/
    public static String formatMillisecondToXHoursXMinutes(long milliseconds){
        int hours   = (int) ((milliseconds / (1000*60*60)) % 24);
        int minutes = (int) ((milliseconds / (1000*60)) % 60);
        int seconds = (int) (milliseconds / 1000) % 60 ;

//        return String.format("%02d min, %02d sec",
//                TimeUnit.MILLISECONDS.toMinutes(millis),
//                TimeUnit.MILLISECONDS.toSeconds(millis) -
//                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))
//        );

        return String.format("%02d hours %02d mins",hours,minutes);
    }

    public static String formatDateToDateWithTime(Date date){
        return new SimpleDateFormat("hh:mma dd MMM yyyy ").format(date);
    }

    public static String formatDateToTime(Date dateObject) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mma");
        return dateFormat.format(dateObject);
    }

    public static String formatDateToTimeFromMilisec(long milisecond) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mma");
        return dateFormat.format(milisecond);
    }

    public static String formatDateToString(long timeInMilis) {
        return getSdf().format(timeInMilis);
    }

    public static String formatDateToStringOnlyDay(long timeInMilis) {
        return getSdfOnlyDate().format(timeInMilis);
    }

    public static String formatDateToStringWithoutYear(long timeInMilis) {
        return getSdfWithoutYear().format(timeInMilis);
    }

    public static String formatDateToStringWithSlash(long timeInMilis) {
        return getSdfWithSlash().format(timeInMilis);
    }

    public static long getNoofDaysInDate(long departDate, long returnDate){
        return TimeUnit.MILLISECONDS.toDays(returnDate - departDate) + 1;
    }

    public static SimpleDateFormat getSdf(){
        return new SimpleDateFormat("dd MMM yyyy");
    }

    public static SimpleDateFormat getSdfOnlyDate(){
        return new SimpleDateFormat("dd");
    }

    public static SimpleDateFormat getSdfWithoutYear(){
        return new SimpleDateFormat("dd MMM");
    }

    public static SimpleDateFormat getSdfWithSlash(){
        return new SimpleDateFormat("dd/MM/yyyy");
    }

    public static boolean isSearchedDateClashedWithOtherBookedDates(long bkChkInDate, long bkChkOutDate, long searchChkInDate, long searchChkOutDate){
        return (bkChkInDate >= searchChkInDate && bkChkInDate <= searchChkOutDate) ||
                (bkChkOutDate >= searchChkInDate && bkChkOutDate <= searchChkOutDate) ||
                (searchChkInDate >= bkChkInDate && searchChkInDate <= bkChkOutDate) ||
                (searchChkOutDate >= bkChkInDate && searchChkOutDate <= bkChkOutDate);
    }

    //this function returns a calendar that has everything set to 0 (Time zone is UTC, hour, minute, seconds, everything is 0, meaning at the start of the day)
    public static Calendar getDefaultUtcCalendar(int year, int month, int dayOfMonth){
        Calendar calendar = new GregorianCalendar();
        calendar.set(year,month,dayOfMonth,0,0,0);
        calendar.set(Calendar.MILLISECOND,0);
        calendar.setTimeZone(TimeZone.getTimeZone("UTC"));

        return calendar;
    }

    //this function returns a calendar that has everything set to 0 (Time zone is UTC, hour, minute, seconds, everything is 0, meaning at the start of the day)
    public static Calendar getDefaultUtcCalendar(){
        Calendar calendar = new GregorianCalendar();
        calendar.set(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH),0,0,0);
        calendar.set(Calendar.MILLISECOND,0);
        calendar.setTimeZone(TimeZone.getTimeZone("UTC"));

        return calendar;
    }

    public static Calendar getDefaultUtcCalendarFifteenDaysAgo(){
        Calendar calendar = new GregorianCalendar();
        calendar.set(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH),0,0,0);
        calendar.set(Calendar.MILLISECOND,0);
        calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
        calendar.add(Calendar.DATE, -15);

        return calendar;
    }

    public static CharSequence getTimeAgo(long miliseconds){
        return DateUtils.getRelativeTimeSpanString(miliseconds, System.currentTimeMillis(),DateUtils.MINUTE_IN_MILLIS);
    }

    public static Calendar toCalendar(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }

    public static Calendar formatLongToCalendar(long milliSeconds)
    {
        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return calendar;
    }

/************************************************************NUMBER MANIPULATION**************************************************************************************/
    //refer from here https://stackoverflow.com/questions/4753251/how-to-go-about-formatting-1200-to-1-2k-in-java
    private static final NavigableMap<Long, String> suffixes = new TreeMap<> ();
    static {
        suffixes.put(1_000L, "k");
        suffixes.put(1_000_000L, "M");
        suffixes.put(1_000_000_000L, "G");
        suffixes.put(1_000_000_000_000L, "T");
        suffixes.put(1_000_000_000_000_000L, "P");
        suffixes.put(1_000_000_000_000_000_000L, "E");
    }

    public static String shortenNumbersFormat(long value) {
        //Long.MIN_VALUE == -Long.MIN_VALUE so we need an adjustment here
        if (value == Long.MIN_VALUE) return shortenNumbersFormat(Long.MIN_VALUE + 1);
        if (value < 0) return "-" + shortenNumbersFormat(-value);
        if (value < 1000) return Long.toString(value); //deal with easy case

        Map.Entry<Long, String> e = suffixes.floorEntry(value);
        Long divideBy = e.getKey();
        String suffix = e.getValue();

        long truncated = value / (divideBy / 10); //the number part of the output times 10
        boolean hasDecimal = truncated < 100 && (truncated / 10d) != (truncated / 10);
        return hasDecimal ? (truncated / 10d) + suffix : (truncated / 10) + suffix;
    }

    //get number of decimal format based on the arguments
    public static DecimalFormat getDecimalFormat(int noOfDecimalPoints){
        StringBuilder pattern = new StringBuilder();
        pattern.append("0.");
        if(noOfDecimalPoints == 0){
            pattern.replace(0,pattern.length(),"0");
        }else{
            for(int i = 0; i < noOfDecimalPoints; i++){
                pattern.append("0");
            }
        }

        return new DecimalFormat(pattern.toString());
    }

/************************************************************CURRENCY MANIPULATION**************************************************************************************/
    public static Currency getCurrency(AppCompatActivity parentActivity){
        Currency currency = null;

        try {
            //this 1 gets locale from the user sim card
            TelephonyManager telephonyManager = (TelephonyManager) parentActivity.getSystemService(Context.TELEPHONY_SERVICE);
            if(telephonyManager != null){
                currency = Currency.getInstance(new Locale("", telephonyManager.getNetworkCountryIso()));
            }

            //if phone no sim card then use the phone's locale
            if(currency == null){
                Locale defaultLocale = Locale.getDefault();
                currency = Currency.getInstance(defaultLocale);
            }
        }catch (Exception e){
            Log.e("getCurrency",e.getMessage());
        }

        return currency;
    }

    public static String getCurrencyCode(AppCompatActivity parentActivity){
        String currencyCode = CurrencyRate.getPreferredCurrencyCodeFromSP(parentActivity);
        if(currencyCode != null){
            return currencyCode;
        }else{
            Currency currency = getCurrency(parentActivity);
            if(currency != null && currency.getCurrencyCode() != null){
                return currency.getCurrencyCode();
            }else{
                return "MYR";
            }
        }
    }

/************************************************************COLOUR MANIPULATION**************************************************************************************/
    public static int generateRandomColor(AppCompatActivity parentActivity, int colorResId) {
        final Random mRandom = new Random(System.currentTimeMillis());

        // This is the base color which will be mixed with the generated one
//        final int baseColor = Color.WHITE;
        int baseColor = ContextCompat.getColor(parentActivity,colorResId);

        final int baseRed = Color.red(baseColor);
        final int baseGreen = Color.green(baseColor);
        final int baseBlue = Color.blue(baseColor);

        final int red = (baseRed + mRandom.nextInt(256)) / 2;
        final int green = (baseGreen + mRandom.nextInt(256)) / 2;
        final int blue = (baseBlue + mRandom.nextInt(256)) / 2;

        return Color.rgb(red, green, blue);
    }

/************************************************************GET RANDOM NUMBERS**************************************************************************************/
    public static int generateRandomNumber(int inclusiveMin, int exclusiveMax) {
        //from here https://stackoverflow.com/questions/6029495/how-can-i-generate-random-number-in-specific-range-in-android
        //inclusive of min and exclusive of max
        //Wont work for negatives, If you need a negative number, use this and multiply by -1. You need a float, I think.

        return new Random().nextInt(exclusiveMax - inclusiveMin) + inclusiveMin;
    }

/************************************************************ANDROID STUFF**************************************************************************************/
    public static boolean isAppInForground(Context context){
        boolean isInForeground = false;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);

        List<ActivityManager.RunningAppProcessInfo> runningProcesses = null;
        if (am != null) {
            runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInForeground = true;
                        }
                    }
                }
            }
        }

        return isInForeground;
    }

/************************************************************CALCULATION, PIXELS, DP**************************************************************************************/
    public static int getPixelsFromDp(Context context, float dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int)(dp * scale + 0.5f);
    }

/************************************************************FRIENDS FRAGMENT**************************************************************************************/
    public static HashMap<String,Object> blockOrUnblockUser(MainActivity parentActivity, Friend friendStatus, User user, String status, String TAG){
        if(friendStatus == null || friendStatus.id == null || user == null || user.id == null){
            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.friend_err),R.drawable.notice_bad,TAG);
            return null;
        }

        HashMap<String,Object> tempFriendMap = Friend.toMap(friendStatus);
        tempFriendMap.put(user.id,true);
        tempFriendMap.put(parentActivity.uid,true);
        tempFriendMap.put(Friend.BLOCKED_ID,user.id);
        tempFriendMap.put(Friend.TARGET_ID,null);
        tempFriendMap.put(Friend.REQUESTOR_ID,null);
        tempFriendMap.put(Friend.STATUS,status);
        tempFriendMap.put(Friend.DATE_UPDATED, FieldValue.serverTimestamp());

        GeneralFunction.getFirestoreInstance()
                .collection(Friend.URL_FRIENDS)
                .document(friendStatus.id)
                .set(tempFriendMap)
                .addOnCompleteListener(GeneralFunction.getOnCompleteListener(parentActivity,TAG));

        return tempFriendMap;
    }

    public static HashMap<String,Object> acceptOrDeclineUser(MainActivity parentActivity, Friend friendStatus, String status, String TAG){
        if(friendStatus == null || friendStatus.id == null || friendStatus.requestorId == null || friendStatus.targetId == null){
            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.friend_err),R.drawable.notice_bad,TAG);
            return null;
        }

        if(TextUtils.equals(friendStatus.requestorId,parentActivity.uid) || !TextUtils.equals(friendStatus.targetId,parentActivity.uid)){
            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.friend_err_accept_decline),R.drawable.notice_bad,TAG);
            return null;
        }

        ArrayList<String> friendList = new ArrayList<>();
        friendList.add(friendStatus.requestorId);
        friendList.add(friendStatus.targetId);
        HashMap<String,Object> tempFriendMap = new HashMap<>();
        tempFriendMap.put(Friend.REQUESTOR_ID, null);
        tempFriendMap.put(Friend.TARGET_ID, null);
        tempFriendMap.put(Friend.BLOCKED_ID, null);
        tempFriendMap.put(Friend.FRIEND_LIST, friendList);
        tempFriendMap.put(friendStatus.requestorId,true);
        tempFriendMap.put(friendStatus.targetId,true);
        tempFriendMap.put(Friend.STATUS,status);
        tempFriendMap.put(Friend.DATE_CREATED,friendStatus.dateCreated);
        tempFriendMap.put(Friend.DATE_UPDATED,FieldValue.serverTimestamp());

        GeneralFunction.getFirestoreInstance()
                .collection(Friend.URL_FRIENDS)
                .document(friendStatus.id)
                .set(tempFriendMap)
                .addOnCompleteListener(GeneralFunction.getOnCompleteListener(parentActivity,TAG));

        return tempFriendMap;
    }

    public static HashMap<String,Object> addNewFriend(MainActivity parentActivity, Friend friendStatus, User user, String TAG){
        if(user == null || user.id == null){
            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.friend_err),R.drawable.notice_bad,TAG);
            return null;
        }

        if(TextUtils.equals(parentActivity.uid,user.id)){
            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.friend_err_same_user),R.drawable.notice_bad,TAG);
            return null;
        }

        Friend tempFriend = new Friend(user.id,parentActivity.uid);
        HashMap<String,Object> tempFriendMap = Friend.toMap(tempFriend);
        tempFriendMap.put(parentActivity.uid,true);
        tempFriendMap.put(user.id,true);
        tempFriendMap.put(Friend.STATUS,Friend.STATUS_PENDING);
        tempFriendMap.put(Friend.DATE_CREATED,FieldValue.serverTimestamp());
        tempFriendMap.put(Friend.DATE_UPDATED,FieldValue.serverTimestamp());

        if(friendStatus != null && friendStatus.id != null){
            GeneralFunction.getFirestoreInstance()
                    .collection(Friend.URL_FRIENDS)
                    .document(friendStatus.id)
                    .set(tempFriendMap)
                    .addOnCompleteListener(GeneralFunction.getOnCompleteListener(parentActivity,TAG));

        }else{
            GeneralFunction.getFirestoreInstance()
                    .collection(Friend.URL_FRIENDS)
                    .document()
                    .set(tempFriendMap)
                    .addOnCompleteListener(GeneralFunction.getOnCompleteListener(parentActivity,TAG));
        }

        return tempFriendMap;
    }

    public static boolean unfriendUser(MainActivity parentActivity, Friend friendStatus, String TAG){
        if(friendStatus == null || friendStatus.id == null){
            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.friend_err),R.drawable.notice_bad,TAG);
            return false;
        }

        GeneralFunction.getFirestoreInstance()
                .collection(Friend.URL_FRIENDS)
                .document(friendStatus.id)
                .delete()
                .addOnCompleteListener(GeneralFunction.getOnCompleteListener(parentActivity,TAG));

        return true;
    }

    //////////////////////////////////////////////////////TOAST////////////////////////////////////////////////////////////

    public static void Toast(Activity parentActivity,String text){
        Toast toast = Toast.makeText(parentActivity,text,Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM|Gravity.CENTER,0,100);
        toast.show();

    }

/**********************************************************SUBSCRIBE SUBSCRIPTION PUSH NOTIFICATION**************************************************************************************/
    public static void subscribeInbox(String uid, final String TAG){
        FirebaseMessaging.getInstance()
                .subscribeToTopic(uid + MyFirebaseMessagingService.TYPE_INBOX)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (!task.isSuccessful()) {
                            Log.e(TAG, "failed subscribe");
                        }
                    }
                });
    }

    public static void unsubscribeInbox(String uid, final String TAG){
        FirebaseMessaging.getInstance()
                .unsubscribeFromTopic(uid + MyFirebaseMessagingService.TYPE_INBOX)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (!task.isSuccessful()) {
                            Log.e(TAG, "failed unsubscribe");
                        }
                    }
                });
    }

    public static void subscribeChat(final String chatId, final String TAG){
        FirebaseMessaging.getInstance()
                .subscribeToTopic(chatId)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (!task.isSuccessful()) {
                            Log.e(TAG, "failed subscribe" + chatId);
                        }
                    }
                });
    }

    public static void unsubscribeChat(final String chatId, final String TAG){
        FirebaseMessaging.getInstance()
                .unsubscribeFromTopic(chatId)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (!task.isSuccessful()) {
                            Log.e(TAG, "failed unsubscribe" + chatId);
                        }
                    }
                });
    }

    //    public static void createArrayToSpinner(AppCompatActivity parentActivity, Spinner spinner, int myArray){
//        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(parentActivity,
//                myArray, android.R.layout.simple_list_item_1);
//        // Specify the layout to use when the list of choices appears
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        // Apply the adapter to the spinner
//        //spinner.setEnabled(false);
//        //spinner.setClickable(false);
//        spinner.setAdapter(adapter);
//
//        //custom spinner height below:
//        try {
//            Field popup = Spinner.class.getDeclaredField("mPopup");
//            popup.setAccessible(true);
//
//            // Get private mPopup member variable and try cast to ListPopupWindow
//            android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(spinner);
//
//            // Set popupWindow height to 500px
//            popupWindow.setHeight(500);
//        }
//        catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
//            // silently fail...
//        }
////        spinner.setSelection(adapter.getRoomPosition("6"));
//    }

    /**********************************************DEEP LINK********************************************************/
    public static void shareDeepLink(Activity parentActivity, String msg, Uri deepLink){
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, msg + deepLink);
        sendIntent.setType("text/plain");
        sendIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        parentActivity.startActivity(Intent.createChooser(sendIntent,parentActivity.getString(R.string.share_with)));
    }

    public static void listenForDeepLink(final AppCompatActivity parentActivity, final String TAG){
        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(parentActivity.getIntent())
                .addOnSuccessListener(parentActivity, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                        if (pendingDynamicLinkData == null) {
                            return;
                        }

                        Uri deepLink = pendingDynamicLinkData.getLink();

                        if(deepLink.getQueryParameter(Config.SP_DEEP_LINK_TARGET_ID) != null && deepLink.getQueryParameter(Config.SP_DEEP_LINK_TYPE) != null){
                            saveSPStringIntoKey(parentActivity,Config.SP_DEEP_LINK_TARGET_ID,deepLink.getQueryParameter(Config.SP_DEEP_LINK_TARGET_ID),Config.SP_DEEP_LINK_TARGET_ID);
                            saveSPStringIntoKey(parentActivity,Config.SP_DEEP_LINK_TYPE,deepLink.getQueryParameter(Config.SP_DEEP_LINK_TYPE),Config.SP_DEEP_LINK_TYPE);

                            //this 1 is for if user is in LoginActivity and maybe you want to show some message
//                            if(FirebaseAuth.getInstance().getCurrentUser() == null){
//                                getUserNameAndOpenDialogOnly(parentActivity,deepLink.getQueryParameter(Config.SP_DEEP_LINK_TARGET_ID),TAG);
//                            }
                        }
                    }
                })
                .addOnFailureListener(parentActivity, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "getDynamicLink:onFailure", e);
                    }
                });
    }

    public static Task<ShortDynamicLink> generateDeepLink(MainActivity parentActivity, Uri urlLink, String desc){
//        Task<ShortDynamicLink> dynamicLink = FirebaseDynamicLinks.getInstance().createDynamicLink()
        return FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(urlLink)
                .setDomainUriPrefix(Config.DEEP_LINK_DYNAMIC_LINK_DOMAIN)
                .setAndroidParameters(
                        new DynamicLink.AndroidParameters.Builder("com.vidatechft.yupa")
                                .setMinimumVersion(1)
                                .build())
//            .setIosParameters(
//                    new DynamicLink.IosParameters.Builder("com.vidatechft.yupa")
//                            .setAppStoreId("123456789")
//                            .setMinimumVersion("1.0.0")
//                            .build())
                .setGoogleAnalyticsParameters(
                        new DynamicLink.GoogleAnalyticsParameters.Builder()
                                .setSource("orkut")
                                .setMedium("social")
                                .setCampaign("invite_friend")
                                .build())
//            .setItunesConnectAnalyticsParameters(
//                    new DynamicLink.ItunesConnectAnalyticsParameters.Builder()
//                            .setProviderToken("123456")
//                            .setCampaignToken("example-promo")
//                            .build())
                .setSocialMetaTagParameters(
                        new DynamicLink.SocialMetaTagParameters.Builder()
                                .setTitle(parentActivity.getResources().getString(R.string.app_name))
                                .setDescription(desc)
                                .setImageUrl(Uri.parse(Config.DEEP_LINK_LOGO_URL))
                                .build())
                .buildShortDynamicLink();
    }

    //******************************************VIBRATION****************************************************//
    public static void vibratePhone(AppCompatActivity parentActivity){
        Vibrator v = (Vibrator) parentActivity.getSystemService(Context.VIBRATOR_SERVICE);
        if(v != null){
            // Vibrate for 500 milliseconds
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                v.vibrate(VibrationEffect.createOneShot(100, VibrationEffect.DEFAULT_AMPLITUDE));
            } else {
                //deprecated in API 26
                v.vibrate(100);
            }
        }
    }

    //*****************************************GET JSON COUNTRY DATA********************************************/

//    public static String GetCountryZipCode(AppCompatActivity parentActivity){
//        String CountryID="";
//        String CountryZipCode="";
//
//        TelephonyManager manager = (TelephonyManager) parentActivity.getSystemService(Context.TELEPHONY_SERVICE);
//        //getNetworkCountryIso
//        CountryID= manager.getSimCountryIso().toUpperCase();
//        String[] rl = parentActivity.getResources().getStringArray(R.array.CountryCodes);
//        for(int i=0;i<rl.length;i++){
//            String[] g=rl[i].split(",");
//            if(g[1].trim().equals(CountryID.trim())){
//                CountryZipCode=g[0];
//                break;
//            }
//        }
//        return CountryZipCode;
//    }



    //https://stackoverflow.com/questions/19945411/android-java-how-can-i-parse-a-local-json-file-from-assets-folder-into-a-listvi/19945484#19945484
    //Load JSON file from asset
    public static String loadJSONFromAsset(String countryJSON, AppCompatActivity parentActivity) {
        String json = null;
        try {
            InputStream is = parentActivity.getAssets().open(countryJSON);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    //*****************************************IS GUEST/IS ANONYMOUS********************************************/
    public static boolean isGuest(){
        try {
            if(FirebaseAuth.getInstance().getCurrentUser() == null){
                return true;
            }

            return FirebaseAuth.getInstance().getCurrentUser().isAnonymous();
        }catch (Exception e){
            e.printStackTrace();
            return true;
        }
    }

    //*******************************************FAVOURITE*************************************************************/
    public static void updateFavourite(final AppCompatActivity parentActivity, DocumentReference favRef, HashMap<String,Object> favMap, final Favourite favourite, final String TAG){
        favRef.set(favMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(GeneralFunction.hasErrorDocOnComplete(parentActivity,task,TAG)){
                    return;
                }

                //after complete setting the new favourite document, the following code is to do this:
                //Fixed when liking an itinerary in offline mode it will make duplicates
                GeneralFunction.getFirestoreInstance()
                        .collection(Favourite.URL_FAVOURITE)
                        .whereEqualTo(Favourite.TARGET_ID,favourite.targetId)
                        .whereEqualTo(Favourite.TYPE,Favourite.FAV_TYPE_ITI)
                        .whereEqualTo(Favourite.USER_ID,favourite.userId)
                        .orderBy(Favourite.DATE_UPDATED, Query.Direction.DESCENDING)
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                if(GeneralFunction.hasError(parentActivity,task,TAG)){
                                    return;
                                }

                                if(task.getResult() != null && task.getResult().size() > 1){
                                    //the first 1 will be the latest 1 because in descending order
                                    int docCount = 0;
                                    for(QueryDocumentSnapshot documentSnapshot : task.getResult()){
                                        if(docCount > 0){
                                            GeneralFunction.getFirestoreInstance()
                                                    .collection(Favourite.URL_FAVOURITE)
                                                    .document(documentSnapshot.getId())
                                                    .delete();
                                        }
                                        docCount++;
                                    }
                                }
                            }
                        });
            }
        });
    }
}
