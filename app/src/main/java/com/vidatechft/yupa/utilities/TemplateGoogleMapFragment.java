package com.vidatechft.yupa.utilities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.AppliedCandidate;
import com.vidatechft.yupa.classes.Chat;
import com.vidatechft.yupa.classes.Job;
import com.vidatechft.yupa.dialogFragments.ApplyPTJobDialogFragment;
import com.vidatechft.yupa.employerFragments.PTJobSwipeFragment;

import java.util.ArrayList;
import java.util.HashMap;

public class TemplateGoogleMapFragment extends Fragment implements OnMapReadyCallback {
    public static final String TAG = TemplateGoogleMapFragment.class.getName();
    private MainActivity parentActivity;
    private String lastToolbarTitle;
    private ArrayList<Job> jobs;
    private PTJobSwipeFragment parentFragment;

    private GoogleMap googleMap;
    private ClusterManager<Job> mClusterManager;

    //this is the marker's view
    private ViewGroup infoWindow;
    private TextView locationTV,earnTV,jobscopeTV,workspaceTV;
    private Job selectedJob;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static TemplateGoogleMapFragment newInstance(MainActivity parentActivity, PTJobSwipeFragment parentFragment, String lastToolbarTitle, ArrayList<Job> jobs) {
        TemplateGoogleMapFragment fragment = new TemplateGoogleMapFragment();

        fragment.parentActivity = parentActivity;
        fragment.lastToolbarTitle = lastToolbarTitle;
        fragment.jobs = jobs;
        fragment.parentFragment = parentFragment;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_ptjob_map, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.showBtmNav();

                SupportMapFragment mapFragment = (SupportMapFragment) TemplateGoogleMapFragment.this.getChildFragmentManager()
                        .findFragmentById(R.id.nearbyJobGoogleMap);
                mapFragment.getMapAsync(TemplateGoogleMapFragment.this);

                PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                        parentActivity.getFragmentManager().findFragmentById(R.id.nearbyJobMapPAF);
                autocompleteFragment.setHint(parentActivity.getString(R.string.pt_map_search_hint));

                autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
                    @Override
                    public void onPlaceSelected(Place place) {
                        final HashMap<String,LatLng> boundingBox = GeneralFunction.getBoundingBoxCoor(place.getLatLng(),Job.JOB_RADIUS);
                        GeoPoint btmLeft = new GeoPoint(boundingBox.get(Config.BOUNDING_BOX_BTM_LEFT).latitude,boundingBox.get(Config.BOUNDING_BOX_BTM_LEFT).longitude);
                        GeoPoint topRight = new GeoPoint(boundingBox.get(Config.BOUNDING_BOX_TOP_RIGHT).latitude,boundingBox.get(Config.BOUNDING_BOX_TOP_RIGHT).longitude);

                        GeneralFunction.getFirestoreInstance()
                                .collection(Job.URL_JOB)
                                .whereGreaterThan(Job.GPS,btmLeft)
                                .whereLessThan(Job.GPS,topRight)
                                .whereEqualTo(Job.IS_AVAILABLE,true)
                                .get()
                                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                        if(task.isSuccessful()){
                                            if(!task.getResult().isEmpty()){

                                                jobs.clear();

                                                for (QueryDocumentSnapshot snapshot : task.getResult()) {
                                                    jobs.add(GeneralFunction.getJobscopeAndWorkIcons(parentActivity,new Job(parentActivity,snapshot)));
                                                }

                                                populateMap(boundingBox);
                                            }else{
                                                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.pt_no_jobs_available_map),R.drawable.notice_bad,TAG);
                                            }
                                        }else{
                                            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.pt_err_loading_jobs_map),R.drawable.notice_bad,TAG);
                                        }
                                    }
                                });
                    }

                    @Override
                    public void onError(Status status) {
                        GeneralFunction.openMessageNotificationDialog(parentActivity,status.getStatusMessage(),R.drawable.notice_bad,TAG);
                    }
                });

            }
        });

        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        setupInfoWindow();
        populateMap(parentFragment.getCurrentBoundingBox());
    }

    private void setupInfoWindow(){
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                infoWindow = (ViewGroup)getLayoutInflater().inflate(R.layout.info_window_google_map_marker, null);
                earnTV = infoWindow.findViewById(R.id.earnTV);
                locationTV = infoWindow.findViewById(R.id.locationTV);
                workspaceTV = infoWindow.findViewById(R.id.workspaceTV);
                jobscopeTV = infoWindow.findViewById(R.id.jobscopeTV);

                TemplateGoogleMapFragment.this.googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                    @Override
                    public View getInfoWindow(final Marker marker) {
                        //the whole code in getInfoContents can be put inside here too (to eliminate the original paddings)
                        return null;
                    }

                    @Override
                    public View getInfoContents(final Marker marker) {
                        parentActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if(selectedJob != null){
                                    if(selectedJob.jobScope != null){
                                        jobscopeTV.setText(GeneralFunction.formatList(selectedJob.jobScope,true));
                                    }else{
                                        jobscopeTV.setText(parentActivity.getString(R.string.none));
                                    }

                                    if(selectedJob.earnList != null){
                                        earnTV.setText(GeneralFunction.formatList(selectedJob.earnList,true));
                                    }else{
                                        earnTV.setText(parentActivity.getString(R.string.none));
                                    }

                                    if(selectedJob.workspaceName != null){
                                        workspaceTV.setText(String.valueOf(selectedJob.workspaceName));
                                    }

                                    if(selectedJob.location != null){
                                        locationTV.setText(String.valueOf(selectedJob.location));
                                    }

                                    if(selectedJob.workspaceName == null && selectedJob.location == null){
                                        workspaceTV.setText(parentActivity.getString(R.string.none));
                                    }

                                }
                            }
                        });

                        if(selectedJob != null){
                            return infoWindow;
                        }else{
                            return null;
                        }
                    }
                });

                //if want to make a button clickable only instead of the whole window refer to the link below
                //from here https://stackoverflow.com/questions/14123243/google-maps-android-api-v2-interactive-infowindow-like-in-original-android-go/15040761#15040761
                TemplateGoogleMapFragment.this.googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                    @Override
                    public void onInfoWindowClick(Marker marker) {
                        if(selectedJob != null){
                            AppliedCandidate appliedCandidate = new AppliedCandidate(false,"", Chat.TYPE_JOB_DIRECT + selectedJob.employerId + parentActivity.uid);

                            if(parentActivity != null && !parentActivity.isFinishing() && GeneralFunction.isAppInForground(parentActivity.getApplicationContext())){
                                FragmentManager fm = parentActivity.getSupportFragmentManager();
                                ApplyPTJobDialogFragment applyPTJobDialogFragment = ApplyPTJobDialogFragment.newInstance(parentActivity,parentFragment,lastToolbarTitle,appliedCandidate,selectedJob,jobs,false);
                                applyPTJobDialogFragment.show(fm, TAG);
                            }
                        }
                    }
                });

                TemplateGoogleMapFragment.this.googleMap.setOnInfoWindowCloseListener(new GoogleMap.OnInfoWindowCloseListener() {
                    @Override
                    public void onInfoWindowClose(Marker marker) {
                        selectedJob = null;
                    }
                });
            }
        });
    }

    private void populateMap(final HashMap<String,LatLng> currentBoundingBox){
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                googleMap.clear();
                mClusterManager = new ClusterManager<>(parentActivity, googleMap);
                mClusterManager.setRenderer(new CustomMapRenderer<>(parentActivity, googleMap, mClusterManager));
                googleMap.setOnCameraIdleListener(mClusterManager);
                googleMap.setOnMarkerClickListener(mClusterManager);
                googleMap.getUiSettings().setZoomControlsEnabled(true);

                googleMap.addMarker(new MarkerOptions()
                        .title(parentActivity.getString(R.string.you_are_here))
                        .position(currentBoundingBox.get(Config.BOUNDING_BOX_CENTER))
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_gps)));

                if(!jobs.isEmpty()){
                    mClusterManager.addItems(jobs);

                    CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                            currentBoundingBox.get(Config.BOUNDING_BOX_CENTER), Job.JOB_ZOOM_IN_LEVEL);//the bigger the number, the deeper it goes
                    googleMap.animateCamera(location);

                    googleMap.addCircle(GeneralFunction.getMapCircle(currentBoundingBox.get(Config.BOUNDING_BOX_CENTER),Job.JOB_RADIUS,Config.MAP_COLOUR_GREEN));
                }else{
                    Toast.makeText(parentActivity,parentActivity.getString(R.string.pt_no_jobs_available),Toast.LENGTH_LONG).show();
                }

                mClusterManager.setOnClusterClickListener(new ClusterManager.OnClusterClickListener<Job>() {
                    @Override
                    public boolean onClusterClick(Cluster<Job> cluster) {
                        //zoom in the cluster to uncluster it
                        //from here https://stackoverflow.com/questions/25395357/android-how-to-uncluster-on-single-tap-on-a-cluster-marker-maps-v2
                        LatLngBounds.Builder builder = LatLngBounds.builder();
                        for (ClusterItem item : cluster.getItems()) {
                            builder.include(item.getPosition());
                        }
                        final LatLngBounds bounds = builder.build();
                        googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
                        return true;
                    }
                });

                mClusterManager.setOnClusterItemClickListener(new ClusterManager.OnClusterItemClickListener<Job>() {
                    @Override
                    public boolean onClusterItemClick(Job job) {
                        selectedJob = job;
                        return false;
                    }
                });
            }
        });
    }

    //because the default google map cluster manager will only cluster the marker if there is more than 4 marker on same position
    //so we need to overwrite it ourselves by using the class below
    class CustomMapRenderer<T extends ClusterItem> extends DefaultClusterRenderer<Job> {
        private final IconGenerator mClusterIconGenerator = new IconGenerator(parentActivity);
        public CustomMapRenderer(Context context, GoogleMap map, ClusterManager<Job> clusterManager) {
            super(context, map, clusterManager);
        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster<Job> cluster) {
            //start clustering if at least 2 items overlap
            return cluster.getSize() > 1;
        }

        //for single marker item
        @Override
        protected void onBeforeClusterItemRendered(final Job item, final MarkerOptions markerOptions) {
            super.onBeforeClusterItemRendered(item, markerOptions);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //if want to use own icon from drawable then use this
                    markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker_low));

                    //this was before i use the custom layout, if use the default 1 then uncomment this 2 lines
//            markerOptions.snippet(item.getSnippet());
//            markerOptions.title(item.getTitle());

                    //use this if want to use default marker but want to change the colour
//            BitmapDescriptor markerDescriptor = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED);
//            markerOptions.icon(markerDescriptor);
                }
            });
        }

        //for clustered item marker
        //Refer this https://codedump.io/share/JTvSCafSuuf7/1/android-maps-utils-cluster-icon-color
        //and this http://stackoverflow.com/questions/30967961/android-maps-utils-cluster-icon-color
        //for changing icons and colours of clustered markers and single markers with even more control
        @Override
        protected void onBeforeClusterRendered(final Cluster<Job> cluster, final MarkerOptions markerOptions){

            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //modify padding for one or two digit numbers
//            if (cluster.getSize() < 10) {
//                mClusterIconGenerator.setContentPadding(40, 20, 0, 0);
//            }
//            else {
//                mClusterIconGenerator.setContentPadding(30, 20, 0, 0);
//            }

                    //if just want straight change the icon use this
//            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.amu_bubble_mask));

                    Drawable clusterIcon;

                    if(cluster.getSize() <= 15){
                        clusterIcon = ContextCompat.getDrawable(parentActivity, R.drawable.ic_map_marker_low);
                    }
                    else if(cluster.getSize() > 15){
                        clusterIcon = ContextCompat.getDrawable(parentActivity, R.drawable.ic_map_marker_high);
                    }
                    else{
                        clusterIcon = ContextCompat.getDrawable(parentActivity, R.drawable.ic_map_marker_low);
                    }

                    //PorterDuff.Mode can refer to this link
                    // http://stackoverflow.com/questions/8280027/what-does-porterduff-mode-mean-in-android-graphics-what-does-it-do
                    //set icon to other colors
//            if (clusterIcon != null) {
//                clusterIcon.setColorFilter(ContextCompat.getColor(parentActivity, android.R.color.holo_red_light),
//                        PorterDuff.Mode.SRC_ATOP);
//            }

                    mClusterIconGenerator.setTextAppearance(R.style.clusterIconText);
                    mClusterIconGenerator.setBackground(clusterIcon);

                    //modify padding for one or two digit numbers
                    //change the text's position with padding in the cluster icon
//            if (cluster.getSize() < 10) {
//                mClusterIconGenerator.setContentPadding(40, 20, 0, 0); //L,R,T,B
//            }
//            else {
//                mClusterIconGenerator.setContentPadding(30, 20, 0, 0);
//            }

                    Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
                }
            });
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(lastToolbarTitle);
            }
        });
    }

    //needs this to prevent duplicate id of map and autocomplete that is making the app crash
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        MapFragment f = (MapFragment) parentActivity.getFragmentManager()
                .findFragmentById(R.id.nearbyJobGoogleMap);
        if (f != null)
            parentActivity.getFragmentManager().beginTransaction().remove(f).commit();

        PlaceAutocompleteFragment p = (PlaceAutocompleteFragment) parentActivity.getFragmentManager()
                .findFragmentById(R.id.nearbyJobMapPAF);
        if (p != null)
            parentActivity.getFragmentManager().beginTransaction().remove(p).commit();
    }
}
