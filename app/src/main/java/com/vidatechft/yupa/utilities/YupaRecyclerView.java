package com.vidatechft.yupa.utilities;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.widget.GridView;

public class YupaRecyclerView extends RecyclerView {
    public YupaRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public YupaRecyclerView(Context context) {
        super(context);
    }

    public YupaRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,
                MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, expandSpec);
    }
}