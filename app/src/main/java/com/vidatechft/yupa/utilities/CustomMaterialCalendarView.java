package com.vidatechft.yupa.utilities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateLongClickListener;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;
import com.prolificinteractive.materialcalendarview.OnRangeSelectedListener;
import com.vidatechft.yupa.CalendaUtilities.CustomMCVSelectorDecorator;
import com.vidatechft.yupa.CalendaUtilities.MySelectorDecorator;
import com.vidatechft.yupa.CalendaUtilities.TodayDecorator;
import com.vidatechft.yupa.R;

import org.threeten.bp.LocalDate;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static com.vidatechft.yupa.CalendaUtilities.CustomMCVSelectorDecorator.SELECTION_TYPE_LEFT;
import static com.vidatechft.yupa.CalendaUtilities.CustomMCVSelectorDecorator.SELECTION_TYPE_MIDDLE;
import static com.vidatechft.yupa.CalendaUtilities.CustomMCVSelectorDecorator.SELECTION_TYPE_RIGHT;
import static com.vidatechft.yupa.CalendaUtilities.CustomMCVSelectorDecorator.SELECTION_TYPE_ROUND;

//from here https://medium.com/@Sserra90/android-writing-a-compound-view-1eacbf1957fc
public class CustomMaterialCalendarView extends LinearLayout {
    public static final String TAG = CustomMaterialCalendarView.class.getName();
    public static final int SELECTION_MODE_DEFAULT = 0;
    public static final int SELECTION_MODE_SINGLE_RANGE = 1;
    public static final int SELECTION_MODE_MULTIPLE_RANGE = 2;

    public interface OnDateSelected {
        void onDateSelected(Date selectedDate);
        void onRangeSelected(ArrayList<Date> selectedDates, boolean isMultiRange); //if is multirange means that the date is UNSORTED. the list is added based on first clicked first tracked (unlike only one range, that 1 can get min and max date without sorting it, it already has a correct order)
        void onCanceled();
    }

    private TextView customSelectedYearTV,setDateTV;
    private MaterialCalendarView customMaterialCalendarView;
    private OnDateSelected onDateSelected;
    private List<CalendarDay> selectedCalendarDays = new ArrayList<>();
    private RelativeLayout customButtonRL;
    private AppCompatActivity parentActivity;
    private int selectionMode;

    private CalendarDay longClickedCalendarDay;
    private boolean isLongClicked;
    private HashMap<CalendarDay,Integer> multiRangeDayMap = new HashMap<>();

    public CustomMaterialCalendarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init(){
        //if dont have the lines setOrientation and setLayoutParams, the material calendar view wont have any content
        setOrientation(LinearLayout.VERTICAL);
//        setGravity(Gravity.CENTER);
        setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        //Inflate xml resource, pass "this" as the parent, we use <merge> tag in xml to avoid
        //redundant parent, otherwise a LinearLayout will be added to this LinearLayout ending up
        //with two view groups
        inflate(getContext(),R.layout.custom_material_calendar_view,this);

        setupCalendar();
        setupButton();
    }

    private void setupCalendar(){
        customSelectedYearTV = findViewById(R.id.customSelectedYearTV);
        customMaterialCalendarView = findViewById(R.id.customMaterialCalendarView);

        LocalDate localDate = LocalDate.now();

        customMaterialCalendarView.state().edit()
                .setMinimumDate(localDate)
                .commit();

        customMaterialCalendarView.addDecorators(new TodayDecorator());
        //to get today date and auto select
        customMaterialCalendarView.setSelectedDate(localDate);

        customSelectedYearTV.setText(String.valueOf(customMaterialCalendarView.getCurrentDate().getYear()));

        customMaterialCalendarView.setOnMonthChangedListener(new OnMonthChangedListener() {
            @Override
            public void onMonthChanged(MaterialCalendarView materialCalendarView, CalendarDay calendarDay) {
                customSelectedYearTV.setText(String.valueOf(calendarDay.getYear()));
            }
        });
    }

    private void setupButton(){
        ImageView customPrevYearIV = findViewById(R.id.customPrevYearIV);
        ImageView customNextYearIV = findViewById(R.id.customNextYearIV);
        TextView cancelTV = findViewById(R.id.cancelTV);
        TextView doneTV = findViewById(R.id.doneTV);
        customButtonRL = findViewById(R.id.customButtonRL);
        setDateTV = findViewById(R.id.setDateTV);

        customPrevYearIV.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onYearChanged(-1);
            }
        });

        customNextYearIV.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onYearChanged(1);
            }
        });

        cancelTV.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onDateSelected.onCanceled();
            }
        });

        doneTV.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<Date> selectedDate = getSelectedDate();

                if(selectedDate == null || selectedDate.size() == 0 || selectedDate.get(0) == null){
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_search_no_date_selected),R.drawable.notice_bad,TAG);
                    return;
                }

                boolean isMultiRange = false;
                if(selectionMode == SELECTION_MODE_MULTIPLE_RANGE){
                    isMultiRange = true;
                }

                if(selectedDate.size() > 1){
                    onDateSelected.onRangeSelected(selectedDate,isMultiRange);
                }else{
                    onDateSelected.onDateSelected(selectedDate.get(0));
                }
            }
        });
    }

    private void setupMultipleRange(){
        customMaterialCalendarView.clearSelection();
        customMaterialCalendarView.setLongClickable(true);
        customMaterialCalendarView.setSelectionMode(MaterialCalendarView.SELECTION_MODE_MULTIPLE);

        customMaterialCalendarView.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView materialCalendarView, @NonNull CalendarDay calendarDay, boolean b) {
                if(isLongClicked && longClickedCalendarDay != null){
                    Calendar tempCalendar = GeneralFunction.getDefaultUtcCalendar();
                    if(longClickedCalendarDay.isBefore(calendarDay)){
                        multiRangeDayMap.put(longClickedCalendarDay,SELECTION_TYPE_LEFT);
                        multiRangeDayMap.put(calendarDay,SELECTION_TYPE_RIGHT);

                        customMaterialCalendarView.setDateSelected(longClickedCalendarDay,true);
                        customMaterialCalendarView.setDateSelected(calendarDay,true);

                        while(!longClickedCalendarDay.equals(calendarDay)){
                            tempCalendar.set(longClickedCalendarDay.getYear(),longClickedCalendarDay.getMonth() - 1,longClickedCalendarDay.getDay());
                            tempCalendar.add(Calendar.HOUR_OF_DAY, 24);
                            longClickedCalendarDay = CalendarDay.from(tempCalendar.get(Calendar.YEAR),tempCalendar.get(Calendar.MONTH) + 1,tempCalendar.get(Calendar.DAY_OF_MONTH));

                            if(longClickedCalendarDay.equals(calendarDay)){
                                break;
                            }

                            multiRangeDayMap.put(longClickedCalendarDay,SELECTION_TYPE_MIDDLE);
                            customMaterialCalendarView.setDateSelected(longClickedCalendarDay,true);
                        }
                    }else{
                        multiRangeDayMap.put(calendarDay,SELECTION_TYPE_LEFT);
                        multiRangeDayMap.put(longClickedCalendarDay,SELECTION_TYPE_RIGHT);

                        customMaterialCalendarView.setDateSelected(calendarDay,true);
                        customMaterialCalendarView.setDateSelected(longClickedCalendarDay,true);

                        while(!longClickedCalendarDay.equals(calendarDay)){
                            tempCalendar.set(longClickedCalendarDay.getYear(),longClickedCalendarDay.getMonth() - 1,longClickedCalendarDay.getDay());
                            tempCalendar.add(Calendar.HOUR_OF_DAY, -24);//minus
                            longClickedCalendarDay = CalendarDay.from(tempCalendar.get(Calendar.YEAR),tempCalendar.get(Calendar.MONTH) + 1,tempCalendar.get(Calendar.DAY_OF_MONTH));

                            if(longClickedCalendarDay.equals(calendarDay)){
                                break;
                            }

                            multiRangeDayMap.put(longClickedCalendarDay,SELECTION_TYPE_MIDDLE);
                            customMaterialCalendarView.setDateSelected(longClickedCalendarDay,true);
                        }
                    }

                    isLongClicked = false;
                    longClickedCalendarDay = null;
                }else{
                    if(b){
                        multiRangeDayMap.put(calendarDay,SELECTION_TYPE_ROUND);
                        customMaterialCalendarView.setDateSelected(calendarDay,true);
                    }else{
                        multiRangeDayMap.remove(calendarDay);
                        customMaterialCalendarView.setDateSelected(calendarDay,false);
                    }
                }

                final CustomMCVSelectorDecorator customMCVSelectorDecorator = new CustomMCVSelectorDecorator(parentActivity,multiRangeDayMap);
                customMaterialCalendarView.addDecorators(customMCVSelectorDecorator);
            }
        });

        customMaterialCalendarView.setOnDateLongClickListener(new OnDateLongClickListener() {
            @Override
            public void onDateLongClick(@NonNull MaterialCalendarView materialCalendarView, @NonNull CalendarDay calendarDay) {
                longClickedCalendarDay = calendarDay;
                isLongClicked = true;
                customMaterialCalendarView.setDateSelected(calendarDay,true);

                GeneralFunction.vibratePhone(parentActivity);
            }
        });
    }

    private void setupSingleRange(){
        customMaterialCalendarView.setLongClickable(false);
        customMaterialCalendarView.setSelectionMode(MaterialCalendarView.SELECTION_MODE_RANGE);

        customMaterialCalendarView.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView materialCalendarView, @NonNull CalendarDay calendarDay, boolean b) {
                if(b){
                    selectedCalendarDays.clear();
                    selectedCalendarDays.add(calendarDay);
                    materialCalendarView.setSelectedDate(calendarDay);
                }
            }
        });

        customMaterialCalendarView.setOnRangeSelectedListener(new OnRangeSelectedListener() {
            @Override
            public void onRangeSelected(@NonNull MaterialCalendarView widget, @NonNull final List<CalendarDay> dates) {

                CalendarDay firstDay = dates.get(0);
                CalendarDay lastDay = dates.get(dates.size()-1);

                final HashMap<CalendarDay,Integer> customCalendarDayMap = new HashMap<>();
                //highlight the calendar date
                for(int i = 0; i< dates.size(); i++){
                    if(dates.size() == 1){
                        customCalendarDayMap.put(dates.get(i),SELECTION_TYPE_MIDDLE);
                    }
                    else{
                        if(dates.get(i).equals(firstDay)){
                            customCalendarDayMap.put(dates.get(i),SELECTION_TYPE_LEFT);
                        }
                        if(dates.get(i).equals(lastDay)){
                            customCalendarDayMap.put(dates.get(i),SELECTION_TYPE_RIGHT);
                        }
                        if(!dates.get(i).equals(firstDay) && !dates.get(i).equals(lastDay)){
                            customCalendarDayMap.put(dates.get(i),SELECTION_TYPE_MIDDLE);
                        }
                    }
                }
                selectedCalendarDays.addAll(dates);
                //solve the reverse date
                if(selectedCalendarDays.get(0).equals(firstDay)||selectedCalendarDays.get(0).equals(lastDay)){
                    selectedCalendarDays.remove(selectedCalendarDays.get(0));
                }

                final CustomMCVSelectorDecorator customMCVSelectorDecorator = new CustomMCVSelectorDecorator(parentActivity,customCalendarDayMap);
                customMaterialCalendarView.addDecorators(customMCVSelectorDecorator);

                widget.setOnDateChangedListener(new OnDateSelectedListener() {
                    @Override
                    public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                        if(selected){
                            customMaterialCalendarView.removeDecorator(customMCVSelectorDecorator);

                            selectedCalendarDays.clear();
                            selectedCalendarDays.add(date);
                        }else{
                            customMaterialCalendarView.removeDecorator(customMCVSelectorDecorator);

                            widget.setSelectedDate(date);
                            selectedCalendarDays.clear();
                            selectedCalendarDays.add(date);
                        }
                    }
                });
            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void onYearChanged(int diff){
        try {
            int newYear = Integer.parseInt(customSelectedYearTV.getText().toString()) + diff;
            customMaterialCalendarView.setCurrentDate(CalendarDay.from(newYear,
                    customMaterialCalendarView.getCurrentDate().getMonth(),
                    customMaterialCalendarView.getCurrentDate().getDay()
            ),true);
        }catch (Exception e){
            customSelectedYearTV.setText("1970");
            e.printStackTrace();
        }
    }

    public void setOnDateSelected(OnDateSelected onDateSelected) {
        this.onDateSelected = onDateSelected;
    }

    public String getYear(){
        return customSelectedYearTV.getText().toString();
    }

    public HashMap<CalendarDay,Integer> getSelectedMultiRangeDay(){
        return multiRangeDayMap;
    }

    public ArrayList<Date> getSelectedDate(){
        ArrayList<Date> selectedDates = new ArrayList<>();

        //make all the dates become default UTC
        for(CalendarDay calendarDay : selectedCalendarDays){
            selectedDates.add(GeneralFunction.getDefaultUtcCalendar(
                    calendarDay.getYear(),
                    calendarDay.getMonth() - 1, //need - 1 because the android Calendar class's month default starts at 0
                    calendarDay.getDay())
                    .getTime());
        }
        return selectedDates;
    }

    public void setNewAllSelectedDate(AppCompatActivity parentActivity, HashMap<CalendarDay,Integer> multiRangeDayMapFromUser){
        customMaterialCalendarView.clearSelection();
        multiRangeDayMap = multiRangeDayMapFromUser;
        HashMap<CalendarDay,Integer> tempMap = multiRangeDayMap; //if dont use this will have ConcurrentModificationException error when looping
        //        for(CalendarDay mCalendarDay : tempMap.keySet()){
        //            if(mCalendarDay != null){
        //                customMaterialCalendarView.setDateSelected(mCalendarDay,true);
        //            }
        //        }
        ArrayList<CalendarDay> mCalendarDayList = new ArrayList<>(tempMap.keySet());

        for(CalendarDay mCalendarDay : mCalendarDayList){
            customMaterialCalendarView.setDateSelected(mCalendarDay,true);
        }

        final CustomMCVSelectorDecorator customMCVSelectorDecorator = new CustomMCVSelectorDecorator(parentActivity,multiRangeDayMap);
        customMaterialCalendarView.addDecorators(customMCVSelectorDecorator);
    }

    public void hideButtons(){
        customButtonRL.setVisibility(View.GONE);
    }

    public void showButtons(){
        customButtonRL.setVisibility(View.VISIBLE);
    }

    public void changeSetDateTextOrHideSetDate(String setYourTextHere,boolean wantToHideSetDateJustPutTrueHere)
    {
        if(wantToHideSetDateJustPutTrueHere)
        {
            setDateTV.setVisibility(View.GONE);
        }
        else
        {
            setDateTV.setText(setYourTextHere);
        }
    }
    public void setTextSizeAndTextStyle(Float setTextSizeInSP,boolean makeItBoldText)
    {
        if(setTextSizeInSP != null)
        {
            setDateTV.setTextSize(TypedValue.COMPLEX_UNIT_SP, setTextSizeInSP);
        }
        if(makeItBoldText)
        {
            setDateTV.setTypeface(Typeface.DEFAULT_BOLD);
        }
    }

    public void setContext(AppCompatActivity parentActivity){
        this.parentActivity = parentActivity;
    }

    public int getSelectionMode() {
        return selectionMode;
    }

    public void setSelectionMode(int selectionMode) {
        this.selectionMode = selectionMode;
        if(selectionMode == SELECTION_MODE_MULTIPLE_RANGE){
            setupMultipleRange();
        }else{
            setupSingleRange();
        }
    }
}