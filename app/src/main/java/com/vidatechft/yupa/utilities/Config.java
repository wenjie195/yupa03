package com.vidatechft.yupa.utilities;

import android.graphics.Color;

public class Config {
    public static final String GOOGLE_MAP_APY_KEY = "AIzaSyAVWNpOzOPIVlRjdykFNCXrXgcx-GAYNLY";
    public static final int REQUEST_PLACE_PICKER = 263;
    public static final int REQUEST_CAMERA_AND_STORAGE_PERMISSION = 101;
    public static final int REQUEST_CALENDAR_PERMISSION = 103;
    public static final int REQUEST_GPS_PERMISSION = 102;
    public static final int REQUEST_PLACE_AUTOCOMPLETE = 1;
    public static final int REQUEST_PAYMENT = 8888;

    public static final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

    public static final String DONT_HAVE_ANY_MAP_APPLICATION = "You don't have any Map application!";

    public static final long GPS_UPDATE_INTERVAL = 10 * 1000;  /* 10 secs */
    public static final long GPS_UPDATE_INTERVAL_NEARBY_USER = 10 * 1000;  /* 10 secs */
    public static final long GPS_FASTEST_INTERVAL = 2000; /* 2 sec */
    public static final long GPS_FASTEST_INTERVAL_NEARBY_USER = 2000; /* 2 sec */
    public static final String BOUNDING_BOX_TOP_RIGHT = "topRight";
    public static final String BOUNDING_BOX_TOP_LEFT = "topLeft";
    public static final String BOUNDING_BOX_BTM_RIGHT = "btmRight";
    public static final String BOUNDING_BOX_BTM_LEFT = "btmLeft";
    public static final String BOUNDING_BOX_CENTER = "boxCenter";

    public static final int SEARCH_TYPE_HOST_ROOM = 1;
    public static final int SEARCH_TYPE_MAIN = 0;

    public static final int METER_MULTIPLIER = 500;
    public static final float MAP_ZOOM_IN_LEVEL_STATE = 13.5f;

    public static final String MAP_COLOUR_GREEN = "mapGreen";
    public static final int COLOUR_ARGB_DARK_GREEN = Color.argb(180,55, 149, 102);
    public static final int COLOUR_ARGB_LIGHT_TRANSPARENT_GREEN = Color.argb(90,55, 149, 102);
    public static final int COLOUR_ARGB_LIGHT_TRANSPARENT = Color.argb(0,0, 0, 0);

    //APP VERSION
    public static final String URL_APP_VERSION = "appVersion";

    //REGISTER ACCOUNT TYPE
    public static final String ACC_TYPE_GUEST = "guest";
    public static final String ACC_TYPE_ONETIME = "onetime";
    public static final String ACC_TYPE_FREQ = "freq";

    //INTENT
    public static final String ITINERARY_INTENT_ID = "itineraryIntent";
    public static final int INTENT_ITI_MORE_DETAIL_TO_CONTAINER = 333;
    public static final int INTENT_ITI_START_TO_MORE_DETAIL = 334;

    //GOOGLE API
    public static final String ADDRESS_COMPONENT = "address_component";
    public static final String ADDRESS_PLACE_NAME = "name";
    public static final String ADDRESS_GEOMETRY = "geometry";
    public static final String ADDRESS_DEFAULT_LANGUAGE = "en";//english
    public static final String PLACE_SEARCH_INPUT_TYPE_TEXT = "textquery";

    //DEEP LINK
    public static final String SP_DEEP_LINK_TARGET_ID = "targetId";
    public static final String SP_DEEP_LINK_TYPE = "type";
    public static final String SP_DEEP_LINK_TYPE_PROFILE = "typeProfile";
    public static final String SP_DEEP_LINK_TYPE_BLOG = "typeBlog";
    public static final String DEEP_LINK_DYNAMIC_LINK_DOMAIN = "https://yupa.page.link";
    //todo replace the below icon link for deep link with the image from google play store (dont know why, but doesnt work on google drive and firebase storage)
    public static final String DEEP_LINK_LOGO_URL = "https://lh3.googleusercontent.com/aJIG55XJxGpx218m7uT-FcmXdIPcKw9hAQKB7Ylq75A6LSms_HUoheXVAGzNEPDRbEQ=s180-rw";
    public static final String SP_MY_OWN_DEEP_LINK = "spMyDeepLink";

    public static final String CURRENCY_JSON_FILE= "currency.json";
    public static final String CURRENCY_ARRAY= "currency";

    //COUNTRY ARRAY FROM JSON FILE
    public static final String COUNTRY_SORT_NAME = "countrySortName";
    public static final String COUNTRY_NAME = "countryName";
    public static final String COUNTRY_CODE = "countryCode";
    public static final String COUNTRY_ID = "countryId";
    public static final String COUNTRY_JSON_FILE= "countries.json";
    public static final String COUNTRY_ARRAY= "countries";
    public static final String COUNTRY_SORT_NAME_JSON= "sortname";
    public static final String COUNTRY_NAME_JSON = "name";
    public static final String COUNTRY_CODE_JSON= "phoneCode";
    public static final String COUNTRY_ID_JSON = "id";

    //STATE ARRAY FROM JSON FILE
    public static final String STATE_JSON_FILE= "states.json";
    public static final String STATE_ARRAY= "states";
    public static final String STATE_ID_JSON = "id";
    public static final String STATE_NAME_JSON = "name";
    public static final String STATE_COUNTRY_ID_JSON = "country_id";
    public static final String STATE_ID = "stateId";
    public static final String STATE_NAME = "stateName";
    public static final String STATE_COUNTRY_ID = "stateCountryId";


}
