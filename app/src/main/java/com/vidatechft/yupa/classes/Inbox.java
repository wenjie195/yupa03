package com.vidatechft.yupa.classes;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import com.google.firebase.Timestamp;
import com.google.firebase.database.PropertyName;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.ServerTimestamp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vidatechft.yupa.R;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class Inbox implements Comparable<Inbox>{
    //todo apply inbox feature to hotel, add friend and chat message too because now it only works for booking homestay since i didnt do it for others yet in cloud functions
    public static final String URL_INBOX = "inbox";

    public static final String ACTION_TYPE_BOOKING_PAID = BookingHistory.ACTION_TYPE_PAID;
    public static final String ACTION_TYPE_BOOKING_ACCEPTED = BookingHistory.ACTION_TYPE_ACCEPTED;
    public static final String ACTION_TYPE_BOOKING_REJECTED = BookingHistory.ACTION_TYPE_REJECTED;
    public static final String ACTION_TYPE_BOOKING_PENDING = BookingHistory.ACTION_TYPE_PENDING;
    public static final String ACTION_TYPE_BOOKING_CANCEL = BookingHistory.ACTION_TYPE_CANCELED;

    public static final String ACTION_TYPE_FRIEND_PENDING = Friend.STATUS_PENDING;
    public static final String ACTION_TYPE_FRIEND_BLOCKED = Friend.STATUS_BLOCKED;
    public static final String ACTION_TYPE_FRIEND_UNBLOCKED = Friend.STATUS_UNBLOCKED;
    public static final String ACTION_TYPE_FRIEND_ACCEPTED = Friend.STATUS_ACCEPTED;
    public static final String ACTION_TYPE_FRIEND_REJECTED = Friend.STATUS_REJECTED;

    public static final String INBOX_TYPE_HOMESTAY = BookingHistory.BOOK_TYPE_HOMESTAY;
    public static final String INBOX_TYPE_HOTEL = BookingHistory.BOOK_TYPE_HOTEL;
    public static final String INBOX_TYPE_FRIEND = "fren";

    public static final String INBOX_TYPE = "inboxType";//this means is it got people friend? or got host accepted the booking or is it hotel 1?
    public static final String ACTION_TYPE = "actionType";
    public static final String TRIGGER_USER_ID = "triggerUId";//the one that performs something like adding friend (can be a host too)
    public static final String TARGET_ID = "targetId";//the target id like accomodation id or hotel id or other user id
    public static final String REMARK = "remark";//the remark from booking history?
    public static final String IS_READ = "isRead";//the remark from booking history?
    public static final String DATE_CREATED = "dateCreated";
    public static final String DATE_UPDATED = "dateUpdated";

    @Exclude
    public String id;

    @Exclude
    public boolean needShowToUser;

    @Exclude
    public Room room;

    @Exclude
    public User otherUser;

    @PropertyName(INBOX_TYPE)
    @SerializedName(INBOX_TYPE)
    @Expose
    public String inboxType;

    @PropertyName(ACTION_TYPE)
    @SerializedName(ACTION_TYPE)
    @Expose
    public String actionType;

    @PropertyName(TRIGGER_USER_ID)
    @SerializedName(TRIGGER_USER_ID)
    @Expose
    public String triggerUId;

    @PropertyName(TARGET_ID)
    @SerializedName(TARGET_ID)
    @Expose
    public String targetId;

    @PropertyName(REMARK)
    @SerializedName(REMARK)
    @Expose
    public String remark;

    @PropertyName(IS_READ)
    @SerializedName(IS_READ)
    @Expose
    public Boolean isRead;

    @ServerTimestamp
    @PropertyName(DATE_CREATED)
    @SerializedName(DATE_CREATED)
    @Expose
    public Timestamp dateCreated;

    @ServerTimestamp
    @PropertyName(DATE_UPDATED)
    @SerializedName(DATE_UPDATED)
    @Expose
    public Timestamp dateUpdated;

    public Inbox(){

    }

    public Inbox differentiateInbox(Inbox inbox){



        return inbox;
    }

    //from here https://stackoverflow.com/questions/5927109/sort-objects-in-arraylist-by-date
    @Override
    public int compareTo(@NonNull Inbox inbox) {
        if (dateCreated == null || inbox.dateCreated == null)
            return 0;
        return inbox.dateCreated.compareTo(dateCreated);//descending order, newest is at top
    }
}
