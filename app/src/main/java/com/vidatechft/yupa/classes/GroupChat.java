package com.vidatechft.yupa.classes;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.database.PropertyName;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.ServerTimestamp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GroupChat
{
    public static final String URL_GROUP_CHAT = "groupchat";
    public static final String URL_GROUP_CHAT_MEMBERS = "members";
    public static final String URL_GROUP_CHAT_PIC = "groupChatPic";
    public static final String URL_GROUP_CHAT_PICTURE = "groupProfilePic";
    public static final String URL_GROUP_CHAT_PROFILE_PICTURE = "profilePic";
    public static final String URL_GROUP_CHAT_SHOWCASE_PICTURE = "groupShowcase";
    public static final String URL_JOIN_STATUS = "joinStatus";

    public static final String LAST_UID = "lastUid";
    public static final String LAST_TYPE = "lastType";
    public static final String MESSAGE_TYPE_TEXT = "text";
    public static final String MESSAGE_TYPE_IMG = "image";
    public static final String MESSAGE_TYPE_AUDIO = "audio";
    public static final String MESSAGE_TYPE_ICONS = "icons";

    public static final String GROUP_CHAT_ID = "Id";
    public static final String GROUP_CHAT_NAME = "groupName";
    public static final String GROUP_CHAT_DESCRIPTION = "groupDescription";
    public static final String GROUP_CHAT_TYPE = "groupType";
    public static final String GROUP_CHAT_USER_STATUS = "status";
    public static final String GROUP_CHAT_NO_OF_MEMBERS = "NoOfMembers";
    public static final String GROUP_CHAT_LAST_MESSAGE = "lastMessage";
    public static final String GROUP_CHAT_DATE_CREATED = "dateCreated";
    public static final String GROUP_CHAT_LAST_UPDATED = "lastUpdated";
    public static final String GROUP_CHAT_IS_PUBLIC = "isPublic";
    public static final String GROUP_CHAT_NEED_APPROVAL = "needApproval";
    public static final String GROUP_CHAT_IS_PENDING = "pendingMembers";
    public static final String GROUP_CHAT_IS_ALLOWED = "isAllowed";
    public static final String GROUP_CHAT_IS_INVITED = "isInvited";
    public static final String GROUP_CHAT_FILEPATH_SHOWCASE = "filePathShowcase";



//    @Exclude
//    public Map<String, Object> groupChatDetailsInsideMembers;
    //todo : Determine create group / update group
    @Exclude
    public Boolean isCreategroup = false;

    //todo : Current user membership details
    @Exclude
    public Map<String,Object> currentUserMembershipDetails = new HashMap<>();

    //todo : List all of members` membership details
    @Exclude
    public Map<String,Map> userMembershipDetails = new HashMap<>();

    //todo : List all of members id`s inside the group
    @Exclude
    public ArrayList<String> groupMemberUid = new ArrayList<>();

    //todo : Differentiate GroupChatHomeFragment between members view or strangers(non-members) view
    @Exclude
    public Boolean viewGroup;

    @PropertyName(GROUP_CHAT_FILEPATH_SHOWCASE)
    @SerializedName(GROUP_CHAT_FILEPATH_SHOWCASE)
    @Expose
    public ArrayList<String> filePathPicList = new ArrayList<>();

    @PropertyName(GROUP_CHAT_NAME)
    @SerializedName(GROUP_CHAT_NAME)
    @Expose
    public String groupChatName;

    @PropertyName(URL_GROUP_CHAT_PIC)
    @SerializedName(URL_GROUP_CHAT_PIC)
    @Expose
    public String groupChatPic;

    @PropertyName(GROUP_CHAT_DESCRIPTION)
    @SerializedName(GROUP_CHAT_DESCRIPTION)
    @Expose
    public String groupChatDescription;

    @PropertyName(GROUP_CHAT_TYPE)
    @SerializedName(GROUP_CHAT_TYPE)
    @Expose
    public String groupChatType;

    @PropertyName(GROUP_CHAT_USER_STATUS)
    @SerializedName(GROUP_CHAT_USER_STATUS)
    @Expose
    public String status;

    @PropertyName(GROUP_CHAT_ID)
    @SerializedName(GROUP_CHAT_ID)
    @Expose
    public String groupChatId;

    @PropertyName(GROUP_CHAT_NO_OF_MEMBERS)
    @SerializedName(GROUP_CHAT_NO_OF_MEMBERS)
    @Expose
    public String groupChatNoOfMembers;

    @PropertyName(GROUP_CHAT_LAST_MESSAGE)
    @SerializedName(GROUP_CHAT_LAST_MESSAGE)
    @Expose
    public String groupChatLastMessage;

    @ServerTimestamp
    @PropertyName(GROUP_CHAT_DATE_CREATED)
    @SerializedName(GROUP_CHAT_DATE_CREATED)
    @Expose
    public Timestamp groupChatDateCreated;

    @ServerTimestamp
    @PropertyName(GROUP_CHAT_LAST_UPDATED)
    @SerializedName(GROUP_CHAT_LAST_UPDATED)
    @Expose
    public Timestamp groupChatLastUpdated;

    @PropertyName(GROUP_CHAT_IS_PUBLIC)
    @SerializedName(GROUP_CHAT_IS_PUBLIC)
    @Expose
    public Boolean isPublic;

    @PropertyName(GROUP_CHAT_NEED_APPROVAL)
    @SerializedName(GROUP_CHAT_NEED_APPROVAL)
    @Expose
    public Boolean needApproval;

    FirebaseFirestore db = FirebaseFirestore.getInstance();

    public GroupChat()
    {

    }
    //todo : To create a group
    public GroupChat(Boolean isCreategroup)
    {
        this.isCreategroup = isCreategroup;
    }

    public GroupChat(QueryDocumentSnapshot snapshot)
    {
        this.groupChatPic = snapshot.getString(GroupChat.URL_GROUP_CHAT_PIC);
        this.groupChatName = snapshot.getString(GroupChat.GROUP_CHAT_NAME);
        this.groupChatDescription = snapshot.getString(GroupChat.GROUP_CHAT_DESCRIPTION);
        this.groupChatLastMessage = snapshot.getString(GroupChat.GROUP_CHAT_LAST_MESSAGE);
        this.groupChatId = snapshot.getId();
        this.groupChatDateCreated = snapshot.getTimestamp(GroupChat.GROUP_CHAT_DATE_CREATED);
        this.groupChatLastUpdated = snapshot.getTimestamp(GroupChat.GROUP_CHAT_LAST_UPDATED);
        this.isPublic = snapshot.getBoolean(GroupChat.GROUP_CHAT_IS_PUBLIC);
        this.needApproval = snapshot.getBoolean(GroupChat.GROUP_CHAT_NEED_APPROVAL);

        if(snapshot.contains(GroupChat.GROUP_CHAT_FILEPATH_SHOWCASE) && snapshot.get(GroupChat.GROUP_CHAT_FILEPATH_SHOWCASE) != null)
        {
            List<String> filePaths = (List<String>) snapshot.get(GroupChat.GROUP_CHAT_FILEPATH_SHOWCASE);

            for(int i = 0 ; i < filePaths.size(); i++ )
            {
                this.filePathPicList.add(filePaths.get(i));
            }
        }
    }

    public GroupChat(DocumentSnapshot documentSnapshot)
    {
        this.groupChatPic = documentSnapshot.getString(GroupChat.URL_GROUP_CHAT_PIC);
        this.groupChatName = documentSnapshot.getString(GroupChat.GROUP_CHAT_NAME);
        this.groupChatDescription = documentSnapshot.getString(GroupChat.GROUP_CHAT_DESCRIPTION);
        this.groupChatLastMessage = documentSnapshot.getString(GroupChat.GROUP_CHAT_LAST_MESSAGE);
        this.groupChatId = documentSnapshot.getId();
        this.groupChatDateCreated = documentSnapshot.getTimestamp(GroupChat.GROUP_CHAT_DATE_CREATED);
        this.groupChatLastUpdated = documentSnapshot.getTimestamp(GroupChat.GROUP_CHAT_LAST_UPDATED);
        this.isPublic = documentSnapshot.getBoolean(GroupChat.GROUP_CHAT_IS_PUBLIC);
        this.needApproval = documentSnapshot.getBoolean(GroupChat.GROUP_CHAT_NEED_APPROVAL);

        if(documentSnapshot.contains(GroupChat.GROUP_CHAT_FILEPATH_SHOWCASE) && documentSnapshot.get(GroupChat.GROUP_CHAT_FILEPATH_SHOWCASE) != null)
        {
            List<String> filePaths = (List<String>) documentSnapshot.get(GroupChat.GROUP_CHAT_FILEPATH_SHOWCASE);

            for(int i = 0 ; i < filePaths.size(); i++ )
            {
                this.filePathPicList.add(filePaths.get(i));
            }
        }
    }
}