package com.vidatechft.yupa.classes;

import android.support.v7.app.AppCompatActivity;

import com.google.firebase.Timestamp;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.PropertyName;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.ServerTimestamp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@IgnoreExtraProperties
public class BookHomestayDetails {
    public static final String URL_BOOK_GUEST = "bookHomestay";
    public static final String URL_GUEST_LICENSE = "guestLicense";

    public static final String ID = "id";
    public static final String USER_ID = "userId";
    public static final String HOST_ID = "hostId";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String EMAIL = "email";
    public static final String COUNTRY = "country";
    public static final String COUNTRY_CODE = "countryCode";
    public static final String CONTACT_NO = "contactNo";
    public static final String TOTAL_GUEST = "totalGuest";
    public static final String PURPOSE = "purpose";
    public static final String LICENSE_URL = "licenseUrl";
    public static final String TYPE = "type";
    public static final String ACTION_TYPE = "actionType";
    public static final String REMARK = "remark";
    public static final String TARGET_ID = "targetId";
    public static final String CHECKIN_DATE = "checkinDate";
    public static final String CHECKOUT_DATE = "checkoutDate";
    public static final String DATE_ACTION = "dateAction";
    public static final String DATE_CREATED = "dateCreated";
    public static final String DATE_UPDATED = "dateUpdated";

    public static final String CURRENT_GUEST = "currentGuest";
    public static final String UPCOMING_GUEST = "upcomingGuest";
    public static final String PREVOIUS_GUEST = "previousGuest";


    @Exclude
    public String id;

    @PropertyName(USER_ID)
    @SerializedName(USER_ID)
    @Expose
    public String userId;

    @PropertyName(HOST_ID)
    @SerializedName(HOST_ID)
    @Expose
    public String hostId;

    @PropertyName(FIRST_NAME)
    @SerializedName(FIRST_NAME)
    @Expose
    public String firstName;

    @PropertyName(LAST_NAME)
    @SerializedName(LAST_NAME)
    @Expose
    public String lastName;

    @PropertyName(EMAIL)
    @SerializedName(EMAIL)
    @Expose
    public String email;

    @PropertyName(COUNTRY)
    @SerializedName(COUNTRY)
    @Expose
    public String country;

    @PropertyName(CONTACT_NO)
    @SerializedName(CONTACT_NO)
    @Expose
    public String contactNo;

    @PropertyName(TOTAL_GUEST)
    @SerializedName(TOTAL_GUEST)
    @Expose
    public int totalGuest;

    @PropertyName(PURPOSE)
    @SerializedName(PURPOSE)
    @Expose
    public String purpose;

    @PropertyName(LICENSE_URL)
    @SerializedName(LICENSE_URL)
    @Expose
    public String licenseUrl;

    @PropertyName(TYPE)
    @SerializedName(TYPE)
    @Expose
    public String type;

    @PropertyName(ACTION_TYPE)
    @SerializedName(ACTION_TYPE)
    @Expose
    public String actionType;

    @PropertyName(REMARK)
    @SerializedName(REMARK)
    @Expose
    public String remark;

    @PropertyName(TARGET_ID)
    @SerializedName(TARGET_ID)
    @Expose
    public String targetId;

    @PropertyName(COUNTRY_CODE)
    @SerializedName(COUNTRY_CODE)
    @Expose
    public String countryCode;

    @PropertyName(CHECKIN_DATE)
    @SerializedName(CHECKIN_DATE)
    @Expose
    public Long checkinDate;

    @PropertyName(CHECKOUT_DATE)
    @SerializedName(CHECKOUT_DATE)
    @Expose
    public Long checkoutDate;

    @ServerTimestamp
    @PropertyName(DATE_ACTION)
    @SerializedName(DATE_ACTION)
    @Expose
    public Timestamp dateAction;

    @ServerTimestamp
    @PropertyName(DATE_CREATED)
    @SerializedName(DATE_CREATED)
    @Expose
    public Timestamp dateCreated;

    @ServerTimestamp
    @PropertyName(DATE_UPDATED)
    @SerializedName(DATE_UPDATED)
    @Expose
    public Timestamp dateUpdated;

    public BookHomestayDetails(){

    }

    public BookHomestayDetails(String firstName, String lastName, String email, String country, String countryCode, String contactNo, int totalGuest, String purpose, String type, Search search) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.country = country;
        this.contactNo = contactNo;
        this.countryCode = countryCode;
        this.totalGuest = totalGuest;
        this.purpose = purpose;
        this.type = type;
        this.checkinDate = search.checkin_date;
        this.checkoutDate = search.checkout_date;
    }

    public static BookHomestayDetails snapshotToBookHomeStayDetail(AppCompatActivity parentActivity, DocumentSnapshot snapshot) {
        BookHomestayDetails tempBookHomeStayDetail = snapshot.toObject(BookHomestayDetails.class);
        if(tempBookHomeStayDetail != null){
            tempBookHomeStayDetail.id = snapshot.getId();
            return tempBookHomeStayDetail;
        }else{
            return null;
        }
    }
}