package com.vidatechft.yupa.classes;

import android.support.v7.app.AppCompatActivity;

import com.google.firebase.Timestamp;
import com.google.firebase.database.PropertyName;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.ServerTimestamp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BlogDiscussion {
    public static final String URL_BLOG_DISCUSSION = "blogDiscussion";

    public static final String ID = "id";
    public static final String DATE_CREATED = "dateCreated";
    public static final String DATE_UPDATED = "dateUpdated";
    public static final String USER_ID = "userId";
    public static final String BLOG_ID = "blogId";
    public static final String DISCUSSION_ID = "discussionId";
    public static final String DESCRIPTION_DISCUSSION= "descDiscussion";
    public static final String IS_REVIEWED= "isReviewed";
    public static final String TYPE= "type";
    public static final String TYPE_DISCUSSION = "typeDiscussion";
    public static final String TYPE_REPLY = "typeReply";
    public static final String LIKE_COUNT = "likeCount";

    @Exclude
    public String id;

    @Exclude
    public Favourite favourite;
    @Exclude
    public User user;

    @PropertyName(USER_ID)
    @SerializedName(USER_ID)
    @Expose
    public String userId;

    @PropertyName(BLOG_ID)
    @SerializedName(BLOG_ID)
    @Expose
    public String blogId;

    @PropertyName(DISCUSSION_ID)
    @SerializedName(DISCUSSION_ID)
    @Expose
    public String discussionId;

    @PropertyName(LIKE_COUNT)
    @SerializedName(LIKE_COUNT)
    @Expose
    public Integer likeCount;

    @PropertyName(IS_REVIEWED)
    @SerializedName(IS_REVIEWED)
    @Expose
    public Boolean isReviewed;

    @PropertyName(DESCRIPTION_DISCUSSION)
    @SerializedName(DESCRIPTION_DISCUSSION)
    @Expose
    public String descDiscussion;

    @ServerTimestamp
    @PropertyName(DATE_CREATED)
    @SerializedName(DATE_CREATED)
    @Expose
    public Timestamp dateCreated;

    @ServerTimestamp
    @PropertyName(DATE_UPDATED)
    @SerializedName(DATE_UPDATED)
    @Expose
    public Timestamp dateUpdated;

    public BlogDiscussion() { }

    public BlogDiscussion(AppCompatActivity parentActivity, DocumentSnapshot snapshot ){
        this.id = snapshot.getId();
        this.userId = snapshot.getString(BlogDiscussion.USER_ID);
        this.blogId = snapshot.getString(BlogDiscussion.BLOG_ID);
        this.discussionId = snapshot.getString(BlogDiscussion.DISCUSSION_ID);
        this.dateCreated = snapshot.getTimestamp(BlogDiscussion.DATE_CREATED);
        this.dateUpdated = snapshot.getTimestamp(BlogDiscussion.DATE_UPDATED);
        this.descDiscussion = snapshot.getString(BlogDiscussion.URL_BLOG_DISCUSSION);
        this.isReviewed = snapshot.getBoolean(BlogDiscussion.IS_REVIEWED);
    }

}
