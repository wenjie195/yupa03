package com.vidatechft.yupa.classes.SearchPlaceGoogleApi;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Candidates {
    @SerializedName("place_id")
    public String placeId;

    public Candidates(){

    }

    public Candidates(String placeId) {
        this.placeId = placeId;
    }
}
