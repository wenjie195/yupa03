package com.vidatechft.yupa.classes;

import com.google.firebase.Timestamp;
import com.google.firebase.database.PropertyName;
import com.google.firebase.database.ServerValue;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.ServerTimestamp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;

public class Chat {
    public static final String URL_CHAT = "chat";

    public static final String TYPE_JOB_DIRECT = "jobDir";
    public static final String TYPE_GROUP = "personalGroup";
    public static final String TYPE_PERSONAL_MSG = "pm";

    public static final String LAST_TYPE = "lastType";
    public static final String MESSAGE_TYPE_TEXT = "text";
    public static final String MESSAGE_TYPE_IMG = "image";
    public static final String MESSAGE_TYPE_AUDIO = "audio";
    public static final String MESSAGE_TYPE_ICONS = "icons";

    public static final String ID = "id";
    public static final String USER_LIST = "userList";
    public static final String TYPE = "type";
    public static final String STAFF_ID = "staffId";
    public static final String EMPLOYER_ID = "employerId";
    public static final String LAST_MESSAGE = "lastMsg";
    public static final String LAST_UID = "lastUid";
    public static final String DATE_CREATED = "dateCreated";
    public static final String DATE_UPDATED = "dateUpdated";


    @Exclude
    public User user = new User();

    @Exclude
    public String otherUserId;

    @Exclude
    public String id;

    @PropertyName(USER_LIST)
    @SerializedName(USER_LIST)
    @Expose
    public ArrayList<String> userList = new ArrayList<>();

    @PropertyName(TYPE)
    @SerializedName(TYPE)
    @Expose
    public String type;

    @PropertyName(STAFF_ID)
    @SerializedName(STAFF_ID)
    @Expose
    public String staffId;

    @PropertyName(EMPLOYER_ID)
    @SerializedName(EMPLOYER_ID)
    @Expose
    public String employerId;

    @PropertyName(LAST_MESSAGE)
    @SerializedName(LAST_MESSAGE)
    @Expose
    public String lastMsg;

    @PropertyName(LAST_TYPE)
    @SerializedName(LAST_TYPE)
    @Expose
    public String lastType;

    @PropertyName(LAST_UID)
    @SerializedName(LAST_UID)
    @Expose
    public String lastUid;

    @ServerTimestamp
    @PropertyName(DATE_CREATED)
    @SerializedName(DATE_CREATED)
    @Expose
    public Timestamp dateCreated; // from here https://stackoverflow.com/questions/46254266/firebase-no-properties-to-serialize-found-on-class

    @ServerTimestamp
    @PropertyName(DATE_UPDATED)
    @SerializedName(DATE_UPDATED)
    @Expose
    public Timestamp dateUpdated;

    public Chat(){

    }

    public Chat(String type, String staffId, String employerId, String lastMsg) {
        this.type = type;
        this.staffId = staffId;
        this.employerId = employerId;
        this.lastMsg = lastMsg;
    }

    public static HashMap<String,Object> getNewPrivateChatMap(String thisUid, String otherUid){
        HashMap<String,Object> tempChatMap = new HashMap<>();
        ArrayList<String> userList = new ArrayList<>();
        userList.add(thisUid);
        userList.add(otherUid);

        tempChatMap.put(USER_LIST,userList);
        tempChatMap.put(thisUid,true);
        tempChatMap.put(otherUid,true);
        tempChatMap.put(TYPE,TYPE_PERSONAL_MSG);
        tempChatMap.put(DATE_CREATED, FieldValue.serverTimestamp());
        tempChatMap.put(DATE_UPDATED, FieldValue.serverTimestamp());

        return tempChatMap;
    }
}
