package com.vidatechft.yupa.classes;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.SparseArray;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.firebase.Timestamp;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.PropertyName;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.ServerTimestamp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.PlaceDetailsGoogleApi.PlaceAddress;
import com.vidatechft.yupa.itinerary.ItineraryPlansContainerFragment;
import com.vidatechft.yupa.itinerary.StartPlanItineraryFragment;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@IgnoreExtraProperties
public class Itinerary implements Comparable<Itinerary>{

    public static final String URL_ITINERARY = "itinerary";
    public static final String ITINERARY_PIC_URL = "itineraryPicUrl";
    public static final String GALLERY = "gallery";

    //if canceled, jsut set status to null
    public static final String STATUS_PENDING = "pending";
    public static final String STATUS_DONE = "done";
    public static final String STATUS_REJECTED = "rejected";

    public static final String ID = "id";
    public static final String USER_ID = "userId";
    public static final String TRIP_NAME = "tripName";
    public static final String LOCATION_NAME = "locationName";
    public static final String ADDRESS = "address";
    public static final String PLACE_ADDRESS_LIST = "placeAddressList";
    public static final String PLACE_INDEX = "placeIndex";
    public static final String PLACE_ID = "placeId";
    public static final String GPS = "gps";
    public static final String DEPART_DATE = "departDate";
    public static final String RETURN_DATE = "returnDate";
    public static final String NO_OF_DAYS = "noofDays";
    public static final String DURATION_HOUR = "durationHour";
    public static final String IS_PUBLIC = "isPublic";
    public static final String IS_PUBLISH = "isPublish";
    public static final String CURRENCY_CODE = "currencyCode";
    public static final String MIN_BUDGET = "minBudget";
    public static final String MAX_BUDGET = "maxBudget";
    public static final String INTRO_MSG = "introMsg";
    public static final String SPECIAL_REQUEST = "specialRequest";
    public static final String IS_FAVOURITE = "isFavourite";
    public static final String LIKE_COUNT = Favourite.LIKE_COUNT;
    public static final String IS_READ_BY_USER = "isReadByUser";//will set to false after admin finished setting quotation for this itinerary, and will be true if user reacts to this. Other than that it should be null
    public static final String QUOTATION_STATUS = "quotationStatus";//also when user request for quotation. will be set to pending, done or rejected based on the above STATUS_<SOME_STATUS>
    public static final String PRICE = "price";//will be null if user creates the itinerary. will have price if admin created the itinerary or when user creates an itinerary, request for quotations and admin responds by submitting the quotation
    public static final String IS_FROM_ADMIN = "isFromAdmin"; // this is for when user request for quotation in itinerary or buy admin's itinerary then will set to true. If admin is the 1 creating this itinerary or user is creating this itinerary but did not request for quotations, it will be null (nothing is set),it will be set to true also if he buys admin itinerary
    public static final String IS_PAID = "isPaid";
    public static final String PAYMENT_ID = "paymentId";//payment result id of either failed or success
    public static final String ORIGINAL_ITI_ID = "oriItiId";//when buy from admin's itinerary, this variable will indicate which admin's itinerary that he bought. Request for quotations doesn't affect this though (means it should be null)
    public static final String QUOTATION_REMARK = "quotationRemark";
    public static final String DATE_QUOTATION_REQUESTED = "dateQuotationRequested";
    public static final String DATE_QUOTATION_ACTION = "dateQuotationAction";
    public static final String DATE_CREATED = "dateCreated";
    public static final String DATE_UPDATED = "dateUpdated";

    public static final int PROGRESS_MULTIPLIER = 250;
    public static final float MAP_ZOOM_IN_LEVEL = 16.25f;

    @Exclude
    public SparseArray<ArrayList<Plan>> planList = new SparseArray<>();

    @Exclude
    public Boolean isFavourite;

    @Exclude
    public Favourite favourite;

    @Exclude
    public ArrayList<String> cuisineList = new ArrayList<>();

    @Exclude
    public ArrayList<String> activityList = new ArrayList<>();

    @PropertyName(ID)
    @SerializedName(ID)
    @Expose
    public String id;

    @PropertyName(USER_ID)
    @SerializedName(USER_ID)
    @Expose
    public String userId;

    @PropertyName(TRIP_NAME)
    @SerializedName(TRIP_NAME)
    @Expose
    public String tripName;

    @PropertyName(LOCATION_NAME)
    @SerializedName(LOCATION_NAME)
    @Expose
    public String locationName;

    @PropertyName(ADDRESS)
    @SerializedName(ADDRESS)
    @Expose
    public String address;

    @PropertyName(PLACE_ADDRESS_LIST)
    @SerializedName(PLACE_ADDRESS_LIST)
    @Expose
    public ArrayList<HashMap<String,Object>> placeAddressList;

    @PropertyName(PLACE_INDEX)
    @SerializedName(PLACE_INDEX)
    @Expose
    public HashMap<String,Boolean> placeIndex;

    @PropertyName(PLACE_ID)
    @SerializedName(PLACE_ID)
    @Expose
    public String placeId;

    @PropertyName(GPS)
    @SerializedName(GPS)
    @Expose
    public ArrayList<GeoPoint> gps;

    @PropertyName(DEPART_DATE)
    @SerializedName(DEPART_DATE)
    @Expose
    public Long departDate;

    @PropertyName(RETURN_DATE)
    @SerializedName(RETURN_DATE)
    @Expose
    public Long returnDate;

    @PropertyName(NO_OF_DAYS)
    @SerializedName(NO_OF_DAYS)
    @Expose
    public Long noofDays;

    @PropertyName(DURATION_HOUR)
    @SerializedName(DURATION_HOUR)
    @Expose
    public Long durationHour;

    @PropertyName(IS_PUBLIC)
    @SerializedName(IS_PUBLIC)
    @Expose
    public Boolean isPublic;

    @PropertyName(IS_PUBLISH)
    @SerializedName(IS_PUBLISH)
    @Expose
    public Boolean isPublish;

    @PropertyName(CURRENCY_CODE)
    @SerializedName(CURRENCY_CODE)
    @Expose
    public String currencyCode;

    @PropertyName(MIN_BUDGET)
    @SerializedName(MIN_BUDGET)
    @Expose
    public Double minBudget;

    @PropertyName(MAX_BUDGET)
    @SerializedName(MAX_BUDGET)
    @Expose
    public Double maxBudget;

    @PropertyName(INTRO_MSG)
    @SerializedName(INTRO_MSG)
    @Expose
    public String introMsg;

    @PropertyName(SPECIAL_REQUEST)
    @SerializedName(SPECIAL_REQUEST)
    @Expose
    public String specialRequest;

    @PropertyName(ITINERARY_PIC_URL)
    @SerializedName(ITINERARY_PIC_URL)
    @Expose
    public String itineraryPicUrl;

    @PropertyName(GALLERY)
    @SerializedName(GALLERY)
    @Expose
    public ArrayList<Gallery> gallery;

    @PropertyName(LIKE_COUNT)
    @SerializedName(LIKE_COUNT)
    @Expose
    public Long likeCount;

    @PropertyName(IS_READ_BY_USER)
    @SerializedName(IS_READ_BY_USER)
    @Expose
    public Boolean isReadByUser;

    @PropertyName(QUOTATION_STATUS)
    @SerializedName(QUOTATION_STATUS)
    @Expose
    public String quotationStatus;

    @PropertyName(PRICE)
    @SerializedName(PRICE)
    @Expose
    public Double price;

    @PropertyName(IS_FROM_ADMIN)
    @SerializedName(IS_FROM_ADMIN)
    @Expose
    public Boolean isFromAdmin;

    @PropertyName(IS_PAID)
    @SerializedName(IS_PAID)
    @Expose
    public Boolean isPaid;

    @PropertyName(PAYMENT_ID)
    @SerializedName(PAYMENT_ID)
    @Expose
    public String paymentId;

    @PropertyName(ORIGINAL_ITI_ID)
    @SerializedName(ORIGINAL_ITI_ID)
    @Expose
    public String oriItiId;

    @PropertyName(QUOTATION_REMARK)
    @SerializedName(QUOTATION_REMARK)
    @Expose
    public String quotationRemark;

    @ServerTimestamp
    @PropertyName(DATE_QUOTATION_REQUESTED)
    @SerializedName(DATE_QUOTATION_REQUESTED)
    @Expose
    public Timestamp dateQuotationRequested;

    @ServerTimestamp
    @PropertyName(DATE_QUOTATION_ACTION)
    @SerializedName(DATE_QUOTATION_ACTION)
    @Expose
    public Timestamp dateQuotationAction;

    @ServerTimestamp
    @PropertyName(DATE_CREATED)
    @SerializedName(DATE_CREATED)
    @Expose
    public Timestamp dateCreated;

    @ServerTimestamp
    @PropertyName(DATE_UPDATED)
    @SerializedName(DATE_UPDATED)
    @Expose
    public Timestamp dateUpdated;

    public Itinerary(){

    }

    public Itinerary(AppCompatActivity parentActivity, QueryDocumentSnapshot snapshot){
        this.cuisineList = GeneralFunction.copyListIfMatchValueQuerySnapshot(parentActivity,snapshot,R.array.cuisine_list);
        this.activityList = GeneralFunction.copyListIfMatchValueQuerySnapshot(parentActivity,snapshot,R.array.editProfileHobby);

        this.userId = snapshot.getString(Itinerary.USER_ID);
        this.tripName = snapshot.getString(Itinerary.TRIP_NAME);
        this.locationName = snapshot.getString(Itinerary.LOCATION_NAME);
        this.address = snapshot.getString(Itinerary.ADDRESS);
        try{
//            this.placeAddress = PlaceAddress.getMapToClass((HashMap<String, Object>) snapshot.get(Itinerary.PLACE_ADDRESS));
            this.placeAddressList = (ArrayList<HashMap<String,Object>>) snapshot.get(Itinerary.PLACE_ADDRESS_LIST);
        }catch (Exception e){
            e.printStackTrace();
        }
        try{
            this.placeIndex = (HashMap<String,Boolean>) snapshot.get(Itinerary.PLACE_INDEX);
        }catch (Exception e){
            e.printStackTrace();
        }
        this.placeId = snapshot.getString(Itinerary.PLACE_ID);
        try{
            this.gps = (ArrayList<GeoPoint>) snapshot.get(Itinerary.GPS);
        }catch (Exception e){
            e.printStackTrace();
        }
        this.departDate = snapshot.getLong(Itinerary.DEPART_DATE);
        this.returnDate = snapshot.getLong(Itinerary.RETURN_DATE);
        this.noofDays = snapshot.getLong(Itinerary.NO_OF_DAYS);
        this.durationHour = snapshot.getLong(Itinerary.DURATION_HOUR);
        this.isPublic = snapshot.getBoolean(Itinerary.IS_PUBLIC);
        this.isPublish = snapshot.getBoolean(Itinerary.IS_PUBLISH);
        this.currencyCode = snapshot.getString(Itinerary.CURRENCY_CODE);
        this.minBudget = snapshot.getDouble(Itinerary.MIN_BUDGET);
        this.maxBudget = snapshot.getDouble(Itinerary.MAX_BUDGET);
        this.introMsg = snapshot.getString(Itinerary.INTRO_MSG);
        this.specialRequest = snapshot.getString(Itinerary.SPECIAL_REQUEST);
        this.itineraryPicUrl = snapshot.getString(Itinerary.ITINERARY_PIC_URL);
        try{
            //convert map to class from here https://stackoverflow.com/questions/16428817/convert-a-mapstring-string-to-a-pojo
            this.gallery = new ObjectMapper().convertValue(snapshot.get(Itinerary.GALLERY), new TypeReference<ArrayList<Gallery>>(){});
        }catch (Exception e){
            e.printStackTrace();
        }
        this.likeCount = snapshot.getLong(Itinerary.LIKE_COUNT);
        this.isReadByUser = snapshot.getBoolean(Itinerary.IS_READ_BY_USER);
        this.quotationStatus = snapshot.getString(Itinerary.QUOTATION_STATUS);
        this.price = snapshot.getDouble(Itinerary.PRICE);
        this.isFromAdmin = snapshot.getBoolean(Itinerary.IS_FROM_ADMIN);
        this.isPaid = snapshot.getBoolean(Itinerary.IS_PAID);
        this.paymentId = snapshot.getString(Itinerary.PAYMENT_ID);
        this.oriItiId = snapshot.getString(Itinerary.ORIGINAL_ITI_ID);
        this.quotationRemark = snapshot.getString(Itinerary.QUOTATION_REMARK);
        this.dateQuotationRequested = snapshot.getTimestamp(Itinerary.DATE_QUOTATION_REQUESTED);
        this.dateQuotationAction = snapshot.getTimestamp(Itinerary.DATE_QUOTATION_ACTION);
        this.dateCreated = snapshot.getTimestamp(Itinerary.DATE_CREATED);
        this.dateUpdated = snapshot.getTimestamp(Itinerary.DATE_UPDATED);
        this.id = snapshot.getId();
    }

    public Itinerary(AppCompatActivity parentActivity, DocumentSnapshot snapshot){
        this.cuisineList = GeneralFunction.copyListIfMatchValueQuerySnapshot(parentActivity,snapshot,R.array.cuisine_list);
        this.activityList = GeneralFunction.copyListIfMatchValueQuerySnapshot(parentActivity,snapshot,R.array.editProfileHobby);

        this.userId = snapshot.getString(Itinerary.USER_ID);
        this.tripName = snapshot.getString(Itinerary.TRIP_NAME);
        this.locationName = snapshot.getString(Itinerary.LOCATION_NAME);
        this.address = snapshot.getString(Itinerary.ADDRESS);
        try{
//            this.placeAddress = PlaceAddress.getMapToClass((HashMap<String, Object>) snapshot.get(Itinerary.PLACE_ADDRESS));
            this.placeAddressList = (ArrayList<HashMap<String,Object>>) snapshot.get(Itinerary.PLACE_ADDRESS_LIST);
        }catch (Exception e){
            e.printStackTrace();
        }
        try{
            this.placeIndex = (HashMap<String,Boolean>) snapshot.get(Itinerary.PLACE_INDEX);
        }catch (Exception e){
            e.printStackTrace();
        }
        this.placeId = snapshot.getString(Itinerary.PLACE_ID);
        try{
            this.gps = (ArrayList<GeoPoint>) snapshot.get(Itinerary.GPS);
        }catch (Exception e){
            e.printStackTrace();
        }
        this.departDate = snapshot.getLong(Itinerary.DEPART_DATE);
        this.returnDate = snapshot.getLong(Itinerary.RETURN_DATE);
        this.noofDays = snapshot.getLong(Itinerary.NO_OF_DAYS);
        this.durationHour = snapshot.getLong(Itinerary.DURATION_HOUR);
        this.isPublic = snapshot.getBoolean(Itinerary.IS_PUBLIC);
        this.isPublish = snapshot.getBoolean(Itinerary.IS_PUBLISH);
        this.currencyCode = snapshot.getString(Itinerary.CURRENCY_CODE);
        this.minBudget = snapshot.getDouble(Itinerary.MIN_BUDGET);
        this.maxBudget = snapshot.getDouble(Itinerary.MAX_BUDGET);
        this.introMsg = snapshot.getString(Itinerary.INTRO_MSG);
        this.specialRequest = snapshot.getString(Itinerary.SPECIAL_REQUEST);
        this.itineraryPicUrl = snapshot.getString(Itinerary.ITINERARY_PIC_URL);
        try{
            this.gallery = new ObjectMapper().convertValue(snapshot.get(Itinerary.GALLERY), new TypeReference<ArrayList<Gallery>>(){});
        }catch (Exception e){
            e.printStackTrace();
        }
        this.likeCount = snapshot.getLong(Itinerary.LIKE_COUNT);
        this.isReadByUser = snapshot.getBoolean(Itinerary.IS_READ_BY_USER);
        this.quotationStatus = snapshot.getString(Itinerary.QUOTATION_STATUS);
        this.price = snapshot.getDouble(Itinerary.PRICE);
        this.isFromAdmin = snapshot.getBoolean(Itinerary.IS_FROM_ADMIN);
        this.isPaid = snapshot.getBoolean(Itinerary.IS_PAID);
        this.paymentId = snapshot.getString(Itinerary.PAYMENT_ID);
        this.oriItiId = snapshot.getString(Itinerary.ORIGINAL_ITI_ID);
        this.quotationRemark = snapshot.getString(Itinerary.QUOTATION_REMARK);
        this.dateQuotationRequested = snapshot.getTimestamp(Itinerary.DATE_QUOTATION_REQUESTED);
        this.dateQuotationAction = snapshot.getTimestamp(Itinerary.DATE_QUOTATION_ACTION);
        this.dateCreated = snapshot.getTimestamp(Itinerary.DATE_CREATED);
        this.dateUpdated = snapshot.getTimestamp(Itinerary.DATE_UPDATED);
        this.id = snapshot.getId();
    }

    public Itinerary(Itinerary oriIti){
        this.planList = oriIti.planList;
        this.cuisineList = oriIti.cuisineList;
        this.activityList = oriIti.cuisineList;
        this.id = oriIti.id;
        this.userId = oriIti.userId;
        this.tripName = oriIti.tripName;
        this.locationName = oriIti.locationName;
        this.address = oriIti.address;
        this.placeAddressList = oriIti.placeAddressList;
        this.placeIndex = oriIti.placeIndex;
        this.placeId = oriIti.placeId;
        this.gps = oriIti.gps;
        this.departDate = oriIti.departDate;
        this.returnDate = oriIti.returnDate;
        this.noofDays = oriIti.noofDays;
        this.durationHour = oriIti.durationHour;
        this.isPublic = oriIti.isPublic;
        this.isPublish = oriIti.isPublish;
        this.currencyCode = oriIti.currencyCode;
        this.minBudget = oriIti.minBudget;
        this.maxBudget = oriIti.maxBudget;
        this.introMsg = oriIti.introMsg;
        this.specialRequest = oriIti.specialRequest;
        this.itineraryPicUrl = oriIti.itineraryPicUrl;
        this.gallery = oriIti.gallery;
        this.isFavourite = oriIti.isFavourite;
        this.favourite = oriIti.favourite;
        this.likeCount = oriIti.likeCount;
        this.isReadByUser = oriIti.isReadByUser;
        this.quotationStatus = oriIti.quotationStatus;
        this.price = oriIti.price;
        this.isFromAdmin = oriIti.isFromAdmin;
        this.isPaid = oriIti.isPaid;
        this.paymentId = oriIti.paymentId;
        this.oriItiId = oriIti.oriItiId;
        this.quotationRemark = oriIti.quotationRemark;
        this.dateQuotationRequested = oriIti.dateQuotationRequested;
        this.dateQuotationAction = oriIti.dateQuotationAction;
        this.dateCreated = oriIti.dateCreated;
        this.dateUpdated = oriIti.dateUpdated;
    }

    public static HashMap<String,Object> toMap(Itinerary itinerary, boolean includeArrays){
        HashMap<String,Object> itiMap = new HashMap<>();

        if(includeArrays){
            for(String cuisine : itinerary.cuisineList){
                itiMap.put(cuisine,true);
            }

            for(String activiy : itinerary.activityList){
                itiMap.put(activiy,true);
            }
        }

        itiMap.put(USER_ID,itinerary.userId);
        itiMap.put(TRIP_NAME,itinerary.tripName);
        itiMap.put(LOCATION_NAME,itinerary.locationName);
        itiMap.put(ADDRESS,itinerary.address);
//        if(itinerary.placeAddress != null && itinerary.placeAddress.placeId != null){
//            itiMap.put(Job.PLACE_ADDRESS, PlaceAddress.getHashmap(itinerary.placeAddress,itinerary.placeAddress.placeId));
//        }
        itiMap.put(PLACE_ADDRESS_LIST,itinerary.placeAddressList);
        itiMap.put(PLACE_INDEX,itinerary.placeIndex);
        itiMap.put(PLACE_ID,itinerary.placeId);
        itiMap.put(GPS,itinerary.gps);
        itiMap.put(DEPART_DATE,itinerary.departDate);
        itiMap.put(RETURN_DATE,itinerary.returnDate);
        itiMap.put(NO_OF_DAYS,itinerary.noofDays);
        itiMap.put(DURATION_HOUR,itinerary.durationHour);
        itiMap.put(IS_PUBLIC,itinerary.isPublic);
        itiMap.put(IS_PUBLISH,itinerary.isPublish);
        itiMap.put(CURRENCY_CODE,itinerary.currencyCode);
        itiMap.put(MIN_BUDGET,itinerary.minBudget);
        itiMap.put(MAX_BUDGET,itinerary.maxBudget);
        itiMap.put(INTRO_MSG,itinerary.introMsg);
        itiMap.put(SPECIAL_REQUEST,itinerary.specialRequest);
        itiMap.put(ITINERARY_PIC_URL,itinerary.itineraryPicUrl);
        itiMap.put(GALLERY,itinerary.gallery);
        itiMap.put(IS_FAVOURITE,itinerary.isFavourite);
        itiMap.put(LIKE_COUNT,itinerary.likeCount);
        itiMap.put(IS_READ_BY_USER,itinerary.isReadByUser);
        itiMap.put(QUOTATION_STATUS,itinerary.quotationStatus);
        itiMap.put(PRICE,itinerary.price);
        itiMap.put(IS_FROM_ADMIN,itinerary.isFromAdmin);
        itiMap.put(IS_PAID,itinerary.isPaid);
        itiMap.put(PAYMENT_ID,itinerary.paymentId);
        itiMap.put(ORIGINAL_ITI_ID,itinerary.oriItiId);
        itiMap.put(QUOTATION_REMARK,itinerary.quotationRemark);
        itiMap.put(DATE_QUOTATION_REQUESTED,itinerary.dateQuotationRequested);
        itiMap.put(DATE_QUOTATION_ACTION,itinerary.dateQuotationAction);
        itiMap.put(DATE_CREATED,itinerary.dateCreated);
        itiMap.put(DATE_UPDATED,itinerary.dateUpdated);

        return itiMap;
    }

    public static Itinerary toClass(HashMap<String,Object> itineraryMap, AppCompatActivity parentActivity, SparseArray<ArrayList<Plan>> planList){
        Itinerary itinerary = new Itinerary();

        itinerary.planList = planList;

        List<String> cuisineFullList = new ArrayList<>(Arrays.asList(parentActivity.getResources().getStringArray(R.array.cuisine_list)));

        for(String cuisine : cuisineFullList){
            if(itineraryMap.containsKey(cuisine)){
                itinerary.cuisineList.add(cuisine);
            }
        }

        List<String> activityFullList = new ArrayList<>(Arrays.asList(parentActivity.getResources().getStringArray(R.array.editProfileHobby)));

        for(String activity : activityFullList){
            if(itineraryMap.containsKey(activity)){
                itinerary.activityList.add(activity);
            }
        }

        itinerary.id = (String) itineraryMap.get(ID);
        itinerary.userId = (String) itineraryMap.get(USER_ID);
        itinerary.tripName = (String) itineraryMap.get(TRIP_NAME);
        itinerary.locationName = (String) itineraryMap.get(LOCATION_NAME);
        itinerary.address = (String) itineraryMap.get(ADDRESS);
//        try{
//            itinerary.placeAddress = PlaceAddress.getMapToClass((HashMap<String, Object>) itineraryMap.get(Itinerary.PLACE_ADDRESS));
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        try{
//            itinerary.placeIndex = (ArrayList<String>) itineraryMap.get(PLACE_INDEX);
//        }catch (Exception e){
//            e.printStackTrace();
//        }
        try{
//            this.placeAddress = PlaceAddress.getMapToClass((HashMap<String, Object>) snapshot.get(Itinerary.PLACE_ADDRESS));
            itinerary.placeAddressList = (ArrayList<HashMap<String,Object>>) itineraryMap.get(Itinerary.PLACE_ADDRESS_LIST);
        }catch (Exception e){
            e.printStackTrace();
        }
        try{
            itinerary.placeIndex = (HashMap<String,Boolean>) itineraryMap.get(Itinerary.PLACE_INDEX);
        }catch (Exception e){
            e.printStackTrace();
        }
        itinerary.placeId = (String) itineraryMap.get(PLACE_ID);
        try{
            itinerary.gps = (ArrayList<GeoPoint>) itineraryMap.get(Itinerary.GPS);
        }catch (Exception e){
            e.printStackTrace();
        }
        itinerary.departDate = (Long) itineraryMap.get(DEPART_DATE);
        itinerary.returnDate = (Long) itineraryMap.get(RETURN_DATE);
        itinerary.noofDays = (Long) itineraryMap.get(NO_OF_DAYS);
        itinerary.durationHour = (Long) itineraryMap.get(DURATION_HOUR);
        itinerary.isPublic = (Boolean) itineraryMap.get(IS_PUBLIC);
        itinerary.isPublish = (Boolean) itineraryMap.get(IS_PUBLISH);
        itinerary.currencyCode = (String) itineraryMap.get(CURRENCY_CODE);
        itinerary.minBudget = (Double) itineraryMap.get(MIN_BUDGET);
        itinerary.maxBudget = (Double) itineraryMap.get(MAX_BUDGET);
        itinerary.introMsg = (String) itineraryMap.get(INTRO_MSG);
        itinerary.specialRequest = (String) itineraryMap.get(SPECIAL_REQUEST);
        itinerary.itineraryPicUrl = (String) itineraryMap.get(ITINERARY_PIC_URL);
        try{
            itinerary.gallery = new ObjectMapper().convertValue(itineraryMap.get(GALLERY), new TypeReference<ArrayList<Gallery>>(){});
        }catch (Exception e){
            e.printStackTrace();
        }
        itinerary.isFavourite = (Boolean) itineraryMap.get(IS_FAVOURITE);
        itinerary.likeCount = (Long) itineraryMap.get(LIKE_COUNT);
        itinerary.isReadByUser = (Boolean) itineraryMap.get(IS_READ_BY_USER);
        itinerary.quotationStatus = (String) itineraryMap.get(QUOTATION_STATUS);
        itinerary.price = (Double) itineraryMap.get(PRICE);
        itinerary.isFromAdmin = (Boolean) itineraryMap.get(IS_FROM_ADMIN);
        itinerary.isPaid = (Boolean) itineraryMap.get(IS_PAID);
        itinerary.paymentId = (String) itineraryMap.get(PAYMENT_ID);
        itinerary.oriItiId = (String) itineraryMap.get(ORIGINAL_ITI_ID);
        itinerary.quotationRemark = (String) itineraryMap.get(QUOTATION_REMARK);
        try{
            itinerary.dateQuotationRequested = (Timestamp) itineraryMap.get(DATE_QUOTATION_REQUESTED);
        }catch (Exception e){

        }

        try{
            itinerary.dateQuotationAction = (Timestamp) itineraryMap.get(DATE_QUOTATION_ACTION);
        }catch (Exception e){

        }

        try{
            itinerary.dateCreated = (Timestamp) itineraryMap.get(DATE_CREATED);
        }catch (Exception e){

        }

        try{
            itinerary.dateUpdated = (Timestamp) itineraryMap.get(DATE_UPDATED);
        }catch (Exception e){

        }

        return itinerary;
    }

    public static void goToManageItineraryOrPlan(MainActivity parentActivity, Itinerary itinerary){
//        TO CHECK whether need straight go to plan or go to add new plan fragment
        //cant get plan list becuz didnt get from db yet
        if(itinerary != null && itinerary.minBudget != null && itinerary.maxBudget != null){
            Fragment itineraryPlansContainerFragment = ItineraryPlansContainerFragment.newInstance(parentActivity,itinerary);
            GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, itineraryPlansContainerFragment).addToBackStack(ItineraryPlansContainerFragment.TAG).commit();
        }else{
            Fragment startPlanItineraryFragment = StartPlanItineraryFragment.newInstance(parentActivity,itinerary);
            GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, startPlanItineraryFragment).addToBackStack(StartPlanItineraryFragment.TAG).commit();
        }
    }

    @Override
    public int compareTo(@NonNull Itinerary itinerary) {
        if(isFromAdmin != null && isFromAdmin){
            if(itinerary.isFromAdmin != null && itinerary.isFromAdmin){
                if(itinerary.dateUpdated == null || dateUpdated == null){
                    return -1;
                }else{
                    return itinerary.dateUpdated.compareTo(dateUpdated);//if both also is from admin, then sort based on date updated
                }
            }else{
                return -1;//if the next one is from admin, change the next one to current 1. the next 1 is on top of the current 1
            }
        }

        if (likeCount == null || itinerary.likeCount == null || dateUpdated == null || itinerary.dateUpdated == null)
            return 0;

        int result = itinerary.likeCount.compareTo(likeCount);

        if (result == 0) {
            result = itinerary.dateUpdated.compareTo(dateUpdated);
        }

        return result;
//        return itinerary.dateUpdated.compareTo(dateUpdated);//descending order, newest is at top
    }

}