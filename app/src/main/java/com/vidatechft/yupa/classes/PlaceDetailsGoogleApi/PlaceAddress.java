package com.vidatechft.yupa.classes.PlaceDetailsGoogleApi;

import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.Query;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vidatechft.yupa.classes.TourBuddy;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;

//the address component details from here https://developers.google.com/maps/documentation/geocoding/intro
public class PlaceAddress {
    public static final String TAG = PlaceAddress.class.getName();

    public static final String PLACE_ADDRESS = "placeAddress";
    public static final String PLACE_INDEX = "placeIndex";

    public static final String PLACE_ID = "placeId";
    public static final String LATITUDE = "lat";
    public static final String LONGITUDE = "lng";
    public static final String COUNTRY = "country";
    public static final String ADMINISTRATIVE_AREA_LEVEL_1 = "administrative_area_level_1";         //state
    public static final String ADMINISTRATIVE_AREA_LEVEL_2 = "administrative_area_level_2";         //ONE level below state
    public static final String ADMINISTRATIVE_AREA_LEVEL_3 = "administrative_area_level_3";
    public static final String ADMINISTRATIVE_AREA_LEVEL_4 = "administrative_area_level_4";
    public static final String ADMINISTRATIVE_AREA_LEVEL_5 = "administrative_area_level_5";
    public static final String LOCALITY = "locality";                                               //city - Ayer Itam
    public static final String SUBLOCALITY = "sublocality";                                         // like inner city - Bandar Baru Air Itam
    public static final String SUBLOCALITY_LEVEL_1 = "sublocality_level_1";
    public static final String SUBLOCALITY_LEVEL_2 = "sublocality_level_2";                         //ONE level below previous inner city
    public static final String SUBLOCALITY_LEVEL_3 = "sublocality_level_3";
    public static final String SUBLOCALITY_LEVEL_4 = "sublocality_level_4";
    public static final String SUBLOCALITY_LEVEL_5 = "sublocality_level_5";
    public static final String STREET_NUMBER = "street_number";
    public static final String ROUTE = "route";                                                     //street name - Lebuhraya Thean Teik
    public static final String STREET_ADDRESS = "street_address";
    public static final String PREMISE = "premise";                                                 //premise - All seasons place
    public static final String SUBPREMISE = "subpremise";
    public static final String POSTAL_CODE = "postal_code";
    public static final String PLACE_NAME = "name";                                                 //place name - subway

    public String placeId;
    public Double lat;
    public Double lng;
    public String country;
    public String administrative_area_level_1;
    public String administrative_area_level_2;
    public String administrative_area_level_3;
    public String administrative_area_level_4;
    public String administrative_area_level_5;
    public String locality;
    public String sublocality;
    public String sublocality_level_1;
    public String sublocality_level_2;
    public String sublocality_level_3;
    public String sublocality_level_4;
    public String sublocality_level_5;
    public String street_number;
    public String route;
    public String street_address;
    public String premise;
    public String subpremise;
    public String postal_code;
    public String placeName;        //name

    //can use this >>> .whereEqualTo("votes." + getUid(), true) <<< to query from inner hashmap
    public PlaceAddress(){

    }

    public static HashMap<String, Object> getHashmap(PlaceAddress placeAddress, String placeId) {
        Gson gson = new GsonBuilder().create();
        String json = gson.toJson(placeAddress);
        HashMap<String,Object> hashMap = null;
        try{
            hashMap = new Gson().fromJson(json, HashMap.class);
            hashMap.put(PlaceAddress.PLACE_ID,placeId);
        }catch (Exception e){
            e.printStackTrace();
        }
        return hashMap;
    }

    public static PlaceAddress getMapToClass(HashMap<String, Object> map){
        PlaceAddress placeAddress = null;
        if(map != null){
            placeAddress = new PlaceAddress();
            Class aClass = placeAddress.getClass();
            for(Field field : aClass.getFields()){
                if(map.containsKey(field.getName())){
                    try {
                        field.set(placeAddress,map.get(field.getName()));
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        return placeAddress;
    }

//    firestore doesnt support multiple whereArrayContains even querying of the same key/object
//    error: Invalid Query. Queries only support having a single array-contains filter.
//    public static Query getQueriedPlaceIndex(Query itiRef, PlaceAddress placeAddress){
//        if(placeAddress != null){
//            ArrayList<String> placeIndex = getPlaceIndex(placeAddress);
//
//            if(placeIndex != null && !placeIndex.isEmpty()){
//                for(String index : placeIndex){
//                    itiRef = itiRef.whereArrayContains(Itinerary.PLACE_INDEX,index);
//                }
//            }
//        }
//
//        return itiRef;
//    }

    public static Query getQueriedPlaceAddress(Query itiRef, PlaceAddress placeAddress, String keyMap){
        //for creating index
//        placeAddress.country = "a";
//        placeAddress.administrative_area_level_1 = "a";
//        placeAddress.administrative_area_level_2 = "a";
//        placeAddress.administrative_area_level_3 = "a";
//        placeAddress.administrative_area_level_4 = "a";
//        placeAddress.administrative_area_level_5 = "a";
//        placeAddress.locality = "a";
//        placeAddress.sublocality = "a";
//        placeAddress.sublocality_level_1 = "a";
//        placeAddress.sublocality_level_2 = "a";
//        placeAddress.sublocality_level_3 = "a";
//        placeAddress.sublocality_level_4 = "a";
//        placeAddress.sublocality_level_5 = "a";
//        placeAddress.street_number = "a";
//        placeAddress.route = "a";
//        placeAddress.street_address = "a";
//        placeAddress.premise = "a";
//        placeAddress.subpremise = "a";
//        placeAddress.postal_code = "a";

        if(placeAddress != null){
            String paMapKey = "";
            if(keyMap != null && !keyMap.isEmpty()){
                paMapKey += keyMap + ".";
            }

            paMapKey += PLACE_INDEX + ".";

            //COUNTRY
            if(placeAddress.country != null){
                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.country, true );
            }

            //ADMINISTRATIVE AREA
            if(placeAddress.administrative_area_level_1 != null){
                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.administrative_area_level_1, true );
            }

            if(placeAddress.administrative_area_level_2 != null){
                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.administrative_area_level_2, true );
            }

            if(placeAddress.administrative_area_level_3 != null){
                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.administrative_area_level_3, true );
            }

            if(placeAddress.administrative_area_level_4 != null){
                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.administrative_area_level_4, true );
            }

            if(placeAddress.administrative_area_level_5 != null){
                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.administrative_area_level_5, true );
            }

            //LOCALITY
            if(placeAddress.locality != null){
                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.locality, true );
            }

            //SUBLOCALITY
            if(placeAddress.sublocality != null){
                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.sublocality, true );
            }

            if(placeAddress.sublocality_level_1 != null){
                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.sublocality_level_1, true );
            }

            if(placeAddress.sublocality_level_2 != null){
                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.sublocality_level_2, true );
            }

            if(placeAddress.sublocality_level_3 != null){
                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.sublocality_level_3, true );
            }

            if(placeAddress.sublocality_level_4 != null){
                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.sublocality_level_4, true );
            }

            if(placeAddress.sublocality_level_5 != null){
                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.sublocality_level_5, true );
            }

            //dont too specific la i guess if not no result will come out 1 unless u are that specific
//            //STREET NUMBER
//            if(placeAddress.street_number != null){
//                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.street_number, true );
//            }
//
//            //ROUTE
//            if(placeAddress.route != null){
//                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.route, true );
//            }
//
//            //STREET ADDRESS
//            if(placeAddress.street_address != null){
//                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.street_address, true );
//            }
//
//            //PREMISE
//            if(placeAddress.premise != null){
//                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.premise, true );
//            }
//
//            if(placeAddress.subpremise != null){
//                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.subpremise, true );
//            }
//
//            //POSTAL CODE
//            if(placeAddress.postal_code != null){
//                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.postal_code, true );
//            }
        }

        return itiRef;
    }

    public static Query getQueriedPlaceAddressAdministrativeArea(Query itiRef, PlaceAddress placeAddress, String keyMap){
        if(placeAddress != null){
            String paMapKey = "";
            if(keyMap != null && !keyMap.isEmpty()){
                paMapKey += keyMap + ".";
            }

            paMapKey += PLACE_INDEX + ".";

            //COUNTRY
            if(placeAddress.country != null){
                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.country, true );
            }

            //ADMINISTRATIVE AREA
            if(placeAddress.administrative_area_level_1 != null){
                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.administrative_area_level_1, true );
            }

            if(placeAddress.administrative_area_level_2 != null){
                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.administrative_area_level_2, true );
            }

            if(placeAddress.administrative_area_level_3 != null){
                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.administrative_area_level_3, true );
            }

            if(placeAddress.administrative_area_level_4 != null){
                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.administrative_area_level_4, true );
            }

            if(placeAddress.administrative_area_level_5 != null){
                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.administrative_area_level_5, true );
            }
        }

        return itiRef;
    }

    public static Query getQueriedPlaceAddressLocality(Query itiRef, PlaceAddress placeAddress, String keyMap){
        if(placeAddress != null){
            String paMapKey = "";
            if(keyMap != null && !keyMap.isEmpty()){
                paMapKey += keyMap + ".";
            }

            paMapKey += PLACE_INDEX + ".";

            //COUNTRY
            if(placeAddress.country != null){
                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.country, true );
            }

            //ADMINISTRATIVE AREA
            if(placeAddress.administrative_area_level_1 != null){
                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.administrative_area_level_1, true );
            }

            if(placeAddress.administrative_area_level_2 != null){
                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.administrative_area_level_2, true );
            }

            if(placeAddress.administrative_area_level_3 != null){
                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.administrative_area_level_3, true );
            }

            if(placeAddress.administrative_area_level_4 != null){
                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.administrative_area_level_4, true );
            }

            if(placeAddress.administrative_area_level_5 != null){
                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.administrative_area_level_5, true );
            }

            //LOCALITY
            if(placeAddress.locality != null){
                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.locality, true );
            }
        }

        return itiRef;
    }

    public static Query getQueriedPlaceAddressSublocality(Query itiRef, PlaceAddress placeAddress, String keyMap){
        if(placeAddress != null){
            String paMapKey = "";
            if(keyMap != null && !keyMap.isEmpty()){
                paMapKey += keyMap + ".";
            }

            paMapKey += PLACE_INDEX + ".";

            //COUNTRY
            if(placeAddress.country != null){
                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.country, true );
            }

            //ADMINISTRATIVE AREA
            if(placeAddress.administrative_area_level_1 != null){
                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.administrative_area_level_1, true );
            }

            if(placeAddress.administrative_area_level_2 != null){
                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.administrative_area_level_2, true );
            }

            if(placeAddress.administrative_area_level_3 != null){
                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.administrative_area_level_3, true );
            }

            if(placeAddress.administrative_area_level_4 != null){
                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.administrative_area_level_4, true );
            }

            if(placeAddress.administrative_area_level_5 != null){
                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.administrative_area_level_5, true );
            }

            //LOCALITY
            if(placeAddress.locality != null){
                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.locality, true );
            }

            //SUBLOCALITY
            if(placeAddress.sublocality != null){
                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.sublocality, true );
            }

            if(placeAddress.sublocality_level_1 != null){
                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.sublocality_level_1, true );
            }

            if(placeAddress.sublocality_level_2 != null){
                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.sublocality_level_2, true );
            }

            if(placeAddress.sublocality_level_3 != null){
                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.sublocality_level_3, true );
            }

            if(placeAddress.sublocality_level_4 != null){
                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.sublocality_level_4, true );
            }

            if(placeAddress.sublocality_level_5 != null){
                itiRef = itiRef.whereEqualTo( paMapKey + placeAddress.sublocality_level_5, true );
            }
        }

        return itiRef;
    }

    public static HashMap<String,Boolean> getPlaceIndex(PlaceAddress placeAddress){
        HashMap<String,Boolean> placeIndex = new HashMap<>();

        if(placeAddress != null){
            //COUNTRY
            if(placeAddress.country != null){
                placeIndex.put(placeAddress.country,true);
            }

            //ADMINISTRATIVE AREA
            if(placeAddress.administrative_area_level_1 != null){
                placeIndex.put(placeAddress.administrative_area_level_1,true);
            }

            if(placeAddress.administrative_area_level_2 != null){
                placeIndex.put(placeAddress.administrative_area_level_2,true);
            }

            if(placeAddress.administrative_area_level_3 != null){
                placeIndex.put(placeAddress.administrative_area_level_3,true);
            }

            if(placeAddress.administrative_area_level_4 != null){
                placeIndex.put(placeAddress.administrative_area_level_4,true);
            }

            if(placeAddress.administrative_area_level_5 != null){
                placeIndex.put(placeAddress.administrative_area_level_5,true);
            }

            //LOCALITY
            if(placeAddress.locality != null){
                placeIndex.put(placeAddress.locality,true);
            }

            //SUBLOCALITY
            if(placeAddress.sublocality != null){
                placeIndex.put(placeAddress.sublocality,true);
            }

            if(placeAddress.sublocality_level_1 != null){
                placeIndex.put(placeAddress.sublocality_level_1,true);
            }

            if(placeAddress.sublocality_level_2 != null){
                placeIndex.put(placeAddress.sublocality_level_2,true);
            }

            if(placeAddress.sublocality_level_3 != null){
                placeIndex.put(placeAddress.sublocality_level_3,true);
            }

            if(placeAddress.sublocality_level_4 != null){
                placeIndex.put(placeAddress.sublocality_level_4,true);
            }

            if(placeAddress.sublocality_level_5 != null){
                placeIndex.put(placeAddress.sublocality_level_5,true);
            }

            //dont too specific la i guess if not no result will come out 1 unless u are that specific
//            //STREET NUMBER
            if(placeAddress.street_number != null){
                placeIndex.put(placeAddress.street_number,true);
            }

            //ROUTE
            if(placeAddress.route != null){
                placeIndex.put(placeAddress.route,true);
            }

            //STREET ADDRESS
            if(placeAddress.street_address != null){
                placeIndex.put(placeAddress.street_address,true);
            }

            //PREMISE
            if(placeAddress.premise != null){
                placeIndex.put(placeAddress.premise,true);
            }

            if(placeAddress.subpremise != null){
                placeIndex.put(placeAddress.subpremise,true);
            }

            //POSTAL CODE
            if(placeAddress.postal_code != null){
                placeIndex.put(placeAddress.postal_code,true);
            }
        }

        return placeIndex;
    }

    public static String getCombinedStates(PlaceAddress placeAddress){
        String combinedStates = "";
        if(placeAddress != null){
            if(placeAddress.administrative_area_level_1 != null){
                combinedStates += placeAddress.administrative_area_level_1 + " ";
            }

            if(placeAddress.administrative_area_level_2 != null){
                combinedStates += placeAddress.administrative_area_level_2 + " ";
            }

            if(placeAddress.administrative_area_level_3 != null){
                combinedStates += placeAddress.administrative_area_level_3 + " ";
            }

            if(placeAddress.administrative_area_level_4 != null){
                combinedStates += placeAddress.administrative_area_level_4 + " ";
            }

            if(placeAddress.administrative_area_level_5 != null){
                combinedStates += placeAddress.administrative_area_level_5 + " ";
            }
        }

        return combinedStates;
    }

}
