package com.vidatechft.yupa.classes.PlaceSearchRecommendedGoogleApi;

import com.google.gson.annotations.SerializedName;
import com.vidatechft.yupa.classes.PlaceDetailsGoogleApi.Geometry;

import java.util.ArrayList;

public class PlaceRecommendedResult {
    @SerializedName("formatted_address")
    public String formattedAddress;

    @SerializedName("geometry")
    public Geometry geometry;

    @SerializedName("name")
    public String name;

    @SerializedName("place_id")
    public String placeId;

    @SerializedName("rating")
    public Double rating;

    @SerializedName("types")
    public ArrayList<String> types;

    public PlaceRecommendedResult(){

    }

    public PlaceRecommendedResult(String formattedAddress, Geometry geometry, String name, String placeId, Double rating, ArrayList<String> types) {
        this.formattedAddress = formattedAddress;
        this.geometry = geometry;
        this.name = name;
        this.placeId = placeId;
        this.rating = rating;
        this.types = types;
    }
}
