package com.vidatechft.yupa.classes.PlaceDetailsGoogleApi;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GeometryGps {
    @SerializedName("lat")
    public Double lat;

    @SerializedName("lng")
    public Double lng;

    public GeometryGps(Double lat, Double lng) {
        this.lat = lat;
        this.lng = lng;
    }
}
