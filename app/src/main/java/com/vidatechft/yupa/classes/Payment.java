package com.vidatechft.yupa.classes;

import com.google.firebase.Timestamp;
import com.google.firebase.database.PropertyName;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.ServerTimestamp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vidatechft.yupa.activities.MainActivity;

import java.util.ArrayList;
import java.util.HashMap;

public class Payment {
    public static final String TRIGGER_CLIENT_TOKEN_URL = "paymentClientTokenTrigger";
    public static final String GET_CLIENT_TOKEN_URL = "paymentClientToken";
    public static final String CONTINUE_PAYMENT_URL = "paymentContinue";
    public static final String PAYMENT_RESULT_URL = "paymentResult";

    public static final String ITEM_TYPE_ITINERARY = "itinerary";
    public static final String ITEM_TYPE_TRAVEL_KIT = "travelKit";
    public static final String ITEM_TYPE_ROOM = "room";

    public static final String PICKUP_LOCATION_TYPE_HOTEL = "Hotel";
    public static final String PICKUP_LOCATION_TYPE_AIRPORT = "Airport";

    public static final String STATUS_PENDING = "pending";
    public static final String STATUS_SUCCESS = "success";
    public static final String STATUS_FAILED = "failed";

    public static final String ID = "id";
    public static final String USER_ID = "userId";
    public static final String ITEM_TYPE = "itemType";
    public static final String CLIENT_TOKEN = "clientToken";
    public static final String DESCRIBE_CONTENTS = "describeContents";
    public static final String DEVICE_DATA = "deviceData";
    public static final String PAYMENT_METHOD_NONCE = "paymentMethodNonce";
    public static final String PAYMENT_METHOD_TYPE = "paymentMethodType";
    public static final String AMOUNT = "amount";
    public static final String STATUS = "status";
    public static final String LOCAL_TIMESTAMP = "localTimestamp";//actually this is for querying to make sure that the payment made is this 1 unique only
    public static final String BOOKING_HISTORY = "bookingHistory";
    public static final String ITINERARY = "itinerary";
    public static final String TRAVEL_KITS = "travelKits";
    public static final String PAYMENT_AMOUNT = "paymentAmount";
    public static final String PICKUP_TIME = "pickupTime";
    public static final String PICKUP_LOCATION_NAME = "pickupLocationName";
    public static final String PICKUP_LOCATION_TYPE = "pickupLocationType";
    public static final String ERROR_MSG = "errorMsg";
    public static final String IS_READ_BY_ADMIN = "isReadByAdmin";
    public static final String DEPART_DATE = "departDate";
    public static final String RETURN_DATE = "returnDate";
    public static final String DATE_CREATED = "dateCreated";
    public static final String DATE_UPDATED = "dateUpdated";

    @Exclude
    public String id;

    @PropertyName(USER_ID)
    @SerializedName(USER_ID)
    @Expose
    public String userId;

    @PropertyName(ITEM_TYPE)
    @SerializedName(ITEM_TYPE)
    @Expose
    public HashMap<String,Boolean> itemType;

    @PropertyName(CLIENT_TOKEN)
    @SerializedName(CLIENT_TOKEN)
    @Expose
    public String clientToken;

    @PropertyName(DESCRIBE_CONTENTS)
    @SerializedName(DESCRIBE_CONTENTS)
    @Expose
    public int describeContents;

    @PropertyName(DEVICE_DATA)
    @SerializedName(DEVICE_DATA)
    @Expose
    public String deviceData;

    @PropertyName(PAYMENT_METHOD_NONCE)
    @SerializedName(PAYMENT_METHOD_NONCE)
    @Expose
    public Object paymentMethodNonce;

    @PropertyName(PAYMENT_METHOD_TYPE)
    @SerializedName(PAYMENT_METHOD_TYPE)
    @Expose
    public Object paymentMethodType;

    @PropertyName(AMOUNT)
    @SerializedName(AMOUNT)
    @Expose
    public Double amount;//this is from paypal 1 that is taken from my total price

    @PropertyName(STATUS)
    @SerializedName(STATUS)
    @Expose
    public String status;

    @PropertyName(BOOKING_HISTORY)
    @SerializedName(BOOKING_HISTORY)
    @Expose
    public BookingHistory bookingHistory;

    @PropertyName(ITINERARY)
    @SerializedName(ITINERARY)
    @Expose
    public Itinerary itinerary;

    @PropertyName(TRAVEL_KITS)
    @SerializedName(TRAVEL_KITS)
    @Expose
    public ArrayList<TravelKit> travelKits;

    @PropertyName(PAYMENT_AMOUNT)
    @SerializedName(PAYMENT_AMOUNT)
    @Expose
    public PaymentAmount paymentAmount;//this is my own custom 1 where i can know each item's price

    @PropertyName(PICKUP_TIME)
    @SerializedName(PICKUP_TIME)
    @Expose
    public Long pickupTime;

    @PropertyName(PICKUP_LOCATION_NAME)
    @SerializedName(PICKUP_LOCATION_NAME)
    @Expose
    public String pickupLocationName;

    @PropertyName(PICKUP_LOCATION_TYPE)
    @SerializedName(PICKUP_LOCATION_TYPE)
    @Expose
    public String pickupLocationType;

    @PropertyName(ERROR_MSG)
    @SerializedName(ERROR_MSG)
    @Expose
    public String errorMsg;

    @PropertyName(IS_READ_BY_ADMIN)
    @SerializedName(IS_READ_BY_ADMIN)
    @Expose
    public Boolean isReadByAdmin;

    @PropertyName(DEPART_DATE)
    @SerializedName(DEPART_DATE)
    @Expose
    public Long departDate;

    @PropertyName(RETURN_DATE)
    @SerializedName(RETURN_DATE)
    @Expose
    public Long returnDate;

    @PropertyName(LOCAL_TIMESTAMP)
    @SerializedName(LOCAL_TIMESTAMP)
    @Expose
    public Long localTimestamp;

    @ServerTimestamp
    @PropertyName(DATE_CREATED)
    @SerializedName(DATE_CREATED)
    @Expose
    public Timestamp dateCreated;

    @ServerTimestamp
    @PropertyName(DATE_UPDATED)
    @SerializedName(DATE_UPDATED)
    @Expose
    public Timestamp dateUpdated;

    public Payment(){

    }

    public static Payment getSnapshotToPayment(MainActivity parentActivity, DocumentSnapshot snapshot) {
        Payment tempPayment = snapshot.toObject(Payment.class);
        if(tempPayment != null){
            tempPayment.id = snapshot.getId();
            return tempPayment;
        }else{
            return null;
        }
    }
}
