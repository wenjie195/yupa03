package com.vidatechft.yupa.classes.SearchPlaceGoogleApi;

import com.google.gson.annotations.SerializedName;
import com.vidatechft.yupa.classes.PlaceDetailsGoogleApi.AddressComponent;

import java.util.ArrayList;

public class SearchPlaceResult {
    @SerializedName("candidates")
    public ArrayList<Candidates> candidates;

    @SerializedName("debug_log")
    public DebugLog debugLog;

    @SerializedName("status")
    public String status;

    public SearchPlaceResult() {

    }

    public SearchPlaceResult(ArrayList<Candidates> candidates, DebugLog debugLog, String status) {
        this.candidates = candidates;
        this.debugLog = debugLog;
        this.status = status;
    }
}
