package com.vidatechft.yupa.classes;

import android.support.v7.app.AppCompatActivity;
import android.util.SparseArray;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.Timestamp;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.PropertyName;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.ServerTimestamp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.maps.android.clustering.ClusterItem;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.PlaceDetailsGoogleApi.PlaceAddress;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.HashMap;

@IgnoreExtraProperties
public class Merchant implements ClusterItem {

    public static final String URL_MERCHANT = "merchantShop";
    public static final String URL_MERCHANT_DETAILS = "merchantDetails";
    public static final String DRIVER_LICENSE_PIC_URL = "merchantLicensePicUrl";
    public static final String URL_LICENSE = "license";
    public static final String URL_MERCHANT_SHOP_PIC = "shopPicUrl";

    public static final String MERCHANT_ID = "merchantId";
    public static final String MERCHANT_NAME = "name";
    public static final String MERCHANT_NRIC = "nric";
    public static final String MERCHANT_WORKPLACE = "workPlace";
    public static final String MERCHANT_WORKPLACE_ADDRESS = "workPlaceAdd";
    public static final String MERCHANT_PLACE_ADDRESS = "placeAddress";
    public static final String MERCHANT_PLACE_INDEX = "placeIndex";
    public static final String MERCHANT_SSM = "ssmLicense";
    public static final String MERCHANT_EMAIL = "email";
    public static final String MERCHANT_CONTACT_NO = "contactNo";
    public static final String MERCHANT_SOCIAL_MEDIA = "socialMedia";
    public static final String URL_MERCHANT_DRIVER_LICENSE_PIC = "licensePicUrl";
    public static final String URL_MERCHANT_BUSINESS_HOUR = "businessHour";
    public static final String URL_SHOP_PIC = "urlShopPic";
    public static final String URL_SHOP_PROOF = "proof";

    public static final String MERCHANT_DESC= "description";
    public static final String MERCHANT_FIELD_OF_INDUSTRY= "fieldOfIndustry";

    public static final String GPS = "gps";
    public static final String DATE_CREATED = "dateCreated";
    public static final String DATE_UPDATED = "dateUpdated";
    public static  final String SHOP_STATUS = "shopStatus";
    public static final String LIKE_COUNT = "likeCount";
    public static final String IS_DRAFT= "isDraft";
    public static final String IS_PUBLISH= "isPublish";
    public static final String STATUS_PENDING= "pending";
    public static final String STATUS_ACCEPTED= "accept";
    public static final String STATUS_REJECTED= "reject";

    public static final int SHOP_RADIUS = 500;//in meter (means 1km in distance)
    public static final float SHOP_ZOOM_IN_LEVEL = 16.25f;//in meter (means 1km in distance)


    @Exclude
    public SparseArray<ArrayList<BusinessHourTime>> businessHourTimes;

    @Exclude
    public Favourite favourite;

    @Exclude
    public String id;

    @PropertyName(MERCHANT_ID)
    @SerializedName(MERCHANT_ID)
    @Expose
    public String merchantId;

    @PropertyName(MERCHANT_NAME)
    @SerializedName(MERCHANT_NAME)
    @Expose
    public String name;

    @PropertyName(MERCHANT_NRIC)
    @SerializedName(MERCHANT_NRIC)
    @Expose
    public String nric;

    @PropertyName(MERCHANT_WORKPLACE)
    @SerializedName(MERCHANT_WORKPLACE)
    @Expose
    public String workPlace;

    @PropertyName(MERCHANT_WORKPLACE_ADDRESS)
    @SerializedName(MERCHANT_WORKPLACE_ADDRESS)
    @Expose
    public String workPlaceAdd;

    @PropertyName(MERCHANT_PLACE_ADDRESS)
    @SerializedName(MERCHANT_PLACE_ADDRESS)
    @Expose
    public PlaceAddress placeAddress;

    @PropertyName(MERCHANT_PLACE_INDEX)
    @SerializedName(MERCHANT_PLACE_INDEX)
    @Expose
    public HashMap<String,Boolean> placeIndex;

    @PropertyName(MERCHANT_SSM)
    @SerializedName(MERCHANT_SSM)
    @Expose
    public String ssmLicense;

    @PropertyName(MERCHANT_CONTACT_NO)
    @SerializedName(MERCHANT_CONTACT_NO)
    @Expose
    public String contactNo;

    @PropertyName(MERCHANT_SOCIAL_MEDIA)
    @SerializedName(MERCHANT_SOCIAL_MEDIA)
    @Expose
    public String socialMedia;

    @PropertyName(MERCHANT_EMAIL)
    @SerializedName(MERCHANT_EMAIL)
    @Expose
    public String email;

    @PropertyName(MERCHANT_FIELD_OF_INDUSTRY)
    @SerializedName(MERCHANT_FIELD_OF_INDUSTRY)
    @Expose
    public String fieldOfIndustry;

    @PropertyName(MERCHANT_DESC)
    @SerializedName(MERCHANT_DESC)
    @Expose
    public String description;

    @PropertyName(DRIVER_LICENSE_PIC_URL)
    @SerializedName(DRIVER_LICENSE_PIC_URL)
    @Expose
    public String merchantLicensePicUrl;

    @PropertyName(URL_MERCHANT_SHOP_PIC)
    @SerializedName(URL_MERCHANT_SHOP_PIC)
    @Expose
    public String shopPicUrl;

    @PropertyName(GPS)
    @SerializedName(GPS)
    @Expose
    public GeoPoint gps;

    @ServerTimestamp
    @PropertyName(DATE_CREATED)
    @SerializedName(DATE_CREATED)
    @Expose
    public Timestamp dateCreated;

    @ServerTimestamp
    @PropertyName(DATE_UPDATED)
    @SerializedName(DATE_UPDATED)
    @Expose
    public Timestamp dateUpdated;

    @PropertyName(SHOP_STATUS)
    @SerializedName(SHOP_STATUS)
    @Expose
    public String shopStatus;

    @PropertyName(IS_DRAFT)
    @SerializedName(IS_DRAFT)
    @Expose
    public Boolean isDraft;

    @PropertyName(IS_PUBLISH)
    @SerializedName(IS_PUBLISH)
    @Expose
    public Boolean isPublish;

    @PropertyName(LIKE_COUNT)
    @SerializedName(LIKE_COUNT)
    @Expose
    public Long likeCount;


//    @PropertyName(IS_DRAFT)
//    @SerializedName(IS_DRAFT)
//    @Expose
//    public String isDraft;


    public Merchant()
    {

    }

    public static Merchant snapshotToMerchant(MainActivity parentActivity, DocumentSnapshot snapshot) {
        Merchant tempMerchant = snapshot.toObject(Merchant.class);
        if(tempMerchant != null){
            tempMerchant.id = snapshot.getId();
            try{
                tempMerchant.placeAddress = PlaceAddress.getMapToClass((HashMap<String, Object>) snapshot.get(Job.PLACE_ADDRESS));
            }catch (Exception e){
                e.printStackTrace();
            }
            return tempMerchant;
        }else{
            return null;
        }
    }

    public Merchant(MainActivity parentActivity, QueryDocumentSnapshot snapshot) {
        this.name = snapshot.getString(MERCHANT_NAME);

    }

    @Override
    public LatLng getPosition() {
        return new LatLng(gps.getLatitude(),gps.getLongitude());
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public String getSnippet() {
        return String.valueOf(workPlace);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GeoPoint getGps() {
        return gps;
    }

    public void setGps(GeoPoint gps) {
        this.gps = gps;
    }


    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getShopPicUrl() {
        return shopPicUrl;
    }

    public void setShopPicUrl(String shopPicUrl) {
        this.shopPicUrl = shopPicUrl;
    }
}