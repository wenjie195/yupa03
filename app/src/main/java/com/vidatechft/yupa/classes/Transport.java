package com.vidatechft.yupa.classes;

import android.support.v7.app.AppCompatActivity;

import com.google.firebase.Timestamp;
import com.google.firebase.database.PropertyName;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.ServerTimestamp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Transport {
    public static final String URL_TRANSPORT = "transport";
    public static final String URL_TRANSPORT_PHOTO = "transportPhoto";

    public static final String TYPE_CAR = "car";
    public static final String TYPE_MOTORBIKE = "motorbike";
    public static final String TYPE_BICYCLE = "bicycle";

    public static final String MAKE = "make";
    public static final String MODEL = "model";
    public static final String COLOUR = "colour";
    public static final String PLATE_NUMBER = "plateNo";
    public static final String MANUFACTURE_DATE = "manufactureDate";
    public static final String PER_HOUR = "perHour";
    public static final String PER_DAY = "perDay";
    public static final String PER_MONTH = "perMonth";
    public static final String PHOTO_URL = "photoUrl";
    public static final String TYPE = "type";
    public static final String CURRENCY_CODE = "currencyCode";
    public static final String DATE_CREATED = "dateCreated";
    public static final String DATE_UPDATED = "dateUpdated";

    @Exclude
    public String id;

    @Exclude
    public int tempId;


    @PropertyName(MAKE)
    @SerializedName(MAKE)
    @Expose
    public String make;

    @PropertyName(MODEL)
    @SerializedName(MODEL)
    @Expose
    public String model;

    @PropertyName(COLOUR)
    @SerializedName(COLOUR)
    @Expose
    public String colour;

    @PropertyName(CURRENCY_CODE)
    @SerializedName(CURRENCY_CODE)
    @Expose
    public String currencyCode;

    @PropertyName(PLATE_NUMBER)
    @SerializedName(PLATE_NUMBER)
    @Expose
    public String plateNo;

    @PropertyName(MANUFACTURE_DATE)
    @SerializedName(MANUFACTURE_DATE)
    @Expose
    public Long manufactureDate;

    @PropertyName(PER_HOUR)
    @SerializedName(PER_HOUR)
    @Expose
    public Double perHour;

    @PropertyName(PER_DAY)
    @SerializedName(PER_DAY)
    @Expose
    public Double perDay;

    @PropertyName(PER_MONTH)
    @SerializedName(PER_MONTH)
    @Expose
    public Double perMonth;

    @PropertyName(PHOTO_URL)
    @SerializedName(PHOTO_URL)
    @Expose
    public String photoUrl;

    @PropertyName(TYPE)
    @SerializedName(TYPE)
    @Expose
    public String type;

    @ServerTimestamp
    @PropertyName(DATE_CREATED)
    @SerializedName(DATE_CREATED)
    @Expose
    public Timestamp dateCreated; // from here https://stackoverflow.com/questions/46254266/firebase-no-properties-to-serialize-found-on-class

    @ServerTimestamp
    @PropertyName(DATE_UPDATED)
    @SerializedName(DATE_UPDATED)
    @Expose
    public Timestamp dateUpdated;

    public Transport() {

    }

    public Transport(String id, String make) {
        this.id = id;
        this.make = make;
    }

    public static Transport getTransport(AppCompatActivity parentActivity, DocumentSnapshot snapshot) {
        if (snapshot != null && snapshot.exists()) {
            Transport transport = snapshot.toObject(Transport.class);
            if (transport != null) {
                transport.id = snapshot.getId();
            }
            return transport;
        } else {
            return null;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null)
            return false;

        Transport itemCompare = (Transport) obj;
        if(itemCompare.id.equals(this.id))
            return true;

        return false;
    }
}
