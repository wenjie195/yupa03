package com.vidatechft.yupa.classes;

import android.support.v7.app.AppCompatActivity;

import com.google.firebase.database.PropertyName;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Description {
    public static final String URL_DESCRIPTION = "description";
    public static final String DESCRIPTION_DETAILS = "descriptionDetails";
    public static final String DIR_DESCRIPTION = "dirDescription";
    public static final String DESCRIPTION_ACCOMMODATION = "descriptionAccommodation";

    @PropertyName(DESCRIPTION_ACCOMMODATION)
    @SerializedName(DESCRIPTION_ACCOMMODATION)
    @Expose
    public String descriptionAccommodation;

    public Description(){

    }

    public Description(AppCompatActivity parentActivity, DocumentSnapshot snapshot){
        this.descriptionAccommodation = snapshot.getString(Description.DESCRIPTION_ACCOMMODATION);
    }
}
