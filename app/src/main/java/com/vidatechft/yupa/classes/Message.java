package com.vidatechft.yupa.classes;

import com.google.firebase.Timestamp;
import com.google.firebase.database.PropertyName;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.ServerTimestamp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Message {
    public static final String URL_MSG = "msg";
    public static final String URL_MSG_PIC = "msgPic";

    public static final String ID = "id";
    public static final String UID = "uid";
    public static final String MSG = "msg";
    public static final String IS_IMG = "isImg";
    public static final String DATE_CREATED = "dateCreated";
    public static final String IMG = "img";

    @Exclude
    public boolean isThisUser = false;

    @Exclude
    public User user = new User();

    @Exclude
    public int pos = 0;

    @Exclude
    public GroupChat groupChat = new GroupChat();

    @PropertyName(UID)
    @SerializedName(UID)
    @Expose
    public String uid;

    @PropertyName(MSG)
    @SerializedName(MSG)
    @Expose
    public String msg;

    @PropertyName(IMG)
    @SerializedName(IMG)
    @Expose
    public String img;

    @PropertyName(IS_IMG)
    @SerializedName(IS_IMG)
    @Expose
    public boolean isImg;

    @ServerTimestamp
    @PropertyName(DATE_CREATED)
    @SerializedName(DATE_CREATED)
    @Expose
    public Timestamp dateCreated; // from here https://stackoverflow.com/questions/46254266/firebase-no-properties-to-serialize-found-on-class

    public Message()
    {

    }

    public Message(String uid, String msg, boolean isImg) {
        this.uid = uid;
        this.msg = msg;
        this.isImg = isImg;
    }
}
