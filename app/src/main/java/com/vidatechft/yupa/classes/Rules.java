package com.vidatechft.yupa.classes;

import android.support.v7.app.AppCompatActivity;

import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.PropertyName;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@IgnoreExtraProperties
public class Rules {
    public static final String URL_RULES = "rules";
    public static final String RULES_DETAILS = "rulesDetails";
    public static final String DIR_RULES= "dirRules";

    public static final String MINIMIUM_NIGHTS= "minNightStay";
    public static final String MAXIMUM_NIGHTS= "maxNightStay";
    public static final String CHECK_IN= "checkIn";
    public static final String CHECK_OUT= "checkOut";

    //selection
    public static final String PET_ALLOWED= "petAllowed";
    public static final String COOKING_ALLOWED= "cookingAllowed";
    public static final String SMOKE_ALLOWED= "smokeAllowed";
    public static final String LAUNDRY_ALLOWED= "laundryAllowed";
    public static final String EVENT_PARTY_ALLOWED= "eventPartyAllowed";
    public static final String NOISY_ALLOWED= "noisyAllowed";
    public static final String SHOE_ALLOWED= "shoeAllowed";

    //selection
    public static final String petAllowed= "petAllowed";
    public static final String cookingAllowed= "cookingAllowed";
    public static final String smokeAllowed= "smokeAllowed";
    public static final String laundryAllowed= "laundryAllowed";
    public static final String eventPartyAllowed= "eventPartyAllowed";
    public static final String noisyAllowed= "noisyAllowed";
    public static final String shoeAllowed= "shoeAllowed";

    @Exclude
    public ArrayList<String> maxNightSet = new ArrayList<>();

    @Exclude
    public ArrayList<String> minNightSet = new ArrayList<>();

    @Exclude
    public ArrayList<String> checkInSet = new ArrayList<>();

    @Exclude
    public ArrayList<String> checkOutSet = new ArrayList<>();

    @PropertyName(MINIMIUM_NIGHTS)
    @SerializedName(MINIMIUM_NIGHTS)
    @Expose
    public String minNightStay;

    @PropertyName(MAXIMUM_NIGHTS)
    @SerializedName(MAXIMUM_NIGHTS)
    @Expose
    public String maxNightStay;

    @PropertyName(CHECK_IN)
    @SerializedName(CHECK_IN)
    @Expose
    public String checkIn;

    @PropertyName(CHECK_OUT)
    @SerializedName(CHECK_OUT)
    @Expose
    public String checkOut;

    @PropertyName(PET_ALLOWED)
    @SerializedName(PET_ALLOWED)
    @Expose
    public Boolean petAllow;

    @PropertyName(COOKING_ALLOWED)
    @SerializedName(COOKING_ALLOWED)
    @Expose
    public Boolean cookingAllow;

    @PropertyName(SMOKE_ALLOWED)
    @SerializedName(SMOKE_ALLOWED)
    @Expose
    public Boolean smokeAllow;

    @PropertyName(LAUNDRY_ALLOWED)
    @SerializedName(LAUNDRY_ALLOWED)
    @Expose
    public Boolean laundryAllow;

    @PropertyName(EVENT_PARTY_ALLOWED)
    @SerializedName(EVENT_PARTY_ALLOWED)
    @Expose
    public Boolean eventPartyAllow;

    @PropertyName(NOISY_ALLOWED)
    @SerializedName(NOISY_ALLOWED)
    @Expose
    public Boolean noisyAllow;

    @PropertyName(SHOE_ALLOWED)
    @SerializedName(SHOE_ALLOWED)
    @Expose
    public Boolean shoeAllow;

    public Rules(){

    }
    public Rules(AppCompatActivity parentActivity, QueryDocumentSnapshot snapshot){

        this.checkIn = snapshot.getString(Rules.CHECK_IN);
        this.checkOut = snapshot.getString(Rules.CHECK_OUT);

        this.maxNightStay = snapshot.getString(Rules.MAXIMUM_NIGHTS);
        this.minNightStay = snapshot.getString(Rules.MINIMIUM_NIGHTS);
        this.petAllow = snapshot.getBoolean(Rules.PET_ALLOWED);
        this.cookingAllow = snapshot.getBoolean(Rules.COOKING_ALLOWED);
        this.smokeAllow = snapshot.getBoolean(Rules.SMOKE_ALLOWED);
        this.laundryAllow = snapshot.getBoolean(Rules.LAUNDRY_ALLOWED);
        this.eventPartyAllow = snapshot.getBoolean(Rules.EVENT_PARTY_ALLOWED);
        this.noisyAllow = snapshot.getBoolean(Rules.NOISY_ALLOWED);
        this.shoeAllow = snapshot.getBoolean(Rules.SHOE_ALLOWED);

    }

    public boolean isPetAllow() {
        return petAllow;
    }

    public void setPetAllow(boolean petAllow) {
        this.petAllow = petAllow;
    }

    public boolean isCookingAllow() {
        return cookingAllow;
    }

    public void setCookingAllow(boolean cookingAllow) {
        this.cookingAllow = cookingAllow;
    }

    public boolean isSmokeAllow() {
        return smokeAllow;
    }

    public void setSmokeAllow(boolean smokeAllow) {
        this.smokeAllow = smokeAllow;
    }

    public boolean isLaundryAllow() {
        return laundryAllow;
    }

    public void setLaundryAllow(boolean laundryAllow) {
        this.laundryAllow = laundryAllow;
    }

    public boolean isEventPartyAllow() {
        return eventPartyAllow;
    }

    public void setEventPartyAllow(boolean eventPartyAllow) {
        this.eventPartyAllow = eventPartyAllow;
    }

    public boolean isNoisyAllow() {
        return noisyAllow;
    }

    public void setNoisyAllow(boolean noisyAllow) {
        this.noisyAllow = noisyAllow;
    }

    public boolean isShoeAllow() {
        return shoeAllow;
    }

    public void setShoeAllow(boolean shoeAllow) {
        this.shoeAllow = shoeAllow;
    }

    public static HashMap<String,Object> classToMapBoolean(Rules rules){
        HashMap<String,Object> rulesMap = new HashMap<>();
        rulesMap.put(PET_ALLOWED,rules.petAllow);
        rulesMap.put(COOKING_ALLOWED,rules.cookingAllow);
        rulesMap.put(SMOKE_ALLOWED,rules.smokeAllow);
        rulesMap.put(LAUNDRY_ALLOWED,rules.laundryAllow);
        rulesMap.put(EVENT_PARTY_ALLOWED,rules.eventPartyAllow);
        rulesMap.put(NOISY_ALLOWED,rules.noisyAllow);
        rulesMap.put(SHOE_ALLOWED,rules.shoeAllow);

        return rulesMap;
    }

    public static Rules mapToClassBoolean(Map<String,Object> ruleMap, AppCompatActivity parentActivity){
        Rules rule = new Rules();
        rule.petAllow = (Boolean) ruleMap.get(PET_ALLOWED);
        rule.cookingAllow = (Boolean) ruleMap.get(COOKING_ALLOWED);
        rule.smokeAllow = (Boolean) ruleMap.get(SMOKE_ALLOWED);
        rule.laundryAllow = (Boolean) ruleMap.get(LAUNDRY_ALLOWED);
        rule.eventPartyAllow = (Boolean) ruleMap.get(EVENT_PARTY_ALLOWED);
        rule.noisyAllow = (Boolean) ruleMap.get(NOISY_ALLOWED);
        rule.shoeAllow = (Boolean) ruleMap.get(SHOE_ALLOWED);

        return rule;
    }

    public static HashMap<String,Object> classToMapString(Rules rules){
        HashMap<String,Object> rulesMap = new HashMap<>();

        rulesMap.put(CHECK_IN,rules.checkIn);
        rulesMap.put(CHECK_OUT,rules.checkOut);
        rulesMap.put(MINIMIUM_NIGHTS,rules.minNightStay);
        rulesMap.put(MAXIMUM_NIGHTS,rules.maxNightStay);

        return rulesMap;
    }

    public static Rules mapToClassString(Map<String,Object> ruleMap, AppCompatActivity parentActivity){
        Rules rule = new Rules();
        rule.checkIn = (String) ruleMap.get(CHECK_IN);
        rule.checkOut = (String) ruleMap.get(CHECK_OUT);
        rule.minNightStay = (String) ruleMap.get(MINIMIUM_NIGHTS);
        rule.maxNightStay = (String) ruleMap.get(MAXIMUM_NIGHTS);

        return rule;
    }
}

