package com.vidatechft.yupa.classes;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.SparseArray;

import com.google.firebase.Timestamp;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.PropertyName;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.ServerTimestamp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.itinerary.ItineraryPlansContainerFragment;
import com.vidatechft.yupa.itinerary.StartPlanItineraryFragment;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@IgnoreExtraProperties
public class RequestCustomItinerary{
    //**************************************************NOTE NOT USING ANYMORE******************************************************************/

    public static final String URL_REQUEST_CUSTOM_ITINERARY = "requestCustomItinerary";
    public static final String STATUS_PENDING = "pending";
    public static final String STATUS_DONE = "done";

    public static final String ID = "id";
    public static final String USER_ID = "userId";
    public static final String STATUS = "status";
    public static final String ITINERARY_ID = "itineraryId";
    public static final String DATE_CREATED = "dateCreated";
    public static final String DATE_UPDATED = "dateUpdated";

    @Exclude
    public String id;

    @PropertyName(USER_ID)
    @SerializedName(USER_ID)
    @Expose
    public String userId;

    @PropertyName(STATUS)
    @SerializedName(STATUS)
    @Expose
    public String status;

    @PropertyName(ITINERARY_ID)
    @SerializedName(ITINERARY_ID)
    @Expose
    public String itineraryId;

    @ServerTimestamp
    @PropertyName(DATE_CREATED)
    @SerializedName(DATE_CREATED)
    @Expose
    public Timestamp dateCreated;

    @ServerTimestamp
    @PropertyName(DATE_UPDATED)
    @SerializedName(DATE_UPDATED)
    @Expose
    public Timestamp dateUpdated;

    public RequestCustomItinerary(){

    }

    public RequestCustomItinerary(String userId, String itineraryId) {
        this.userId = userId;
        this.itineraryId = itineraryId;
        this.status = STATUS_PENDING;
    }

    public static RequestCustomItinerary snapshotToClass(MainActivity parentActivity, DocumentSnapshot snapshot) {
        RequestCustomItinerary tempClass = snapshot.toObject(RequestCustomItinerary.class);
        if(tempClass != null){
            tempClass.id = snapshot.getId();
            return tempClass;
        }else{
            return null;
        }
    }

}