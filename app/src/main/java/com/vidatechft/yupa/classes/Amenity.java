package com.vidatechft.yupa.classes;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;

import com.google.firebase.database.PropertyName;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Amenity {
    public static final String URL_AMENITY = "amenity";
    public static final String AMENITY_DETAILS = "amenityDetails";
    public static final String DIR_AMENITY = "dirAmenity";

    public static final String AIR_CONDITIONER = "Air-Conditioner";
    public static final String BBQ_AREA = "BBQ Area";
    public static final String BBQ_TOOL = "BBQ Tool";
    public static final String BATH_TUB = "Bath Tub";
    public static final String CAR_PARK = "Car Park";
    public static final String CLEANING_SERVICE = "Cleaning Service After Checkout";
    public static final String CLOTHES_IRON = "Clothes Iron";
    public static final String COOKING_UTENSIL = "Cooking Utensil";
    public static final String GYM = "Gym";
    public static final String HAIR_DRYER = "Hair Dryer";
    public static final String KID_PLAYGROUND = "Kid's Playground";
    public static final String SECURITY = "Security";
    public static final String SHAMPOO = "Shampoo";
    public static final String SOFA = "Sofa";
    public static final String SWIMMING_POOL = "Swimming Pool";
    public static final String TV = "TV";
    public static final String TOILET_PAPER = "Toilet Paper";
    public static final String TOWEL = "Towel";
    public static final String WASHING_MACHINE = "Washing Machine";
    public static final String WATER_HEATER = "Water Heater";
    public static final String WIFI = "Wifi";

    @Exclude
    public ArrayList<String> amenityScope = new ArrayList<>();

    @Exclude
    public ArrayList<String> amenityList = new ArrayList<>();

    @PropertyName(AIR_CONDITIONER)
    @SerializedName(AIR_CONDITIONER)
    @Expose
    public Boolean airConditioner;

    @PropertyName(BBQ_AREA)
    @SerializedName(BBQ_AREA)
    @Expose
    public Boolean bbqArea;

    @PropertyName(BBQ_TOOL)
    @SerializedName(BBQ_TOOL)
    @Expose
    public Boolean bbqTool;

    @PropertyName(BATH_TUB)
    @SerializedName(BATH_TUB)
    @Expose
    public Boolean bathTub;

    @PropertyName(CAR_PARK)
    @SerializedName(CAR_PARK)
    @Expose
    public Boolean carPark;

    @PropertyName(CLEANING_SERVICE)
    @SerializedName(CLEANING_SERVICE)
    @Expose
    public Boolean cleaningService;

    @PropertyName(CLOTHES_IRON)
    @SerializedName(CLOTHES_IRON)
    @Expose
    public Boolean clothesIron;

    @PropertyName(COOKING_UTENSIL)
    @SerializedName(COOKING_UTENSIL)
    @Expose
    public Boolean cookingUtensil;

    @PropertyName(GYM)
    @SerializedName(GYM)
    @Expose
    public Boolean gym;

    @PropertyName(HAIR_DRYER)
    @SerializedName(HAIR_DRYER)
    @Expose
    public Boolean hairDryer;

    @PropertyName(KID_PLAYGROUND)
    @SerializedName(KID_PLAYGROUND)
    @Expose
    public Boolean kidPlayground;

    @PropertyName(SECURITY)
    @SerializedName(SECURITY)
    @Expose
    public Boolean security;

    @PropertyName(SHAMPOO)
    @SerializedName(SHAMPOO)
    @Expose
    public Boolean shampoo;

    @PropertyName(SOFA)
    @SerializedName(SOFA)
    @Expose
    public Boolean sofa;

    @PropertyName(SWIMMING_POOL)
    @SerializedName(SWIMMING_POOL)
    @Expose
    public Boolean swimmingPool;

    @PropertyName(TV)
    @SerializedName(TV)
    @Expose
    public Boolean tv;

    @PropertyName(TOILET_PAPER)
    @SerializedName(TOILET_PAPER)
    @Expose
    public Boolean toiletPaper;

    @PropertyName(TOWEL)
    @SerializedName(TOWEL)
    @Expose
    public Boolean towel;

    @PropertyName(WASHING_MACHINE)
    @SerializedName(WASHING_MACHINE)
    @Expose
    public Boolean washingMachine;

    @PropertyName(WATER_HEATER)
    @SerializedName(WATER_HEATER)
    @Expose
    public Boolean waterHeater;

    @PropertyName(WIFI)
    @SerializedName(WIFI)
    @Expose
    public Boolean wifi;

    public Amenity(){

    }

    public Amenity(MainActivity parentActivity){

    }

    public Amenity(AppCompatActivity parentActivity, DocumentSnapshot snapshot){

        this.amenityScope = GeneralFunction.copyListIfMatchValueQuerySnapshot(parentActivity,snapshot, R.array.host_select_amenity_list);


    }

    public Boolean isAirConditioner() {
        return airConditioner;
    }

    public void setAirConditioner(Boolean airConditioner) {
        this.airConditioner = airConditioner;
    }

    public Boolean isBbqArea() {
        return bbqArea;
    }

    public void setBbqArea(Boolean bbqArea) {
        this.bbqArea = bbqArea;
    }

    public Boolean isBbqTool() {
        return bbqTool;
    }

    public void setBbqTool(Boolean bbqTool) {
        this.bbqTool = bbqTool;
    }

    public Boolean isBathTub() {
        return bathTub;
    }

    public void setBathTub(Boolean bathTub) {
        this.bathTub = bathTub;
    }

    public Boolean isCarPark() {
        return carPark;
    }

    public void setCarPark(Boolean carPark) {
        this.carPark = carPark;
    }

    public Boolean isCleaningService() {
        return cleaningService;
    }

    public void setCleaningService(Boolean cleaningService) {
        this.cleaningService = cleaningService;
    }

    public Boolean isClothesIron() {
        return clothesIron;
    }

    public void setClothesIron(Boolean clothesIron) {
        this.clothesIron = clothesIron;
    }

    public Boolean isCookingUtensil() {
        return cookingUtensil;
    }

    public void setCookingUtensil(Boolean cookingUtensil) {
        this.cookingUtensil = cookingUtensil;
    }

    public Boolean isGym() {
        return gym;
    }

    public void setGym(Boolean gym) {
        this.gym = gym;
    }

    public Boolean isHairDryer() {
        return hairDryer;
    }

    public void setHairDryer(Boolean hairDryer) {
        this.hairDryer = hairDryer;
    }

    public Boolean isKidPlayground() {
        return kidPlayground;
    }

    public void setKidPlayground(Boolean kidPlayground) {
        this.kidPlayground = kidPlayground;
    }

    public Boolean isSecurity() {
        return security;
    }

    public void setSecurity(Boolean security) {
        this.security = security;
    }

    public Boolean isShampoo() {
        return shampoo;
    }

    public void setShampoo(Boolean shampoo) {
        this.shampoo = shampoo;
    }

    public Boolean isSofa() {
        return sofa;
    }

    public void setSofa(Boolean sofa) {
        this.sofa = sofa;
    }

    public Boolean isSwimmingPool() {
        return swimmingPool;
    }

    public void setSwimmingPool(Boolean swimmingPool) {
        this.swimmingPool = swimmingPool;
    }

//    public boolean isTv() {
//        return tv;
//    }
//
//    public void setTv(boolean tv) {
//        this.tv = tv;
//    }

    public Boolean isToiletPaper() {
        return toiletPaper;
    }

    public void setToiletPaper(Boolean toiletPaper) {
        this.toiletPaper = toiletPaper;
    }

    public Boolean isTowel() {
        return towel;
    }

    public void setTowel(Boolean towel) {
        this.towel = towel;
    }

    public Boolean isWashingMachine() {
        return washingMachine;
    }

    public void setWashingMachine(Boolean washingMachine) {
        this.washingMachine = washingMachine;
    }

    public Boolean isWaterHeater() {
        return waterHeater;
    }

    public void setWaterHeater(Boolean waterHeater) {
        this.waterHeater = waterHeater;
    }

    public Boolean isWifi() {
        return wifi;
    }

    public void setWifi(Boolean wifi) {
        this.wifi = wifi;
    }

    public static HashMap<String,Object> toMap(Amenity amenity, Activity parentActivity){
        HashMap<String,Object> amenityMap = new HashMap<>();

        for(String cuisine : amenity.amenityList){
            amenityMap.put(cuisine,true);
        }

//        amenityMap.put(WIFI,amenity.wifi);
//        amenityMap.put(TV,amenity.tv);
//        amenityMap.put(HAIR_DRYER,amenity.hairDryer);
//        amenityMap.put(SECURITY,amenity.security);
//        amenityMap.put(WATER_HEATER,amenity.waterHeater);
//        amenityMap.put(GYM,amenity.gym);
//        amenityMap.put(TOILET_PAPER,amenity.toiletPaper);
//        amenityMap.put(BBQ_AREA,amenity.bbqArea);
//        amenityMap.put(TOWEL,amenity.towel);
//        amenityMap.put(CLEANING_SERVICE,amenity.cleaningService);
//        amenityMap.put(CAR_PARK,amenity.carPark);
//        amenityMap.put(AIR_CONDITIONER,amenity.airConditioner);
//        amenityMap.put(SOFA,amenity.sofa);
//        amenityMap.put(CLOTHES_IRON,amenity.clothesIron);
//        amenityMap.put(BATH_TUB,amenity.bathTub);
//        amenityMap.put(SWIMMING_POOL,amenity.swimmingPool);
//        amenityMap.put(KID_PLAYGROUND,amenity.kidPlayground);
//        amenityMap.put(SHAMPOO,amenity.shampoo);
//        amenityMap.put(BBQ_TOOL,amenity.bbqTool);
//        amenityMap.put(WASHING_MACHINE,amenity.washingMachine);
//        amenityMap.put(COOKING_UTENSIL,amenity.cookingUtensil);


        return amenityMap;
    }

    public static Amenity toClass(Map<String,Object> amenityMap, AppCompatActivity parentActivity){
        Amenity amenity = new Amenity();

        List<String> amenityFullList = new ArrayList<>(Arrays.asList(parentActivity.getResources().getStringArray(R.array.host_select_amenity_list)));

        for(String amenities : amenityFullList){
            if(amenityMap.containsKey(amenities)){
                amenity.amenityList.add(amenities);
            }
        }


//        amenity.wifi = (Boolean) amenityMap.get(WIFI);
//        amenity.tv = (Boolean) amenityMap.get(TV);
//        amenity.hairDryer = (Boolean) amenityMap.get(HAIR_DRYER);
//        amenity.security = (Boolean) amenityMap.get(SECURITY);
//        amenity.waterHeater = (Boolean) amenityMap.get(WATER_HEATER);
//        amenity.gym = (Boolean) amenityMap.get(GYM);
//        amenity.toiletPaper = (Boolean) amenityMap.get(TOILET_PAPER);
//        amenity.bbqArea = (Boolean) amenityMap.get(BBQ_AREA);
//        amenity.towel = (Boolean) amenityMap.get(TOWEL);
//        amenity.cleaningService = (Boolean) amenityMap.get(CLEANING_SERVICE);
//        amenity.carPark = (Boolean) amenityMap.get(CAR_PARK);
//        amenity.airConditioner = (Boolean) amenityMap.get(AIR_CONDITIONER);
//        amenity.sofa = (Boolean) amenityMap.get(SOFA);
//        amenity.clothesIron = (Boolean) amenityMap.get(CLOTHES_IRON);
//        amenity.bathTub = (Boolean) amenityMap.get(BATH_TUB);
//        amenity.swimmingPool = (Boolean) amenityMap.get(SWIMMING_POOL);
//        amenity.kidPlayground = (Boolean) amenityMap.get(KID_PLAYGROUND);
//        amenity.shampoo = (Boolean) amenityMap.get(SHAMPOO);
//        amenity.bbqTool = (Boolean) amenityMap.get(BBQ_TOOL);
//        amenity.washingMachine = (Boolean) amenityMap.get(WASHING_MACHINE);
//        amenity.cookingUtensil = (Boolean) amenityMap.get(COOKING_UTENSIL);

        return amenity;
    }
}
