package com.vidatechft.yupa.classes;

import android.support.v7.app.AppCompatActivity;
import android.util.SparseArray;

import com.google.firebase.Timestamp;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.PropertyName;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.ServerTimestamp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@IgnoreExtraProperties
public class Friend {

    public static final String URL_FRIENDS = "friends";

    public static final String ID = "id";
    public static final String STATUS = "status";
    public static final String FRIEND_LIST = "friendList";
    public static final String TARGET_ID = "targetId"; //the one that needs to accept/decline/block friend request
    public static final String REQUESTOR_ID = "requestorId";//the one that request to add target as a friend
    public static final String BLOCKED_ID = "blockedId";//the one that request to add target as a friend
    public static final String DATE_CREATED = "dateCreated";
    public static final String DATE_UPDATED = "dateUpdated";

    public static final String TYPE_FRIEND = "friend";
    public static final String TYPE_PENDING = "pending";

    public static final String STATUS_PENDING = "pending";
    public static final String STATUS_BLOCKED = "blocked";
    public static final String STATUS_UNBLOCKED = "unblocked";
    public static final String STATUS_ACCEPTED = "accepted";
    public static final String STATUS_REJECTED = "rejected"; //if rejected, delete the whole thing

    @Exclude
    public String id;

    @Exclude
    public User user;

    @PropertyName(STATUS)
    @SerializedName(STATUS)
    @Expose
    public String status;

    @PropertyName(FRIEND_LIST)
    @SerializedName(FRIEND_LIST)
    @Expose
    public ArrayList<String> friendList;

    @PropertyName(TARGET_ID)
    @SerializedName(TARGET_ID)
    @Expose
    public String targetId;

    @PropertyName(REQUESTOR_ID)
    @SerializedName(REQUESTOR_ID)
    @Expose
    public String requestorId;

    @PropertyName(BLOCKED_ID)
    @SerializedName(BLOCKED_ID)
    @Expose
    public String blockedId;

    @ServerTimestamp
    @PropertyName(DATE_CREATED)
    @SerializedName(DATE_CREATED)
    @Expose
    public Timestamp dateCreated;

    @ServerTimestamp
    @PropertyName(DATE_UPDATED)
    @SerializedName(DATE_UPDATED)
    @Expose
    public Timestamp dateUpdated;

    public Friend(){

    }

    public Friend(String targetId, String requestorId) {
        this.targetId = targetId;
        this.requestorId = requestorId;
    }

    public static HashMap<String,Object> toMap(Friend friend){
        HashMap<String,Object> friendMap = new HashMap<>();

        friendMap.put(STATUS,friend.status);
        friendMap.put(FRIEND_LIST,friend.friendList);
        friendMap.put(TARGET_ID,friend.targetId);
        friendMap.put(REQUESTOR_ID,friend.requestorId);
        friendMap.put(BLOCKED_ID,friend.blockedId);
        friendMap.put(DATE_CREATED,friend.dateCreated);
        friendMap.put(DATE_UPDATED,friend.dateUpdated);

        return friendMap;
    }

    public static Friend toClass(HashMap<String,Object> friendMap, MainActivity parentActivity, String targetUid){
        Friend friend = new Friend();

        ArrayList<String> friendList = new ArrayList<>();
        friendList.add(parentActivity.uid);
        friendList.add(targetUid);

        friend.friendList = friendList;

        friend.id = (String) friendMap.get(ID);
        friend.status = (String) friendMap.get(STATUS);
        friend.targetId = (String) friendMap.get(TARGET_ID);
        friend.requestorId = (String) friendMap.get(REQUESTOR_ID);
        friend.blockedId = (String) friendMap.get(BLOCKED_ID);
        try{
            friend.dateCreated = (Timestamp) friendMap.get(DATE_CREATED);
        }catch (Exception e){

        }

        try{
            friend.dateUpdated = (Timestamp) friendMap.get(DATE_UPDATED);
        }catch (Exception e){

        }

        return friend;
    }

    public static Friend toClass(HashMap<String,Object> friendMap, MainActivity parentActivity,ArrayList<String> friendList){
        Friend friend = new Friend();

        friend.friendList = friendList;
        friend.id = (String) friendMap.get(ID);
        friend.status = (String) friendMap.get(STATUS);
        friend.targetId = (String) friendMap.get(TARGET_ID);
        friend.requestorId = (String) friendMap.get(REQUESTOR_ID);
        friend.blockedId = (String) friendMap.get(BLOCKED_ID);
        try{
            friend.dateCreated = (Timestamp) friendMap.get(DATE_CREATED);
        }catch (Exception e){

        }

        try{
            friend.dateUpdated = (Timestamp) friendMap.get(DATE_UPDATED);
        }catch (Exception e){

        }

        return friend;
    }

}