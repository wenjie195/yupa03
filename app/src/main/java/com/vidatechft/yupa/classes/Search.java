package com.vidatechft.yupa.classes;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.Timestamp;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.PropertyName;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.ServerTimestamp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vidatechft.yupa.classes.PlaceDetailsGoogleApi.PlaceAddress;

import java.util.ArrayList;
import java.util.HashMap;

@IgnoreExtraProperties
public class Search {
    public static final int TYPE_ITINERARY = 0;
    public static final int TYPE_HOMESTAY = 1;
    public static final int TYPE_JOB = 2;

    public static final String ID = "id";
    public static final String TITLE = "title";
    public static final String PLACE_ID = "placeId";
    public static final String PLACE_ADDRESS = "placeAddress";
    public static final String DATE = "date";
    public static final String CHECKIN_DATE = "checkin_date";
    public static final String CHECKOUT_DATE = "checkout_date";
    public static final String BOUNDING_BOX = "boundingBox";
    public static final String MIN_BUDGET = "minBudget";
    public static final String MAX_BUDGET = "maxBudget";
    public static final String ROOM_TYPE = "roomType";
    public static final String NOOF_GUEST = "noofGuest";
    public static final String NOOF_BED = "noofBed";
    public static final String AMENITY_LIST = "amenityList";
    public static final String ACTIVITY_LIST = "activityList";
    public static final String RADIUS = "radius";
    public static final String DATE_CREATED = "dateCreated";

    public static final int PROGRESS_MULTIPLIER = 250;

    @Exclude
    public String id;

    @PropertyName(TITLE)
    @SerializedName(TITLE)
    @Expose
    public String title;

    @PropertyName(PLACE_ID)
    @SerializedName(PLACE_ID)
    @Expose
    public String placeId;

    @PropertyName(PLACE_ADDRESS)
    @SerializedName(PLACE_ADDRESS)
    @Expose
    public PlaceAddress placeAddress;

    @PropertyName(DATE)
    @SerializedName(DATE)
    @Expose
    public long date;

    @PropertyName(CHECKIN_DATE)
    @SerializedName(CHECKIN_DATE)
    @Expose
    public long checkin_date;

    @PropertyName(CHECKOUT_DATE)
    @SerializedName(CHECKOUT_DATE)
    @Expose
    public long checkout_date;

    @PropertyName(BOUNDING_BOX)
    @SerializedName(BOUNDING_BOX)
    @Expose
    public HashMap<String,LatLng> boundingBox;

    @PropertyName(MIN_BUDGET)
    @SerializedName(MIN_BUDGET)
    @Expose
    public double minBudget;

    @PropertyName(MAX_BUDGET)
    @SerializedName(MAX_BUDGET)
    @Expose
    public double maxBudget;

    @PropertyName(ROOM_TYPE)
    @SerializedName(ROOM_TYPE)
    @Expose
    public String roomType;

    @PropertyName(NOOF_BED)
    @SerializedName(NOOF_BED)
    @Expose
    public int noofBed;

    @PropertyName(NOOF_GUEST)
    @SerializedName(NOOF_GUEST)
    @Expose
    public int noofGuest;

    @PropertyName(AMENITY_LIST)
    @SerializedName(AMENITY_LIST)
    @Expose
    public ArrayList<String> amenityList;

    @PropertyName(ACTIVITY_LIST)
    @SerializedName(ACTIVITY_LIST)
    @Expose
    public ArrayList<String> activityList;

    @PropertyName(RADIUS)
    @SerializedName(RADIUS)
    @Expose
    public Long radius;

    @ServerTimestamp
    @PropertyName(DATE_CREATED)
    @SerializedName(DATE_CREATED)
    @Expose
    public Timestamp dateCreated;

    public Search(){

    }

    public Search(String title, long date, HashMap<String, LatLng> boundingBox) {
        this.title = title == null ? "" : title;
        this.date = date;
        this.boundingBox = boundingBox == null ? new HashMap<String, LatLng>() : boundingBox;
    }

    public Search(String title, HashMap<String, LatLng> boundingBox, Long radius) {
        this.title = title;
        this.radius = radius;
        this.boundingBox = boundingBox;
    }
}