package com.vidatechft.yupa.classes;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.Timestamp;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.PropertyName;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.ServerTimestamp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.maps.android.clustering.ClusterItem;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.classes.PlaceDetailsGoogleApi.PlaceAddress;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.HashMap;

@IgnoreExtraProperties
public class Job implements ClusterItem, Comparable<Job> {

    public static final String URL_JOB = "job";
    public static final String JOB_PIC_URL = "jobPicUrl";

    public static final String ID = "id";
    public static final String EMPLOYER_ID = "employerId";
    public static final String STAFF_ID = "staffId";
    public static final String OTHERS_DESC = "othersDesc";
    public static final String WORKSPACE_NAME = "workspaceName";
    public static final String LOCATION = "location";
    public static final String GPS = "gps";
    public static final String IS_REMOTE = "isRemote";
    public static final String IS_AVAILABLE = "isAvailable";
    public static final String DATE_CREATED = "dateCreated";
    public static final String DATE_UPDATED = "dateUpdated";
    public static final String SWIPE_RIGHT = "Right";
    public static final String SWIPE_LEFT = "Left";

    //address
    public static final String PLACE_ADDRESS = "placeAddress";
    public static final String PLACE_INDEX = "placeIndex";

    public static final int JOB_RADIUS = 500;//in meter (means 1km in distance)
    public static final float JOB_ZOOM_IN_LEVEL = 16.25f;//in meter (means 1km in distance)

    @Exclude
    public ArrayList<Integer> jobScopeIcon = new ArrayList<>();

    @Exclude
    public ArrayList<Integer> earnListIcon = new ArrayList<>();

    @Exclude
    public ArrayList<String> jobScope = new ArrayList<>();

    @Exclude
    public ArrayList<String> earnList = new ArrayList<>();

    @PropertyName(ID)
    @SerializedName(ID)
    @Expose
    public String id;

    @PropertyName(EMPLOYER_ID)
    @SerializedName(EMPLOYER_ID)
    @Expose
    public String employerId;

    @PropertyName(STAFF_ID)
    @SerializedName(STAFF_ID)
    @Expose
    public String staffId;

    @PropertyName(OTHERS_DESC)
    @SerializedName(OTHERS_DESC)
    @Expose
    public String othersDesc;

    @PropertyName(WORKSPACE_NAME)
    @SerializedName(WORKSPACE_NAME)
    @Expose
    public String workspaceName;

    @PropertyName(LOCATION)
    @SerializedName(LOCATION)
    @Expose
    public String location;

    @PropertyName(PLACE_ADDRESS)
    @SerializedName(PLACE_ADDRESS)
    @Expose
    public PlaceAddress placeAddress;

    @PropertyName(PLACE_INDEX)
    @SerializedName(PLACE_INDEX)
    @Expose
    public HashMap<String,Boolean> placeIndex;

    @PropertyName(GPS)
    @SerializedName(GPS)
    @Expose
    public GeoPoint gps;

    @PropertyName(IS_REMOTE)
    @SerializedName(IS_REMOTE)
    @Expose
    public Boolean isRemote;

    @PropertyName(IS_AVAILABLE)
    @SerializedName(IS_AVAILABLE)
    @Expose
    public Boolean isAvailable;

    @PropertyName(JOB_PIC_URL)
    @SerializedName(JOB_PIC_URL)
    @Expose
    public String jobPicUrl;

    @ServerTimestamp
    @PropertyName(DATE_CREATED)
    @SerializedName(DATE_CREATED)
    @Expose
    public Timestamp dateCreated;

    @ServerTimestamp
    @PropertyName(DATE_UPDATED)
    @SerializedName(DATE_UPDATED)
    @Expose
    public Timestamp dateUpdated;

    public Job(){

    }

    public Job(AppCompatActivity parentActivity, QueryDocumentSnapshot snapshot){
        this.jobScope = GeneralFunction.copyListIfMatchValueQuerySnapshot(parentActivity,snapshot,R.array.jobscope_list);
        this.earnList = GeneralFunction.copyListIfMatchValueQuerySnapshot(parentActivity,snapshot,R.array.job_earning_list);

        this.employerId = snapshot.getString(Job.EMPLOYER_ID);
        this.gps = snapshot.getGeoPoint(Job.GPS);
        this.isAvailable = snapshot.getBoolean(Job.IS_AVAILABLE);
        this.isRemote = snapshot.getBoolean(Job.IS_REMOTE);
        this.location = snapshot.getString(Job.LOCATION);
        this.othersDesc = snapshot.getString(Job.OTHERS_DESC);
        this.workspaceName = snapshot.getString(Job.WORKSPACE_NAME);
        try{
            this.placeAddress = PlaceAddress.getMapToClass((HashMap<String, Object>) snapshot.get(Job.PLACE_ADDRESS));
        }catch (Exception e){
            e.printStackTrace();
        }
        try{
            this.placeIndex = (HashMap<String, Boolean>) snapshot.get(Job.PLACE_INDEX);
        }catch (Exception e){
            e.printStackTrace();
        }
        this.jobPicUrl = snapshot.getString(Job.JOB_PIC_URL);
        this.dateCreated = snapshot.getTimestamp(Job.DATE_CREATED);
        this.dateUpdated = snapshot.getTimestamp(Job.DATE_UPDATED);
        this.id = snapshot.getId();
    }

    @Override
    public LatLng getPosition() {
        if(gps == null){
            return new LatLng(0,0);
        }
        return new LatLng(gps.getLatitude(),gps.getLongitude());
    }

    @Override
    public String getTitle() {
        if(jobScope == null){
            return "";
        }
        return GeneralFunction.formatList(jobScope,true);
    }

    @Override
    public String getSnippet() {
        if(workspaceName == null){
            return "";
        }
        return String.valueOf(workspaceName);
    }

    @Override
    public int compareTo(@NonNull Job job) {
        if (dateUpdated == null || job.dateUpdated == null)
            return 0;
        return job.dateUpdated.compareTo(dateUpdated);//descending order, newest is at top
    }
}