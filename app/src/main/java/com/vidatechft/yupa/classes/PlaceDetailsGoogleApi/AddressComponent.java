package com.vidatechft.yupa.classes.PlaceDetailsGoogleApi;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AddressComponent {
    @SerializedName("long_name")
    public String longName;

    @SerializedName("short_name")
    public String shortName;

    @SerializedName("types")
    public ArrayList<String> types;

    public AddressComponent(String longName, String shortName, ArrayList<String> types) {
        this.longName = longName;
        this.shortName = shortName;
        this.types = types;
    }
}
