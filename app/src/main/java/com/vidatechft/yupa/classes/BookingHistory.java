package com.vidatechft.yupa.classes;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import com.google.firebase.Timestamp;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.PropertyName;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.ServerTimestamp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@IgnoreExtraProperties
public class BookingHistory implements Comparable<BookingHistory>{
    public static final String URL_BOOKING_HISTORY = "bookingHistory";
    public static final String URL_GUEST_LICENSE = "guestLicense";
    public static final String URL_PAYMENT_RECEIPT = "paymentReceipt";

    public static final String BOOK_TYPE_HOMESTAY = "hs";
    public static final String BOOK_TYPE_HOTEL = "hotel";

    public static final String ACTION_TYPE_CANCELED = "cancel";
    public static final String ACTION_TYPE_REJECTED = "reject";
    public static final String ACTION_TYPE_PENDING = "pending";
    public static final String ACTION_TYPE_ACCEPTED = "accept";
    public static final String ACTION_TYPE_PAID = "paid";

    public static final String ID = "id";
    public static final String USER_ID = "userId";
    public static final String HOST_ID = "hostId";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String EMAIL = "email";
    public static final String COUNTRY = "country";
    public static final String COUNTRY_CODE = "countryCode";
    public static final String CONTACT_NO = "contactNo";
    public static final String TOTAL_GUEST = "totalGuest";
    public static final String PURPOSE = "purpose";
    public static final String LICENSE_URL = "licenseUrl";
    public static final String TYPE = "type";
    public static final String ACTION_TYPE = "actionType";
    public static final String REMARK = "remark";
    public static final String TARGET_ID = "targetId";
    public static final String IS_READ_BY_GUEST = "isReadByGuest";
    public static final String IS_PAID = "isPaid";
    public static final String PAYMENT_ID = "paymentId";
    public static final String PAYMENT_RECEIPT_URL = "paymentReceiptUrl";
    public static final String CHECKIN_DATE = "checkinDate";
    public static final String CHECKOUT_DATE = "checkoutDate";
    public static final String DATE_ACTION = "dateAction";
    public static final String DATE_CREATED = "dateCreated";
    public static final String DATE_UPDATED = "dateUpdated";

    @PropertyName(ID)
    @SerializedName(ID)
    @Expose
    public String id;

    @Exclude
    public Room room;

    @Exclude
    public User user;

    @Exclude
    public boolean isFromBookingHistoryCollection;

    @PropertyName(USER_ID)
    @SerializedName(USER_ID)
    @Expose
    public String userId;

    @PropertyName(HOST_ID)
    @SerializedName(HOST_ID)
    @Expose
    public String hostId;

    @PropertyName(FIRST_NAME)
    @SerializedName(FIRST_NAME)
    @Expose
    public String firstName;

    @PropertyName(LAST_NAME)
    @SerializedName(LAST_NAME)
    @Expose
    public String lastName;

    @PropertyName(EMAIL)
    @SerializedName(EMAIL)
    @Expose
    public String email;

    @PropertyName(COUNTRY)
    @SerializedName(COUNTRY)
    @Expose
    public String country;

    @PropertyName(COUNTRY_CODE)
    @SerializedName(COUNTRY_CODE)
    @Expose
    public String countryCode;

    @PropertyName(CONTACT_NO)
    @SerializedName(CONTACT_NO)
    @Expose
    public String contactNo;

    @PropertyName(TOTAL_GUEST)
    @SerializedName(TOTAL_GUEST)
    @Expose
    public int totalGuest;

    @PropertyName(PURPOSE)
    @SerializedName(PURPOSE)
    @Expose
    public String purpose;

    @PropertyName(LICENSE_URL)
    @SerializedName(LICENSE_URL)
    @Expose
    public String licenseUrl;

    @PropertyName(TYPE)
    @SerializedName(TYPE)
    @Expose
    public String type;

    @PropertyName(ACTION_TYPE)
    @SerializedName(ACTION_TYPE)
    @Expose
    public String actionType;

    @PropertyName(REMARK)
    @SerializedName(REMARK)
    @Expose
    public String remark;

    @PropertyName(TARGET_ID)
    @SerializedName(TARGET_ID)
    @Expose
    public String targetId;

    @PropertyName(IS_READ_BY_GUEST)
    @SerializedName(IS_READ_BY_GUEST)
    @Expose
    public Boolean isReadByGuest;

    @PropertyName(IS_PAID)
    @SerializedName(IS_PAID)
    @Expose
    public Boolean isPaid;

    @PropertyName(PAYMENT_ID)
    @SerializedName(PAYMENT_ID)
    @Expose
    public String paymentId;

    @PropertyName(PAYMENT_RECEIPT_URL)
    @SerializedName(PAYMENT_RECEIPT_URL)
    @Expose
    public String paymentReceiptUrl;

    @PropertyName(CHECKIN_DATE)
    @SerializedName(CHECKIN_DATE)
    @Expose
    public Long checkinDate;

    @PropertyName(CHECKOUT_DATE)
    @SerializedName(CHECKOUT_DATE)
    @Expose
    public Long checkoutDate;

    @ServerTimestamp
    @PropertyName(DATE_ACTION)
    @SerializedName(DATE_ACTION)
    @Expose
    public Timestamp dateAction;

    @ServerTimestamp
    @PropertyName(DATE_CREATED)
    @SerializedName(DATE_CREATED)
    @Expose
    public Timestamp dateCreated;

    @ServerTimestamp
    @PropertyName(DATE_UPDATED)
    @SerializedName(DATE_UPDATED)
    @Expose
    public Timestamp dateUpdated;

    public BookingHistory(){

    }

    public BookingHistory(String firstName, String lastName, String email, String country, String contactNo, int totalGuest, String purpose, String type, Search search) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.country = country;
        this.contactNo = contactNo;
        this.totalGuest = totalGuest;
        this.purpose = purpose;
        this.type = type;
        this.checkinDate = search.checkin_date;
        this.checkoutDate = search.checkout_date;
    }

    //from here https://stackoverflow.com/questions/5927109/sort-objects-in-arraylist-by-date
    //this is descending order (newest)
    @Override
    public int compareTo(@NonNull BookingHistory bookingHistory) {
        if (dateUpdated == null || bookingHistory.dateUpdated == null)
            return 0;
        return bookingHistory.dateUpdated.compareTo(dateUpdated);
    }

    public static BookingHistory snapshotToBookingHistory(AppCompatActivity parentActivity, DocumentSnapshot snapshot) {
        BookingHistory tempBookingHistory = snapshot.toObject(BookingHistory.class);
        if(tempBookingHistory != null){
            tempBookingHistory.id = snapshot.getId();
            return tempBookingHistory;
        }else{
            return null;
        }
    }
}