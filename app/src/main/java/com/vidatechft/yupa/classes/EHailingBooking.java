package com.vidatechft.yupa.classes;

import android.support.v7.app.AppCompatActivity;

import com.google.firebase.Timestamp;
import com.google.firebase.database.PropertyName;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.ServerTimestamp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EHailingBooking {
    public static final String URL_E_HAILING_BOOKING = "eHailingBooking";

    public static final String STATUS_APPROVED = "approved";
    public static final String STATUS_REJECTED = "rejected";
    public static final String STATUS_PENDING = "pending";

    public static final String CAR_STATUS_COMING = "coming";
    public static final String CAR_STATUS_WAITING = "waiting";
    public static final String CAR_STATUS_ARRIVED = "arrived";
    public static final String CAR_STATUS_FETCHING = "fetching";
    public static final String CAR_STATUS_FINISHED = "finished";

    public static final String TYPE = "type";
    public static final String TYPE_RENTAL = "rental";
    public static final String TYPE_E_HAILING = "taxi";

    public static final String BOOKING_HISTORY_ID = "bookingHistoryId";
    public static final String TRANSPORT_ID = "transportId";
    public static final String USER_ID = "userId";
    public static final String CAR_STATUS = "carStatus";
    public static final String CUSTOMER_RATING = "customerRating";
    public static final String CUSTOMER_RATING_DESC = "customerRatingDesc";
    public static final String DRIVER_ID = "driverId";
    public static final String DROPOFF_GPS = "dropoffGps";
    public static final String DROPOFF_NAME = "dropoffName";
    public static final String PICKUP_GPS = "pickupGps";
    public static final String PICKUP_NAME = "pickupName";
    public static final String ADMIN_REMARK = "adminRemark";
    public static final String STATUS = "status";
    public static final String CAR_MAKE = "carMake";
    public static final String CAR_MODEL = "carModel";
    public static final String CAR_COLOUR = "carColour";
    public static final String CAR_PLATE_NUMBER = "carPlateNo";
    public static final String DATE_CREATED = "dateCreated";
    public static final String DATE_UPDATED = "dateUpdated";
    public static final String PICKUP_TIME = "pickupTime";

    @Exclude
    public String id;

    @PropertyName(BOOKING_HISTORY_ID)
    @SerializedName(BOOKING_HISTORY_ID)
    @Expose
    public String bookingHistoryId;

    @PropertyName(TRANSPORT_ID)
    @SerializedName(TRANSPORT_ID)
    @Expose
    public String transportId;

    @PropertyName(USER_ID)
    @SerializedName(USER_ID)
    @Expose
    public String userId;

    @PropertyName(CAR_STATUS)
    @SerializedName(CAR_STATUS)
    @Expose
    public String carStatus;

    @PropertyName(TYPE)
    @SerializedName(TYPE)
    @Expose
    public String type;

    @PropertyName(CUSTOMER_RATING)
    @SerializedName(CUSTOMER_RATING)
    @Expose
    public Long customerRating;

    @PropertyName(CUSTOMER_RATING_DESC)
    @SerializedName(CUSTOMER_RATING_DESC)
    @Expose
    public String customerRatingDesc;

    @PropertyName(DRIVER_ID)
    @SerializedName(DRIVER_ID)
    @Expose
    public String driverId;

    @PropertyName(DROPOFF_GPS)
    @SerializedName(DROPOFF_GPS)
    @Expose
    public GeoPoint dropoffGps;

    @PropertyName(DROPOFF_NAME)
    @SerializedName(DROPOFF_NAME)
    @Expose
    public String dropoffName;

    @PropertyName(PICKUP_GPS)
    @SerializedName(PICKUP_GPS)
    @Expose
    public String pickupGps;

    @PropertyName(PICKUP_NAME)
    @SerializedName(PICKUP_NAME)
    @Expose
    public String pickupName;

    @PropertyName(ADMIN_REMARK)
    @SerializedName(ADMIN_REMARK)
    @Expose
    public String adminRemark;

    @PropertyName(STATUS)
    @SerializedName(STATUS)
    @Expose
    public String status;

    @PropertyName(CAR_MAKE)
    @SerializedName(CAR_MAKE)
    @Expose
    public String carMake;

    @PropertyName(CAR_MODEL)
    @SerializedName(CAR_MODEL)
    @Expose
    public String carModel;

    @PropertyName(CAR_COLOUR)
    @SerializedName(CAR_COLOUR)
    @Expose
    public String carColour;

    @PropertyName(CAR_PLATE_NUMBER)
    @SerializedName(CAR_PLATE_NUMBER)
    @Expose
    public String carPlateNo;

    @ServerTimestamp
    @PropertyName(DATE_CREATED)
    @SerializedName(DATE_CREATED)
    @Expose
    public Timestamp dateCreated; // from here https://stackoverflow.com/questions/46254266/firebase-no-properties-to-serialize-found-on-class

    @ServerTimestamp
    @PropertyName(DATE_UPDATED)
    @SerializedName(DATE_UPDATED)
    @Expose
    public Timestamp dateUpdated;

    @PropertyName(PICKUP_TIME)
    @SerializedName(PICKUP_TIME)
    @Expose
    public Long pickupTime;

    public EHailingBooking(){

    }

    public static EHailingBooking getEHailingBooking(AppCompatActivity parentActivity, DocumentSnapshot snapshot){
        if(snapshot != null && snapshot.exists()){
            EHailingBooking eHailingBooking = snapshot.toObject(EHailingBooking.class);
            if(eHailingBooking != null){
                eHailingBooking.id = snapshot.getId();
            }
            return eHailingBooking;
        }else{
            return null;
        }
    }
}
