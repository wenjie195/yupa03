package com.vidatechft.yupa.classes;

import android.support.v7.app.AppCompatActivity;

import com.google.firebase.Timestamp;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.PropertyName;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.ServerTimestamp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@IgnoreExtraProperties
public class Favourite {

    public static final String URL_FAVOURITE = "favourite";

    public static final String ID = "id";
    public static final String USER_ID = "userId";
    public static final String TARGET_ID = "targetId";
    public static final String TARGET_UID = "targetUid";
    public static final String TYPE = "type";
    public static final String IS_LIKED = "isLiked";
    public static final String DATE_CREATED = "dateCreated";
    public static final String DATE_UPDATED = "dateUpdated";
    public static final String ACCOMMODATION_ID = "accommodationId";

    public static final String FAV_TYPE_ITI = "iti";
    public static final String FAV_TYPE_ACC = "acc";
    public static final String FAV_TYPE_BLOG = "blog";
    public static final String FAV_TYPE_BLOG_DISCUSSION = "blogDiscussion";
    public static final String FAV_TYPE_BLOG_BOOKMARK = "blogBookmark";
    public static final String FAV_TYPE_RATING = "rating";
    public static final String FAV_TYPE_SHOP = "shop";
    public static final String LIKE_COUNT = "likeCount";

    @Exclude
    public String id;

    @Exclude
    public Rating rating;

    @Exclude
    public User user;

    @Exclude
    public Accommodation accommodation;

    @Exclude
    public int likeCount;

    @PropertyName(ACCOMMODATION_ID)
    @SerializedName(ACCOMMODATION_ID)
    @Expose
    public String accommodationId;

    @PropertyName(USER_ID)
    @SerializedName(USER_ID)
    @Expose
    public String userId;

    @PropertyName(TARGET_ID)
    @SerializedName(TARGET_ID)
    @Expose
    public String targetId;

    @PropertyName(TARGET_UID)
    @SerializedName(TARGET_UID)
    @Expose
    public String targetUid;

    @PropertyName(TYPE)
    @SerializedName(TYPE)
    @Expose
    public String type;

    @PropertyName(IS_LIKED)
    @SerializedName(IS_LIKED)
    @Expose
    public Boolean isLiked;

    @ServerTimestamp
    @PropertyName(DATE_CREATED)
    @SerializedName(DATE_CREATED)
    @Expose
    public Timestamp dateCreated;

    @ServerTimestamp
    @PropertyName(DATE_UPDATED)
    @SerializedName(DATE_UPDATED)
    @Expose
    public Timestamp dateUpdated;

    public Favourite(){

    }

    public static HashMap<String,Object> toMap(Favourite favourite){
        HashMap<String,Object> favMap = new HashMap<>();

        favMap.put(USER_ID,favourite.userId);
        favMap.put(TARGET_ID,favourite.targetId);
        favMap.put(TARGET_UID,favourite.targetUid);
        favMap.put(IS_LIKED,favourite.isLiked);
        favMap.put(TYPE,favourite.type);
        favMap.put(DATE_CREATED,favourite.dateCreated);
        favMap.put(DATE_UPDATED,favourite.dateUpdated);

        return favMap;
    }

    public Favourite (AppCompatActivity parentActivity, QueryDocumentSnapshot snapshot){
        this.id = snapshot.getId();
        this.accommodationId = snapshot.getString(ACCOMMODATION_ID);
        this.dateCreated = snapshot.getTimestamp(DATE_CREATED);
        this.dateUpdated = snapshot.getTimestamp(DATE_UPDATED);
        this.isLiked = snapshot.getBoolean(IS_LIKED);
        this.targetId = snapshot.getString(TARGET_ID);
        this.type = snapshot.getString(TYPE);
        this.userId = snapshot.getString(USER_ID);
        this.targetUid = snapshot.getString(TARGET_UID);
    }

    public Favourite (AppCompatActivity parentActivity, QuerySnapshot snapshot){
        for(QueryDocumentSnapshot queryDocumentSnapshot : snapshot){
            this.id = queryDocumentSnapshot.getId();
            this.accommodationId = queryDocumentSnapshot.getString(ACCOMMODATION_ID);
            this.dateCreated = queryDocumentSnapshot.getTimestamp(DATE_CREATED);
            this.dateUpdated = queryDocumentSnapshot.getTimestamp(DATE_UPDATED);
            this.isLiked = queryDocumentSnapshot.getBoolean(IS_LIKED);
            this.targetId = queryDocumentSnapshot.getString(TARGET_ID);
            this.type = queryDocumentSnapshot.getString(TYPE);
            this.userId = queryDocumentSnapshot.getString(USER_ID);
            this.targetUid = queryDocumentSnapshot.getString(TARGET_UID);
        }
    }

    public static Favourite snapshotToFavourite(AppCompatActivity parentActivity, DocumentSnapshot snapshot) {
        Favourite tempFavourite = snapshot.toObject(Favourite.class);
        if(tempFavourite != null){
            tempFavourite.id = snapshot.getId();
            return tempFavourite;
        }else{
            return null;
        }
    }

}