package com.vidatechft.yupa.classes;

import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.PropertyName;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

@IgnoreExtraProperties
public class BusinessHourTime {

    public static final String OPEN_HOUR = "openHour";
    public static final String OPEN_MIN = "openMin";
    public static final String CLOSE_HOUR = "closeHour";
    public static final String CLOSE_MIN = "closeMin";

    @PropertyName(OPEN_HOUR)
    @SerializedName(OPEN_HOUR)
    @Expose
    public int openHour;

    @PropertyName(OPEN_MIN)
    @SerializedName(OPEN_MIN)
    @Expose
    public int openMin;

    @PropertyName(CLOSE_HOUR)
    @SerializedName(CLOSE_HOUR)
    @Expose
    public int closeHour;

    @PropertyName(CLOSE_MIN)
    @SerializedName(CLOSE_MIN)
    @Expose
    public int closeMin;

    public int getOpenHour() {
        return openHour;
    }

    public void setOpenHour(int openHour) {
        this.openHour = openHour;
    }

    public int getOpenMin() {
        return openMin;
    }

    public void setOpenMin(int openMin) {
        this.openMin = openMin;
    }

    public int getCloseHour() {
        return closeHour;
    }

    public void setCloseHour(int closeHour) {
        this.closeHour = closeHour;
    }

    public int getCloseMin() {
        return closeMin;
    }

    public void setCloseMin(int closeMin) {
        this.closeMin = closeMin;
    }

    public void setBusinessHourTime(BusinessHourTime businessHourTime){
        this.openHour = businessHourTime.getOpenHour();
        this.openMin = businessHourTime.getOpenMin();
        this.closeHour = businessHourTime.getCloseHour();
        this.closeMin = businessHourTime.getCloseMin();
    }

    public void getBusinessHourTime(BusinessHourTime businessHourTimes){
        this.openHour = businessHourTimes.getOpenHour();
        this.openMin = businessHourTimes.getOpenMin();
        this.closeHour = businessHourTimes.getCloseHour();
        this.closeMin = businessHourTimes.getCloseMin();
    }

    public BusinessHourTime(HashMap<String,Long> bhMap){
        if(bhMap.get(OPEN_HOUR) != null){
            this.openHour = bhMap.get(OPEN_HOUR).intValue();
        }else{
            this.openHour = 0;
        }

        if(bhMap.get(OPEN_MIN) != null){
            this.openMin = bhMap.get(OPEN_MIN).intValue();
        }else{
            this.openMin = 0;
        }

        if(bhMap.get(CLOSE_HOUR) != null){
            this.closeHour = bhMap.get(CLOSE_HOUR).intValue();
        }else{
            this.closeHour = 0;
        }

        if(bhMap.get(CLOSE_MIN) != null){
            this.closeMin = bhMap.get(CLOSE_MIN).intValue();
        }else{
            this.closeMin = 0;
        }
    }

    public BusinessHourTime() {

    }

}
