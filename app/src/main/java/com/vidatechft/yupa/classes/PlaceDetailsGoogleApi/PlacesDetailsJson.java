package com.vidatechft.yupa.classes.PlaceDetailsGoogleApi;

import com.google.gson.annotations.SerializedName;

public class PlacesDetailsJson {
    @SerializedName("result")
    public PlacesDetailsResult placesDetailsResult;

    public PlacesDetailsJson(PlacesDetailsResult placesDetailsResult) {
        this.placesDetailsResult = placesDetailsResult;
    }
}
