package com.vidatechft.yupa.classes;

import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.Timestamp;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.PropertyName;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.ServerTimestamp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.maps.android.clustering.ClusterItem;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.classes.PlaceDetailsGoogleApi.PlaceAddress;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@IgnoreExtraProperties
public class User implements ClusterItem {

    public static final String URL_USER = "user";
    public static final String URL_USER_PROFILE_PICTURE = "profilePic";
    public static final String URL_USER_TOUR_BUDDY = "tourBuddy";

    public static final String GENDER_MALE = "Male";
    public static final String GENDER_FEMALE = "Female";
    public static final String GENDER_OTHER = "Other";

    public static final int SEARCH_MIN_AGE = 5;

    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String EMAIL = "email";
    public static final String PROFILE_PIC_URL = "profilePicUrl";
    public static final String DOB_DAY = "dobDay";
    public static final String DOB_MONTH = "dobMonth";
    public static final String DOB_YEAR = "dobYear";
    public static final String COUNTRY = "country";
    public static final String COUNTRY_CODE = "countryCode";
    public static final String CONTACT = "contactNo";
    public static final String CAN_CONTACT = "canContact";
    public static final String ACC_TYPE = "accType";
    public static final String GENDER = "gender";
    public static final String STATE = "state";
    public static final String GPS = "gps";

    public static final String DATE_CREATED = "dateCreated";
    public static final String DATE_UPDATED = "dateUpdated";


    public static final String IS_MERCHANT = "isMerchant";
    public static final String IS_HOTELIER = "isHotelier";
    public static final String IS_EMPLOYER = "isEmployer";
    public static final String IS_HOST = "isHost";
    public static final String IS_ADMIN = "isAdmin";
    public static final String IS_TOUR_BUDDY = "isTourBuddy";
    public static final String IS_JOB_SEEKER= "isJobSeeker";

    public static final String IS_ONLINE = "isOnline";
    public static final String LAST_ONLINE = "lastOnline";

    public static final String JOB_SEEKER = "jobSeeker";

    public static final int JOB_RADIUS = 500;//in meter (means 1km in distance)
    public static final float JOB_ZOOM_IN_LEVEL = 16.25f;//in meter (means 1km in distance)

    @Exclude
    public ArrayList<String> jobScope = new ArrayList<>();

    @Exclude
    public ArrayList<String> hobbyScope = new ArrayList<>();

    @Exclude
    public String id;

    /////////////////////////////////////////////////////////////////SEARCHING PURPOSE OR JUST TO PASS DATA/////////////////////////////////////////////////////////////////

    @Exclude
    public String placeID;//for searching purposes only (Inside SearchNearbyFragment)

    @Exclude
    public PlaceAddress placeAddress;//for searching purposes only (Inside SearchNearbyFragment)

    @Exclude
    public Map<String,Object> dateSearchRange = new HashMap<>();//for searching purposes only (Inside SearchNearbyFragment)

    @Exclude
    public Long departDate,returnDate;//for searching purposes only (Inside SearchNearbyFragment)

    @Exclude
    public int ageRangeMin,ageRangeMax;//for searching purposes only

    @Exclude
    public String searchNameOrContactOrEmail;//for searching purposes only

    ///////////////////////////////////////////////////////////////// END SEARCHING PURPOSE OR JUST TO PASS DATA/////////////////////////////////////////////////////////////////

    @PropertyName(NAME)
    @SerializedName(NAME)
    @Expose
    public String name;

    @PropertyName(URL_USER_TOUR_BUDDY)
    @SerializedName(URL_USER_TOUR_BUDDY)
    @Expose
    public Map<String,Object> tourBuddy = new HashMap<>();

    @PropertyName(JOB_SEEKER)
    @SerializedName(JOB_SEEKER)
    @Expose
    public Map<String,Object> jobSeekerMap = new HashMap<>();

    @PropertyName(EMAIL)
    @SerializedName(EMAIL)
    @Expose
    public String email;

    @PropertyName(GENDER)
    @SerializedName(GENDER)
    @Expose
    public String gender;

    @PropertyName(PROFILE_PIC_URL)
    @SerializedName(PROFILE_PIC_URL)
    @Expose
    public String profilePicUrl;

    @PropertyName(DOB_DAY)
    @SerializedName(DOB_DAY)
    @Expose
    public int dobDay;

    @PropertyName(DOB_MONTH)
    @SerializedName(DOB_MONTH)
    @Expose
    public int dobMonth;

    @PropertyName(DOB_YEAR)
    @SerializedName(DOB_YEAR)
    @Expose
    public int dobYear;

    @PropertyName(COUNTRY)
    @SerializedName(COUNTRY)
    @Expose
    public String country;

    @PropertyName(COUNTRY_CODE)
    @SerializedName(COUNTRY_CODE)
    @Expose
    public String countryCode;

    @PropertyName(STATE)
    @SerializedName(STATE)
    @Expose
    public String state;

    @PropertyName(GPS)
    @SerializedName(GPS)
    @Expose
    public GeoPoint gps;

    @PropertyName(CONTACT)
    @SerializedName(CONTACT)
    @Expose
    public String contactNo;

    @PropertyName(CAN_CONTACT)
    @SerializedName(CAN_CONTACT)
    @Expose
    public Boolean canContact;

    @PropertyName(ACC_TYPE)
    @SerializedName(ACC_TYPE)
    @Expose
    public String accType;

    @ServerTimestamp
    @PropertyName(DATE_CREATED)
    @SerializedName(DATE_CREATED)
    @Expose
    public Timestamp dateCreated;

    @ServerTimestamp
    @PropertyName(DATE_UPDATED)
    @SerializedName(DATE_UPDATED)
    @Expose
    public Timestamp dateUpdated;

    @PropertyName(IS_HOTELIER)
    @SerializedName(IS_HOTELIER)
    @Expose
    public Boolean isHotelier;

    @PropertyName(IS_MERCHANT)
    @SerializedName(IS_MERCHANT)
    @Expose
    public Boolean isMerchant;

    @PropertyName(IS_HOST)
    @SerializedName(IS_HOST)
    @Expose
    public Boolean isHost;

    @PropertyName(IS_ADMIN)
    @SerializedName(IS_ADMIN)
    @Expose
    public Boolean isAdmin;

    @PropertyName(IS_EMPLOYER)
    @SerializedName(IS_EMPLOYER)
    @Expose
    public Boolean isEmployer;

    @PropertyName(IS_ONLINE)
    @SerializedName(IS_ONLINE)
    @Expose
    public Boolean isOnline;

    @PropertyName(IS_TOUR_BUDDY)
    @SerializedName(IS_TOUR_BUDDY)
    @Expose
    public Boolean isTourBuddy;

    @PropertyName(IS_JOB_SEEKER)
    @SerializedName(IS_JOB_SEEKER)
    @Expose
    public Boolean isJobSeeker;

    @ServerTimestamp
    @PropertyName(LAST_ONLINE)
    @SerializedName(LAST_ONLINE)
    @Expose
    public Timestamp lastOnline;

    public User(){

    }

    public User(String name, String email,String gender, int dobDay, int dobMonth, int dobYear, String country,String countryCode, String state, String contactNo, Boolean canContact, String accType){
        this.name = name;
        this.email = email;
        this.gender = gender;
        this.dobDay = dobDay;
        this.dobMonth = dobMonth;
        this.dobYear = dobYear;
        this.country = country;
        this.countryCode = countryCode;
        this.state = state;
        this.contactNo = contactNo;
        this.canContact = canContact;
        this.accType = accType;
    }

    public User(AppCompatActivity parentActivity, DocumentSnapshot snapshot){
        this.jobScope = GeneralFunction.copyListIfMatchValueQuerySnapshot(parentActivity,snapshot, R.array.jobscope_list);
        this.hobbyScope = GeneralFunction.copyListIfMatchValueQuerySnapshot(parentActivity,snapshot, R.array.editProfileHobby);

        this.id = snapshot.getId();
        this.name = snapshot.getString(NAME);
        this.email = snapshot.getString(EMAIL);
        this.gender = snapshot.getString(GENDER);
        this.profilePicUrl = snapshot.getString(PROFILE_PIC_URL);

        try {
            this.dobDay = snapshot.getLong(DOB_DAY).intValue();
        }catch (Exception e){
            this.dobDay = 0;
        }

        try {
            this.dobMonth = snapshot.getLong(DOB_MONTH).intValue();
        }catch (Exception e){
            this.dobMonth = 0;
        }

        try {
            this.dobYear = snapshot.getLong(DOB_YEAR).intValue();
        }catch (Exception e){
            this.dobYear = 0;
        }

        this.country = snapshot.getString(COUNTRY);
        this.countryCode = snapshot.getString(COUNTRY_CODE);
        this.state = snapshot.getString(STATE);
        this.gps = snapshot.getGeoPoint(GPS);
        this.contactNo = snapshot.getString(CONTACT);
        this.canContact = snapshot.getBoolean(CAN_CONTACT);
        this.accType = snapshot.getString(ACC_TYPE);
        this.dateCreated = snapshot.getTimestamp(DATE_CREATED);
        this.dateUpdated = snapshot.getTimestamp(DATE_UPDATED);
        this.isMerchant = snapshot.getBoolean(IS_MERCHANT);
        this.isHotelier = snapshot.getBoolean(IS_HOTELIER);
        this.isHost = snapshot.getBoolean(IS_HOST);
        this.isEmployer = snapshot.getBoolean(IS_EMPLOYER);
        this.isAdmin = snapshot.getBoolean(IS_ADMIN);
        this.isTourBuddy = snapshot.getBoolean(IS_TOUR_BUDDY);
        this.isOnline = snapshot.getBoolean(IS_ONLINE);
        this.lastOnline = snapshot.getTimestamp(LAST_ONLINE);

        if(snapshot.contains(IS_TOUR_BUDDY))
        {
            this.isTourBuddy = snapshot.getBoolean(IS_TOUR_BUDDY);
        }

        if(snapshot.contains(URL_USER_TOUR_BUDDY))
        {
            this.tourBuddy = (Map<String, Object>) snapshot.get(URL_USER_TOUR_BUDDY);
        }
        this.isJobSeeker = snapshot.getBoolean(IS_JOB_SEEKER);
        this.jobSeekerMap = (Map<String, Object>) snapshot.get(JOB_SEEKER);
    }

    @Override
    public LatLng getPosition() {
        if(gps == null){
            return new LatLng(0,0);
        }
        return new LatLng(gps.getLatitude(),gps.getLongitude());
    }

    @Override
    public String getTitle() {
        if(name == null){
            return "";
        }
        return name;
    }

    @Override
    public String getSnippet() {
        if(country != null){
            if(state != null && !state.isEmpty()){
                return state + ", " + country;
            }else{
                return country;
            }
        }else{
            return "";
        }
    }

}
