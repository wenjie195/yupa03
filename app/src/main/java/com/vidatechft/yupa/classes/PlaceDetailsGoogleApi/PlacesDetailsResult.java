package com.vidatechft.yupa.classes.PlaceDetailsGoogleApi;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class PlacesDetailsResult {
    @SerializedName("address_components")
    public ArrayList<AddressComponent> addressComponents;

    @SerializedName("name")
    public String placeName;

    @SerializedName("geometry")
    public Geometry geometry;

    public PlacesDetailsResult(ArrayList<AddressComponent> addressComponents, String placeName, Geometry geometry) {
        this.addressComponents = addressComponents;
        this.placeName = placeName;
        this.geometry = geometry;
    }
}
