package com.vidatechft.yupa.classes;

import com.google.firebase.Timestamp;
import com.google.firebase.database.PropertyName;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.ServerTimestamp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.utilities.MillisecondsToDate;

import java.util.HashMap;
import java.util.Map;

//todo when searching job seeker available date, need to query availability equals to "everyday,custom,etc..." also because if user changes from custom to others, the customDateIndex is still exists
//todo same goes for tour buddy
public class JobSeeker
{
    public static final String USER_ID = "userId";
    public static final String GET_ALL_AVAILABLE_DATES = "customDate";
    public static final String GET_ALL_AVAILABLE_DATES_INDEX = "customDateIndex";
    public static final String DATE_CREATED = "dateCreated";
    public static final String DATE_UPDATED = "dateUpdated";
    public static final String AVAILABILITY = "availability";
    public static final String STATUS = "status";

    public static final String STATUS_ALL_WEEKEND = "allWeekend";
    public static final String STATUS_ALL_WEEKDAY = "allWeekday";
    public static final String STATUS_CUSTOM = "custom";
    public static final String STATUS_EVERYDAY = "everyday";
    public static final String STATUS_PENDING = "pending";


    @PropertyName(STATUS)
    @SerializedName(STATUS)
    @Expose
    public String status;

    @PropertyName(AVAILABILITY)
    @SerializedName(AVAILABILITY)
    @Expose
    public String availability;

    @PropertyName(USER_ID)
    @SerializedName(USER_ID)
    @Expose
    public String userId;

    @ServerTimestamp
    @PropertyName(DATE_CREATED)
    @SerializedName(DATE_CREATED)
    @Expose
    public Timestamp dateCreated;

    @ServerTimestamp
    @PropertyName(DATE_UPDATED)
    @SerializedName(DATE_UPDATED)
    @Expose
    public Timestamp dateUpdated;

    @PropertyName(GET_ALL_AVAILABLE_DATES)
    @SerializedName(GET_ALL_AVAILABLE_DATES)
    @Expose
    public Map<MillisecondsToDate,Boolean> customDate = new HashMap<>();

    public JobSeeker()
    {

    }
    public Map<String, Object> setMap(MainActivity parentActivity, Map<String, Object> setJobSeekerCalendarDate, Map<String, Object> setDates, Map<String, Object> setDatesBoolean,int availableDateType , boolean isCustom)
    {
        setJobSeekerCalendarDate.put(JobSeeker.USER_ID,parentActivity.uid);
        if(setJobSeekerCalendarDate.get(JobSeeker.DATE_CREATED) == null){
            setJobSeekerCalendarDate.put(JobSeeker.DATE_CREATED,FieldValue.serverTimestamp());
        }
        setJobSeekerCalendarDate.put(JobSeeker.DATE_UPDATED,FieldValue.serverTimestamp());

//        if(availableDateType == 1)
//        {
//            setJobSeekerCalendarDate.put(JobSeeker.AVAILABILITY,STATUS_ALL_WEEKEND);
//        }
//        else if(availableDateType == 2)
//        {
//            setJobSeekerCalendarDate.put(JobSeeker.AVAILABILITY,STATUS_ALL_WEEKDAY);
//        }
//        else if(availableDateType == 3)
//        {
//            setJobSeekerCalendarDate.put(JobSeeker.AVAILABILITY, STATUS_CUSTOM);
//        }
//        else
//        {
//            setJobSeekerCalendarDate.put(JobSeeker.AVAILABILITY,STATUS_EVERYDAY);
//        }

        if(isCustom)
        {
            if(setJobSeekerCalendarDate.size() != 0)
            {
//                setJobSeekerCalendarDate.put(JobSeeker.AVAILABILITY,setJobSeekerCalendarDate);
                setJobSeekerCalendarDate.put(JobSeeker.GET_ALL_AVAILABLE_DATES,setDates);
                setJobSeekerCalendarDate.put(JobSeeker.GET_ALL_AVAILABLE_DATES_INDEX,setDatesBoolean);
            }
        }

        setJobSeekerCalendarDate.put(JobSeeker.STATUS,JobSeeker.STATUS_PENDING);

        return setJobSeekerCalendarDate;
    }
}
