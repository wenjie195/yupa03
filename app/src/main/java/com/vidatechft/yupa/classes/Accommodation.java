package com.vidatechft.yupa.classes;

import android.support.v7.app.AppCompatActivity;

import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.PropertyName;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Exclude;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@IgnoreExtraProperties
public class Accommodation implements Serializable {
    public static final String DIR_ACCOMMODATION = "accommodation";
    public static final String ID = "id";
    public static final String UPLOAD_ACCOMMODATION_DETAILS = "uploadAccommodationDetails";
    public static final String UPLOAD_ACCOMMODATION_DETAILS_ALL = "uploadAccommodationDetailsAll";
    //accommodation
    public static final String DIR_ACCOMMODATION_1 = "dirAccommodation1";
    public static final String DIR_ACCOMMODATION_2 = "dirAccommodation2";
    public static final String DIR_ACCOMMODATION_3 = "dirAccommodation3";
    public static final String DIR_ACCOMMODATION_4 = "dirAccommodation4";
    public static final String DIR_ACCOMMODATION_5 = "dirAccommodation5";




    //accommodation pic details
    public static final String ACCOMMODATION_DETAILS_1 = "accommodationDetails1";
    public static final String ACCOMMODATION_DETAILS_2 = "accommodationDetails2";
    public static final String ACCOMMODATION_DETAILS_3 = "accommodationDetails3";
    public static final String ACCOMMODATION_DETAILS_4 = "accommodationDetails4";
    public static final String ACCOMMODATION_DETAILS_5 = "accommodationDetails5";

    public static final String URL_ACCOMMODATION = "accommodation";
    public static final String URL_ACCOMMODATION_PIC = "urlAccommodationPic";
    public static final String URL_UPLOADED_ACCOMMODATION = "urlUploadedAccommodation";
    public static final String ACCOMMODATION_PIC = "accommodationPic";
    public static final String ACCOMMODATION_POSITION = "position";


    //img push id
    public static final String IMG_PUSH_ID = "imgPushId";

    //Accommodation name
    public static final String ACCOMMODATION_NAME = "accommodationName";

    //Accommodation photo
    public static final String IMG_ACCOMMODATION= "imgAccommodation";

    @Exclude
    public String id;

    @Exclude
    public Rating rating;

    @PropertyName(ACCOMMODATION_NAME)
    @SerializedName(ACCOMMODATION_NAME)
    @Expose
    public String accommodationName;

    @PropertyName(IMG_ACCOMMODATION)
    @SerializedName(IMG_ACCOMMODATION)
    @Expose
    public String imgAccommodation;

    @PropertyName(IMG_PUSH_ID)
    @SerializedName(IMG_PUSH_ID)
    @Expose
    public String imgPushId;

    @PropertyName(URL_ACCOMMODATION_PIC)
    @SerializedName(URL_ACCOMMODATION_PIC)
    @Expose
    public String urlAccommodationPic;


//    public Accommodation(String accommodationName1, String accommodationName2, String accommodationName3, String accommodationName4, String accommodationName5, String imgAccommodation1, String imgAccommodation2, String imgAccommodation3, String imgAccommodation4, String imgAccommodation5) {
//        this.accommodationName1 = accommodationName1;
//        this.accommodationName2 = accommodationName2;
//        this.accommodationName3 = accommodationName3;
//        this.accommodationName4 = accommodationName4;
//        this.accommodationName5 = accommodationName5;
//        this.imgAccommodation1 = imgAccommodation1;
//        this.imgAccommodation2 = imgAccommodation2;
//        this.imgAccommodation3 = imgAccommodation3;
//        this.imgAccommodation4 = imgAccommodation4;
//        this.imgAccommodation5 = imgAccommodation5;
//    }

    public Accommodation(){

    }

    public Accommodation(String accommodationName, String imgAccommodation, String imgPushId) {
        this.accommodationName = accommodationName;
        this.imgAccommodation = imgAccommodation;
        this.imgPushId = imgPushId;
    }

    public Accommodation (AppCompatActivity parentActivity, DocumentSnapshot snapshot){
    }

    public static Accommodation snapshotToAccommodation(AppCompatActivity parentActivity, DocumentSnapshot snapshot) {
        Accommodation tempAccommodation = snapshot.toObject(Accommodation.class);
        if(tempAccommodation != null){
            tempAccommodation.id = snapshot.getId();
            return tempAccommodation;
        }else{
            return null;
        }
    }
}
