package com.vidatechft.yupa.classes;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.Timestamp;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.PropertyName;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.ServerTimestamp;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vidatechft.yupa.utilities.Config;
import com.vidatechft.yupa.utilities.GeneralFunction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.Nullable;

//the currency is being updated hourly by using CRON jobs in cloud functions
//currently using fixer.io with 1000 free API calls per month. That is why i can only call it hourly to maintain the free tier
//24 * 31 = 744 API calls per month
public class CurrencyRate {
    public static final String TAG = CurrencyRate.class.getName();
    public static final String URL_CURRENCY_RATE = "currencyRates";
    public static final String SP_TYPE_CURRENCY_RATE = "currencyRates";
    public static final String SP_TYPE_PREFERRED_CURRENCY_CODE = "preferredCurrencyCode";

    public static final String BASE = "base";
    public static final String CODE = "code";
    public static final String VALUE = "value"; //the rate value...
    public static final String DATE_CREATED = "dateCreated";

    @Exclude
    public boolean isSelected = false;

    @PropertyName(BASE)
    @SerializedName(BASE)
    @Expose
    public String base;

    @PropertyName(CODE)
    @SerializedName(CODE)
    @Expose
    public String code;

    @PropertyName(VALUE)
    @SerializedName(VALUE)
    @Expose
    public Double value;

    @ServerTimestamp
    @PropertyName(DATE_CREATED)
    @SerializedName(DATE_CREATED)
    @Expose
    public Timestamp dateCreated;

    public CurrencyRate(){

    }

    public static ListenerRegistration syncCurrencyFromDB(final AppCompatActivity parentActivity){
        return GeneralFunction.getFirestoreInstance()
                .collection(URL_CURRENCY_RATE)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
                        if(e != null){
                            Log.e(TAG,e.toString());
                            return;
                        }

                        if(snapshots != null && !snapshots.isEmpty()){
                            for(DocumentSnapshot snapshot : snapshots){
                                if(snapshot != null && snapshot.exists()){
                                    CurrencyRate currencyRate = snapshot.toObject(CurrencyRate.class);
                                    if(currencyRate != null){
                                        saveRateToSP(parentActivity,currencyRate);
                                    }
                                }
                            }
                        }
                    }
                });
    }

    public static void saveRateToSP(AppCompatActivity parentActivity, CurrencyRate currencyRate){
        final Gson gson = new Gson();
        SharedPreferences sharedPreferences = parentActivity.getSharedPreferences(SP_TYPE_CURRENCY_RATE, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();

        String json = gson.toJson(currencyRate);
        prefsEditor.putString(currencyRate.code, json);

        prefsEditor.apply();
    }

    public static ArrayList<CurrencyRate> getAllRatesFromSP(AppCompatActivity parentActivity){
        ArrayList<CurrencyRate> rates = new ArrayList<>();

        final Gson gson = new Gson();
        SharedPreferences sharedPreferences = parentActivity.getSharedPreferences(SP_TYPE_CURRENCY_RATE, Context.MODE_PRIVATE);

        TreeMap<String, ?> allRatesFromSP = new TreeMap<String, Object>(sharedPreferences.getAll());

        for (Map.Entry<String, ?> entry : allRatesFromSP.entrySet()) {
            CurrencyRate rate = gson.fromJson((String) entry.getValue(), CurrencyRate.class);

            rates.add(rate);
        }
        return rates;
    }

    public static CurrencyRate getRateFromSP(AppCompatActivity parentActivity, String code){
        CurrencyRate rate = null;
        final Gson gson = new Gson();

        SharedPreferences sharedPreferences = parentActivity.getSharedPreferences(SP_TYPE_CURRENCY_RATE, Context.MODE_PRIVATE);

        String rateFromSP = sharedPreferences.getString(code,null);

        if(rateFromSP != null){
            rate = gson.fromJson(rateFromSP, CurrencyRate.class);
        }

        return rate;
    }

    public static HashMap<String,String> getCurrencyName(AppCompatActivity parentActivity){
        HashMap<String,String> currencyMap = new HashMap<>();
        try {
            JSONObject obj = new JSONObject(GeneralFunction.loadJSONFromAsset(Config.CURRENCY_JSON_FILE, parentActivity));
            JSONArray array = obj.names();

            for(int i = 0; i < array.length(); i++){
                Object tempObj = array.get(i);
                String key = tempObj.toString();
                String value = obj.getString(key);
                currencyMap.put(key,value);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

        if(currencyMap.isEmpty()){
            return null;
        }else{
            return currencyMap;
        }
    }

    public static Double getUserLocalPrice(AppCompatActivity parentActivity, String localCurrencyCode, String foreignCurrencyCode, Double originalPrice){

        if(localCurrencyCode == null || foreignCurrencyCode == null || originalPrice == null){
            return 0.00;
        }

        if(TextUtils.equals(localCurrencyCode,foreignCurrencyCode)){
            return originalPrice;
        }

        CurrencyRate foreignRate = getRateFromSP(parentActivity,foreignCurrencyCode);
        CurrencyRate userLocalRate = getRateFromSP(parentActivity,localCurrencyCode);
        if(foreignRate != null && userLocalRate != null && foreignRate.value != null && userLocalRate.value != null){
            return (userLocalRate.value / foreignRate.value) * originalPrice; //convert other country currency into our own currency and calculate the price
        }else{
            return originalPrice;
        }

    }

    public static void savePreferredCurrencyCodeToSP(AppCompatActivity parentActivity, String currencyCode){
        final Gson gson = new Gson();
        SharedPreferences sharedPreferences = parentActivity.getSharedPreferences(SP_TYPE_PREFERRED_CURRENCY_CODE, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();

        String json = gson.toJson(currencyCode);
        prefsEditor.putString(SP_TYPE_PREFERRED_CURRENCY_CODE, currencyCode);

        prefsEditor.apply();
    }

    public static String getPreferredCurrencyCodeFromSP(AppCompatActivity parentActivity){
        String currencyCode = null;
        final Gson gson = new Gson();

        SharedPreferences sharedPreferences = parentActivity.getSharedPreferences(SP_TYPE_PREFERRED_CURRENCY_CODE, Context.MODE_PRIVATE);

        String codeFromSP = sharedPreferences.getString(SP_TYPE_PREFERRED_CURRENCY_CODE,null);

        if(codeFromSP != null){
            currencyCode = gson.fromJson(codeFromSP, String.class);
        }

        return currencyCode;
    }
}
