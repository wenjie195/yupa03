package com.vidatechft.yupa.classes;

import android.support.v7.app.AppCompatActivity;

import com.google.firebase.Timestamp;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.PropertyName;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.ServerTimestamp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@IgnoreExtraProperties
public class Host {

    public static final String URL_HOST = "host";
    public static final String URL_HOST_DETAILS = "hostDetails";
    public static final String URL_HOST_PROFILE_PIC = "hostProfilePic";
    public static final String URL_HOST_DRIVER_LICENSE_PIC = "licensePic";

    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String UID = "uid";
    public static final String NRIC = "nric";
    public static final String ADDRESS = "address";
    public static final String EMAIL = "email";
    public static final String CONTACT_NO = "contactNo";
    public static final String DRIVER_LICENSE_PIC_URL = "hostLicensePicUrl";
    public static final String IS_APPROVED = "isApproved";
    public static final String DATE_CREATED = "dateCreated";
    public static final String GEOPOINT = "geopoint";

    public static final String DONT_HAVE_ANY_MAP_APPLICATION = "You don't have any Map application!";

    @Exclude
    public Boolean isFavourite;

    @Exclude
    public Boolean isLiked;

    @Exclude
    public String targetId;

    @PropertyName(ID)
    @SerializedName(ID)
    @Expose
    @Exclude
    public String id;

    @PropertyName(NAME)
    @SerializedName(NAME)
    @Expose
    public String name;

    @PropertyName(NRIC)
    @SerializedName(NRIC)
    @Expose
    public String nric;

    @PropertyName(ADDRESS)
    @SerializedName(ADDRESS)
    @Expose
    public String address;

    @PropertyName(CONTACT_NO)
    @SerializedName(CONTACT_NO)
    @Expose
    public String contactNo;

    @PropertyName(EMAIL)
    @SerializedName(EMAIL)
    @Expose
    public String email;

    @PropertyName(UID)
    @SerializedName(UID)
    @Expose
    public String uid;

    @PropertyName(DRIVER_LICENSE_PIC_URL)
    @SerializedName(DRIVER_LICENSE_PIC_URL)
    @Expose
    public String hostLicensePicUrl;

    @PropertyName(IS_APPROVED)
    @SerializedName(IS_APPROVED)
    @Expose
    public boolean isApproved;

    @ServerTimestamp
    @PropertyName(DATE_CREATED)
    @SerializedName(DATE_CREATED)
    @Expose
    public Timestamp dateCreated; // from here https://stackoverflow.com/questions/46254266/firebase-no-properties-to-serialize-found-on-class

    @PropertyName(GEOPOINT)
    @SerializedName(GEOPOINT)
    @Expose
    public GeoPoint geoPoint;

    public Host(){

    }

    public Host(String name, String nric, String address, String contactNo, String email, GeoPoint geoPoint, String uid) {
        this.name = name;
        this.nric = nric;
        this.address = address;
        this.contactNo = contactNo;
        this.email = email;
        this.geoPoint = geoPoint;
        this.uid = uid;
    }

    public Host (AppCompatActivity parentActivity, QueryDocumentSnapshot snapshot){

    }
}