package com.vidatechft.yupa.classes;

import com.google.firebase.Timestamp;
import com.google.firebase.database.PropertyName;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.ServerTimestamp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vidatechft.yupa.activities.MainActivity;

import java.util.ArrayList;
import java.util.List;

public class Blog {
    public static final String URL_BLOG = "blog";

    public static final String USER_ID = "userId";
    public static final String TITLE = "title";
    public static final String CATEGORY = "category";
    public static final String COVER_PHOTO = "coverPhoto";
    public static final String CONTENT = "content";
    public static final String IS_DRAFT = "isDraft";
    public static final String IS_PUBLISH = "isPublish";
    public static final String IS_FAVOURITE = "isFavourite";
    public static final String LIKE_COUNT = "likeCount";
    public static final String BOOKMARK_COUNT = "bookmarkCount";
    public static final String DATE_CREATED = "dateCreated";
    public static final String DATE_UPDATED = "dateUpdated";

    @Exclude
    public String id;

    @PropertyName(USER_ID)
    @SerializedName(USER_ID)
    @Expose
    public String userId;

    @PropertyName(TITLE)
    @SerializedName(TITLE)
    @Expose
    public String title;

    @PropertyName(CATEGORY)
    @SerializedName(CATEGORY)
    @Expose
    public String category;

    @PropertyName(IS_FAVOURITE)
    @SerializedName(IS_FAVOURITE)
    @Expose
    public Boolean isFavourite;

    @PropertyName(COVER_PHOTO)
    @SerializedName(COVER_PHOTO)
    @Expose
    public String coverPhoto;

    @PropertyName(LIKE_COUNT)
    @SerializedName(LIKE_COUNT)
    @Expose
    public Integer likeCount;

    @PropertyName(BOOKMARK_COUNT)
    @SerializedName(BOOKMARK_COUNT)
    @Expose
    public Integer bookmarkCount;

    @PropertyName(CONTENT)
    @SerializedName(CONTENT)
    @Expose
    public String content;

    @PropertyName(IS_DRAFT)
    @SerializedName(IS_DRAFT)
    @Expose
    public Boolean isDraft;

    @PropertyName(IS_PUBLISH)
    @SerializedName(IS_PUBLISH)
    @Expose
    public Boolean isPublish;

    @ServerTimestamp
    @PropertyName(DATE_CREATED)
    @SerializedName(DATE_CREATED)
    @Expose
    public Timestamp dateCreated; // from here https://stackoverflow.com/questions/46254266/firebase-no-properties-to-serialize-found-on-class

    @ServerTimestamp
    @PropertyName(DATE_UPDATED)
    @SerializedName(DATE_UPDATED)
    @Expose
    public Timestamp dateUpdated;

    public Blog(){}

    public Blog(String userId) {
        this.userId = userId;
    }

    public static Blog snapshotToBlog(MainActivity parentActivity, DocumentSnapshot snapshot) {
        Blog tempBlog = snapshot.toObject(Blog.class);
        if(tempBlog != null){
            tempBlog.id = snapshot.getId();
            return tempBlog;
        }else{
            return null;
        }
    }

    @SerializedName("blogs")
    @Expose
    public List<Blog> blogs = new ArrayList<Blog>();

    public List<Blog> getBlogs() {
        return blogs;
    }

    public void setBlogs(List<Blog> blogs) {
        this.blogs = blogs;
    }
}
