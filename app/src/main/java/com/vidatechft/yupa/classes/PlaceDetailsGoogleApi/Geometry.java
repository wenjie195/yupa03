package com.vidatechft.yupa.classes.PlaceDetailsGoogleApi;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Geometry {
    @SerializedName("location")
    public GeometryGps location;

    public Geometry(GeometryGps location) {
        this.location = location;
    }
}
