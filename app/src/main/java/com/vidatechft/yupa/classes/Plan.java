package com.vidatechft.yupa.classes;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.firebase.Timestamp;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.PropertyName;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.ServerTimestamp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vidatechft.yupa.classes.PlaceDetailsGoogleApi.PlaceAddress;

import java.util.HashMap;

@IgnoreExtraProperties
public class Plan {

    public static final String URL_PLAN = "plan";

    public static final String ID = "id";
    public static final String ACTIVITY_NAME = "activityName";
    public static final String GPS = "gps";
    public static final String PLACE_ADDRESS = "placeAddress";
    public static final String PLACE_INDEX = "placeIndex";
    public static final String START_TIME = "startTime";
    public static final String TRANSPORT_MODE = "transportMode";
    public static final String DAY = "day";
    public static final String REMINDER_DATE = "reminderDate";
    public static final String IS_NEED_TAXI = "isNeedTaxi";
    public static final String DATE_CREATED = "dateCreated";
    public static final String EVENT_ID = "eventId";

    public static final String MODE_CAR = "car";
    public static final String MODE_BUS = "bus";
    public static final String MODE_WALK = "walk";
    public static final String MODE_HAIL = "hail";

    public static final String BROADCAST_ID = "planBroadcast";
    public static final String INTENT_EDIT_PLAN = "editPlan";
    public static final String INTENT_VALUE_SAVE = "save";
    public static final String INTENT_VALUE_EDIT = "edit";

    @Exclude
    public String id;

    @PropertyName(ACTIVITY_NAME)
    @SerializedName(ACTIVITY_NAME)
    @Expose
    public String activityName;

    @PropertyName(REMINDER_DATE)
    @SerializedName(REMINDER_DATE)
    @Expose
    public Long reminderDate;

    @PropertyName(EVENT_ID)
    @SerializedName(EVENT_ID)
    @Expose
    public int eventId;

    @PropertyName(IS_NEED_TAXI)
    @SerializedName(IS_NEED_TAXI)
    @Expose
    public Boolean isNeedTaxi;

    @PropertyName(GPS)
    @SerializedName(GPS)
    @Expose
    public GeoPoint gps;

    @PropertyName(PLACE_ADDRESS)
    @SerializedName(PLACE_ADDRESS)
    @Expose
    public PlaceAddress placeAddress;

    @PropertyName(PLACE_INDEX)
    @SerializedName(PLACE_INDEX)
    @Expose
    public HashMap<String,Boolean> placeIndex;

    @PropertyName(START_TIME)
    @SerializedName(START_TIME)
    @Expose
    public Long startTime;

    @PropertyName(TRANSPORT_MODE)
    @SerializedName(TRANSPORT_MODE)
    @Expose
    public String transportMode;

    @PropertyName(DAY)
    @SerializedName(DAY)
    @Expose
    public Long day;

    @ServerTimestamp
    @PropertyName(DATE_CREATED)
    @SerializedName(DATE_CREATED)
    @Expose
    public Timestamp dateCreated;


    public Plan(){

    }

    public static Plan snapshotToPlan(AppCompatActivity parentActivity, QueryDocumentSnapshot snapshot) {
        Plan tempPlan = snapshot.toObject(Plan.class);
        if(tempPlan != null){
            tempPlan.id = snapshot.getId();
            try{
                tempPlan.placeIndex = (HashMap<String, Boolean>) snapshot.get(Plan.PLACE_INDEX);
            }catch (Exception e){
                e.printStackTrace();
            }
            return tempPlan;
        }else{
            return null;
        }
    }

}