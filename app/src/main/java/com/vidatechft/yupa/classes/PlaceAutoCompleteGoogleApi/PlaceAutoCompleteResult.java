package com.vidatechft.yupa.classes.PlaceAutoCompleteGoogleApi;

import com.google.gson.annotations.SerializedName;
import com.vidatechft.yupa.classes.SearchPlaceGoogleApi.Candidates;
import com.vidatechft.yupa.classes.SearchPlaceGoogleApi.DebugLog;

import java.util.ArrayList;

public class PlaceAutoCompleteResult {
    @SerializedName("predictions")
    public ArrayList<Predictions> predictions;

    public PlaceAutoCompleteResult() {

    }

    public PlaceAutoCompleteResult(ArrayList<Predictions> predictions) {
        this.predictions = predictions;
    }
}
