package com.vidatechft.yupa.classes;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;

import com.google.firebase.Timestamp;
import com.google.firebase.database.PropertyName;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.ServerTimestamp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Report {
    public static final String URL_REPORT = "report";

    public static final String ID = "id";
    public static final String DATE_CREATED = "dateCreated";
    public static final String DATE_UPDATED = "dateUpdated";
    public static final String ACCOMMODATION_ID = "accommodationId";
    public static final String USER_ID = "userId";
    public static final String TARGET_ID = "targetId";
    public static final String TARGET_UID = "targetUid";
    public static final String DESCRIPTION_REPORT= "descReport";
    public static final String IS_REVIEWED= "isReviewed";
    public static final String REPORT_TYPE= "type";
    public static final String REPORT_TYPE_ROOM = "room";
    public static final String REPORT_TYPE_BLOG = "blog";
    public static final String REPORT_TYPE_BLOG_DISCUSSION = "blogDiscussion";

    @Exclude
    public String id;

    @Exclude
    public Favourite favourite;
    @Exclude
    public User user;

    @PropertyName(USER_ID)
    @SerializedName(USER_ID)
    @Expose
    public String userId;

    @PropertyName(TARGET_ID)
    @SerializedName(TARGET_ID)
    @Expose
    public String targetId;

    @PropertyName(TARGET_UID)
    @SerializedName(TARGET_UID)
    @Expose
    public String targetUid;

    @PropertyName(ACCOMMODATION_ID)
    @SerializedName(ACCOMMODATION_ID)
    @Expose
    public String accommodationId;

    @PropertyName(IS_REVIEWED)
    @SerializedName(IS_REVIEWED)
    @Expose
    public Boolean isReviewed;

    @PropertyName(DESCRIPTION_REPORT)
    @SerializedName(DESCRIPTION_REPORT)
    @Expose
    public String descReport;

    @PropertyName(REPORT_TYPE)
    @SerializedName(REPORT_TYPE)
    @Expose
    public String type;

    @ServerTimestamp
    @PropertyName(DATE_CREATED)
    @SerializedName(DATE_CREATED)
    @Expose
    public Timestamp dateCreated;

    @ServerTimestamp
    @PropertyName(DATE_UPDATED)
    @SerializedName(DATE_UPDATED)
    @Expose
    public Timestamp dateUpdated;

    public Report() { }

    public Report(AppCompatActivity parentActivity, DocumentSnapshot snapshot ){
        this.id = snapshot.getId();
        this.userId = snapshot.getString(Report.USER_ID);
        this.targetId = snapshot.getString(Report.TARGET_ID);
        this.targetUid = snapshot.getString(Report.TARGET_UID);
        this.accommodationId = snapshot.getString(Report.ACCOMMODATION_ID);
        this.dateCreated = snapshot.getTimestamp(Report.DATE_CREATED);
        this.dateUpdated = snapshot.getTimestamp(Report.DATE_UPDATED);
        this.descReport = snapshot.getString(Report.DESCRIPTION_REPORT);
        this.isReviewed = snapshot.getBoolean(Report.IS_REVIEWED);
    }

}
