package com.vidatechft.yupa.classes;

import android.graphics.drawable.Drawable;

import com.prolificinteractive.materialcalendarview.CalendarDay;

public class CustomCalendarDay {
    public Drawable drawable;
    public CalendarDay calendarDay;

    public CustomCalendarDay(Drawable drawable, CalendarDay calendarDay) {
        this.drawable = drawable;
        this.calendarDay = calendarDay;
    }
}
