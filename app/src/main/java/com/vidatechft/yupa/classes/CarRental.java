package com.vidatechft.yupa.classes;

import android.support.v7.app.AppCompatActivity;

import com.google.firebase.Timestamp;
import com.google.firebase.database.PropertyName;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.ServerTimestamp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CarRental {
    public static final String URL_CAR_RENTAL = "carRental";
    public static final String URL_COVER_PHOTO = "coverPhoto";

    public static final String STATUS_APPROVED = "approved";
    public static final String STATUS_REJECTED = "rejected";
    public static final String STATUS_PENDING = "pending";

    public static final String IS_PICKUPED = "isPickuped";
    public static final String IS_RETURNED = "isReturned";
    public static final String PICKUP_TIME = "pickupTime";
    public static final String RETURN_TIME = "returnTime";
    public static final String LOCATION = "location";
    public static final String TRANSPORT_ID = "transportId";
    public static final String USER_PICKUP_TIME = "userPickupTime";
    public static final String USER_RETURN_TIME = "userReturnTime";
    public static final String USER_ID = "userId";
    public static final String RENTAL_TYPE = "type";
    public static final String RENTAL_TYPE_EHAILING = "taxi";
    public static final String RENTAL_TYPE_RENTAL = "rental";;
    public static final String DATE_CREATED = "dateCreated";
    public static final String DATE_UPDATED = "dateUpdated";

    @Exclude
    public String id;

    @PropertyName(IS_PICKUPED)
    @SerializedName(IS_PICKUPED)
    @Expose
    public Boolean isPickuped;

    @PropertyName(IS_RETURNED)
    @SerializedName(IS_RETURNED)
    @Expose
    public Boolean isReturned;

    @PropertyName(PICKUP_TIME)
    @SerializedName(PICKUP_TIME)
    @Expose
    public Long pickupTime;

    @PropertyName(RETURN_TIME)
    @SerializedName(RETURN_TIME)
    @Expose
    public Long returnTime;

    @PropertyName(LOCATION)
    @SerializedName(LOCATION)
    @Expose
    public String location;

    @PropertyName(RENTAL_TYPE)
    @SerializedName(RENTAL_TYPE)
    @Expose
    public String type;

    @PropertyName(TRANSPORT_ID)
    @SerializedName(TRANSPORT_ID)
    @Expose
    public String transportId;

    @PropertyName(USER_PICKUP_TIME)
    @SerializedName(USER_PICKUP_TIME)
    @Expose
    public Long userPickupTime;

    @PropertyName(USER_RETURN_TIME)
    @SerializedName(USER_RETURN_TIME)
    @Expose
    public Boolean userReturnTime;

    @PropertyName(USER_ID)
    @SerializedName(USER_ID)
    @Expose
    public String userId;

    @ServerTimestamp
    @PropertyName(DATE_CREATED)
    @SerializedName(DATE_CREATED)
    @Expose
    public Timestamp dateCreated; // from here https://stackoverflow.com/questions/46254266/firebase-no-properties-to-serialize-found-on-class

    @ServerTimestamp
    @PropertyName(DATE_UPDATED)
    @SerializedName(DATE_UPDATED)
    @Expose
    public Timestamp dateUpdated;

    public CarRental(){

    }

    public static CarRental getCarRental(AppCompatActivity parentActivity, DocumentSnapshot snapshot){
        if(snapshot != null && snapshot.exists()){
            CarRental carRental = snapshot.toObject(CarRental.class);
            if(carRental != null){
                carRental.id = snapshot.getId();
            }
            return carRental;
        }else{
            return null;
        }
    }
}
