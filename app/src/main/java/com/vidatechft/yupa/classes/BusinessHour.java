package com.vidatechft.yupa.classes;

import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.PropertyName;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;

@IgnoreExtraProperties
public class BusinessHour {

    public BusinessHour() {
        minute = new HashMap<>();
        hour = new HashMap<>();
        isAM = new HashMap<>();
    }

    @PropertyName("minute")
    @SerializedName("minute")
    @Expose
    public HashMap<Integer,Integer> minute;

    @PropertyName("hour")
    @SerializedName("hour")
    @Expose
    public HashMap<Integer,Integer> hour;

    @PropertyName("isAM")
    @SerializedName("isAM")
    @Expose
    public HashMap<Integer,Boolean> isAM;

    public HashMap<Integer, Integer> getMinute() {
        return minute;
    }

    public void setMinute(HashMap<Integer, Integer> minute) {

        this.minute = minute;
    }

    public HashMap<Integer, Integer> getHour() {
        return hour;
    }

    public void setHour(HashMap<Integer, Integer> hour) {

        this.hour = hour;
    }

    public HashMap<Integer, Boolean> getIsAM() {
        return isAM;
    }

    public void setIsAM(HashMap<Integer, Boolean> isAM) {

        this.isAM = isAM;
    }

}

