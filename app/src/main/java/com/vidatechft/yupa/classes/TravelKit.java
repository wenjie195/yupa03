package com.vidatechft.yupa.classes;

import android.support.v7.app.AppCompatActivity;

import com.google.firebase.Timestamp;
import com.google.firebase.database.PropertyName;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.ServerTimestamp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class TravelKit {
    public static final String URL_TRAVELKIT = "travelKit";
    public static final String URL_COVER_PHOTO = "coverPhoto";

    public static final String STATUS_APPROVED = "approved";
    public static final String STATUS_REJECTED = "rejected";
    public static final String STATUS_PENDING = "pending";

    public static final String ID = "id";
    public static final String COUNTRY = "country";
    public static final String COVER_PHOTO_URL = "coverPhotoUrl";
    public static final String CURRENCY_CODE = "currencyCode";
    public static final String DESC = "desc";
    public static final String DISCOUNT = "discount";
    public static final String IS_PUBLISH = "isPublish";
    public static final String NAME = "name";
    public static final String ORIGINAL_PRICE = "originalPrice";
    public static final String STATE = "state";
    public static final String STATUS = "status";
    public static final String QUANTITY_MAP = "quantityMap";
    public static final String USER_ID = "userId";
    public static final String ICON_URL = "iconUrl";
    public static final String IS_ADDON = "isAddOn";
    public static final String PRODUCT_LIST = "productList";
    public static final String PRODUCT_ORDER = "productOrder";//this is product's order
    public static final String ORDER = "order"; //this is travel kit's order
    public static final String PRODUCT_REMARK = "projectRemark";
    public static final String DATE_CREATED = "dateCreated";
    public static final String DATE_UPDATED = "dateUpdated";

    @PropertyName(ID)
    @SerializedName(ID)
    @Expose
    public String id;

    @Exclude
    public ArrayList<String> selectAddOn = new ArrayList<>();

    @PropertyName(COUNTRY)
    @SerializedName(COUNTRY)
    @Expose
    public String country;

    @PropertyName(COVER_PHOTO_URL)
    @SerializedName(COVER_PHOTO_URL)
    @Expose
    public String coverPhotoUrl;

    @PropertyName(CURRENCY_CODE)
    @SerializedName(CURRENCY_CODE)
    @Expose
    public String currencyCode;

    @PropertyName(DESC)
    @SerializedName(DESC)
    @Expose
    public String desc;

    @PropertyName(ORDER)
    @SerializedName(ORDER)
    @Expose
    public Long order;

    @PropertyName(DISCOUNT)
    @SerializedName(DISCOUNT)
    @Expose
    public Double discount;

    @PropertyName(IS_PUBLISH)
    @SerializedName(IS_PUBLISH)
    @Expose
    public Boolean isPublish;

    @PropertyName(NAME)
    @SerializedName(NAME)
    @Expose
    public String name;

    @PropertyName(ICON_URL)
    @SerializedName(ICON_URL)
    @Expose
    public String iconUrl;

    @PropertyName(ORIGINAL_PRICE)
    @SerializedName(ORIGINAL_PRICE)
    @Expose
    public Double originalPrice;

    @PropertyName(STATE)
    @SerializedName(STATE)
    @Expose
    public String state;

    @PropertyName(IS_ADDON)
    @SerializedName(IS_ADDON)
    @Expose
    public Boolean isAddOn;

    @PropertyName(STATUS)
    @SerializedName(STATUS)
    @Expose
    public String status;

    @PropertyName(QUANTITY_MAP)
    @SerializedName(QUANTITY_MAP)
    @Expose
    public HashMap<String,Long> quantityMap;

    @PropertyName(PRODUCT_LIST)
    @SerializedName(PRODUCT_LIST)
    @Expose
    public HashMap<String,Double> productList;

    @PropertyName(PRODUCT_ORDER)
    @SerializedName(PRODUCT_ORDER)
    @Expose
    public ArrayList<String> productOrder;

    @PropertyName(PRODUCT_REMARK)
    @SerializedName(PRODUCT_REMARK)
    @Expose
    public HashMap<String,String> productRemark;

    @PropertyName(USER_ID)
    @SerializedName(USER_ID)
    @Expose
    public String userId;

    @ServerTimestamp
    @PropertyName(DATE_CREATED)
    @SerializedName(DATE_CREATED)
    @Expose
    public Timestamp dateCreated; // from here https://stackoverflow.com/questions/46254266/firebase-no-properties-to-serialize-found-on-class

    @ServerTimestamp
    @PropertyName(DATE_UPDATED)
    @SerializedName(DATE_UPDATED)
    @Expose
    public Timestamp dateUpdated;

    public TravelKit(){

    }

    public static TravelKit getTravelKit(AppCompatActivity parentActivity, DocumentSnapshot snapshot){
        if(snapshot != null && snapshot.exists()){
            TravelKit travelKit = snapshot.toObject(TravelKit.class);
            if(travelKit != null){
                travelKit.id = snapshot.getId();
            }
            return  travelKit;
        }else{
            return null;
        }
    }
}
