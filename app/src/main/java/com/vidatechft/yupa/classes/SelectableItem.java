package com.vidatechft.yupa.classes;

public class SelectableItem extends Transport{
    private boolean isSelected = false;


    public SelectableItem(Transport item,boolean isSelected) {
        super(item.id,item.make);
        this.isSelected = isSelected;
    }


    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}