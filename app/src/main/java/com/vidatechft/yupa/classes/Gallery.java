package com.vidatechft.yupa.classes;

import com.google.firebase.firestore.PropertyName;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

////convert map to class from here https://stackoverflow.com/questions/16428817/convert-a-mapstring-string-to-a-pojo
//this.gallery = new ObjectMapper().convertValue(snapshot.get(Itinerary.GALLERY), new TypeReference<ArrayList<Gallery>>(){});
public class Gallery {
    public static final String NAME = "name";
    public static final String DESC = "desc";
    public static final String URL = "url";

    @PropertyName(NAME)
    @SerializedName(NAME)
    @Expose
    public String name;

    @PropertyName(DESC)
    @SerializedName(DESC)
    @Expose
    public String desc;

    @PropertyName(URL)
    @SerializedName(URL)
    @Expose
    public String url;

    public Gallery() {

    }

    public Gallery(String name, String desc, String url) {
        this.name = name;
        this.desc = desc;
        this.url = url;
    }
}
