package com.vidatechft.yupa.classes;

import com.google.firebase.firestore.GeoPoint;
import com.vidatechft.yupa.classes.PlaceSearchRecommendedGoogleApi.PlaceRecommendedResult;

import java.util.Date;

public class PlanSuggestion {
    public String activityName;
    public PlaceRecommendedResult placeRecommendedResult;


    public PlanSuggestion(){

    }

    public PlanSuggestion(String activityName, PlaceRecommendedResult placeRecommendedResult) {
        this.activityName = activityName;
        this.placeRecommendedResult = placeRecommendedResult;
    }
}
