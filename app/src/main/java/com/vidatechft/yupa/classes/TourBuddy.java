package com.vidatechft.yupa.classes;

import com.google.android.gms.location.places.Place;
import com.google.firebase.Timestamp;
import com.google.firebase.database.PropertyName;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.ServerTimestamp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.PlaceDetailsGoogleApi.PlaceAddress;
import com.vidatechft.yupa.utilities.MillisecondsToDate;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//todo when searching tour buddy available date, need to query availability equals to "everyday,custom,etc..." also because if user changes from custom to others, the customDateIndex is still exists
//todo same goes for job seeker
public class TourBuddy
{
    public static final String URL_TOUR_BUDDY = "applyTourBuddy";
    public static final String URL_TOUR_BUDDY_CLASS_NAME = "tourBuddy";
    public static final String URL_TOUR_BUDDY_BOOK_CLASS_NAME = "bookTourBuddy";
    public static final String TOUR_BUDDY_USER_ID = "userID";
    public static final String TOUR_BUDDY_BOOKING_ID = "tourBuddyBookingID";
    public static final String TOUR_BUDDY_GET_CURRENT_LOCATION = "gps";
    public static final String TOUR_BUDDY_GET_ALL_AVAILABLE_DATES = "customDate";
    public static final String TOUR_BUDDY_GET_ALL_AVAILABLE_DATES_INDEX = "customDateIndex";

    public static final String TOUR_BUDDY_UID = "tourBuddyUid";
    public static final String TOUR_BUDDY_CURRENTLY_SEARCHING_UID = "uid";
    public static final String TOUR_BUDDY_SEARCH_DEPART_DATE = "departDate";
    public static final String TOUR_BUDDY_SEARCH_RETURN_DATE = "returnDate";

    public static final String TOUR_BUDDY_DATE_CREATED = "dateCreated";
    public static final String TOUR_BUDDY_DATE_UPDATED = "dateUpdated";
    public static final String TOUR_BUDDY_AVAILABILITY = "availability";

    public static final String TOUR_BUDDY_ADDRESS = "address";
    public static final String TOUR_BUDDY_PLACE_ADDRESS = "placeAddress";
    public static final String TOUR_BUDDY_PLACE_INDEX = "placeIndex";

    public static final String TOUR_BUDDY_APPLY_STATUS = "status";
    public static final String TOUR_BUDDY_APPLY_STATUS_ACCEPTED = "accepted";
    public static final String TOUR_BUDDY_APPLY_STATUS_REJECTED = "rejected";
    public static final String TOUR_BUDDY_APPLY_STATUS_PENDING = "pending";

    public static final String TOUR_BUDDY_IS_AVAILABLE = "isTourBuddyAvailable"; // it means he is available to be a tour buddy at the moment


    @Exclude
    public String tourBuddyUid;

    @PropertyName(URL_TOUR_BUDDY)
    @SerializedName(URL_TOUR_BUDDY)
    @Expose
    public String applyTourBuddy;

    @PropertyName(TOUR_BUDDY_ADDRESS)
    @SerializedName(TOUR_BUDDY_ADDRESS)
    @Expose
    public String address;

    @PropertyName(TOUR_BUDDY_APPLY_STATUS)
    @SerializedName(TOUR_BUDDY_APPLY_STATUS)
    @Expose
    public String status;

    @PropertyName(TOUR_BUDDY_AVAILABILITY)
    @SerializedName(TOUR_BUDDY_AVAILABILITY)
    @Expose
    public String availability;

    @PropertyName(TOUR_BUDDY_USER_ID)
    @SerializedName(TOUR_BUDDY_USER_ID)
    @Expose
    public String userID;

    @PropertyName(TOUR_BUDDY_GET_CURRENT_LOCATION)
    @SerializedName(TOUR_BUDDY_GET_CURRENT_LOCATION)
    @Expose
    public GeoPoint gps;

    @PropertyName(TOUR_BUDDY_PLACE_INDEX)
    @SerializedName(TOUR_BUDDY_PLACE_INDEX)
    @Expose
    public HashMap<String,Boolean> placeIndex;

    @ServerTimestamp
    @PropertyName(TOUR_BUDDY_DATE_CREATED)
    @SerializedName(TOUR_BUDDY_DATE_CREATED)
    @Expose
    public Timestamp dateCreated;

    @ServerTimestamp
    @PropertyName(TOUR_BUDDY_DATE_UPDATED)
    @SerializedName(TOUR_BUDDY_DATE_UPDATED)
    @Expose
    public Timestamp dateUpdated;

    @PropertyName(TOUR_BUDDY_PLACE_ADDRESS)
    @SerializedName(TOUR_BUDDY_PLACE_ADDRESS)
    @Expose
    public PlaceAddress placeAddress;

    @PropertyName(TOUR_BUDDY_GET_ALL_AVAILABLE_DATES)
    @SerializedName(TOUR_BUDDY_GET_ALL_AVAILABLE_DATES)
    @Expose
    public String customDate;

    @PropertyName(TOUR_BUDDY_GET_ALL_AVAILABLE_DATES_INDEX)
    @SerializedName(TOUR_BUDDY_GET_ALL_AVAILABLE_DATES_INDEX)
    @Expose
    public HashMap<String,Boolean> customDateIndex;

    public TourBuddy (){}
}
