package com.vidatechft.yupa.classes.PlaceAutoCompleteGoogleApi;

import com.google.gson.annotations.SerializedName;

public class Predictions {
    //because i need place_id only for now so i include place id only, if need more just add
    @SerializedName("place_id")
    public String placeId;

    public Predictions(){

    }

    public Predictions(String placeId) {
        this.placeId = placeId;
    }
}
