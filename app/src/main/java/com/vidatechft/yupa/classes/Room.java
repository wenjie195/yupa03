package com.vidatechft.yupa.classes;

import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.Timestamp;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.PropertyName;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.ServerTimestamp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.maps.android.clustering.ClusterItem;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.classes.PlaceDetailsGoogleApi.PlaceAddress;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.io.Serializable;
import java.sql.Time;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@IgnoreExtraProperties
public class Room implements Serializable, ClusterItem, Comparable<Room> {
    public static final String HOST_ID = "hostId";
    public static final String NAME = "name";
    public static final String DIR_DOCUMENT = "pushId1";
    public static final String DIR_ELECTRIC_BILL = "dirElectricBill";
    public static final String ROOM_NAME = "roomName";
    public static final String ELECTRIC_BILL = "electricBill";
    public static final String URL_ROOM = "room";
    public static final String URL_UPLOADED_ROOMS = "uploadedRooms";
    public static final String URL_ROOM_PIC = "urlRoomPic";
    public static final String URL_ROOM_PROOF = "proof";
    public static final String URL_ROOM_OUTLOOK_PIC = "outlookPic";

    public static final String IS_DRAFT= "isDraft";
    public static final String IS_PUBLISH= "isPublish";
    public static final String IS_EDIT_PROFILE= "isEditProfile";

    public static final String PUSH_ID_ROOM_PIC = "pushIdRoomPic";

    public static final String URL_ROOM_ELECTRIC_BILL_PIC = "electricBillPic";
    public static final String ACCOMMODATION_DETIALS = "accommodationDetails";
    public static final String ACCOMMODATION_DETIALS_ALL = "accommodationDetailsAll";
    public static final String UPLOAD_ROOM_DETIALS_ALL = "uploadRoomDetailsAll";
    public static final String UPLOAD_ROOM_DETIALS = "uploadRoomDetails";
    public static final String URL_OUTLOOK = "urlOutlook";
    public static final String URL_ELECTRIC_BILL = "urlElectricBill";
    public static final String ROOM_TYPE = "roomType";
    public static final String ACCOMMODATION_NAME = "accommodationName";
    public static final String ACCOMMODATION_LOT_DETAILS = "location";
    public static final String ACCOMMODATION_GEOPOINT = "gps";
    public static final String ACCOMMODATION_ADDRESS = "address";
    public static final String ACCOMMODATION_PLACE_ADDRESS = "placeAddress";
    public static final String ACCOMMODATION_PLACE_INDEX = "placeIndex";
    public static final String CURRENCY_TYPE = "currencyType";
    public static final String CURRENCY_VALUE = "currencyValue";
    public static final String GUEST_QUANTITY = "guestQuantity";
    public static final String BEDROOM_QUANTITY = "bedroomQuantity";
    public static final String SINGLE_SIZE_BED_QUANTITY = "singleSizeBedQuantity";
    public static final String QUEEN_SIZE_BED_QUANTITY = "queenSizeBedQuantity";
    public static final String KING_SIZE_BED_QUANTITY = "kingSizeBedQuantity";
    public static final String BATHROOM_QUANTITY = "bathroomQuantity";
    public static final String STATUS = "status";
    public static final String STATUS_PENDING = "pending";
    public static final String STATUS_PUBLISHED = "published";
    public static final String STATUS_UNPUBLISHED = "unpublished";
    public static final String STATUS_REJECT = "rejected";
    public static final String STATUS_APPROVED = "approved";
    public static final String LIKE_COUNT = "likeCount";
    public static final String NOT_AVAILABLE = "Not available!";

    //status type
    public static final String TYPE_START_HOST = "typeStartHost";
    public static final String TYPE_WAIT_VERIFY = "typeWaitVerify";
    public static final String TYPE_PUBLISHED = "typePublished";
    public static final String TYPE_UNPUBLISHED = "typeUnPublished";
    public static final String TYPE_REJECTED = "typeRejected";

    //room directory
    public static final String DIR_ROOM_1 = "dirRoom1";
    public static final String DIR_ROOM_2 = "dirRoom2";
    public static final String DIR_ROOM_3 = "dirRoom3";
    public static final String DIR_ROOM_4 = "dirRoom4";
    public static final String DIR_ROOM_5 = "dirRoom5";
    public static final String DIR_ROOM_6 = "dirRoom6";
    public static final String DIR_ROOM_7 = "dirRoom7";
    public static final String DIR_ROOM_8 = "dirRoom8";

    //room type
    public static final String TYPE_ROOM_ENTIRE_PLACE = "entirePlace";
    public static final String TYPE_ROOM_PRIVATE = "privateRoom";
    public static final String TYPE_ROOM_SHARE = "shareRoom";

    //Room photo
    public static final String IMG_ROOM_1 = "imgRoom1";
    public static final String IMG_ROOM_2 = "imgRoom2";
    public static final String IMG_ROOM_3 = "imgRoom3";
    public static final String IMG_ROOM_4 = "imgRoom4";
    public static final String IMG_ROOM_5 = "imgRoom5";
    public static final String IMG_ROOM_6 = "imgRoom6";
    public static final String IMG_ROOM_7 = "imgRoom7";
    public static final String IMG_ROOM_8 = "imgRoom8";

    public static final String URL_IMG_ROOM_1 = "urlRoom1";
    public static final String URL_IMG_ROOM_2 = "urlRoom2";
    public static final String URL_IMG_ROOM_3 = "urlRoom3";
    public static final String URL_IMG_ROOM_4 = "urlRoom4";
    public static final String URL_IMG_ROOM_5 = "urlRoom5";
    public static final String URL_IMG_ROOM_6 = "urlRoom6";
    public static final String URL_IMG_ROOM_7 = "urlRoom7";
    public static final String URL_IMG_ROOM_8 = "urlRoom8";

    //Room name
    public static final String ROOM_NAME_1 = "roomName1";
    public static final String ROOM_NAME_2 = "roomName2";
    public static final String ROOM_NAME_3 = "roomName3";
    public static final String ROOM_NAME_4 = "roomName4";
    public static final String ROOM_NAME_5 = "roomName5";
    public static final String ROOM_NAME_6 = "roomName6";
    public static final String ROOM_NAME_7 = "roomName7";
    public static final String ROOM_NAME_8 = "roomName8";

    public static final String DATE_CREATED = "dateCreated";
    public static final String DATE_UPDATED = "dateUpdated";

    public static final String POSITION = "position";
    public static final String GUEST = "Guests";


    @Exclude
    public ArrayList<BookingHistory> bookingHistories; //this is for HostStayManageRecyclerGridAdapter only

    @Exclude
    public String status;

    @Exclude
    public Boolean isFavourite;

    @Exclude
    public String id;

    @Exclude
    public BookingHistory bookingHistory;

    @Exclude
    public ArrayList<String> amenityList = new ArrayList<>();


    @PropertyName(ROOM_TYPE)
    @SerializedName(ROOM_TYPE)
    @Expose
    public String roomType;

    @PropertyName(ACCOMMODATION_NAME)
    @SerializedName(ACCOMMODATION_NAME)
    @Expose
    public String accommodationName;

    @PropertyName(LIKE_COUNT)
    @SerializedName(LIKE_COUNT)
    @Expose
    public Long likeCount;

    @PropertyName(ACCOMMODATION_LOT_DETAILS)
    @SerializedName(ACCOMMODATION_LOT_DETAILS)
    @Expose
    public String location;

    @PropertyName(ACCOMMODATION_GEOPOINT)
    @SerializedName(ACCOMMODATION_GEOPOINT)
    @Expose
    public GeoPoint accommodationGeoPoint;

    @PropertyName(ACCOMMODATION_ADDRESS)
    @SerializedName(ACCOMMODATION_ADDRESS)
    @Expose
    public String address;

    @PropertyName(ACCOMMODATION_PLACE_ADDRESS)
    @SerializedName(ACCOMMODATION_PLACE_ADDRESS)
    @Expose
    public PlaceAddress placeAddress;

    @PropertyName(ACCOMMODATION_PLACE_INDEX)
    @SerializedName(ACCOMMODATION_PLACE_INDEX)
    @Expose
    public HashMap<String,Boolean> placeIndex;

    @PropertyName(HOST_ID)
    @SerializedName(HOST_ID)
    @Expose
    public String hostId;

    @PropertyName(CURRENCY_TYPE)
    @SerializedName(CURRENCY_TYPE)
    @Expose
    public String currencyType;

    @PropertyName(CURRENCY_VALUE)
    @SerializedName(CURRENCY_VALUE)
    @Expose
    public Double currencyValue;

    @PropertyName(GUEST_QUANTITY)
    @SerializedName(GUEST_QUANTITY)
    @Expose
    public Integer guestQuantity;

    @PropertyName(BEDROOM_QUANTITY)
    @SerializedName(BEDROOM_QUANTITY)
    @Expose
    public Integer bedroomQuantity;

    @PropertyName(SINGLE_SIZE_BED_QUANTITY)
    @SerializedName(SINGLE_SIZE_BED_QUANTITY)
    @Expose
    public Integer singleSizeBedQuantity;

    @PropertyName(QUEEN_SIZE_BED_QUANTITY)
    @SerializedName(QUEEN_SIZE_BED_QUANTITY)
    @Expose
    public Integer queenSizeBedQuantity;

    @PropertyName(KING_SIZE_BED_QUANTITY)
    @SerializedName(KING_SIZE_BED_QUANTITY)
    @Expose
    public Integer kingSizeBedQuantity;

    @PropertyName(BATHROOM_QUANTITY)
    @SerializedName(BATHROOM_QUANTITY)
    @Expose
    public Integer bathroomQuantity;

    @PropertyName(URL_OUTLOOK)
    @SerializedName(URL_OUTLOOK)
    @Expose
    public String urlOutlook;

    @PropertyName(URL_ELECTRIC_BILL)
    @SerializedName(URL_ELECTRIC_BILL)
    @Expose
    public String urlElectricBill;

    @PropertyName(URL_ROOM_PIC)
    @SerializedName(URL_ROOM_PIC)
    @Expose
    public String urlRoomPic;

    @PropertyName(ROOM_NAME)
    @SerializedName(ROOM_NAME)
    @Expose
    public String roomName;

    @PropertyName(PUSH_ID_ROOM_PIC)
    @SerializedName(PUSH_ID_ROOM_PIC)
    @Expose
    public String pushIdRoomPic;

    //Room Image
    @PropertyName(IMG_ROOM_1)
    @SerializedName(IMG_ROOM_1)
    @Expose
    public String imgRoom1;

    @PropertyName(IMG_ROOM_2)
    @SerializedName(IMG_ROOM_2)
    @Expose
    public String imgRoom2;

    @PropertyName(IMG_ROOM_3)
    @SerializedName(IMG_ROOM_3)
    @Expose
    public String imgRoom3;

    @PropertyName(IMG_ROOM_4)
    @SerializedName(IMG_ROOM_4)
    @Expose
    public String imgRoom4;

    @PropertyName(IMG_ROOM_5)
    @SerializedName(IMG_ROOM_5)
    @Expose
    public String imgRoom5;

    @PropertyName(IMG_ROOM_6)
    @SerializedName(IMG_ROOM_6)
    @Expose
    public String imgRoom6;

    @PropertyName(IMG_ROOM_7)
    @SerializedName(IMG_ROOM_7)
    @Expose
    public String imgRoom7;

    @PropertyName(IMG_ROOM_8)
    @SerializedName(IMG_ROOM_8)
    @Expose
    public String imgRoom8;

    //Room name
    @PropertyName(ROOM_NAME_1)
    @SerializedName(ROOM_NAME_1)
    @Expose
    public String roomName1;

    @PropertyName(ROOM_NAME_2)
    @SerializedName(ROOM_NAME_2)
    @Expose
    public String roomName2;

    @PropertyName(ROOM_NAME_3)
    @SerializedName(ROOM_NAME_3)
    @Expose
    public String roomName3;

    @PropertyName(ROOM_NAME_4)
    @SerializedName(ROOM_NAME_4)
    @Expose
    public String roomName4;

    @PropertyName(ROOM_NAME_5)
    @SerializedName(ROOM_NAME_5)
    @Expose
    public String roomName5;

    @PropertyName(ROOM_NAME_6)
    @SerializedName(ROOM_NAME_6)
    @Expose
    public String roomName6;

    @PropertyName(ROOM_NAME_7)
    @SerializedName(ROOM_NAME_7)
    @Expose
    public String roomName7;

    @PropertyName(ROOM_NAME_8)
    @SerializedName(ROOM_NAME_8)
    @Expose
    public String roomName8;

    @PropertyName(POSITION)
    @SerializedName(POSITION)
    @Expose
    public int position;

    @PropertyName(IS_DRAFT)
    @SerializedName(IS_DRAFT)
    @Expose
    public Boolean isDraft;

    @PropertyName(IS_PUBLISH)
    @SerializedName(IS_PUBLISH)
    @Expose
    public Boolean isPublish;

    @PropertyName(IS_EDIT_PROFILE)
    @SerializedName(IS_EDIT_PROFILE)
    @Expose
    public Boolean isEditProfile;

    @ServerTimestamp
    @PropertyName(DATE_CREATED)
    @SerializedName(DATE_CREATED)
    @Expose
    public Timestamp dateCreated; // from here https://stackoverflow.com/questions/46254266/firebase-no-properties-to-serialize-found-on-class

    @ServerTimestamp
    @PropertyName(DATE_UPDATED)
    @SerializedName(DATE_UPDATED)
    @Expose
    public Timestamp dateUpdated; // from here https://stackoverflow.com/questions/46254266/firebase-no-properties-to-serialize-found-on-class


    public Room(){

    }

    public Room(String urlOutlook, String roomType, String accommodationName, String location,String address, GeoPoint accommodationGeoPoint, String urlElectricBill, String currencyType, Double currencyValue, Integer guestQuantity, Integer bedroomQuantity, Integer singleSizeBedQuantity, Integer queenSizeBedQuantity, Integer kingSizeBedQuantity, Integer bathroomQuantity) {
        this.urlOutlook = urlOutlook;
        this.roomType = roomType;
        this.accommodationName = accommodationName;
        this.location = location;
        this.address = address;
        this.accommodationGeoPoint = accommodationGeoPoint;
        this.urlElectricBill = urlElectricBill;
        this.currencyType = currencyType;
        this.currencyValue = currencyValue;
        this.guestQuantity = guestQuantity;
        this.bedroomQuantity = bedroomQuantity;
        this.singleSizeBedQuantity = singleSizeBedQuantity;
        this.queenSizeBedQuantity = queenSizeBedQuantity;
        this.kingSizeBedQuantity = kingSizeBedQuantity;
        this.bathroomQuantity = bathroomQuantity;
    }

    public Room(String imgRoom1, String imgRoom2, String imgRoom3, String imgRoom4, String imgRoom5, String imgRoom6, String imgRoom7, String imgRoom8, String roomName1, String roomName2, String roomName3, String roomName4, String roomName5, String roomName6, String roomName7, String roomName8) {
        this.imgRoom1 = imgRoom1;
        this.imgRoom2 = imgRoom2;
        this.imgRoom3 = imgRoom3;
        this.imgRoom4 = imgRoom4;
        this.imgRoom5 = imgRoom5;
        this.imgRoom6 = imgRoom6;
        this.imgRoom7 = imgRoom7;
        this.imgRoom8 = imgRoom8;
        this.roomName1 = roomName1;
        this.roomName2 = roomName2;
        this.roomName3 = roomName3;
        this.roomName4 = roomName4;
        this.roomName5 = roomName5;
        this.roomName6 = roomName6;
        this.roomName7 = roomName7;
        this.roomName8 = roomName8;
    }

    public Room(AppCompatActivity parentActivity, DocumentSnapshot snapshot){
        this.id = snapshot.getId();
        this.urlOutlook = snapshot.getString(Room.URL_OUTLOOK);
        this.accommodationName = snapshot.getString(Room.ACCOMMODATION_NAME);
        this.likeCount = snapshot.getLong(Room.LIKE_COUNT);
        this.roomType = snapshot.getString(Room.ROOM_TYPE);
        this.location = snapshot.getString(Room.ACCOMMODATION_LOT_DETAILS);
        this.address = snapshot.getString(Room.ACCOMMODATION_ADDRESS);
        try{
            this.placeAddress = PlaceAddress.getMapToClass((HashMap<String, Object>) snapshot.get(Room.ACCOMMODATION_PLACE_ADDRESS));
        }catch (Exception e){
            e.printStackTrace();
        }
        try{
            this.placeIndex = (HashMap<String,Boolean>) snapshot.get(Room.ACCOMMODATION_PLACE_INDEX);
        }catch (Exception e){
            e.printStackTrace();
        }
        this.accommodationGeoPoint = snapshot.getGeoPoint(Room.ACCOMMODATION_GEOPOINT);
        this.urlElectricBill = snapshot.getString(Room.URL_ELECTRIC_BILL);
        this.hostId = snapshot.getString(Room.HOST_ID);
        this.currencyType = snapshot.getString(Room.CURRENCY_TYPE);
        this.currencyValue = snapshot.getLong(Room.CURRENCY_VALUE).doubleValue();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            this.guestQuantity = Objects.requireNonNull(snapshot.getLong(Room.GUEST_QUANTITY)).intValue();
            this.bedroomQuantity = Objects.requireNonNull(snapshot.getLong(Room.BEDROOM_QUANTITY)).intValue();
            this.singleSizeBedQuantity = Objects.requireNonNull(snapshot.getLong(Room.SINGLE_SIZE_BED_QUANTITY)).intValue();
            this.queenSizeBedQuantity = Objects.requireNonNull(snapshot.getLong(Room.QUEEN_SIZE_BED_QUANTITY)).intValue();
            this.kingSizeBedQuantity = Objects.requireNonNull(snapshot.getLong(Room.KING_SIZE_BED_QUANTITY)).intValue();
            this.bathroomQuantity = Objects.requireNonNull(snapshot.getLong(Room.BATHROOM_QUANTITY)).intValue();
        }

        this.isPublish = snapshot.getBoolean(Room.IS_PUBLISH);
        this.isDraft = snapshot.getBoolean(Room.IS_DRAFT);
        this.status = snapshot.getString(Room.STATUS);
        this.roomName = snapshot.getString(Room.ROOM_NAME);
        this.urlRoomPic = snapshot.getString(Room.URL_ROOM_PIC);
        this.amenityList = GeneralFunction.copyListIfMatchValueQuerySnapshot(parentActivity,snapshot, R.array.host_select_amenity_list);
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public GeoPoint getAccommodationGeoPoint() {
        return accommodationGeoPoint;
    }

    public void setAccommodationGeoPoint(GeoPoint accommodationGeoPoint) {
        this.accommodationGeoPoint = accommodationGeoPoint;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyType(String currencyType) {
        this.currencyType = currencyType;
    }

    public Double getCurrencyValue() {
        return currencyValue;
    }

    public void setCurrencyValue(Double currencyValue) {
        this.currencyValue = currencyValue;
    }

    public Integer getGuestQuantity() {
        return guestQuantity;
    }

    public void setGuestQuantity(Integer guestQuantity) {
        this.guestQuantity = guestQuantity;
    }

    public Integer getBedroomQuantity() {
        return bedroomQuantity;
    }

    public void setBedroomQuantity(Integer bedroomQuantity) {
        this.bedroomQuantity = bedroomQuantity;
    }

    public Integer getSingleSizeBedQuantity() {
        return singleSizeBedQuantity;
    }

    public void setSingleSizeBedQuantity(Integer singleSizeBedQuantity) {
        this.singleSizeBedQuantity = singleSizeBedQuantity;
    }

    public Integer getQueenSizeBedQuantity() {
        return queenSizeBedQuantity;
    }

    public void setQueenSizeBedQuantity(Integer queenSizeBedQuantity) {
        this.queenSizeBedQuantity = queenSizeBedQuantity;
    }

    public Integer getKingSizeBedQuantity() {
        return kingSizeBedQuantity;
    }

    public void setKingSizeBedQuantity(Integer kingSizeBedQuantity) {
        this.kingSizeBedQuantity = kingSizeBedQuantity;
    }

    public Integer getBathroomQuantity() {
        return bathroomQuantity;
    }

    public void setBathroomQuantity(Integer bathroomQuantity) {
        this.bathroomQuantity = bathroomQuantity;
    }


    public String getUrlElectricBill() {
        return urlElectricBill;
    }

    public void setUrlElectricBill(String urlElectricBill) {
        this.urlElectricBill = urlElectricBill;
    }

    public String getAccommodationName() {
        return accommodationName;
    }

    public void setAccommodationName(String accommodationName) {
        this.accommodationName = accommodationName;
    }

    public Timestamp getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Timestamp dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Timestamp getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Timestamp dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public String getUrlOutlook() {
        return urlOutlook;
    }

    public void setUrlOutlook(String urlOutlook) {
        this.urlOutlook = urlOutlook;
    }

    public String getUrlRoomPic() {
        return urlRoomPic;
    }

    public void setUrlRoomPic(String urlRoomPic) {
        this.urlRoomPic = urlRoomPic;
    }

    public String getPushIdRoomPic() {
        return pushIdRoomPic;
    }

    public void setPushIdRoomPic(String pushIdRoomPic) {
        this.pushIdRoomPic = pushIdRoomPic;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public int getRoomPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @SerializedName("rooms")
    @Expose
    public List<Room> rooms = new ArrayList<Room>();

    public List<Room> getRooms() {
        return rooms;
    }

    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }

    public static HashMap<String,Object> toMap(Room room){
        HashMap<String,Object> roomMap = new HashMap<>();

        roomMap.put(ACCOMMODATION_NAME,room.accommodationName);
        roomMap.put(LIKE_COUNT,room.likeCount);
        roomMap.put(URL_OUTLOOK,room.urlOutlook);
        roomMap.put(URL_ELECTRIC_BILL,room.urlElectricBill);
        roomMap.put(ROOM_TYPE,room.roomType);
        roomMap.put(ACCOMMODATION_LOT_DETAILS,room.location);
        roomMap.put(ACCOMMODATION_GEOPOINT,room.accommodationGeoPoint);
        roomMap.put(ACCOMMODATION_ADDRESS,room.address);
        if(room.placeAddress != null && room.placeAddress.placeId != null){
            roomMap.put(ACCOMMODATION_PLACE_ADDRESS,PlaceAddress.getHashmap(room.placeAddress,room.placeAddress.placeId));
        }
        roomMap.put(ACCOMMODATION_PLACE_INDEX,room.placeIndex);
        roomMap.put(CURRENCY_TYPE,room.currencyType);
        roomMap.put(CURRENCY_VALUE,room.currencyValue);
        roomMap.put(GUEST_QUANTITY,room.guestQuantity);
        roomMap.put(BEDROOM_QUANTITY,room.bedroomQuantity);
        roomMap.put(SINGLE_SIZE_BED_QUANTITY,room.singleSizeBedQuantity);
        roomMap.put(QUEEN_SIZE_BED_QUANTITY,room.queenSizeBedQuantity);
        roomMap.put(KING_SIZE_BED_QUANTITY,room.kingSizeBedQuantity);
        roomMap.put(BATHROOM_QUANTITY,room.bathroomQuantity);
        roomMap.put(IS_DRAFT,room.isDraft);
        roomMap.put(IS_PUBLISH,room.isPublish);
        roomMap.put(STATUS,room.status);
        roomMap.put(HOST_ID,room.hostId);
        roomMap.put(DATE_CREATED,room.dateCreated);
        roomMap.put(DATE_UPDATED,room.dateUpdated);

        return roomMap;
    }

    public static Room toClass(Map<String,Object> roomMap, AppCompatActivity parentActivity){
        Room room = new Room();
        room.accommodationName = (String) roomMap.get(ACCOMMODATION_NAME);
        room.likeCount = (Long) roomMap.get(LIKE_COUNT);
        room.urlOutlook = (String) roomMap.get(URL_OUTLOOK);
        room.urlElectricBill = (String) roomMap.get(URL_ELECTRIC_BILL);
        room.roomType = (String) roomMap.get(ROOM_TYPE);
        room.location = (String) roomMap.get(ACCOMMODATION_LOT_DETAILS);
        room.accommodationGeoPoint = (GeoPoint) roomMap.get(ACCOMMODATION_GEOPOINT);
        room.address = (String) roomMap.get(ACCOMMODATION_ADDRESS);
        try{
            room.placeAddress = PlaceAddress.getMapToClass((HashMap<String, Object>) roomMap.get(Room.ACCOMMODATION_PLACE_ADDRESS));
        }catch (Exception e){
            e.printStackTrace();
        }
        try{
            room.placeIndex = (HashMap<String,Boolean>) roomMap.get(Room.ACCOMMODATION_PLACE_INDEX);
        }catch (Exception e){
            e.printStackTrace();
        }
        room.currencyType = (String) roomMap.get(CURRENCY_TYPE);
        room.currencyValue = (Double) roomMap.get(CURRENCY_VALUE);
//        room.currencyValue = (String) roomMap.get(CURRENCY_VALUE);
        room.guestQuantity = ((Long) roomMap.get(GUEST_QUANTITY)).intValue();
//        room.guestQuantity = (Integer) roomMap.get(GUEST_QUANTITY);
        room.bedroomQuantity = ((Long) roomMap.get(BEDROOM_QUANTITY)).intValue();
//        room.bedroomQuantity = (Integer) roomMap.get(BEDROOM_QUANTITY);
        room.singleSizeBedQuantity = ((Long) roomMap.get(SINGLE_SIZE_BED_QUANTITY)).intValue();
//        room.singleSizeBedQuantity = (Integer) roomMap.get(SINGLE_SIZE_BED_QUANTITY);
        room.queenSizeBedQuantity = ((Long) roomMap.get(QUEEN_SIZE_BED_QUANTITY)).intValue();
//        room.queenSizeBedQuantity = (Integer) roomMap.get(QUEEN_SIZE_BED_QUANTITY);
        room.kingSizeBedQuantity = ((Long) roomMap.get(KING_SIZE_BED_QUANTITY)).intValue();
//        room.kingSizeBedQuantity = (Integer) roomMap.get(KING_SIZE_BED_QUANTITY);
        room.bathroomQuantity = ((Long) roomMap.get(BATHROOM_QUANTITY)).intValue();
//        room.bathroomQuantity = (Integer) roomMap.get(BATHROOM_QUANTITY);
        room.isDraft = (Boolean) roomMap.get(IS_DRAFT);
        room.isPublish = (Boolean) roomMap.get(IS_PUBLISH);
        room.status = (String) roomMap.get(STATUS);
        room.hostId = (String) roomMap.get(HOST_ID);
        room.dateCreated = (Timestamp) roomMap.get(DATE_CREATED);
        room.dateUpdated = (Timestamp) roomMap.get(DATE_UPDATED);
        return room;
    }

    @Override
    public LatLng getPosition() {
        if(accommodationGeoPoint == null){
            return new LatLng(0,0);
        }
        return new LatLng(accommodationGeoPoint.getLatitude(),accommodationGeoPoint.getLongitude());
    }

    @Override
    public String getTitle() {
        if(accommodationName == null){
            return "";
        }
        return accommodationName;
    }

    @Override
    public String getSnippet() {
        if(address == null){
            return "";
        }
        return address;
    }

    //from here https://stackoverflow.com/questions/5927109/sort-objects-in-arraylist-by-date
    @Override
    public int compareTo(@NonNull Room room) {
        if (dateUpdated == null || room.dateUpdated == null)
            return 0;
        return room.dateUpdated.compareTo(dateUpdated);//descending order, newest is at top
    }

    public static Room snapshotToRoom(AppCompatActivity parentActivity, DocumentSnapshot snapshot) {
        Room tempRoom = snapshot.toObject(Room.class);
        if(tempRoom != null){
            tempRoom.id = snapshot.getId();
            tempRoom.accommodationGeoPoint = snapshot.getGeoPoint(Room.ACCOMMODATION_GEOPOINT);//dont know why he is not getting the gps la from to object but there really is a geo point
            try{
                tempRoom.placeAddress = PlaceAddress.getMapToClass((HashMap<String, Object>) snapshot.get(Room.ACCOMMODATION_PLACE_ADDRESS));
            }catch (Exception e){
                e.printStackTrace();
            }
            try{
                tempRoom.placeIndex = (HashMap<String,Boolean>) snapshot.get(Room.ACCOMMODATION_PLACE_INDEX);
            }catch (Exception e){
                e.printStackTrace();
            }

            tempRoom.amenityList = GeneralFunction.copyListIfMatchValueQuerySnapshot(parentActivity,snapshot, R.array.host_select_amenity_list);

            return tempRoom;
        }else{
            return null;
        }
    }
}


