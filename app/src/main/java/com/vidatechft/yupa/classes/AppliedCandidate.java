package com.vidatechft.yupa.classes;

import com.google.firebase.Timestamp;
import com.google.firebase.database.PropertyName;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.ServerTimestamp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class AppliedCandidate {
    public static final String URL_CANDIDATE = "appliedCandidate";

    public static final String DATE_CREATED = "dateCreated";
    public static final String DATE_UPDATED = "dateUpdated";
    public static final String IS_ACCEPTED = "isAccepted";
    public static final String DESC = "desc";
    public static final String CHAT_ID = "chatId";

    @Exclude
    public User user = new User();

    @Exclude
    public String uid;

    @ServerTimestamp
    @PropertyName(DATE_CREATED)
    @SerializedName(DATE_CREATED)
    @Expose
    public Timestamp dateCreated;

    @ServerTimestamp
    @PropertyName(DATE_UPDATED)
    @SerializedName(DATE_UPDATED)
    @Expose
    public Timestamp dateUpdated;

    @PropertyName(IS_ACCEPTED)
    @SerializedName(IS_ACCEPTED)
    @Expose
    public Boolean isAccepted;

    @PropertyName(DESC)
    @SerializedName(DESC)
    @Expose
    public String desc;

    @PropertyName(CHAT_ID)
    @SerializedName(CHAT_ID)
    @Expose
    public String chatId;

    public AppliedCandidate(){

    }

    public AppliedCandidate(QueryDocumentSnapshot snapshot){
        this.desc = snapshot.getString(AppliedCandidate.DESC);
        this.chatId = snapshot.getString(AppliedCandidate.CHAT_ID);
        this.isAccepted = snapshot.getBoolean(AppliedCandidate.IS_ACCEPTED);
        this.dateCreated = snapshot.getTimestamp(AppliedCandidate.DATE_CREATED);
        this.dateUpdated = snapshot.getTimestamp(AppliedCandidate.DATE_UPDATED);
        this.uid = snapshot.getId();
    }

    public AppliedCandidate(Boolean isAccepted, String desc, String chatId) {
        this.isAccepted = isAccepted;
        this.desc = desc;
        this.chatId = chatId;
    }
}
