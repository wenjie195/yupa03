package com.vidatechft.yupa.classes;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.Timestamp;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.PropertyName;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.ServerTimestamp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import static com.vidatechft.yupa.hostFragments.HostStayMainPageFragment.TAG;

@IgnoreExtraProperties
public class Rating implements Serializable {

    public static final String URL_RATING = "rating";

    public static final String RATING = "rating";
    public static final String REVIEW = "review";
    public static final String DATE_CREATED = "dateCreated";
    public static final String DATE_UPDATED = "dateUpdated";
    public static final String LIKE_COUNT = "likeCount";

    @Exclude
    public User user;

    @Exclude
    public Favourite favourite;

    @Exclude
    public String id;

    @Exclude
    public String documentId;

    @PropertyName(RATING)
    @SerializedName(RATING)
    @Expose
    public Double rating;

    @PropertyName(REVIEW)
    @SerializedName(REVIEW)
    @Expose
    public String review;

    @PropertyName(LIKE_COUNT)
    @SerializedName(LIKE_COUNT)
    @Expose
    public Integer likeCount;

    @ServerTimestamp
    @PropertyName(DATE_UPDATED)
    @SerializedName(DATE_UPDATED)
    @Expose
    public Timestamp dateUpdated;

    @ServerTimestamp
    @PropertyName(DATE_CREATED)
    @SerializedName(DATE_CREATED)
    @Expose
    public Timestamp dateCreated;



//    public Accommodation(String accommodationName1, String accommodationName2, String accommodationName3, String accommodationName4, String accommodationName5, String imgAccommodation1, String imgAccommodation2, String imgAccommodation3, String imgAccommodation4, String imgAccommodation5) {
//        this.accommodationName1 = accommodationName1;
//        this.accommodationName2 = accommodationName2;
//        this.accommodationName3 = accommodationName3;
//        this.accommodationName4 = accommodationName4;
//        this.accommodationName5 = accommodationName5;
//        this.imgAccommodation1 = imgAccommodation1;
//        this.imgAccommodation2 = imgAccommodation2;
//        this.imgAccommodation3 = imgAccommodation3;
//        this.imgAccommodation4 = imgAccommodation4;
//        this.imgAccommodation5 = imgAccommodation5;
//    }

    public Rating(){

    }


    public Rating(AppCompatActivity parentActivity, DocumentSnapshot snapshot){
        this.id = snapshot.getId();
        try{
            this.rating = snapshot.getDouble(Rating.RATING);
            this.likeCount = snapshot.getLong(Rating.LIKE_COUNT).intValue();
        }
        catch (Exception e){
//            Toast.makeText(parentActivity,e.toString(),Toast.LENGTH_SHORT).show();
            Log.d(TAG,e.toString());
        }
        this.review = snapshot.getString(Rating.REVIEW);
        this.dateCreated = snapshot.getTimestamp(Rating.DATE_CREATED);
        this.user = snapshot.toObject(User.class);
    }
}
