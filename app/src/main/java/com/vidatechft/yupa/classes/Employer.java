package com.vidatechft.yupa.classes;

import com.google.firebase.Timestamp;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.PropertyName;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.ServerTimestamp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@IgnoreExtraProperties
public class Employer implements Serializable {

    public static final String URL_EMPLOYER = "employer";
    public static final String URL_EMPLOYER_DETAILS = "employerDetails";
    public static final String URL_EMPLOYER_LICENSE = "license";

    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String NRIC = "nric";
    public static final String EMAIL = "email";
    public static final String CONTACT = "contactNo";
    public static final String LICENSE_URL = "licenseUrl";
    public static final String IS_APPROVED = "isApproved";
    public static final String DATE_CREATED = "dateCreated";

    @PropertyName(ID)
    @SerializedName(ID)
    @Expose
    public String id;

    @PropertyName(NAME)
    @SerializedName(NAME)
    @Expose
    public String name;

    @PropertyName(NRIC)
    @SerializedName(NRIC)
    @Expose
    public String nric;

    @PropertyName(EMAIL)
    @SerializedName(EMAIL)
    @Expose
    public String email;

    @PropertyName(CONTACT)
    @SerializedName(CONTACT)
    @Expose
    public String contactNo;

    @PropertyName(LICENSE_URL)
    @SerializedName(LICENSE_URL)
    @Expose
    public String licenseUrl;

    @PropertyName(IS_APPROVED)
    @SerializedName(IS_APPROVED)
    @Expose
    public boolean isApproved;

    @ServerTimestamp
    @PropertyName(DATE_CREATED)
    @SerializedName(DATE_CREATED)
    @Expose
    public Timestamp dateCreated; // from here https://stackoverflow.com/questions/46254266/firebase-no-properties-to-serialize-found-on-class

    public Employer(){

    }

    public Employer(String id, String name, String nric, String email, String contactNo, String licenseUrl) {
        this.id = id;
        this.name = name;
        this.nric = nric;
        this.email = email;
        this.contactNo = contactNo;
        this.licenseUrl = licenseUrl;
    }
}