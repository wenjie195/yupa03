package com.vidatechft.yupa.classes;

import android.content.Context;

import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.load.engine.cache.LruResourceCache;
import com.bumptech.glide.load.engine.cache.MemorySizeCalculator;
import com.bumptech.glide.module.AppGlideModule;
import com.bumptech.glide.request.target.ViewTarget;
import com.vidatechft.yupa.R;

@GlideModule
public final class Glide extends AppGlideModule {

   //Needed the class for GlideApp using

    @Override
    public void applyOptions(Context context, GlideBuilder builder) {
        // Apply options to the builder here.
//        builder.setDiskCache(
//                new InternalCacheDiskCacheFactory(context, 20*1024));
        builder.setMemoryCache(new LruResourceCache(10 * 1024 * 1024));
//        builder.setDefaultRequestOptions(new RequestOptions().format(DecodeFormat.PREFER_RGB_565  ));
//        RequestOptions requestOptions = new RequestOptions();
//        requestOptions.format(DecodeFormat.PREFER_RGB_565);
//        requestOptions.disallowHardwareConfig();
//        builder.setDefaultRequestOptions(requestOptions);

        MemorySizeCalculator calculator = new MemorySizeCalculator.Builder(context)
                .setMemoryCacheScreens(2)
                .build();
        builder.setMemoryCache(new LruResourceCache(calculator.getMemoryCacheSize()));


        ViewTarget.setTagId(R.id.glide_tag); //If you want use setTag() and getTag(), this prevent the application crash when logout
        //Because the glide will used it own glide view
    }

    @Override
    public boolean isManifestParsingEnabled() {
        return false;
    }
}
