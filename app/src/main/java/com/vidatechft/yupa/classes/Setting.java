package com.vidatechft.yupa.classes;

import android.support.v7.app.AppCompatActivity;

import com.google.firebase.Timestamp;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.PropertyName;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.ServerTimestamp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

@IgnoreExtraProperties
public class Setting {
    public static final String URL_SETTING = "setting";
    public static final String SETTING_DETAILS = "settingDetails";
    public static final String DIR_SETTING= "dirSetting";
    public static final String IS_DRAFT= "isDraft";

    public static final String GUEST_MOVE_IN= "guestMoveIn";
    public static final String GUEST_GET_DISCOUNTS= "guestGetDiscount";
    public static final String GUEST_GET_QUALIFY= "guestGetQualify";

    public static final String approvalType= "approvalType";
    public static final String guestGender= "guestGender";

    public static final String APPROVAL_TYPE= "approvalType";
    public static final String GUEST_GENDER= "guestGender";
    public static final String SHOULD_GUEST_MESSAGE_YOU= "shouldGuestMessageYou";
    public static final String SHOULD_GUEST_UPLOAD_IDENTITY= "shouldGuestUploadIdentity";
    public static final String WANT_BOOST_ACCOMMODATION= "wantBoostAccommodation";
    public static final String PROVIDE_DISCOUNT= "provideDiscount";

    public static final String myApproval= "my approval";
    public static final String payment= "payment";
    public static final String shouldGuestMessageYou= "shouldGuestMessageYou";
    public static final String shouldGuestUploadIdentity= "shouldGuestUploadIdentity";
    public static final String guestFemale= "female";
    public static final String guestMale= "male";
    public static final String guestBoth= "both";
    public static final String wantBoostAccommodation= "wantBoostAccommodation";
    public static final String provideDiscount= "provideDiscount";

    public static final String DATE_CREATED= "dateCreated";
    public static final String DATE_UPDATED= "dateUpdated";

    @PropertyName(APPROVAL_TYPE)
    @SerializedName(APPROVAL_TYPE)
    @Expose
    public String type;

    @PropertyName(SHOULD_GUEST_MESSAGE_YOU)
    @SerializedName(SHOULD_GUEST_MESSAGE_YOU)
    @Expose
    public Boolean shouldMessageYou;

    @PropertyName(SHOULD_GUEST_UPLOAD_IDENTITY)
    @SerializedName(SHOULD_GUEST_UPLOAD_IDENTITY)
    @Expose
    public Boolean shouldUploadIdentity;

    @PropertyName(GUEST_GENDER)
    @SerializedName(GUEST_GENDER)
    @Expose
    public String gender;

    @PropertyName(WANT_BOOST_ACCOMMODATION)
    @SerializedName(WANT_BOOST_ACCOMMODATION)
    @Expose
    public Boolean boost;

    @PropertyName(PROVIDE_DISCOUNT)
    @SerializedName(PROVIDE_DISCOUNT)
    @Expose
    public Boolean discount;

    @PropertyName(GUEST_MOVE_IN)
    @SerializedName(GUEST_MOVE_IN)
    @Expose
    public String guestMoveIn;

    @PropertyName(GUEST_GET_DISCOUNTS)
    @SerializedName(GUEST_GET_DISCOUNTS)
    @Expose
    public String guestGetDiscount;

    @PropertyName(GUEST_GET_QUALIFY)
    @SerializedName(GUEST_GET_QUALIFY)
    @Expose
    public String guestGetQualify;

    @ServerTimestamp
    @PropertyName(DATE_CREATED)
    @SerializedName(DATE_CREATED)
    @Expose
    public Timestamp dateCreated;


    @ServerTimestamp
    @PropertyName(DATE_UPDATED)
    @SerializedName(DATE_UPDATED)
    @Expose
    public Timestamp dateUpdated;

    public Setting(){

    }

    public Setting(AppCompatActivity parentActivity, QueryDocumentSnapshot snapshot){

        this.type = snapshot.getString(Setting.APPROVAL_TYPE);
        this.shouldMessageYou = snapshot.getBoolean(Setting.SHOULD_GUEST_MESSAGE_YOU);
        this.shouldUploadIdentity = snapshot.getBoolean(Setting.SHOULD_GUEST_UPLOAD_IDENTITY);
        this.gender = snapshot.getString(Setting.GUEST_GENDER);
        this.boost = snapshot.getBoolean(Setting.WANT_BOOST_ACCOMMODATION);
        this.discount = snapshot.getBoolean(Setting.PROVIDE_DISCOUNT);

        this.guestMoveIn = snapshot.getString(Setting.GUEST_MOVE_IN);
        this.guestGetDiscount = snapshot.getString(Setting.GUEST_GET_DISCOUNTS);
        this.guestGetQualify = snapshot.getString(Setting.GUEST_GET_QUALIFY);
        this.dateCreated = snapshot.getTimestamp(Setting.DATE_CREATED);
        this.dateUpdated = snapshot.getTimestamp(Setting.DATE_UPDATED);
    }

    public static HashMap<String,Object> classToMapString(Setting setting, int section){
        HashMap<String,Object> settingMap = new HashMap<>();

        if(section == 0){
            settingMap.put(APPROVAL_TYPE,setting.type);
            settingMap.put(GUEST_GENDER,setting.gender);
        }
        if(section ==1){
//        settingMap.put(GUEST_MOVE_IN,setting.guestMoveIn);
        settingMap.put(GUEST_GET_DISCOUNTS,setting.guestGetDiscount);
        settingMap.put(GUEST_GET_QUALIFY,setting.guestGetQualify);
        }

        return settingMap;
    }

    public static Setting mapToClassString(Map<String,Object> settingMap, AppCompatActivity parentActivity, int section){
        Setting setting = new Setting();
        if(section == 0){
            setting.type = (String) settingMap.get(APPROVAL_TYPE);
            setting.gender = (String) settingMap.get(GUEST_GENDER);
        }
        if(section == 1){
//            setting.guestMoveIn = (String) settingMap.get(GUEST_MOVE_IN);
            setting.guestGetDiscount = (String) settingMap.get(GUEST_GET_DISCOUNTS);
            setting.guestGetQualify = (String) settingMap.get(GUEST_GET_QUALIFY);

        }
        return setting;
    }

    public static HashMap<String,Object> classToMapBoolean(Setting setting){
        HashMap<String,Object> settingMap = new HashMap<>();

        settingMap.put(SHOULD_GUEST_MESSAGE_YOU,setting.shouldMessageYou);
        settingMap.put(SHOULD_GUEST_UPLOAD_IDENTITY,setting.shouldUploadIdentity);
        settingMap.put(WANT_BOOST_ACCOMMODATION,setting.boost);
        settingMap.put(PROVIDE_DISCOUNT,setting.discount);

        return settingMap;
    }

    public static Setting mapToClassBoolean(Map<String,Object> settingMap, AppCompatActivity parentActivity){
        Setting setting = new Setting();
        setting.shouldMessageYou = (Boolean) settingMap.get(SHOULD_GUEST_MESSAGE_YOU);
        setting.shouldUploadIdentity = (Boolean) settingMap.get(SHOULD_GUEST_UPLOAD_IDENTITY);
        setting.boost = (Boolean) settingMap.get(WANT_BOOST_ACCOMMODATION);
        setting.discount = (Boolean) settingMap.get(PROVIDE_DISCOUNT);

        return setting;
    }
}
