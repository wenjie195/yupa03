package com.vidatechft.yupa.classes.PlaceSearchRecommendedGoogleApi;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class PlaceSearchRecommendedResult {
    @SerializedName("next_page_token")
    public String nextPageToken;

    @SerializedName("results")
    public ArrayList<PlaceRecommendedResult> recommendedResults;

    @SerializedName("status")
    public String status;

    public PlaceSearchRecommendedResult(){

    }

    public PlaceSearchRecommendedResult(String nextPageToken, ArrayList<PlaceRecommendedResult> recommendedResults, String status) {
        this.nextPageToken = nextPageToken;
        this.recommendedResults = recommendedResults;
        this.status = status;
    }
}
