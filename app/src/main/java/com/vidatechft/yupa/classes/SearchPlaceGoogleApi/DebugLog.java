package com.vidatechft.yupa.classes.SearchPlaceGoogleApi;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DebugLog {
    @SerializedName("line")
    public ArrayList<String> line;

    public DebugLog(){

    }

    public DebugLog(ArrayList<String> line) {
        this.line = line;
    }
}
