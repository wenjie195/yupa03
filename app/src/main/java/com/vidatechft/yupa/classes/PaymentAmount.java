package com.vidatechft.yupa.classes;

import com.braintreepayments.api.dropin.utils.PaymentMethodType;
import com.braintreepayments.api.models.PaymentMethodNonce;
import com.google.firebase.Timestamp;
import com.google.firebase.database.PropertyName;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.ServerTimestamp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vidatechft.yupa.activities.MainActivity;

import java.util.HashMap;

public class PaymentAmount {

    public static final String KEY_ROOM_EACH = "roomEach";//AKA accomodation
    public static final String KEY_ROOM_NOOF_NIGHTS = "roomNoofNights";
    public static final String KEY_ROOM_TOTAL = "roomTotal";
    public static final String KEY_ITINERARY = "itinerary";
    public static final String KEY_TRAVEL_KIT_MAP = "travelKitMap";
    public static final String KEY_SERVICE_AND_TAX= "serviceAndTax";
    public static final String KEY_GRAND_TOTAL = "grandTotal";
    public static final String KEY_CURRENCY_CODE = "currencyCode";

    @PropertyName(KEY_ROOM_EACH)
    @SerializedName(KEY_ROOM_EACH)
    @Expose
    public Double roomEach;

    @PropertyName(KEY_ROOM_NOOF_NIGHTS)
    @SerializedName(KEY_ROOM_NOOF_NIGHTS)
    @Expose
    public Double roomNoofNights;

    @PropertyName(KEY_ROOM_TOTAL)
    @SerializedName(KEY_ROOM_TOTAL)
    @Expose
    public Double roomTotal;

    @PropertyName(KEY_ITINERARY)
    @SerializedName(KEY_ITINERARY)
    @Expose
    public Double itinerary;

    @PropertyName(KEY_TRAVEL_KIT_MAP)
    @SerializedName(KEY_TRAVEL_KIT_MAP)
    @Expose
    public HashMap<String,Double> travelKitMap;

    @PropertyName(KEY_SERVICE_AND_TAX)
    @SerializedName(KEY_SERVICE_AND_TAX)
    @Expose
    public Double serviceAndTax;

    @PropertyName(KEY_GRAND_TOTAL)
    @SerializedName(KEY_GRAND_TOTAL)
    @Expose
    public Double grandTotal;

    @PropertyName(KEY_CURRENCY_CODE)
    @SerializedName(KEY_CURRENCY_CODE)
    @Expose
    public String currencyCode;
}
