package com.vidatechft.yupa.merchantShopFragment;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ServerValue;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.BusinessHourTime;
import com.vidatechft.yupa.classes.Merchant;
import com.vidatechft.yupa.classes.PlaceDetailsGoogleApi.PlaceAddress;
import com.vidatechft.yupa.retrofitNetwork.UnifyPlaceAddress;
import com.vidatechft.yupa.utilities.Config;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.FilePath;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static android.app.Activity.RESULT_OK;
import static com.vidatechft.yupa.merchantShopFragment.BusinessHourDialogFragment.NUM_OF_WEEKDAYS;
import static com.vidatechft.yupa.utilities.GeneralFunction.GlideImageSetting;

public class ApplyAsMerchantFragment extends Fragment implements View.OnClickListener {
    public static final String TAG = ApplyAsMerchantFragment.class.getName();
    private MainActivity parentActivity;
    private View view;
    private AppCompatEditText merchantNameET,merchantIcET,ssmLicenseNumberET,emailET,contactNumberET,socialMediaET,
            descBusinessET,industryET;
    private AppCompatTextView workPlaceTV,workPlaceAddTV,submitTV;
    private TextView addBHTV;
    private ListView listViewSpinnerItem;
    private ImageButton uploadLicenseIB, uploadShopIB;
    private Button btn_saveAsDraft;

    private ConstraintLayout constraintLayoutSpinnerItem;
    private String shopPicPath, licensePicPath,workPlace,workPlaceAdd;

    private int imageButton = 0;
    private GeoPoint getGeoPoint;

    private Place place;
    public static boolean isOpen = true;
    private DocumentReference merchantRef;

    private Uri shopPhotoUri,licenseUri;

    private SparseArray<ArrayList<BusinessHourTime>> businessHourTimes;
    private boolean isSaveDraft = false;

    private Merchant merchant;
    private HashMap<String,Object> merchantMap;
    private Map<String, Object> licenseMap = new HashMap<>();
    private boolean isEditMode;
    private boolean hasAlreadyPopulatedOldData = false;

    private List<Boolean> uploadedAllPic = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static ApplyAsMerchantFragment newInstance(MainActivity parentActivity, Merchant merchant) {
        ApplyAsMerchantFragment fragment = new ApplyAsMerchantFragment();

        fragment.parentActivity = parentActivity;
        fragment.merchant = merchant;
        if(merchant != null){
            fragment.isEditMode = true;
        }

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_apply_as_merchant, container, false);
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                initView();

                if(isEditMode){
                    populateOldData();
                }else{
                    if(businessHourTimes!=null){
                        formatBusinessHours(businessHourTimes);
                    }
                }

                //got 2 pictures to upload
                uploadedAllPic.clear();
                uploadedAllPic.add(false);
                uploadedAllPic.add(false);
            }
        });

        return view;
    }

    private void initView(){
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                merchantNameET = view.findViewById(R.id.merchantNameET);
                merchantIcET = view.findViewById(R.id.merchantIcET);
                workPlaceTV = view.findViewById(R.id.workPlaceTV);
                workPlaceAddTV = view.findViewById(R.id.workPlaceAddTV);
                ssmLicenseNumberET = view.findViewById(R.id.ssmLicenseNumberET);
                emailET = view.findViewById(R.id.emailET);
                contactNumberET = view.findViewById(R.id.contactNumberET);
                socialMediaET = view.findViewById(R.id.socialMediaET);
                descBusinessET = view.findViewById(R.id.descBusinessET);
                uploadLicenseIB = view.findViewById(R.id.uploadLicenseIB);
                uploadShopIB = view.findViewById(R.id.uploadShopIB);
                addBHTV = view.findViewById(R.id.addBHTV);

                industryET = view.findViewById(R.id.industryET);
                listViewSpinnerItem = view.findViewById(R.id.listViewSpinnerItem);
                constraintLayoutSpinnerItem = view.findViewById(R.id.constraintLayoutSpinnerItem);

                submitTV = view.findViewById(R.id.submitTV);
                btn_saveAsDraft = view.findViewById(R.id.btn_saveAsDraft);
                if(isEditMode){
                    submitTV.setText(parentActivity.getString(R.string.update));

                    if(merchant != null && merchant.shopStatus != null && TextUtils.equals(merchant.shopStatus,Merchant.STATUS_ACCEPTED)){
                        btn_saveAsDraft.setText(parentActivity.getString(R.string.publish));
                        if(merchant.isPublish != null){
                            if(merchant.isPublish){
                                btn_saveAsDraft.setText(parentActivity.getString(R.string.unpublish));
                            }
                        }
                    }else{
                        btn_saveAsDraft.setText(parentActivity.getString(R.string.saveasdraft));
                    }
                }else{
                    submitTV.setText(parentActivity.getString(R.string.btn_submit));
                    btn_saveAsDraft.setText(parentActivity.getString(R.string.saveasdraft));
                }

                constraintLayoutSpinnerItem.setOnClickListener(ApplyAsMerchantFragment.this);
                uploadLicenseIB.setOnClickListener(ApplyAsMerchantFragment.this);
                uploadShopIB.setOnClickListener(ApplyAsMerchantFragment.this);
                submitTV.setOnClickListener(ApplyAsMerchantFragment.this);
                btn_saveAsDraft.setOnClickListener(ApplyAsMerchantFragment.this);
                industryET.setOnClickListener(ApplyAsMerchantFragment.this);

                workPlaceTV.setOnClickListener(new DebouncedOnClickListener(500) {
                    @Override
                    public void onDebouncedClick(View v) {
                        GeneralFunction.openPlacePicker(parentActivity,ApplyAsMerchantFragment.this);
                    }
                });

                workPlaceAddTV.setOnClickListener(new DebouncedOnClickListener(500) {
                    @Override
                    public void onDebouncedClick(View v) {
                        GeneralFunction.openPlacePicker(parentActivity,ApplyAsMerchantFragment.this);
                    }
                });

                addBHTV.setOnClickListener(new DebouncedOnClickListener(500) {
                    @Override
                    public void onDebouncedClick(View v) {
                        BusinessHourDialogFragment businessHourDialogFragment = BusinessHourDialogFragment.newInstance(parentActivity,ApplyAsMerchantFragment.this,businessHourTimes);
                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, businessHourDialogFragment).addToBackStack(BusinessHourDialogFragment.TAG).commit();
                    }
                });
            }
        });
    }

    private void populateOldData(){
        //to fix bug of below using hasAlreadyPopulatedOldData
        //in edit mode, select a new location
        //go to business hour fragment
        //go back to apply merchant fragment, the workplace variable will still retain the old value
        //workplace address is ok because im using the place's value, but not for workplaceTV it is using workplace variable

        if(!hasAlreadyPopulatedOldData){
            if(merchant.name != null){
                merchantNameET.setText(merchant.name);
            }

            if(merchant.nric != null){
                merchantIcET.setText(merchant.nric);
            }

            if(merchant.gps != null){
                getGeoPoint = merchant.gps;

                if(merchant.workPlace != null){
                    workPlaceTV.setText(merchant.workPlace);
                    workPlaceTV.requestLayout();
                    workPlace = merchant.workPlace;
                }

                if(merchant.workPlaceAdd != null){
                    workPlaceAddTV.setText(merchant.workPlaceAdd);
                    workPlaceAddTV.requestLayout();
                    workPlaceAdd = merchant.workPlaceAdd;
                }
            }

            if(merchant.ssmLicense != null){
                ssmLicenseNumberET.setText(merchant.ssmLicense);
            }

            if(merchant.email != null){
                emailET.setText(merchant.email);
            }

            if(merchant.contactNo != null){
                contactNumberET.setText(merchant.contactNo);
            }

            if(merchant.socialMedia != null){
                socialMediaET.setText(merchant.socialMedia);
            }

            if(merchant.fieldOfIndustry != null){
                industryET.setText(merchant.fieldOfIndustry);
            }

            if(merchant.description != null){
                descBusinessET.setText(merchant.description);
            }
        }

        //dont include these inside the if else though because the content will disappear if came back from other fragments
        if(merchant.merchantLicensePicUrl != null){
            GlideImageSetting(parentActivity,Uri.parse(merchant.merchantLicensePicUrl),uploadLicenseIB);
        }

        if(merchant.shopPicUrl != null){
            GlideImageSetting(parentActivity,Uri.parse(merchant.shopPicUrl),uploadShopIB);
        }

        if(merchant.businessHourTimes != null){
            businessHourTimes = merchant.businessHourTimes;
            formatBusinessHours(merchant.businessHourTimes);
        }

        hasAlreadyPopulatedOldData = true;
    }

    public void setBusinessHours(SparseArray<ArrayList<BusinessHourTime>> businessHourTimes){
        this.businessHourTimes = businessHourTimes;
        if(isEditMode){
            merchant.businessHourTimes = businessHourTimes;
        }
        formatBusinessHours(businessHourTimes);
    }

    private void formatBusinessHours(SparseArray<ArrayList<BusinessHourTime>> businessHourTimes){
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < NUM_OF_WEEKDAYS; i++){
            switch (i){
                case 0:
                    stringBuilder.append("<b>Monday</b> </br>");
                    break;
                case 1:
                    stringBuilder.append("<b> <br> Tuesday </b> </br> ");
                    break;
                case 2:
                    stringBuilder.append("<b> <br> Wednesday </b> </br>");
                    break;
                case 3:
                    stringBuilder.append("<b> <br> Thursday </b> </br>");
                    break;
                case 4:
                    stringBuilder.append("<b> <br> Friday </b> </br>");
                    break;
                case 5:
                    stringBuilder.append("<b> <br> Saturday </b> </br>");
                    break;
                case 6:
                    stringBuilder.append("<b> <br> Sunday </b> </br>");
                    break;
            }


            if(businessHourTimes.get(i) != null && !businessHourTimes.get(i).isEmpty()){
                for(BusinessHourTime tempBHT: businessHourTimes.get(i)){
                        stringBuilder.append("<p>");
                        int tempOpenHour = tempBHT.getOpenHour();
                        int tempOpenMin = tempBHT.getOpenMin();
                        stringBuilder.append(String.format(Locale.US,"%02d:%02d %s", tempOpenHour == 12 || tempOpenHour == 0 ? 12 : tempOpenHour % 12, tempOpenMin, tempOpenHour < 12 ? "am" : "pm")+ "&#160;&#160");

                        stringBuilder.append(" - " );
                        stringBuilder.append(" &#160 " );
                        int tempCloseHour = tempBHT.getCloseHour();
                        int tempCloseMin = tempBHT.getCloseMin();
                        stringBuilder.append(String.format(Locale.US,"%02d:%02d %s", tempCloseHour == 12 || tempCloseHour == 0 ? 12 : tempCloseHour % 12, tempCloseMin, tempCloseHour < 12 ? "am" : "pm")+ "&#160;&#160");
                        stringBuilder.append("<br>");

                }
            }else {
                stringBuilder.append("<br> Closed <br>");
            }


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                addBHTV.setText(Html.fromHtml(stringBuilder.toString(), Html.FROM_HTML_MODE_COMPACT));
            }else{
                addBHTV.setText(Html.fromHtml(stringBuilder.toString()));
            }
        }
    }

    private boolean isValidInput(String name, String nric,String workPlace,String workPlaceAdd, String ssmLicense, String email, String contactNo, String socialMedia,String description, String fieldOfIndustry){
        return  GeneralFunction.checkAndReturnString(name,parentActivity.getString(R.string.error_please_enter_name),parentActivity)
                &&
                GeneralFunction.checkAndReturnString(nric,parentActivity.getString(R.string.error_please_enter_ic),parentActivity)
                &&
                GeneralFunction.checkAndReturnString(workPlaceAdd,parentActivity.getString(R.string.error_please_enter_work_place_address),parentActivity)
                &&
                GeneralFunction.checkAndReturnString(ssmLicense,parentActivity.getString(R.string.error_please_enter_ssm),parentActivity)
                &&
                GeneralFunction.isValidEmail(email,parentActivity)
                &&
                GeneralFunction.checkAndReturnString(contactNo,parentActivity.getString(R.string.error_please_enter_contact),parentActivity)
                &&
                GeneralFunction.checkAndReturnString(socialMedia,parentActivity.getString(R.string.error_please_enter_social_media),parentActivity)
                &&
                GeneralFunction.checkAndReturnString(description,parentActivity.getString(R.string.error_please_enter_description),parentActivity)
                &&
                GeneralFunction.checkAndReturnString(fieldOfIndustry,parentActivity.getString(R.string.error_please_enter_field_of_industry),parentActivity);


    }

    private void applyAsMerchant(){
        String name = merchantNameET.getText().toString().trim();
        String nric = merchantIcET.getText().toString().trim();
        workPlace = workPlaceTV.getText().toString();
        workPlaceAdd = workPlaceAddTV.getText().toString();
        if(place != null){
            getGeoPoint = new GeoPoint(place.getLatLng().latitude,place.getLatLng().longitude);
        }

        if(getGeoPoint == null && !isSaveDraft){
            GeneralFunction.checkAndReturnString(workPlace,parentActivity.getString(R.string.error_location_picker),parentActivity);
            return;
        }

        String ssmLicense = ssmLicenseNumberET.getText().toString().trim();
        String email = emailET.getText().toString().trim();
        String contactNo = contactNumberET.getText().toString().trim();
        String socialMedia = socialMediaET.getText().toString();
        String description = descBusinessET.getText().toString();
        String fieldOfIndustry = industryET.getText().toString();

        if (licensePicPath == null && !isSaveDraft){
            if(isEditMode && merchant.merchantLicensePicUrl != null){

            }else{
                GeneralFunction.checkAndReturnString(licensePicPath,parentActivity.getString(R.string.error_please_upload_driving_license),parentActivity);
                return;
            }
        }

        if(shopPicPath == null && !isSaveDraft){
            if(isEditMode && merchant.shopPicUrl != null){

            }else{
                GeneralFunction.checkAndReturnString(shopPicPath,parentActivity.getString(R.string.error_please_upload_shop_photo),parentActivity);
                return;
            }
        }

        if(isSaveDraft){
            saveMerchant(name, nric, workPlace, workPlaceAdd, contactNo, email, socialMedia, fieldOfIndustry,description,ssmLicense, shopPicPath, licensePicPath, getGeoPoint);
        }else{
            if (isValidInput(name, nric,workPlace, workPlaceAdd, ssmLicense, email, contactNo, socialMedia,description, fieldOfIndustry)) {
                saveMerchant(name, nric, workPlace, workPlaceAdd, contactNo, email, socialMedia, fieldOfIndustry,description,ssmLicense, shopPicPath, licensePicPath, getGeoPoint);
            }
        }
    }

    private void saveMerchant(String name, String nric, String workPlace, String workPlaceAdd, String contactNo, String email, String socialMedia, String fieldOfIndustry, String description, String ssmLicense, String getLicensePath, String licensePath, GeoPoint getGeoPoint) {
        merchantMap = new HashMap<>();
        merchantMap.put(Merchant.MERCHANT_ID, parentActivity.uid);
        merchantMap.put(Merchant.MERCHANT_NAME, name);
        merchantMap.put(Merchant.MERCHANT_NRIC, nric);
        merchantMap.put(Merchant.MERCHANT_WORKPLACE, workPlace);
        merchantMap.put(Merchant.MERCHANT_WORKPLACE_ADDRESS, workPlaceAdd);
        merchantMap.put(Merchant.MERCHANT_CONTACT_NO, contactNo);
        merchantMap.put(Merchant.MERCHANT_EMAIL, email);
        merchantMap.put(Merchant.MERCHANT_SOCIAL_MEDIA, socialMedia);
        merchantMap.put(Merchant.MERCHANT_FIELD_OF_INDUSTRY, fieldOfIndustry);
        merchantMap.put(Merchant.MERCHANT_DESC, description);
        merchantMap.put(Merchant.MERCHANT_SSM, ssmLicense);
        merchantMap.put(Merchant.GPS, getGeoPoint);
        merchantMap.put(Merchant.SHOP_STATUS,Merchant.STATUS_PENDING);
        merchantMap.put(Merchant.IS_PUBLISH,false);

        if(!isEditMode){
            merchantMap.put(Merchant.DATE_CREATED, FieldValue.serverTimestamp());
        }
        merchantMap.put(Merchant.DATE_UPDATED, FieldValue.serverTimestamp());

        if(isSaveDraft){
            merchantMap.put(Merchant.IS_DRAFT, true);
        }
        else{
            merchantMap.put(Merchant.IS_DRAFT, false);
        }

        if(merchantRef == null){
            if(isEditMode){
                merchantRef = GeneralFunction.getFirestoreInstance()
                        .collection(Merchant.URL_MERCHANT)
                        .document(merchant.id);
            }else{
                merchantRef = GeneralFunction.getFirestoreInstance()
                        .collection(Merchant.URL_MERCHANT)
                        .document();
            }
        }

        uploadedAllPic.clear();
        uploadedAllPic.add(false);
        uploadedAllPic.add(false);

        parentActivity.initializeProgressIndicator(parentActivity.getString(R.string.merchant_upload_detail),false,this);
        parentActivity.controlProgressIndicator(true,this);

        if(place != null && place.getId() != null){
            getPlaceDetails();
        }else{
            uploadLicensePic();
            uploadShopPic();
        }
    }

    private void getPlaceDetails(){
        new UnifyPlaceAddress(parentActivity, place.getId(), new UnifyPlaceAddress.OnUnifyComplete() {
            @Override
            public void onUnifySuccess(PlaceAddress placeAddress) {
                merchantMap.put(Merchant.MERCHANT_PLACE_ADDRESS,PlaceAddress.getHashmap(placeAddress,place.getId()));
                merchantMap.put(Merchant.MERCHANT_PLACE_INDEX,PlaceAddress.getPlaceIndex(placeAddress));
                parentActivity.updateProgress(30,ApplyAsMerchantFragment.this);
                uploadLicensePic();
                uploadShopPic();
            }

            @Override
            public void onUnifyFailed(Exception e) {
                GeneralFunction.handleUploadError(parentActivity,ApplyAsMerchantFragment.this,R.string.error_occured_getting_place_details,TAG);
                e.printStackTrace();
            }
        });
    }

    private void uploadLicensePic(){
        if(licensePicPath != null ){
            final StorageReference ref = GeneralFunction.getFirebaseStorageInstance()
                    .child(Merchant.URL_MERCHANT)
                    .child(parentActivity.uid)
                    .child(Merchant.DRIVER_LICENSE_PIC_URL)
                    .child(merchantRef.getId())
                    .child(Merchant.URL_MERCHANT_DRIVER_LICENSE_PIC);

            UploadTask uploadTask = ref.putBytes(GeneralFunction.getCompressedBitmap(parentActivity, licensePicPath));
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                    handleOnEachPicUploaded(1);
                    GeneralFunction.handleUploadError(parentActivity,ApplyAsMerchantFragment.this,R.string.merchant_err_posting_details,TAG);
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri downloadUri) {
                            if(downloadUri != null){
                                licenseMap.put(Merchant.DRIVER_LICENSE_PIC_URL, downloadUri.toString());
                                licenseMap.put(Merchant.DATE_CREATED, FieldValue.serverTimestamp());
                            }
                            handleOnEachPicUploaded(1);
                        }
                    });

                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    // + 10 is because i set the progress to 10 when it finished saving the initial firebase data
                    // * 80 is because i want the progress to go to maximum 90% only when the image is finished uploaded
                    // so that the final 10% is set when the firebase updates the imageUrl data as the final step

                    int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * (80 - getAddonProgress())) + 10 + getAddonProgress();
                    parentActivity.updateProgress((float) totalProgress,ApplyAsMerchantFragment.this);
                }
            });//end addOnprogresslistener
        }else{
            handleOnEachPicUploaded(1);
            if(!isSaveDraft && !isEditMode){
                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_upload_driving_license),R.drawable.notice_bad,TAG);
            }
        }
    }

    private void uploadShopPic(){
        if(shopPicPath != null ){
            final StorageReference ref = GeneralFunction.getFirebaseStorageInstance()
                    .child(Merchant.URL_MERCHANT+"/"+parentActivity.uid+"/"+Merchant.URL_MERCHANT_SHOP_PIC +"/"+ merchantRef.getId() +"/"+Merchant.URL_SHOP_PIC);

            UploadTask uploadTask = ref.putBytes(GeneralFunction.getCompressedBitmap(parentActivity, shopPicPath));
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                    handleOnEachPicUploaded(0);
                    GeneralFunction.handleUploadError(parentActivity,ApplyAsMerchantFragment.this,R.string.merchant_err_posting_details,TAG);
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                    ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri downloadUri) {
                            if(downloadUri != null){
                                merchantMap.put(Merchant.URL_MERCHANT_SHOP_PIC, downloadUri.toString());
                            }
                            handleOnEachPicUploaded(0);
                        }
                    });
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    // + 10 is because i set the progress to 10 when it finished saving the initial firebase data
                    // * 80 is because i want the progress to go to maximum 90% only when the image is finished uploaded
                    // so that the final 10% is set when the firebase updates the imageUrl data as the final step
                    int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * (80 - getAddonProgress())) + 10 + getAddonProgress();
                    parentActivity.updateProgress((float) totalProgress,ApplyAsMerchantFragment.this);
                }
            });
        }else{
            handleOnEachPicUploaded(0);
            if(!isSaveDraft && !isEditMode){
                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_upload_shop_photo),R.drawable.notice_bad,TAG);
            }
        }
    }

    private int getAddonProgress(){
        int addProgress = 40;
        if(uploadedAllPic.get(0) && uploadedAllPic.get(1)){
            addProgress = 80;
        }
        return addProgress;
    }

    private void handleOnEachPicUploaded(int index){
        uploadedAllPic.set(index,true);

        for(boolean isThisFinished : uploadedAllPic){
            if(!isThisFinished){
                return;
            }
        }

        setMerchantDetails();
    }

    private void setMerchantDetails() {
        if(isEditMode){
            merchantRef.update(merchantMap).addOnCompleteListener(getOnCompleteListener());
        }else{
            merchantRef.set(merchantMap).addOnCompleteListener(getOnCompleteListener());
        }
    }

    private OnCompleteListener<Void> getOnCompleteListener() {
        return new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.getException() != null){
                    //not null = have error
//                    GeneralFunction.openMessageNotificationDialog(parentActivity,task.getException().toString(),R.drawable.notice_bad,TAG);
                    GeneralFunction.handleUploadError(parentActivity,ApplyAsMerchantFragment.this,R.string.merchant_err_posting_details,TAG);
                }else{
                    GeneralFunction.handleUploadSuccess(parentActivity,ApplyAsMerchantFragment.this,R.string.merchant_upload_success,TAG);
                    //NOTE*** if got crash, then it might be because of this
                    //Internal error in Firestore (0.6.6-dev).     View was refilled using docs that themselves needed refilling.
                    //not sure why it happen but when you set, everything is saved successfully but it will crash
                    //this crash happens when a collection is written at the same time as the inner collection (happens when i update subcollection of license only, not on business hour subcollection)
                    //FIXED by deleting proof subcollection then only set it

                    if(licenseMap != null && licenseMap.get(Merchant.DRIVER_LICENSE_PIC_URL) != null){
                        merchantRef.collection(Merchant.URL_SHOP_PROOF)
                                .document(Merchant.URL_SHOP_PROOF)
                                .delete()
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        merchantRef.collection(Merchant.URL_SHOP_PROOF)
                                                .document(Merchant.URL_SHOP_PROOF)
                                                .set(licenseMap);
                                    }
                                });
                    }

                    if(businessHourTimes != null && businessHourTimes.size() > 0){
                        BusinessHourDialogFragment.pushDatabase(merchantRef,businessHourTimes);
                    }
                }
            }
        };
    }

    private void showPublishConfirmDialog(final boolean isPublish){
        String title, message;

        if(isPublish){
            title = parentActivity.getString(R.string.publish);
            message = parentActivity.getString(R.string.message_publish_shop);
        }else{
            title = parentActivity.getString(R.string.unpublish);
            message = parentActivity.getString(R.string.message_unpublish_shop);
        }

        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(parentActivity);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setPositiveButton(parentActivity.getString(R.string.btn_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                publishOrUnpublishShop(isPublish);
                dialog.dismiss();
            }
        });

        alertDialogBuilder.setNegativeButton(parentActivity.getString(R.string.btn_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        android.support.v7.app.AlertDialog alertDialog  = alertDialogBuilder.create();
        if( !( parentActivity.isFinishing() && alertDialog.isShowing() ) ){
            alertDialog.show();
        }
    }

    private void publishOrUnpublishShop(final boolean isPublish){
        final List<String> resultMsg = new ArrayList<>();

        //0 means failed
        //1 means succeed
        if(isPublish){
            resultMsg.add(parentActivity.getString(R.string.publish_failed));
            resultMsg.add(parentActivity.getString(R.string.publish_success));
        }else{
            resultMsg.add(parentActivity.getString(R.string.unpublish_failed));
            resultMsg.add(parentActivity.getString(R.string.unpublish_success));
        }

        if(merchant == null || merchant.id == null){
            GeneralFunction.openMessageNotificationDialog(parentActivity,resultMsg.get(0),R.drawable.notice_bad,TAG);
            return;
        }

        HashMap<String,Object> updateMap = new HashMap<>();
        updateMap.put(Merchant.IS_PUBLISH,isPublish);
        updateMap.put(Merchant.DATE_UPDATED,FieldValue.serverTimestamp());

        GeneralFunction.getFirestoreInstance()
                .collection(Merchant.URL_MERCHANT)
                .document(merchant.id)
                .update(updateMap)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.getException() != null){
                            Log.e(TAG,task.getException().getMessage());
                            GeneralFunction.openMessageNotificationDialog(parentActivity,resultMsg.get(0),R.drawable.notice_bad,TAG);
                        }else{
                            GeneralFunction.openMessageNotificationDialog(parentActivity,resultMsg.get(1),R.drawable.notice_bad,TAG);
                            if(btn_saveAsDraft != null){
                                if(isPublish){
                                    btn_saveAsDraft.setText(parentActivity.getString(R.string.unpublish));
                                }else{
                                    btn_saveAsDraft.setText(parentActivity.getString(R.string.publish));
                                }
                            }
                        }
                    }
                });
    }

    @Override
    public void onPause() {
        workPlace = workPlaceTV.getText().toString();
        workPlaceAdd = workPlaceAddTV.getText().toString();

        super.onPause();
    }

    @Override
    public void onResume() {
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.frag_apply_as_merchant));
                parentActivity.selectHighlights();
            }
        });

        if(licenseUri != null ){
            GlideImageSetting(parentActivity, licenseUri,uploadLicenseIB);
        }

        if(shopPhotoUri != null){
            GlideImageSetting(parentActivity, shopPhotoUri,uploadShopIB);
        }

        if(workPlace != null){
            workPlaceTV.setText(workPlace);
        }

        if (place != null && place.getAddress() != null && !place.getAddress().equals("")) {
            workPlaceAddTV.setText(place.getAddress().toString());
        }
        if(businessHourTimes!= null){
            formatBusinessHours(businessHourTimes);
        }

        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode  == RESULT_OK) {
            if (requestCode == Config.REQUEST_PLACE_PICKER) {
                place = PlacePicker.getPlace(parentActivity, data);

                if (place != null && place.getAddress() != null && !place.getAddress().equals("")) {
                    workPlaceAddTV.setText(place.getAddress().toString());
                    GeneralFunction.setLocationNameForPlace(parentActivity, place, workPlaceTV);
                    workPlaceTV.requestLayout();
                    workPlaceAddTV.requestLayout();
                } else {
                    GeneralFunction.openMessageNotificationDialog(parentActivity, parentActivity.getString(R.string.error_location_picker), R.drawable.notice_bad, TAG);
                }
            }else  if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                Uri resultUri = result.getUri();

                if(imageButton == 1){
                    this.shopPicPath = FilePath.getPath(parentActivity, resultUri);
                    GlideImageSetting(parentActivity, resultUri,uploadShopIB);
                    shopPhotoUri = resultUri;
                }
                if(imageButton == 2){
                    this.licensePicPath = FilePath.getPath(parentActivity, resultUri);
                    GlideImageSetting(parentActivity, resultUri,uploadLicenseIB);
                    licenseUri = resultUri;
                }
            }

            if (requestCode == CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE) {
                if (ContextCompat.checkSelfPermission(parentActivity, Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_DENIED) {
                    ActivityCompat.requestPermissions(parentActivity, new String[]{Manifest.permission.CAMERA}, requestCode);

                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.uploadLicenseIB:
                if(GeneralFunction.isStorageAndCameraPermitted(parentActivity.getApplicationContext(),parentActivity)){
                    GeneralFunction.showFileChooser(parentActivity,this);
                    imageButton = 2;
                }
                break;

            case R.id.constraintLayoutSpinnerItem:
                GeneralFunction.slideDownAnimationForListView(parentActivity,constraintLayoutSpinnerItem);
                break;

            case R.id.industryET:
                GeneralFunction.slideUpAnimationForListView(parentActivity,constraintLayoutSpinnerItem, listViewSpinnerItem,R.array.fieldOfIndustry,industryET);
                isOpen = true;
                break;

            case R.id.uploadShopIB:
                if(GeneralFunction.isStorageAndCameraPermitted(parentActivity.getApplicationContext(),parentActivity)){
                    GeneralFunction.showFileChooser(parentActivity,this);
                }
                imageButton = 1;
                break;

            case R.id.submitTV:
                isSaveDraft = false;
                applyAsMerchant();
                break;

            case R.id.btn_saveAsDraft:
                if(merchant != null && merchant.shopStatus != null && TextUtils.equals(merchant.shopStatus,Merchant.STATUS_ACCEPTED)){
                    if(merchant.isPublish != null){
                        if(merchant.isPublish){
                            //unpublish
                            showPublishConfirmDialog(false);
                        }else{
                            //publish
                            showPublishConfirmDialog(true);
                        }
                    }else{
                        //publish
                        showPublishConfirmDialog(true);
                    }
                }else{
                    isSaveDraft = true;
                    applyAsMerchant();
                }

                break;
        }
    }

}
