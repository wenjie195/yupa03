package com.vidatechft.yupa.merchantShopFragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.MSNearbyStayAdapter;
import com.vidatechft.yupa.classes.BusinessHourTime;
import com.vidatechft.yupa.classes.Favourite;
import com.vidatechft.yupa.classes.Merchant;
import com.vidatechft.yupa.dialogFragments.MSNearbyViewAllDialogFragment;
import com.vidatechft.yupa.dialogFragments.MerchantShopDetailsDialogFragment;
import com.vidatechft.yupa.dialogFragments.MerchantShopMapClusterListDialogFragment;
import com.vidatechft.yupa.utilities.Config;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.vidatechft.yupa.merchantShopFragment.BusinessHourDialogFragment.NUM_OF_WEEKDAYS;

//todo the points are not in the bouding box...It seems like in between (horizonal only) the two points all things will be returned //it ignores the longitude of the result it seems (as mentioned from here https://stackoverflow.com/questions/46630507/how-to-run-a-geo-nearby-query-with-firestore   for longitude we need to filter ourself manually) // this bug is same for all that is using the same method like job map fragment
public class MerchantMapFragment extends Fragment implements OnMapReadyCallback {
    public static final String TAG = MerchantMapFragment.class.getName();
    private MainActivity parentActivity;

    private GoogleMap googleMap;
    private ClusterManager<Merchant> mClusterManager;

    private FusedLocationProviderClient locationProviderClient;
    private LocationCallback locationCallback;

    private TextView alertTV;
    private RecyclerView nearbyStayRV;
    private Button viewAllBtn;

    private MSNearbyStayAdapter msNearbyStayAdapter;
    private ArrayList<Merchant> merchants = new ArrayList<>();
    private List<Boolean> isAllDownloaded = new ArrayList<>();
    private HashMap<String,LatLng> boundingBox;
    private ListenerRegistration shopListener;

    //this is the marker's view
    private ViewGroup infoWindow;
    private TextView locationTV,workspaceTV;
    private ImageView shopImg;
    private Merchant selectedShop;
    private boolean isViewAll = false;

    private int permissionCheckCount = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static MerchantMapFragment newInstance(MainActivity parentActivity) {
        MerchantMapFragment fragment = new MerchantMapFragment();

        fragment.parentActivity = parentActivity;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_merchantshop_map, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.showBtmNav();

                alertTV = view.findViewById(R.id.alertTV);
                viewAllBtn = view.findViewById(R.id.viewAllBtn);
                nearbyStayRV = view.findViewById(R.id.nearbyStayRV);

                viewAllBtn.setOnClickListener(new DebouncedOnClickListener(500) {
                    @Override
                    public void onDebouncedClick(View v) {
                        isViewAll = true;
                        if (parentActivity != null && !parentActivity.isFinishing() && GeneralFunction.isAppInForground(parentActivity.getApplicationContext())) {
                            FragmentManager fm = parentActivity.getSupportFragmentManager();
                            MSNearbyViewAllDialogFragment msNearbyViewAllDialogFragment = MSNearbyViewAllDialogFragment.newInstance(parentActivity, MerchantMapFragment.this, merchants, isViewAll);
                            msNearbyViewAllDialogFragment.show(fm, TAG);
                        }
                    }
                });

                SupportMapFragment mapFragment = (SupportMapFragment) MerchantMapFragment.this.getChildFragmentManager()
                        .findFragmentById(R.id.nearbyShopGoogleMap);
                mapFragment.getMapAsync(MerchantMapFragment.this);

                initAllDownloaded();
            }
        });

        msNearbyStayAdapter = new MSNearbyStayAdapter(parentActivity, merchants);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(parentActivity);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        nearbyStayRV.setAdapter(msNearbyStayAdapter);
        nearbyStayRV.setLayoutManager(linearLayoutManager);
        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        setupInfoWindow();
    }

    private void setupInfoWindow(){
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(infoWindow == null){
                    infoWindow = (ViewGroup)getLayoutInflater().inflate(R.layout.info_window_google_map_marker_merchant_shop, infoWindow);
                }

                //declare variable below
                locationTV = infoWindow.findViewById(R.id.locationTV);
                workspaceTV = infoWindow.findViewById(R.id.workspaceTV);
                shopImg = infoWindow.findViewById(R.id.shopImg);

                MerchantMapFragment.this.googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                    @Override
                    public View getInfoWindow(final Marker marker) {
                        return null;
                    }

                    @Override
                    public View getInfoContents(final Marker marker) {
                        //the whole code in getInfoContents can be put inside here too (to eliminate the original paddings)
                        if(selectedShop != null){
                            if(selectedShop.workPlace != null){
                                workspaceTV.setText(String.valueOf(selectedShop.workPlace));
                            }

                            if(selectedShop.workPlaceAdd != null){
                                locationTV.setText(String.valueOf(selectedShop.workPlaceAdd));
                            }

                            if(selectedShop.workPlace == null && selectedShop.workPlaceAdd == null){
                                workspaceTV.setText(parentActivity.getString(R.string.none));
                            }

                            if(selectedShop.shopPicUrl != null) {
//                                GeneralFunction.GlideImageSetting(parentActivity,Uri.parse(selectedShop.shopPicUrl),shopImg);
                                //has to be shrinked in google map marker because if not the image cant be shown
                                GeneralFunction.GlideImageSettingShrinkImage(parentActivity,Uri.parse(selectedShop.shopPicUrl),shopImg,marker);
                            }

                        }
                        if(selectedShop != null){
                            return infoWindow;
                        }else{
                            return null;
                        }

                    }
                });

                //if want to make a button clickable only instead of the whole window refer to the link below
                //from here https://stackoverflow.com/questions/14123243/google-maps-android-api-v2-interactive-infowindow-like-in-original-android-go/15040761#15040761
                MerchantMapFragment.this.googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                    @Override
                    public void onInfoWindowClick(Marker marker) {
                        if(selectedShop != null){

                            if(parentActivity != null && !parentActivity.isFinishing() && GeneralFunction.isAppInForground(parentActivity.getApplicationContext())){
                                FragmentManager fm = parentActivity.getSupportFragmentManager();
                                MerchantShopDetailsDialogFragment merchantShopDetailsDialogFragment = MerchantShopDetailsDialogFragment.newInstance(parentActivity,selectedShop);
                                merchantShopDetailsDialogFragment.show(fm, TAG);
                            }
                        }
                    }
                });

                MerchantMapFragment.this.googleMap.setOnInfoWindowCloseListener(new GoogleMap.OnInfoWindowCloseListener() {
                    @Override
                    public void onInfoWindowClose(Marker marker) {
                        selectedShop = null;
                    }
                });
            }
        });
    }

    private void populateMap(){
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                googleMap.clear();
                mClusterManager = new ClusterManager<>(parentActivity, googleMap);
                mClusterManager.setRenderer(new CustomMapRenderer<>(parentActivity, googleMap, mClusterManager));
                googleMap.setOnCameraIdleListener(mClusterManager);
                googleMap.setOnMarkerClickListener(mClusterManager);
                googleMap.getUiSettings().setZoomControlsEnabled(true);

                googleMap.addMarker(new MarkerOptions()
                        .title(parentActivity.getString(R.string.you_are_here))
                        .position(boundingBox.get(Config.BOUNDING_BOX_CENTER))
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_gps)))
                        .setZIndex(0.1f);

                CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                    boundingBox.get(Config.BOUNDING_BOX_CENTER), Merchant.SHOP_ZOOM_IN_LEVEL);//the bigger the number, the deeper it goes
                    googleMap.animateCamera(location);

                googleMap.addCircle(GeneralFunction.getMapCircle(boundingBox.get(Config.BOUNDING_BOX_CENTER),Merchant.SHOP_RADIUS,Config.MAP_COLOUR_GREEN));

                mClusterManager.setOnClusterClickListener(new ClusterManager.OnClusterClickListener<Merchant>() {
                    @Override
                    public boolean onClusterClick(Cluster<Merchant> cluster) {

                        if(googleMap.getCameraPosition().zoom >= 20){
                            if(parentActivity != null && !parentActivity.isFinishing() && GeneralFunction.isAppInForground(parentActivity.getApplicationContext())){
                                FragmentManager fm = parentActivity.getSupportFragmentManager();
                                MerchantShopMapClusterListDialogFragment merchantShopMapClusterListDialogFragment = MerchantShopMapClusterListDialogFragment.newInstance(parentActivity,new ArrayList<>(cluster.getItems()));
                                merchantShopMapClusterListDialogFragment.show(fm, TAG);
                            }
                        }else{
                            //zoom in the cluster to uncluster it
                            //from here https://stackoverflow.com/questions/25395357/android-how-to-uncluster-on-single-tap-on-a-cluster-marker-maps-v2
                            LatLngBounds.Builder builder = LatLngBounds.builder();
                            for (ClusterItem item : cluster.getItems()) {
                                builder.include(item.getPosition());
                            }
                            final LatLngBounds bounds = builder.build();
                            googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
                        }

                        return true;
                    }
                });

                mClusterManager.setOnClusterItemClickListener(new ClusterManager.OnClusterItemClickListener<Merchant>() {
                    @Override
                    public boolean onClusterItemClick(Merchant merchant) {
                        selectedShop = merchant;
                        return false;
                    }
                });
            }
        });
    }

    //**************************************************GET NEARBY SHOPS START***************************************************//

    private void getNearbyShop(final Location currentLocation){
        //stop getting updates
        if(locationProviderClient != null && locationCallback != null){
            locationProviderClient.removeLocationUpdates(locationCallback);
        }

        if(merchants != null && !merchants.isEmpty()){
            return;
        }

        alertTV.setText(parentActivity.getString(R.string.loading_merchant_shops));
        alertTV.setVisibility(View.VISIBLE);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(currentLocation.getLatitude() == 0 && currentLocation.getLongitude() == 0){
                    alertTV.setVisibility(View.VISIBLE);
                    alertTV.setText(parentActivity.getString(R.string.err_getting_gps));
                    return;
                }

                HashMap<String,GeoPoint> boundingBox = GeneralFunction.getBoundingBoxCoor(currentLocation,Merchant.SHOP_RADIUS);

                shopListener = GeneralFunction.getFirestoreInstance()
                        .collection(Merchant.URL_MERCHANT)
                        .whereGreaterThan(Merchant.GPS,boundingBox.get(Config.BOUNDING_BOX_BTM_LEFT))
                        .whereLessThan(Merchant.GPS,boundingBox.get(Config.BOUNDING_BOX_TOP_RIGHT))
                        .whereEqualTo(Merchant.IS_DRAFT, false)
                        .whereEqualTo(Merchant.SHOP_STATUS,Merchant.STATUS_ACCEPTED)
                        .whereEqualTo(Merchant.IS_PUBLISH,true)
                        .orderBy(Merchant.GPS)
                        .orderBy(Merchant.DATE_CREATED, Query.Direction.DESCENDING)
                        .addSnapshotListener(new EventListener<QuerySnapshot>() {
                            @Override
                            public void onEvent(@javax.annotation.Nullable QuerySnapshot snapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                                if(e != null){
                                    Log.e(TAG,e.getMessage());
                                    alertTV.setVisibility(View.VISIBLE);
                                    alertTV.setText(parentActivity.getString(R.string.merchant_err_loading_shops));
                                    return;
                                }

                                if(snapshots != null && !snapshots.isEmpty()){
                                    ArrayList<Task<QuerySnapshot>> getBusinessHours = new ArrayList<>();
                                    ArrayList<Task<QuerySnapshot>> getFavourites = new ArrayList<>();

                                    merchants.clear();

                                    for(final DocumentSnapshot snapshot : snapshots){
                                        Merchant tempMerc = Merchant.snapshotToMerchant(parentActivity,snapshot);

                                        if(tempMerc != null){
                                            merchants.add(tempMerc);
                                            Task<QuerySnapshot> getBusinessHour = snapshot.getReference().collection(Merchant.URL_MERCHANT_BUSINESS_HOUR).get();
                                            Task<QuerySnapshot> getFavourite = GeneralFunction.getFirestoreInstance()
                                                    .collection(Favourite.URL_FAVOURITE)
                                                    .whereEqualTo(Favourite.TYPE,Favourite.FAV_TYPE_SHOP)
                                                    .whereEqualTo(Favourite.USER_ID, parentActivity.uid)
                                                    .whereEqualTo(Favourite.TARGET_ID, tempMerc.id)
                                                    .orderBy(Favourite.DATE_CREATED, Query.Direction.DESCENDING)
                                                    .limit(1)
                                                    .get();
                                            getBusinessHours.add(getBusinessHour);
                                            getFavourites.add(getFavourite);
                                        }
                                    }

                                    initAllDownloaded();
                                    getAllBusinessHours(getBusinessHours);
                                    getAllFavourites(getFavourites);
                                    alertTV.setVisibility(View.GONE);
                                }else{
                                    alertTV.setText(parentActivity.getString(R.string.no_shops_available));
                                    alertTV.setVisibility(View.VISIBLE);
                                }
                            }
                        });
            }
        });
    }

    private void initAllDownloaded(){
        isAllDownloaded.clear();
        isAllDownloaded.add(false);
        isAllDownloaded.add(false);
    }

    private void getAllBusinessHours(ArrayList<Task<QuerySnapshot>> getBusinessHours){
        if(getBusinessHours.isEmpty()){
            handleOnAllDownloaded(0);
            return;
        }
        Task<List<QuerySnapshot>> allTasks = Tasks.whenAllSuccess(getBusinessHours);
        allTasks.addOnSuccessListener(new OnSuccessListener<List<QuerySnapshot>>() {
            @Override
            public void onSuccess(List<QuerySnapshot> querySnapshots) {
                for(int i = 0; i < querySnapshots.size(); i++){
                    QuerySnapshot querySnapshot = querySnapshots.get(i);

                    SparseArray<ArrayList<BusinessHourTime>> businessHourTimes = new SparseArray<>();
                    for(int day = 0; day < NUM_OF_WEEKDAYS; day++){
                        businessHourTimes.put(day,null);
                    }

                    for(int x = 0; x < querySnapshot.size(); x++){
                        ArrayList<BusinessHourTime> allOperatingHours = new ArrayList<>();
                        DocumentSnapshot documentSnapshot = querySnapshot.getDocuments().get(x);

                        if(documentSnapshot.getData() != null && !documentSnapshot.getData().isEmpty()){
                            for(int y = 0; y < documentSnapshot.getData().size(); y++){
                                BusinessHourTime temp = new BusinessHourTime((HashMap<String,Long>) documentSnapshot.getData().get(String.valueOf(y)));
                                allOperatingHours.add(temp);
                            }
                        }
                        businessHourTimes.put(Integer.parseInt(querySnapshot.getDocuments().get(x).getId()),allOperatingHours);
                    }
                    merchants.get(i).businessHourTimes = businessHourTimes;
                }
                handleOnAllDownloaded(0);
            }
        }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e(TAG,e.getMessage());
                    handleOnAllDownloaded(0);
                }
            });
    }

    private void getAllFavourites(ArrayList<Task<QuerySnapshot>> getFavourites){
        if(getFavourites.isEmpty()){
            handleOnAllDownloaded(1);
            return;
        }
        Task<List<QuerySnapshot>> allTasks = Tasks.whenAllSuccess(getFavourites);
        allTasks.addOnSuccessListener(new OnSuccessListener<List<QuerySnapshot>>() {
            @Override
            public void onSuccess(List<QuerySnapshot> querySnapshots) {
                for(int i = 0; i < querySnapshots.size(); i++){
                    QuerySnapshot querySnapshot = querySnapshots.get(i);

                    Favourite favourite = null;
                    for(int x = 0; x < querySnapshot.size(); x++){
                        DocumentSnapshot documentSnapshot = querySnapshot.getDocuments().get(x);

                        if(documentSnapshot != null && documentSnapshot.exists()){
                            favourite = Favourite.snapshotToFavourite(parentActivity,documentSnapshot);
                        }
                    }
                    merchants.get(i).favourite = favourite;
                }
                handleOnAllDownloaded(1);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e(TAG,e.getMessage());
                handleOnAllDownloaded(1);
            }
        });
    }

    private void handleOnAllDownloaded(int downloadTaskId){
        isAllDownloaded.set(downloadTaskId,true);
        for(Boolean isDownloaded : isAllDownloaded){
            if(!isDownloaded){
                return;
            }
        }

        if(merchants != null && !merchants.isEmpty()){
            msNearbyStayAdapter.notifyDataSetChanged();
            alertTV.setVisibility(View.GONE);
            if(mClusterManager != null){
                mClusterManager.clearItems();
                for(Merchant tempMerchant : merchants){
                    mClusterManager.addItem(tempMerchant);
                }
                mClusterManager.cluster();//after clearing need to use this function to show the new changes
            }
        }else{
            alertTV.setVisibility(View.VISIBLE);
            alertTV.setText(parentActivity.getString(R.string.no_shops_available_map));
        }
    }

    public void updateMerchantFavouriteFromViewAllDialogFragment(int position, Merchant merchant){
        if(merchants.get(position) != null){
            merchants.set(position,merchant);
            msNearbyStayAdapter.notifyItemChanged(position);
        }
    }

    //**************************************************GET NEARBY SHOPS END***************************************************//

    //because the default google map cluster manager will only cluster the marker if there is more than 4 marker on same position
    //so we need to overwrite it ourselves by using the class below
    class CustomMapRenderer<T extends ClusterItem> extends DefaultClusterRenderer<Merchant> {
        private final IconGenerator mClusterIconGenerator = new IconGenerator(parentActivity);
        public CustomMapRenderer(Context context, GoogleMap map, ClusterManager<Merchant> clusterManager) {
            super(context, map, clusterManager);
        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster<Merchant> cluster) {
            //start clustering if at least 2 items overlap
            return cluster.getSize() > 1;
        }

        //for single marker item
        @Override
        protected void onBeforeClusterItemRendered(final Merchant item, final MarkerOptions markerOptions) {
            super.onBeforeClusterItemRendered(item, markerOptions);
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //if want to use own icon from drawable then use this
                    markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker_low));
                    markerOptions.zIndex(1.0f);
                }
            });
        }

        //for clustered item marker
        //Refer this https://codedump.io/share/JTvSCafSuuf7/1/android-maps-utils-cluster-icon-color
        //and this http://stackoverflow.com/questions/30967961/android-maps-utils-cluster-icon-color
        //for changing icons and colours of clustered markers and single markers with even more control
        @Override
        protected void onBeforeClusterRendered(final Cluster<Merchant> cluster, final MarkerOptions markerOptions){

            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Drawable clusterIcon;

                    if(cluster.getSize() <= 15){
                        clusterIcon = ContextCompat.getDrawable(parentActivity, R.drawable.ic_map_marker_low);
                    }
                    else if(cluster.getSize() > 15){
                            clusterIcon = ContextCompat.getDrawable(parentActivity, R.drawable.ic_map_marker_high);
                    }
                    else{
                            clusterIcon = ContextCompat.getDrawable(parentActivity, R.drawable.ic_map_marker_low);
                    }

                    mClusterIconGenerator.setTextAppearance(R.style.clusterIconText);
                    mClusterIconGenerator.setBackground(clusterIcon);

                    Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
                    markerOptions.zIndex(1.0f);
                }
            });
        }
    }

    //needs this to prevent duplicate id of map and autocomplete that is making the app crash
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        MapFragment f = (MapFragment) parentActivity.getFragmentManager()
                .findFragmentById(R.id.nearbyShopGoogleMap);
        if (f != null)
            parentActivity.getFragmentManager().beginTransaction().remove(f).commit();

        if(shopListener != null){
            shopListener.remove();
        }
    }

    //***********************************************************GETTING GPS STUFF START****************************************************/

    private boolean isAllCheckPassed(){
        if(merchants != null && !merchants.isEmpty()){
            alertTV.setVisibility(View.GONE);
//            msNearbyStayAdapter.notifyDataSetChanged();
            return false;
        }

        if (GeneralFunction.dontHaveLocationPermission(parentActivity)) {
            permissionCheckCount++;

            GeneralFunction.requestForLocationPermission(parentActivity,permissionCheckCount);
            alertTV.setText(parentActivity.getString(R.string.need_enable_gps));
            alertTV.setVisibility(View.VISIBLE);
            return false;
        }

        if (!GeneralFunction.isGpsEnabled(parentActivity)){
            GeneralFunction.showGpsSettingsAlert(parentActivity);
            alertTV.setText(parentActivity.getString(R.string.need_on_gps));
            alertTV.setVisibility(View.VISIBLE);
            return false;
        }else{
            alertTV.setText(parentActivity.getString(R.string.searching_gps));
            alertTV.setVisibility(View.VISIBLE);
        }

        return true;
    }

    // Trigger new location updates at interval
    //protected means that this function can only be accessed by the class in the same package only
    protected void startLocationUpdates() {
        parentActivity.runOnUiThread(new Runnable() {
            //added this lint because i actually already checked whether got permission before executung the location updates
            @SuppressLint("MissingPermission")
            @Override
            public void run() {
                if(!isAllCheckPassed()){
                    return;
                }

//                ptJobSwipeAdapter = new PTJobSwipeAdapter(PTJobSwipeFragment.this,parentActivity,lastToolbarTitle);
//                cardStackView.setAdapter(ptJobSwipeAdapter);

                // Create the location request to start receiving updates
                LocationRequest locationRequest = new LocationRequest();
                locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                locationRequest.setInterval(Config.GPS_UPDATE_INTERVAL);
                locationRequest.setFastestInterval(Config.GPS_FASTEST_INTERVAL);

                // Create LocationSettingsRequest object using location request
                LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
                builder.addLocationRequest(locationRequest);
                LocationSettingsRequest locationSettingsRequest = builder.build();

                // Check whether location settings are satisfied
                // https://developers.google.com/android/reference/com/google/android/gms/location/SettingsClient
                SettingsClient settingsClient = LocationServices.getSettingsClient(parentActivity);
                settingsClient.checkLocationSettings(locationSettingsRequest);

                // new Google API SDK v11 uses getFusedLocationProviderClient(this)
                locationProviderClient = LocationServices.getFusedLocationProviderClient(parentActivity);
                locationCallback = new LocationCallback(){
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        if(locationResult != null && locationResult.getLastLocation() != null){
                            getNearbyShop(locationResult.getLastLocation());
                            boundingBox = GeneralFunction.getBoundingBoxCoor(new LatLng(locationResult.getLastLocation().getLatitude(),locationResult.getLastLocation().getLongitude()),500);
                            populateMap();
                        }else{
                            alertTV.setVisibility(View.VISIBLE);
                            alertTV.setText(parentActivity.getString(R.string.err_getting_gps));
                        }
                    }

                    @Override
                    public void onLocationAvailability(LocationAvailability locationAvailability) {
                        if(!locationAvailability.isLocationAvailable()){
                            alertTV.setVisibility(View.VISIBLE);
                            alertTV.setText(parentActivity.getString(R.string.err_getting_gps));
                        }
                    }
                };
                locationProviderClient.requestLocationUpdates(locationRequest,locationCallback,Looper.myLooper());
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Config.REQUEST_GPS_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startLocationUpdates();
                } else {
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.need_enable_gps),R.drawable.notice_bad,TAG);
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(merchants == null || merchants.isEmpty()){
            startLocationUpdates();
        }else{
            alertTV.setVisibility(View.GONE);
        }

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.nearbyShops));
                parentActivity.selectHighlights();
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        if(locationProviderClient != null && locationCallback != null){
            locationProviderClient.removeLocationUpdates(locationCallback);
        }
    }

    //***************************************************************GETTING GPS STUFF END****************************************************/

}
