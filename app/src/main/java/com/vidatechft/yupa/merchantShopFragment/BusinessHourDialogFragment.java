package com.vidatechft.yupa.merchantShopFragment;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.BusinessHour;
import com.vidatechft.yupa.classes.BusinessHourTime;
import com.vidatechft.yupa.classes.GlideApp;
import com.vidatechft.yupa.classes.Merchant;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.SortedSet;
import java.util.TreeSet;

import static com.bumptech.glide.load.DecodeFormat.PREFER_RGB_565;
import static com.vidatechft.yupa.utilities.GeneralFunction.clearForm;

public class BusinessHourDialogFragment extends android.support.v4.app.Fragment implements View.OnClickListener {
    public static final String TAG = BusinessHourDialogFragment.class.getName();
    private final int DAY_LIMIT = 2; //means limit 3 business times per day

    private MainActivity parentActivity;
    private ApplyAsMerchantFragment parentFragment;
    private View view;
    public static final int NUM_OF_WEEKDAYS = 7;


    private TextView[] openBHTV, closeBHTV;
    private Button submitBtn, resetBtn, addBtn;
    private Button[] addButtons;
    private static Switch[] switches;
    private ConstraintLayout[] constraintLayouts;
    private LinearLayout[] linearLayouts;
    private SparseArray<ArrayList<BusinessHourTime>> businessHourTimes;
    private ScrollView merchantBusinessHourSV;

    private SparseArray<TextView> opensInnerTV, closesInnerTV;
    private static ArrayList<BusinessHour> openBHs;
    private static ArrayList<BusinessHour> closeBHs;
    private ArrayList<Integer> eachDayLimit = new ArrayList<>();
    private int innerTVViewCount = 0;
    private ViewGroup viewGroup;
    private StringBuilder businessHourBuilder;
    private SortedSet<Integer> openKeys, closeKeys;
    private boolean isReset = false;


    public static BusinessHourDialogFragment newInstance(MainActivity parentActivity, ApplyAsMerchantFragment parentFragment, SparseArray<ArrayList<BusinessHourTime>> businessHourTimes) {
        BusinessHourDialogFragment fragment = new BusinessHourDialogFragment();
        fragment.businessHourTimes = businessHourTimes;
        fragment.parentActivity = parentActivity;
        fragment.parentFragment = parentFragment;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        opensInnerTV = new SparseArray<>();
        closesInnerTV = new SparseArray<>();

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.frag_add_business_hour));
            }
        });


        //INITIALIZE ALL BUSINESS HOURS
        openBHs = new ArrayList<>();
        closeBHs = new ArrayList<>();
        for (int i = 0; i < NUM_OF_WEEKDAYS; i++) {
            openBHs.add(new BusinessHour());
            closeBHs.add(new BusinessHour());
        }
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_merchant_business_hours, container, false);
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                submitBtn = view.findViewById(R.id.submitBtn);
                resetBtn = view.findViewById(R.id.resetBtn);
                viewGroup = view.findViewById(R.id.merchantBusinessHourSV);
                switches = new Switch[NUM_OF_WEEKDAYS];
                addButtons = new Button[NUM_OF_WEEKDAYS];
                constraintLayouts = new ConstraintLayout[NUM_OF_WEEKDAYS];
                linearLayouts = new LinearLayout[NUM_OF_WEEKDAYS];
                openBHTV = new TextView[NUM_OF_WEEKDAYS];
                closeBHTV = new TextView[NUM_OF_WEEKDAYS];
            }
        });

        setUpListener(view);

        submitBtn.setOnClickListener(this);
        resetBtn.setOnClickListener(this);

        return view;
    }

    private void setUpListener(final View view) {
        for (int day = 0; day < NUM_OF_WEEKDAYS; day++) {
            final int DAY = day;

            //initialize the eachDayLimit Array
            eachDayLimit.add(0);

            final int switchesID = getResources().getIdentifier("switch" + (DAY + 1), "id", parentActivity.getPackageName());
            int addButtonID = getResources().getIdentifier("add" + (DAY + 1) + "Btn", "id", parentActivity.getPackageName());
            final int clsID = getResources().getIdentifier("open" + (DAY + 1) + "CL", "id", parentActivity.getPackageName());
            int linearID = getResources().getIdentifier("day" + (DAY + 1) + "LL", "id", parentActivity.getPackageName());
            final int openTVID = getResources().getIdentifier("openBHTV" + (DAY + 1), "id", parentActivity.getPackageName());
            final int closeTVID = getResources().getIdentifier("closeBHTV" + (DAY + 1), "id", parentActivity.getPackageName());
            switches[DAY] = view.findViewById(switchesID);
            constraintLayouts[DAY] = view.findViewById(clsID);
            linearLayouts[DAY] = view.findViewById(linearID);
            addButtons[DAY] = view.findViewById(addButtonID);
            openBHTV[DAY] = view.findViewById(openTVID);
            closeBHTV[DAY] = view.findViewById(closeTVID);
            switches[DAY].setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        //Toast.makeText(getContext(),"ON", Toast.LENGTH_LONG).show();
                        constraintLayouts[DAY].setVisibility(View.VISIBLE);
                    } else {
                        //Toast.makeText(getContext(),"OFF", Toast.LENGTH_LONG).show();
                        constraintLayouts[DAY].setVisibility(View.GONE);
                    }
                }
            });

            openBHTV[DAY].setOnClickListener(new DebouncedOnClickListener(500) {
                @Override
                public void onDebouncedClick(View v) {
                    showTimePickerDialog((TextView) v, DAY, 0, true);
                }
            });

            closeBHTV[DAY].setOnClickListener(new DebouncedOnClickListener(500) {
                @Override
                public void onDebouncedClick(View v) {
                    showTimePickerDialog((TextView) v, DAY, 0, false);
                }
            });


            addButtons[DAY].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    appendAddBusinessHour(DAY);
                    addButtons[DAY].setVisibility(View.INVISIBLE);
                    eachDayLimit.set(DAY, eachDayLimit.get(DAY) + 1);
                }
            });

            if (businessHourTimes != null && businessHourTimes.get(DAY) != null) {
                int viewCounts = 0;
                for (BusinessHourTime tempBHT : businessHourTimes.get(DAY)) {
                    int openHourBH = tempBHT.getOpenHour();
                    int openMinBH = tempBHT.getOpenMin();
                    int closeHourBH = tempBHT.getCloseHour();
                    int closeMinBH = tempBHT.getCloseMin();
                    if (viewCounts == 0) {
                        openBHs.get(DAY).getHour().put(0, openHourBH);
                        openBHs.get(DAY).getMinute().put(0, openMinBH);
                        closeBHs.get(DAY).getHour().put(0, closeHourBH);
                        closeBHs.get(DAY).getMinute().put(0, closeMinBH);
                        openBHTV[DAY].setText(timeFormat(openHourBH, openMinBH, 1000, 1000));
                        closeBHTV[DAY].setText(timeFormat(closeHourBH, closeMinBH, closeHourBH, closeMinBH));

                    } else {
                        addButtons[DAY].performClick();
                        openBHs.get(DAY).getHour().put(innerTVViewCount, openHourBH);
                        openBHs.get(DAY).getMinute().put(innerTVViewCount, openMinBH);
                        closeBHs.get(DAY).getHour().put(innerTVViewCount, closeHourBH);
                        closeBHs.get(DAY).getMinute().put(innerTVViewCount, closeMinBH);
                        opensInnerTV.get(innerTVViewCount).setText(timeFormat(openHourBH, openMinBH, 1000, 1000));
                        closesInnerTV.get(innerTVViewCount).setText(timeFormat(closeHourBH, closeMinBH, 1000, 1000));
                    }

                    viewCounts++;
                }
            }


//            submitBtn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    saveBusinessHour();
//                }
//            });

//            resetBtn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    isReset = true;
//                    GeneralFunction.clearFormTV(viewGroup);
//
//                    //INITIALIZE ALL BUSINESS HOURS
//                    openBHs = new ArrayList<>();
//                    closeBHs = new ArrayList<>();
//                    for (int i = 0; i < NUM_OF_WEEKDAYS; i++) {
//                        openBHs.add(new BusinessHour());
//                        closeBHs.add(new BusinessHour());
//                    }
//
//                }
//            });
        }
    }


    private void saveBusinessHour() {
        StringBuilder businessHourBuilder = new StringBuilder();
        for (int i = 0; i < NUM_OF_WEEKDAYS; i++) {
            switch (i) {
                case 0:
                    businessHourBuilder.append("Monday \n");
                    break;
                case 1:
                    businessHourBuilder.append("\n Tuesday \n");
                    break;
                case 2:
                    businessHourBuilder.append("\n Wednesday \n");
                    break;
                case 3:
                    businessHourBuilder.append("\n Thursday \n");
                    break;
                case 4:
                    businessHourBuilder.append("\n Friday \n");
                    break;
                case 5:
                    businessHourBuilder.append("\n Saturday \n");
                    break;
                case 6:
                    businessHourBuilder.append("\n Sunday \n");
                    break;
                default:
            }

            if (!switches[i].isChecked()) {
                businessHourBuilder.append("Closed" + "\n");
            } else {
                SparseIntArray allKeys = new SparseIntArray();

                SortedSet<Integer> openKeys = new TreeSet<>(openBHs.get(i).getHour().keySet());
                for (int key : openKeys) {
                    allKeys.append(key, key);
                }

                SortedSet<Integer> closeKeys = new TreeSet<>(closeBHs.get(i).getHour().keySet());
                for (int key : closeKeys) {
                    allKeys.append(key, key);
                }

                boolean hasValue = false;

                for (int key = 0; key < allKeys.size(); key++) {
                    businessHourBuilder.append("Open :" + "\t");
                    if (openBHs.get(i).getHour().get(allKeys.keyAt(key)) != null) {
                        int tempHour = openBHs.get(i).getHour().get(allKeys.keyAt(key));
                        int tempMin = openBHs.get(i).getMinute().get(allKeys.keyAt(key));
                        businessHourBuilder.append((timeFormat(tempHour, tempMin, 1000, 1000)) + "\t\t\t");
                    } else {
                        businessHourBuilder.append("00:00 \t");
                    }
                    businessHourBuilder.append(" - " + "\t");
                    if (closeBHs.get(i).getHour().get(allKeys.keyAt(key)) != null) {
                        int tempHour = closeBHs.get(i).getHour().get(allKeys.keyAt(key));
                        int tempMin = closeBHs.get(i).getMinute().get(allKeys.keyAt(key));
                        businessHourBuilder.append((timeFormat(tempHour, tempMin, 1000, 1000)) + "\n");
                    } else {
                        businessHourBuilder.append("00:00 \n");
                    }

                    hasValue = true;
                }

                if (!hasValue) {
                    businessHourBuilder.append("Open : 00:00 " + "\t\t\t\t\t\t\t\t" + "Close: 00:00\n");
                }
            }
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(parentActivity);

        // Set a title for alert dialog
        builder.setTitle("Your Business Hour \n");

        // Ask the final question
        builder.setMessage(businessHourBuilder.toString());



        // Set click listener for alert dialog buttons
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        // User clicked the Yes button

                        SparseArray<ArrayList<BusinessHourTime>> businessHourTimes = new SparseArray<>();//NEW

                        for (int day = 0; day < NUM_OF_WEEKDAYS; day++) {
                            if (!switches[day].isChecked()) {
                            } else {
                                SparseIntArray allKeys = new SparseIntArray();

                                SortedSet<Integer> openKeys = new TreeSet<>(openBHs.get(day).getHour().keySet());
                                for (int key : openKeys) {
                                    allKeys.append(key, key);
                                }

                                SortedSet<Integer> closeKeys = new TreeSet<>(closeBHs.get(day).getHour().keySet());
                                for (int key : closeKeys) {
                                    allKeys.append(key, key);
                                }

                                //NEWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW
                                ArrayList<BusinessHourTime> bhList = new ArrayList<>();

                                for (int key = 0; key < allKeys.size(); key++) {
                                    BusinessHourTime businessHourTime = new BusinessHourTime();
                                    if (openBHs.get(day).getHour().get(allKeys.keyAt(key)) != null) {
                                        businessHourTime.setOpenHour(openBHs.get(day).getHour().get(allKeys.keyAt(key)));
                                    }
                                    if (openBHs.get(day).getMinute().get(allKeys.keyAt(key)) != null) {
                                        businessHourTime.setOpenMin(openBHs.get(day).getMinute().get(allKeys.keyAt(key)));
                                    }

                                    if (closeBHs.get(day).getHour().get(allKeys.keyAt(key)) != null) {
                                        businessHourTime.setCloseHour(closeBHs.get(day).getHour().get(allKeys.keyAt(key)));
                                    }

                                    if (closeBHs.get(day).getMinute().get(allKeys.keyAt(key)) != null) {
                                        businessHourTime.setCloseMin(closeBHs.get(day).getMinute().get(allKeys.keyAt(key)));
                                    }

                                    bhList.add(businessHourTime);
                                }

                                businessHourTimes.put(day, bhList);

//                                getDialog().dismiss();
                            }
                        }

                        parentActivity.onBackPressed();
                        parentFragment.setBusinessHours(businessHourTimes);


                    case DialogInterface.BUTTON_NEGATIVE:
                        // User clicked the No button
                        break;

                }

            }
        };
        // Set the alert dialog yes button click listener
        builder.setPositiveButton("Yes", dialogClickListener);

        // Set the alert dialog no button click listener
        builder.setNegativeButton("No", dialogClickListener);

        AlertDialog dialog = builder.create();
        // Display the alert dialog on interface
        isReset = false;
        dialog.show();
    }

    private String getNextHour(int day, int position, boolean isOpen) {
        int hour = 0, min = 0;
        if (isOpen) {
            if (openBHs.get(day).getHour().get(0) != null) {
                hour = closeBHs.get(day).getHour().get(0) + 1;
                min = closeBHs.get(day).getMinute().get(0);

                openBHs.get(day).getHour().put(position, hour);
                openBHs.get(day).getMinute().put(position, min);
                openBHs.get(day).getIsAM().put(position, hour < 12);//if less than 12 then true
            }
        } else {
            if (closeBHs.get(day).getHour().get(0) != null) {
                hour = closeBHs.get(day).getHour().get(0) + 2;
                min = closeBHs.get(day).getMinute().get(0);

                closeBHs.get(day).getHour().put(position, hour);
                closeBHs.get(day).getMinute().put(position, min);
                closeBHs.get(day).getIsAM().put(position, hour < 12);//if less than 12 then true
            }
        }
        return timeFormat(1000, 1000, hour, min);
    }

    public final static String timeFormat(int tempHour, int tempMin, int openHourBH, int openMinBH) {
        if (tempHour == 1000 && tempMin == 1000) {
            return String.format(Locale.US, "%02d:%02d %s", openHourBH % 12, openMinBH, openHourBH < 12 ? "am" : "pm");
        } else {
            return String.format(Locale.US, "%02d:%02d %s", tempHour == 12 || tempHour == 0 ? 12 : tempHour % 12, tempMin, tempHour < 12 ? "am" : "pm");
        }
    }

    private void showTimePickerDialog(final TextView v, final int day, final int position, final boolean isOpen) {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;

        mTimePicker = new TimePickerDialog(parentActivity, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                int hour = selectedHour % 12;
                if (hour == 0) hour = 12;
                v.setText(String.format(Locale.US, "%02d:%02d %s", hour, selectedMinute, selectedHour < 12 ? "am" : "pm"));

                if (isOpen) {
                    openBHs.get(day).getHour().put(position, selectedHour);
                    openBHs.get(day).getMinute().put(position, selectedMinute);
                    openBHs.get(day).getIsAM().put(position, selectedHour < 12);//if less than 12 then true
                } else {
                    closeBHs.get(day).getHour().put(position, selectedHour);
                    closeBHs.get(day).getMinute().put(position, selectedMinute);
                    closeBHs.get(day).getIsAM().put(position, selectedHour < 12);//if less than 12 then true
                }
            }
        }, hour, minute, false);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    private void addNewBusinessHour(View v, final int DAY) {
        v.setOnClickListener(new DebouncedOnClickListener(500) {
            @Override
            public void onDebouncedClick(View v) {
                if (!isExceedLimit(DAY)) {
                    appendAddBusinessHour(DAY);
                    eachDayLimit.set(DAY, eachDayLimit.get(DAY) + 1);
                }
            }
        });
    }

    private void appendAddBusinessHour(final int DAY) {
        innerTVViewCount++;

        LayoutInflater layoutInflater = (LayoutInflater) parentActivity.getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View addView = layoutInflater.inflate(R.layout.fragment_add_business_hour, null);
        addBtn = addView.findViewById(R.id.addBtn);

        if (addBtn != null) {
            addNewBusinessHour(addBtn, DAY);
        }

        final int thisViewCount = innerTVViewCount;

        opensInnerTV.put(thisViewCount, (TextView) addView.findViewById(R.id.openBHTV));
        closesInnerTV.put(thisViewCount, (TextView) addView.findViewById(R.id.closeBHTV));
        TextView openBHTV = opensInnerTV.get(thisViewCount);
        TextView closeBHTV = closesInnerTV.get(thisViewCount);
        openBHTV.setText(getNextHour(DAY, thisViewCount, true));
        closeBHTV.setText(getNextHour(DAY, thisViewCount, false));

        Button deleteBtn = addView.findViewById(R.id.deleteBtn);
        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                innerTVViewCount--;
                eachDayLimit.set(DAY, eachDayLimit.get(DAY) - 1);
                ((LinearLayout) addView.getParent()).removeView(addView);
                closeBHs.get(DAY).getHour().remove(thisViewCount);
                closeBHs.get(DAY).getMinute().remove(thisViewCount);
                closeBHs.get(DAY).getIsAM().remove(thisViewCount);
                openBHs.get(DAY).getHour().remove(thisViewCount);
                openBHs.get(DAY).getMinute().remove(thisViewCount);
                openBHs.get(DAY).getIsAM().remove(thisViewCount);
                if (eachDayLimit.get(DAY) <= 0) {
                    addButtons[DAY].setVisibility(View.VISIBLE);
                }
            }
        });

        openBHTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePickerDialog((TextView) v, DAY, thisViewCount, true);
            }
        });

        closeBHTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePickerDialog((TextView) v, DAY, thisViewCount, false);
            }
        });

        linearLayouts[DAY].addView(addView);
    }

    private boolean isExceedLimit(final int DAY) {
        if (eachDayLimit.get(DAY) >= DAY_LIMIT) {
            return true;
        } else {
            return false;
        }
    }

    public static void pushDatabase(final DocumentReference merchantRef, final SparseArray<ArrayList<BusinessHourTime>> businessHourTimes) {
        if(businessHourTimes == null){
            return;
        }
        GeneralFunction.getFirestoreInstance()
                .collection(Merchant.URL_MERCHANT)
                .document(merchantRef.getId())
                .collection(Merchant.URL_MERCHANT_BUSINESS_HOUR)
                .document()
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        setBusinessHourTimesToDBWithPassedArgs(merchantRef,businessHourTimes);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e(TAG,e.getMessage());
                    }
                });
    }

    private static void setBusinessHourTimesToDBWithPassedArgs(final DocumentReference merchantRef, final SparseArray<ArrayList<BusinessHourTime>> businessHourTimes){
        for (int day = 0; day < NUM_OF_WEEKDAYS; day++) {
            final int currentDay = day;
            ArrayList<BusinessHourTime> eachDayBHTimes = businessHourTimes.get(day);
            HashMap<String, BusinessHourTime> bhTimeMap = new HashMap<>();
            if(eachDayBHTimes != null && !eachDayBHTimes.isEmpty()){
                for(int x = 0; x < eachDayBHTimes.size(); x++){
                    bhTimeMap.put(String.valueOf(x), eachDayBHTimes.get(x));
                }
            }

            GeneralFunction.getFirestoreInstance()
                    .collection(Merchant.URL_MERCHANT)
                    .document(merchantRef.getId())
                    .collection(Merchant.URL_MERCHANT_BUSINESS_HOUR)
                    .document(String.valueOf(day))
                    .set(bhTimeMap)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.getException() != null){
                                Log.e(TAG,task.getException().getMessage());
                                return;
                            }

                            //<DONE FIXED> seems like if I update the business hours and the adapter is refreshed with new updated data, the business hour still remains the old 1, the other stuff like name or description is updated though, go back out and in again will get the updated business hours though, the proof is updated though
                            //<DONE FIXED> okok i know why ad, is because the thing is already updated but business hours havent finished updated
                            //make it update only one time by running the update action only on the 7th day
                            //todo probably batch write is better but nvm ba
                            if(currentDay == NUM_OF_WEEKDAYS - 1){
                                merchantRef.update(Merchant.DATE_UPDATED, FieldValue.serverTimestamp());
                            }
                        }
                    });
        }
    }

    private static void setBusinessHourTimesToDBWithThisPage(final DocumentReference merchantRef){
        for (int day = 0; day < NUM_OF_WEEKDAYS; day++) {
            if (switches != null && switches[day] != null && switches[day].isChecked()) {
                SparseIntArray allKeys = new SparseIntArray();

                SortedSet<Integer> openKeys = new TreeSet<>(openBHs.get(day).getHour().keySet());
                for (int key : openKeys) {
                    allKeys.append(key, key);
                }

                TreeSet<Integer> closeKeys = new TreeSet<>(closeBHs.get(day).getHour().keySet());
                for (int key : closeKeys) {
                    allKeys.append(key, key);
                }

                HashMap<String, BusinessHourTime> timeBH = new HashMap<>(); // yg kat dlm db
                for (int key = 0; key < allKeys.size(); key++) {
                    BusinessHourTime businessHourTime = new BusinessHourTime();
                    if (openBHs.get(day).getHour().get(allKeys.keyAt(key)) != null) {
                        businessHourTime.setOpenHour(openBHs.get(day).getHour().get(allKeys.keyAt(key)));
                    }
                    if (openBHs.get(day).getMinute().get(allKeys.keyAt(key)) != null) {
                        businessHourTime.setOpenMin(openBHs.get(day).getMinute().get(allKeys.keyAt(key)));
                    }

                    if (closeBHs.get(day).getHour().get(allKeys.keyAt(key)) != null) {
                        businessHourTime.setCloseHour(closeBHs.get(day).getHour().get(allKeys.keyAt(key)));
                    }

                    if (closeBHs.get(day).getMinute().get(allKeys.keyAt(key)) != null) {
                        businessHourTime.setCloseMin(closeBHs.get(day).getMinute().get(allKeys.keyAt(key)));
                    }

                    timeBH.put(String.valueOf(key), businessHourTime);
                }

                GeneralFunction.getFirestoreInstance()
                        .collection(Merchant.URL_MERCHANT)
                        .document(merchantRef.getId())
                        .collection(Merchant.URL_MERCHANT_BUSINESS_HOUR)
                        .document(String.valueOf(day))
                        .set(timeBH);

            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.submitBtn:
                saveBusinessHour();
                break;

            case R.id.resetBtn:
                isReset = true;
                GeneralFunction.clearFormTV(viewGroup);

                //INITIALIZE ALL BUSINESS HOURS
                openBHs = new ArrayList<>();
                closeBHs = new ArrayList<>();
                for (int i = 0; i < NUM_OF_WEEKDAYS; i++) {
                    openBHs.add(new BusinessHour());
                    closeBHs.add(new BusinessHour());
                }
                break;

        }

    }
}