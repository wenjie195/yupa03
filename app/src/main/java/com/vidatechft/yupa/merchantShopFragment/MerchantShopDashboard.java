package com.vidatechft.yupa.merchantShopFragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.MerchantShopForManageRecyclerGridAdapter;
import com.vidatechft.yupa.classes.BusinessHourTime;
import com.vidatechft.yupa.classes.Merchant;
import com.vidatechft.yupa.dialogFragments.MerchantShopDetailsDialogFragment;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.vidatechft.yupa.merchantShopFragment.BusinessHourDialogFragment.NUM_OF_WEEKDAYS;

//<DONE FIXED> seems like if I update the business hours and the adapter is refreshed with new updated data, the business hour still remains the old 1, the other stuff like name or description is updated though, go back out and in again will get the updated business hours though, the proof is updated though
//<DONE FIXED> okok i know why ad, is because the thing is already updated but business hours havent finished updated
public class MerchantShopDashboard extends Fragment implements View.OnClickListener{
    public static final String TAG = MerchantShopDashboard.class.getName();
    private MainActivity parentActivity;

    private ArrayList<Merchant> mixedAllShops = new ArrayList<>();
    private List<Boolean> isAllDownloaded = new ArrayList<>();

    private RecyclerView draftRV;
    private ArrayList<Merchant> draftShops = new ArrayList<>();
    private MerchantShopForManageRecyclerGridAdapter draftAdapter;
    private LinearLayout draftLL;

    private RecyclerView veriRV;
    private ArrayList<Merchant> veriShops = new ArrayList<>();
    private MerchantShopForManageRecyclerGridAdapter veriAdapter;
    private LinearLayout veriLL;

    private RecyclerView publishRV;
    private ArrayList<Merchant> publishShops = new ArrayList<>();
    private MerchantShopForManageRecyclerGridAdapter publishAdapter;
    private LinearLayout publishLL;

    private RecyclerView unpublishRV;
    private ArrayList<Merchant> unpublishShops = new ArrayList<>();
    private MerchantShopForManageRecyclerGridAdapter unpublishAdapter;
    private LinearLayout unpublishLL;

    private RecyclerView rejectRV;
    private ArrayList<Merchant> rejectShops = new ArrayList<>();
    private MerchantShopForManageRecyclerGridAdapter rejectAdapter;
    private LinearLayout rejectLL;

    private TextView noTV;

    private String loadingTxt,emptyTxt,errorTxt;
    private ListenerRegistration shopListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static MerchantShopDashboard newInstance(MainActivity parentActivity) {
        MerchantShopDashboard fragment = new MerchantShopDashboard();

        fragment.parentActivity = parentActivity;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_merchant_shop_dashboard, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loadingTxt = parentActivity.getString(R.string.merchant_loading_shop);
                emptyTxt = parentActivity.getString(R.string.merchant_shop_empty);
                errorTxt = parentActivity.getString(R.string.merchant_loading_shop_err);

                draftRV = view.findViewById(R.id.draftRV);
                veriRV = view.findViewById(R.id.veriRV);
                publishRV = view.findViewById(R.id.publishRV);
                unpublishRV = view.findViewById(R.id.unpublishRV);
                rejectRV = view.findViewById(R.id.rejectRV);

                draftLL = view.findViewById(R.id.draftLL);
                veriLL = view.findViewById(R.id.veriLL);
                publishLL = view.findViewById(R.id.publishLL);
                unpublishLL = view.findViewById(R.id.unpublishLL);
                rejectLL = view.findViewById(R.id.rejectLL);

                noTV = view.findViewById(R.id.noTV);
                TextView submitShopTV = view.findViewById(R.id.submitShopTV);

                submitShopTV.setOnClickListener(MerchantShopDashboard.this);

                isAllDownloaded.clear();
                isAllDownloaded.add(false);
                isAllDownloaded.add(false);

                setupAllAdapters();

                checkIfIsEmpty(true);
            }
        });
        return view;
    }

    private void getThisUserShops(){
        shopListener = GeneralFunction.getFirestoreInstance()
                        .collection(Merchant.URL_MERCHANT)
                        .whereEqualTo(Merchant.MERCHANT_ID,parentActivity.uid)
                        .addSnapshotListener(new EventListener<QuerySnapshot>() {
                            @Override
                            public void onEvent(@javax.annotation.Nullable QuerySnapshot snapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                                if(GeneralFunction.hasError(parentActivity,e,TAG)){
                                    noTV.setText(errorTxt);
                                    noTV.setVisibility(View.VISIBLE);
                                    return;
                                }

                                mixedAllShops.clear();

                                if(snapshots != null && !snapshots.isEmpty()){

                                    ArrayList<Task<QuerySnapshot>> getBusinessHours = new ArrayList<>();
                                    ArrayList<Task<QuerySnapshot>> getProofs = new ArrayList<>();

                                    for(DocumentSnapshot snapshot : snapshots){
                                        //based on this link from here https://stackoverflow.com/questions/50123649/does-tasks-whenallsuccess-guarantee-the-order-in-which-i-pass-tasks-to-it
                                        //the order returned from when all success in task master will be the correct order that i gave
                                        if(snapshot != null && snapshot.exists()){
                                            Merchant tempMerchant = Merchant.snapshotToMerchant(parentActivity,snapshot);
                                            if(tempMerchant != null){
                                                mixedAllShops.add(tempMerchant);
                                                Task<QuerySnapshot> getBusinessHour = snapshot.getReference().collection(Merchant.URL_MERCHANT_BUSINESS_HOUR).get();
                                                Task<QuerySnapshot> getProof = snapshot.getReference().collection(Merchant.URL_SHOP_PROOF).orderBy(Merchant.DATE_CREATED, Query.Direction.DESCENDING).limit(1).get();
                                                getBusinessHours.add(getBusinessHour);
                                                getProofs.add(getProof);
                                            }
                                        }
                                    }

                                    isAllDownloaded.clear();
                                    isAllDownloaded.add(false);
                                    isAllDownloaded.add(false);
                                    getAllBusinessHours(getBusinessHours);
                                    getAllProofs(getProofs);
                                }else{
                                    noTV.setText(emptyTxt);
                                    noTV.setVisibility(View.VISIBLE);
                                    checkIfIsEmpty(false);
                                }
                            }
                        });
    }

    //taskmaster / task master
    private void getAllBusinessHours(ArrayList<Task<QuerySnapshot>> getBusinessHours){
        if(getBusinessHours.isEmpty()){
            handleOnAllDownloaded(0);
            return;
        }
        Task<List<QuerySnapshot>> allTasks = Tasks.whenAllSuccess(getBusinessHours);
        allTasks.addOnSuccessListener(new OnSuccessListener<List<QuerySnapshot>>() {
            @Override
            public void onSuccess(List<QuerySnapshot> querySnapshots) {
                for(int i = 0; i < querySnapshots.size(); i++){
                    QuerySnapshot querySnapshot = querySnapshots.get(i);

                    SparseArray<ArrayList<BusinessHourTime>> businessHourTimes = new SparseArray<>();
                    for(int day = 0; day < NUM_OF_WEEKDAYS; day++){
                        businessHourTimes.put(day,null);
                    }

                    for(int x = 0; x < querySnapshot.size(); x++){
                        ArrayList<BusinessHourTime> allOperatingHours = new ArrayList<>();
                        DocumentSnapshot documentSnapshot = querySnapshot.getDocuments().get(x);

                        if(documentSnapshot.getData() != null && !documentSnapshot.getData().isEmpty()){
                            for(int y = 0; y < documentSnapshot.getData().size(); y++){
                                BusinessHourTime temp = new BusinessHourTime((HashMap<String,Long>) documentSnapshot.getData().get(String.valueOf(y)));
                                allOperatingHours.add(temp);
                            }
                        }
                        businessHourTimes.put(Integer.parseInt(querySnapshot.getDocuments().get(x).getId()),allOperatingHours);
                    }
                    mixedAllShops.get(i).businessHourTimes = businessHourTimes;
                }
                handleOnAllDownloaded(0);
            }
        })
        .addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e(TAG,e.getMessage());
                handleOnAllDownloaded(0);
            }
        });
    }

    private void getAllProofs(ArrayList<Task<QuerySnapshot>> getAllProofs){
        if(getAllProofs.isEmpty()){
            handleOnAllDownloaded(1);
            return;
        }
        Task<List<QuerySnapshot>> allTasks = Tasks.whenAllSuccess(getAllProofs);
        allTasks.addOnSuccessListener(new OnSuccessListener<List<QuerySnapshot>>() {
            @Override
            public void onSuccess(List<QuerySnapshot> querySnapshots) {
                for(int i = 0; i < querySnapshots.size(); i++){
                    QuerySnapshot querySnapshot = querySnapshots.get(i);

                    String proofUrl = null;
                    for(int x = 0; x < querySnapshot.size(); x++){
                        DocumentSnapshot documentSnapshot = querySnapshot.getDocuments().get(x);

                        if(documentSnapshot != null && documentSnapshot.exists() && documentSnapshot.get(Merchant.DRIVER_LICENSE_PIC_URL) != null){
                            proofUrl = (String) documentSnapshot.get(Merchant.DRIVER_LICENSE_PIC_URL);
                        }
                    }
                    mixedAllShops.get(i).merchantLicensePicUrl = proofUrl;
                }
                handleOnAllDownloaded(1);
            }
        }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e(TAG,e.getMessage());
                    handleOnAllDownloaded(1);
                }
            });
    }

    private void handleOnAllDownloaded(int downloadTaskId){
        isAllDownloaded.set(downloadTaskId,true);
        for(Boolean isDownloaded : isAllDownloaded){
            if(!isDownloaded){
                return;
            }
        }

        draftShops.clear();
        veriShops.clear();
        publishShops.clear();
        unpublishShops.clear();
        rejectShops.clear();

        for(Merchant tempMerchant : mixedAllShops){
            if(tempMerchant != null){
                if(tempMerchant.isDraft != null && tempMerchant.isDraft){
                    draftShops.add(tempMerchant);
                }else if(tempMerchant.shopStatus != null){
                    switch (tempMerchant.shopStatus){
                        case Merchant.STATUS_PENDING:
                            veriShops.add(tempMerchant);
                            break;
                        case Merchant.STATUS_REJECTED:
                            rejectShops.add(tempMerchant);
                            break;
                        case Merchant.STATUS_ACCEPTED:
                            if(tempMerchant.isPublish != null){
                                if(tempMerchant.isPublish){
                                    publishShops.add(tempMerchant);
                                }else{
                                    unpublishShops.add(tempMerchant);
                                }
                            }
                            break;
                    }
                }
            }
        }

        checkIfIsEmpty(false);
    }

    private void setupAllAdapters(){
        //*******************************************DRAFT********************************************/
        draftAdapter = new MerchantShopForManageRecyclerGridAdapter(parentActivity, draftShops, new MerchantShopForManageRecyclerGridAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Merchant merchant, int pos) {
                goToEditMode(merchant);
            }
        });
        draftRV.setNestedScrollingEnabled(false);
        draftRV.setAdapter(draftAdapter);
        draftRV.setLayoutManager(new GridLayoutManager(parentActivity, 2));

        //*******************************************PENDING********************************************/
        veriAdapter = new MerchantShopForManageRecyclerGridAdapter(parentActivity, veriShops, new MerchantShopForManageRecyclerGridAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Merchant merchant, int pos) {
                goToShopDetailsDialog(merchant);
            }
        });
        veriRV.setNestedScrollingEnabled(false);
        veriRV.setAdapter(veriAdapter);
        veriRV.setLayoutManager(new GridLayoutManager(parentActivity, 2));

        //*******************************************PUBLISHED********************************************/
        publishAdapter = new MerchantShopForManageRecyclerGridAdapter(parentActivity, publishShops, new MerchantShopForManageRecyclerGridAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Merchant merchant, int pos) {
                goToEditMode(merchant);
            }
        });
        publishRV.setNestedScrollingEnabled(false);
        publishRV.setAdapter(publishAdapter);
        publishRV.setLayoutManager(new GridLayoutManager(parentActivity, 2));

        //*******************************************UNPUBLISHED********************************************/
        unpublishAdapter = new MerchantShopForManageRecyclerGridAdapter(parentActivity, unpublishShops, new MerchantShopForManageRecyclerGridAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Merchant merchant, int pos) {
                goToEditMode(merchant);
            }
        });
        unpublishRV.setNestedScrollingEnabled(false);
        unpublishRV.setAdapter(unpublishAdapter);
        unpublishRV.setLayoutManager(new GridLayoutManager(parentActivity, 2));

        //*******************************************REJECTED********************************************/
        rejectAdapter = new MerchantShopForManageRecyclerGridAdapter(parentActivity, rejectShops, new MerchantShopForManageRecyclerGridAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Merchant merchant, int pos) {
                goToShopDetailsDialog(merchant);
            }
        });
        rejectRV.setNestedScrollingEnabled(false);
        rejectRV.setAdapter(rejectAdapter);
        rejectRV.setLayoutManager(new GridLayoutManager(parentActivity, 2));
    }

    private void goToShopDetailsDialog(Merchant merchant){
        if(parentActivity != null && !parentActivity.isFinishing() && GeneralFunction.isAppInForground(parentActivity.getApplicationContext())){
            FragmentManager fm = parentActivity.getSupportFragmentManager();
            MerchantShopDetailsDialogFragment merchantShopDetailsDialogFragment = MerchantShopDetailsDialogFragment.newInstance(parentActivity,merchant);
            merchantShopDetailsDialogFragment.show(fm, TAG);
        }
    }

    private void goToEditMode(Merchant merchant){
        Fragment applyAsMerchantFragment = ApplyAsMerchantFragment.newInstance(parentActivity,merchant);
        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, applyAsMerchantFragment).addToBackStack(ApplyAsMerchantFragment.TAG).commit();
    }

    private void checkIfIsEmpty(boolean needGetMerchantShop){
        boolean isAllEmpty = true;

        if(draftShops.isEmpty()){
            draftLL.setVisibility(View.GONE);
        }else{
            isAllEmpty = false;
            draftAdapter.notifyDataSetChanged();
            draftLL.setVisibility(View.VISIBLE);
        }

        if(veriShops.isEmpty()){
            veriLL.setVisibility(View.GONE);
        }else{
            isAllEmpty = false;
            veriAdapter.notifyDataSetChanged();
            veriLL.setVisibility(View.VISIBLE);
        }

        if(publishShops.isEmpty()){
            publishLL.setVisibility(View.GONE);
        }else{
            isAllEmpty = false;
            publishAdapter.notifyDataSetChanged();
            publishLL.setVisibility(View.VISIBLE);
        }

        if(unpublishShops.isEmpty()){
            unpublishLL.setVisibility(View.GONE);
        }else{
            isAllEmpty = false;
            unpublishAdapter.notifyDataSetChanged();
            unpublishLL.setVisibility(View.VISIBLE);
        }

        if(rejectShops.isEmpty()){
            rejectLL.setVisibility(View.GONE);
        }else{
            isAllEmpty = false;
            rejectAdapter.notifyDataSetChanged();
            rejectLL.setVisibility(View.VISIBLE);
        }

        if(isAllEmpty){
            if(needGetMerchantShop){
                noTV.setText(loadingTxt);
                noTV.setVisibility(View.VISIBLE);
                getThisUserShops();
            }else{
                noTV.setText(emptyTxt);
                noTV.setVisibility(View.VISIBLE);
            }
        }else{
            noTV.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.setToolbarTitle(parentActivity.getString(R.string.merchant_dashboard));
        parentActivity.selectHighlights();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(shopListener != null){
                    shopListener.remove();
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.submitShopTV:
                Fragment applyAsMerchantFragment = ApplyAsMerchantFragment.newInstance(parentActivity,null);
                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, applyAsMerchantFragment).addToBackStack(ApplyAsMerchantFragment.TAG).commit();
                break;
        }
    }
}