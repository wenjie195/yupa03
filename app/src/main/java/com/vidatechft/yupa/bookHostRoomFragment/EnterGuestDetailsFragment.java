package com.vidatechft.yupa.bookHostRoomFragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ListView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.YupaCustomArrayAdapterMethodTwo;
import com.vidatechft.yupa.classes.BookHomestayDetails;
import com.vidatechft.yupa.classes.BookingHistory;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.classes.Search;
import com.vidatechft.yupa.classes.User;
import com.vidatechft.yupa.utilities.CountryFunction;
import com.vidatechft.yupa.utilities.FilePath;
import com.vidatechft.yupa.utilities.GeneralFunction;
import com.vidatechft.yupa.utilities.KeyboardUtilities;

import static android.app.Activity.RESULT_OK;
import static com.vidatechft.yupa.utilities.GeneralFunction.GlideImageSetting;

public class EnterGuestDetailsFragment extends Fragment implements View.OnClickListener{
    public static final String TAG = EnterGuestDetailsFragment.class.getName();
    private MainActivity parentActivity;
    private Room room;
    private Search search;

    private AppCompatEditText firstNameET,lastNameET, emailET, contactET, totalGuestET, purposeET;
    private CheckBox saveForFutureCB;
    private String filePath;
    private AutoCompleteTextView countryET,countryCodeATV;
    private ImageButton uploadLicenseIB;
    private View showMoreCountry,showMoreCountryCode;
    private ConstraintLayout constraintLayoutRuleSpinnerItem;
    private ListView listViewRuleSpinnerItem;
    private ConstraintLayout mainRoot;
    private Boolean isOpen = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static EnterGuestDetailsFragment newInstance(MainActivity parentActivity, Search search, Room room) {
        EnterGuestDetailsFragment fragment = new EnterGuestDetailsFragment();

        fragment.parentActivity = parentActivity;
        fragment.room = room;
        fragment.search = search;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_enter_guest_details, container, false);
        parentActivity.runOnUiThread(new Runnable() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public void run() {
                parentActivity.hideBtmNav();

                uploadLicenseIB = view.findViewById(R.id.uploadLicenseIB);
                AppCompatTextView confirmTV = view.findViewById(R.id.confirmTV);
                AppCompatTextView resetTV = view.findViewById(R.id.resetTV);
                firstNameET = view.findViewById(R.id.firstNameET);
                lastNameET = view.findViewById(R.id.lastNameET);
                emailET = view.findViewById(R.id.emailET);
                countryET = view.findViewById(R.id.countryET);
                mainRoot = view.findViewById(R.id.mainRoot);
                countryCodeATV = view.findViewById(R.id.countryCodeATV);
                showMoreCountry = view.findViewById(R.id.showMoreCountry);
                showMoreCountryCode = view.findViewById(R.id.showMoreCountryCode);
                contactET = view.findViewById(R.id.contactET);
                totalGuestET = view.findViewById(R.id.totalGuestET);
                purposeET = view.findViewById(R.id.purposeET);
                saveForFutureCB = view.findViewById(R.id.saveForFutureCB);

                constraintLayoutRuleSpinnerItem = view.findViewById(R.id.constraintLayoutSpinnerItem);
                listViewRuleSpinnerItem = view.findViewById(R.id.listViewRuleSpinnerItem);

//                countryET.setOnClickListener(EnterGuestDetailsFragment.this);
                uploadLicenseIB.setOnClickListener(EnterGuestDetailsFragment.this);
                confirmTV.setOnClickListener(EnterGuestDetailsFragment.this);
                resetTV.setOnClickListener(EnterGuestDetailsFragment.this);
                //this is to make edit text scrollable inside scroll view
                //from here https://stackoverflow.com/questions/24428808/how-to-scroll-the-edittext-inside-the-scrollview
                purposeET.setOnTouchListener(new View.OnTouchListener() {
                    public boolean onTouch(View v, MotionEvent event) {
                        if (purposeET.hasFocus()) {
                            v.getParent().requestDisallowInterceptTouchEvent(true);
                            switch (event.getAction() & MotionEvent.ACTION_MASK){
                                case MotionEvent.ACTION_SCROLL:
                                    v.getParent().requestDisallowInterceptTouchEvent(false);
                                    return true;
                            }
                        }
                        return false;
                    }
                });

                populateGuestDetailsFromDatabase();
//                if want to use populate from sp feature, then set saveForFutureCB checked = true in xml and visibility to visible
//                populateGuestDetailsFromSP();
                initiateCountryAutoCompleteTextView();
            }
        });
        return view;
    }

    private void populateGuestDetailsFromDatabase(){
        User user = parentActivity.getThisUser();
        if(user != null){
            if(user.name != null){
                firstNameET.setText(user.name);
            }

            if(user.name != null){
                lastNameET.setText(user.name);
            }

            if(user.email != null){
                emailET.setText(user.email);
            }

            if(user.country != null){
                countryET.setText(user.country);
            }

            if(user.countryCode != null){
                countryCodeATV.setText(user.countryCode);
            }

            if(user.contactNo != null){
                contactET.setText(user.contactNo);
            }
        }
    }

    private void populateGuestDetailsFromSP(){
        BookHomestayDetails tempBookHomestayDetails = GeneralFunction.getBookGuestDetailsFromSharedPreferences(parentActivity, BookingHistory.BOOK_TYPE_HOMESTAY);
        if(tempBookHomestayDetails != null){
            if(tempBookHomestayDetails.firstName != null){
                firstNameET.setText(tempBookHomestayDetails.firstName);
            }

            if(tempBookHomestayDetails.lastName != null){
                lastNameET.setText(tempBookHomestayDetails.lastName);
            }

            if(tempBookHomestayDetails.email != null){
                emailET.setText(tempBookHomestayDetails.email);
            }

            if(tempBookHomestayDetails.country != null){
                countryET.setText(tempBookHomestayDetails.country);
            }

            if(tempBookHomestayDetails.countryCode != null){
                countryCodeATV.setText(tempBookHomestayDetails.countryCode);
            }

            if(tempBookHomestayDetails.contactNo != null){
                contactET.setText(tempBookHomestayDetails.contactNo);
            }
        }
    }

    private boolean isValidInput(BookHomestayDetails bookHomestayDetails){
        return  GeneralFunction.checkAndReturnString(bookHomestayDetails.firstName,parentActivity.getString(R.string.book_homestay_err_enter_first_name),parentActivity)
                &&
                GeneralFunction.checkAndReturnString(bookHomestayDetails.lastName,parentActivity.getString(R.string.book_homestay_err_enter_last_name),parentActivity)
                &&
                GeneralFunction.isValidEmail(bookHomestayDetails.email,parentActivity)
//                GeneralFunction.checkAndReturnString(bookHomestayDetails.country,parentActivity.getString(R.string.book_homestay_err_enter_country),parentActivity)
                &&
                CountryFunction.isHasThisCountry(parentActivity,bookHomestayDetails.country.trim())
                &&
//                GeneralFunction.checkAndReturnString(bookHomestayDetails.contactNo,parentActivity.getString(R.string.book_homestay_err_enter_contact_no),parentActivity)
                CountryFunction.isPhoneNumberValid(parentActivity,bookHomestayDetails.contactNo.trim(),CountryFunction.getSortNameByCode(bookHomestayDetails.countryCode,parentActivity))
                &&
                GeneralFunction.checkAndReturnNum(bookHomestayDetails.totalGuest,parentActivity.getString(R.string.book_homestay_err_enter_noof_guests),parentActivity)
//                &&
//                GeneralFunction.checkAndReturnString(bookHomestayDetails.purpose,parentActivity.getString(R.string.book_homestay_err_enter_purpose),parentActivity)
                &&
                GeneralFunction.checkAndReturnString(filePath,parentActivity.getString(R.string.book_homestay_err_upload_license),parentActivity);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                Uri resultUri = result.getUri();
                filePath = FilePath.getPath(parentActivity, resultUri);
                GeneralFunction.GlideImageSettingBitmap(parentActivity, GeneralFunction.addWatermark(parentActivity.getResources(), BitmapFactory.decodeFile(filePath)),uploadLicenseIB);
            }else if (requestCode == CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE) {
                if (ContextCompat.checkSelfPermission(parentActivity, Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_DENIED) {
                    ActivityCompat.requestPermissions(parentActivity, new String[]{Manifest.permission.CAMERA}, requestCode);
                }
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void setGuestBookingDetails(BookHomestayDetails bookHomestayDetails, DocumentReference guestRef){
        bookHomestayDetails.actionType = null;
        guestRef.set(bookHomestayDetails)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        parentActivity.updateProgress(100f,EnterGuestDetailsFragment.this);
//                        GeneralFunction.handleUploadSuccess(parentActivity,EnterGuestDetailsFragment.this,R.string.book_homestay_upload_success,TAG);
                        //the below if code is basically the same as handleUploadSuccess except it has String.format
                        if(parentActivity != null && !parentActivity.isFinishing() && GeneralFunction.isAppInForground(parentActivity.getApplicationContext())){
                            parentActivity.updateProgress(100f,EnterGuestDetailsFragment.this);
                            parentActivity.controlProgressIndicator(false,EnterGuestDetailsFragment.this);
                            GeneralFunction.openMessageNotificationDialog(parentActivity,String.format(parentActivity.getString(R.string.book_homestay_upload_success),room.accommodationName),R.drawable.notice_good,TAG);
                        }
                        parentActivity.goToHome();
//                        parentActivity.onBackPressed();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        GeneralFunction.handleUploadError(parentActivity,EnterGuestDetailsFragment.this,R.string.book_homestay_err_uploading_license,TAG);
                    }
                });
    }

    private void uploadLicense(final BookHomestayDetails bookHomestayDetails){
        final DocumentReference guestRef = GeneralFunction.getFirestoreInstance()
                .collection(BookHomestayDetails.URL_BOOK_GUEST)
                .document();

        final StorageReference storageReference = GeneralFunction.getFirebaseStorageInstance()
                .child(BookHomestayDetails.URL_BOOK_GUEST)
                .child(parentActivity.uid)
                .child(guestRef.getId())
                .child(BookHomestayDetails.URL_GUEST_LICENSE);

        UploadTask uploadTask = storageReference.putBytes(GeneralFunction.getCompressedBitmapWithWatermark(parentActivity,filePath));

        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                GeneralFunction.handleUploadError(parentActivity,EnterGuestDetailsFragment.this,R.string.book_homestay_err_uploading_license,TAG);
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(final UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri downloadUri) {
                        if(downloadUri != null){
                            bookHomestayDetails.licenseUrl = downloadUri.toString();
                            setGuestBookingDetails(bookHomestayDetails,guestRef);
                        }else{
                            GeneralFunction.handleUploadError(parentActivity,EnterGuestDetailsFragment.this,R.string.book_homestay_err_uploading_license,TAG);
                        }
                    }
                });
            }
        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * 90) + 10;
                parentActivity.updateProgress((float) totalProgress,EnterGuestDetailsFragment.this);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.homestay_booking_action_bar6));
                parentActivity.selectHotel();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.confirmTV:
                String firstName = firstNameET.getText().toString().trim();
                String lastName = lastNameET.getText().toString().trim();
                String email = emailET.getText().toString().trim();
                String country = countryET.getText().toString().trim();
                String countryCode = countryCodeATV.getText().toString().trim();
                String contactNo = contactET.getText().toString().trim();
                String totalGuestStr = totalGuestET.getText().toString().trim();
                int totalGuest = 0;
                try {
                    totalGuest = Integer.parseInt(totalGuestStr);
                }catch (NumberFormatException e){
                    Log.e("Invalid num",e.getMessage());
                }
                String purpose = purposeET.getText().toString().trim();

                BookHomestayDetails bookHomestayDetails = new BookHomestayDetails(firstName,lastName,email,country,countryCode,contactNo,totalGuest,purpose, BookingHistory.BOOK_TYPE_HOMESTAY,search);
                bookHomestayDetails.userId = parentActivity.uid;
                bookHomestayDetails.hostId = room.hostId;
                bookHomestayDetails.targetId = room.id;

                if(saveForFutureCB.isChecked()){
                    GeneralFunction.saveGuestDetailsToSP(parentActivity, bookHomestayDetails, BookingHistory.BOOK_TYPE_HOMESTAY);
                }

                if(isValidInput(bookHomestayDetails)){
                    parentActivity.initializeProgressIndicator(parentActivity.getString(R.string.book_homestay_uploading),false,this);
                    parentActivity.controlProgressIndicator(true,this);
                    uploadLicense(bookHomestayDetails);
                }
                break;
            case R.id.resetTV:
                firstNameET.setText("");
                lastNameET.setText("");
                emailET.setText("");
                countryET.setText("");
                countryCodeATV.setText("");
                contactET.setText("");
                totalGuestET.setText("");
                purposeET.setText("");
                GeneralFunction.GlideImageSettingOwnDrawable(parentActivity,R.drawable.ic_upload_placeholder,uploadLicenseIB);
                filePath = null;
                break;
            case R.id.uploadLicenseIB:
                GeneralFunction.showFileChooser(parentActivity,this);
                break;
            case R.id.countryET:
//                GeneralFunction.slideUpAnimationForListView(parentActivity,constraintLayoutRuleSpinnerItem, listViewRuleSpinnerItem,R.array.country_list,countryET);
//                MainActivity.isOpen = true;
                break;
        }
    }

    private void initiateCountryAutoCompleteTextView(){
        final YupaCustomArrayAdapterMethodTwo countryAdapter = new YupaCustomArrayAdapterMethodTwo(parentActivity,android.R.layout.simple_dropdown_item_1line,CountryFunction.getCountry(parentActivity));
        //Programmatically set the auto complete textview drop down height
        countryET.setDropDownHeight(getResources().getDisplayMetrics().widthPixels/2);
        countryET.setThreshold(1);
        countryET.setAdapter(countryAdapter);

        countryET.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                countryET.setText(CountryFunction.getCountry(parentActivity).get(position));
                KeyboardUtilities.hideSoftKeyboard(parentActivity);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        // Add Text Change Listener to EditText
        countryET.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // Call back the Adapter with current character to Filter
                countryAdapter.getFilter().filter(s.toString());
                String countryName = s.toString();
//                if(CountryFunction.isCountryExist(countryName,parentActivity)){
//                    //remove the list when the country changes
//                }
                String tempCountryCode = CountryFunction.getCountryCodeWithName(countryName, parentActivity);
                String tempSortName = CountryFunction.getCountryCodeWithSortName(countryName,parentActivity);
                if(tempSortName != null && tempCountryCode != null){
                    String sortNameCode = tempSortName + " +" + tempCountryCode;
                    countryCodeATV.setText(sortNameCode);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().isEmpty()){
                    //remove the list when the country changes
                    final YupaCustomArrayAdapterMethodTwo countryAdapter = new YupaCustomArrayAdapterMethodTwo(parentActivity,android.R.layout.simple_dropdown_item_1line,CountryFunction.getCountry(parentActivity));
                    countryET.setThreshold(1);
                    countryET.setAdapter(countryAdapter);
                    countryET.showDropDown();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,int after) {
            }

        });

        countryET.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                KeyboardUtilities.hideSoftKeyboard(parentActivity);
            }
        });

        showMoreCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ifSoftKeyboardOpen()){
                    KeyboardUtilities.hideSoftKeyboard(parentActivity);
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        focusOnView(countryET);
                        countryET.requestFocus();
                        KeyboardUtilities.showSoftKeyboard(parentActivity,countryET);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                countryET.showDropDown();
                            }
                        },300);
                    }
                },100);

            }
        });

        //////////////////////////////////////////////////////////////////////////////////// initialize country code here ////////////////////////////////////////////////////////////////////////////////////

        final YupaCustomArrayAdapterMethodTwo countryCodeAdapter = new YupaCustomArrayAdapterMethodTwo(parentActivity,android.R.layout.simple_dropdown_item_1line,CountryFunction.getCountrySortNameAndCodeList(parentActivity));
        countryCodeATV.setDropDownHeight(getResources().getDisplayMetrics().widthPixels/2);
        countryCodeATV.setThreshold(1);
        countryCodeATV.setAdapter(countryCodeAdapter);
        showMoreCountryCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        KeyboardUtilities.showSoftKeyboard(parentActivity,countryCodeATV);
                        countryCodeATV.showDropDown();
                    }
                },200);

            }
        });

        countryCodeATV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                KeyboardUtilities.hideSoftKeyboard(parentActivity);
                contactET.requestFocus();
                KeyboardUtilities.showSoftKeyboard(parentActivity,contactET);
            }
        });


        // Add Text Change Listener to EditText
        countryCodeATV.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // Call back the Adapter with current character to Filter
                countryCodeAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().isEmpty()){
                    final YupaCustomArrayAdapterMethodTwo countryAdapter = new YupaCustomArrayAdapterMethodTwo(parentActivity,android.R.layout.simple_dropdown_item_1line,CountryFunction.getCountrySortNameAndCodeList(parentActivity));
                    countryCodeATV.setThreshold(1);
                    countryCodeATV.setAdapter(countryAdapter);
                    countryCodeATV.showDropDown();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,int after) {
            }

        });

        countryCodeATV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                KeyboardUtilities.hideSoftKeyboard(parentActivity);
                String getCountryCode = countryCodeATV.getText().toString();
                countryCodeATV.setText(getCountryCode);
                contactET.requestFocus();
                KeyboardUtilities.showSoftKeyboard(parentActivity,contactET);

            }
        });
    }

    private boolean ifSoftKeyboardOpen(){

        mainRoot.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int heightDiff = mainRoot.getRootView().getHeight() - mainRoot.getHeight();
                if (heightDiff > dpToPx(parentActivity, 200)) { // if more than 200 dp, it's probably a keyboard...
                    // ... do something here
                    isOpen = true;
                }
                else{
                    isOpen = false;
                }
            }
        });
        if(isOpen != null && isOpen){
            return true;
        }
        else{
            return false;
        }
    }

    private final void focusOnView(final AutoCompleteTextView autoCompleteTextView){
        mainRoot.post(new Runnable() {
            @Override
            public void run() {
                mainRoot.scrollTo(0, autoCompleteTextView.getBottom());
            }
        });
    }

    public static float dpToPx(Context context, float valueInDp) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueInDp, metrics);
    }
}