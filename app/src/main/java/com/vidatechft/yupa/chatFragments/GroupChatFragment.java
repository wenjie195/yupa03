package com.vidatechft.yupa.chatFragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.GroupChatAdapter;
import com.vidatechft.yupa.classes.GroupChat;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class GroupChatFragment extends Fragment
{
    public static final String TAG = GroupChatFragment.class.getName();
    private MainActivity parentActivity;
    private String lastToolbarTitle;
    private ChatContainerFragment parentFragment;

    private RecyclerView groupChatRV;
    private ScrollView scrollSV;

    private ArrayList<GroupChat> groupChatGroupList = new ArrayList<>();
    private ListenerRegistration groupChatListener;
    private GroupChatAdapter groupChatAdapter;

    public GroupChatFragment()
    {

    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

    }

    public static GroupChatFragment newInstance(MainActivity parentActivity , ChatContainerFragment parentFragment , String lastToolbarTitle)
    {
        GroupChatFragment fragment = new GroupChatFragment();
        fragment.parentActivity = parentActivity;
        fragment.lastToolbarTitle = lastToolbarTitle;
        fragment.parentFragment = parentFragment;
        return fragment;
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        if(groupChatListener != null)
        {
            groupChatListener.remove();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState)
    {
        final View view = inflater.inflate(R.layout.fragment_group_chat, container, false);

        parentActivity.runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                scrollSV =  view.findViewById(R.id.scrollSV);
                groupChatRV  = view.findViewById(R.id.groupChatRV);

                setUpGridAdapter();

                if(groupChatGroupList.size() <= 0)
                {
                    getGroupChat();
                }

            }
        });
        return  view;
    }

    private void getGroupChat()
    {
        //todo : Get all chat groups inside database
        groupChatListener = GeneralFunction.getFirestoreInstance()
                .collection(GroupChat.URL_GROUP_CHAT)
                .addSnapshotListener(new EventListener<QuerySnapshot>()
        {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots , @Nullable final FirebaseFirestoreException e)
            {
                groupChatGroupList.clear();

                GroupChat addNewGroup = new GroupChat(true);

                groupChatGroupList.add(addNewGroup);

                if (queryDocumentSnapshots != null && !queryDocumentSnapshots.isEmpty())
                {
                    for (QueryDocumentSnapshot snapshot : queryDocumentSnapshots)
                    {
                        //todo : Get snapshot data
                        final GroupChat tempGroupChat = new GroupChat(snapshot);

                        //todo : Get group member ID`s
                        snapshot.getReference().collection(GroupChat.URL_GROUP_CHAT_MEMBERS).get()
                            .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>()
                            {
                                @Override
                                public void onComplete(@NonNull Task<QuerySnapshot> task)
                                {
                                    if (task.isSuccessful())
                                    {

                                        for (QueryDocumentSnapshot document : task.getResult())
                                        {
                                            //todo : Get other members` uid
                                            tempGroupChat.groupMemberUid.add(document.getId());

                                            //todo : Get their membership status
                                            Map<String,Object> membershipStatus =  new HashMap();

                                            membershipStatus.put(GroupChat.GROUP_CHAT_USER_STATUS,document.getString(GroupChat.GROUP_CHAT_USER_STATUS));
                                            membershipStatus.put(GroupChat.GROUP_CHAT_DATE_CREATED,document.getTimestamp(GroupChat.GROUP_CHAT_DATE_CREATED));
                                            membershipStatus.put(GroupChat.GROUP_CHAT_LAST_UPDATED,document.getTimestamp(GroupChat.GROUP_CHAT_LAST_UPDATED));

                                            tempGroupChat.userMembershipDetails.put(document.getId(),membershipStatus);
                                        }
                                        //todo : Check whether the group is public or not

                                        //todo : if the group is public
                                        if (tempGroupChat.isPublic.equals(true))
                                        {
                                            groupChatGroupList.add(tempGroupChat);
                                        }
                                        //todo : if the group is private but he is a member of that private group
                                        else if (tempGroupChat.isPublic.equals(false))
                                        {
                                            if (tempGroupChat.groupMemberUid.contains(parentActivity.uid))
                                            {
                                                groupChatGroupList.add(tempGroupChat);
                                            }
                                        }
                                        parentActivity.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run()
                                            {
                                                groupChatAdapter.notifyDataSetChanged();
                                            }
                                        });
                                    }
                                }
                            });
                    }
                }
                else
                {
                    parentActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            groupChatAdapter.notifyDataSetChanged();
                        }
                    });
                }
            }
        });
    }

    private void setUpGridAdapter()
    {
        groupChatAdapter = new GroupChatAdapter(parentActivity,this, groupChatGroupList,lastToolbarTitle);
        groupChatRV.setNestedScrollingEnabled(false);
        groupChatRV.setAdapter(groupChatAdapter);
        groupChatRV.setLayoutManager(new GridLayoutManager(parentActivity, 2));
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                parentActivity.setToolbarTitle(parentActivity.getString(R.string.group_chat_toolbar_title));
                parentActivity.selectChat();
            }
        });
    }
}
