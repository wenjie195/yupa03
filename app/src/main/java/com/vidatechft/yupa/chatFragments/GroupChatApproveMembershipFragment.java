package com.vidatechft.yupa.chatFragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.MembershipAdapter;
import com.vidatechft.yupa.classes.GroupChat;
import com.vidatechft.yupa.classes.User;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

public class GroupChatApproveMembershipFragment extends Fragment
{
    public static final String TAG = GroupChatMessageFragment.class.getName();
    private MainActivity parentActivity ;
    private String groupID;
    private String loadingTxt,emptyTxt,errorTxt;
    private RecyclerView membershipRV;
    private TextView noMembershipTV;

    private ArrayList<Map> membershipList = new ArrayList<>();
    private MembershipAdapter membershipAdapter;
    private ListenerRegistration membershipListener,getUserDataListener;
    private List<Map> forDisplayChatDataset = new ArrayList<>();

    public GroupChatApproveMembershipFragment()
    {
        // Required empty public constructor
    }

    public static GroupChatApproveMembershipFragment newInstance(MainActivity parentActivity , String groupID)
    {
        GroupChatApproveMembershipFragment fragment = new GroupChatApproveMembershipFragment();
        fragment.parentActivity = parentActivity;
        fragment.groupID = groupID;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        if (membershipListener != null)
        {
            membershipListener.remove();
        }
        if (getUserDataListener != null)
        {
            getUserDataListener.remove();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater , ViewGroup container ,
            Bundle savedInstanceState)
    {
        final View view =  inflater.inflate(R.layout.fragment_group_chat_approve_membership , container , false);

        parentActivity.runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {

                loadingTxt = parentActivity.getString(R.string.chat_group_approve_loading_membership_request);
                emptyTxt = parentActivity.getString(R.string.chat_group_approve_no_membership_request);
                errorTxt = parentActivity.getString(R.string.chat_group_approve_error_membership_request);

                membershipRV  = view.findViewById(R.id.membershipRV);
                noMembershipTV  = view.findViewById(R.id.noMembershipTV);

                setUpGridAdapter();

                if(membershipList.size() <= 0)
                {
                    noMembershipTV.setText(loadingTxt);
                    getGroupMembership();
                }
                else
                {
                    noMembershipTV.setVisibility(View.GONE);
                }
                
            }
        });


        return view;
    }

    private void getGroupMembership()
    {
        parentActivity.runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {

                membershipListener = GeneralFunction.getFirestoreInstance()
                        .collection(GroupChat.URL_GROUP_CHAT)
                        .document(groupID)
                        .collection(GroupChat.GROUP_CHAT_IS_PENDING)
                        .addSnapshotListener(new EventListener<QuerySnapshot>() {
                            @Override
                            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots , @Nullable FirebaseFirestoreException e)
                            {
                                if( e != null )
                                {
                                    Log.w(TAG, "Listen failed.", e);
                                    noMembershipTV.setVisibility(View.VISIBLE);
                                    noMembershipTV.setText(errorTxt);
                                    return;
                                }
                                if(queryDocumentSnapshots != null && !queryDocumentSnapshots.isEmpty())
                                {
                                    membershipList.clear();

                                    for ( final QueryDocumentSnapshot qd: queryDocumentSnapshots )
                                    {
                                        Map<String,Object> membershipMap = new HashMap<>();

                                        membershipMap.put(User.ID,qd.getId());
                                        membershipMap.put("invitedBy",qd.get("invitedBy"));
                                        membershipMap.put(GroupChat.GROUP_CHAT_DATE_CREATED,qd.getTimestamp(GroupChat.GROUP_CHAT_DATE_CREATED));

                                        membershipList.add(membershipMap);
                                    }
                                    getUserDetails();
                                }
                                if (queryDocumentSnapshots.isEmpty())
                                {
                                        noMembershipTV.setText(emptyTxt);
                                        noMembershipTV.setVisibility(View.VISIBLE);
                                }

                            }
                        });
            }
        });
    }

    private void getUserDetails()
    {
        parentActivity.runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                getUserDataListener = GeneralFunction.getFirestoreInstance()
                        .collection(User.URL_USER)
                        .addSnapshotListener(new EventListener<QuerySnapshot>() {
                            @Override
                            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots , @Nullable FirebaseFirestoreException e)
                            {
                                if (queryDocumentSnapshots != null && !queryDocumentSnapshots.isEmpty())
                                {
                                    for (DocumentSnapshot document : queryDocumentSnapshots)
                                    {
                                        String id = document.getId();

                                        for (int i = 0; i < membershipList.size();i++)
                                        {
                                            String id2 = (String) membershipList.get(i).get(User.ID);

                                            if (TextUtils.equals(id,id2))
                                            {
                                                User tempUser = document.toObject(User.class);
                                                tempUser.id = id;
                                                membershipList.get(i).put("userDetails",tempUser);
                                            }
                                        }
                                    }

                                    forDisplayChatDataset.addAll(membershipList);
                                    if(forDisplayChatDataset.size() != 1)
                                    {
                                        sortChats();
                                    }
                                    forDisplayChatDataset.clear();
                                    membershipAdapter.notifyDataSetChanged();
                                    noMembershipTV.setVisibility(View.GONE);
                                }
                            }
                        });
            }
        });
    }

    private void sortChats()
    {
        Collections.sort(forDisplayChatDataset, new Comparator<Map>() {
            @Override public int compare(Map map1, Map map2)
            {
                Timestamp time1 = (Timestamp) map1.get(GroupChat.GROUP_CHAT_DATE_CREATED);
                Timestamp time2 = (Timestamp) map2.get(GroupChat.GROUP_CHAT_DATE_CREATED);
                //because new chat hor meaning the chat 2 dont have date updated will crash, this is to prevent that
                if(map1.get(GroupChat.GROUP_CHAT_DATE_CREATED) != null || map1.get(GroupChat.GROUP_CHAT_DATE_CREATED) != "null"
                        && map1.get(GroupChat.GROUP_CHAT_DATE_CREATED) != null || map2.get(GroupChat.GROUP_CHAT_DATE_CREATED) != "null")
                {
                    return (int)(time2.getSeconds() - time1.getSeconds()); // in ascending order
                }
                else
                {
                    return -1;//if asc then return 1, if desc then -1
                }
            }
        });
    }

    private void setUpGridAdapter()
    {
        membershipAdapter = new MembershipAdapter(parentActivity, membershipList,groupID,this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(parentActivity);
        membershipRV.setNestedScrollingEnabled(false);
        membershipRV.setAdapter(membershipAdapter);
        membershipRV.setLayoutManager(linearLayoutManager);
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.chat_group_toolbar_5));
                parentActivity.selectChat();
            }
        });
    }
}
