package com.vidatechft.yupa.chatFragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.GroupChatGroupPictureAdapter;
import com.vidatechft.yupa.classes.GroupChat;
import com.vidatechft.yupa.classes.Message;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;


public class GroupChatMediaFragment extends Fragment
{
    public static final String TAG = GroupChatMediaFragment.class.getName();
    private MainActivity parentActivity;
    private String groupID;
    private RecyclerView groupChatMediaRV;
    private TextView loadingImageTV;
    private ListenerRegistration groupChatListener;
    private List<Message> getMediaOnly = new ArrayList<>();
    private GroupChatGroupPictureAdapter groupChatGroupPictureAdapter;
    private ArrayList <String> imagesList = new ArrayList<>();

    public GroupChatMediaFragment()
    {
        // Required empty public constructor
    }


    public static GroupChatMediaFragment newInstance(String groupID, MainActivity parentActivity)
    {
        GroupChatMediaFragment fragment = new GroupChatMediaFragment();
        fragment.groupID = groupID;
        fragment.parentActivity = parentActivity;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        if (groupChatListener != null)
        {
            groupChatListener.remove();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater , ViewGroup container ,
            Bundle savedInstanceState)
    {
        View view =  inflater.inflate(R.layout.fragment_group_chat_media , container , false);
        // Inflate the layout for this fragment

        groupChatMediaRV = view.findViewById(R.id.groupChatMediaRV);
        loadingImageTV = view.findViewById(R.id.loadingImageTV);

        loadingImageTV.setVisibility(View.VISIBLE);

        //todo : Setting up Grid Adapters
        setUpGridAdapter();

        //todo : If the getMediaOnly have a list
        if(getMediaOnly.size() <= 0)
        {
            getAllMedia();
        }
        else
        {
            loadingImageTV.setVisibility(View.GONE);
        }

        return view;
    }

    private void getAllMedia()
    {
        groupChatListener = GeneralFunction.getFirestoreInstance()
                .collection(GroupChat.URL_GROUP_CHAT)
                .document(groupID)
                .collection(Message.URL_MSG)
                .orderBy(Message.DATE_CREATED, Query.Direction.DESCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots , @Nullable FirebaseFirestoreException e)
                    {
                        if (e != null) {
                            Log.w(TAG, "Listen failed.", e);
                            return;
                        }
                        getMediaOnly.clear();
                        if(queryDocumentSnapshots.isEmpty())
                        {
                            loadingImageTV.setText("Sorry, but there was no media inside the group chat");
                        }
                        else if(!queryDocumentSnapshots.isEmpty() && queryDocumentSnapshots != null)
                        {
                            for (QueryDocumentSnapshot snapshot : queryDocumentSnapshots)
                            {
                                Message tempMessage = snapshot.toObject(Message.class);
                                if(tempMessage.isImg == true && !tempMessage.img.equals(null))
                                {

                                    getMediaOnly.add(tempMessage);
                                    tempMessage.pos = getMediaOnly.indexOf(tempMessage);
                                }
                            }
                            parentActivity.runOnUiThread(new Runnable()
                            {
                                @Override
                                public void run() {
                                    loadingImageTV.setVisibility(View.GONE);
                                    groupChatGroupPictureAdapter.notifyDataSetChanged();

                                }
                            });
                        }
                    }
                });
    }

    private void setUpGridAdapter()
    {
        groupChatGroupPictureAdapter = new GroupChatGroupPictureAdapter(parentActivity,this,getMediaOnly);
        groupChatMediaRV.setNestedScrollingEnabled(false);
        groupChatMediaRV.setAdapter(groupChatGroupPictureAdapter);
        groupChatMediaRV.setLayoutManager(new GridLayoutManager(parentActivity, 2));
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.chat_group_toolbar_3));
                parentActivity.selectChat();
            }
        });
    }
}
