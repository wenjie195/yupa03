package com.vidatechft.yupa.chatFragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.theartofdev.edmodo.cropper.CropImage;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.MessageGroupAdapter;
import com.vidatechft.yupa.classes.GroupChat;
import com.vidatechft.yupa.classes.Message;
import com.vidatechft.yupa.classes.User;
import com.vidatechft.yupa.dialogFragments.ShowImageDialogFragment;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.FilePath;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import static android.app.Activity.RESULT_OK;


public class GroupChatMessageFragment extends Fragment
{
    public static final String TAG = GroupChatMessageFragment.class.getName();
    private MainActivity parentActivity ;
    private String groupID;
    private Map<String, Object> currentUserMembershipDetails;

    private RecyclerView msgRV;
    private TextView noMsgTV,displayGroupNameTV,displayNoOfGroupMembersTV,inv1TV,displayWhoMembersTV;
    private ImageView groupPicIV,sendIV,groupMenuIV,inv1IV;
    private RelativeLayout groupRL;
    private EditText msgET;
    private ImageButton addImgIB,emojiIB,voiceIB;

    private ListenerRegistration chatListener,groupListener ,membersListener;
    private MessageGroupAdapter messageGroupAdapter;

    private List<String> membersUID = new ArrayList<>();
    private List<Message> messageDataset = new ArrayList<>();
    private List<Message> pictureDataset = new ArrayList<>();

    private int messageCnt = 0;
    private String filePath;

    FirebaseFirestore db = FirebaseFirestore.getInstance();

    public GroupChatMessageFragment()
    {
        // Required empty public constructor
    }

    public static GroupChatMessageFragment newInstance(MainActivity parentActivity , String groupID , Map<String, Object> currentUserMembershipDetails)
    {
        GroupChatMessageFragment fragment = new GroupChatMessageFragment();
        fragment.groupID = groupID;
        fragment.parentActivity = parentActivity;
        fragment.currentUserMembershipDetails = currentUserMembershipDetails;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        if(chatListener != null)
        {
            chatListener.remove();
        }
        if (groupListener != null)
        {
            groupListener.remove();
        }
        if (membersListener != null)
        {
            membersListener.remove();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater , ViewGroup container ,
            Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_group_chat_message , container , false);

        parentActivity.runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                msgRV = view.findViewById(R.id.msgRV);
                noMsgTV = view.findViewById(R.id.noMsgTV);
                displayGroupNameTV = view.findViewById(R.id.displayGroupNameTV);
                displayNoOfGroupMembersTV = view.findViewById(R.id.displayNoOfGroupMembersTV);
                groupPicIV = view.findViewById(R.id.groupPicIV);
                groupRL = view.findViewById(R.id.groupRL);
                msgET = view.findViewById(R.id.msgET);
                sendIV = view.findViewById(R.id.sendIV);
                addImgIB = view.findViewById(R.id.addImgIB);
                emojiIB = view.findViewById(R.id.emojiIB);
                emojiIB.setVisibility(View.GONE);
                voiceIB = view.findViewById(R.id.voiceIB);
                voiceIB.setVisibility(View.GONE);
                inv1TV = view.findViewById(R.id.inv1TV);
                inv1IV = view.findViewById(R.id.inv1IV);
                groupMenuIV = view.findViewById(R.id.groupMenuIV);
                displayWhoMembersTV = view.findViewById(R.id.displayWhoMembersTV);

                //todo : to re-color the text on the bottom to invite the admin
                String fword = "Invite friends";
                String sword = " to join your group chat";
                String fontOpen = "<u><font color='#EE0000'>";
                String fontClose = "</font></u>";

                inv1TV.setText(Html.fromHtml(fontOpen + fword + fontClose + sword));
                inv1IV.setVisibility(View.VISIBLE);
                inv1TV.setVisibility(View.VISIBLE);

                setupAdapter();

                // todo : Set up group chat details listener
                setupGroupChatListener();


                if(messageDataset.size() <=0 && pictureDataset.size() <=0)
                {
                    // todo : Set up message details listener
                    setupMessageListener();
                }
                else
                {
                    inv1TV.setVisibility(View.GONE);
                    inv1IV.setVisibility(View.GONE);
                    noMsgTV.setVisibility(View.GONE);
                }

                //todo : Send message using this send button
                sendIVclick();

                //todo : The Icon Buttons ; addImgIB, emojiIB, voiceIB preparing the media
                insertMedia();

                //todo : Top Right Menu
                getMenu(groupID);

            }
        });

        return view;
    }

    private void getMenu(final String groupID)
    {
        groupMenuIV.setOnClickListener(new DebouncedOnClickListener(500)
        {
            @Override
            public void onDebouncedClick(View v)
            {
                final PopupMenu popup = new PopupMenu(parentActivity, groupMenuIV);
                popup.getMenuInflater()
                        .inflate(R.menu.groupchat_options, popup.getMenu());

                popup.setGravity(Gravity.END);
                popup.getMenu().getItem(3).setVisible(false);
                popup.getMenu().getItem(4).setVisible(false);

                if(currentUserMembershipDetails.containsValue("admin"))
                {

                }
                else
                {
                    popup.getMenu().getItem(1).setVisible(false);
                    popup.getMenu().getItem(5).setVisible(false);
                }
                //popup.getMenu().getItem(1).setVisible(false); Todo hide option for admin use
                //Todo registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener()
                {
                    public boolean onMenuItemClick(MenuItem item)
                    {
                        //Todo Group Homepage
                        if(item.getTitle().toString().equals("Group Page"))
                        {
                            boolean isMember = true;
                            Fragment groupChatHomeFragment = GroupChatHomeFragment.newInstance(parentActivity,isMember,groupID);
                            GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, groupChatHomeFragment).addToBackStack(GroupChatHomeFragment.TAG).commit();
                        }
                        //Todo Info
                        if(item.getTitle().toString().equals("Edit Group Info"))
                        {
                            Fragment groupChatCreateFragment = GroupChatCreateFragment.newInstance(parentActivity,false,currentUserMembershipDetails,groupID);
                            GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, groupChatCreateFragment).addToBackStack(GroupChatCreateFragment.TAG).commit();
                        }
                        //Todo Media
                        if(item.getTitle().toString().equals("Group Media"))
                        {
                            Fragment groupChatMediaFragment = GroupChatMediaFragment.newInstance(groupID,parentActivity);
                            GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, groupChatMediaFragment).addToBackStack(GroupChatMediaFragment.TAG).commit();
                        }
                        //Todo Search
                        if(item.getTitle().toString().equals("Search"))
                        {
                            Toast.makeText(parentActivity , "Search" , Toast.LENGTH_SHORT).show();
                            //                            Fragment groupChatMediaFragment = GroupChatMediaFragment.newInstance(groupID,parentActivity);
                            //                            GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, groupChatMediaFragment).addToBackStack(GroupChatMediaFragment.TAG).commit();
                        }
                        //Todo Mute
                        if(item.getTitle().toString().equals("Mute"))
                        {
                            Toast.makeText(parentActivity , "Mute" , Toast.LENGTH_SHORT).show();
                            //                            Fragment groupChatMediaFragment = GroupChatMediaFragment.newInstance(groupID,parentActivity);
                            //                            GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, groupChatMediaFragment).addToBackStack(GroupChatMediaFragment.TAG).commit();
                        }
                        //Todo approve membership
                        if(item.getTitle().toString().equals("Approve Membership"))
                        {
                            //                            Toast.makeText(parentActivity , "ajasklasdjf;lsd" , Toast.LENGTH_SHORT).show();
                            Fragment groupChatApproveMembershipFragment = GroupChatApproveMembershipFragment.newInstance(parentActivity,groupID);
                            GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, groupChatApproveMembershipFragment).addToBackStack(GroupChatApproveMembershipFragment.TAG).commit();

                        }
                        //Todo invite
                        if(item.getTitle().toString().equals("Invite"))
                        {
                            Fragment groupChatInviteFriends = GroupChatInviteFriendsFragment.newInstance(groupID,parentActivity,currentUserMembershipDetails);
                            GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, groupChatInviteFriends).addToBackStack(GroupChatInviteFriendsFragment.TAG).commit();
                        }


                        return true;
                    }
                });

                popup.show(); //showing popup menu
            }
        });

    }

    private void setupGroupChatListener()
    {
        groupListener = GeneralFunction.getFirestoreInstance()
            .collection(GroupChat.URL_GROUP_CHAT).document(groupID).addSnapshotListener(new EventListener<DocumentSnapshot>()
                {
                    @Override
                    public void onEvent(@Nullable final DocumentSnapshot documentSnapshot , @Nullable FirebaseFirestoreException e)
                    {
                        if (documentSnapshot != null)
                        {
                            final GroupChat tempGroupChat =  new GroupChat(documentSnapshot);

                            final String tempPicURL;
                            // todo : To detect null in photos
                            if(tempGroupChat.groupChatPic == null)
                            {
                                tempPicURL = "";
                            }
                            else
                            {
                                tempPicURL = tempGroupChat.groupChatPic;
                            }
                            if(tempPicURL != "")
                            {
                                GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(tempPicURL), groupPicIV);
                            }
                            // todo : Display group picture
                            if(tempGroupChat.groupChatName != null)
                            {
                                displayGroupNameTV.setText(tempGroupChat.groupChatName);
                            }
                            // todo : this
                            documentSnapshot.getReference().collection(GroupChat.URL_GROUP_CHAT_MEMBERS).addSnapshotListener(new EventListener<QuerySnapshot>()
                            {
                                @Override
                                public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots , @Nullable FirebaseFirestoreException e)
                                {
                                    final List<String> membersID = new ArrayList<>();
                                    String singularPlurals;

                                    for(DocumentSnapshot dc : queryDocumentSnapshots)
                                    {
                                        membersID.add(dc.getId());
                                    }

                                    if(membersID.size() == 1)
                                    {
                                        singularPlurals = membersID.size()+" member";
                                    }
                                    else if(membersID.size() > 1)
                                    {
                                        singularPlurals = membersID.size()+" members";
                                    }
                                    else
                                    {
                                        singularPlurals = "defaultNoOfMembers";
                                    }

                                    displayNoOfGroupMembersTV.setText(singularPlurals);

                                    GeneralFunction.getFirestoreInstance().collection(User.URL_USER)
                                            .get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>()
                                    {
                                        @Override
                                        public void onComplete(@NonNull Task<QuerySnapshot> task)
                                        {
                                             Map <String,User> userDetailsMap = new HashMap<>();
                                             int counter = 0;
                                             String n = ", ";
                                             

                                            if(task.isSuccessful())
                                            {
                                                String k = "";
                                                for (DocumentSnapshot documentSnapshot1:task.getResult())
                                                {
                                                    User tempUser = documentSnapshot1.toObject(User.class);

                                                    if(membersID.contains(documentSnapshot1.getId()))
                                                    {
//                                                        userDetailsMap.put(tempUser.id,tempUser);

                                                        counter++;

                                                        k = k + tempUser.name;

                                                        if(membersID.size() > 2)
                                                        {
                                                            if (counter < 2)
                                                            {
                                                                k += n;
                                                            }
                                                            else if(counter == 2)
                                                            {
                                                                k += " ";
                                                                displayWhoMembersTV.setText(k + "and other "+(membersID.size() - 2)+ " member");
                                                            }
                                                        }
                                                        else if(membersID.size() <= 2)
                                                        {
                                                            if (membersID.size() == 2)
                                                            {
                                                                if (counter < 2)
                                                                {
                                                                    k += " and ";
                                                                }
                                                                else if(counter == 2)
                                                                {
                                                                    k += " ";
                                                                    displayWhoMembersTV.setText(k + "are members.");
                                                                }
                                                            }
                                                            if (membersID.size() == 1)
                                                            {
                                                                if(membersID.contains(parentActivity.uid))
                                                                displayWhoMembersTV.setText("You are the only member inside this group.");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    });
                                }
                            });
                            groupRL.setOnClickListener(new DebouncedOnClickListener(500) {
                                @Override
                                public void onDebouncedClick(View v)
                                {
                                    Fragment groupChatHomeFragment = GroupChatHomeFragment.newInstance(parentActivity,true,groupID);
                                    GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, groupChatHomeFragment).addToBackStack(GroupChatHomeFragment.TAG).commit();
                                }
                            });
                            inv1TV.setOnClickListener(new DebouncedOnClickListener(1500) {
                                @Override
                                public void onDebouncedClick(View v)
                                {
                                    Fragment groupChatInviteFriends = GroupChatInviteFriendsFragment.newInstance(groupID,parentActivity ,currentUserMembershipDetails );
                                    GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, groupChatInviteFriends).addToBackStack(GroupChatInviteFriendsFragment.TAG).commit();
                                }
                            });

                            msgET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View view, boolean hasFocus) {
                                if (hasFocus) {
                                    noMsgTV.setVisibility(View.GONE);
                                } else {
                                    noMsgTV.setVisibility(View.VISIBLE);
                                }
                            }
                        });

                        }
                    }
                });
    }

    private void setupMessageListener()
    {
        final Map<String , User> memberDetails = new HashMap<>();
        final ArrayList<String> memberId = new ArrayList<>();

        membersListener = GeneralFunction.getFirestoreInstance()
                .collection(GroupChat.URL_GROUP_CHAT)
                .document(groupID)
                .collection(GroupChat.URL_GROUP_CHAT_MEMBERS)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots , @Nullable FirebaseFirestoreException e)
                    {
                        for(DocumentSnapshot dc:queryDocumentSnapshots)
                        {
                            memberId.add(dc.getId());
//                            String singularPlurals;
//
//                            if(queryDocumentSnapshots.size() == 1)
//                            {
//                                singularPlurals = queryDocumentSnapshots.size()+" member";
//                            }
//                            else if(queryDocumentSnapshots.size() > 1)
//                            {
//                                singularPlurals = queryDocumentSnapshots.size()+" members";
//                            }
//                            else
//                            {
//                                singularPlurals = "defaultNoOfMembers";
//                            }
//                            displayNoOfGroupMembersTV.setText(singularPlurals);
                        }

                            db.collection(User.URL_USER).addSnapshotListener(new EventListener<QuerySnapshot>() {
                                @Override
                                public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots , @Nullable FirebaseFirestoreException e)
                                {
                                    for(DocumentSnapshot dcsn:queryDocumentSnapshots)
                                    {
                                        if(memberId.contains(dcsn.getId()))
                                        {
                                            memberDetails.put(dcsn.getId(),dcsn.toObject(User.class));
                                        }
                                    }
                                    if(messageDataset.size() <= 0 && pictureDataset.size() <=0)
                                    {
                                        getChatMessage(memberDetails);
                                    }
                                }
                            });
                    }
                });
    }

    private void getChatMessage(final Map<String,User> memberDetails)
    {

        chatListener =  GeneralFunction.getFirestoreInstance()
                .collection(GroupChat.URL_GROUP_CHAT)
                .document(groupID)
                .collection(Message.URL_MSG)
                .orderBy(Message.DATE_CREATED)
                .addSnapshotListener(new EventListener<QuerySnapshot>()
                {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots , @Nullable FirebaseFirestoreException e)
                    {
                        if (e != null)
                        {
                            Log.e(TAG , "listen:error" , e);
                            noMsgTV.setVisibility(View.VISIBLE);
                            inv1IV.setVisibility(View.VISIBLE);
                            inv1TV.setVisibility(View.VISIBLE);
                            noMsgTV.setText(parentActivity.getString(R.string.chat_err_loding_msg));
                            return;
                        }
                        if (queryDocumentSnapshots != null )
                        {
                            messageDataset.clear();
                            pictureDataset.clear();
                            for(DocumentSnapshot dc:queryDocumentSnapshots)
                            {
                                final Message tempMsg =  dc.toObject(Message.class);

                                tempMsg.isThisUser = TextUtils.equals(tempMsg.uid, parentActivity.uid);
                                if(!tempMsg.isThisUser)
                                {
                                    tempMsg.user = memberDetails.get(tempMsg.uid);
                                }
                                messageDataset.add(tempMsg);


                                if(tempMsg.isImg)
                                {
//                                    tempMsg.pos = messageDataset.indexOf(tempMsg);
                                    pictureDataset.add(tempMsg);
                                    tempMsg.pos = pictureDataset.indexOf(tempMsg);
                                }

                                messageCnt++;
                                if (messageCnt < 1 || messageDataset.size() < 1)
                                {
                                    inv1IV.setVisibility(View.VISIBLE);
                                    inv1TV.setVisibility(View.VISIBLE);
                                }
                                else if(messageDataset.size() >= 1)
                                {
                                    inv1IV.setVisibility(View.GONE);
                                    inv1TV.setVisibility(View.GONE);
                                }
                                noMsgTV.setVisibility(View.GONE);
                                msgRV.smoothScrollToPosition(msgRV.getAdapter().getItemCount() - 1);

                            }
                            messageGroupAdapter.notifyDataSetChanged();

                        }
                        if(queryDocumentSnapshots.isEmpty() && messageDataset.isEmpty()){
                            noMsgTV.setText(parentActivity.getString(R.string.chat_no_msg));
                        }
                        else{
                            noMsgTV.setText(parentActivity.getString(R.string.chat_no_msg));
                        }
                    }
                });

    }

    private void setupAdapter()
    {
        messageGroupAdapter = new MessageGroupAdapter(messageDataset,parentActivity,pictureDataset);//

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(parentActivity);
        msgRV.setAdapter(messageGroupAdapter);
        msgRV.setLayoutManager(linearLayoutManager);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if( resultCode == RESULT_OK )
        {
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE)
            {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                Uri resultUri = result.getUri();
                filePath = FilePath.getPath(parentActivity, resultUri);

                boolean isGroupChat = true;

                FragmentManager fm = parentActivity.getSupportFragmentManager();
                ShowImageDialogFragment showImageDialogFragment = ShowImageDialogFragment.newInstance(parentActivity,resultUri,groupID,isGroupChat);
                showImageDialogFragment.show(fm, TAG);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void insertMedia()
    {
        addImgIB.setOnClickListener(new DebouncedOnClickListener(500)
        {
            @Override
            public void onDebouncedClick(View v)
            {
                //Todo edit this for the user to show one fragment with send button
                GeneralFunction.showFileChooser11(parentActivity,GroupChatMessageFragment.this);
            }
        });
        emojiIB.setOnClickListener(new DebouncedOnClickListener(500)
        {
            @Override
            public void onDebouncedClick(View v)
            {
                Toast.makeText(parentActivity , "This is emoji" , Toast.LENGTH_SHORT).show();
            }
        });
        voiceIB.setOnClickListener(new DebouncedOnClickListener(500)
        {
            @Override
            public void onDebouncedClick(View v)
            {
                Toast.makeText(parentActivity , "This is voice" , Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void sendIVclick()
    {
        sendIV.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                final DocumentReference ref = GeneralFunction.getFirestoreInstance()
                        .collection(GroupChat.URL_GROUP_CHAT)
                        .document(groupID)
                        .collection(Message.URL_MSG)
                        .document();

                parentActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run()
                    {
                        Message tempMsg;

                        if(msgET.getText() != null && !TextUtils.equals("",msgET.getText().toString().trim()))
                        {
                            tempMsg = new Message(parentActivity.uid,msgET.getText().toString().trim(),false);
                            updateFireStore(groupID,tempMsg,ref);
                        }
                    }
                });
            }
        });
    }

    private void updateFireStore(String groupID, Message tempMsg, DocumentReference ref)
    {
        ref.set(tempMsg)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.getException() != null){
                            GeneralFunction.openMessageNotificationDialog(parentActivity,task.getException().getMessage(),R.drawable.notice_bad,TAG);
                        }
                    }
                });
        HashMap<String,Object> tempChatMap = new HashMap<>();
        tempChatMap.put(GroupChat.GROUP_CHAT_LAST_UPDATED, FieldValue.serverTimestamp());
        tempChatMap.put(GroupChat.GROUP_CHAT_LAST_MESSAGE, tempMsg.msg);
        tempChatMap.put(GroupChat.LAST_UID, parentActivity.uid);
        tempChatMap.put(GroupChat.LAST_TYPE,GroupChat.MESSAGE_TYPE_TEXT);
        GeneralFunction.getFirestoreInstance()
                .collection(GroupChat.URL_GROUP_CHAT)
                .document(groupID)
                .update(tempChatMap);

        msgET.setText("");
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.group_chat_toolbar_title));
                parentActivity.selectChat();
            }
        });
    }
}
