package com.vidatechft.yupa.chatFragments;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.GroupChat;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.FilePath;
import com.vidatechft.yupa.utilities.GeneralFunction;
import com.vidatechft.yupa.utilities.KeyboardUtilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.app.Activity.RESULT_OK;
import static com.vidatechft.yupa.utilities.GeneralFunction.GlideImageSetting;


public class GroupChatCreateFragment extends Fragment implements View.OnClickListener {

    public static final String TAG = GroupChatCreateFragment.class.getName();
    private MainActivity parentActivity;
    private Boolean isCreategroup;

    private ImageButton uploadGroupPicIB,isPublicIB,noIsPublicIB,needApprovalIB,noNeedApprovalIB;
    private ImageButton showcaseImg1IB,showcaseImg2IB,showcaseImg3IB,showcaseImg4IB,showcaseImg5IB;
    private Button uploadGroupPicBTN,addChatGroupBTN,addShowcaseImgBTN;
    private Button addBTN,minusBTN;
    private EditText groupNameET,groupDescET;
    private TextView numCountTV;
    private ConstraintLayout addMinusCL,mainDescriptionCL,descriptionCL1,descriptionCL2,descriptionCL3,descriptionCL4,descriptionCL5;
    private ScrollView scrollSV;

    private int value,imageButton = 0;
//
    private boolean isGoCropPhoto = false;
    private boolean checked = false;
//    private ListenerRegistration getGroupClass;
    private Map<String, Object> groupChatDetailsInsideMembers;

    FirebaseFirestore db = FirebaseFirestore.getInstance();

    private String filePath,filePath1,filePath2,filePath3,filePath4,filePath5,groupID;

    public GroupChatCreateFragment()
    {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static GroupChatCreateFragment newInstance(MainActivity parentActivity , Boolean isCreategroup , Map<String, Object> groupChatDetailsInsideMembers,String groupID)
    {
        GroupChatCreateFragment fragment = new GroupChatCreateFragment();
        fragment.parentActivity = parentActivity;
        fragment.isCreategroup = isCreategroup;
        fragment.groupChatDetailsInsideMembers = groupChatDetailsInsideMembers;
        fragment.groupID = groupID;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater , ViewGroup container ,
            Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_group_chat_create , container , false);

        //todo : Set all views
        findView(view);

        setupView();



        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE)
            {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                Uri resultUri = result.getUri();
                if(imageButton == 0)
                {
                    this.filePath = FilePath.getPath(parentActivity, resultUri);
                    GlideImageSetting(parentActivity, resultUri, uploadGroupPicIB);
                }
                if(imageButton == 1){
                    this.filePath1 = FilePath.getPath(parentActivity, resultUri);
                    GlideImageSetting(parentActivity, resultUri,showcaseImg1IB);
                }
                if(imageButton == 2){
                    this.filePath2 = FilePath.getPath(parentActivity, resultUri);
                    GlideImageSetting(parentActivity, resultUri,showcaseImg2IB);
                }
                if(imageButton == 3){
                    this.filePath3 = FilePath.getPath(parentActivity, resultUri);
                    GlideImageSetting(parentActivity, resultUri,showcaseImg3IB);
                }
                if(imageButton == 4){
                    this.filePath4 = FilePath.getPath(parentActivity, resultUri);
                    GlideImageSetting(parentActivity, resultUri,showcaseImg4IB);
                }
                if(imageButton == 5){
                    this.filePath5 = FilePath.getPath(parentActivity, resultUri);
                    GlideImageSetting(parentActivity, resultUri,showcaseImg5IB);
                }
            }
            if (requestCode == CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE) {
                if (ContextCompat.checkSelfPermission(parentActivity, Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_DENIED) {
                    ActivityCompat.requestPermissions(parentActivity, new String[]{Manifest.permission.CAMERA}, requestCode);
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void setupView()
    {
        if(isCreategroup)
        {
            mainDescriptionCL.setVisibility(View.GONE);
            addMinusCL.setVisibility(View.GONE);

            //show default text value
            numCountTV.setText("0");

            //onTextChange listener
            numCountTV.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s , int start , int count , int after)
                {

                }

                @Override
                public void onTextChanged(CharSequence s , int start , int before , int count)
                {

                }

                @Override
                public void afterTextChanged(Editable s)
                {
                    showUploadCL(s.toString());
                }
            });
        }
        else
        {
            db.collection(GroupChat.URL_GROUP_CHAT)
                    .document(groupID)
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task)
                        {
                            if (task.isSuccessful()) {
                                DocumentSnapshot document = task.getResult();
                                if (document.exists())
                                {
                                    ArrayList<String> filePathsShowcase = new ArrayList<>();

                                    groupNameET.setText(document.getString(GroupChat.GROUP_CHAT_NAME));
                                    groupDescET.setText(document.getString(GroupChat.GROUP_CHAT_DESCRIPTION));
                                    uploadGroupPicBTN.setText("Update Picture");
                                    addChatGroupBTN.setText("Edit Group");

                                    if(document.contains(GroupChat.URL_GROUP_CHAT_PIC) && document.getString(GroupChat.URL_GROUP_CHAT_PIC) != null)
                                    {
                                        GlideImageSetting(parentActivity,Uri.parse(document.getString(GroupChat.URL_GROUP_CHAT_PIC)),uploadGroupPicIB);
                                        filePath = document.getString(GroupChat.URL_GROUP_CHAT_PIC);
                                    }

                                    if(document.getBoolean(GroupChat.GROUP_CHAT_IS_PUBLIC).equals(true))
                                    {
                                        buttonClickableAtOnce(isPublicIB,noIsPublicIB,true);
                                    }
                                    else
                                    {
                                        buttonClickableAtOnce(isPublicIB,noIsPublicIB,false);
                                    }

                                    if(document.getBoolean(GroupChat.GROUP_CHAT_NEED_APPROVAL).equals(true))
                                    {
                                        buttonClickableAtOnce(needApprovalIB,noNeedApprovalIB,true);
                                    }
                                    else
                                    {
                                        buttonClickableAtOnce(needApprovalIB,noNeedApprovalIB,false);
                                    }

                                    if(document.contains(GroupChat.GROUP_CHAT_FILEPATH_SHOWCASE) && document.get(GroupChat.GROUP_CHAT_FILEPATH_SHOWCASE)!=null)
                                    {
                                        mainDescriptionCL.setVisibility(View.VISIBLE);
                                        addMinusCL.setVisibility(View.VISIBLE);

                                        List<String> filePaths = (List<String>) document.get(GroupChat.GROUP_CHAT_FILEPATH_SHOWCASE);
                                        for(int i = 0 ; i < filePaths.size(); i++ )
                                        {
                                            filePathsShowcase.add(filePaths.get(i));
                                        }
                                        String numcount = String.valueOf(filePaths.size());

                                        numCountTV.setText(numcount);
                                        showUploadCL(numcount);

                                        //onTextChange listener
                                        numCountTV.addTextChangedListener(new TextWatcher() {
                                            @Override
                                            public void beforeTextChanged(CharSequence s , int start , int count , int after)
                                            {

                                            }

                                            @Override
                                            public void onTextChanged(CharSequence s , int start , int before , int count)
                                            {

                                            }

                                            @Override
                                            public void afterTextChanged(Editable s)
                                            {
                                                showUploadCL(s.toString());
                                            }
                                        });
                                        for(int counter = 0; counter < filePathsShowcase.size(); counter++)
                                        {
                                            if(counter == 0)
                                            {
                                                GlideImageSetting(parentActivity,Uri.parse(filePathsShowcase.get(counter)),showcaseImg1IB);
                                                filePath1 = filePathsShowcase.get(counter);
                                            }
                                            if(counter == 1)
                                            {
                                                GlideImageSetting(parentActivity,Uri.parse(filePathsShowcase.get(counter)),showcaseImg2IB);
                                                filePath2 = filePathsShowcase.get(counter);
                                            }
                                            if(counter == 2)
                                            {
                                                GlideImageSetting(parentActivity,Uri.parse(filePathsShowcase.get(counter)),showcaseImg3IB);
                                                filePath3 = filePathsShowcase.get(counter);
                                            }
                                            if(counter == 3)
                                            {
                                                GlideImageSetting(parentActivity,Uri.parse(filePathsShowcase.get(counter)),showcaseImg4IB);
                                                filePath4 = filePathsShowcase.get(counter);
                                            }
                                            if(counter == 4)
                                            {
                                                GlideImageSetting(parentActivity,Uri.parse(filePathsShowcase.get(counter)),showcaseImg5IB);
                                                filePath5 = filePathsShowcase.get(counter);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        mainDescriptionCL.setVisibility(View.GONE);
                                        addMinusCL.setVisibility(View.GONE);

                                        //show default text value
                                        numCountTV.setText("0");

                                        //onTextChange listener
                                        numCountTV.addTextChangedListener(new TextWatcher() {
                                            @Override
                                            public void beforeTextChanged(CharSequence s , int start , int count , int after)
                                            {

                                            }

                                            @Override
                                            public void onTextChanged(CharSequence s , int start , int before , int count)
                                            {

                                            }

                                            @Override
                                            public void afterTextChanged(Editable s)
                                            {
                                                showUploadCL(s.toString());
                                            }
                                        });
                                    }
                                }
                                else
                                {
                                    Log.d(TAG, "No such document");
                                }
                            } else {
                                Log.d(TAG, "get failed with ", task.getException());
                            }
                        }
                    });
        }

        uploadGroupPicBTN.setOnClickListener(GroupChatCreateFragment.this);
        uploadGroupPicIB.setOnClickListener(GroupChatCreateFragment.this);
        isPublicIB.setOnClickListener(GroupChatCreateFragment.this);
        noIsPublicIB.setOnClickListener(GroupChatCreateFragment.this);
        needApprovalIB.setOnClickListener(GroupChatCreateFragment.this);
        noNeedApprovalIB.setOnClickListener(GroupChatCreateFragment.this);

        addShowcaseImgBTN.setOnClickListener(GroupChatCreateFragment.this);

        addBTN.setOnClickListener(GroupChatCreateFragment.this);
        minusBTN.setOnClickListener(GroupChatCreateFragment.this);

        showcaseImg1IB.setOnClickListener(GroupChatCreateFragment.this);
        showcaseImg2IB.setOnClickListener(GroupChatCreateFragment.this);
        showcaseImg3IB.setOnClickListener(GroupChatCreateFragment.this);
        showcaseImg4IB.setOnClickListener(GroupChatCreateFragment.this);
        showcaseImg5IB.setOnClickListener(GroupChatCreateFragment.this);

        addChatGroupBTN.setOnClickListener(new DebouncedOnClickListener(1500)
        {
            @Override
            public void onDebouncedClick(View v)
            {
               setUpListener();
            }
        });
    }

    private void setUpListener()
    {
        final Map<String,Object> createGroupDetails = new HashMap<>();
        final ArrayList<String> filePaths = new ArrayList<>();
        String groupName = "";
        String groupDesc = "";


        if(groupNameET.getText() != null && !TextUtils.equals("",groupNameET.getText().toString().trim()))
        {
            groupName = groupNameET.getText().toString().trim();
        }
        if(groupDescET.getText() != null && !TextUtils.equals("",groupDescET.getText().toString().trim()))
        {
            groupDesc = groupDescET.getText().toString().trim();
        }

        if(!isPublicIB.isSelected() && !noIsPublicIB.isSelected()){
            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.chat_group_add_is_public_message),R.drawable.notice_good,TAG);
            checked = false;
            return;
        }

        else if(!needApprovalIB.isSelected() && !noNeedApprovalIB.isSelected()){
            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.chat_group_add_need_approval_message),R.drawable.notice_good,TAG);
            checked = false;
            return;
        }

        if(filePath1 != null)
        {
            filePaths.add(filePath1);
        }
        if(filePath2 != null)
        {
            filePaths.add(filePath2);
        }
        if(filePath3 != null)
        {
            filePaths.add(filePath3);
        }
        if(filePath4 != null)
        {
            filePaths.add(filePath4);
        }
        if(filePath5 != null)
        {
            filePaths.add(filePath5);
        }
        if(!checked)
        {
            initializeProgressBar();

            createGroupDetails.put(GroupChat.GROUP_CHAT_NAME, groupName);
            createGroupDetails.put(GroupChat.GROUP_CHAT_DESCRIPTION, groupDesc);
            createGroupDetails.put(GroupChat.GROUP_CHAT_LAST_UPDATED, FieldValue.serverTimestamp());
            createGroupDetails.put(GroupChat.GROUP_CHAT_IS_PUBLIC,isPublicIB.isSelected());
            createGroupDetails.put(GroupChat.GROUP_CHAT_NEED_APPROVAL, needApprovalIB.isSelected());

            if(isCreategroup)
            {
                createGroupDetails.put(GroupChat.GROUP_CHAT_DATE_CREATED, FieldValue.serverTimestamp());
                createGroupDetails.put(GroupChat.GROUP_CHAT_LAST_MESSAGE, "");
            }

            filePathsUploadToStorage(createGroupDetails,filePaths);
        }
    }

    private void filePathsUploadToStorage(final Map<String,Object> createGroupDetails ,final ArrayList<String> filePaths )
    {
        final DocumentReference groupChatRef;
        final String groupIDS;

        if(isCreategroup)
        {
            groupChatRef = db.collection(GroupChat.URL_GROUP_CHAT).document();
            groupIDS = groupChatRef.getId();
            createGroupDetails.put(GroupChat.GROUP_CHAT_ID,groupIDS);
        }
        else
        {
            groupChatRef = db.collection(GroupChat.URL_GROUP_CHAT).document(groupID);
            groupIDS = groupChatRef.getId();
            createGroupDetails.put(GroupChat.GROUP_CHAT_ID,groupIDS);
        }

        if(filePath !=  "" && filePath  != null)
        {
            if(filePath.contains("https://firebasestorage.googleapis.com"))
            {
                createGroupDetails.put(GroupChat.URL_GROUP_CHAT_PIC,filePath);
                storeShowcasePic(groupChatRef,createGroupDetails,filePaths);
            }
            else
            {
                final StorageReference storeGroupPic =
                        GeneralFunction.getFirebaseStorageInstance().child(GroupChat.URL_GROUP_CHAT +
                                "/" + groupIDS +
                                "/" + GroupChat.URL_GROUP_CHAT_PICTURE +
                                "/" + parentActivity.uid +
                                "/" + GroupChat.URL_GROUP_CHAT_PROFILE_PICTURE);
                UploadTask uploadTask = storeGroupPic.putBytes(GeneralFunction.getCompressedBitmap(parentActivity , filePath));
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception)
                    {
                        GeneralFunction.handleUploadError(parentActivity , GroupChatCreateFragment.this , R.string.employer_err_job_posting_job , TAG);
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot)
                    {
                        storeGroupPic.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri)
                            {
                                if (uri != null)
                                {
                                    createGroupDetails.put(GroupChat.URL_GROUP_CHAT_PIC , uri.toString());
                                    storeShowcasePic(groupChatRef , createGroupDetails , filePaths );
                                }
                            }
                        });
                    }
                });
            }
        }
        else
        {
            storeShowcasePic(groupChatRef , createGroupDetails , filePaths);
        }
    }

    private void storeShowcasePic(final DocumentReference groupChatRef ,final Map<String,Object> createGroupDetails ,final ArrayList<String> filePaths )
    {
        if(filePaths.size() != 0)
        {
            for(int counter = 0; counter < filePaths.size(); counter++)
            {
                if(filePaths.get(counter).contains("com.vidatechft.yupa"))
                {
                    final int finalI = counter;
                    final StorageReference storeShowcaseRef =
                            GeneralFunction.getFirebaseStorageInstance().child(GroupChat.URL_GROUP_CHAT +
                                    "/" + createGroupDetails.get(GroupChat.GROUP_CHAT_ID) +
                                    "/" + GroupChat.URL_GROUP_CHAT_SHOWCASE_PICTURE +
                                    "/" + parentActivity.uid +
                                    "/" + "showcaseImg"+counter);
                    UploadTask uploadTask = storeShowcaseRef.putBytes(GeneralFunction.getCompressedBitmap(parentActivity, filePaths.get(counter)));
                    uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot)
                        {
                            storeShowcaseRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri)
                                {
                                    if(uri != null)
                                    {
                                        filePaths.set(finalI ,uri.toString());
                                        if(finalI == filePaths.size()-1)
                                        {
                                            createGroupDetails.put(GroupChat.GROUP_CHAT_FILEPATH_SHOWCASE,filePaths);
                                            goToFirestore(groupChatRef,createGroupDetails);
                                        }
                                    }
                                }
                            });
                        }
                    }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>()
                    {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot)
                        {
                            int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * 80) + 10;
                            parentActivity.updateProgress((float) totalProgress, GroupChatCreateFragment.this);
                        }
                    });
                }
                else
                {
                    if(counter == filePaths.size()-1)
                    {
                        createGroupDetails.put(GroupChat.GROUP_CHAT_FILEPATH_SHOWCASE,filePaths);
                        goToFirestore(groupChatRef,createGroupDetails );
                    }
                }
            }
        }
        else
        {
            goToFirestore(groupChatRef,createGroupDetails);
        }
    }

    private void goToFirestore(final DocumentReference groupChatRef ,final Map<String,Object> createGroupDetails )
    {
        Map<String, Object> groupChatDetailsInsideMembers = new HashMap<>();

        if(isCreategroup)
        {
            groupChatRef.set(createGroupDetails)
                    .addOnSuccessListener(new OnSuccessListener<Void>()
                    {
                        @Override
                        public void onSuccess(Void aVoid)
                        {
                            GeneralFunction.handleUploadSuccess(parentActivity, GroupChatCreateFragment.this, R.string.chat_group_add_success, TAG);
                        }
                    })
                    .addOnFailureListener(new OnFailureListener()
                    {
                        @Override
                        public void onFailure(@NonNull Exception e)
                        {
                            GeneralFunction.handleUploadError(parentActivity, GroupChatCreateFragment.this, R.string.chat_group_add_fail, TAG);
                        }
                    });


            groupChatDetailsInsideMembers.put(GroupChat.GROUP_CHAT_USER_STATUS, "admin");
            groupChatDetailsInsideMembers.put(GroupChat.GROUP_CHAT_DATE_CREATED, FieldValue.serverTimestamp());
            groupChatDetailsInsideMembers.put(GroupChat.GROUP_CHAT_LAST_UPDATED, FieldValue.serverTimestamp());
            groupChatRef.collection(GroupChat.URL_GROUP_CHAT_MEMBERS).document(parentActivity.uid).set(groupChatDetailsInsideMembers);

        }
        else
        {
            groupChatRef.update(
                    GroupChat.GROUP_CHAT_NAME,createGroupDetails.get(GroupChat.GROUP_CHAT_NAME),
                    GroupChat.GROUP_CHAT_DESCRIPTION,createGroupDetails.get(GroupChat.GROUP_CHAT_DESCRIPTION),
                    GroupChat.GROUP_CHAT_LAST_UPDATED,createGroupDetails.get(GroupChat.GROUP_CHAT_LAST_UPDATED),
                    GroupChat.GROUP_CHAT_IS_PUBLIC,createGroupDetails.get(GroupChat.GROUP_CHAT_IS_PUBLIC),
                    GroupChat.GROUP_CHAT_NEED_APPROVAL,createGroupDetails.get(GroupChat.GROUP_CHAT_NEED_APPROVAL),
                    GroupChat.GROUP_CHAT_FILEPATH_SHOWCASE,createGroupDetails.get(GroupChat.GROUP_CHAT_FILEPATH_SHOWCASE),
                    GroupChat.GROUP_CHAT_ID,createGroupDetails.get(GroupChat.GROUP_CHAT_ID),
                    GroupChat.URL_GROUP_CHAT_PIC,createGroupDetails.get(GroupChat.URL_GROUP_CHAT_PIC))
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid)
                        {
                            GeneralFunction.handleUploadSuccess(parentActivity, GroupChatCreateFragment.this, R.string.chat_group_update_success, TAG);
                        }
                    }) .addOnFailureListener(new OnFailureListener()
            {
                @Override
                public void onFailure(@NonNull Exception e)
                {
                    GeneralFunction.handleUploadError(parentActivity, GroupChatCreateFragment.this, R.string.chat_group_update_fail, TAG);
                }
            });
            groupChatDetailsInsideMembers = this.groupChatDetailsInsideMembers;
        }

        goToGroupChatFrag(groupChatRef.getId(),groupChatDetailsInsideMembers);
    }

    private void goToGroupChatFrag(String id , Map<String,Object> groupChatDetailsInsideMembers)
    {
        Fragment groupChatMessageFragment = GroupChatMessageFragment.newInstance(parentActivity,id,groupChatDetailsInsideMembers);
        if(isCreategroup)
        {
            GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, groupChatMessageFragment).commit();
        }
        else
        {
            //todo : just let it be . Otherwise, it will messed up the fragments before
        }
    }

    private void initializeProgressBar()
    {
        parentActivity.initializeProgressIndicator(parentActivity.getString(R.string.uploadEP_picture), false, this);
        parentActivity.controlProgressIndicator(true, this);
    }

    private void showUploadCL(String s)
    {
        value = Integer.parseInt(s);

        if(value == 1)
        {
            GeneralFunction.fadeInAnimation(parentActivity,descriptionCL1);
        }
        if (value != 1 && value < 1)
        {
            GeneralFunction.fadeOutAnimation(parentActivity,descriptionCL1);
        }
        if(value == 2)
        {
            GeneralFunction.fadeInAnimation(parentActivity,descriptionCL2);
        }
        if(value != 2 && value < 2)
        {
            GeneralFunction.fadeOutAnimation(parentActivity,descriptionCL2);
        }
        if(value == 3)
        {
            GeneralFunction.fadeInAnimation(parentActivity,descriptionCL3);
        }
        if(value != 3 && value < 3)
        {
            GeneralFunction.fadeOutAnimation(parentActivity,descriptionCL3);
        }
        if(value == 4){
            GeneralFunction.fadeInAnimation(parentActivity,descriptionCL4);
        }
        if(value != 4 && value < 4)
        {
            GeneralFunction.fadeOutAnimation(parentActivity,descriptionCL4);
        }
        if(value == 5){
            GeneralFunction.fadeInAnimation(parentActivity,descriptionCL5);
        }
        if(value != 5 && value < 5)
        {
            GeneralFunction.fadeOutAnimation(parentActivity,descriptionCL5);
        }
    }

    private void buttonClickableAtOnce(ImageButton btnTrue, ImageButton btnFalse, boolean isChecked){

        if(!isChecked)
        {
            btnFalse.setSelected(true);
            btnTrue.setSelected(false);
            btnFalse.setBackground(getResources().getDrawable(R.drawable.rounded_acctype_white_bg_border_black));
            btnTrue.setBackground(getResources().getDrawable(R.color.transparent));
        }
        else
        {
            btnTrue.setSelected(true);
            btnFalse.setSelected(false);
            btnTrue.setBackground(getResources().getDrawable(R.drawable.rounded_acctype_white_bg_border_black));
            btnFalse.setBackground(getResources().getDrawable(R.color.transparent));
        }
    }

    private void findView(View view)
    {
        //todo : Main Layout
        uploadGroupPicIB = view.findViewById(R.id.uploadGroupPicIB);
        isPublicIB = view.findViewById(R.id.isPublicIB);
        noIsPublicIB = view.findViewById(R.id.noIsPublicIB);
        needApprovalIB = view.findViewById(R.id.needApprovalIB);
        noNeedApprovalIB = view.findViewById(R.id.noNeedApprovalIB);
        uploadGroupPicBTN = view.findViewById(R.id.uploadGroupPicBTN);
        groupNameET = view.findViewById(R.id.groupNameET);
        groupDescET = view.findViewById(R.id.groupDescET);
        addChatGroupBTN = view.findViewById(R.id.addChatGroupBTN);
        addShowcaseImgBTN = view.findViewById(R.id.addShowcaseImgBTN);
        addMinusCL = view.findViewById(R.id.addMinusCL);
        mainDescriptionCL = view.findViewById(R.id.mainDescriptionCL);
        scrollSV = view.findViewById(R.id.scrollSV);

        //todo : ConstraintLayout For Filepaths
        descriptionCL1 = view.findViewById(R.id.descriptionCL1);
        descriptionCL2 = view.findViewById(R.id.descriptionCL2);
        descriptionCL3 = view.findViewById(R.id.descriptionCL3);
        descriptionCL4 = view.findViewById(R.id.descriptionCL4);
        descriptionCL5 = view.findViewById(R.id.descriptionCL5);

        //todo : Showcase Images layout
        showcaseImg1IB = view.findViewById(R.id.showcaseImg1IB);
        showcaseImg2IB = view.findViewById(R.id.showcaseImg2IB);
        showcaseImg3IB = view.findViewById(R.id.showcaseImg3IB);
        showcaseImg4IB = view.findViewById(R.id.showcaseImg4IB);
        showcaseImg5IB = view.findViewById(R.id.showcaseImg5IB);

        //todo : Add/Minus Buttons
        addBTN = view.findViewById(R.id.addBTN);
        minusBTN = view.findViewById(R.id.minusBTN);
        numCountTV = view.findViewById(R.id.numCountTV);
    }

    private void toggleVisibility()
    {
        if(addMinusCL.getVisibility() == View.GONE  && mainDescriptionCL.getVisibility() == View.GONE)
        {
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run()
                {

                    addMinusCL.setVisibility(View.VISIBLE);
                    mainDescriptionCL.setVisibility(View.VISIBLE);

                    descriptionCL1.setVisibility(View.GONE);
                    numCountTV.setText("0");

                    addShowcaseImgBTN.setText("Remove Showcase Images");

                    GeneralFunction.slideDownAnimation(parentActivity,mainDescriptionCL);
                    GeneralFunction.slideDownAnimation(parentActivity,addMinusCL);
                    scrollSV.fullScroll(ScrollView.FOCUS_DOWN);
                }
            });
        }
        else
        {
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run()
                {
                    addMinusCL.setVisibility(View.GONE);
                    mainDescriptionCL.setVisibility(View.GONE);

                    addShowcaseImgBTN.setText("Add Showcase Images");
                    scrollSV.fullScroll(ScrollView.FOCUS_DOWN);
                }
            });
        }
    }

    private void chooseImage(int imageButtonNo)
    {
        isGoCropPhoto = true;
        if(GeneralFunction.isStorageAndCameraPermitted(parentActivity.getApplicationContext(),parentActivity)){
            GeneralFunction.showFileChooserRatio169(parentActivity,this);
            imageButton = imageButtonNo;
        }
        KeyboardUtilities.hideSoftKeyboard(parentActivity);
    }

    public void increaseInteger()
    {
        if(value < 5 )
        {
            value = value + 1;
            numCountTV.setText(String.valueOf(value));
            if(value == 5)
            {
                Toast.makeText(parentActivity, "Maximum photo is "+ value, Toast.LENGTH_LONG).show();
            }
            scrollSV.fullScroll(ScrollView.FOCUS_DOWN);
        }
        if(value == 5)
        {
            Toast.makeText(parentActivity, "Maximum photo is "+ value, Toast.LENGTH_LONG).show();
        }


    }
    public void decreaseInteger()
    {
        if (value == 0)
        {
            Toast.makeText(parentActivity, "There are no photos to be uploaded" ,Toast.LENGTH_LONG).show();
        }
        if(value > 0)
        {
            value = value - 1;
            numCountTV.setText(String.valueOf(value));
            if(value == 0)
            {
                Toast.makeText(parentActivity, "There are no photos to be uploaded", Toast.LENGTH_SHORT).show();
            }
        }
        if(value >= 0 )
        {
            if(value == 4)
            {
                showcaseImg5IB.setImageDrawable(null);
                filePath5 = null;
            }
            if(value == 3)
            {
                showcaseImg4IB.setImageDrawable(null);
                filePath4 = null;
            }
            if(value == 2)
            {
                showcaseImg3IB.setImageDrawable(null);
                filePath3 = null;
            }
            if(value == 1)
            {
                showcaseImg2IB.setImageDrawable(null);
                filePath2 = null;
            }
            if(value == 0)
            {
                showcaseImg1IB.setImageDrawable(null);
                filePath1 = null;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(isCreategroup.equals(true))
                {
                    parentActivity.setToolbarTitle(parentActivity.getString(R.string.chat_group_toolbar_2));
                }
                else
                {
                    parentActivity.setToolbarTitle(parentActivity.getString(R.string.chat_group_toolbar_2_Edit));
                }
                parentActivity.selectChat();
            }
        });
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.uploadGroupPicIB:
                imageButton = 0;
                GeneralFunction.showFileChooser11(parentActivity,this);
                break;
            case R.id.uploadGroupPicBTN:
                GeneralFunction.showFileChooser11(parentActivity,this);
                break;
            case R.id.isPublicIB:
                buttonClickableAtOnce(isPublicIB,noIsPublicIB,true);
                break;
            case R.id.noIsPublicIB:
                buttonClickableAtOnce(isPublicIB,noIsPublicIB,false);
                break;
            case R.id.needApprovalIB:
                buttonClickableAtOnce(needApprovalIB, noNeedApprovalIB, true);
                break;
            case R.id.noNeedApprovalIB:
                buttonClickableAtOnce(needApprovalIB, noNeedApprovalIB,false);
                break;
            case R.id.addShowcaseImgBTN:
                toggleVisibility();
                break;
            case R.id.showcaseImg1IB:
                chooseImage(1);
                break;
            case R.id.showcaseImg2IB:
                chooseImage(2);
                break;
            case R.id.showcaseImg3IB:
                chooseImage(3);
                break;
            case R.id.showcaseImg4IB:
                chooseImage(4);
                break;
            case R.id.showcaseImg5IB:
                chooseImage(5);
                break;
            case R.id.addBTN:
                increaseInteger();
                break;
            case R.id.minusBTN:
                decreaseInteger();
                break;
        }
    }
}
