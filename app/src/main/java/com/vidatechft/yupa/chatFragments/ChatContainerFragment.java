package com.vidatechft.yupa.chatFragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.ChatAdapter;
import com.vidatechft.yupa.adapter.ChatDashboardTabPagerAdapter;
import com.vidatechft.yupa.profileFragments.SearchNearbyUserFragment;
import com.vidatechft.yupa.utilities.GeneralFunction;

public class ChatContainerFragment extends Fragment implements View.OnClickListener{
    public static final String TAG = ChatContainerFragment.class.getName();
    private MainActivity parentActivity;
    private String lastToolbarTitle;

    private EditText searchET;
    private ChatDashboardTabPagerAdapter adapter;
    private ViewPager viewPager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static ChatContainerFragment newInstance(MainActivity parentActivity, String lastToolbarTitle) {
        ChatContainerFragment fragment = new ChatContainerFragment();

        fragment.parentActivity = parentActivity;
        fragment.lastToolbarTitle = lastToolbarTitle;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_chat_dashboard_container, container, false);
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.showBtmNav();
                TabLayout tabLayout = view.findViewById(R.id.tabLayout);
                searchET = view.findViewById(R.id.searchET);
                ImageView deleteIV = view.findViewById(R.id.deleteIV);
                TextView searchCompanionTV = view.findViewById(R.id.searchCompanionTV);

                deleteIV.setOnClickListener(ChatContainerFragment.this);
                searchET.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        int position = viewPager.getCurrentItem();
                        //using broadcast receiver to search instead

                        if(position == 0 || position == 2){
                            Intent intent = new Intent();
                            intent.putExtra(PersonalChatListFragment.INTENT_SEARCH_CHAT,s.toString());
                            intent.setAction(PersonalChatListFragment.BROADCAST_ID);
                            parentActivity.sendBroadcast(intent);
                        }else if(position == 1){

                        }
                    }
                });

                searchET.setOnClickListener(ChatContainerFragment.this);
                searchCompanionTV.setOnClickListener(ChatContainerFragment.this);

                tabLayout.addTab(tabLayout.newTab().setText(parentActivity.getString(R.string.chat_personal)));
                tabLayout.addTab(tabLayout.newTab().setText(parentActivity.getString(R.string.chat_group)));
                tabLayout.addTab(tabLayout.newTab().setText(parentActivity.getString(R.string.chat_job)));

                viewPager = view.findViewById(R.id.pager);

                adapter = new ChatDashboardTabPagerAdapter(parentActivity,ChatContainerFragment.this,ChatContainerFragment.this.getChildFragmentManager(), tabLayout.getTabCount(),lastToolbarTitle);
                viewPager.setOffscreenPageLimit(2);
                viewPager.setAdapter(adapter);
                viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

                tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        viewPager.setCurrentItem(tab.getPosition());
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {

                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {

                    }
                });
            }
        });
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public boolean canUpdateAdapter(){
        return searchET.getText().length() == 0;
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.chat_toolbar_title));
                parentActivity.selectChat();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.deleteIV:
                searchET.setText("");
                break;
            case R.id.searchCompanionTV:
                Fragment searchNearbyUserFragment = SearchNearbyUserFragment.newInstance(parentActivity);
                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, searchNearbyUserFragment).addToBackStack(SearchNearbyUserFragment.TAG).commit();
                break;
        }
    }
}