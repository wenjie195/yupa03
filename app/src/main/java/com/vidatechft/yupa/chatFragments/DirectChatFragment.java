package com.vidatechft.yupa.chatFragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QuerySnapshot;
import com.theartofdev.edmodo.cropper.CropImage;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.MessageAdapter;
import com.vidatechft.yupa.classes.Chat;
import com.vidatechft.yupa.classes.Message;
import com.vidatechft.yupa.classes.User;
import com.vidatechft.yupa.dialogFragments.ShowImageDialogFragment;
import com.vidatechft.yupa.profileFragments.ProfileFragment;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.FilePath;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.app.Activity.RESULT_OK;

public class DirectChatFragment extends Fragment implements View.OnClickListener{
    public static final String TAG = DirectChatFragment.class.getName();
    private MainActivity parentActivity;
    private String lastToolbarTitle;
    private String otherUid;
    private String chatId;
    private String filePath;

    private MessageAdapter messageAdapter;
    private List<Message> messageDataset = new ArrayList<>();
    private List<Message> pictureDataset = new ArrayList<>();
    private RecyclerView msgRV;
    private TextView noMsgTV,otherUserNameTV,otherUserDescTV;
    private ImageView profilePicIV, onlineIV;
    private EditText msgET;
    private RelativeLayout otherUserRL;
    private ImageButton addImgIB,emojiIB,voiceIB;

    private ListenerRegistration chatListener,onlineListener;

    private User otherUser;

    private String subChatId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static DirectChatFragment newInstance(MainActivity parentActivity, String lastToolbarTitle, String otherUid, String chatId) {
        DirectChatFragment fragment = new DirectChatFragment();

        fragment.parentActivity = parentActivity;
        fragment.lastToolbarTitle = lastToolbarTitle;
        fragment.otherUid = otherUid;
        fragment.chatId = chatId;

        return fragment;
    }

    public static DirectChatFragment newInstance(MainActivity parentActivity, String lastToolbarTitle, User otherUser, String chatId) {
        DirectChatFragment fragment = new DirectChatFragment();

        fragment.parentActivity = parentActivity;
        fragment.lastToolbarTitle = lastToolbarTitle;
        fragment.otherUser = otherUser;
        fragment.chatId = chatId;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_direct_chat, container, false);

        if(chatId != null){
            subChatId = chatId + parentActivity.uid;
            GeneralFunction.unsubscribeChat(subChatId,TAG);
        }

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                msgRV = view.findViewById(R.id.msgRV);
                noMsgTV = view.findViewById(R.id.noMsgTV);
                otherUserNameTV = view.findViewById(R.id.otherUserNameTV);
                otherUserDescTV = view.findViewById(R.id.otherUserDescTV);
                profilePicIV = view.findViewById(R.id.profilePicIV);
                onlineIV = view.findViewById(R.id.onlineIV);
                otherUserRL = view.findViewById(R.id.otherUserRL);
                msgET = view.findViewById(R.id.msgET);

                addImgIB = view.findViewById(R.id.addImgIB);
                emojiIB = view.findViewById(R.id.emojiIB);
                voiceIB = view.findViewById(R.id.voiceIB);
                view.findViewById(R.id.sendIV).setOnClickListener(DirectChatFragment.this);

                if(otherUser != null){
                    setOtherUserDetails(otherUser);
                }

                if(onlineListener == null){
                    listenForOnlineStatus();
                }

                insertMedia();

                if(messageDataset.size() <= 0 && pictureDataset.size() <=0){
                    if(otherUser != null){
                        getChatMessage();
                    }else{
                        getOtherUserDetails();
                    }
                }else{
                    noMsgTV.setVisibility(View.GONE);
                }
            }
        });

        return view;
    }

    private void insertMedia()
    {
        addImgIB.setOnClickListener(new DebouncedOnClickListener(500)
        {
            @Override
            public void onDebouncedClick(View v)
            {
                //Todo edit this for the user to show one fragment with send button
                GeneralFunction.showFileChooser11(parentActivity,DirectChatFragment.this);
            }
        });
        emojiIB.setOnClickListener(new DebouncedOnClickListener(500)
        {
            @Override
            public void onDebouncedClick(View v)
            {
                Toast.makeText(parentActivity , "This is emoji" , Toast.LENGTH_SHORT).show();
            }
        });
        voiceIB.setOnClickListener(new DebouncedOnClickListener(500)
        {
            @Override
            public void onDebouncedClick(View v)
            {
                Toast.makeText(parentActivity , "This is voice" , Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if( resultCode == RESULT_OK )
        {
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE)
            {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                Uri resultUri = result.getUri();
                filePath = FilePath.getPath(parentActivity, resultUri);

                boolean isGroupChat = false;

                FragmentManager fm = parentActivity.getSupportFragmentManager();
                ShowImageDialogFragment showImageDialogFragment = ShowImageDialogFragment.newInstance(parentActivity,resultUri,chatId,isGroupChat);
                showImageDialogFragment.show(fm, TAG);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    private void sendMsg(){
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(msgET.getText() != null && !TextUtils.equals("",msgET.getText().toString().trim())){
                    Message tempMsg = new Message(parentActivity.uid,msgET.getText().toString().trim(),false);

                    GeneralFunction.getFirestoreInstance()
                            .collection(Chat.URL_CHAT)
                            .document(chatId)
                            .collection(Message.URL_MSG)
                            .document()
                            .set(tempMsg)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.getException() != null){
                                GeneralFunction.openMessageNotificationDialog(parentActivity,task.getException().getMessage(),R.drawable.notice_bad,TAG);
                            }
                        }
                    });

                    HashMap<String,Object> tempChatMap = new HashMap<>();
                    tempChatMap.put(Chat.DATE_UPDATED, FieldValue.serverTimestamp());
                    tempChatMap.put(Chat.LAST_MESSAGE, tempMsg.msg);
                    tempChatMap.put(Chat.LAST_TYPE,Chat.MESSAGE_TYPE_TEXT);
                    tempChatMap.put(Chat.LAST_UID, parentActivity.uid);
                    GeneralFunction.getFirestoreInstance()
                            .collection(Chat.URL_CHAT)
                            .document(chatId)
                            .update(tempChatMap);

                    msgET.setText("");
                }else{
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.chat_blank_msg),R.drawable.notice_bad,TAG);
                }

//                KeyboardUtilities.hideSoftKeyboard(parentActivity);//dont hide keyboard
            }
        });
    }

    private void getChatMessage(){
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                chatListener = GeneralFunction.getFirestoreInstance()
                        .collection(Chat.URL_CHAT)
                        .document(chatId)
                        .collection(Message.URL_MSG)
                        .orderBy(Message.DATE_CREATED)
                        .addSnapshotListener(new EventListener<QuerySnapshot>() {
                            @Override
                            public void onEvent(@javax.annotation.Nullable QuerySnapshot snapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                                if (e != null) {
                                    Log.e(TAG, "listen:error", e);
                                    noMsgTV.setVisibility(View.VISIBLE);
                                    noMsgTV.setText(parentActivity.getString(R.string.chat_err_loding_msg));
                                    return;
                                }

                                if (snapshots != null) {
                                    for (DocumentChange dc : snapshots.getDocumentChanges()) {
                                        switch (dc.getType()) {
                                            case ADDED:
                                                if(dc.getDocument().exists()){
                                                    Message tempMsg =  dc.getDocument().toObject(Message.class);
                                                    tempMsg.user = otherUser;
                                                    tempMsg.isThisUser = TextUtils.equals(tempMsg.uid, parentActivity.uid);
                                                    messageDataset.add(tempMsg);

                                                    if(tempMsg.isImg)
                                                    {
                                                        //                                    tempMsg.pos = messageDataset.indexOf(tempMsg);
                                                        pictureDataset.add(tempMsg);
                                                        tempMsg.pos = pictureDataset.indexOf(tempMsg);
                                                    }

                                                    messageAdapter.notifyDataSetChanged();
                                                    noMsgTV.setVisibility(View.GONE);
                                                    msgRV.smoothScrollToPosition(msgRV.getAdapter().getItemCount() - 1);
                                                }

                                                if(messageDataset.size() == 0){
                                                    noMsgTV.setVisibility(View.VISIBLE);
                                                    noMsgTV.setText(parentActivity.getString(R.string.chat_no_msg));
                                                }

                                                break;
                                            case MODIFIED:
                                                break;
                                            case REMOVED:
                                                //todo remove something
                                                break;
                                        }
                                    }

                                    if(snapshots.isEmpty() && messageDataset.isEmpty()){
                                        noMsgTV.setText(parentActivity.getString(R.string.chat_no_msg));
                                    }

                                }else{
                                    noMsgTV.setText(parentActivity.getString(R.string.chat_no_msg));
                                }
                            }
                        });
            }
        });
    }

    private void getOtherUserDetails()
    {
        GeneralFunction.getFirestoreInstance()
            .collection(User.URL_USER)
            .document(otherUid)
            .get()
            .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if(task.isSuccessful() && task.getResult().exists()){
                        setOtherUserDetails(new User(parentActivity,task.getResult()));
                        getChatMessage();
                    }
                }
            });
    }

    private void setOtherUserDetails(final User otherUser){
        this.otherUser = otherUser;
        messageAdapter = new MessageAdapter(messageDataset,parentActivity,pictureDataset);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(parentActivity);
        msgRV.setAdapter(messageAdapter);
        msgRV.setLayoutManager(linearLayoutManager);

        otherUserRL.setOnClickListener(new DebouncedOnClickListener(500) {
            @Override
            public void onDebouncedClick(View v) {
                Fragment profileFragment = ProfileFragment.newInstance(parentActivity,otherUser,false);
                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, profileFragment).addToBackStack(ProfileFragment.TAG).commit();
            }
        });

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                otherUserNameTV.setText(otherUser.name);
                parentActivity.setToolbarTitle(otherUser.name);
                otherUserDescTV.setText(GeneralFunction.formatList(otherUser.jobScope,true));
                if(otherUser.profilePicUrl != null){
                    GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(otherUser.profilePicUrl),profilePicIV);
                }
                if(otherUser.isOnline != null && otherUser.isOnline){
                    setOnline();
                }else{
                    setOffline();
                }
            }
        });
    }

    private void setOnline(){
        onlineIV.setImageDrawable(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.rounded_circle,R.color.green));
    }

    private void setOffline(){
        onlineIV.setImageDrawable(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.rounded_circle,R.color.medium_light_gray));
    }

    private void listenForOnlineStatus(){
        DocumentReference ref;
        if(otherUser == null){
            ref = GeneralFunction.getFirestoreInstance()
                    .collection(User.URL_USER)
                    .document(otherUid);
        }else{
            ref = GeneralFunction.getFirestoreInstance()
                    .collection(User.URL_USER)
                    .document(otherUser.id);
        }
        onlineListener = ref.addSnapshotListener(new EventListener<DocumentSnapshot>() {
                                @Override
                                public void onEvent(@javax.annotation.Nullable DocumentSnapshot documentSnapshot, @javax.annotation.Nullable final FirebaseFirestoreException e) {
                                    if (e != null) {
                                        parentActivity.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Log.w(TAG, "Listen failed.", e);
                                                onlineListener.remove();
                                            }
                                        });
                                        return;
                                    }

                                    if(documentSnapshot != null && documentSnapshot.exists()){
                                        User user = documentSnapshot.toObject(User.class);
                                        if(user != null && user.isOnline != null){
                                            if(user.isOnline){
                                                setOnline();
                                            }else{
                                                setOffline();
                                            }
                                        }else{
                                            setOffline();
                                        }
                                    }
                                }
                            });
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.selectChat();
        if(chatId != null){
            subChatId = chatId + parentActivity.uid;
            GeneralFunction.unsubscribeChat(subChatId,TAG);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(subChatId != null){
            GeneralFunction.subscribeChat(subChatId,TAG);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if(chatListener != null){
            chatListener.remove();
            chatListener = null;
        }

        if(onlineListener != null){
            onlineListener.remove();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.sendIV:
                sendMsg();
                break;
        }
    }
}
