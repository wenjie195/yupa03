package com.vidatechft.yupa.chatFragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.GroupFriendsRecyclerGridAdapter;
import com.vidatechft.yupa.classes.Friend;
import com.vidatechft.yupa.classes.GroupChat;
import com.vidatechft.yupa.classes.User;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.security.acl.Group;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

public class GroupChatInviteFriendsFragment extends Fragment
{
    public static final String TAG = GroupChatInviteFriendsFragment.class.getName();
    private String groupID;
    private MainActivity parentActivity;
    private RecyclerView groupChatFriendListRV;
    private String loadingTxt,emptyTxt,errorTxt,pendingTxt;
    private int totalFriendCount;
    private Button addIntoTheGroupBTN;
    private SparseArray<Friend> getFriend = new SparseArray<>();
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    //
    private ArrayList<Friend> fullFriendsList = new ArrayList<>();
    private ArrayList<Friend> friendsList = new ArrayList<>();
    //
    private TextView noFriendsTV;
    private ListenerRegistration friendsListener,filterMembershipListener,viewListener,checkWhetherIfItsPending;
    //    private int totalFriendCount;
    private GroupFriendsRecyclerGridAdapter groupChatFriendListAdapter;
    //
    private int friendCount = 0;
    //
    private HashMap<String,Friend> friendsMap = new HashMap<>();
    private HashMap<String,User> userMap = new HashMap<>();
    private Map<String, Object> groupChatDetailsInsideMembers;

    public GroupChatInviteFriendsFragment()
    {
        // Required empty public constructor
    }

    public static GroupChatInviteFriendsFragment newInstance(String groupID , MainActivity parentActivity , Map<String, Object> groupChatDetailsInsideMembers)
    {
        GroupChatInviteFriendsFragment fragment = new GroupChatInviteFriendsFragment();
        fragment.groupID = groupID;
        fragment.parentActivity = parentActivity;
        fragment.groupChatDetailsInsideMembers = groupChatDetailsInsideMembers;

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState)
    {
        final View view = inflater.inflate(R.layout.fragment_group_chat_invite_friends, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.showBtmNav();
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.nav_friend_list));

                loadingTxt = parentActivity.getString(R.string.friend_own_loading);
                emptyTxt = parentActivity.getString(R.string.chat_group_invite_no_friend);
                errorTxt = parentActivity.getString(R.string.friend_own_loading_error);
                pendingTxt = parentActivity.getString(R.string.chat_group_invite_pending_error);

                groupChatFriendListRV  = view.findViewById(R.id.groupChatFriendListRV);
                addIntoTheGroupBTN = view.findViewById(R.id.addIntoTheGroupBTN);
                noFriendsTV = view.findViewById(R.id.noFriendsTV);


                setUpGridAdapter();

                if(fullFriendsList.size() <= 0)
                {
                    getFriends();
                    addData();
                }
                else
                {
                    noFriendsTV.setVisibility(View.GONE);
                }



            }
        });
        return view;
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        if (friendsListener != null)
        {
            friendsListener.remove();
        }
        if (filterMembershipListener != null)
        {
            filterMembershipListener.remove();
        }
        if (viewListener != null)
        {
            viewListener.remove();
        }
        if (checkWhetherIfItsPending != null)
        {
            checkWhetherIfItsPending.remove();
        }

    }

    private void addData()
    {
        addIntoTheGroupBTN.setOnClickListener(new DebouncedOnClickListener(500) {
            @Override
            public void onDebouncedClick(View v)
            {
                SparseArray<CheckBox> sparseArray = groupChatFriendListAdapter.getCheckboxMap();
                Map<String,User> getArrayFriend = new HashMap<>();

                for(int counter = 0; counter < sparseArray.size(); counter++)
                {
                    if(sparseArray.get(counter).isChecked())
                    {
                        if(getFriend.get(counter).friendList != null)
                        {
                            for(String uid : getFriend.get(counter).friendList)
                            {
                                if(!TextUtils.equals(uid,parentActivity.uid))
                                {
                                    getArrayFriend.put(uid,getFriend.get(counter).user);
                                }
                            }
                        }
                    }
                }
                //todo : send private message to user to enter the group
                if(getArrayFriend.size() != 0)
                {
                    if(groupChatDetailsInsideMembers.containsValue("admin"))
                    {
                        approveMembershipDirectly(getArrayFriend,sparseArray);
                    }
                    else
                    {
                        sendInvitation(getArrayFriend);
                    }
                    
                }
                else if (getArrayFriend.size() == 0)
                {
                    Toast.makeText(parentActivity , "You did not pick someone, please try again" , Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void approveMembershipDirectly(final Map<String, User> getArrayFriend , final SparseArray<CheckBox> sparseArray)
    {
        if (getArrayFriend.size() > 0)
        {
            final Map<String, Object> saveIntoFireStore = new HashMap<>();

            saveIntoFireStore.put(GroupChat.GROUP_CHAT_DATE_CREATED,FieldValue.serverTimestamp());
            saveIntoFireStore.put(GroupChat.GROUP_CHAT_LAST_UPDATED,FieldValue.serverTimestamp());
            saveIntoFireStore.put(GroupChat.GROUP_CHAT_USER_STATUS,"members");

            for (final String uid : getArrayFriend.keySet())
            {
                db.collection(GroupChat.URL_GROUP_CHAT)
                .document(groupID)
                .collection(GroupChat.URL_GROUP_CHAT_MEMBERS)
                .document(uid)
                .set(saveIntoFireStore)
                .addOnSuccessListener(new OnSuccessListener<Void>()
                {
                    @Override
                    public void onSuccess(Void aVoid)
                    {
                        for(int counter = 0; counter < sparseArray.size(); counter++)
                        {
                            if(sparseArray.get(counter).isChecked())
                            {
//

                                groupChatFriendListAdapter.notifyItemRemoved(counter);
                                groupChatFriendListAdapter.notifyItemRangeChanged(counter,groupChatFriendListAdapter.getItemCount());
                                sparseArray.get(counter).setChecked(false);
                            }
                        }
                    }
                });

                if(sparseArray.size() > 1)
                {
                    GeneralFunction.openMessageNotificationDialog(parentActivity,"You have invited and approve their membership",R.drawable.notice_good,TAG);
                }
                if(sparseArray.size() == 1)
                {
                    if(TextUtils.equals(getArrayFriend.get(uid).gender,"Male"))
                    {
                        GeneralFunction.openMessageNotificationDialog(parentActivity,"You have invited and approve his membership",R.drawable.notice_good,TAG);
                    }
                    else
                    {
                        GeneralFunction.openMessageNotificationDialog(parentActivity,"You have invited and approve her membership",R.drawable.notice_good,TAG);
                    }
                }
                sparseArray.clear();
            }
        }
    }

    private void getFriends()
    {
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                noFriendsTV.setText(loadingTxt);
                noFriendsTV.setVisibility(View.VISIBLE);
                addIntoTheGroupBTN.setVisibility(View.GONE);
            }
        });

        friendsListener = GeneralFunction.getFirestoreInstance()
                .collection(Friend.URL_FRIENDS)
                .whereEqualTo(parentActivity.uid,true)
                .whereEqualTo(Friend.STATUS,Friend.STATUS_ACCEPTED).addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots , @Nullable final FirebaseFirestoreException e)
                    {
                        if (e != null) {
                            parentActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.w(TAG, "Listen failed.", e);
                                    noFriendsTV.setText(errorTxt);
                                    noFriendsTV.setVisibility(View.VISIBLE);
                                }
                            });
                            return;
                        }
                        friendsList.clear();
                        fullFriendsList.clear();
                        if (queryDocumentSnapshots != null && !queryDocumentSnapshots.isEmpty())
                        {
                            totalFriendCount = queryDocumentSnapshots.size();
                            for(QueryDocumentSnapshot snapshot : queryDocumentSnapshots)
                            {
                                final Friend tempFriend = snapshot.toObject(Friend.class);
                                if(tempFriend.friendList != null)
                                {
                                    for(String uid : tempFriend.friendList)
                                    {
                                        if(!TextUtils.equals(uid,parentActivity.uid))
                                        {
                                            friendsMap.put(uid,tempFriend);
                                            getFriendDetails(uid,Friend.TYPE_FRIEND);
                                        }
                                    }

                                }
                            }
                        }
                    }
                });
    }

    private void getFriendDetails(String uid ,final String typeFriend)
    {
        filterMembershipListener =  GeneralFunction.getFirestoreInstance()
                        .collection(User.URL_USER)
                        .document(uid)
                .addSnapshotListener(new EventListener<DocumentSnapshot>()
                {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot documentSnapshot , @Nullable FirebaseFirestoreException e)
                    {
                        if(e != null)
                        {
                            GeneralFunction.openMessageNotificationDialog(parentActivity,e.toString(),R.drawable.notice_bad,TAG);

                            handleIfGotErrorGetFriendDetails(typeFriend);
                        }
                        if(documentSnapshot != null && documentSnapshot.exists())
                        {
                            User tempUser = documentSnapshot.toObject(User.class);

                            if (tempUser != null)
                            {
                                tempUser.id = documentSnapshot.getId();

                                if(friendsMap.get(tempUser.id) != null)
                                {
                                    friendCount++;
                                    friendsMap.get(tempUser.id).user = tempUser;

                                    handleOnFinishFriend();
                                }

                                if(userMap.get(tempUser.id) == null)
                                {
                                    userMap.put(tempUser.id,tempUser);
                                }
                            }
                            else
                            {
                                handleIfGotErrorGetFriendDetails(typeFriend);
                            }
                        }
                    }
                });
    }
    private void handleIfGotErrorGetFriendDetails(String type){
        if(TextUtils.equals(Friend.TYPE_FRIEND,type)){
            friendCount++;
            handleOnFinishFriend();
        }
    }

    private void handleOnFinishFriend()
    {
        if(friendCount >= totalFriendCount){
            friendsList.clear();
            fullFriendsList.clear();
            friendCount = 0;


            //            db.collection(GroupChat.URL_GROUP_CHAT).document(groupID).collection(GroupChat.URL_GROUP_CHAT_MEMBERS).get().addOnCompleteListener()

            for (Map.Entry<String, Friend> entry : friendsMap.entrySet())
            {

                friendsList.add(entry.getValue());
            }

            viewListener = GeneralFunction.getFirestoreInstance().collection(GroupChat.URL_GROUP_CHAT)
                        .document(groupID)
                        .collection(GroupChat.URL_GROUP_CHAT_MEMBERS)
                        .addSnapshotListener(new EventListener<QuerySnapshot>()
                        {
                            @Override
                            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots , @Nullable FirebaseFirestoreException e)
                            {
                                ArrayList<String> usersInGroup = new ArrayList<>();

                                if(e != null)
                                {
                                    Log.d(TAG, "Error getting documents: ",e);
                                    addIntoTheGroupBTN.setVisibility(View.GONE);
                                    noFriendsTV.setText(errorTxt);
                                    noFriendsTV.setVisibility(View.VISIBLE);
                                }
                                if (queryDocumentSnapshots != null && !queryDocumentSnapshots.isEmpty())
                                {
                                    for (DocumentSnapshot document : queryDocumentSnapshots)
                                    {
                                        usersInGroup.add(document.getId());
                                    }

                                    for(int x = 0 ; x < usersInGroup.size();x++)
                                    {
                                        for (int y = 0; y < friendsList.size();y++)
                                        {
                                            if(friendsList.get(y).user.id.equals(usersInGroup.get(x)))
                                            {
                                                friendsList.remove(y);
                                            }
                                        }
                                    }
                                    if(friendsList.isEmpty())
                                    {
                                        addIntoTheGroupBTN.setVisibility(View.GONE);
                                        noFriendsTV.setText(emptyTxt);
                                        noFriendsTV.setVisibility(View.VISIBLE);
                                        groupChatFriendListAdapter.notifyDataSetChanged();
                                    }
                                    else
                                    {
                                        noFriendsTV.setVisibility(View.GONE);
                                        addIntoTheGroupBTN.setVisibility(View.VISIBLE);
                                        checkWhetherIfItsPending();
//                                        groupChatFriendListAdapter.notifyDataSetChanged();
                                    }
                                }
                            }
                        });

//            db.collection(GroupChat.URL_GROUP_CHAT)
//                    .document(groupID)
//                    .collection(GroupChat.URL_GROUP_CHAT_MEMBERS)
//                    .get()
//                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
//                        @Override
//                        public void onComplete(@NonNull Task<QuerySnapshot> task)
//                        {
//                            ArrayList<String> usersInGroup = new ArrayList<>();
//                            if (task.isSuccessful())
//                            {
//
//                                for (QueryDocumentSnapshot document : task.getResult())
//                                {
//                                    usersInGroup.add(document.getId());
//                                    //                                    Log.d(TAG, document.getId() + " => " + document.getData());
//                                }
//
//                                for(int x = 0 ; x < usersInGroup.size();x++)
//                                {
//                                    for (int y = 0; y < friendsList.size();y++)
//                                    {
//                                        if(friendsList.get(y).user.id.equals(usersInGroup.get(x)))
//                                        {
//                                            friendsList.remove(y);
//                                        }
//                                    }
//                                }
//                                if(friendsList.isEmpty())
//                                {
//                                    addIntoTheGroupBTN.setVisibility(View.GONE);
//                                    noFriendsTV.setText(emptyTxt);
//                                    noFriendsTV.setVisibility(View.VISIBLE);
//                                    groupChatFriendListAdapter.notifyDataSetChanged();
//                                }
//                                else
//                                {
//                                    noFriendsTV.setVisibility(View.GONE);
//                                    addIntoTheGroupBTN.setVisibility(View.VISIBLE);
//                                    checkWhetherIfItsPending();
//                                }
//                            }
//                            else
//                            {
//                                Log.d(TAG, "Error getting documents: ", task.getException());
//                                addIntoTheGroupBTN.setVisibility(View.GONE);
//                                noFriendsTV.setText(errorTxt);
//                                noFriendsTV.setVisibility(View.VISIBLE);
//                            }
//                        }
//                    });

        }
    }

    //todo check whether his friends is already pending or not. if not , can display friends .if yes ,don`t display that friend
    private void checkWhetherIfItsPending()
    {

        checkWhetherIfItsPending = GeneralFunction.getFirestoreInstance().collection(GroupChat.URL_GROUP_CHAT)
                .document(groupID)
                .collection(GroupChat.GROUP_CHAT_IS_PENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots , @Nullable FirebaseFirestoreException e)
                    {
                        if(e != null)
                        {
                            Log.d(TAG, "Error getting documents: ", e);
                        }
                        if (queryDocumentSnapshots  != null && !queryDocumentSnapshots.isEmpty())
                        {
                            for (DocumentSnapshot document : queryDocumentSnapshots)
                            {
                                for (int y = 0; y < friendsList.size();y++)
                                {
                                    String id = document.getId();
                                    String id2 = friendsList.get(y).user.id;

                                    if(TextUtils.equals(id,id2))
                                    {
                                        friendsList.remove(y);
                                    }
                                }
                            }
                        }

                        fullFriendsList.addAll(friendsList);

                        friendsMap.clear();

                        parentActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if(friendsList.isEmpty()){
                                    addIntoTheGroupBTN.setVisibility(View.GONE);
                                    noFriendsTV.setText(pendingTxt);
                                    noFriendsTV.setVisibility(View.VISIBLE);
                                }
                                else
                                {
                                    noFriendsTV.setVisibility(View.GONE);
                                    addIntoTheGroupBTN.setVisibility(View.VISIBLE);
                                }
                                groupChatFriendListAdapter.notifyDataSetChanged();
                            }
                        });
                    }
                });
//                .get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>()
//        {
//            @Override
//            public void onComplete(@NonNull Task<QuerySnapshot> task)
//            {
//                if (task.isSuccessful())
//                {
//                    for (QueryDocumentSnapshot document : task.getResult())
//                    {
//                        //Log.d(TAG, document.getId() + " => " + document.getData());
//                        for (int y = 0; y < friendsList.size();y++)
//                        {
//                            String id = document.getId();
//                            String id2 = friendsList.get(y).user.id;
//
//                            if(TextUtils.equals(id,id2))
//                            {
//                                friendsList.remove(y);
//                            }
//                        }
//
//                    }
//
//                    fullFriendsList.addAll(friendsList);
//
//                    friendsMap.clear();
//
//                    parentActivity.runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            if(friendsList.isEmpty()){
//                                addIntoTheGroupBTN.setVisibility(View.GONE);
//                                noFriendsTV.setText(pendingTxt);
//                                noFriendsTV.setVisibility(View.VISIBLE);
//                            }
//                            else
//                            {
//                                noFriendsTV.setVisibility(View.GONE);
//                                addIntoTheGroupBTN.setVisibility(View.VISIBLE);
//                            }
//                            groupChatFriendListAdapter.notifyDataSetChanged();
//                        }
//                    });
//                }
//                else
//                {
//                    Log.d(TAG, "Error getting documents: ", task.getException());
//                }
//            }
//        });
    }

    private void setUpGridAdapter()
    {
        groupChatFriendListAdapter = new  GroupFriendsRecyclerGridAdapter(parentActivity , this , friendsList , new GroupFriendsRecyclerGridAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(final Friend friend ,final int pos) {
                //                groupChatFriendListAdapter.getCheckboxMap().get(pos).setChecked(true);\

                if(groupChatFriendListAdapter.getCheckboxMap().get(pos).isChecked())
                {
                    groupChatFriendListAdapter.getCheckboxMap().get(pos).setChecked(false);
                    getFriend.remove(pos);
                }
                else
                {
                    groupChatFriendListAdapter.getCheckboxMap().get(pos).setChecked(true);
                    getFriend.put(pos,friend);
                }
            }
        });
        groupChatFriendListRV.setNestedScrollingEnabled(false);
        groupChatFriendListRV.setAdapter(groupChatFriendListAdapter);
        groupChatFriendListRV.setLayoutManager(new GridLayoutManager(parentActivity, 3));
    }

    private void sendInvitation(final Map<String,User> getArrayFriend)
    {
        if (getArrayFriend.size() > 0)
        {
            final Map<String, Object> saveIntoFireStore = new HashMap<>();

            for (final String uid : getArrayFriend.keySet())
            {
                //friendsIDList.add(uid);
                saveIntoFireStore.put(GroupChat.GROUP_CHAT_DATE_CREATED, FieldValue.serverTimestamp());
                saveIntoFireStore.put("invitedBy" , parentActivity.uid);

                db.collection(GroupChat.URL_GROUP_CHAT)
                        .document(groupID)
                        .collection(GroupChat.GROUP_CHAT_IS_PENDING)
                        .document(uid)
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<DocumentSnapshot> task)
                            {
                                if (task.isSuccessful())
                                {
                                    DocumentSnapshot document = task.getResult();
                                    if (document.exists())
                                    {

                                    }
                                    else
                                    {
                                        db.collection(GroupChat.URL_GROUP_CHAT)
                                                .document(groupID)
                                                .collection(GroupChat.GROUP_CHAT_IS_PENDING)
                                                .document(uid)
                                                .set(saveIntoFireStore);
                                    }
                                }
                            }
                        });
            }
            GeneralFunction.openMessageNotificationDialog(parentActivity ,
                    parentActivity.getString(R.string.chat_group_view_join_group_pending) ,
                    R.drawable.notice_good ,
                    TAG);

            parentActivity.onBackPressed();

        }
        else
        {
            GeneralFunction.openMessageNotificationDialog(parentActivity ,
                    parentActivity.getString(R.string.chat_group_no_invitation) ,
                    R.drawable.notice_bad ,
                    TAG);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.chat_group_toolbar_6));
                parentActivity.selectChat();
            }
        });
    }
}
