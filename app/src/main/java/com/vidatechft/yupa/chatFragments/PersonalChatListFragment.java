package com.vidatechft.yupa.chatFragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.ChatAdapter;
import com.vidatechft.yupa.classes.Chat;
import com.vidatechft.yupa.classes.User;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class PersonalChatListFragment extends Fragment{
    public static final String TAG = PersonalChatListFragment.class.getName();
    public static final String BROADCAST_ID = "pmChatBroadcast";
    public static final String INTENT_SEARCH_CHAT = "searchPMChat";

    private MainActivity parentActivity;
    private ChatContainerFragment parentFragment;
    private String lastToolbarTitle,chatType;

    private RecyclerView chatRV;
    private List<String> chatIDDataset = new ArrayList<>();
    private List<Chat> fullChatDataset = new ArrayList<>();
    private List<Chat> forDisplayChatDataset = new ArrayList<>();
    private ChatAdapter chatAdapter;
    private TextView noChatTV;
    private ListenerRegistration chatListener;

    private String loadingTxt,emptyTxt,errorTxt;

    private HashMap<String,User> userDetailsMap = new HashMap<>();
    private ArrayList<String> totalUserList = new ArrayList<>();
    private int userCount;

    private BroadcastReceiver broadcastReceiver;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static PersonalChatListFragment newInstance(MainActivity parentActivity, ChatContainerFragment parentFragment, String lastToolbarTitle, String chatType) {
        PersonalChatListFragment fragment = new PersonalChatListFragment();

        fragment.parentActivity = parentActivity;
        fragment.parentFragment = parentFragment;
        fragment.lastToolbarTitle = lastToolbarTitle;
        fragment.chatType = chatType;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_chat_direct_list, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loadingTxt = parentActivity.getString(R.string.chat_loading_chats);
                emptyTxt = parentActivity.getString(R.string.chat_no_chats);
                errorTxt = parentActivity.getString(R.string.chat_no_chats);

                chatRV = view.findViewById(R.id.chatRV);
                noChatTV = view.findViewById(R.id.noChatTV);

                chatAdapter = new ChatAdapter(parentActivity, lastToolbarTitle, forDisplayChatDataset);

                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(parentActivity);
                chatRV.setAdapter(chatAdapter);
                chatRV.setLayoutManager(linearLayoutManager);

                if(forDisplayChatDataset.size() <= 0){
                    getThisUserPersonalChat();
                }else{
                    noChatTV.setVisibility(View.GONE);
                }

                broadcastReceiver = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, final Intent intent) {
                        parentActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (intent != null && intent.getAction() != null && intent.getAction().equals(BROADCAST_ID)) {
                                    String keyword = intent.getStringExtra(INTENT_SEARCH_CHAT);

                                    forDisplayChatDataset.clear();
                                    chatAdapter.notifyDataSetChanged();

                                    for(Chat tempChat: fullChatDataset){
                                        String name = (tempChat.user.name != null ? tempChat.user.name : "").toLowerCase();
                                        String contactNo = (tempChat.user.contactNo != null ? tempChat.user.contactNo : "").toLowerCase();
                                        keyword = keyword.toLowerCase();
                                        if(name.contains(keyword) || contactNo.contains(keyword)){
                                            forDisplayChatDataset.add(tempChat);
                                        }
                                    }

                                    sortChats();
                                    chatAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }
                };
            }
        });
        return view;
    }

    private void sortChats(){
        Collections.sort(forDisplayChatDataset, new Comparator<Chat>() {
            @Override public int compare(Chat chat1, Chat chat2) {
                //because new chat hor meaning the chat 2 dont have date updated will crash, this is to prevent that
                if(chat1.dateUpdated != null && chat2.dateUpdated != null){
                    return (int)(chat2.dateUpdated.getSeconds() - chat1.dateUpdated.getSeconds()); // in ascending order
                }else{
                    return -1;//if asc then return 1, if desc then -1
                }
            }
        });
    }

    public void getThisUserPersonalChat(){
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                noChatTV.setVisibility(View.VISIBLE);
                noChatTV.setText(loadingTxt);
                chatIDDataset.clear();
                fullChatDataset.clear();
                forDisplayChatDataset.clear();

                chatListener = GeneralFunction.getFirestoreInstance().collection(Chat.URL_CHAT)
                        .whereEqualTo(Chat.TYPE,chatType)
                        .whereEqualTo(parentActivity.uid,true)
                        .addSnapshotListener(new EventListener<QuerySnapshot>() {
                            @Override
                            public void onEvent(@javax.annotation.Nullable QuerySnapshot snapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                                if (e != null) {
                                    Log.w(TAG, "Listen failed.", e);
                                    noChatTV.setVisibility(View.VISIBLE);
                                    noChatTV.setText(errorTxt);
                                    return;
                                }

                                if(snapshots != null && !snapshots.isEmpty()){
                                    for(DocumentChange dc : snapshots.getDocumentChanges()){
                                        switch (dc.getType()){
                                            case ADDED:
                                                //dont break it. let it go to modified because the first time open this fragment it goes into here instead
                                            case MODIFIED:
                                                if(TextUtils.equals(Chat.TYPE_JOB_DIRECT,chatType)){
                                                    for(QueryDocumentSnapshot snapshot : snapshots){
                                                        Chat tempChat = snapshot.toObject(Chat.class);
                                                        tempChat.id = snapshot.getId();

                                                        //this is to set other users id
                                                        if(tempChat.employerId != null && tempChat.staffId != null){
                                                            if(TextUtils.equals(parentActivity.uid,tempChat.staffId)){
                                                                totalUserList.add(tempChat.employerId);
                                                                tempChat.otherUserId = tempChat.employerId;
                                                            }else{
                                                                totalUserList.add(tempChat.staffId);
                                                                tempChat.otherUserId = tempChat.staffId;
                                                            }
                                                        }

                                                        if(chatIDDataset.contains(tempChat.id)){
                                                            int position = chatIDDataset.indexOf(tempChat.id);
                                                            if(fullChatDataset.get(position) != null){
                                                                fullChatDataset.set(position,tempChat);
                                                            }else{
                                                                fullChatDataset.add(tempChat);
                                                            }
                                                        }else{
                                                            fullChatDataset.add(tempChat);
                                                            chatIDDataset.add(tempChat.id);
                                                        }
                                                    }
                                                }else if(TextUtils.equals(Chat.TYPE_PERSONAL_MSG,chatType)){
                                                    for(QueryDocumentSnapshot snapshot : snapshots){
                                                        Chat tempChat = snapshot.toObject(Chat.class);
                                                        tempChat.id = snapshot.getId();

                                                        //this is to set other users id
                                                        if(tempChat.userList != null){
                                                            for(String uid : tempChat.userList){
                                                                if(!TextUtils.equals(parentActivity.uid,uid)){
                                                                    tempChat.otherUserId = uid;
                                                                    totalUserList.add(uid);
                                                                }
                                                            }
                                                        }

                                                        if(chatIDDataset.contains(tempChat.id)){
                                                            int position = chatIDDataset.indexOf(tempChat.id);
                                                            if(fullChatDataset.get(position) != null){
                                                                fullChatDataset.set(position,tempChat);
                                                            }else{
                                                                fullChatDataset.add(tempChat);
                                                            }
                                                        }else{
                                                            fullChatDataset.add(tempChat);
                                                            chatIDDataset.add(tempChat.id);
                                                        }
                                                    }
                                                }

                                                getUserDetails();
                                                break;
                                            case REMOVED:
                                                //todo remove something
                                                break;
                                        }
                                    }
                                }else{
                                    noChatTV.setVisibility(View.VISIBLE);
                                    noChatTV.setText(emptyTxt);
                                }
                            }
                        });
            }
        });

        //old ones
//        GeneralFunction.getFirestoreInstance().collection(Chat.URL_CHAT)
//                .whereEqualTo(Chat.TYPE,chatType)
//                .whereEqualTo(parentActivity.uid,true)
//                .get()
//                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
//            @Override
//            public void onComplete(@NonNull final Task<QuerySnapshot> task) {
//                parentActivity.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        if(task.isSuccessful()){
////                            fullChatDataset.addAll(task.getResult().toObjects(Chat.class));
//
//                            if(TextUtils.equals(Chat.TYPE_JOB_DIRECT,chatType)){
//                                for(QueryDocumentSnapshot snapshot : task.getResult()){
//                                    Chat tempChat = snapshot.toObject(Chat.class);
//                                    tempChat.id = snapshot.getId();
//
//                                    if(tempChat.employerId != null && tempChat.staffId != null){
//                                        if(TextUtils.equals(parentActivity.uid,tempChat.staffId)){
//                                            totalUserList.add(tempChat.employerId);
//                                            tempChat.otherUserId = tempChat.employerId;
//                                        }else{
//                                            totalUserList.add(tempChat.staffId);
//                                            tempChat.otherUserId = tempChat.staffId;
//                                        }
//                                    }
//
//                                    fullChatDataset.add(tempChat);
//                                }
//                            }else if(TextUtils.equals(Chat.TYPE_PERSONAL_MSG,chatType)){
//
//                            }
//
//                            if(task.getResult().isEmpty()){
//                                noChatTV.setVisibility(View.VISIBLE);
//                                noChatTV.setText(emptyTxt);
//                            }else{
//                                getUserDetails();
//                            }
//                        }else{
//                            noChatTV.setVisibility(View.VISIBLE);
//                            noChatTV.setText(errorTxt);
//                        }
//                    }
//                });
//            }
//        });
    }

    private void getUserDetails()
    {
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //the linkedHashedSet will filter out duplicate user id (and the order is maintained)
                Set<String> filteredTotalUserList = new LinkedHashSet<>(totalUserList);
                final int userSize = filteredTotalUserList.size();
                for(String usersId : filteredTotalUserList){
                    if(!userDetailsMap.containsKey(usersId)){
                        GeneralFunction.getFirestoreInstance()
                                .collection(User.URL_USER)
                                .document(usersId)
                                .get()
                                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                        if(task.isSuccessful()){
                                            User tempUser = new User(parentActivity,task.getResult());
                                            tempUser.id = task.getResult().getId();
                                            userDetailsMap.put(task.getResult().getId(),tempUser);
                                        }

                                        userCount++;

                                        if(userCount == userSize){
                                            userCount = 0;
                                            updateAdapter();
                                        }
                                    }
                                });
                    }else{
                        userCount++;

                        if(userCount == userSize){
                            userCount = 0;
                            updateAdapter();
                        }
                    }
                }
            }
        });
    }

    private void updateAdapter()
    {
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for(int i = 0; i < fullChatDataset.size(); i++){
                    fullChatDataset.get(i).user = userDetailsMap.get(fullChatDataset.get(i).otherUserId);
                }
                setAdapterInfo();
            }
        });
    }

    private void setAdapterInfo(){
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                forDisplayChatDataset.clear();
                forDisplayChatDataset.addAll(fullChatDataset);

                if(parentFragment.canUpdateAdapter()){
                    sortChats();
                    chatAdapter.notifyDataSetChanged();
                }

                noChatTV.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onResume() {
        parentActivity.selectChat();
        if(broadcastReceiver != null){
            IntentFilter intentFilter = new IntentFilter(BROADCAST_ID);
            parentActivity.registerReceiver(broadcastReceiver, intentFilter);
        }
        super.onResume();
    }

    @Override
    public void onPause() {
        if(broadcastReceiver != null){
            parentActivity.unregisterReceiver(broadcastReceiver);
        }
        super.onPause();
    }
}