package com.vidatechft.yupa.chatFragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.GalleryViewPagerAdapter;
import com.vidatechft.yupa.classes.Message;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.List;

public class GroupChatViewImageFragment extends Fragment
{
    public static final String TAG = GroupChatViewImageFragment.class.getName();
    private MainActivity parentActivity;
    private Message tempMessage;
    private int position;
    private ArrayList<String> showcasePic;
    private ImageView imageView,sendIV;
    private EditText msgET;
    private List<Message> listAllImageMessage;

    public GroupChatViewImageFragment()
    {

    }

    public static GroupChatViewImageFragment newInstance(MainActivity parentActivity , Message tempMessage , int position , List<Message> showcasePic)
    {
        GroupChatViewImageFragment fragment = new GroupChatViewImageFragment();
        fragment.tempMessage = tempMessage;
        fragment.position = position;
        fragment.parentActivity = parentActivity;
        fragment.listAllImageMessage = showcasePic;
        return fragment;
    }
    public static GroupChatViewImageFragment newInstance(MainActivity parentActivity , ArrayList<String> showcasePic , int position)
    {
        GroupChatViewImageFragment fragment = new GroupChatViewImageFragment();
        fragment.showcasePic = showcasePic;
        fragment.position = position;
        fragment.parentActivity = parentActivity;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        parentActivity.hideToolbar();
        parentActivity.hideBtmNav();
        super.onResume();
    }

    @Override
    public void onPause() {
        parentActivity.showToolbar();
        parentActivity.showBtmNav();
        super.onPause();
    }
    @Override
    public View onCreateView(LayoutInflater inflater , ViewGroup container ,
            Bundle savedInstanceState)
    {
        View view  = inflater.inflate(R.layout.fragment_group_chat_view_image , container , false);
        // Inflate the layout for this fragment
        parentActivity.hideToolbar();
        parentActivity.hideBtmNav();

//        imageView = view.findViewById(R.id.imageViewPG);
//        msgET = view.findViewById(R.id.msgET);
//        sendIV = view.findViewById(R.id.sendIV);
//
//        msgET.setVisibility(View.GONE);
//        sendIV.setVisibility(View.GONE);

        if(tempMessage != null)
        {
            ViewPager viewPager = view.findViewById(R.id.galleryViewPager);
            GalleryViewPagerAdapter galleryViewPagerAdapter = new GalleryViewPagerAdapter(parentActivity,parentActivity.getApplicationContext(),tempMessage,listAllImageMessage);
            viewPager.setAdapter(galleryViewPagerAdapter);
            viewPager.setCurrentItem(tempMessage.pos);
            viewPager.setOffscreenPageLimit(1);

//            if (image != "")
//            {
//                GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(image), imageView);
//            }
        }

        else if(showcasePic != null)
        {
            ViewPager viewPager = view.findViewById(R.id.galleryViewPager);
            GalleryViewPagerAdapter galleryViewPagerAdapter = new GalleryViewPagerAdapter(parentActivity,parentActivity.getApplicationContext(),showcasePic);
            viewPager.setAdapter(galleryViewPagerAdapter);
            viewPager.setCurrentItem(position);
            viewPager.setOffscreenPageLimit(1);

            //            if (image != "")
            //            {
            //                GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(image), imageView);
            //            }
        }
        else
        {
            GeneralFunction.openMessageNotificationDialog(parentActivity,"There is something wrong with your picture \nPlease try again later.",R.drawable.notice_bad,TAG);
        }
        return view;
    }

}
