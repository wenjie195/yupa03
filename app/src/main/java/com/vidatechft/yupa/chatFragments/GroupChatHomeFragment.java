package com.vidatechft.yupa.chatFragments;


import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.GroupChatGroupShowcaseAdapter;
import com.vidatechft.yupa.classes.GroupChat;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;


public class GroupChatHomeFragment extends Fragment {

    public static final String TAG = GroupChatHomeFragment.class.getName();
    private Boolean isMember;
    private MainActivity parentActivity;
    private ImageButton uploadGroupPicIB;
    private Button joinGroupBTN;
    private TextView groupDescTV,groupNameTV,noImgTV,showcaseTV;
    private RecyclerView groupChatPicRV;

    private String groupID;
    private String loadingTxt,emptyTxt,errorTxt,pendingTxt;
    private ListenerRegistration homeGroupDetailsListener,getShowcasePicListener;
    private GroupChatGroupShowcaseAdapter groupChatGroupShowcaseAdapter;
    private ArrayList<String> groupChatGroupPicList = new ArrayList<>();
    FirebaseFirestore db = FirebaseFirestore.getInstance();

    public GroupChatHomeFragment()
    {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static GroupChatHomeFragment newInstance(MainActivity parentActivity , boolean isMember , String groupID)
    {
        GroupChatHomeFragment fragment = new GroupChatHomeFragment();
        fragment.parentActivity = parentActivity;
        fragment.isMember = isMember; //todo : is he a member or a stranger
        fragment.groupID = groupID;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        if(homeGroupDetailsListener != null)
        {
            homeGroupDetailsListener.remove();
        }
        if(getShowcasePicListener != null)
        {
            getShowcasePicListener.remove();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater , ViewGroup container ,
            Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_group_chat_home , container , false);
        // Inflate the layout for this fragment

        uploadGroupPicIB = view.findViewById(R.id.uploadGroupPicIB);
        groupNameTV = view.findViewById(R.id.groupNameTV);
        groupDescTV = view.findViewById(R.id.groupDescTV);
        joinGroupBTN = view.findViewById(R.id.joinGroupBTN);
        groupChatPicRV  = view.findViewById(R.id.groupChatPicRV);
        noImgTV = view.findViewById(R.id.noImgTV);
        showcaseTV = view.findViewById(R.id.showcaseTV);

        loadingTxt = parentActivity.getString(R.string.chat_group_home_loading_image);
        emptyTxt = parentActivity.getString(R.string.chat_group_home_empty_image);
        errorTxt = parentActivity.getString(R.string.chat_group_home_error_image);

        if(isMember)
        {
            //todo : To set the visibility of the text
            noImgTV.setText(loadingTxt);
            noImgTV.setVisibility(View.VISIBLE);
        }
        else
        {
            showcaseTV.setVisibility(View.GONE);
            noImgTV.setVisibility(View.GONE);
        }

        //todo : To setup view and differentiate whether it is for member OR for strangers
        setupView();

        //todo : Setting up grid adapter for showcase images AND members
        setUpGridAdapter();

        //todo : If there is no showcase picture AND he is the member of the group
        if(groupChatGroupPicList.size() <= 0 && isMember)
        {
            getShowcasePictures();
        }
        else
        {
            noImgTV.setVisibility(View.GONE);
        }



        return view;
    }

    private void wantsToJoinTheGroup(final Boolean needApproval)
    {
        joinGroupBTN.setOnClickListener(new DebouncedOnClickListener(500)
        {
            @Override
            public void onDebouncedClick(View v)
            {
                if(needApproval.equals(true))
                {
                    //Todo Make the user need approval from admin to join the group
                    ifNeedApproval();
                }
                else
                {
                    //Todo Make the user directly as a member
                    ifNoNeedApproval();
                }
            }
        });
    }

    private void ifNeedApproval()
    {
        db.collection(GroupChat.URL_GROUP_CHAT).document(groupID)
                .collection(GroupChat.GROUP_CHAT_IS_PENDING).document(parentActivity.uid)
                .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>()
        {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task)
            {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists())
                    {
                        Log.d(TAG, "document exists");
                        GeneralFunction
                                .openMessageNotificationDialog(
                                        parentActivity,
                                        parentActivity.getString(R.string.chat_group_view_join_group_pending_2ndTime),
                                        R.drawable.notice_good,
                                        TAG);
                        parentActivity.onBackPressed();
                    }
                    else
                    {
                        Log.d(TAG, "No such document");
                        Map<String, Object> pendingNewUser = new HashMap<>();
                        pendingNewUser.put(GroupChat.GROUP_CHAT_DATE_CREATED, FieldValue.serverTimestamp());
                        pendingNewUser.put("invitedBy" , parentActivity.uid);


                        db.collection(GroupChat.URL_GROUP_CHAT).document(groupID)
                                .collection(GroupChat.GROUP_CHAT_IS_PENDING).document(parentActivity.uid)
                                .set(pendingNewUser)
                                .addOnSuccessListener(new OnSuccessListener<Void>()
                                {
                                    @Override
                                    public void onSuccess(Void aVoid)
                                    {
                                        GeneralFunction.openMessageNotificationDialog(
                                                parentActivity,
                                                parentActivity.getString(R.string.chat_group_view_join_group_pending),
                                                R.drawable.notice_good,
                                                TAG);
                                        parentActivity.onBackPressed();
                                    }
                                });

                    }
                } else {
                    Log.d(TAG, "get failed with ", task.getException());

                }
            }
        });
    }

    private void ifNoNeedApproval()
    {
        final DocumentReference groupChatRef =
                db.collection(GroupChat.URL_GROUP_CHAT).document(groupID)
                        .collection(GroupChat.URL_GROUP_CHAT_MEMBERS).document(parentActivity.uid);

        final Map<String, Object> groupChatDetailsInsideMembers = new HashMap<>();
        groupChatDetailsInsideMembers.put(GroupChat.GROUP_CHAT_USER_STATUS, "members");
        groupChatDetailsInsideMembers.put(GroupChat.GROUP_CHAT_DATE_CREATED, FieldValue.serverTimestamp());
        groupChatDetailsInsideMembers.put(GroupChat.GROUP_CHAT_LAST_UPDATED, FieldValue.serverTimestamp());

        groupChatRef.set(groupChatDetailsInsideMembers);
        GeneralFunction
                .openMessageNotificationDialog(
                        parentActivity,
                        parentActivity.getString(R.string.chat_group_view_join_group_approved),
                        R.drawable.notice_good,
                        TAG);

        Fragment groupChatMessageFragment = GroupChatMessageFragment.newInstance(parentActivity,groupID,groupChatDetailsInsideMembers);
        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, groupChatMessageFragment).addToBackStack(GroupChatMessageFragment.TAG).commit();
    }

    private void getShowcasePictures()
    {
        getShowcasePicListener = GeneralFunction.getFirestoreInstance().collection(GroupChat.URL_GROUP_CHAT)
                .document(groupID)
                .addSnapshotListener(new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot documentSnapshot , @Nullable FirebaseFirestoreException e)
                    {
                        if (e != null)
                        {
                            GeneralFunction.Toast(parentActivity,e.toString());
                            return;
                        }

                        //todo : Clear the showcase pic list
                        groupChatGroupPicList.clear();

                        if(documentSnapshot != null && documentSnapshot.exists() && documentSnapshot.contains(GroupChat.GROUP_CHAT_FILEPATH_SHOWCASE) && documentSnapshot.get(GroupChat.GROUP_CHAT_FILEPATH_SHOWCASE)!= null)
                        {
                            List<String> filePaths = (List<String>) documentSnapshot.get(GroupChat.GROUP_CHAT_FILEPATH_SHOWCASE);

                            for(int i = 0 ; i < filePaths.size(); i++ )
                            {
                                groupChatGroupPicList.add(filePaths.get(i));
                            }

                            noImgTV.setVisibility(View.GONE);
                            parentActivity.runOnUiThread(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    groupChatGroupShowcaseAdapter.notifyDataSetChanged();
                                }
                            });
                        }
                        else
                        {
                            parentActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    //noOwnItiTV.setVisibility(View.GONE);
                                    noImgTV.setText(emptyTxt);
                                    noImgTV.setVisibility(View.VISIBLE);
                                    groupChatGroupShowcaseAdapter.notifyDataSetChanged();

                                }
                            });
                        }
                    }
                });
    }

    private void setUpGridAdapter()
    {
        groupChatGroupShowcaseAdapter = new GroupChatGroupShowcaseAdapter(parentActivity,this, groupChatGroupPicList);
        groupChatPicRV.setAdapter(groupChatGroupShowcaseAdapter);
        groupChatPicRV.setLayoutManager(new GridLayoutManager(parentActivity, 2));
        groupChatPicRV.setNestedScrollingEnabled(false);
    }

    private void setupView()
    {
        homeGroupDetailsListener = GeneralFunction.getFirestoreInstance()
                .collection(GroupChat.URL_GROUP_CHAT)
                .document(groupID)
                .addSnapshotListener(new EventListener<DocumentSnapshot>()
                {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot documentSnapshot , @Nullable FirebaseFirestoreException e)
                    {
                        if (e != null)
                        {
                            GeneralFunction.Toast(parentActivity,e.toString());
                            return;
                        }
                        //todo : If there are groupchat data
                        if(documentSnapshot.exists())
                        {
                            final GroupChat tempGroupChat = new GroupChat(documentSnapshot);

                            parentActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run()
                                {

                                    if(tempGroupChat != null)
                                    {
                                        //todo : If the group name is present
                                        if (tempGroupChat.groupChatName != null)
                                        {
                                            groupNameTV.setText(tempGroupChat.groupChatName);
                                        }
                                        //todo : If the group description is present
                                        if (tempGroupChat.groupChatDescription != null)
                                        {
                                            groupDescTV.setText(tempGroupChat.groupChatDescription);
                                        }
                                        //todo : If the group image is NOT present
                                        if (tempGroupChat.groupChatPic == null)
                                        {
                                           tempGroupChat.groupChatPic = "";
                                        }
                                        //todo : If the group image is present
                                        if (tempGroupChat.groupChatPic != null)
                                        {
                                            GeneralFunction.GlideCircleImageSetting(parentActivity , Uri.parse(tempGroupChat.groupChatPic) , uploadGroupPicIB);
                                        }
                                        //todo : If the page is from GroupChatMessageFragment(Members)
                                        if(isMember)
                                        {
                                            joinGroupBTN.setVisibility(View.GONE);
                                            groupChatPicRV.setVisibility(View.VISIBLE);
                                        }
                                        //todo : If the page is from GroupChatAdapter(Strangers)
                                        if(!isMember)
                                        {
                                            joinGroupBTN.setVisibility(View.VISIBLE);
                                            groupChatPicRV.setVisibility(View.VISIBLE);

                                            //todo : If stranger wants to join the group
                                            wantsToJoinTheGroup(tempGroupChat.needApproval);
                                        }
                                    }
                                    else
                                    {
                                        GeneralFunction.openMessageNotificationDialog(parentActivity,errorTxt,R.drawable.notice_bad,TAG);
                                        parentActivity.onBackPressed();
                                    }
                                }
                            });
                        }
                        else
                        {
                            GeneralFunction.openMessageNotificationDialog(parentActivity,errorTxt,R.drawable.notice_bad,TAG);
                            parentActivity.onBackPressed();
                        }

                    }
                });
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.chat_group_toolbar_1));
                parentActivity.selectChat();
            }
        });
    }
}
