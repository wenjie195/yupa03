package com.vidatechft.yupa.retrofitNetwork;

import android.support.annotation.NonNull;

import com.google.firebase.firestore.GeoPoint;
import com.vidatechft.yupa.classes.PlaceDetailsGoogleApi.AddressComponent;
import com.vidatechft.yupa.classes.PlaceDetailsGoogleApi.PlaceAddress;
import com.vidatechft.yupa.classes.PlaceDetailsGoogleApi.PlacesDetailsJson;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class PlaceAddressCallback<T> implements Callback<PlacesDetailsJson> {

    @Override
    public void onResponse(@NonNull Call<PlacesDetailsJson> call, @NonNull Response<PlacesDetailsJson> response) {
        if (response.errorBody() != null) {
            try {
                onPlaceDetailsFailure(new Exception(response.errorBody().string()));
            } catch (IOException e) {
                e.printStackTrace();
                onPlaceDetailsFailure(e);
            }
            return;
        }

        PlacesDetailsJson result = response.body();
        if (result != null) {
            if(result.placesDetailsResult != null && result.placesDetailsResult.addressComponents != null){

                ArrayList<AddressComponent> addressComponents = result.placesDetailsResult.addressComponents;

                PlaceAddress placeAddress = new PlaceAddress();
                placeAddress.placeName = result.placesDetailsResult.placeName;
                placeAddress.lat = result.placesDetailsResult.geometry.location.lat;
                placeAddress.lng = result.placesDetailsResult.geometry.location.lng;

                for(AddressComponent addressComponent : addressComponents){
                    if(addressComponent.types != null){
                        for(String type : addressComponent.types){
                            switch (type){
                                case PlaceAddress.COUNTRY:
                                    placeAddress.country = addressComponent.longName;
                                    break;
                                case PlaceAddress.ADMINISTRATIVE_AREA_LEVEL_1:
                                    placeAddress.administrative_area_level_1 = addressComponent.longName;
                                    break;
                                case PlaceAddress.ADMINISTRATIVE_AREA_LEVEL_2:
                                    placeAddress.administrative_area_level_2 = addressComponent.longName;
                                    break;
                                case PlaceAddress.ADMINISTRATIVE_AREA_LEVEL_3:
                                    placeAddress.administrative_area_level_3 = addressComponent.longName;
                                    break;
                                case PlaceAddress.ADMINISTRATIVE_AREA_LEVEL_4:
                                    placeAddress.administrative_area_level_4 = addressComponent.longName;
                                    break;
                                case PlaceAddress.ADMINISTRATIVE_AREA_LEVEL_5:
                                    placeAddress.administrative_area_level_5 = addressComponent.longName;
                                    break;
                                case PlaceAddress.LOCALITY:
                                    placeAddress.locality = addressComponent.longName;
                                    break;
                                case PlaceAddress.SUBLOCALITY:
                                    if( addressComponent.types.contains(PlaceAddress.SUBLOCALITY_LEVEL_1) ||
                                        addressComponent.types.contains(PlaceAddress.SUBLOCALITY_LEVEL_2) ||
                                        addressComponent.types.contains(PlaceAddress.SUBLOCALITY_LEVEL_3) ||
                                        addressComponent.types.contains(PlaceAddress.SUBLOCALITY_LEVEL_4) ||
                                        addressComponent.types.contains(PlaceAddress.SUBLOCALITY_LEVEL_5)) {
                                        //if any one of it has, dont put sublocality
                                    }else{
                                        //if all also dont have then put into sublocality
                                        placeAddress.sublocality = addressComponent.longName;
                                    }
                                    break;
                                case PlaceAddress.SUBLOCALITY_LEVEL_1:
                                    placeAddress.sublocality_level_1 = addressComponent.longName;
                                    break;
                                case PlaceAddress.SUBLOCALITY_LEVEL_2:
                                    placeAddress.sublocality_level_2 = addressComponent.longName;
                                    break;
                                case PlaceAddress.SUBLOCALITY_LEVEL_3:
                                    placeAddress.sublocality_level_3 = addressComponent.longName;
                                    break;
                                case PlaceAddress.SUBLOCALITY_LEVEL_4:
                                    placeAddress.sublocality_level_4 = addressComponent.longName;
                                    break;
                                case PlaceAddress.SUBLOCALITY_LEVEL_5:
                                    placeAddress.sublocality_level_5 = addressComponent.longName;
                                    break;
                                case PlaceAddress.STREET_NUMBER:
                                    placeAddress.street_number = addressComponent.longName;
                                    break;
                                case PlaceAddress.ROUTE:
                                    placeAddress.route = addressComponent.longName;
                                    break;
                                case PlaceAddress.STREET_ADDRESS:
                                    placeAddress.street_address = addressComponent.longName;
                                    break;
                                case PlaceAddress.PREMISE:
                                    placeAddress.premise = addressComponent.longName;
                                    break;
                                case PlaceAddress.SUBPREMISE:
                                    placeAddress.subpremise = addressComponent.longName;
                                    break;
                                case PlaceAddress.POSTAL_CODE:
                                    placeAddress.postal_code = addressComponent.longName;
                                    break;
                            }
                        }
                    }
                }
                onPlaceDetailsSuccess(placeAddress);
            }else{
                onPlaceDetailsFailure(new Exception("place details result or address components is null"));
            }
        }else{
            onPlaceDetailsFailure(new Exception("result is null"));
        }

    }

    @Override
    public void onFailure(@NonNull Call<PlacesDetailsJson> call, @NonNull Throwable t) {
        onPlaceDetailsFailure(new Exception(t));
    }

    public abstract void onPlaceDetailsSuccess(PlaceAddress placeAddress);

    public abstract void onPlaceDetailsFailure(Exception e);

}
