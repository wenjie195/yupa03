package com.vidatechft.yupa.retrofitNetwork;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.PlaceAutoCompleteGoogleApi.PlaceAutoCompleteResult;
import com.vidatechft.yupa.classes.PlaceDetailsGoogleApi.PlaceAddress;
import com.vidatechft.yupa.classes.PlaceDetailsGoogleApi.PlacesDetailsJson;
import com.vidatechft.yupa.classes.SearchPlaceGoogleApi.SearchPlaceResult;

import java.util.ArrayList;

public class UnifyPlaceAddress {
    public static final String TAG = UnifyPlaceAddress.class.getName();

    public interface OnUnifyComplete {
        void onUnifySuccess(PlaceAddress placeAddress);
        void onUnifyFailed(Exception e);
    }

    private MainActivity parentActivity;
    private OnUnifyComplete unifyResponse;
    private String oriPlaceId;

    private PlaceAddress unifiedPlaceAddress;
    private ArrayList<Boolean> isAllPlaceAddressUnified = new ArrayList<>();

    public UnifyPlaceAddress(MainActivity parentActivity, String oriPlaceId, OnUnifyComplete unifyResponse){
        this.parentActivity = parentActivity;
        this.oriPlaceId = oriPlaceId;
        this.unifyResponse = unifyResponse;

        getPlaceDetails();
    }

    private void getPlaceDetails(){
        RetrofitInstance.getPlaceAddressCall(parentActivity,oriPlaceId)
                .enqueue(new PlaceAddressCallback<PlacesDetailsJson>() {
                    @Override
                    public void onPlaceDetailsSuccess(final PlaceAddress placeAddress) {
                        unifiedPlaceAddress = placeAddress;
                        initUnificationBooleanArray();

                        if(placeAddress != null && placeAddress.country != null){
                            //use findplacefromtext to search first because it is more precise and accurate
                            // and i dont have to care about what user type as
                            //the query is generated from place details retrieved andn ot from user input.
                            //then if the place id returned from findplacefromtext does not have any place details
                            //attached when searched, then will try again with the place autocomplete api
                            //and get the first result's place id and then get the place details again
                            //ALL THESE STEPS are to ensure that it really can get correct place details from various countries.and not getting null results
                            //SOLVED able to get penang's place details but not able to get if is akihabara japan when using findplacefromtext
                            //and SOLVED able to get akihabara japan place details but not penang's 1 when using place autocomplete api
                            //by searching using findplacefromtext at first and then retry again with place autocomplete api if result has error or is null
                            //tested on akihabara japan penang and moscow russia
                            //need to have country in also because if not it will be harder to search for results. might be empty result
                            unifiedPlaceAddress.lat = placeAddress.lat;
                            unifiedPlaceAddress.lng = placeAddress.lng;
                            getSublocality(placeAddress);
                            getLocality(placeAddress);
                            getAdministrativeArea(placeAddress);
                        }else{
                            onPlaceAddressError("place address is returned null");
                        }
                    }

                    @Override
                    public void onPlaceDetailsFailure(Exception e) {
                        initUnificationBooleanArray();
                        onPlaceAddressError(e.getMessage());
                        e.printStackTrace();
                    }
                });
    }

    private void getSublocality(PlaceAddress placeAddress){
        //****************************SUBLOCALITY******************************************/
        final StringBuilder combinedSublocality = new StringBuilder();
        combinedSublocality.append("");

        if(placeAddress.sublocality != null){
            combinedSublocality.append(" ");
            combinedSublocality.append(placeAddress.sublocality);
            combinedSublocality.append(" ");
        }

        if(placeAddress.sublocality_level_1 != null){
            combinedSublocality.append(" ");
            combinedSublocality.append(placeAddress.sublocality_level_1);
            combinedSublocality.append(" ");
        }

        if(placeAddress.sublocality_level_2 != null){
            combinedSublocality.append(" ");
            combinedSublocality.append(placeAddress.sublocality_level_2);
            combinedSublocality.append(" ");
        }

        if(placeAddress.sublocality_level_3 != null){
            combinedSublocality.append(" ");
            combinedSublocality.append(placeAddress.sublocality_level_3);
            combinedSublocality.append(" ");
        }

        if(placeAddress.sublocality_level_4 != null){
            combinedSublocality.append(" ");
            combinedSublocality.append(placeAddress.sublocality_level_4);
            combinedSublocality.append(" ");
        }

        if(placeAddress.sublocality_level_5 != null){
            combinedSublocality.append(" ");
            combinedSublocality.append(placeAddress.sublocality_level_5);
            combinedSublocality.append(" ");
        }

        if(!combinedSublocality.toString().trim().isEmpty()){
            if(placeAddress.administrative_area_level_1 != null){
                combinedSublocality.append(" ");
                combinedSublocality.append(placeAddress.administrative_area_level_1);
                combinedSublocality.append(" ");
            }

            if(placeAddress.country != null){
                combinedSublocality.append(" ");
                combinedSublocality.append(placeAddress.country);
                combinedSublocality.append(" ");
            }

            RetrofitInstance.searchPlaceQueryCall(parentActivity,combinedSublocality.toString())
                    .enqueue(new SearchPlaceCallback<SearchPlaceResult>() {
                        @Override
                        public void onSearchPlaceSuccess(String placeId) {
                            RetrofitInstance.getPlaceAddressCall(parentActivity,placeId)
                                    .enqueue(new PlaceAddressCallback<PlacesDetailsJson>() {
                                        @Override
                                        public void onPlaceDetailsSuccess(PlaceAddress tempPlaceAddress) {
                                            if(tempPlaceAddress != null){
                                                unifiedPlaceAddress.sublocality = tempPlaceAddress.sublocality;
                                                unifiedPlaceAddress.sublocality_level_1 = tempPlaceAddress.sublocality_level_1;
                                                unifiedPlaceAddress.sublocality_level_2 = tempPlaceAddress.sublocality_level_2;
                                                unifiedPlaceAddress.sublocality_level_3 = tempPlaceAddress.sublocality_level_3;
                                                unifiedPlaceAddress.sublocality_level_4 = tempPlaceAddress.sublocality_level_4;
                                                unifiedPlaceAddress.sublocality_level_5 = tempPlaceAddress.sublocality_level_5;

                                                handlePlaceAddressUnification(0,true);
                                            }else{
                                                getSublocalityRetryWithAutoComplete(combinedSublocality.toString());
                                            }
                                        }

                                        @Override
                                        public void onPlaceDetailsFailure(Exception e) {
                                            getSublocalityRetryWithAutoComplete(combinedSublocality.toString());
                                        }
                                    });
                        }

                        @Override
                        public void onSearchPlaceFailure(Exception e) {
                            getSublocalityRetryWithAutoComplete(combinedSublocality.toString());
                        }
                    });
        }else{
            handlePlaceAddressUnification(0,true);
        }
    }

    private void getSublocalityRetryWithAutoComplete(String combinedSublocality){
        RetrofitInstance.searchAutoCompleteQueryCall(parentActivity,combinedSublocality)
                .enqueue(new SearchAutoCompleteCallback<PlaceAutoCompleteResult>() {
                    @Override
                    public void onSearchAutoCompleteSuccess(String placeId) {
                        RetrofitInstance.getPlaceAddressCall(parentActivity,placeId)
                                .enqueue(new PlaceAddressCallback<PlacesDetailsJson>() {
                                    @Override
                                    public void onPlaceDetailsSuccess(PlaceAddress tempPlaceAddress) {
                                        if(tempPlaceAddress != null){
                                            unifiedPlaceAddress.sublocality = tempPlaceAddress.sublocality;
                                            unifiedPlaceAddress.sublocality_level_1 = tempPlaceAddress.sublocality_level_1;
                                            unifiedPlaceAddress.sublocality_level_2 = tempPlaceAddress.sublocality_level_2;
                                            unifiedPlaceAddress.sublocality_level_3 = tempPlaceAddress.sublocality_level_3;
                                            unifiedPlaceAddress.sublocality_level_4 = tempPlaceAddress.sublocality_level_4;
                                            unifiedPlaceAddress.sublocality_level_5 = tempPlaceAddress.sublocality_level_5;

                                            handlePlaceAddressUnification(0,true);
                                        }else{
                                            handlePlaceAddressUnification(0,false);
                                        }
                                    }

                                    @Override
                                    public void onPlaceDetailsFailure(Exception e) {
                                        handlePlaceAddressUnification(0,false);
                                        e.printStackTrace();
                                    }
                                });
                    }

                    @Override
                    public void onSearchAutoCompleteFailure(Exception e) {
                        e.printStackTrace();
                        handlePlaceAddressUnification(0,false);
                    }
                });
    }

    private void getLocality(PlaceAddress placeAddress){
        //****************************LOCALITY******************************************/

        if(placeAddress.locality != null && !placeAddress.locality.trim().isEmpty()){
            final StringBuilder combinedLocality = new StringBuilder();
            combinedLocality.append("");
            combinedLocality.append(placeAddress.locality);

            if(placeAddress.administrative_area_level_1 != null){
                combinedLocality.append(" ");
                combinedLocality.append( placeAddress.administrative_area_level_1);
                combinedLocality.append(" ");
            }

            if(placeAddress.country != null){
                combinedLocality.append(" ");
                combinedLocality.append( placeAddress.country);
                combinedLocality.append(" ");
            }

            RetrofitInstance.searchPlaceQueryCall(parentActivity,combinedLocality.toString())
                    .enqueue(new SearchPlaceCallback<SearchPlaceResult>() {
                        @Override
                        public void onSearchPlaceSuccess(String placeId) {
                            RetrofitInstance.getPlaceAddressCall(parentActivity,placeId)
                                    .enqueue(new PlaceAddressCallback<PlacesDetailsJson>() {
                                        @Override
                                        public void onPlaceDetailsSuccess(PlaceAddress tempPlaceAddress) {
                                            if(tempPlaceAddress != null){
                                                unifiedPlaceAddress.locality = tempPlaceAddress.locality;

                                                handlePlaceAddressUnification(1,true);
                                            }else{
                                                getLocalityRetryWithAutoComplete(combinedLocality.toString());
                                            }
                                        }

                                        @Override
                                        public void onPlaceDetailsFailure(Exception e) {
                                            getLocalityRetryWithAutoComplete(combinedLocality.toString());
                                        }
                                    });
                        }

                        @Override
                        public void onSearchPlaceFailure(Exception e) {
                            getLocalityRetryWithAutoComplete(combinedLocality.toString());
                        }
                    });
        }else{
            handlePlaceAddressUnification(1,true);
        }
    }

    private void getLocalityRetryWithAutoComplete(String combinedLocality){
        RetrofitInstance.searchAutoCompleteQueryCall(parentActivity,combinedLocality)
                .enqueue(new SearchAutoCompleteCallback<PlaceAutoCompleteResult>() {
                    @Override
                    public void onSearchAutoCompleteSuccess(String placeId) {
                        RetrofitInstance.getPlaceAddressCall(parentActivity,placeId)
                                .enqueue(new PlaceAddressCallback<PlacesDetailsJson>() {
                                    @Override
                                    public void onPlaceDetailsSuccess(PlaceAddress tempPlaceAddress) {
                                        if(tempPlaceAddress != null){
                                            unifiedPlaceAddress.locality = tempPlaceAddress.locality;

                                            handlePlaceAddressUnification(1,true);
                                        }else{
                                            handlePlaceAddressUnification(1,false);
                                        }
                                    }

                                    @Override
                                    public void onPlaceDetailsFailure(Exception e) {
                                        handlePlaceAddressUnification(1,false);
                                        e.printStackTrace();
                                    }
                                });
                    }

                    @Override
                    public void onSearchAutoCompleteFailure(Exception e) {
                        e.printStackTrace();
                        handlePlaceAddressUnification(1,false);
                    }
                });
    }

    private void getAdministrativeArea(PlaceAddress placeAddress){
        //*********************************ADMINISTRATIVE AREA***************************************/
        final StringBuilder combinedAdminstrativeArea = new StringBuilder();
        combinedAdminstrativeArea.append("");

        if(placeAddress.administrative_area_level_1 != null){
            combinedAdminstrativeArea.append(" ");
            combinedAdminstrativeArea.append(placeAddress.administrative_area_level_1);
            combinedAdminstrativeArea.append(" ");
        }

        if(placeAddress.administrative_area_level_2 != null){
            combinedAdminstrativeArea.append(" ");
            combinedAdminstrativeArea.append(placeAddress.administrative_area_level_2);
            combinedAdminstrativeArea.append(" ");
        }

        if(placeAddress.administrative_area_level_3 != null){
            combinedAdminstrativeArea.append(" ");
            combinedAdminstrativeArea.append(placeAddress.administrative_area_level_3);
            combinedAdminstrativeArea.append(" ");
        }

        if(placeAddress.administrative_area_level_4 != null){
            combinedAdminstrativeArea.append(" ");
            combinedAdminstrativeArea.append(placeAddress.administrative_area_level_4);
            combinedAdminstrativeArea.append(" ");
        }

        if(placeAddress.administrative_area_level_5 != null){
            combinedAdminstrativeArea.append(" ");
            combinedAdminstrativeArea.append(placeAddress.administrative_area_level_5);
            combinedAdminstrativeArea.append(" ");
        }

        if(!combinedAdminstrativeArea.toString().trim().isEmpty()){
            if(placeAddress.country != null){
                combinedAdminstrativeArea.append(" ");
                combinedAdminstrativeArea.append(placeAddress.country);
                combinedAdminstrativeArea.append(" ");
            }
            RetrofitInstance.searchPlaceQueryCall(parentActivity,combinedAdminstrativeArea.toString())
                    .enqueue(new SearchPlaceCallback<SearchPlaceResult>() {
                        @Override
                        public void onSearchPlaceSuccess(String placeId) {
                            RetrofitInstance.getPlaceAddressCall(parentActivity,placeId)
                                    .enqueue(new PlaceAddressCallback<PlacesDetailsJson>() {
                                        @Override
                                        public void onPlaceDetailsSuccess(PlaceAddress tempPlaceAddress) {
                                            if(tempPlaceAddress != null){
                                                unifiedPlaceAddress.administrative_area_level_1 = tempPlaceAddress.administrative_area_level_1;
                                                unifiedPlaceAddress.administrative_area_level_2 = tempPlaceAddress.administrative_area_level_2;
                                                unifiedPlaceAddress.administrative_area_level_3 = tempPlaceAddress.administrative_area_level_3;
                                                unifiedPlaceAddress.administrative_area_level_4 = tempPlaceAddress.administrative_area_level_4;
                                                unifiedPlaceAddress.administrative_area_level_5 = tempPlaceAddress.administrative_area_level_5;
                                                unifiedPlaceAddress.country = tempPlaceAddress.country;

                                                handlePlaceAddressUnification(2,true);
                                            }else{
                                                getAdministrativeAreaRetryWithAutoComplete(combinedAdminstrativeArea.toString());
                                            }
                                        }

                                        @Override
                                        public void onPlaceDetailsFailure(Exception e) {
                                            getAdministrativeAreaRetryWithAutoComplete(combinedAdminstrativeArea.toString());
                                        }
                                    });
                        }

                        @Override
                        public void onSearchPlaceFailure(Exception e) {
                            getAdministrativeAreaRetryWithAutoComplete(combinedAdminstrativeArea.toString());
                        }
                    });
        }else{
            handlePlaceAddressUnification(2,true);
        }
    }

    private void getAdministrativeAreaRetryWithAutoComplete(String combinedAdminstrativeArea){
        RetrofitInstance.searchAutoCompleteQueryCall(parentActivity,combinedAdminstrativeArea)
                .enqueue(new SearchAutoCompleteCallback<PlaceAutoCompleteResult>() {
                    @Override
                    public void onSearchAutoCompleteSuccess(String placeId) {
                        RetrofitInstance.getPlaceAddressCall(parentActivity,placeId)
                                .enqueue(new PlaceAddressCallback<PlacesDetailsJson>() {
                                    @Override
                                    public void onPlaceDetailsSuccess(PlaceAddress tempPlaceAddress) {
                                        if(tempPlaceAddress != null){
                                            unifiedPlaceAddress.administrative_area_level_1 = tempPlaceAddress.administrative_area_level_1;
                                            unifiedPlaceAddress.administrative_area_level_2 = tempPlaceAddress.administrative_area_level_2;
                                            unifiedPlaceAddress.administrative_area_level_3 = tempPlaceAddress.administrative_area_level_3;
                                            unifiedPlaceAddress.administrative_area_level_4 = tempPlaceAddress.administrative_area_level_4;
                                            unifiedPlaceAddress.administrative_area_level_5 = tempPlaceAddress.administrative_area_level_5;

                                            handlePlaceAddressUnification(2,true);
                                        }else{
                                            handlePlaceAddressUnification(2,false);
                                        }
                                    }

                                    @Override
                                    public void onPlaceDetailsFailure(Exception e) {
                                        handlePlaceAddressUnification(2,false);
                                        e.printStackTrace();
                                    }
                                });
                    }

                    @Override
                    public void onSearchAutoCompleteFailure(Exception e) {
                        e.printStackTrace();
                        handlePlaceAddressUnification(2,false);
                    }
                });
    }

    private void handlePlaceAddressUnification(int index, boolean isSuccess){
        if(!isSuccess){
            onPlaceAddressError(parentActivity.getString(R.string.error_occured_getting_place_details));
            return;
        }

        isAllPlaceAddressUnified.set(index,isSuccess);

        for(boolean isDownloaded : isAllPlaceAddressUnified){
            if(!isDownloaded){
                return;
            }
        }

        if(unifiedPlaceAddress != null){
            onPlaceAddressFullyCompleted(unifiedPlaceAddress);
        }else{
            onPlaceAddressError(parentActivity.getString(R.string.error_occured_getting_place_details));
        }
    }

    private void initUnificationBooleanArray(){
        isAllPlaceAddressUnified.clear();
        isAllPlaceAddressUnified.add(false);
        isAllPlaceAddressUnified.add(false);
        isAllPlaceAddressUnified.add(false);
    }

    private void onPlaceAddressFullyCompleted(PlaceAddress placeAddress){
        initUnificationBooleanArray();
        placeAddress.placeId = oriPlaceId;
        unifyResponse.onUnifySuccess(placeAddress);
    }

    private void onPlaceAddressError(String errorMsg){
        initUnificationBooleanArray();
        unifyResponse.onUnifyFailed(new Exception(errorMsg));
    }
}
