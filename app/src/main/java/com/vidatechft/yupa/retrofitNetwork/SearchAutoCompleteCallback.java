package com.vidatechft.yupa.retrofitNetwork;

import android.support.annotation.NonNull;

import com.vidatechft.yupa.classes.PlaceAutoCompleteGoogleApi.PlaceAutoCompleteResult;
import com.vidatechft.yupa.classes.SearchPlaceGoogleApi.SearchPlaceResult;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class SearchAutoCompleteCallback<T> implements Callback<PlaceAutoCompleteResult> {

    @Override
    public void onResponse(@NonNull Call<PlaceAutoCompleteResult> call, @NonNull Response<PlaceAutoCompleteResult> response) {
        if (response.errorBody() != null) {
            try {
                onSearchAutoCompleteFailure(new Exception(response.errorBody().string()));
            } catch (IOException e) {
                e.printStackTrace();
                onSearchAutoCompleteFailure(e);
            }
            return;
        }

        PlaceAutoCompleteResult result = response.body();
        if (result != null) {
            if(result.predictions != null && !result.predictions.isEmpty() && result.predictions.get(0) != null){
                onSearchAutoCompleteSuccess(result.predictions.get(0).placeId);
            }else{
                onSearchAutoCompleteFailure(new Exception("search autocomplete result predictions is null or empty"));
            }
        }else{
            onSearchAutoCompleteFailure(new Exception("search autocomplete result is null"));
        }

    }

    @Override
    public void onFailure(@NonNull Call<PlaceAutoCompleteResult> call, @NonNull Throwable t) {
        onSearchAutoCompleteFailure(new Exception(t));
    }

    public abstract void onSearchAutoCompleteSuccess(String placeId);

    public abstract void onSearchAutoCompleteFailure(Exception e);

}
