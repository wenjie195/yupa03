package com.vidatechft.yupa.retrofitNetwork;

import com.vidatechft.yupa.classes.PlaceAutoCompleteGoogleApi.PlaceAutoCompleteResult;
import com.vidatechft.yupa.classes.PlaceDetailsGoogleApi.PlacesDetailsJson;
import com.vidatechft.yupa.classes.PlaceSearchRecommendedGoogleApi.PlaceSearchRecommendedResult;
import com.vidatechft.yupa.classes.SearchPlaceGoogleApi.SearchPlaceResult;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GetPlaceDetailsService {
    //https://maps.googleapis.com/maps/api/place/details/json?placeid=ChIJN1t_tDeuEmsRUsoyG83frY4&fields=address_component&key=AIzaSyCBtpTbwMZLT36cR2BrIRefIYpitoBKqA0&language=en
    @GET("/maps/api/place/details/json")
    Call<PlacesDetailsJson> getPlaceDetails(@Query("placeid") String placeId, @Query("fields") String fields, @Query("key") String key, @Query("language") String language);

    //search places with text from here https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=akihabara%20japan&inputtype=textquery&key=AIzaSyCBtpTbwMZLT36cR2BrIRefIYpitoBKqA0
    @GET("/maps/api/place/findplacefromtext/json")
    Call<SearchPlaceResult> searchPlace(@Query("input") String query, @Query("inputtype") String inputType, @Query("key") String key, @Query("language") String language);

    //search place with autocomplete (i searched using findplacefromtext but sometimes he gives me place id that has no place details so i use autocomplete instead)
    //https://maps.googleapis.com/maps/api/place/autocomplete/json?input=Bandar%20Bayan%20Baru,%20Penang%20Malaysia&fields=placeid&key=AIzaSyCBtpTbwMZLT36cR2BrIRefIYpitoBKqA0&sessiontoken=1234567890&language=en
    @GET("/maps/api/place/autocomplete/json")
    Call<PlaceAutoCompleteResult> searchAutoComplete(@Query("input") String query, @Query("key") String key, @Query("language") String language);

    //get google search result for list of attractions or recommended activities to do using this query
    //https://maps.googleapis.com/maps/api/place/textsearch/json?query=penang%20what%20to%20do&language=en&key=AIzaSyCBtpTbwMZLT36cR2BrIRefIYpitoBKqA0
    @GET("/maps/api/place/textsearch/json")
    Call<PlaceSearchRecommendedResult> searchRecommendedPlaces(@Query("query") String query, @Query("key") String key, @Query("language") String language);
}