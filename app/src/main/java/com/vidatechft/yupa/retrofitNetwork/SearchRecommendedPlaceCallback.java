package com.vidatechft.yupa.retrofitNetwork;

import android.support.annotation.NonNull;

import com.vidatechft.yupa.classes.PlaceSearchRecommendedGoogleApi.PlaceRecommendedResult;
import com.vidatechft.yupa.classes.PlaceSearchRecommendedGoogleApi.PlaceSearchRecommendedResult;
import com.vidatechft.yupa.classes.SearchPlaceGoogleApi.SearchPlaceResult;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class SearchRecommendedPlaceCallback<T> implements Callback<PlaceSearchRecommendedResult> {

    @Override
    public void onResponse(@NonNull Call<PlaceSearchRecommendedResult> call, @NonNull Response<PlaceSearchRecommendedResult> response) {
        if (response.errorBody() != null) {
            try {
                onSearchPlaceFailure(new Exception(response.errorBody().string()));
            } catch (IOException e) {
                e.printStackTrace();
                onSearchPlaceFailure(e);
            }
            return;
        }

        PlaceSearchRecommendedResult result = response.body();
        if (result != null) {
            if(result.recommendedResults != null && !result.recommendedResults.isEmpty()){
                onSearchPlaceSuccess(result.recommendedResults);
            }else{
                onSearchPlaceFailure(new Exception("search recommended place result is null or empty"));
            }
        }else{
            onSearchPlaceFailure(new Exception("search recommended place result is null"));
        }

    }

    @Override
    public void onFailure(@NonNull Call<PlaceSearchRecommendedResult> call, @NonNull Throwable t) {
        onSearchPlaceFailure(new Exception(t));
    }

    public abstract void onSearchPlaceSuccess(ArrayList<PlaceRecommendedResult> recommendedPlaces);

    public abstract void onSearchPlaceFailure(Exception e);

}
