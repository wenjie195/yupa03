package com.vidatechft.yupa.retrofitNetwork;

import android.support.annotation.NonNull;

import com.vidatechft.yupa.classes.PlaceDetailsGoogleApi.AddressComponent;
import com.vidatechft.yupa.classes.PlaceDetailsGoogleApi.PlaceAddress;
import com.vidatechft.yupa.classes.PlaceDetailsGoogleApi.PlacesDetailsJson;
import com.vidatechft.yupa.classes.SearchPlaceGoogleApi.SearchPlaceResult;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class SearchPlaceCallback<T> implements Callback<SearchPlaceResult> {

    @Override
    public void onResponse(@NonNull Call<SearchPlaceResult> call, @NonNull Response<SearchPlaceResult> response) {
        if (response.errorBody() != null) {
            try {
                onSearchPlaceFailure(new Exception(response.errorBody().string()));
            } catch (IOException e) {
                e.printStackTrace();
                onSearchPlaceFailure(e);
            }
            return;
        }

        SearchPlaceResult result = response.body();
        if (result != null) {
            if(result.candidates != null && !result.candidates.isEmpty() && result.candidates.get(0) != null){
                onSearchPlaceSuccess(result.candidates.get(0).placeId);
            }else{
                onSearchPlaceFailure(new Exception("search place result candidates is null or empty"));
            }
        }else{
            onSearchPlaceFailure(new Exception("search place result is null"));
        }

    }

    @Override
    public void onFailure(@NonNull Call<SearchPlaceResult> call, @NonNull Throwable t) {
        onSearchPlaceFailure(new Exception(t));
    }

    public abstract void onSearchPlaceSuccess(String placeId);

    public abstract void onSearchPlaceFailure(Exception e);

}
