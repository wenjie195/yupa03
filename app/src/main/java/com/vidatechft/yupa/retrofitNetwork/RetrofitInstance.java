package com.vidatechft.yupa.retrofitNetwork;

import android.content.Context;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.classes.PlaceAutoCompleteGoogleApi.PlaceAutoCompleteResult;
import com.vidatechft.yupa.classes.PlaceDetailsGoogleApi.PlacesDetailsJson;
import com.vidatechft.yupa.classes.PlaceSearchRecommendedGoogleApi.PlaceRecommendedResult;
import com.vidatechft.yupa.classes.PlaceSearchRecommendedGoogleApi.PlaceSearchRecommendedResult;
import com.vidatechft.yupa.classes.SearchPlaceGoogleApi.SearchPlaceResult;
import com.vidatechft.yupa.utilities.Config;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

//from here https://medium.com/@prakash_pun/retrofit-a-simple-android-tutorial-48437e4e5a23
//and from here https://qiita.com/furusin_oriver/items/59dd0ae6dc795737eded
public class RetrofitInstance {
    private static Retrofit retrofit;
    private static final String BASE_URL = "https://maps.googleapis.com";

    public static Retrofit getRetrofitInstance() {
        if (retrofit == null) {
            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public static Call<PlacesDetailsJson> getPlaceAddressCall(Context parentActivity, String placeId){
        GetPlaceDetailsService service = getRetrofitInstance().create(GetPlaceDetailsService.class);
//        Call<PlacesDetailsJson> call = service.getPlaceDetails("ChIJN1t_tDeuEmsRUsoyG83frY4", Config.ADDRESS_COMPONENT + "," + Config.ADDRESS_PLACE_NAME,parentActivity.getString(R.string.google_geo_api_key));
        return service.getPlaceDetails(placeId, Config.ADDRESS_COMPONENT + "," + Config.ADDRESS_PLACE_NAME + "," + Config.ADDRESS_GEOMETRY,parentActivity.getString(R.string.google_geo_api_key), Config.ADDRESS_DEFAULT_LANGUAGE);
    }

    public static Call<SearchPlaceResult> searchPlaceQueryCall(Context parentActivity, String query){
        GetPlaceDetailsService service = getRetrofitInstance().create(GetPlaceDetailsService.class);
        return service.searchPlace(query, Config.PLACE_SEARCH_INPUT_TYPE_TEXT,parentActivity.getString(R.string.google_geo_api_key), Config.ADDRESS_DEFAULT_LANGUAGE);
    }

    public static Call<PlaceAutoCompleteResult> searchAutoCompleteQueryCall(Context parentActivity, String query){
        GetPlaceDetailsService service = getRetrofitInstance().create(GetPlaceDetailsService.class);
        return service.searchAutoComplete(query, parentActivity.getString(R.string.google_geo_api_key), Config.ADDRESS_DEFAULT_LANGUAGE);
    }

    public static Call<PlaceSearchRecommendedResult> searchRecommendedPlaceResultQueryCall(Context parentActivity, String query){
        GetPlaceDetailsService service = getRetrofitInstance().create(GetPlaceDetailsService.class);
        return service.searchRecommendedPlaces(query, parentActivity.getString(R.string.google_geo_api_key), Config.ADDRESS_DEFAULT_LANGUAGE);
    }
}
