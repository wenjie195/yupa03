package com.vidatechft.yupa.hostFragments;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.theartofdev.edmodo.cropper.CropImage;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.Accommodation;
import com.vidatechft.yupa.classes.Host;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.utilities.FilePath;
import com.vidatechft.yupa.utilities.GeneralFunction;
import com.vidatechft.yupa.utilities.KeyboardUtilities;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;
import static com.vidatechft.yupa.utilities.GeneralFunction.GlideImageSetting;

public class UploadAccommodationPhotoFragement extends Fragment implements View.OnClickListener {
    public static final String TAG = UploadAccommodationPhotoFragement.class.getName();
    private MainActivity parentActivity;
    private View view;
    private TextView numCountTV;
    private ImageButton editHouseButton1,editHouseButton2,editHouseButton3,editHouseButton4,editHouseButton5,upload_house_1, upload_house_2, upload_house_3,upload_house_4,upload_house_5;
    private EditText descriptionET1, descriptionET2, descriptionET3, descriptionET4, descriptionET5;
    private Button btn_next_upload_accommodation_photo, btn_skip_upload_accommodation_photo, addBTN, minusBTN;
    private ConstraintLayout descriptionCL1, descriptionCL2,descriptionCL3,descriptionCL4,descriptionCL5;
    private int imageButton = 0;
    private String filePath1,filePath2,filePath3,filePath4,filePath5;
//    private int value = 1;
    private String getAccommodationName1,getAccommodationName2,getAccommodationName3,getAccommodationName4,getAccommodationName5;
    private String noAvailable = "no available";
    private String getPushId;
    private DocumentReference ref1,ref2,ref3,ref4,ref5;
    private String setPushId1,setPushId2,setPushId3,setPushId4,setPushId5,getPushId1,getPushId2,getPushId3,
            getPushId4,getPushId5;
    private int value;
    private int count;
    private int goToSelectAvailableAmenityFragment;
    private Accommodation accommodation;
    private boolean checkRoomName = false;
    private boolean isFromDatabase = false;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private String documentPushId;
    private List<String> accommodationURL = new ArrayList<>();
    private List<String> accommodationPushID = new ArrayList<>();
    private List<String> accommodationName = new ArrayList<>();
    private List<ImageButton> imageButtons = new ArrayList<>();
    private List<EditText> editTexts = new ArrayList<>();
    private boolean isEmpty = false;
    private boolean isGoCropPhoto = false;
    private boolean isSubmit = false;
    private boolean isEditProfile = false;
    private Boolean hasImage1, hasImage2, hasImage3, hasImage4,hasImage5;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static UploadAccommodationPhotoFragement newInstance(MainActivity parentActivity) {
        UploadAccommodationPhotoFragement fragment = new UploadAccommodationPhotoFragement();
        fragment.parentActivity = parentActivity;
        return fragment;
    }

    public static UploadAccommodationPhotoFragement newInstance(MainActivity parentActivity, boolean isFromDatabase, String documentPushId, boolean isEditProfile) {
        UploadAccommodationPhotoFragement fragment = new UploadAccommodationPhotoFragement();
        fragment.parentActivity = parentActivity;
        fragment.isFromDatabase = isFromDatabase;
        fragment.documentPushId = documentPushId;
        fragment.isEditProfile = isEditProfile;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_upload_accommodation_photo_fragement, container, false);

        //text view
        numCountTV = view.findViewById(R.id.numCountTV);

        //image button
        upload_house_1 = view.findViewById(R.id.upload_house_1);
        upload_house_2 = view.findViewById(R.id.upload_house_2);
        upload_house_3 = view.findViewById(R.id.upload_house_3);
        upload_house_4 = view.findViewById(R.id.upload_house_4);
        upload_house_5 = view.findViewById(R.id.upload_house_5);
        editHouseButton1 = view.findViewById(R.id.editHouseButton1);
        editHouseButton2 = view.findViewById(R.id.editHouseButton2);
        editHouseButton3 = view.findViewById(R.id.editHouseButton3);
        editHouseButton4 = view.findViewById(R.id.editHouseButton4);
        editHouseButton5 = view.findViewById(R.id.editHouseButton5);

        //text view
        descriptionET1 = view.findViewById(R.id.descriptionET1);
        descriptionET2 = view.findViewById(R.id.descriptionET2);
        descriptionET3 = view.findViewById(R.id.descriptionET3);
        descriptionET4 = view.findViewById(R.id.descriptionET4);
        descriptionET5 = view.findViewById(R.id.descriptionET5);

        //constraint layout
        descriptionCL1 = view.findViewById(R.id.descriptionCL1);
        descriptionCL2 = view.findViewById(R.id.descriptionCL2);
        descriptionCL3 = view.findViewById(R.id.descriptionCL3);
        descriptionCL4 = view.findViewById(R.id.descriptionCL4);
        descriptionCL5 = view.findViewById(R.id.descriptionCL5);

        //button
        btn_next_upload_accommodation_photo = view.findViewById(R.id.btn_next_upload_accommodation_photo);
        btn_skip_upload_accommodation_photo = view.findViewById(R.id.btn_skip_upload_accommodation_photo);

        addBTN = view.findViewById(R.id.addBTN);
        minusBTN = view.findViewById(R.id.minusBTN);

        //onclick listener
        upload_house_1.setOnClickListener(this);
        upload_house_2.setOnClickListener(this);
        upload_house_3.setOnClickListener(this);
        upload_house_4.setOnClickListener(this);
        upload_house_5.setOnClickListener(this);
        editHouseButton1.setOnClickListener(this);
        editHouseButton2.setOnClickListener(this);
        editHouseButton3.setOnClickListener(this);
        editHouseButton4.setOnClickListener(this);
        editHouseButton5.setOnClickListener(this);
        btn_next_upload_accommodation_photo.setOnClickListener(this);
        btn_skip_upload_accommodation_photo.setOnClickListener(this);
        addBTN.setOnClickListener(this);
        minusBTN.setOnClickListener(this);

        //show default text value
        numCountTV.setText("1");

        //onTextChange listener
        numCountTV.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                showUploadCL(s.toString());
            }
        });
        //get the path of the accommodation details
        if(!isFromDatabase){
            setSharedPreferences();
            checkCL();
        }



        return view;
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.upload_house_1){
            isGoCropPhoto = true;
            if(GeneralFunction.isStorageAndCameraPermitted(parentActivity.getApplicationContext(),parentActivity)){
                GeneralFunction.showFileChooserRatio169(parentActivity,this);
                imageButton = 1;
            }
            KeyboardUtilities.hideSoftKeyboard(parentActivity);
        }

        if(v.getId() == R.id.upload_house_2){
            isGoCropPhoto = true;
            if(GeneralFunction.isStorageAndCameraPermitted(parentActivity.getApplicationContext(),parentActivity)){
                GeneralFunction.showFileChooserRatio169(parentActivity,this);
                imageButton = 2;
            }
            KeyboardUtilities.hideSoftKeyboard(parentActivity);
        }

        if(v.getId() == R.id.upload_house_3){
            isGoCropPhoto = true;
            if(GeneralFunction.isStorageAndCameraPermitted(parentActivity.getApplicationContext(),parentActivity)){
                GeneralFunction.showFileChooserRatio169(parentActivity,this);
                imageButton = 3;
            }
            KeyboardUtilities.hideSoftKeyboard(parentActivity);
        }

        if(v.getId() == R.id.upload_house_4){
            isGoCropPhoto = true;
            if(GeneralFunction.isStorageAndCameraPermitted(parentActivity.getApplicationContext(),parentActivity)){
                GeneralFunction.showFileChooserRatio169(parentActivity,this);
                imageButton = 4;
            }
            KeyboardUtilities.hideSoftKeyboard(parentActivity);
        }

        if(v.getId() == R.id.upload_house_5){
            isGoCropPhoto = true;
            if(GeneralFunction.isStorageAndCameraPermitted(parentActivity.getApplicationContext(),parentActivity)){
                GeneralFunction.showFileChooserRatio169(parentActivity,this);
                imageButton = 5;
            }
            KeyboardUtilities.hideSoftKeyboard(parentActivity);
        }

        if(v.getId() == R.id.editHouseButton1){
            GeneralFunction.editRoomNameFunc(descriptionET1,parentActivity);
        }

        if(v.getId() == R.id.editHouseButton2){
            GeneralFunction.editRoomNameFunc(descriptionET2,parentActivity);
        }

        if(v.getId() == R.id.editHouseButton3){
            GeneralFunction.editRoomNameFunc(descriptionET3,parentActivity);
        }

        if(v.getId() == R.id.editHouseButton4){
            GeneralFunction.editRoomNameFunc(descriptionET4,parentActivity);
        }
        if(v.getId() == R.id.editHouseButton5){
            GeneralFunction.editRoomNameFunc(descriptionET5,parentActivity);
        }

        if(v.getId() == R.id.btn_next_upload_accommodation_photo){
            submitAccommodationPhotoDetail();
        }

        if(v.getId() == R.id.btn_skip_upload_accommodation_photo){
            goToSelectAvailableAmenityFragment();
            value = 0;
        }

        if(v.getId() == R.id.addBTN){
            increaseInteger();
        }

        if(v.getId() == R.id.minusBTN){
            decreaseInteger();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode  == RESULT_OK) {
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    Uri resultUri = result.getUri();

                    if(imageButton == 1){
                        this.filePath1 = FilePath.getPath(parentActivity, resultUri);
                        GlideImageSetting(parentActivity, resultUri,upload_house_1);
                    }
                    if(imageButton == 2){
                        this.filePath2 = FilePath.getPath(parentActivity, resultUri);
                        GlideImageSetting(parentActivity, resultUri,upload_house_2);
                    }
                    if(imageButton == 3){
                        this.filePath3 = FilePath.getPath(parentActivity, resultUri);
                        GlideImageSetting(parentActivity, resultUri,upload_house_3);
                    }
                    if(imageButton == 4){
                        this.filePath4 = FilePath.getPath(parentActivity, resultUri);
                        GlideImageSetting(parentActivity, resultUri,upload_house_4);
                    }
                    if(imageButton == 5){
                        this.filePath5 = FilePath.getPath(parentActivity, resultUri);
                        GlideImageSetting(parentActivity, resultUri,upload_house_5);
                    }

                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Exception error = result.getError();
                }
            }

            if (requestCode == CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE) {
                if (ContextCompat.checkSelfPermission(parentActivity, Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_DENIED) {
                    ActivityCompat.requestPermissions(parentActivity, new String[]{Manifest.permission.CAMERA}, requestCode);
                }
            }
        }
    }

    public void increaseInteger() {
        if(value <5 ){
            value = value + 1;
            numCountTV.setText(String.valueOf(value));
            if(value == 5){
                Toast.makeText(parentActivity, "Maximum photo is "+ value, Toast.LENGTH_SHORT).show();
            }
            if(isFromDatabase){
                if(value < accommodationURL.size()+1){
                    value = 0;
                    imageButtons.clear();
                    editTexts.clear();
                    accommodationURL.clear();
                    accommodationName.clear();
                    accommodationPushID.clear();
                    retrieveFromFirestore();
                }
            }
        }


    }public void decreaseInteger() {
        if(value >1){
            value = value - 1;
            numCountTV.setText(String.valueOf(value));
            if(value == 1){
                Toast.makeText(parentActivity, "Minimum photo is "+ value, Toast.LENGTH_SHORT).show();
            }
        }
        if(value >= 1 ){
            if(value == 4){
                GeneralFunction.clearSharedPreferencesWithKey(parentActivity,Accommodation.UPLOAD_ACCOMMODATION_DETAILS_ALL,Accommodation.DIR_ACCOMMODATION_5);
                upload_house_5.setImageDrawable(null);
                descriptionET5.setText("");
            }
            if(value == 3){
                GeneralFunction.clearSharedPreferencesWithKey(parentActivity,Accommodation.UPLOAD_ACCOMMODATION_DETAILS_ALL,Accommodation.DIR_ACCOMMODATION_4);
                upload_house_4.setImageDrawable(null);
                descriptionET4.setText("");
            }
            if(value == 2){
                GeneralFunction.clearSharedPreferencesWithKey(parentActivity,Accommodation.UPLOAD_ACCOMMODATION_DETAILS_ALL,Accommodation.DIR_ACCOMMODATION_3);
                upload_house_3.setImageDrawable(null);
                descriptionET3.setText("");
            }
            if(value == 1){
                GeneralFunction.clearSharedPreferencesWithKey(parentActivity,Accommodation.UPLOAD_ACCOMMODATION_DETAILS_ALL,Accommodation.DIR_ACCOMMODATION_2);
                upload_house_2.setImageDrawable(null);
                descriptionET2.setText("");
            }
        }
    }

    private void showUploadCL(String s){
        value = Integer.parseInt(s);

        if(value != 0) {
            if(value == 2){
                GeneralFunction.fadeInAnimation(parentActivity,descriptionCL2);
            }
            if(value != 2 && value < 2){
                GeneralFunction.fadeOutAnimation(parentActivity,descriptionCL2);
            }
            if(value == 3){
                GeneralFunction.fadeInAnimation(parentActivity,descriptionCL3);
            }
            if(value != 3 && value < 3){
                GeneralFunction.fadeOutAnimation(parentActivity,descriptionCL3);
            }
            if(value == 4){
                GeneralFunction.fadeInAnimation(parentActivity,descriptionCL4);
            }
            if(value != 4 && value < 4){
                GeneralFunction.fadeOutAnimation(parentActivity,descriptionCL4);
            }
            if(value == 5){
                GeneralFunction.fadeInAnimation(parentActivity,descriptionCL5);
            }
            if(value != 5 && value < 5){
                GeneralFunction.fadeOutAnimation(parentActivity,descriptionCL5);
            }
        }
    }
    private void submitAccommodationPhotoDetail(){

        if(descriptionCL1.getVisibility() != View.GONE && descriptionCL1.getVisibility() == View.VISIBLE ){
            if(filePath1 == null ){
                GeneralFunction.checkAndReturnString(filePath1,parentActivity.getString(R.string.error_please_upload_a_photo_room1),parentActivity);
            }
            if(descriptionET1.getText().toString().trim().equals("")){
                GeneralFunction.openMessageNotificationDialog(parentActivity,"Please enter room name 1",R.drawable.notice_bad,"invalid_string");
                checkRoomName = false;
                return;
            }
            else{
                getAccommodationName1 = descriptionET1.getText().toString();
            }
        }

        if(descriptionCL2.getVisibility() != View.GONE && descriptionCL2.getVisibility() == View.VISIBLE){
            if(filePath2 == null ){
                GeneralFunction.checkAndReturnString(filePath2,parentActivity.getString(R.string.error_please_upload_a_photo_room2),parentActivity);
            }
            if(descriptionET2.getText().toString().trim().equals("")){
                GeneralFunction.openMessageNotificationDialog(parentActivity,"Please enter room name 2",R.drawable.notice_bad,"invalid_string");
                checkRoomName = false;
                return;
            }
            else{
                getAccommodationName2 = descriptionET2.getText().toString();
            }
        }

        if(descriptionCL3.getVisibility() != View.GONE && descriptionCL3.getVisibility() == View.VISIBLE){
            if(filePath3 == null ){
                GeneralFunction.checkAndReturnString(filePath3,parentActivity.getString(R.string.error_please_upload_a_photo_room3),parentActivity);
            }
            if(descriptionET3.getText().toString().trim().equals("")){
                GeneralFunction.openMessageNotificationDialog(parentActivity,"Please enter room name 3",R.drawable.notice_bad,"invalid_string");
                checkRoomName = false;
                return;
            }
            else{
                getAccommodationName3 = descriptionET3.getText().toString();
            }
        }

        if(descriptionCL4.getVisibility() != View.GONE && descriptionCL4.getVisibility() == View.VISIBLE){
            if(filePath4 == null ){
                GeneralFunction.checkAndReturnString(filePath4,parentActivity.getString(R.string.error_please_upload_a_photo_room4),parentActivity);
            }
            if(descriptionET4.getText().toString().trim().equals("")){
                GeneralFunction.openMessageNotificationDialog(parentActivity,"Please enter room name 4",R.drawable.notice_bad,"invalid_string");
                checkRoomName = false;
                return;
            }
            else{
                getAccommodationName4 = descriptionET4.getText().toString();
            }
        }

        if(descriptionCL5.getVisibility() != View.GONE && descriptionCL5.getVisibility() == View.VISIBLE){
            if(filePath5 == null){
                GeneralFunction.checkAndReturnString(filePath5,parentActivity.getString(R.string.error_please_upload_a_photo_room5),parentActivity);
            }
            if(descriptionET5.getText().toString().trim().equals("")){
                GeneralFunction.openMessageNotificationDialog(parentActivity,"Please enter room name 5",R.drawable.notice_bad,"invalid_string");
                checkRoomName = false;
                return;
            }
            else{
                getAccommodationName5 = descriptionET5.getText().toString();
            }
        }
        if(!checkRoomName){
            if(isValidInput(getAccommodationName1,getAccommodationName2,getAccommodationName3,getAccommodationName4,getAccommodationName5)){

                if(filePath1 != null || !TextUtils.isEmpty(filePath1) ||filePath2 != null || !TextUtils.isEmpty(filePath2) ||filePath3 != null ||
                        !TextUtils.isEmpty(filePath3) ||filePath4 != null || !TextUtils.isEmpty(filePath4) ||filePath5 != null || !TextUtils.isEmpty(filePath5)){

                    parentActivity.initializeProgressIndicator(parentActivity.getString(R.string.host_upload_room),false,this);
                    parentActivity.controlProgressIndicator(true,this);
                    if(!isFromDatabase){
                        //IF USING GET NORMALLY THE VALUE IS EMPTY
                        getPushId = GeneralFunction.getDocumentPath(parentActivity,Room.ACCOMMODATION_DETIALS,Room.DIR_DOCUMENT, "");

                        //get the path of the uploadAccommodation photo path
                        getPushId1 = getPushId(Accommodation.DIR_ACCOMMODATION_1,getPushId1);
                        getPushId2 = getPushId(Accommodation.DIR_ACCOMMODATION_2,getPushId2);
                        getPushId3 = getPushId(Accommodation.DIR_ACCOMMODATION_3,getPushId3);
                        getPushId4 = getPushId(Accommodation.DIR_ACCOMMODATION_4,getPushId4);
                        getPushId5 = getPushId(Accommodation.DIR_ACCOMMODATION_5,getPushId5);
//                    getPushId2 = GeneralFunction.getDocumentPath(parentActivity,Room.ACCOMMODATION_DETIALS,Accommodation.DIR_ACCOMMODATION_2,"");
//                    getPushId3 = GeneralFunction.getDocumentPath(parentActivity,Room.ACCOMMODATION_DETIALS,Accommodation.DIR_ACCOMMODATION_3,"");
//                    getPushId4 = GeneralFunction.getDocumentPath(parentActivity,Room.ACCOMMODATION_DETIALS,Accommodation.DIR_ACCOMMODATION_4,"");
//                    getPushId5 = GeneralFunction.getDocumentPath(parentActivity,Room.ACCOMMODATION_DETIALS,Accommodation.DIR_ACCOMMODATION_5,"");
                    }

//                    if(getPushId == null){
//                        getPushId = documentPushId;
//                    }


                    if(getPushId != null){

                        if(value != 0) {
                            if(value < 2 && value > 0){
                                if(getPushId1 != null && !getPushId1.trim().equals("")){
                                    ref1 = GeneralFunction.getFirestoreInstance()
                                            .collection(Accommodation.URL_ACCOMMODATION)
                                            .document(getPushId)
                                            .collection(Accommodation.DIR_ACCOMMODATION)
                                            .document(getPushId1);
                                    accommodation = new Accommodation(getAccommodationName1,filePath1,getPushId1);
                                    if(!isFromDatabase){
                                        GeneralFunction.saveAccommodationSharedPreferences(parentActivity, accommodation, Accommodation.UPLOAD_ACCOMMODATION_DETAILS_ALL, Accommodation.DIR_ACCOMMODATION_1);

                                    }
                                }
                                else{
                                    ref1 = GeneralFunction.getFirestoreInstance()
                                            .collection(Accommodation.URL_ACCOMMODATION)
                                            .document(getPushId)
                                            .collection(Accommodation.DIR_ACCOMMODATION)
                                            .document();
                                    setPushId1 = ref1.getId();
                                    accommodation = new Accommodation(getAccommodationName1,filePath1,setPushId1);
                                    if(!isFromDatabase){
                                        GeneralFunction.saveAccommodationSharedPreferences(parentActivity, accommodation, Accommodation.UPLOAD_ACCOMMODATION_DETAILS_ALL, Accommodation.DIR_ACCOMMODATION_1);

                                    }
                                }
                            }
                            if(value < 3 && value > 1){
                                if(getPushId1 != null && !getPushId1.trim().equals("")){
                                    ref1 = GeneralFunction.getFirestoreInstance()
                                            .collection(Accommodation.URL_ACCOMMODATION)
                                            .document(getPushId)
                                            .collection(Accommodation.DIR_ACCOMMODATION)
                                            .document(getPushId1);
                                    accommodation = new Accommodation(getAccommodationName1,filePath1,getPushId1);
                                    if(!isFromDatabase){
                                        GeneralFunction.saveAccommodationSharedPreferences(parentActivity, accommodation, Accommodation.UPLOAD_ACCOMMODATION_DETAILS_ALL, Accommodation.DIR_ACCOMMODATION_1);

                                    }
                                }
                                else{
                                    ref1 = GeneralFunction.getFirestoreInstance()
                                            .collection(Accommodation.URL_ACCOMMODATION)
                                            .document(getPushId)
                                            .collection(Accommodation.DIR_ACCOMMODATION)
                                            .document();
                                    setPushId1 = ref1.getId();
                                    accommodation = new Accommodation(getAccommodationName1,filePath1,setPushId1);
                                    if(!isFromDatabase){
                                        GeneralFunction.saveAccommodationSharedPreferences(parentActivity, accommodation, Accommodation.UPLOAD_ACCOMMODATION_DETAILS_ALL, Accommodation.DIR_ACCOMMODATION_1);

                                    }
                                }

                                if(getPushId2 != null && !getPushId2.trim().equals("")){
                                    ref2 = GeneralFunction.getFirestoreInstance()
                                            .collection(Accommodation.URL_ACCOMMODATION)
                                            .document(getPushId)
                                            .collection(Accommodation.DIR_ACCOMMODATION)
                                            .document(getPushId2);
                                    accommodation = new Accommodation(getAccommodationName2,filePath2,getPushId2);
                                    if(!isFromDatabase){
                                        GeneralFunction.saveAccommodationSharedPreferences(parentActivity, accommodation, Accommodation.UPLOAD_ACCOMMODATION_DETAILS_ALL, Accommodation.DIR_ACCOMMODATION_2);

                                    }
                                }
                                else{
                                    ref2 = GeneralFunction.getFirestoreInstance()
                                            .collection(Accommodation.URL_ACCOMMODATION)
                                            .document(getPushId)
                                            .collection(Accommodation.DIR_ACCOMMODATION)
                                            .document();
                                    setPushId2 = ref2.getId();
                                    accommodation = new Accommodation(getAccommodationName2,filePath2,setPushId2);
                                    if(!isFromDatabase){
                                        GeneralFunction.saveAccommodationSharedPreferences(parentActivity, accommodation, Accommodation.UPLOAD_ACCOMMODATION_DETAILS_ALL, Accommodation.DIR_ACCOMMODATION_2);

                                    }
                                }
                            }
                            if(value < 4 && value > 2){
                                if(getPushId1 != null && !getPushId1.trim().equals("")){
                                    ref1 = GeneralFunction.getFirestoreInstance()
                                            .collection(Accommodation.URL_ACCOMMODATION)
                                            .document(getPushId)
                                            .collection(Accommodation.DIR_ACCOMMODATION)
                                            .document(getPushId1);
                                    accommodation = new Accommodation(getAccommodationName1,filePath1,getPushId1);
                                    if(!isFromDatabase){
                                        GeneralFunction.saveAccommodationSharedPreferences(parentActivity, accommodation, Accommodation.UPLOAD_ACCOMMODATION_DETAILS_ALL, Accommodation.DIR_ACCOMMODATION_1);

                                    }
                                }
                                else{
                                    ref1 = GeneralFunction.getFirestoreInstance()
                                            .collection(Accommodation.URL_ACCOMMODATION)
                                            .document(getPushId)
                                            .collection(Accommodation.DIR_ACCOMMODATION)
                                            .document();
                                    setPushId1 = ref1.getId();
                                    accommodation = new Accommodation(getAccommodationName1,filePath1,setPushId1);
                                    if(!isFromDatabase){
                                        GeneralFunction.saveAccommodationSharedPreferences(parentActivity, accommodation, Accommodation.UPLOAD_ACCOMMODATION_DETAILS_ALL, Accommodation.DIR_ACCOMMODATION_1);

                                    }
                                }

                                if(getPushId2 != null && !getPushId2.trim().equals("")){
                                    ref2 = GeneralFunction.getFirestoreInstance()
                                            .collection(Accommodation.URL_ACCOMMODATION)
                                            .document(getPushId)
                                            .collection(Accommodation.DIR_ACCOMMODATION)
                                            .document(getPushId2);
                                    accommodation = new Accommodation(getAccommodationName2,filePath2,getPushId2);
                                    if(!isFromDatabase){
                                        GeneralFunction.saveAccommodationSharedPreferences(parentActivity, accommodation, Accommodation.UPLOAD_ACCOMMODATION_DETAILS_ALL, Accommodation.DIR_ACCOMMODATION_2);

                                    }
                                }
                                else{
                                    ref2 = GeneralFunction.getFirestoreInstance()
                                            .collection(Accommodation.URL_ACCOMMODATION)
                                            .document(getPushId)
                                            .collection(Accommodation.DIR_ACCOMMODATION)
                                            .document();
                                    setPushId2 = ref2.getId();
                                    accommodation = new Accommodation(getAccommodationName2,filePath2,setPushId2);
                                    if(!isFromDatabase){
                                        GeneralFunction.saveAccommodationSharedPreferences(parentActivity, accommodation, Accommodation.UPLOAD_ACCOMMODATION_DETAILS_ALL, Accommodation.DIR_ACCOMMODATION_2);

                                    }
                                }
                                if(getPushId3 != null && !getPushId3.trim().equals("")){
                                    ref3 = GeneralFunction.getFirestoreInstance()
                                            .collection(Accommodation.URL_ACCOMMODATION)
                                            .document(getPushId)
                                            .collection(Accommodation.DIR_ACCOMMODATION)
                                            .document(getPushId3);
                                    accommodation = new Accommodation(getAccommodationName3,filePath3,getPushId3);
                                    if(!isFromDatabase){
                                        GeneralFunction.saveAccommodationSharedPreferences(parentActivity, accommodation, Accommodation.UPLOAD_ACCOMMODATION_DETAILS_ALL, Accommodation.DIR_ACCOMMODATION_3);

                                    }
                                }
                                else{
                                    ref3 = GeneralFunction.getFirestoreInstance()
                                            .collection(Accommodation.URL_ACCOMMODATION)
                                            .document(getPushId)
                                            .collection(Accommodation.DIR_ACCOMMODATION)
                                            .document();
                                    setPushId3 = ref3.getId();
                                    accommodation = new Accommodation(getAccommodationName3,filePath3,setPushId3);
                                    if(!isFromDatabase){
                                        GeneralFunction.saveAccommodationSharedPreferences(parentActivity, accommodation, Accommodation.UPLOAD_ACCOMMODATION_DETAILS_ALL, Accommodation.DIR_ACCOMMODATION_3);

                                    }
                                }
                            }
                            if(value < 5 && value > 3){
                                if(getPushId1 != null && !getPushId1.trim().equals("")){
                                    ref1 = GeneralFunction.getFirestoreInstance()
                                            .collection(Accommodation.URL_ACCOMMODATION)
                                            .document(getPushId)
                                            .collection(Accommodation.DIR_ACCOMMODATION)
                                            .document(getPushId1);
                                    accommodation = new Accommodation(getAccommodationName1,filePath1,getPushId1);
                                    if(!isFromDatabase){
                                        GeneralFunction.saveAccommodationSharedPreferences(parentActivity, accommodation, Accommodation.UPLOAD_ACCOMMODATION_DETAILS_ALL, Accommodation.DIR_ACCOMMODATION_1);

                                    }
                                }
                                else{
                                    ref1 = GeneralFunction.getFirestoreInstance()
                                            .collection(Accommodation.URL_ACCOMMODATION)
                                            .document(getPushId)
                                            .collection(Accommodation.DIR_ACCOMMODATION)
                                            .document();
                                    setPushId1 = ref1.getId();
                                    accommodation = new Accommodation(getAccommodationName1,filePath1,setPushId1);
                                    if(!isFromDatabase){
                                        GeneralFunction.saveAccommodationSharedPreferences(parentActivity, accommodation, Accommodation.UPLOAD_ACCOMMODATION_DETAILS_ALL, Accommodation.DIR_ACCOMMODATION_1);

                                    }
                                }

                                if(getPushId2 != null && !getPushId2.trim().equals("")){
                                    ref2 = GeneralFunction.getFirestoreInstance()
                                            .collection(Accommodation.URL_ACCOMMODATION)
                                            .document(getPushId)
                                            .collection(Accommodation.DIR_ACCOMMODATION)
                                            .document(getPushId2);
                                    accommodation = new Accommodation(getAccommodationName2,filePath2,getPushId2);
                                    if(!isFromDatabase){
                                        GeneralFunction.saveAccommodationSharedPreferences(parentActivity, accommodation, Accommodation.UPLOAD_ACCOMMODATION_DETAILS_ALL, Accommodation.DIR_ACCOMMODATION_2);

                                    }
                                }
                                else{
                                    ref2 = GeneralFunction.getFirestoreInstance()
                                            .collection(Accommodation.URL_ACCOMMODATION)
                                            .document(getPushId)
                                            .collection(Accommodation.DIR_ACCOMMODATION)
                                            .document();
                                    setPushId2 = ref2.getId();
                                    accommodation = new Accommodation(getAccommodationName2,filePath2,setPushId2);
                                    if(!isFromDatabase){
                                        GeneralFunction.saveAccommodationSharedPreferences(parentActivity, accommodation, Accommodation.UPLOAD_ACCOMMODATION_DETAILS_ALL, Accommodation.DIR_ACCOMMODATION_2);

                                    }
                                }
                                if(getPushId3 != null && !getPushId3.trim().equals("")){
                                    ref3 = GeneralFunction.getFirestoreInstance()
                                            .collection(Accommodation.URL_ACCOMMODATION)
                                            .document(getPushId)
                                            .collection(Accommodation.DIR_ACCOMMODATION)
                                            .document(getPushId3);
                                    accommodation = new Accommodation(getAccommodationName3,filePath3,getPushId3);
                                    if(!isFromDatabase){
                                        GeneralFunction.saveAccommodationSharedPreferences(parentActivity, accommodation, Accommodation.UPLOAD_ACCOMMODATION_DETAILS_ALL, Accommodation.DIR_ACCOMMODATION_3);

                                    }
                                }
                                else{
                                    ref3 = GeneralFunction.getFirestoreInstance()
                                            .collection(Accommodation.URL_ACCOMMODATION)
                                            .document(getPushId)
                                            .collection(Accommodation.DIR_ACCOMMODATION)
                                            .document();
                                    setPushId3 = ref3.getId();
                                    accommodation = new Accommodation(getAccommodationName3,filePath3,setPushId3);
                                    if(!isFromDatabase){
                                        GeneralFunction.saveAccommodationSharedPreferences(parentActivity, accommodation, Accommodation.UPLOAD_ACCOMMODATION_DETAILS_ALL, Accommodation.DIR_ACCOMMODATION_3);                                }

                                    }

                                if(getPushId4 != null && !getPushId4.trim().equals("")){
                                    ref4 = GeneralFunction.getFirestoreInstance()
                                            .collection(Accommodation.URL_ACCOMMODATION)
                                            .document(getPushId)
                                            .collection(Accommodation.DIR_ACCOMMODATION)
                                            .document(getPushId4);
                                    accommodation = new Accommodation(getAccommodationName4,filePath4,getPushId4);
                                    if(!isFromDatabase){
                                        GeneralFunction.saveAccommodationSharedPreferences(parentActivity, accommodation, Accommodation.UPLOAD_ACCOMMODATION_DETAILS_ALL, Accommodation.DIR_ACCOMMODATION_4);

                                    }
                                }
                                else{
                                    ref4 = GeneralFunction.getFirestoreInstance()
                                            .collection(Accommodation.URL_ACCOMMODATION)
                                            .document(getPushId)
                                            .collection(Accommodation.DIR_ACCOMMODATION)
                                            .document();
                                    setPushId4 = ref4.getId();
                                    accommodation = new Accommodation(getAccommodationName4,filePath4,setPushId4);
                                    if(!isFromDatabase){
                                        GeneralFunction.saveAccommodationSharedPreferences(parentActivity, accommodation, Accommodation.UPLOAD_ACCOMMODATION_DETAILS_ALL, Accommodation.DIR_ACCOMMODATION_4);                                }

                                }
                            }
                            if(value < 6 && value > 4){
                                if(getPushId1 != null && !getPushId1.trim().equals("")){
                                    ref1 = GeneralFunction.getFirestoreInstance()
                                            .collection(Accommodation.URL_ACCOMMODATION)
                                            .document(getPushId)
                                            .collection(Accommodation.DIR_ACCOMMODATION)
                                            .document(getPushId1);
                                    accommodation = new Accommodation(getAccommodationName1,filePath1,getPushId1);
                                    if(!isFromDatabase){
                                        GeneralFunction.saveAccommodationSharedPreferences(parentActivity, accommodation, Accommodation.UPLOAD_ACCOMMODATION_DETAILS_ALL, Accommodation.DIR_ACCOMMODATION_1);
                                    }
                                }
                                else{
                                    ref1 = GeneralFunction.getFirestoreInstance()
                                            .collection(Accommodation.URL_ACCOMMODATION)
                                            .document(getPushId)
                                            .collection(Accommodation.DIR_ACCOMMODATION)
                                            .document();
                                    setPushId1 = ref1.getId();
                                    accommodation = new Accommodation(getAccommodationName1,filePath1,setPushId1);
                                    if(!isFromDatabase){
                                        GeneralFunction.saveAccommodationSharedPreferences(parentActivity, accommodation, Accommodation.UPLOAD_ACCOMMODATION_DETAILS_ALL, Accommodation.DIR_ACCOMMODATION_1);                                }

                                }

                                if(getPushId2 != null && !getPushId2.trim().equals("")){
                                    ref2 = GeneralFunction.getFirestoreInstance()
                                            .collection(Accommodation.URL_ACCOMMODATION)
                                            .document(getPushId)
                                            .collection(Accommodation.DIR_ACCOMMODATION)
                                            .document(getPushId2);
                                    accommodation = new Accommodation(getAccommodationName2,filePath2,getPushId2);
                                    if(!isFromDatabase){
                                        GeneralFunction.saveAccommodationSharedPreferences(parentActivity, accommodation, Accommodation.UPLOAD_ACCOMMODATION_DETAILS_ALL, Accommodation.DIR_ACCOMMODATION_2);

                                    }
                                }
                                else{
                                    ref2 = GeneralFunction.getFirestoreInstance()
                                            .collection(Accommodation.URL_ACCOMMODATION)
                                            .document(getPushId)
                                            .collection(Accommodation.DIR_ACCOMMODATION)
                                            .document();
                                    setPushId2 = ref2.getId();
                                    accommodation = new Accommodation(getAccommodationName2,filePath2,setPushId2);
                                    if(!isFromDatabase){
                                        GeneralFunction.saveAccommodationSharedPreferences(parentActivity, accommodation, Accommodation.UPLOAD_ACCOMMODATION_DETAILS_ALL, Accommodation.DIR_ACCOMMODATION_2);                                }

                                }
                                if(getPushId3 != null && !getPushId3.trim().equals("")){
                                    ref3 = GeneralFunction.getFirestoreInstance()
                                            .collection(Accommodation.URL_ACCOMMODATION)
                                            .document(getPushId)
                                            .collection(Accommodation.DIR_ACCOMMODATION)
                                            .document(getPushId3);
                                    accommodation = new Accommodation(getAccommodationName3,filePath3,getPushId3);
                                    if(!isFromDatabase){
                                        GeneralFunction.saveAccommodationSharedPreferences(parentActivity, accommodation, Accommodation.UPLOAD_ACCOMMODATION_DETAILS_ALL, Accommodation.DIR_ACCOMMODATION_3);

                                    }
                                }
                                else{
                                    ref3 = GeneralFunction.getFirestoreInstance()
                                            .collection(Accommodation.URL_ACCOMMODATION)
                                            .document(getPushId)
                                            .collection(Accommodation.DIR_ACCOMMODATION)
                                            .document();
                                    setPushId3 = ref3.getId();
                                    accommodation = new Accommodation(getAccommodationName3,filePath3,setPushId3);
                                    if(!isFromDatabase){
                                        GeneralFunction.saveAccommodationSharedPreferences(parentActivity, accommodation, Accommodation.UPLOAD_ACCOMMODATION_DETAILS_ALL, Accommodation.DIR_ACCOMMODATION_3);                                }

                                }

                                if(getPushId4 != null && !getPushId4.trim().equals("")){
                                    ref4 = GeneralFunction.getFirestoreInstance()
                                            .collection(Accommodation.URL_ACCOMMODATION)
                                            .document(getPushId)
                                            .collection(Accommodation.DIR_ACCOMMODATION)
                                            .document(getPushId4);
                                    accommodation = new Accommodation(getAccommodationName4,filePath4,getPushId4);
                                    if(!isFromDatabase){
                                        GeneralFunction.saveAccommodationSharedPreferences(parentActivity, accommodation, Accommodation.UPLOAD_ACCOMMODATION_DETAILS_ALL, Accommodation.DIR_ACCOMMODATION_4);

                                    }
                                }
                                else{
                                    ref4 = GeneralFunction.getFirestoreInstance()
                                            .collection(Accommodation.URL_ACCOMMODATION)
                                            .document(getPushId)
                                            .collection(Accommodation.DIR_ACCOMMODATION)
                                            .document();
                                    setPushId4 = ref4.getId();
                                    accommodation = new Accommodation(getAccommodationName4,filePath4,setPushId4);
                                    if(!isFromDatabase){
                                        GeneralFunction.saveAccommodationSharedPreferences(parentActivity, accommodation, Accommodation.UPLOAD_ACCOMMODATION_DETAILS_ALL, Accommodation.DIR_ACCOMMODATION_4);

                                    }
                                }
                                if(getPushId5 != null && !getPushId5.trim().equals("")){
                                    ref5 = GeneralFunction.getFirestoreInstance()
                                            .collection(Accommodation.URL_ACCOMMODATION)
                                            .document(getPushId)
                                            .collection(Accommodation.DIR_ACCOMMODATION)
                                            .document(getPushId5);
                                    accommodation = new Accommodation(getAccommodationName5,filePath5,getPushId5);
                                    if(!isFromDatabase){
                                        GeneralFunction.saveAccommodationSharedPreferences(parentActivity, accommodation, Accommodation.UPLOAD_ACCOMMODATION_DETAILS_ALL, Accommodation.DIR_ACCOMMODATION_5);

                                    }
                                }
                                else{
                                    ref5 = GeneralFunction.getFirestoreInstance()
                                            .collection(Accommodation.URL_ACCOMMODATION)
                                            .document(getPushId)
                                            .collection(Accommodation.DIR_ACCOMMODATION)
                                            .document();
                                    setPushId5 = ref5.getId();
                                    accommodation = new Accommodation(getAccommodationName5,filePath5,setPushId5);
                                    if(!isFromDatabase){
                                        GeneralFunction.saveAccommodationSharedPreferences(parentActivity, accommodation, Accommodation.UPLOAD_ACCOMMODATION_DETAILS_ALL, Accommodation.DIR_ACCOMMODATION_5);                                }

                                }

                            }
                        }
//                if(getAccommodationName1 != null && !TextUtils.isEmpty(getAccommodationName1) && !getAccommodationName1.equals(noAvailable)&& filePath1 != null){
//                    HashMap<String,Object> accommodationPicDetails = new HashMap<>();
//                    accommodationPicDetails.put(Accommodation.ACCOMMODATION_NAME,getAccommodationName1);
//                    uploadRoom1Pic(accommodationPicDetails, ref1);
//                    count++;
//                }
//                if(getAccommodationName2 != null && !TextUtils.isEmpty(getAccommodationName2) && !getAccommodationName2.equals(noAvailable)&& filePath2!= null){
//                    HashMap<String,Object> accommodationPicDetails = new HashMap<>();
//                    accommodationPicDetails.put(Accommodation.ACCOMMODATION_NAME,getAccommodationName2);
//                    uploadRoom2Pic(accommodationPicDetails, ref2);
//                    count++;
//                }
//                if(getAccommodationName3 != null && !TextUtils.isEmpty(getAccommodationName3) && !getAccommodationName3.equals(noAvailable)&& filePath3 != null){
//                    HashMap<String,Object> accommodationPicDetails = new HashMap<>();
//                    accommodationPicDetails.put(Accommodation.ACCOMMODATION_NAME,getAccommodationName3);
//                    uploadRoom3Pic(accommodationPicDetails, ref3);
//                    count++;
//                }
//                if(getAccommodationName4 != null && !TextUtils.isEmpty(getAccommodationName4) && !getAccommodationName4.equals(noAvailable)&& filePath4 != null){
//                    HashMap<String,Object> accommodationPicDetails = new HashMap<>();
//                    accommodationPicDetails.put(Accommodation.ACCOMMODATION_NAME,getAccommodationName4);
//                    uploadRoom4Pic(accommodationPicDetails, ref4);
//                    count++;
//                }
//                if(getAccommodationName5 != null && !TextUtils.isEmpty(getAccommodationName5) && !getAccommodationName5.equals(noAvailable)&& filePath5 != null){
//                    HashMap<String,Object> accommodationPicDetails = new HashMap<>();
//                    accommodationPicDetails.put(Accommodation.ACCOMMODATION_NAME,getAccommodationName5);
//                    uploadRoom5Pic(accommodationPicDetails, ref5);
//                    count++;
//                }

                        //test here
                        if(getAccommodationName1 != null && !TextUtils.isEmpty(getAccommodationName1) && filePath1 != null && !filePath1.trim().equals("") && upload_house_1.getDrawable() != null) {
                            count++;
                            HashMap<String, Object> accommodationPicDetails1 = new HashMap<>();
                            accommodationPicDetails1.put(Accommodation.ACCOMMODATION_NAME, getAccommodationName1);

                            if(getPushId1 != null){
                                uploadRoom1Pic(accommodationPicDetails1, ref1, getPushId1);
                            }
                            else{
                                uploadRoom1Pic(accommodationPicDetails1, ref1, setPushId1);
                            }


                        }
                        if(getAccommodationName2 != null && !TextUtils.isEmpty(getAccommodationName2) && filePath2!= null && !filePath2.trim().equals("") && upload_house_2.getDrawable() != null) {
                            count++;
                            HashMap<String, Object> accommodationPicDetails2 = new HashMap<>();
                            accommodationPicDetails2.put(Accommodation.ACCOMMODATION_NAME, getAccommodationName2);
                            if(getPushId2 != null){
                                uploadRoom2Pic(accommodationPicDetails2, ref2, getPushId2);
                            }
                            else{
                                uploadRoom2Pic(accommodationPicDetails2, ref2, setPushId2);
                            }

                        }

                        if(getAccommodationName3 != null && !TextUtils.isEmpty(getAccommodationName3) && filePath3 != null && !filePath3.trim().equals("") && upload_house_3.getDrawable() != null) {
                            count++;
                            HashMap<String, Object> accommodationPicDetails3 = new HashMap<>();
                            accommodationPicDetails3.put(Accommodation.ACCOMMODATION_NAME, getAccommodationName3);
                            if(getPushId3 != null){
                                uploadRoom3Pic(accommodationPicDetails3, ref3, getPushId3);
                            }
                            else{
                                uploadRoom3Pic(accommodationPicDetails3, ref3, setPushId3);
                            }

                        }
                        if(getAccommodationName4 != null && !TextUtils.isEmpty(getAccommodationName4) && filePath4 != null && !filePath4.trim().equals("") && upload_house_4.getDrawable() != null) {
                            count++;
                            HashMap<String, Object> accommodationPicDetails4 = new HashMap<>();
                            accommodationPicDetails4.put(Accommodation.ACCOMMODATION_NAME, getAccommodationName4);
                            if(getPushId4 != null){
                                uploadRoom4Pic(accommodationPicDetails4, ref4, getPushId4);
                            }
                            else{
                                uploadRoom4Pic(accommodationPicDetails4, ref4, setPushId4);
                            }
                        }

                        if(getAccommodationName5 != null && !TextUtils.isEmpty(getAccommodationName5) && filePath5 != null && !filePath5.trim().equals("") && upload_house_5.getDrawable() != null){
                            count++;
                            HashMap<String,Object> accommodationPicDetails5 = new HashMap<>();
                            accommodationPicDetails5.put(Accommodation.ACCOMMODATION_NAME,getAccommodationName5);
                            if(getPushId4 != null){
                                uploadRoom5Pic(accommodationPicDetails5, ref5, getPushId5);
                            }
                            else{
                                uploadRoom5Pic(accommodationPicDetails5, ref5, setPushId5);
                            }
                        }
                    }
                }




//            if(filePath1!=null && filePath2!=null &&filePath3!=null && filePath4!=null && filePath5!=null){
//                HashMap<String,Object> accommodationDetails = new HashMap<>();
//                accommodationDetails.put(Accommodation.ACCOMMODATION_PIC,filePath1);
//                accommodationDetails.put(Accommodation.ACCOMMODATION_PIC_2,filePath2);
//                accommodationDetails.put(Accommodation.ACCOMMODATION_PIC_3,filePath3);
//                accommodationDetails.put(Accommodation.ACCOMMODATION_PIC_4,filePath4);
//                accommodationDetails.put(Accommodation.ACCOMMODATION_PIC_5,filePath5);
//                accommodationDetails.put(Accommodation.ACCOMMODATION_NAME,getAccommodationName1);
//                accommodationDetails.put(Accommodation.ACCOMMODATION_NAME_2,getAccommodationName2);
//                accommodationDetails.put(Accommodation.ACCOMMODATION_NAME_3,getAccommodationName3);
//                accommodationDetails.put(Accommodation.ACCOMMODATION_NAME_4,getAccommodationName4);
//                accommodationDetails.put(Accommodation.ACCOMMODATION_NAME_5,getAccommodationName5);
//
//                if(getPushId != null){
//
//                    accommodationRef = GeneralFunction.getFirestoreInstance()
//                            .collection(Host.URL_HOST)
//                            .document(getPushId)
//                            .collection(Accommodation.DIR_ACCOMMODATION)
//                            .document();
//                    accommodationRef.getId();
//
//                    setAccommodationDetails(accommodationRef,accommodationDetails);
//                }
//
//                if(getAccommodationName1 != null && !TextUtils.isEmpty(getAccommodationName1) && !getAccommodationName1.equals(noAvailable)&& filePath1 != null){
//                    HashMap<String,Object> roomPicDetails = new HashMap<>();
//                    roomPicDetails.put(Room.ROOM_NAME,getAccommodationName1);
//                    uploadRoom1Pic(roomPicDetails, ref1);
//                    count++;
//                }
//
//            }
            }
        }
    }

    private boolean isValidInput(String accommodationName1, String accommodationName2, String accommodationName3,String accommodationName4, String accommodationName5){
        return  GeneralFunction.checkAndReturnString(accommodationName1,parentActivity.getString(R.string.error_please_enter_accommodation_name),parentActivity)
                ||
                GeneralFunction.checkAndReturnString(accommodationName2,parentActivity.getString(R.string.error_please_enter_accommodation_name),parentActivity)
                ||
                GeneralFunction.checkAndReturnString(accommodationName3,parentActivity.getString(R.string.error_please_enter_accommodation_name),parentActivity)
                ||
                GeneralFunction.checkAndReturnString(accommodationName4,parentActivity.getString(R.string.error_please_enter_accommodation_name),parentActivity)
                ||
                GeneralFunction.checkAndReturnString(accommodationName5,parentActivity.getString(R.string.error_please_enter_accommodation_name),parentActivity);

    }

    private void setAccommodationDetails(final DocumentReference accommodationRef, HashMap<String,Object> roomMap, Boolean hasImage){
        goToSelectAvailableAmenityFragment++;

        if(isFromDatabase && !isEmpty && hasImage != null && hasImage){
            accommodationRef.update(roomMap).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    if(count >= 1){
                        GeneralFunction.handleUploadSuccessTommy(parentActivity,UploadAccommodationPhotoFragement.this,R.string.upload_accommodation_photo_successful,TAG);
                        if(goToSelectAvailableAmenityFragment <= 1){
                            goToSelectAvailableAmenityFragment();
                            value = 0;
                        }
                        else{
                            goToSelectAvailableAmenityFragment = 0;
                        }
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    GeneralFunction.handleUploadError(parentActivity,UploadAccommodationPhotoFragement.this,R.string.upload_accommodation_photo_err_upload,TAG);
                    Log.e(TAG,e.toString());
                }
            });
        }
        else {
            accommodationRef.set(roomMap).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    if(count >= 1){
                        GeneralFunction.handleUploadSuccessTommy(parentActivity,UploadAccommodationPhotoFragement.this,R.string.upload_accommodation_photo_successful,TAG);
                        if(goToSelectAvailableAmenityFragment <= 1){
                            goToSelectAvailableAmenityFragment();
                            value = 0;
                        }
                        else{
                            goToSelectAvailableAmenityFragment = 0;
                        }
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    GeneralFunction.handleUploadError(parentActivity,UploadAccommodationPhotoFragement.this,R.string.upload_accommodation_photo_err_upload,TAG);
                    Log.e(TAG,e.toString());
                }
            });
        }
    }

    private void uploadRoom1Pic(final HashMap<String,Object> accommodationMap, final DocumentReference accommodationRef, final String pushId){
        accommodationMap.put(Accommodation.ACCOMMODATION_POSITION, 1);
        if(filePath1 != null && !isFromDatabase){

            final StorageReference ref = GeneralFunction.getFirebaseStorageInstance().child(Accommodation.URL_ACCOMMODATION+"/"+parentActivity.uid+"/"+Accommodation.URL_UPLOADED_ACCOMMODATION+"/"+pushId+"/"+getAccommodationName1);

            UploadTask uploadTask = ref.putBytes(GeneralFunction.getCompressedBitmap(parentActivity,filePath1));
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                    GeneralFunction.handleUploadError(parentActivity,UploadAccommodationPhotoFragement.this,R.string.employer_err_job_posting_job,TAG);
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri downloadUri) {
                            if(downloadUri != null){
                                accommodationMap.put(Accommodation.URL_ACCOMMODATION_PIC, downloadUri.toString());
//                                GeneralFunction.saveAccommodationHashMapSharedPreferences(parentActivity, accommodationMap,Accommodation.UPLOAD_ACCOMMODATION_DETAILS, Accommodation.ACCOMMODATION_DETAILS_1);
                                setAccommodationDetails(accommodationRef,accommodationMap, hasImage1);
                            }
                        }
                    });
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    // + 10 is because i set the progress to 10 when it finished saving the initial firebase data
                    // * 80 is because i want the progress to go to maximum 90% only when the image is finished uploaded
                    // so that the final 10% is set when the firebase updates the imageUrl data as the final step
                    int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * 80) + 10;
                    parentActivity.updateProgress((float) totalProgress,UploadAccommodationPhotoFragement.this);
                }
            });//end addOnprogresslistener
        }
        else{
            if(filePath1 != null && !URLUtil.isHttpsUrl(filePath1)){

                final StorageReference ref = GeneralFunction.getFirebaseStorageInstance().child(Accommodation.URL_ACCOMMODATION+"/"+parentActivity.uid+"/"+Accommodation.URL_UPLOADED_ACCOMMODATION+"/"+pushId+"/"+getAccommodationName1);

                UploadTask uploadTask = ref.putBytes(GeneralFunction.getCompressedBitmap(parentActivity,filePath1));
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        GeneralFunction.handleUploadError(parentActivity,UploadAccommodationPhotoFragement.this,R.string.employer_err_job_posting_job,TAG);
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri downloadUri) {
                                if(downloadUri != null){
                                    accommodationMap.put(Accommodation.URL_ACCOMMODATION_PIC, downloadUri.toString());
//                                GeneralFunction.saveAccommodationHashMapSharedPreferences(parentActivity, accommodationMap,Accommodation.UPLOAD_ACCOMMODATION_DETAILS, Accommodation.ACCOMMODATION_DETAILS_1);
                                    setAccommodationDetails(accommodationRef,accommodationMap, hasImage1);
                                }
                            }
                        });
                    }
                }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        // + 10 is because i set the progress to 10 when it finished saving the initial firebase data
                        // * 80 is because i want the progress to go to maximum 90% only when the image is finished uploaded
                        // so that the final 10% is set when the firebase updates the imageUrl data as the final step
                        int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * 80) + 10;
                        parentActivity.updateProgress((float) totalProgress,UploadAccommodationPhotoFragement.this);
                    }
                });//end addOnprogresslistener
            }
            else{
                if(filePath1 != null){
                    accommodationMap.put(Accommodation.URL_ACCOMMODATION_PIC, filePath1);
                }
                setAccommodationDetails(accommodationRef,accommodationMap, null);
            }
        }
    }

    private void uploadRoom2Pic(final HashMap<String,Object> accommodationMap, final DocumentReference accommodationRef,final String pushId){
        accommodationMap.put(Accommodation.ACCOMMODATION_POSITION, 2);
        if(filePath2 != null && !isFromDatabase){

            final StorageReference ref = GeneralFunction.getFirebaseStorageInstance().child(Accommodation.URL_ACCOMMODATION+"/"+parentActivity.uid+"/"+Accommodation.URL_UPLOADED_ACCOMMODATION+"/"+pushId+"/"+getAccommodationName2);

            UploadTask uploadTask = ref.putBytes(GeneralFunction.getCompressedBitmap(parentActivity,filePath2));
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                    GeneralFunction.handleUploadError(parentActivity,UploadAccommodationPhotoFragement.this,R.string.employer_err_job_posting_job,TAG);
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri downloadUri) {
                            if(downloadUri != null){
                                accommodationMap.put(Accommodation.URL_ACCOMMODATION_PIC, downloadUri.toString());
//                                GeneralFunction.saveAccommodationHashMapSharedPreferences(parentActivity, accommodationMap,Accommodation.UPLOAD_ACCOMMODATION_DETAILS, Accommodation.ACCOMMODATION_DETAILS_2);
                                setAccommodationDetails(accommodationRef,accommodationMap, hasImage2);
                            }
                        }
                    });
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    // + 10 is because i set the progress to 10 when it finished saving the initial firebase data
                    // * 80 is because i want the progress to go to maximum 90% only when the image is finished uploaded
                    // so that the final 10% is set when the firebase updates the imageUrl data as the final step
                    int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * 80) + 10;
                    parentActivity.updateProgress((float) totalProgress,UploadAccommodationPhotoFragement.this);
                }
            });//end addOnprogresslistener
        }
        else{
            if(filePath2 != null && !URLUtil.isHttpsUrl(filePath2)){

                final StorageReference ref = GeneralFunction.getFirebaseStorageInstance().child(Accommodation.URL_ACCOMMODATION+"/"+parentActivity.uid+"/"+Accommodation.URL_UPLOADED_ACCOMMODATION+"/"+pushId+"/"+getAccommodationName2);

                UploadTask uploadTask = ref.putBytes(GeneralFunction.getCompressedBitmap(parentActivity,filePath2));
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        GeneralFunction.handleUploadError(parentActivity,UploadAccommodationPhotoFragement.this,R.string.employer_err_job_posting_job,TAG);
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri downloadUri) {
                                if(downloadUri != null){
                                    accommodationMap.put(Accommodation.URL_ACCOMMODATION_PIC, downloadUri.toString());
//                                GeneralFunction.saveAccommodationHashMapSharedPreferences(parentActivity, accommodationMap,Accommodation.UPLOAD_ACCOMMODATION_DETAILS, Accommodation.ACCOMMODATION_DETAILS_2);
                                    setAccommodationDetails(accommodationRef,accommodationMap, hasImage2);
                                }
                            }
                        });
                    }
                }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        // + 10 is because i set the progress to 10 when it finished saving the initial firebase data
                        // * 80 is because i want the progress to go to maximum 90% only when the image is finished uploaded
                        // so that the final 10% is set when the firebase updates the imageUrl data as the final step
                        int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * 80) + 10;
                        parentActivity.updateProgress((float) totalProgress,UploadAccommodationPhotoFragement.this);
                    }
                });//end addOnprogresslistener
            }
            else{
                if(filePath2 != null){
                    accommodationMap.put(Accommodation.URL_ACCOMMODATION_PIC, filePath2);
                }
                setAccommodationDetails(accommodationRef,accommodationMap, null);
            }
        }
    }

    private void uploadRoom3Pic(final HashMap<String,Object> accommodationMap, final DocumentReference accommodationRef, final String pushId){
        accommodationMap.put(Accommodation.ACCOMMODATION_POSITION, 3);
        if(filePath3 != null && !isFromDatabase){

            final StorageReference ref = GeneralFunction.getFirebaseStorageInstance().child(Accommodation.URL_ACCOMMODATION+"/"+parentActivity.uid+"/"+Accommodation.URL_UPLOADED_ACCOMMODATION+"/"+pushId+"/"+getAccommodationName3);

            UploadTask uploadTask = ref.putBytes(GeneralFunction.getCompressedBitmap(parentActivity,filePath3));
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                    GeneralFunction.handleUploadError(parentActivity,UploadAccommodationPhotoFragement.this,R.string.employer_err_job_posting_job,TAG);
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri downloadUri) {
                            if(downloadUri != null){
                                accommodationMap.put(Accommodation.URL_ACCOMMODATION_PIC, downloadUri.toString());
                                setAccommodationDetails(accommodationRef,accommodationMap, hasImage3);
//                                GeneralFunction.saveAccommodationHashMapSharedPreferences(parentActivity, accommodationMap,Accommodation.UPLOAD_ACCOMMODATION_DETAILS, Accommodation.ACCOMMODATION_DETAILS_3);                                setAccommodationDetails(accommodationRef,accommodationMap);
                            }
                        }
                    });
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    // + 10 is because i set the progress to 10 when it finished saving the initial firebase data
                    // * 80 is because i want the progress to go to maximum 90% only when the image is finished uploaded
                    // so that the final 10% is set when the firebase updates the imageUrl data as the final step
                    int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * 80) + 10;
                    parentActivity.updateProgress((float) totalProgress,UploadAccommodationPhotoFragement.this);
                }
            });//end addOnprogresslistener
        }else{
            if(filePath3 != null && !URLUtil.isHttpsUrl(filePath3)){

                final StorageReference ref = GeneralFunction.getFirebaseStorageInstance().child(Accommodation.URL_ACCOMMODATION+"/"+parentActivity.uid+"/"+Accommodation.URL_UPLOADED_ACCOMMODATION+"/"+pushId+"/"+getAccommodationName3);

                UploadTask uploadTask = ref.putBytes(GeneralFunction.getCompressedBitmap(parentActivity,filePath3));
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        GeneralFunction.handleUploadError(parentActivity,UploadAccommodationPhotoFragement.this,R.string.employer_err_job_posting_job,TAG);
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri downloadUri) {
                                if(downloadUri != null){
                                    accommodationMap.put(Accommodation.URL_ACCOMMODATION_PIC, downloadUri.toString());
                                    setAccommodationDetails(accommodationRef,accommodationMap, hasImage3);
//                                GeneralFunction.saveAccommodationHashMapSharedPreferences(parentActivity, accommodationMap,Accommodation.UPLOAD_ACCOMMODATION_DETAILS, Accommodation.ACCOMMODATION_DETAILS_3);                                setAccommodationDetails(accommodationRef,accommodationMap);
                                }
                            }
                        });
                    }
                }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        // + 10 is because i set the progress to 10 when it finished saving the initial firebase data
                        // * 80 is because i want the progress to go to maximum 90% only when the image is finished uploaded
                        // so that the final 10% is set when the firebase updates the imageUrl data as the final step
                        int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * 80) + 10;
                        parentActivity.updateProgress((float) totalProgress,UploadAccommodationPhotoFragement.this);
                    }
                });//end addOnprogresslistener
            }
            else{
                if(filePath3 != null){
                    accommodationMap.put(Accommodation.URL_ACCOMMODATION_PIC, filePath3);
                }
                setAccommodationDetails(accommodationRef,accommodationMap, null);
            }
        }
    }

    private void uploadRoom4Pic(final HashMap<String,Object> accommodationMap, final DocumentReference accommodationRef, final String pushId){
        accommodationMap.put(Accommodation.ACCOMMODATION_POSITION, 4);
        if(filePath4 != null && !isFromDatabase){

            final StorageReference ref = GeneralFunction.getFirebaseStorageInstance().child(Accommodation.URL_ACCOMMODATION+"/"+parentActivity.uid+"/"+Accommodation.URL_UPLOADED_ACCOMMODATION+"/"+pushId+"/"+getAccommodationName4);

            UploadTask uploadTask = ref.putBytes(GeneralFunction.getCompressedBitmap(parentActivity,filePath4));
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                    GeneralFunction.handleUploadError(parentActivity,UploadAccommodationPhotoFragement.this,R.string.employer_err_job_posting_job,TAG);
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri downloadUri) {
                            if(downloadUri != null){
                                accommodationMap.put(Accommodation.URL_ACCOMMODATION_PIC, downloadUri.toString());
                                setAccommodationDetails(accommodationRef,accommodationMap,hasImage4);
//                                GeneralFunction.saveAccommodationHashMapSharedPreferences(parentActivity, accommodationMap,Accommodation.UPLOAD_ACCOMMODATION_DETAILS, Accommodation.ACCOMMODATION_DETAILS_4);                                setAccommodationDetails(accommodationRef,accommodationMap);
                            }
                        }
                    });
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    // + 10 is because i set the progress to 10 when it finished saving the initial firebase data
                    // * 80 is because i want the progress to go to maximum 90% only when the image is finished uploaded
                    // so that the final 10% is set when the firebase updates the imageUrl data as the final step
                    int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * 80) + 10;
                    parentActivity.updateProgress((float) totalProgress,UploadAccommodationPhotoFragement.this);
                }
            });//end addOnprogresslistener
        }
        else{
            if(filePath4 != null && !URLUtil.isHttpsUrl(filePath4)){

                final StorageReference ref = GeneralFunction.getFirebaseStorageInstance().child(Accommodation.URL_ACCOMMODATION+"/"+parentActivity.uid+"/"+Accommodation.URL_UPLOADED_ACCOMMODATION+"/"+pushId+"/"+getAccommodationName4);

                UploadTask uploadTask = ref.putBytes(GeneralFunction.getCompressedBitmap(parentActivity,filePath4));
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        GeneralFunction.handleUploadError(parentActivity,UploadAccommodationPhotoFragement.this,R.string.employer_err_job_posting_job,TAG);
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri downloadUri) {
                                if(downloadUri != null){
                                    accommodationMap.put(Accommodation.URL_ACCOMMODATION_PIC, downloadUri.toString());
                                    setAccommodationDetails(accommodationRef,accommodationMap,hasImage4);
//                                GeneralFunction.saveAccommodationHashMapSharedPreferences(parentActivity, accommodationMap,Accommodation.UPLOAD_ACCOMMODATION_DETAILS, Accommodation.ACCOMMODATION_DETAILS_4);                                setAccommodationDetails(accommodationRef,accommodationMap);
                                }
                            }
                        });
                    }
                }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        // + 10 is because i set the progress to 10 when it finished saving the initial firebase data
                        // * 80 is because i want the progress to go to maximum 90% only when the image is finished uploaded
                        // so that the final 10% is set when the firebase updates the imageUrl data as the final step
                        int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * 80) + 10;
                        parentActivity.updateProgress((float) totalProgress,UploadAccommodationPhotoFragement.this);
                    }
                });//end addOnprogresslistener
            }
            else{
                if(filePath4 != null){
                    accommodationMap.put(Accommodation.URL_ACCOMMODATION_PIC, filePath4);
                }
                setAccommodationDetails(accommodationRef,accommodationMap, null);
            }
        }
    }

    private void uploadRoom5Pic(final HashMap<String,Object> accommodationMap, final DocumentReference accommodationRef, final String pushId){
        accommodationMap.put(Accommodation.ACCOMMODATION_POSITION, 5);
        if(filePath5 != null && !isFromDatabase){

            final StorageReference ref = GeneralFunction.getFirebaseStorageInstance().child(Accommodation.URL_ACCOMMODATION+"/"+parentActivity.uid+"/"+Accommodation.URL_UPLOADED_ACCOMMODATION+"/"+pushId+"/"+getAccommodationName5);

            UploadTask uploadTask = ref.putBytes(GeneralFunction.getCompressedBitmap(parentActivity,filePath5));
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                    GeneralFunction.handleUploadError(parentActivity,UploadAccommodationPhotoFragement.this,R.string.employer_err_job_posting_job,TAG);
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri downloadUri) {
                            if(downloadUri != null){
                                accommodationMap.put(Accommodation.URL_ACCOMMODATION_PIC, downloadUri.toString());
                                setAccommodationDetails(accommodationRef,accommodationMap,hasImage5);
//                                GeneralFunction.saveAccommodationHashMapSharedPreferences(parentActivity, accommodationMap,Accommodation.UPLOAD_ACCOMMODATION_DETAILS, Accommodation.ACCOMMODATION_DETAILS_5);                                setAccommodationDetails(accommodationRef,accommodationMap);
                            }
                        }
                    });
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    // + 10 is because i set the progress to 10 when it finished saving the initial firebase data
                    // * 80 is because i want the progress to go to maximum 90% only when the image is finished uploaded
                    // so that the final 10% is set when the firebase updates the imageUrl data as the final step
                    int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * 80) + 10;
                    parentActivity.updateProgress((float) totalProgress,UploadAccommodationPhotoFragement.this);
                }
            });//end addOnprogresslistener
        }
        else{
            if(filePath5 != null && !URLUtil.isHttpsUrl(filePath5)){

                final StorageReference ref = GeneralFunction.getFirebaseStorageInstance().child(Accommodation.URL_ACCOMMODATION+"/"+parentActivity.uid+"/"+Accommodation.URL_UPLOADED_ACCOMMODATION+"/"+pushId+"/"+getAccommodationName5);

                UploadTask uploadTask = ref.putBytes(GeneralFunction.getCompressedBitmap(parentActivity,filePath5));
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        GeneralFunction.handleUploadError(parentActivity,UploadAccommodationPhotoFragement.this,R.string.employer_err_job_posting_job,TAG);
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri downloadUri) {
                                if(downloadUri != null){
                                    accommodationMap.put(Accommodation.URL_ACCOMMODATION_PIC, downloadUri.toString());
                                    setAccommodationDetails(accommodationRef,accommodationMap, hasImage5);
//                                GeneralFunction.saveAccommodationHashMapSharedPreferences(parentActivity, accommodationMap,Accommodation.UPLOAD_ACCOMMODATION_DETAILS, Accommodation.ACCOMMODATION_DETAILS_5);                                setAccommodationDetails(accommodationRef,accommodationMap);
                                }
                            }
                        });
                    }
                }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        // + 10 is because i set the progress to 10 when it finished saving the initial firebase data
                        // * 80 is because i want the progress to go to maximum 90% only when the image is finished uploaded
                        // so that the final 10% is set when the firebase updates the imageUrl data as the final step
                        int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * 80) + 10;
                        parentActivity.updateProgress((float) totalProgress,UploadAccommodationPhotoFragement.this);
                    }
                });//end addOnprogresslistener
            }
            else{
                if(filePath5 != null){
                    accommodationMap.put(Accommodation.URL_ACCOMMODATION_PIC, filePath5);
                }
                setAccommodationDetails(accommodationRef,accommodationMap, null);
            }
        }
    }

    private void goToSelectAvailableAmenityFragment(){
        isSubmit = false;
        isGoCropPhoto = false;
        SelectAvailableAmenityFragment selectAvailableAmenityFragment = SelectAvailableAmenityFragment.newInstance(parentActivity,isFromDatabase,documentPushId,isEditProfile);
        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, selectAvailableAmenityFragment).addToBackStack(SelectAvailableAmenityFragment.TAG).commit();
        showMessageNotificationDialog();
    }

    private void showMessageNotificationDialog (){

        if(count == 1){
            GeneralFunction.openMessageNotificationDialog(parentActivity,getString(R.string.host_successful_upload_room), R.drawable.notice_good,TAG);
            count = 0;
            value = 1;
        }
        if(count == 2){
            GeneralFunction.openMessageNotificationDialog(parentActivity,getString(R.string.host_successful_upload_room), R.drawable.notice_good,TAG);
            count = 0;
            value = 1;
        }
        if(count == 3){
            GeneralFunction.openMessageNotificationDialog(parentActivity,getString(R.string.host_successful_upload_room), R.drawable.notice_good,TAG);
            count = 0;
            value = 1;
        }
        if(count == 4){
            GeneralFunction.openMessageNotificationDialog(parentActivity,getString(R.string.host_successful_upload_room), R.drawable.notice_good,TAG);
            count = 0;
            value = 1;
        }
        if(count == 5){
            GeneralFunction.openMessageNotificationDialog(parentActivity,getString(R.string.host_successful_upload_room), R.drawable.notice_good,TAG);
            count = 0;
            value = 1;
        }
    }

    private void setSharedPreferences(){
        try{
            SharedPreferences sharedPreferences = parentActivity.getSharedPreferences(Accommodation.UPLOAD_ACCOMMODATION_DETAILS_ALL, MODE_PRIVATE);

            // Get saved string data in it.
            String userInfoListJsonString = sharedPreferences.getString(Accommodation.DIR_ACCOMMODATION_1, "");

            // Create Gson object and translate the json string to related java object array.
            Gson gson = new Gson();

            Accommodation userInfoDtoArray = gson.fromJson(userInfoListJsonString, Accommodation.class);

            // Loop the UserInfoDTO array and print each UserInfoDTO data in android monitor as debug log.
            // Get each user info in dto.
            Accommodation userInfoDto = userInfoDtoArray;

            if(userInfoDto != null){

                if (descriptionCL1.getVisibility() == View.VISIBLE){
                    GeneralFunction.showPicFromFilePath(upload_house_1, userInfoDto.imgAccommodation);
                    descriptionET1.setText(userInfoDto.accommodationName);
                    filePath1 = userInfoDto.imgAccommodation;
                }
                else{
                    descriptionCL1.setVisibility(View.VISIBLE);
                    GeneralFunction.showPicFromFilePath(upload_house_1, userInfoDto.imgAccommodation);
                    descriptionET1.setText(userInfoDto.accommodationName);
                    filePath1 = userInfoDto.imgAccommodation;
                }
            }
        }
        catch(Exception e){
            Crashlytics.logException(e);
        }
        try{
            SharedPreferences sharedPreferences = parentActivity.getSharedPreferences(Accommodation.UPLOAD_ACCOMMODATION_DETAILS_ALL, MODE_PRIVATE);

            // Get saved string data in it.
            String userInfoListJsonString = sharedPreferences.getString(Accommodation.DIR_ACCOMMODATION_2, "");

            // Create Gson object and translate the json string to related java object array.
            Gson gson = new Gson();

            Accommodation userInfoDtoArray = gson.fromJson(userInfoListJsonString, Accommodation.class);

            // Loop the UserInfoDTO array and print each UserInfoDTO data in android monitor as debug log.
            // Get each user info in dto.
            Accommodation userInfoDto = userInfoDtoArray;

            if(userInfoDto != null){

                if (descriptionCL2.getVisibility() == View.VISIBLE){
                    GeneralFunction.showPicFromFilePath(upload_house_2, userInfoDto.imgAccommodation);
                    descriptionET2.setText(userInfoDto.accommodationName);
                    filePath2 = userInfoDto.imgAccommodation;
                }
                else{
                    descriptionCL2.setVisibility(View.VISIBLE);
                    GeneralFunction.showPicFromFilePath(upload_house_2, userInfoDto.imgAccommodation);
                    descriptionET2.setText(userInfoDto.accommodationName);
                    filePath2 = userInfoDto.imgAccommodation;
                }
            }
        }
        catch(Exception e){
            Crashlytics.logException(e);
        }
        try{
            SharedPreferences sharedPreferences = parentActivity.getSharedPreferences(Accommodation.UPLOAD_ACCOMMODATION_DETAILS_ALL, MODE_PRIVATE);

            // Get saved string data in it.
            String userInfoListJsonString = sharedPreferences.getString(Accommodation.DIR_ACCOMMODATION_3, "");

            // Create Gson object and translate the json string to related java object array.
            Gson gson = new Gson();

            Accommodation userInfoDtoArray = gson.fromJson(userInfoListJsonString, Accommodation.class);

            // Loop the UserInfoDTO array and print each UserInfoDTO data in android monitor as debug log.
            // Get each user info in dto.
            Accommodation userInfoDto = userInfoDtoArray;

            if(userInfoDto != null){

                if (descriptionCL3.getVisibility() == View.VISIBLE){
                    GeneralFunction.showPicFromFilePath(upload_house_3, userInfoDto.imgAccommodation);
                    descriptionET3.setText(userInfoDto.accommodationName);
                    filePath3 = userInfoDto.imgAccommodation;
                }
                else{
                    descriptionCL3.setVisibility(View.VISIBLE);
                    GeneralFunction.showPicFromFilePath(upload_house_3, userInfoDto.imgAccommodation);
                    descriptionET3.setText(userInfoDto.accommodationName);
                    filePath3 = userInfoDto.imgAccommodation;
                }
            }
        }
        catch(Exception e){
            Crashlytics.logException(e);
        }

        try{
            SharedPreferences sharedPreferences = parentActivity.getSharedPreferences(Accommodation.UPLOAD_ACCOMMODATION_DETAILS_ALL, MODE_PRIVATE);

            // Get saved string data in it.
            String userInfoListJsonString = sharedPreferences.getString(Accommodation.DIR_ACCOMMODATION_4, "");

            // Create Gson object and translate the json string to related java object array.
            Gson gson = new Gson();

            Accommodation userInfoDtoArray = gson.fromJson(userInfoListJsonString, Accommodation.class);

            // Loop the UserInfoDTO array and print each UserInfoDTO data in android monitor as debug log.
            // Get each user info in dto.
            Accommodation userInfoDto = userInfoDtoArray;

            if(userInfoDto != null){

                if (descriptionCL4.getVisibility() == View.VISIBLE){
                    GeneralFunction.showPicFromFilePath(upload_house_4, userInfoDto.imgAccommodation);
                    descriptionET4.setText(userInfoDto.accommodationName);
                    filePath4 = userInfoDto.imgAccommodation;
                }
                else{
                    descriptionCL4.setVisibility(View.VISIBLE);
                    GeneralFunction.showPicFromFilePath(upload_house_4, userInfoDto.imgAccommodation);
                    descriptionET4.setText(userInfoDto.accommodationName);
                    filePath4 = userInfoDto.imgAccommodation;
                }
            }
        }
        catch(Exception e){
            Crashlytics.logException(e);
        }

        try{
            SharedPreferences sharedPreferences = parentActivity.getSharedPreferences(Accommodation.UPLOAD_ACCOMMODATION_DETAILS_ALL, MODE_PRIVATE);

            // Get saved string data in it.
            String userInfoListJsonString = sharedPreferences.getString(Accommodation.DIR_ACCOMMODATION_5, "");

            // Create Gson object and translate the json string to related java object array.
            Gson gson = new Gson();

            Accommodation userInfoDtoArray = gson.fromJson(userInfoListJsonString, Accommodation.class);

            // Loop the UserInfoDTO array and print each UserInfoDTO data in android monitor as debug log.
            // Get each user info in dto.
            Accommodation userInfoDto = userInfoDtoArray;

            if(userInfoDto != null){

                if (descriptionCL5.getVisibility() == View.VISIBLE){
                    GeneralFunction.showPicFromFilePath(upload_house_5, userInfoDto.imgAccommodation);
                    descriptionET5.setText(userInfoDto.accommodationName);
                    filePath5 = userInfoDto.imgAccommodation;
                }
                else{
                    descriptionCL5.setVisibility(View.VISIBLE);
                    GeneralFunction.showPicFromFilePath(upload_house_5, userInfoDto.imgAccommodation);
                    descriptionET5.setText(userInfoDto.accommodationName);
                    filePath5 = userInfoDto.imgAccommodation;
                }
            }
        }
        catch(Exception e){
            Crashlytics.logException(e);
        }

    }

    private void checkCL(){
        if(!isEmpty){
            if(descriptionCL1.getVisibility() == View.VISIBLE){
                value = value+1;
            }
            if(descriptionCL2.getVisibility() == View.VISIBLE){
                value = value+1;
            }
            if(descriptionCL3.getVisibility() == View.VISIBLE){
                value = value+1;
            }
            if(descriptionCL4.getVisibility() == View.VISIBLE){
                value = value+1;
            }
            if(descriptionCL5.getVisibility() == View.VISIBLE){
                value = value+1;
            }
            if(value != 0){
                numCountTV.setText(String.valueOf(value));
            }
        }
    }

    private void setCL(int value){
        if(value != 0){
            if(value == 1){
                descriptionCL1.setVisibility(View.VISIBLE);
                numCountTV.setText(String.valueOf(value));
            }

            if(value == 2){
                descriptionCL1.setVisibility(View.VISIBLE);
                descriptionCL2.setVisibility(View.VISIBLE);
                numCountTV.setText(String.valueOf(value));
            }

            if(value == 3){
                descriptionCL1.setVisibility(View.VISIBLE);
                descriptionCL2.setVisibility(View.VISIBLE);
                descriptionCL3.setVisibility(View.VISIBLE);
                numCountTV.setText(String.valueOf(value));
            }

            if(value == 4){
                descriptionCL1.setVisibility(View.VISIBLE);
                descriptionCL2.setVisibility(View.VISIBLE);
                descriptionCL3.setVisibility(View.VISIBLE);
                descriptionCL4.setVisibility(View.VISIBLE);
                numCountTV.setText(String.valueOf(value));
            }

            if(value == 5){
                descriptionCL1.setVisibility(View.VISIBLE);
                descriptionCL2.setVisibility(View.VISIBLE);
                descriptionCL3.setVisibility(View.VISIBLE);
                descriptionCL4.setVisibility(View.VISIBLE);
                descriptionCL5.setVisibility(View.VISIBLE);
                numCountTV.setText(String.valueOf(value));
            }

        }
    }

    private String getPushId(String dir, String pushId){
        try{
            SharedPreferences sharedPreferences = parentActivity.getSharedPreferences(Accommodation.UPLOAD_ACCOMMODATION_DETAILS_ALL, MODE_PRIVATE);

            // Create Gson object and translate the json string to related java object array.
            Gson gson = new Gson();

            //to get the bundle of value in sharedpreference with GOOGLE_MAP_APY_KEY
            Type type = new TypeToken<Map<String, Object>>(){}.getType();
            Map<String, String> stringMap = gson.fromJson(sharedPreferences.getString(dir,""), type);

            if(stringMap != null){
                //set button here
                pushId = stringMap.get(Accommodation.IMG_PUSH_ID);
            }
            return pushId;
        }
        catch(Exception e){
            Crashlytics.logException(e);
        }
        return null;
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.selectHotel();
        parentActivity.setToolbarTitle(parentActivity.getString(R.string.host_an_accommodation));
        if(!isGoCropPhoto){
            if(isFromDatabase && !isEditProfile){
                isGoCropPhoto = false;
                GeneralFunction.clearGlideCacheMemory(parentActivity);
                retrieveFromFirestore();
                checkCL();
            }
            else{
                isGoCropPhoto = false;
            }
            if(isEditProfile){
                GeneralFunction.clearGlideCacheMemory(parentActivity);
                retrieveFromFirestore();

            }
        }
        else{
            if(isEditProfile){
                GeneralFunction.clearGlideCacheMemory(parentActivity);
                retrieveFromFirestore();

            }
        }
    }

    private void retrieveFromFirestore(){
        //get room url and document id
        CollectionReference roomRef = db.collection(Accommodation.URL_ACCOMMODATION).document(documentPushId).collection(Accommodation.DIR_ACCOMMODATION);
        roomRef.orderBy(Accommodation.ACCOMMODATION_POSITION, Query.Direction.ASCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.w(TAG, "Listen failed.", e);
                    return;
                }
                String source = queryDocumentSnapshots != null && queryDocumentSnapshots.getMetadata().hasPendingWrites()
                        ? "Local" : "Server";
                Log.d(TAG,source);

                if (queryDocumentSnapshots != null) {

                    if (!queryDocumentSnapshots.isEmpty()){

                        queryDocumentSnapshots.getMetadata().isFromCache();
                        queryDocumentSnapshots.getMetadata().hasPendingWrites();
                        accommodationURL.clear();
                        accommodationName.clear();
                        accommodationPushID.clear();
                        for(QueryDocumentSnapshot queryDocumentSnapshot : queryDocumentSnapshots) {
                            accommodationURL.add(queryDocumentSnapshot.toObject(Accommodation.class).urlAccommodationPic);
                            accommodationName.add(queryDocumentSnapshot.toObject(Accommodation.class).accommodationName);
                        }
                        for (DocumentSnapshot documentReference : queryDocumentSnapshots.getDocuments()) {
                            accommodationPushID.add(documentReference.getId());
                        }
                    }
                    else{
                        isEmpty = true;
                    }
                }

                if(!isEmpty){
                    accommodationURL.size();
                    setFromDatabase();
                }
                else{
                    //because no value in dir accommodation set the value back to it prevent it add additional
                    numCountTV.setText("1");
                    value = 1;
                }
                accommodationName.size();
                accommodationURL.size();
                accommodationPushID.size();
            }
        });
    }
    private void setFromDatabase(){
        //get accommodationUrl
        if(accommodationURL != null){
            imageButtons.add(upload_house_1);
            imageButtons.add(upload_house_2);
            imageButtons.add(upload_house_3);
            imageButtons.add(upload_house_4);
            imageButtons.add(upload_house_5);

            for(int i = 0; i<accommodationURL.size(); i++){
                if(accommodationURL.get(i) != null){
                    GlideImageSetting(parentActivity, Uri.parse(accommodationURL.get(i)),imageButtons.get(i));
                }
            }
        }
      //get accommodationName
        if(accommodationName != null){
            editTexts.add(descriptionET1);
            editTexts.add(descriptionET2);
            editTexts.add(descriptionET3);
            editTexts.add(descriptionET4);
            editTexts.add(descriptionET5);

            for(int i = 0; i<accommodationName.size(); i++){
                editTexts.get(i).setText(accommodationName.get(i));
            }
        }

            for(int i = 0; i<accommodationURL.size(); i++){
                value = i+1;
            }
            numCountTV.setText(String.valueOf(value));


        if(value != 0 && accommodationURL != null && accommodationPushID != null){
            if(value == 1){
                filePath1 = accommodationURL.get(0);
                getPushId1 = accommodationPushID.get(0);
                hasImage1 = true;
                setCL(value);
            }
            if(value == 2){
                filePath1 = accommodationURL.get(0);
                filePath2 = accommodationURL.get(1);
                getPushId1 = accommodationPushID.get(0);
                getPushId2 = accommodationPushID.get(1);
                hasImage1 = true;
                hasImage2 = true;
                setCL(value);
            }
            if(value == 3){
                filePath1 = accommodationURL.get(0);
                filePath2 = accommodationURL.get(1);
                filePath3 = accommodationURL.get(2);
                getPushId1 = accommodationPushID.get(0);
                getPushId2 = accommodationPushID.get(1);
                getPushId3 = accommodationPushID.get(2);
                hasImage1 = true;
                hasImage2 = true;
                hasImage3 = true;
                setCL(value);
            }
            if(value == 4){
                filePath1 = accommodationURL.get(0);
                filePath2 = accommodationURL.get(1);
                filePath3 = accommodationURL.get(2);
                filePath4 = accommodationURL.get(3);
                getPushId1 = accommodationPushID.get(0);
                getPushId2 = accommodationPushID.get(1);
                getPushId3 = accommodationPushID.get(2);
                getPushId4 = accommodationPushID.get(3);
                hasImage1 = true;
                hasImage2 = true;
                hasImage3 = true;
                hasImage4 = true;
                setCL(value);
            }
            if(value == 5){
                filePath1 = accommodationURL.get(0);
                filePath2 = accommodationURL.get(1);
                filePath3 = accommodationURL.get(2);
                filePath4 = accommodationURL.get(3);
                filePath5 = accommodationURL.get(4);
                getPushId1 = accommodationPushID.get(0);
                getPushId2 = accommodationPushID.get(1);
                getPushId3 = accommodationPushID.get(2);
                getPushId4 = accommodationPushID.get(3);
                getPushId5 = accommodationPushID.get(4);
                hasImage1 = true;
                hasImage2 = true;
                hasImage3 = true;
                hasImage4 = true;
                hasImage5 = true;
                setCL(value);
            }
        }
        if(isFromDatabase && documentPushId != null){
            getPushId = documentPushId;
        }
        if(isEditProfile && documentPushId != null){
            getPushId = documentPushId;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        imageButtons.clear();
        editTexts.clear();
        accommodationName.clear();
        accommodationURL.clear();
        accommodationPushID.clear();
        goToSelectAvailableAmenityFragment = 0;
    }

}
