package com.vidatechft.yupa.hostFragments;

import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.vidatechft.yupa.CalendaUtilities.MySelectorDecorator;
import com.vidatechft.yupa.CalendaUtilities.TodayDecorator;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.PublishAccommodationAdapter;
import com.vidatechft.yupa.classes.Accommodation;
import com.vidatechft.yupa.classes.BookHomestayDetails;
import com.vidatechft.yupa.classes.BookingHistory;
import com.vidatechft.yupa.classes.Host;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;
import com.vidatechft.yupa.utilities.MillisecondsToDate;

import org.threeten.bp.Instant;
import org.threeten.bp.LocalDate;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZoneOffset;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class PublishAccommodationStatusFragment extends Fragment implements View.OnClickListener{
    public static final String TAG = PublishAccommodationStatusFragment.class.getName();
    private MainActivity parentActivity;
    private MaterialCalendarView calendarView;
    private CalendarDay todayDate;
    private String mAccommodationID;
    private ImageView publish_accommodation_cover_photo;
    private RecyclerView currentGuestRV,upcomingGuestRV, previousGuestRV;
    private PublishAccommodationAdapter publishAccommodationAdapter;
    private TextView hostAccommodationNameTV;
    private TextView upcomingGuestViewAllTV,previousGuestViewAllTV;
    private LinearLayout currentGuestTopLL,upcomingGuestTopLL,previousGuestTopLL;
    private Button btn_stopPublish,btn_editDetail;

    private HashMap<CalendarDay,Drawable> calendarDayDrawableHashMap = new HashMap<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static PublishAccommodationStatusFragment newInstance(MainActivity parentActivity, String mAccommodationID) {
        PublishAccommodationStatusFragment fragment = new PublishAccommodationStatusFragment();
        fragment.parentActivity = parentActivity;
        fragment.mAccommodationID = mAccommodationID;
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_publish_accommodation_status, container, false);
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //Relative Layout
                currentGuestRV = view.findViewById(R.id.currentGuestRV);
                upcomingGuestRV = view.findViewById(R.id.upcomingGuestRV);
                previousGuestRV = view.findViewById(R.id.previousGuestRV);

                publish_accommodation_cover_photo = view.findViewById(R.id.publish_accommodation_cover_photo);
                hostAccommodationNameTV = view.findViewById(R.id.hostAccommodationNameTV);
                //View all Text View
                upcomingGuestViewAllTV = view.findViewById(R.id.upcomingGuestViewAllTV);
                previousGuestViewAllTV = view.findViewById(R.id.previousGuestViewAllTV);
                //Linear Layout
                currentGuestTopLL = view.findViewById(R.id.currentGuestTopLL);
                upcomingGuestTopLL = view.findViewById(R.id.upcomingGuestTopLL);
                previousGuestTopLL = view.findViewById(R.id.previousGuestTopLL);

                //Button
                btn_stopPublish = view.findViewById(R.id.btn_stopPublish);
                btn_editDetail = view.findViewById(R.id.btn_editDetail);

                //Listener
                btn_stopPublish.setOnClickListener(PublishAccommodationStatusFragment.this);
                btn_editDetail.setOnClickListener(PublishAccommodationStatusFragment.this);

            }
        });
        return view;
    }

    private void getCalendarEvent(View view){
        calendarView = view.findViewById(R.id.calendarView);
        todayDate = CalendarDay.today();
        Calendar instance = Calendar.getInstance();
        LocalDate localDate = LocalDate.now();
        calendarView.setSelectedDate(localDate);
        final Calendar instance2 = Calendar.getInstance();
        instance2.add(Calendar.DAY_OF_MONTH, 10);
        //setting of calendar view
        calendarView.state().edit()
//                .setMinimumDate(instance)//set minimum date
                .commit();
        calendarView.setSelectionMode(MaterialCalendarView.SELECTION_MODE_NONE);
        calendarView.addDecorators(new TodayDecorator());
        final List<BookingHistory> currentDetailList = new ArrayList<>();
        final List<BookingHistory> upcomingDetailList = new ArrayList<>();
        final List<BookingHistory> previousDetailList = new ArrayList<>();
        final HashSet<CalendarDay> checkInDates = new HashSet();
        final HashSet<CalendarDay> checkOutDates = new HashSet();
        final HashMap<CalendarDay,Drawable> customCalendarDayMap = new HashMap<>();

        GeneralFunction.getFirestoreInstance()
                .collection(BookingHistory.URL_BOOKING_HISTORY)
                .whereEqualTo(BookingHistory.HOST_ID,parentActivity.uid)
                .whereEqualTo(BookingHistory.TARGET_ID, mAccommodationID)
                .whereEqualTo(BookingHistory.ACTION_TYPE, BookingHistory.ACTION_TYPE_ACCEPTED)
                .orderBy(BookHomestayDetails.DATE_CREATED, Query.Direction.ASCENDING)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        currentDetailList.clear();
                        upcomingDetailList.clear();
                        previousDetailList.clear();
                        checkInDates.clear();
                        checkOutDates.clear();
                        customCalendarDayMap.clear();
                        if(task.isSuccessful() && task.getResult() != null && !task.getResult().isEmpty()){
                            for(QueryDocumentSnapshot queryDocumentSnapshot:task.getResult()){
                                BookingHistory bookHomestayDetails = BookingHistory.snapshotToBookingHistory(parentActivity,queryDocumentSnapshot);
                                if(bookHomestayDetails != null && bookHomestayDetails.checkinDate!= null && bookHomestayDetails.checkoutDate != null){

                                    //convert millisecond to string it useless here
                                    String date = MillisecondsToDate.convert(bookHomestayDetails.checkinDate);
                                    //define the Date
                                    final Date mCheckInDate = new Date(bookHomestayDetails.checkinDate);
                                    final Date mCheckOutDate = new Date(bookHomestayDetails.checkoutDate);
                                    long timeInMilliseconds = mCheckInDate.getTime();
                                    //Store the date into the HashSet()
                                    LocalDate dttCheckInDate = Instant.ofEpochMilli(mCheckInDate.getTime()).atZone(ZoneOffset.UTC).toLocalDate();
                                    LocalDate dttCheckOutDate = Instant.ofEpochMilli(mCheckOutDate.getTime()).atZone(ZoneOffset.UTC).toLocalDate();
                                    checkInDates.add(CalendarDay.from(dttCheckInDate));
                                    checkOutDates.add(CalendarDay.from(dttCheckOutDate));
                                    //get match the (dates)with today
                                    if(GeneralFunction.getDefaultUtcCalendar().getTimeInMillis() >= timeInMilliseconds && GeneralFunction.getDefaultUtcCalendar().getTimeInMillis() <= mCheckOutDate.getTime()){
//                                        GeneralFunction.Toast(parentActivity,"Today");
                                        currentDetailList.add(bookHomestayDetails);
                                        if(currentDetailList.size() != 0){
                                            if(currentGuestTopLL.getVisibility() == View.GONE){
                                                currentGuestTopLL.setVisibility(View.VISIBLE);
                                            }
                                        }
                                        parentActivity.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                publishAccommodationAdapter = new PublishAccommodationAdapter(parentActivity,PublishAccommodationStatusFragment.this,currentDetailList,BookHomestayDetails.CURRENT_GUEST);
                                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(parentActivity);
                                                currentGuestRV.setAdapter(publishAccommodationAdapter);
                                                publishAccommodationAdapter.notifyDataSetChanged();
                                                //when in scrollview prevent laggy
                                                currentGuestRV.setNestedScrollingEnabled(false);
                                                currentGuestRV.setHasFixedSize(true);
                                                currentGuestRV.setLayoutManager(linearLayoutManager);
                                                currentGuestRV.setVisibility(View.VISIBLE);
                                            }
                                        });
                                    }
                                    //get match the (dates)with upcoming
                                    if(timeInMilliseconds > GeneralFunction.getDefaultUtcCalendar().getTimeInMillis()){
                                        upcomingDetailList.add(bookHomestayDetails);
                                        if(upcomingDetailList.size() != 0 || upcomingDetailList.size()>1 || upcomingDetailList.size() <2){
                                            if(upcomingGuestTopLL.getVisibility() == View.GONE){
                                                upcomingGuestTopLL.setVisibility(View.VISIBLE);
                                            }
                                            List<BookingHistory> bookDetailsLimit1 = Collections.singletonList(upcomingDetailList.get(0));
                                            publishAccommodationAdapter = new PublishAccommodationAdapter(parentActivity,PublishAccommodationStatusFragment.this,bookDetailsLimit1,BookHomestayDetails.UPCOMING_GUEST);
                                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(parentActivity);
                                            upcomingGuestRV.setAdapter(publishAccommodationAdapter);
                                            publishAccommodationAdapter.notifyDataSetChanged();
                                            //when in scrollview prevent laggy
                                            upcomingGuestRV.setNestedScrollingEnabled(false);
                                            upcomingGuestRV.setHasFixedSize(true);
                                            upcomingGuestRV.setLayoutManager(linearLayoutManager);
                                            upcomingGuestRV.setVisibility(View.VISIBLE);
                                        }
                                        upcomingGuestViewAllTV.setOnClickListener(new DebouncedOnClickListener(500) {
                                            @Override
                                            public void onDebouncedClick(View v) {
                                                Fragment publishAccommodationStatusViewAllFragment = PublishAccommodationStatusViewAllFragment.newInstance(parentActivity, upcomingDetailList, BookHomestayDetails.UPCOMING_GUEST);
                                                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, publishAccommodationStatusViewAllFragment).addToBackStack(PublishAccommodationStatusViewAllFragment.TAG).commit();
                                            }
                                        });

                                    }

                                    //get match the (dates)with previous
                                    if(timeInMilliseconds < GeneralFunction.getDefaultUtcCalendar().getTimeInMillis()){
                                        previousDetailList.add(bookHomestayDetails);
                                        if(previousDetailList.size() != 0 || previousDetailList.size()>1 || previousDetailList.size() <2){
                                            if(previousGuestTopLL.getVisibility() == View.GONE){
                                                previousGuestTopLL.setVisibility(View.VISIBLE);
                                            }
                                            List<BookingHistory> bookDetailsLimit1 = Collections.singletonList(previousDetailList.get(0));
                                            publishAccommodationAdapter = new PublishAccommodationAdapter(parentActivity,PublishAccommodationStatusFragment.this,bookDetailsLimit1,BookHomestayDetails.PREVOIUS_GUEST);
                                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(parentActivity);
                                            previousGuestRV.setAdapter(publishAccommodationAdapter);
                                            publishAccommodationAdapter.notifyDataSetChanged();
                                            //when in scrollview prevent laggy
                                            previousGuestRV.setNestedScrollingEnabled(false);
                                            previousGuestRV.setHasFixedSize(true);
                                            previousGuestRV.setLayoutManager(linearLayoutManager);
                                            previousGuestRV.setVisibility(View.VISIBLE);
                                        }
                                        previousGuestViewAllTV.setOnClickListener(new DebouncedOnClickListener(500) {
                                            @Override
                                            public void onDebouncedClick(View v) {
                                                Fragment publishAccommodationStatusViewAllFragment = PublishAccommodationStatusViewAllFragment.newInstance(parentActivity, previousDetailList, BookHomestayDetails.PREVOIUS_GUEST);
                                                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, publishAccommodationStatusViewAllFragment).addToBackStack(PublishAccommodationStatusViewAllFragment.TAG).commit();
                                            }
                                        });
                                    }
//                                    long bobo =  mCheckInDate.getTime() - mCheckOutDate.getTime();
//                                    GeneralFunction.openMessageNotificationDialog(parentActivity,String.valueOf(bobo),R.drawable.notice_bad,TAG);
                                    Calendar minDate = GeneralFunction.toCalendar(mCheckInDate);
                                    Calendar maxDate = GeneralFunction.toCalendar(mCheckOutDate);
                                    CalendarDay maxDay = CalendarDay.from(dttCheckOutDate);
                                    CalendarDay minDay = CalendarDay.from(dttCheckInDate);
                                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyyy");
                                    String strMinDate = simpleDateFormat.format(mCheckInDate);
                                    String strMaxDate = simpleDateFormat.format(mCheckOutDate);

                                    List<Date> dates = new ArrayList<Date>();
                                    List<CalendarDay> dates2 = new ArrayList<CalendarDay>();


//                                    String start_date ="26/9/2018";
//                                    String end_date ="30/9/2018";

                                    DateFormat formatter ;

                                    formatter = new SimpleDateFormat("dd/M/yyyy");
//                                    Date  startDate = mCheckInDate;
//                                    Date  endDate = mCheckOutDate;

                                    long interval = 24*1000 * 60 * 60;
                                    long endTime = mCheckOutDate.getTime() ;
                                    long curTime = mCheckInDate.getTime();

                                    while (curTime <= endTime) {
                                        dates.add(new Date(curTime));
                                        dates2.add(CalendarDay.from(Instant.ofEpochMilli(curTime).atZone(ZoneOffset.UTC).toLocalDate()));
                                        curTime += interval;
                                    }

                                    dates.size();
                                    dates2.size();
//                                    for(int i=0;i<dates.size();i++){
//                                        dates.get(i);
//                                        String result = formatter.format(dates.get(i));
//                                        Toast.makeText(parentActivity,result,Toast.LENGTH_SHORT).show();
//                                    }

                                    //highlight the calendar date
                                    for(int i = 0; i< dates2.size(); i++){
                                        if(dates2.size() == 1){
                                            customCalendarDayMap.put(dates2.get(i),parentActivity.getResources().getDrawable(R.drawable.square_single_calendar));
                                        }
                                        else{
                                            if(dates2.get(i).equals(minDay)){
                                                customCalendarDayMap.put(dates2.get(i),parentActivity.getResources().getDrawable(R.drawable.left_half_circle));
                                            }
                                            if(dates2.get(i).equals(maxDay)){
                                                customCalendarDayMap.put(dates2.get(i),parentActivity.getResources().getDrawable(R.drawable.right_half_circle));
                                            }
                                            if(!dates2.get(i).equals(minDay) && !dates2.get(i).equals(maxDay)){
                                                customCalendarDayMap.put(dates2.get(i),parentActivity.getResources().getDrawable(R.drawable.square_calendar));
                                            }
                                        }
                                    }
                                    customCalendarDayMap.size();


                                }
                            }//end for loop

                            //set circle
                            final MySelectorDecorator mySelectorDecorator =new MySelectorDecorator(parentActivity,customCalendarDayMap);
                            calendarView.addDecorators(mySelectorDecorator);
                            //Calendar listener to check event and changes
                            calendarView.setOnDateChangedListener(new OnDateSelectedListener() {
                                @Override
                                public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
//                        widget.setCurrentDate(PublishAccommodationStatusFragment.this.mDate);
                                    //to enter the event date
                                    for(CalendarDay calendarDay : checkInDates){
                                        if(selected && date.equals(calendarDay)){
                                            GeneralFunction.Toast(parentActivity,calendarDay.toString());
                                        }
                                    }
                                }
                            });
//                            calendarView.setOnRangeSelectedListener(new OnRangeSelectedListener() {
//                                @Override
//                                public void onRangeSelected(@NonNull MaterialCalendarView widget, @NonNull List<CalendarDay> dates) {
//                                    for(CalendarDay calendarDay : dates){
//                                       widget.setDateSelected(calendarDay,true);
//                                    }
//                                }
//                            });
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if(e != null){
                    Log.e(TAG,e.toString());
                    GeneralFunction.Toast(parentActivity,e.toString());
                }
            }
        });
    }

    private void getGuestDetail(){

    }

    private void getAccommodationDetail(){
        GeneralFunction.getFirestoreInstance()
                .collection(Accommodation.URL_ACCOMMODATION)
                .document(mAccommodationID)
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if(task.isSuccessful() && task.getResult().exists()){
                            Room accommodation = Room.snapshotToRoom(parentActivity,task.getResult());
                            if(accommodation != null){
                                if(accommodation.status == null){
                                    btn_stopPublish.setText(R.string.btn_startPublish);
                                }
//                            else{
//                                GeneralFunction.openMessageNotificationDialog(parentActivity,getString(R.string.status_error),R.drawable.notice_bad,TAG);
//                            }

                                if(accommodation.urlOutlook != null){
                                    GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(accommodation.urlOutlook),publish_accommodation_cover_photo);
                                }
                                if(accommodation.accommodationName != null){
                                    hostAccommodationNameTV.setText(accommodation.accommodationName);
                                }
                                if(accommodation.isPublish != null){
                                    if(accommodation.isPublish){
                                        btn_stopPublish.setText(R.string.btn_stopPublish);
                                    }
                                    else{
                                        btn_stopPublish.setText(R.string.btn_startPublish);
                                    }
                                }
                                else{
                                    btn_stopPublish.setText(R.string.btn_startPublish);
                                }
                            }
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if(e != null){
                    GeneralFunction.Toast(parentActivity,e.toString());
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                View view = getView();
                if(view != null){
                    getCalendarEvent(view);
                    getAccommodationDetail();
                }

                parentActivity.selectHotel();
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.host_room_details));
            }
        });
    }

    private long GetTodayDateInMilliseconds(){
        Calendar cal = Calendar.getInstance();
        int year  = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int date  = cal.get(Calendar.DATE);
        cal.clear();
        cal.set(year, month, date);
        return cal.getTimeInMillis();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_stopPublish:
                final HashMap<String,Object> updateStatusMap = new HashMap<>();
                GeneralFunction.getFirestoreInstance()
                        .collection(Accommodation.URL_ACCOMMODATION)
                        .document(mAccommodationID)
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                if(task.isSuccessful()){
                                    updateStatusMap.clear();
                                    Room room = task.getResult().toObject(Room.class);
                                    if(room != null){
                                       if(room.status != null && TextUtils.equals(room.status,Room.STATUS_APPROVED)){
                                           if(room.isPublish != null && room.isPublish){

                                               android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(parentActivity);
                                               alertDialogBuilder.setTitle(parentActivity.getString(R.string.unpublish));
                                               alertDialogBuilder.setMessage(parentActivity.getString(R.string.message_unpublish_accommodation));
                                               alertDialogBuilder.setPositiveButton(parentActivity.getString(R.string.btn_yes), new DialogInterface.OnClickListener() {
                                                   @Override
                                                   public void onClick(DialogInterface dialog, int which) {
                                                       updateStatusMap.put(Room.IS_PUBLISH,false);
                                                       btn_stopPublish.setText(R.string.btn_startPublish);
                                                       setPublish(updateStatusMap);
                                                   }
                                               });

                                               alertDialogBuilder.setNegativeButton(parentActivity.getString(R.string.btn_no), new DialogInterface.OnClickListener() {
                                                   @Override
                                                   public void onClick(DialogInterface dialog, int which) {
                                                       dialog.cancel();
                                                   }
                                               });

                                               android.support.v7.app.AlertDialog alertDialog  = alertDialogBuilder.create();
                                               if( !( parentActivity.isFinishing() && alertDialog.isShowing() ) ){
                                                   alertDialog.show();
                                               }
                                           }
                                           else{

                                               android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(parentActivity);
                                               alertDialogBuilder.setTitle(parentActivity.getString(R.string.publish));
                                               alertDialogBuilder.setMessage(parentActivity.getString(R.string.message_publish_accommodation));
                                               alertDialogBuilder.setPositiveButton(parentActivity.getString(R.string.btn_yes), new DialogInterface.OnClickListener() {
                                                   @Override
                                                   public void onClick(DialogInterface dialog, int which) {
                                                       updateStatusMap.put(Room.IS_PUBLISH,true);
                                                       btn_stopPublish.setText(R.string.btn_stopPublish);
                                                       setPublish(updateStatusMap);
                                                   }
                                               });

                                               alertDialogBuilder.setNegativeButton(parentActivity.getString(R.string.btn_no), new DialogInterface.OnClickListener() {
                                                   @Override
                                                   public void onClick(DialogInterface dialog, int which) {
                                                       dialog.cancel();
                                                   }
                                               });

                                               android.support.v7.app.AlertDialog alertDialog  = alertDialogBuilder.create();
                                               if( !( parentActivity.isFinishing() && alertDialog.isShowing() ) ){
                                                   alertDialog.show();
                                               }
                                           }
                                       }
                                       else{
//                                           btn_stopPublish.setText(R.string.btn_startPublish);
                                           GeneralFunction.openMessageNotificationDialog(parentActivity,getString(R.string.status_error),R.drawable.notice_bad,TAG);
                                       }
                                    }
                                }
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        if(e!= null){
                            Log.e(TAG,e.toString());
                            GeneralFunction.Toast(parentActivity,e.toString());
                        }
                    }
                });

                break;

            case R.id.btn_editDetail:
                GeneralFunction.getFirestoreInstance()
                        .collection(Accommodation.URL_ACCOMMODATION)
                        .document(mAccommodationID)
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                if(task.isSuccessful()){
                                    final Room room = task.getResult().toObject(Room.class);
                                    if(room != null){
                                        GeneralFunction.getFirestoreInstance()
                                                .collection(Accommodation.URL_ACCOMMODATION)
                                                .document(mAccommodationID)
                                                .collection(Room.URL_ROOM_PROOF)
                                                .get()
                                                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                        if(task.isSuccessful()){
                                                            for(QueryDocumentSnapshot queryDocumentSnapshot : task.getResult()){
                                                                AccommodationDetailFragment fragment = AccommodationDetailFragment.newInstance(parentActivity,room,0,Room.TYPE_START_HOST,mAccommodationID,queryDocumentSnapshot.getId(), true, true);
                                                                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, fragment).addToBackStack(AccommodationDetailFragment.TAG).commit();
                                                            }
                                                        }
                                                    }
                                                });
                                    }
                                }
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e(TAG,e.toString());
                        GeneralFunction.Toast(parentActivity,e.toString());
                    }
                });


                break;
        }
    }
    private void setPublish(HashMap<String,Object>updateStatusMap){
        updateStatusMap.put(Room.DATE_UPDATED, FieldValue.serverTimestamp());
        GeneralFunction.getFirestoreInstance().collection(Accommodation.URL_ACCOMMODATION)
                .document(mAccommodationID)
                .update(updateStatusMap)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()){

                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e(TAG,e.toString());
            }
        });
    }
}
