package com.vidatechft.yupa.hostFragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.Accommodation;
import com.vidatechft.yupa.classes.Host;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.classes.Rules;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import static android.content.Context.MODE_PRIVATE;


public class RulesFragment extends Fragment implements View.OnClickListener {
    public static final String TAG = RulesFragment.class.getName();
    private MainActivity parentActivity;
    private EditText minimumStaySPN,maximumStaySPN,checkInSPN,checkOutSPN;
    private ConstraintLayout constraintLayoutRuleSpinnerItem;
    private ListView listViewRuleSpinnerItem;
    private ImageButton noPetAllowIB,petAllowIB,cookingAllowIB,noCookingAllowIB,smokingAllowIB,nosmokingAllowIB,
            laundryAllowIB,nolaundryAllowIB,eventPartyAllowIB,noeventPartyAllowIB,makingNoiseAllowIB,
            nomakingNoiseAllowIB,wearShoeInHouseAllowIB,nowearshoeInHouseAllowIB;
    private Button btn_next_rulesFragment;
    private String value;
    private String setRulesDocumentPath, getRulesDocumentPath;
    private DocumentReference rulesRef;
    private boolean checked = false;
    private boolean isFromDatabase = false;
    private String documentPushId;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private List<Rules> rulesDataset = new ArrayList<>();
    private List<EditText> editTexts = new ArrayList<>();
    private List<ImageButton> imageButtons = new ArrayList<>();
    private String rulesPushId;
    private boolean isEditProfile = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    public static RulesFragment newInstance(MainActivity parentActivity) {
        RulesFragment fragment = new RulesFragment();
        fragment.parentActivity = parentActivity;
        return fragment;
    }

    public static RulesFragment newInstance(MainActivity parentActivity, String documentPushId, boolean isFromDatabase, boolean isEditProfile) {
        RulesFragment fragment = new RulesFragment();
        fragment.parentActivity = parentActivity;
        fragment.documentPushId = documentPushId;
        fragment.isFromDatabase = isFromDatabase;
        fragment.isEditProfile = isEditProfile;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rules, container, false);
        // Inflate the layout for this fragment

        //editText
        minimumStaySPN = view.findViewById(R.id.minimumStaySPN);
        maximumStaySPN = view.findViewById(R.id.maximumStaySPN);
        checkInSPN = view.findViewById(R.id.checkInSPN);
        checkOutSPN = view.findViewById(R.id.checkOutSPN);

        //imageButton
        noPetAllowIB = view.findViewById(R.id.noPetAllowIB);
        petAllowIB = view.findViewById(R.id.petAllowIB);
        cookingAllowIB = view.findViewById(R.id.cookingAllowIB);
        noCookingAllowIB = view.findViewById(R.id.noCookingAllowIB);
        smokingAllowIB = view.findViewById(R.id.smokingAllowIB);
        nosmokingAllowIB = view.findViewById(R.id.nosmokingAllowIB);
        laundryAllowIB = view.findViewById(R.id.laundryAllowIB);
        nolaundryAllowIB = view.findViewById(R.id.nolaundryAllowIB);
        eventPartyAllowIB = view.findViewById(R.id.eventPartyAllowIB);
        noeventPartyAllowIB = view.findViewById(R.id.noeventPartyAllowIB);
        makingNoiseAllowIB = view.findViewById(R.id.makingNoiseAllowIB);
        nomakingNoiseAllowIB = view.findViewById(R.id.nomakingNoiseAllowIB);
        wearShoeInHouseAllowIB = view.findViewById(R.id.wearShoeInHouseAllowIB);
        nowearshoeInHouseAllowIB = view.findViewById(R.id.nowearshoeInHouseAllowIB);

        //button
        btn_next_rulesFragment = view.findViewById(R.id.btn_next_rulesFragment);

        constraintLayoutRuleSpinnerItem = view.findViewById(R.id.constraintLayoutSpinnerItem);
        listViewRuleSpinnerItem = view.findViewById(R.id.listViewRuleSpinnerItem);

        constraintLayoutRuleSpinnerItem.setOnClickListener(this);
        checkInSPN.setOnClickListener(this);
        checkOutSPN.setOnClickListener(this);
        noPetAllowIB.setOnClickListener(this);
        petAllowIB.setOnClickListener(this);
        cookingAllowIB.setOnClickListener(this);
        noCookingAllowIB.setOnClickListener(this);
        smokingAllowIB.setOnClickListener(this);
        nosmokingAllowIB.setOnClickListener(this);
        laundryAllowIB.setOnClickListener(this);
        nolaundryAllowIB.setOnClickListener(this);
        eventPartyAllowIB.setOnClickListener(this);
        noeventPartyAllowIB.setOnClickListener(this);
        makingNoiseAllowIB.setOnClickListener(this);
        nomakingNoiseAllowIB.setOnClickListener(this);
        wearShoeInHouseAllowIB.setOnClickListener(this);
        nowearshoeInHouseAllowIB.setOnClickListener(this);
        btn_next_rulesFragment.setOnClickListener(this);

        if(!isFromDatabase){
            setSharedPreferences();
        }
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.checkInSPN:
                GeneralFunction.slideUpAnimationForListView(parentActivity,constraintLayoutRuleSpinnerItem, listViewRuleSpinnerItem,R.array.guest_check_in,checkInSPN);
                MainActivity.isOpen = true;
                break;

            case R.id.checkOutSPN:
                GeneralFunction.slideUpAnimationForListView(parentActivity,constraintLayoutRuleSpinnerItem, listViewRuleSpinnerItem,R.array.guest_check_out,checkOutSPN);
                MainActivity.isOpen = true;
                break;

            case R.id.constraintLayoutSpinnerItem:
                GeneralFunction.slideDownAnimationForListView(parentActivity,constraintLayoutRuleSpinnerItem);
                break;

            case R.id.petAllowIB:
                buttonClickableAtOnce(petAllowIB, noPetAllowIB, true);
                break;

            case R.id.noPetAllowIB:
                buttonClickableAtOnce(petAllowIB, noPetAllowIB, false);
                break;

            case R.id.cookingAllowIB:
                buttonClickableAtOnce(cookingAllowIB, noCookingAllowIB, true);
                break;

            case R.id.noCookingAllowIB:
                buttonClickableAtOnce(cookingAllowIB, noCookingAllowIB, false);
                break;

            case R.id.smokingAllowIB:
                buttonClickableAtOnce(smokingAllowIB, nosmokingAllowIB, true);
                break;

            case R.id.nosmokingAllowIB:
                buttonClickableAtOnce(smokingAllowIB, nosmokingAllowIB, false);
                break;

            case R.id.laundryAllowIB:
                buttonClickableAtOnce(laundryAllowIB, nolaundryAllowIB, true);
                break;

            case R.id.nolaundryAllowIB:
                buttonClickableAtOnce(laundryAllowIB, nolaundryAllowIB, false);
                break;

            case R.id.eventPartyAllowIB:
                buttonClickableAtOnce(eventPartyAllowIB, noeventPartyAllowIB, true);
                break;

            case R.id.noeventPartyAllowIB:
                buttonClickableAtOnce(eventPartyAllowIB, noeventPartyAllowIB, false);
                break;

            case R.id.makingNoiseAllowIB:
                buttonClickableAtOnce(makingNoiseAllowIB, nomakingNoiseAllowIB, true);
                break;

            case R.id.nomakingNoiseAllowIB:
                buttonClickableAtOnce(makingNoiseAllowIB, nomakingNoiseAllowIB, false);
                break;

            case R.id.wearShoeInHouseAllowIB:
                buttonClickableAtOnce(wearShoeInHouseAllowIB, nowearshoeInHouseAllowIB, true);
                break;

            case R.id.nowearshoeInHouseAllowIB:
                buttonClickableAtOnce(wearShoeInHouseAllowIB, nowearshoeInHouseAllowIB, false);
                break;

            case R.id.btn_next_rulesFragment:
                submitRulesDetails();
                break;
        }
    }


    private void buttonClickableAtOnce(ImageButton btnTrue, ImageButton btnFalse, boolean isChecked){

        if(!isChecked){
            //set selection
            btnFalse.setSelected(true);
            btnTrue.setSelected(false);
            //set false background
            btnFalse.setBackground(parentActivity.getResources().getDrawable(R.drawable.rounded_acctype_white_bg_border_black));
//            btnFalse.setColorFilter(Color.argb(255, 255, 255, 255)); // White Tint
            //set true background
            btnTrue.setBackground(parentActivity.getResources().getDrawable(R.color.transparent));
        }
        else{
            //set selection
            btnTrue.setSelected(true);
            btnFalse.setSelected(false);
            //set true background
            btnTrue.setBackground(parentActivity.getResources().getDrawable(R.drawable.rounded_acctype_white_bg_border_black));
//            btnTrue.setColorFilter(Color.argb(255, 255, 255, 255)); // White Tint
            //set false background
            btnFalse.setBackground(parentActivity.getResources().getDrawable(R.color.transparent));
        }
    }

    private void submitRulesDetails(){
        String defaultText = "none";
        String getMinimumStay = minimumStaySPN.getText().toString().trim();
        String getMaximumStay = maximumStaySPN.getText().toString().trim();
        String getCheckIn = checkInSPN.getText().toString().trim();
        String getCheckOut = checkOutSPN.getText().toString().trim();

        if(TextUtils.isEmpty(getMinimumStay.trim())){
            minimumStaySPN.setText(defaultText);
        }
        else if(TextUtils.isEmpty(getMaximumStay.trim())){
            maximumStaySPN.setText(defaultText);
        }
        else if(!petAllowIB.isSelected() && !noPetAllowIB.isSelected()){
            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_choose_pet),R.drawable.notice_good,TAG);
            checked = false;
            return;
        }
        else if(!cookingAllowIB.isSelected() && !noCookingAllowIB.isSelected()){
            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_choose_cooking),R.drawable.notice_good,TAG);
            checked = false;
            return;
        }
        else if(!laundryAllowIB.isSelected() && !nolaundryAllowIB.isSelected()){
            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_choose_laundry),R.drawable.notice_good,TAG);
            checked = false;
            return;
        }
        else if(!eventPartyAllowIB.isSelected() && !noeventPartyAllowIB.isSelected()){
            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_choose_event),R.drawable.notice_good,TAG);
            checked = false;
            return;
        }
        else if(!makingNoiseAllowIB.isSelected() && !nomakingNoiseAllowIB.isSelected()){
            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_choose_making_noise),R.drawable.notice_good,TAG);
            checked = false;
            return;
        }

        else if(!wearShoeInHouseAllowIB.isSelected() && !nowearshoeInHouseAllowIB.isSelected()){
            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_choose_wear_shoe),R.drawable.notice_good,TAG);
            checked = false;
            return;
        }
        if(!checked){
            if(isValidInput(getMinimumStay,getMaximumStay,getCheckIn,getCheckOut)){
                HashMap<String, Object> ruleMap = new HashMap<>();
                ruleMap.put(Rules.MINIMIUM_NIGHTS,getMinimumStay);
                ruleMap.put(Rules.MAXIMUM_NIGHTS,getMaximumStay);
                ruleMap.put(Rules.CHECK_IN,getCheckIn);
                ruleMap.put(Rules.CHECK_OUT,getCheckOut);
                ruleMap.put(Rules.petAllowed,petAllowIB.isSelected());
                ruleMap.put(Rules.cookingAllowed,cookingAllowIB.isSelected());
                ruleMap.put(Rules.smokeAllowed,smokingAllowIB.isSelected());
                ruleMap.put(Rules.laundryAllowed,laundryAllowIB.isSelected());
                ruleMap.put(Rules.eventPartyAllowed,eventPartyAllowIB.isSelected());
                ruleMap.put(Rules.noisyAllowed,makingNoiseAllowIB.isSelected());
                ruleMap.put(Rules.shoeAllowed,wearShoeInHouseAllowIB.isSelected());
                if(!isFromDatabase){
                    GeneralFunction.saveAccommodationHashMapSharedPreferences(parentActivity,ruleMap,Rules.RULES_DETAILS,Rules.RULES_DETAILS);
                }
                parentActivity.initializeProgressIndicator(parentActivity.getString(R.string.employer_job_posting),false,this);
                parentActivity.controlProgressIndicator(true,this);
                saveRulesDetialsToDatabase(ruleMap);
            }

            else{
                Log.d("failed", "Failed Save to Shared Preferences");
            }
        }
    }

    private void saveRulesDetialsToDatabase(HashMap<String, Object> ruleMap){
        //initiate get path
        if(!isFromDatabase){
            value = GeneralFunction.getDocumentPath(parentActivity, Room.ACCOMMODATION_DETIALS,Room.DIR_DOCUMENT, "");
            getRulesDocumentPath = GeneralFunction.getDocumentPath(parentActivity,Rules.RULES_DETAILS,Rules.DIR_RULES,"");
        }


        if(getRulesDocumentPath != null && !getRulesDocumentPath.trim().equals("")){
            rulesRef = GeneralFunction.getFirestoreInstance()
                    .collection(Accommodation.URL_ACCOMMODATION)
                    .document(value)
                    .collection(Rules.URL_RULES)
                    .document(getRulesDocumentPath);
        }
        else{
            rulesRef = GeneralFunction.getFirestoreInstance()
                    .collection(Accommodation.URL_ACCOMMODATION)
                    .document(value)
                    .collection(Rules.URL_RULES)
                    .document();

            setRulesDocumentPath = rulesRef.getId();
            if(!isFromDatabase){
                GeneralFunction.saveDocumentPath(parentActivity, Rules.RULES_DETAILS,Rules.DIR_RULES,setRulesDocumentPath);
            }
        }
        setRulesDetails(rulesRef,ruleMap);
    }


    private boolean isValidInput(String getMinimumStay,String getMaximumStay, String getCheckIn, String getCheckOut){
        return  GeneralFunction.checkAndReturnString(getMinimumStay,parentActivity.getString(R.string.error_please_choose_minimum_night),parentActivity)
                &&
                GeneralFunction.checkAndReturnString(getMaximumStay,parentActivity.getString(R.string.error_please_choose_maximum_night),parentActivity)
                &&
                GeneralFunction.checkAndReturnString(getCheckIn,parentActivity.getString(R.string.error_please_select_checkIn_time),parentActivity)
                &&
                GeneralFunction.checkAndReturnString(getCheckOut,parentActivity.getString(R.string.error_please_select_checkOut_time),parentActivity);
    }

    private void setRulesDetails(DocumentReference rulesRef, HashMap<String,Object> rulesMap){
        if (isFromDatabase){
            rulesRef.update(rulesMap).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    goToDescriptionFragment();
                    GeneralFunction.handleUploadSuccess(parentActivity,RulesFragment.this,R.string.rules_upload_success,TAG);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    GeneralFunction.handleUploadError(parentActivity,RulesFragment.this,R.string.rules_err_upload,TAG);
                }
            });
        }
        else{
            rulesRef.set(rulesMap).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    goToDescriptionFragment();
                    GeneralFunction.handleUploadSuccess(parentActivity,RulesFragment.this,R.string.rules_upload_success,TAG);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    GeneralFunction.handleUploadError(parentActivity,RulesFragment.this,R.string.rules_err_upload,TAG);
                }
            });
        }
    }

    private void goToDescriptionFragment() {
        DescriptionFragment descriptionFragment = DescriptionFragment.newInstance(parentActivity, documentPushId, isFromDatabase,isEditProfile);
        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, descriptionFragment).addToBackStack(DescriptionFragment.TAG).commit();
    }

    private void setSharedPreferences(){
        try{
            SharedPreferences sharedPreferences = parentActivity.getSharedPreferences(Rules.RULES_DETAILS, MODE_PRIVATE);

            // Get saved string data in it.
            String userInfoListJsonString = sharedPreferences.getString(Rules.RULES_DETAILS, "");

            // Create Gson object and translate the json string to related java object array.
            Gson gson = new Gson();

            Rules userInfoDtoArray = gson.fromJson(userInfoListJsonString, Rules.class);

            // Loop the UserInfoDTO array and print each UserInfoDTO data in android monitor as debug log.
            // Get each user info in dto.
            Rules userInfoDto = userInfoDtoArray;

            if(userInfoDto != null){
                minimumStaySPN.setText(userInfoDto.minNightStay);
                maximumStaySPN.setText(userInfoDto.maxNightStay);
                checkInSPN.setText(userInfoDto.checkIn);
                checkOutSPN.setText(userInfoDto.checkOut);
            }

            //to get the bundle of value in sharedpreference with GOOGLE_MAP_APY_KEY
            Type type = new TypeToken<Map<String, Boolean>>(){}.getType();
            Map<String, Boolean> storeRuleMap = gson.fromJson(sharedPreferences.getString(Rules.RULES_DETAILS,""), type);
            if(storeRuleMap != null){
                //set button here
                petAllowIB.setSelected(storeRuleMap.get(Rules.petAllowed));
                cookingAllowIB.setSelected(storeRuleMap.get(Rules.cookingAllowed));
                smokingAllowIB.setSelected(storeRuleMap.get(Rules.smokeAllowed));
                laundryAllowIB.setSelected(storeRuleMap.get(Rules.laundryAllowed));
                eventPartyAllowIB.setSelected(storeRuleMap.get(Rules.eventPartyAllowed));
                makingNoiseAllowIB.setSelected(storeRuleMap.get(Rules.noisyAllowed));
                wearShoeInHouseAllowIB.setSelected(storeRuleMap.get(Rules.shoeAllowed));

                //set button for whether it true or false by compare
                buttonClickableAtOnce(petAllowIB,noPetAllowIB,storeRuleMap.get(Rules.petAllowed));
                buttonClickableAtOnce(cookingAllowIB,noCookingAllowIB,storeRuleMap.get(Rules.cookingAllowed));
                buttonClickableAtOnce(smokingAllowIB,nosmokingAllowIB,storeRuleMap.get(Rules.smokeAllowed));
                buttonClickableAtOnce(laundryAllowIB,nolaundryAllowIB,storeRuleMap.get(Rules.laundryAllowed));
                buttonClickableAtOnce(eventPartyAllowIB,noeventPartyAllowIB,storeRuleMap.get(Rules.eventPartyAllowed));
                buttonClickableAtOnce(makingNoiseAllowIB,nomakingNoiseAllowIB,storeRuleMap.get(Rules.noisyAllowed));
                buttonClickableAtOnce(wearShoeInHouseAllowIB,nowearshoeInHouseAllowIB,storeRuleMap.get(Rules.shoeAllowed));
            }
        }
        catch(Exception e){
            Crashlytics.logException(e);
        }
    }

    private void retrieveFromFirestore(){
        final CollectionReference rulesRef = db.collection(Accommodation.URL_ACCOMMODATION).document(documentPushId).collection(Rules.URL_RULES);
        rulesRef.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.w(TAG, "Listen failed.", e);
                    return;
                }
                String source = queryDocumentSnapshots != null && queryDocumentSnapshots.getMetadata().hasPendingWrites()
                        ? "Local" : "Server";
                Log.d(TAG,source);

                if(queryDocumentSnapshots != null){
                    if(!queryDocumentSnapshots.isEmpty()){

                        for(QueryDocumentSnapshot queryDocumentSnapshot : queryDocumentSnapshots){
                            rulesDataset.add(new Rules(parentActivity,queryDocumentSnapshot));
                        }

                        for(DocumentSnapshot documentSnapshot : queryDocumentSnapshots){
                            rulesPushId = documentSnapshot.getId();
                        }

                        rulesDataset.size();

                        setRulesDetails(rulesDataset);

                    }
                }
            }
        });

    }

    private void setRulesDetails(List<Rules> dataSet){
        value = documentPushId;
        if(rulesPushId != null){
            getRulesDocumentPath = rulesPushId;
        }
        //clear before use ensure it cant loop many
//        editTexts.clear();
//        editTexts.add(minimumStaySPN);
//        editTexts.add(maximumStaySPN);
//        editTexts.add(checkInSPN);
//        editTexts.add(checkOutSPN);
//
//        imageButtons.clear();
//        imageButtons.add(petAllowIB);
//        imageButtons.add(cookingAllowIB);
//        imageButtons.add(smokingAllowIB);
//        imageButtons.add(laundryAllowIB);
//        imageButtons.add(eventPartyAllowIB);
//        imageButtons.add(makingNoiseAllowIB);
//        imageButtons.add(wearShoeInHouseAllowIB);

        if(dataSet != null){
            for(Rules text : dataSet){
                minimumStaySPN.setText(text.minNightStay);
                maximumStaySPN.setText(text.maxNightStay);
                checkInSPN.setText(text.checkIn);
                checkOutSPN.setText(text.checkOut);
                petAllowIB.setSelected(text.petAllow);
                cookingAllowIB.setSelected(text.cookingAllow);
                smokingAllowIB.setSelected(text.smokeAllow);
                laundryAllowIB.setSelected(text.laundryAllow);
                eventPartyAllowIB.setSelected(text.eventPartyAllow);
                makingNoiseAllowIB.setSelected(text.noisyAllow);
                wearShoeInHouseAllowIB.setSelected(text.shoeAllow);

                //set button for whether it true or false by compare
                buttonClickableAtOnce(petAllowIB,noPetAllowIB,text.petAllow);
                buttonClickableAtOnce(cookingAllowIB,noCookingAllowIB,text.cookingAllow);
                buttonClickableAtOnce(smokingAllowIB,nosmokingAllowIB,text.smokeAllow);
                buttonClickableAtOnce(laundryAllowIB,nolaundryAllowIB,text.laundryAllow);
                buttonClickableAtOnce(eventPartyAllowIB,noeventPartyAllowIB,text.eventPartyAllow);
                buttonClickableAtOnce(makingNoiseAllowIB,nomakingNoiseAllowIB,text.noisyAllow);
                buttonClickableAtOnce(wearShoeInHouseAllowIB,nowearshoeInHouseAllowIB,text.shoeAllow);
            }
        }
        else{
            Toast.makeText(parentActivity, "Data is null", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if(isFromDatabase){
            retrieveFromFirestore();
        }
        parentActivity.selectHotel();
        parentActivity.setToolbarTitle(parentActivity.getString(R.string.host_an_accommodation));
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
