package com.vidatechft.yupa.hostFragments;

import android.animation.ValueAnimator;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.DiscussionAdapter;
import com.vidatechft.yupa.adapter.HostStayForManageRecyclerGridAdapter;
import com.vidatechft.yupa.classes.Accommodation;
import com.vidatechft.yupa.classes.BookHomestayDetails;
import com.vidatechft.yupa.classes.BookingHistory;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Nullable;

import static com.vidatechft.yupa.classes.Room.TYPE_PUBLISHED;
import static com.vidatechft.yupa.classes.Room.TYPE_REJECTED;
import static com.vidatechft.yupa.classes.Room.TYPE_START_HOST;
import static com.vidatechft.yupa.classes.Room.TYPE_UNPUBLISHED;
import static com.vidatechft.yupa.classes.Room.TYPE_WAIT_VERIFY;


public class HostAnAccommodationFragment extends Fragment implements AbsListView.OnScrollListener, View.OnClickListener{
    public static final String TAG = HostAnAccommodationFragment.class.getName();
    private MainActivity parentActivity;
    private RecyclerView startHostGV, waitingForVerifyGV,publishGV,unPublishGV,rejectedGV;
    private String getPushId;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private HostStayForManageRecyclerGridAdapter startToHostAdapter,waitingVerificationAdapter, publishedAdapter, unPublishedAdapter, rejectedAdapter;


    private ArrayList<Room> startHostAllRooms = new ArrayList<>();
    private ArrayList<Room> startHostDisplayRooms = new ArrayList<>();

    final ArrayList<Room>pendingAllRoom = new ArrayList<>();
    final ArrayList<Room>pendingRoom = new ArrayList<>();

    final ArrayList<Room>rejectedAllRoom = new ArrayList<>();
    final ArrayList<Room>rejectedRoom = new ArrayList<>();

    final ArrayList<Room>unpublishedAllRoom = new ArrayList<>();
    final ArrayList<Room>unpublishedRoom = new ArrayList<>();

    final ArrayList<Room>publishedAllRoom = new ArrayList<>();
    final ArrayList<Room>publishedRoom = new ArrayList<>();
    private ArrayList<Room> finalRooms = new ArrayList<>();

    private List<String> documentPushIdFromFirestore = new ArrayList<>();
    private List<String> electricBillPushIdFromFirestore = new ArrayList<>();
    private List<List<Room>> electricBillUrl = new ArrayList<List<Room>>();
    private List<GeoPoint> geoPoint = new ArrayList<>();

    private ImageButton startToHost_loadMore,waitingForVerify_loadMore,publish_loadMore,unPublish_loadMore,rejected_loadMore;
    private boolean btnIsDown_startHost = false;
    private boolean btnIsDown_waitVerify = false;
    private boolean btnIsDown_publised = false;
    private boolean btnIsDown_unpublised = false;
    private boolean btnIsDown_rejected = false;
    private boolean btn_startHost, btn_waitVerify, btn_publish,btn_unpublish,btn_reject;
    private ConstraintLayout startHostCL,waitingForVerifyCL,publishCL,unPublishCL, rejectCL;
    private Button btn_submitNewAccommodation;
    private HashMap<Integer, List<String>> getDocumentId;
    private boolean isFromFireStore = false;
    private boolean startHostOnBackPress = false;
    private boolean waitVerifyOnBackPress = false;
    private boolean publishedOnBackPress = false;
    private boolean unpublishedOnBackPress = false;
    private boolean rejectedOnBackPress = false;
    private ConstraintLayout emptyCL;
    private ProgressDialog progress;
    private int progressBarCount = 0;
    private ListenerRegistration listenerRegistration1;
    private String getElectricTrueURL;
    private boolean isRecentPress = false;
    private Bundle getSavedInstanceState;
    private Boolean firstTime = null;
    private List<Boolean> isAllDownloaded = new ArrayList<>();


    private int pubishedRoomBookingHistoriesDownloadCount = 0;

    public static HostAnAccommodationFragment newInstance(MainActivity parentActivity) {
        HostAnAccommodationFragment fragment = new HostAnAccommodationFragment();
        fragment.parentActivity = parentActivity;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPushId = GeneralFunction.getDocumentPath(parentActivity, Room.ACCOMMODATION_DETIALS,Room.DIR_DOCUMENT, "");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, final Bundle savedInstanceState) {
        //get saveinstancestate
        getSavedInstanceState = savedInstanceState;
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_host_an_accommodation, container, false);

        progress = new ProgressDialog(parentActivity);

        //constraint layout
        emptyCL = view.findViewById(R.id.emptyCL);

        //grid view
        startHostGV = view.findViewById(R.id.startHostGV);
        waitingForVerifyGV = view.findViewById(R.id.waitingForVerifyGV);
        publishGV = view.findViewById(R.id.publishGV);
        unPublishGV = view.findViewById(R.id.unPublishGV);
        rejectedGV = view.findViewById(R.id.rejectedGV);

        //image button
        startToHost_loadMore = view.findViewById(R.id.startToHost_loadMore);
        waitingForVerify_loadMore = view.findViewById(R.id.waitingForVerify_loadMore);
        publish_loadMore = view.findViewById(R.id.publish_loadMore);
        unPublish_loadMore = view.findViewById(R.id.unPublish_loadMore);
        rejected_loadMore = view.findViewById(R.id.rejected_loadMore);

        //button
        btn_submitNewAccommodation = view.findViewById(R.id.btn_submitNewAccommodation);

        //constraint layout
        startHostCL = view.findViewById(R.id.startHostCL);
        waitingForVerifyCL = view.findViewById(R.id.waitingForVerifyCL);
        publishCL = view.findViewById(R.id.publishCL);
        unPublishCL = view.findViewById(R.id.unPublishCL);
        rejectCL = view.findViewById(R.id.rejectCL);

        //listener
        startToHost_loadMore.setOnClickListener(this);
        waitingForVerify_loadMore.setOnClickListener(this);
        publish_loadMore.setOnClickListener(this);
        unPublish_loadMore.setOnClickListener(this);
        rejected_loadMore.setOnClickListener(this);
        btn_submitNewAccommodation.setOnClickListener(this);

        startHostGV.requestDisallowInterceptTouchEvent(false);
        waitingForVerifyGV.requestDisallowInterceptTouchEvent(false);
        publishGV.requestDisallowInterceptTouchEvent(false);
        unPublishGV.requestDisallowInterceptTouchEvent(false);
        rejectedGV.requestDisallowInterceptTouchEvent(false);

        startToHostAdapter = new HostStayForManageRecyclerGridAdapter(parentActivity, startHostDisplayRooms, TYPE_START_HOST);
        waitingVerificationAdapter = new HostStayForManageRecyclerGridAdapter(parentActivity, pendingRoom, TYPE_WAIT_VERIFY);
        unPublishedAdapter = new HostStayForManageRecyclerGridAdapter(parentActivity, unpublishedRoom, TYPE_UNPUBLISHED);
        rejectedAdapter = new HostStayForManageRecyclerGridAdapter(parentActivity, rejectedRoom, TYPE_REJECTED);

//        triggerBookHomeStay();
        return view;
    }

    private void showStartHost(){

        if(btnIsDown_startHost){
            startHostOnBackPress = true;
            startHostDisplayRooms.clear();
            startHostDisplayRooms.addAll(startHostAllRooms);
            startToHostAdapter.notifyDataSetChanged();
            //request layout to update the layout
            startHostGV.requestLayout();

        }
        else{
            startHostDisplayRooms.clear();
            startHostOnBackPress = false;
        }
        if(btnIsDown_waitVerify){
            waitVerifyOnBackPress = true;
            pendingRoom.clear();
            pendingRoom.addAll(pendingAllRoom);
            waitingVerificationAdapter.notifyDataSetChanged();
            //request layout to update the layout
            waitingForVerifyGV.requestLayout();

        }
        else{
            pendingRoom.clear();
            waitVerifyOnBackPress = false;
        }

        if(btnIsDown_publised){
            publishedOnBackPress = true;
            publishedRoom.clear();
            publishedRoom.addAll(finalRooms);
            publishedAdapter.notifyDataSetChanged();
            //request layout to update the layout
            publishGV.requestLayout();

        }
        else{
            publishedRoom.clear();
            publishedOnBackPress = false;
        }

        if(btnIsDown_unpublised){
            unpublishedOnBackPress = true;
            unpublishedRoom.clear();
            unpublishedRoom.addAll(unpublishedAllRoom);
            unPublishedAdapter.notifyDataSetChanged();
            //request layout to update the layout
            unPublishGV.requestLayout();

        }
        else{
            unpublishedRoom.clear();
            unpublishedOnBackPress = false;
        }

        if(btnIsDown_rejected){
            rejectedOnBackPress = true;
            rejectedRoom.clear();
            rejectedRoom.addAll(rejectedAllRoom);
            rejectedAdapter.notifyDataSetChanged();
            //request layout to update the layout
            rejectedGV.requestLayout();

        }
        else{
            rejectedRoom.clear();
            rejectedOnBackPress = false;
        }
//        CollectionReference cocRef = db.collection(Accommodation.URL_ACCOMMODATION);
//        listenerRegistration1 = cocRef.whereEqualTo(Room.HOST_ID, parentActivity.uid)
//                .whereEqualTo(Setting.IS_DRAFT, true)
        listenerRegistration1 =  GeneralFunction.getFirestoreInstance()
                        .collection(Accommodation.URL_ACCOMMODATION)
                        .whereEqualTo(Room.HOST_ID,parentActivity.uid)
                .orderBy(Room.DATE_CREATED, Query.Direction.DESCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        if(e != null){
                            progress.dismiss();
                            Log.e(TAG,e.toString());
                            Toast.makeText(parentActivity, e.toString(), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        startHostDisplayRooms.clear();
                        startHostAllRooms.clear();
                        pendingRoom.clear();
                        pendingAllRoom.clear();
                        publishedRoom.clear();
                        publishedAllRoom.clear();
                        finalRooms.clear();
                        unpublishedRoom.clear();
                        unpublishedAllRoom.clear();
                        rejectedRoom.clear();
                        rejectedAllRoom.clear();
                        String source = queryDocumentSnapshots != null && queryDocumentSnapshots.getMetadata().hasPendingWrites()
                                ? "Local" : "Server";
                        if(!source.equals("Local")){
                            GeneralFunction.clearGlideCacheMemory(parentActivity);
                            startHostDisplayRooms.clear();
                            startHostAllRooms.clear();
                            pendingRoom.clear();
                            pendingAllRoom.clear();
                            publishedRoom.clear();
                            publishedAllRoom.clear();
                            finalRooms.clear();
                            unpublishedRoom.clear();
                            unpublishedAllRoom.clear();
                            rejectedRoom.clear();
                            rejectedAllRoom.clear();
                            startToHostAdapter.notifyDataSetChanged();
                            waitingVerificationAdapter.notifyDataSetChanged();
                            unPublishedAdapter.notifyDataSetChanged();
                            rejectedAdapter.notifyDataSetChanged();
                            startHostGV.requestLayout();
                            waitingForVerifyGV.requestLayout();
                            publishGV.requestLayout();
                            unPublishGV.requestLayout();
                            rejectedGV.requestLayout();
                        }
                            if(queryDocumentSnapshots != null){
                                if(!queryDocumentSnapshots.isEmpty()){
                                    for(final QueryDocumentSnapshot queryDocumentSnapshot:queryDocumentSnapshots){
                                        if(queryDocumentSnapshot.exists()){
                                            if(emptyCL.getVisibility() == View.VISIBLE){
                                                emptyCL.setVisibility(View.GONE);
                                            }
                                        }
                                        else{
                                            emptyCL.setVisibility(View.GONE);
                                        }
                                        final Room room = Room.snapshotToRoom(parentActivity,queryDocumentSnapshot);
                                        if(room != null){
                                            if(room.isDraft != null && room.isDraft){
                                                startHostAllRooms.add(room);
                                            }
                                            if(room.status != null){
                                                switch (room.status){
                                                    case Room.STATUS_PENDING:
                                                        pendingAllRoom.add(room);
                                                        break;
                                                    case Room.STATUS_APPROVED:
                                                        if(room.isPublish != null && room.isPublish){
                                                            publishedAllRoom.add(room);
                                                            final int INDEX = publishedAllRoom.size() - 1;

                                                            GeneralFunction.getFirestoreInstance()
                                                                    .collection(BookHomestayDetails.URL_BOOK_GUEST)
                                                                    .whereEqualTo(BookingHistory.TARGET_ID,room.id)
                                                                    .whereEqualTo(BookingHistory.HOST_ID,parentActivity.uid)
                                                                    .addSnapshotListener(new EventListener<QuerySnapshot>() {
                                                                        @Override
                                                                        public void onEvent(@Nullable QuerySnapshot bookHomestaySnapshots, @Nullable FirebaseFirestoreException e) {
                                                                            if(e != null){
                                                                                GeneralFunction.openMessageNotificationDialog(parentActivity,e.toString(),R.drawable.notice_bad,TAG);
                                                                                return;
                                                                            }

                                                                            if(bookHomestaySnapshots !=null && !bookHomestaySnapshots.isEmpty()){
                                                                                ArrayList<BookingHistory> tempBookingList = new ArrayList<>();
                                                                                for(final DocumentSnapshot bookHomestaySnapshot : bookHomestaySnapshots){
                                                                                    if(bookHomestaySnapshot != null && bookHomestaySnapshot.exists()){
                                                                                        BookingHistory tempBookingHistory = BookingHistory.snapshotToBookingHistory(parentActivity,bookHomestaySnapshot);

                                                                                        if (tempBookingHistory != null) {
                                                                                            if(tempBookingHistory.checkinDate < GeneralFunction.getDefaultUtcCalendar().getTimeInMillis()){
                                                                                                bookHomestaySnapshot.getReference().delete();
                                                                                            }else{
                                                                                                tempBookingList.add(tempBookingHistory);
                                                                                            }
                                                                                        }

                                                                                    }
                                                                                }
                                                                                publishedAllRoom.get(INDEX).bookingHistories = tempBookingList;
                                                                            }

                                                                            publishDisplayRoom(publishedAllRoom);
                                                                        }
                                                                    });
                                                        }
                                                        else{
                                                            unpublishedAllRoom.add(room);
                                                        }
                                                        break;
                                                    case Room.STATUS_REJECT:
                                                        rejectedAllRoom.add(room);
                                                        break;
                                                }

                                            }
                                        }
                                    }
                                }

                                //show visible to CL when the room size is not equal 0
                                if(startHostAllRooms.size() != 0){
                                    startHostCL.setVisibility(View.VISIBLE);
                                    //if the room size is small or less than 2 hide the load more button
                                    if(startHostAllRooms.size() <= 2 ){
                                        startToHost_loadMore.setVisibility(View.GONE); // Load More Button
                                        //for show one result by if
                                        for(int i = 0; i < startHostAllRooms.size(); i++){
                                            if(startHostAllRooms.size() == 1){
                                                startHostDisplayRooms.add(startHostAllRooms.get(i));
                                            }
                                            else{
                                                if(startHostAllRooms.get(i) != null){
                                                    startHostDisplayRooms.add(startHostAllRooms.get(i));
                                                }
                                            }
                                        }
                                    }
                                    else{
                                        startToHost_loadMore.setVisibility(View.VISIBLE); // Load More Button
                                        //if not on back press
                                        if(!startHostOnBackPress){
                                            //purposely show 2 result only
                                            for(int i=0; i < 2; i++){
                                                if(startHostAllRooms.get(i) != null){
                                                    startHostDisplayRooms.add(startHostAllRooms.get(i));
                                                }
                                            }
                                        }
                                        //show more than 2 result when onbackpress
                                        else{
                                            startHostAllRooms.clear();
                                            startHostDisplayRooms.addAll(startHostAllRooms);
                                            startToHostAdapter.notifyDataSetChanged();
                                        }
                                    }
//                                    setStartToHostAdapter(savedInstanceState);
                                    startHostGV.setLayoutManager(new GridLayoutManager(parentActivity, 2));
                                    startHostGV.setNestedScrollingEnabled(false);
                                    startHostGV.setAdapter(startToHostAdapter);
                                }
                                else{
                                    startHostCL.setVisibility(View.GONE);
                                }

                                startToHostAdapter.notifyDataSetChanged();


                                // Waiting verifty here
                                pendingDisplayRoom(pendingAllRoom);

//                                if(!isFirstTime()){
//
//                                    //publish room here
//                                    for(final Room tempRoom : publishedAllRoom){
//                                        if(tempRoom != null){
//                                            GeneralFunction.getFirestoreInstance()
//                                                    .collection(BookHomestayDetails.URL_BOOK_GUEST)
//                                                    .whereEqualTo(BookingHistory.TARGET_ID,tempRoom.id)
//                                                    .whereEqualTo(BookingHistory.HOST_ID,parentActivity.uid)
//                                                    .addSnapshotListener(new EventListener<QuerySnapshot>() {
//                                                        @Override
//                                                        public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
//                                                            if(e != null){
//                                                                GeneralFunction.openMessageNotificationDialog(parentActivity,e.toString(),R.drawable.notice_bad,TAG);
//                                                                return;
//                                                            }
//                                                            pubishedRoomBookingHistoriesDownloadCount++;
//                                                            if(queryDocumentSnapshots != null && !queryDocumentSnapshots.isEmpty()){
//                                                                ArrayList<BookingHistory> bookingHistories = new ArrayList<>();
//                                                                for(DocumentSnapshot snapshot : queryDocumentSnapshots){
//                                                                    BookingHistory bookingHistory = new BookingHistory(parentActivity,snapshot);
//                                                                    if(bookingHistory.actionType == null){
//                                                                        bookingHistories.add(bookingHistory);
//                                                                        tempRoom.bookingHistories = bookingHistories;
//                                                                    }
//                                                                }
//
//                                                            }
//                                                            finalRooms.clear();
//                                                            finalRooms.add(tempRoom);
//
//                                                            if(pubishedRoomBookingHistoriesDownloadCount == publishedAllRoom.size()){
//                                                                pubishedRoomBookingHistoriesDownloadCount = 0;
//                                                                publishDisplayRoom(finalRooms);
//                                                            }
//                                                        }
//                                                    });
//
//                                        }
//                                    }
//                                }


                                //unpublish room here
                                unPublishDisplayRoom(unpublishedAllRoom);

                                //rejected room here
                                rejectDisplayRoom(rejectedAllRoom);
                            }
                    }
                });
    }


    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
//        if (startHostGV.getLastVisiblePosition() + 1 >= 2) {
//            startToHost_loadMore.setVisibility(View.VISIBLE); // Load More Button
//            startHostGV.setOnTouchListener(new View.OnTouchListener(){
//
//                @Override
//                public boolean onTouch(View v, MotionEvent event) {
//                    return event.getAction() == MotionEvent.ACTION_MOVE;
//                }
//
//            });
//            startToHost_loadMore.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                   expand(startHostGV,1000,1200);
//                }
//            });
//        }
//        else{
//            startToHost_loadMore.setVisibility(View.GONE); // Load More Button
//
//        }
    }

    public static void expand(final View v, int duration, int targetHeight) {

        int prevHeight  = v.getHeight();

        v.setVisibility(View.VISIBLE);
        ValueAnimator valueAnimator = ValueAnimator.ofInt(prevHeight, targetHeight);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                v.getLayoutParams().height = (int) animation.getAnimatedValue();
                v.requestLayout();
            }
        });
        valueAnimator.setInterpolator(new DecelerateInterpolator());
        valueAnimator.setDuration(duration);
        valueAnimator.start();
    }

    public static void collapse(final View v, int duration, int targetHeight) {
        int prevHeight  = v.getHeight();
        ValueAnimator valueAnimator = ValueAnimator.ofInt(prevHeight, targetHeight);
        valueAnimator.setInterpolator(new DecelerateInterpolator());
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                v.getLayoutParams().height = (int) animation.getAnimatedValue();
                v.requestLayout();
            }
        });
        valueAnimator.setInterpolator(new DecelerateInterpolator());
        valueAnimator.setDuration(duration);
        valueAnimator.start();
    }


    @Override
    public void onClick(View v) {

        FragmentTransaction ft = parentActivity.getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.fragment_enter, R.anim.fragment_exit, R.anim.fragment_pop_enter, R.anim.fragment_pop_exit);
        Fragment fragment = parentActivity.getSupportFragmentManager().findFragmentById(R.id.mainFL);
        if(v.getId() == R.id.startToHost_loadMore){

            if(!btnIsDown_startHost){
                btnIsDown_startHost = true;
                startHostDisplayRooms.clear();
                startHostDisplayRooms.addAll(startHostAllRooms);
                startToHostAdapter.notifyDataSetChanged();
                //request layout to update the layout
                startHostGV.requestLayout();
                startToHost_loadMore.setImageResource(R.drawable.arrow_up_black);
            }
            else{
                btnIsDown_startHost = false;
                startHostDisplayRooms.clear();
                startHostGV.requestLayout();
                for(int i=0; i < 2; i++){
                    if(startHostAllRooms.get(i) != null){
                        startHostDisplayRooms.add(startHostAllRooms.get(i));
                        startToHost_loadMore.setVisibility(View.VISIBLE); // Load More Button
                    }
                    else{
                        startToHost_loadMore.setVisibility(View.GONE); // Load More Button
                    }
                }
                startToHostAdapter.notifyDataSetChanged();
                startToHost_loadMore.setImageResource(R.drawable.arrow_down_black);
            }
        }

        if(v.getId() == R.id.waitingForVerify_loadMore){
            if(!btnIsDown_waitVerify){
                btnIsDown_waitVerify = true;
                pendingRoom.clear();
                pendingRoom.addAll(pendingAllRoom);
                startToHostAdapter.notifyDataSetChanged();
                //request layout to update the layout
                waitingForVerifyGV.requestLayout();
                waitingForVerify_loadMore.setImageResource(R.drawable.arrow_up_black);
            }
            else{
                btnIsDown_waitVerify = false;
                pendingRoom.clear();
                waitingForVerifyGV.requestLayout();
                for(int i=0; i < 2; i++){
                    if(pendingAllRoom.get(i) != null){
                        pendingRoom.add(pendingAllRoom.get(i));
                        waitingForVerify_loadMore.setVisibility(View.VISIBLE); // Load More Button
                    }
                    else{
                        waitingForVerify_loadMore.setVisibility(View.GONE); // Load More Button
                    }
                }
                waitingVerificationAdapter.notifyDataSetChanged();
                waitingForVerify_loadMore.setImageResource(R.drawable.arrow_down_black);
            }
        }

        if(v.getId() == R.id.publish_loadMore){
            if(!btnIsDown_publised){
                btnIsDown_publised = true;
                publishedRoom.clear();
                publishedRoom.addAll(finalRooms);
                publishedAdapter.notifyDataSetChanged();
                //request layout to update the layout
                publishGV.requestLayout();
                publish_loadMore.setImageResource(R.drawable.arrow_up_black);
            }
            else{
                btnIsDown_publised = false;
                publishedRoom.clear();
//                publishGV.requestLayout();
                for(int i=0; i < 2; i++){
                    if(finalRooms.get(i) != null){
                        publishedRoom.add(finalRooms.get(i));
                        publish_loadMore.setVisibility(View.VISIBLE); // Load More Button
                    }
                    else{
                        publish_loadMore.setVisibility(View.GONE); // Load More Button
                    }
                }
                publishedAdapter.notifyDataSetChanged();
                publish_loadMore.setImageResource(R.drawable.arrow_down_black);
            }
        }

        if(v.getId() == R.id.unPublish_loadMore){
            if(!btnIsDown_unpublised){
                btnIsDown_unpublised = true;
                unpublishedRoom.clear();
                unpublishedRoom.addAll(unpublishedAllRoom);
                unPublishedAdapter.notifyDataSetChanged();
                //request layout to update the layout
                unPublishGV.requestLayout();
                unPublish_loadMore.setImageResource(R.drawable.arrow_up_black);
            }
            else{
                btnIsDown_unpublised = false;
                unpublishedRoom.clear();
                unPublishGV.requestLayout();
                for(int i=0; i < 2; i++){
                    if(unpublishedAllRoom.get(i) != null){
                        unpublishedRoom.add(unpublishedAllRoom.get(i));
                        unPublish_loadMore.setVisibility(View.VISIBLE); // Load More Button
                    }
                    else{
                        unPublish_loadMore.setVisibility(View.GONE); // Load More Button
                    }
                }
                unPublishedAdapter.notifyDataSetChanged();
                unPublish_loadMore.setImageResource(R.drawable.arrow_down_black);
            }
        }

        if(v.getId() == R.id.rejected_loadMore){
            if(!btnIsDown_rejected){
                btnIsDown_rejected = true;
                rejectedRoom.clear();
                rejectedRoom.addAll(rejectedAllRoom);
                rejectedAdapter.notifyDataSetChanged();
                //request layout to update the layout
                rejectedGV.requestLayout();
                rejected_loadMore.setImageResource(R.drawable.arrow_up_black);
            }
            else{
                btnIsDown_rejected = false;
                rejectedRoom.clear();
                rejectedGV.requestLayout();
                for(int i=0; i < 2; i++){
                    if(rejectedAllRoom.get(i) != null){
                        rejectedRoom.add(rejectedAllRoom.get(i));
                        rejected_loadMore.setVisibility(View.VISIBLE); // Load More Button
                    }
                    else{
                        rejected_loadMore.setVisibility(View.GONE); // Load More Button
                    }
                }
                rejectedAdapter.notifyDataSetChanged();
                rejected_loadMore.setImageResource(R.drawable.arrow_down_black);
            }
        }

        if(v.getId() == R.id.btn_submitNewAccommodation){
            isFromFireStore = false;
            if(!(fragment instanceof AccommodationDetailFragment)){
                AccommodationDetailFragment accommodationDetailFragment = AccommodationDetailFragment.newInstance(parentActivity);
                ft.replace(R.id.mainFL, accommodationDetailFragment).addToBackStack(AccommodationDetailFragment.TAG);
                ft.commit();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        geoPoint.clear();
        finalRooms.clear();
        progressBarCount = 0;
        btn_startHost = btnIsDown_startHost;
        btn_waitVerify = btnIsDown_waitVerify;
        btn_publish = btnIsDown_publised;
        btn_unpublish = btnIsDown_unpublised;
        btn_reject = btnIsDown_rejected;
        documentPushIdFromFirestore.clear();
        electricBillPushIdFromFirestore.clear();
        electricBillUrl.clear();
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.setToolbarTitle(parentActivity.getString(R.string.host_dashoard_toolbar));
        parentActivity.selectHotel();
//        showProgressBar();
        showStartHost();
//        showWaitingVerify(getSavedInstanceState);
//        showPublishedVerify(getSavedInstanceState);
        if(btn_startHost){
            startToHost_loadMore.setImageResource(R.drawable.arrow_up_black);
        }
        else{
            startToHost_loadMore.setImageResource(R.drawable.arrow_down_black);
        }
        if(btn_waitVerify){
            waitingForVerify_loadMore.setImageResource(R.drawable.arrow_up_black);
        }
        else{
            waitingForVerify_loadMore.setImageResource(R.drawable.arrow_down_black);
        }
        if(btn_publish){
            publish_loadMore.setImageResource(R.drawable.arrow_up_black);
        }
        else{
            publish_loadMore.setImageResource(R.drawable.arrow_down_black);
        }
        if(btn_unpublish){
            unPublish_loadMore.setImageResource(R.drawable.arrow_up_black);
        }
        else{
            unPublish_loadMore.setImageResource(R.drawable.arrow_down_black);
        }
        if(btn_reject){
            rejected_loadMore.setImageResource(R.drawable.arrow_up_black);
        }
        else{
            rejected_loadMore.setImageResource(R.drawable.arrow_down_black);
        }

    }

    private void showProgressBar(){
        progress.setCancelable(false);
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();
    }

    private View.OnClickListener mRecentsClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            //add by gaoming for LG_MLT Menu GOOGLE_MAP_APY_KEY Monitor--start{
            isRecentPress = true;
            Intent intent = new Intent();
            intent.setAction("com.action.lgdb.key_event");
            Bundle bundle = new Bundle();
            bundle.putInt("lgdb_key_event", 187);
            Log.d("gaoming", " system ui send lgdb GOOGLE_MAP_APY_KEY event MENU: " + "KEYCODE_APP_SWITCH");
            intent.putExtras(bundle);
            parentActivity.sendBroadcast(intent);
        }
    };

    private void getElectricBillPicUrl(final Room room){
        if(room != null && room.id != null){
            GeneralFunction.getFirestoreInstance()
                    .collection(Accommodation.URL_ACCOMMODATION)
                    .document(room.id)
                    .collection(Room.URL_ROOM_PROOF)
                    .limit(1)
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if(task.isSuccessful()){
                                for(QueryDocumentSnapshot queryDocumentSnapshot : task.getResult()){
                                    Room tempRoom = Room.snapshotToRoom(parentActivity, queryDocumentSnapshot);
                                    if(tempRoom != null){
                                        room.urlElectricBill = tempRoom.urlElectricBill;
                                    }
                                }
                            }
                        }
                    });

        }
    }

    private void pendingDisplayRoom(ArrayList<Room>pendingAllRoom){
        if(pendingAllRoom.size() != 0) {
//                                    if(emptyCL.getVisibility() == View.VISIBLE){
//                                        emptyCL.setVisibility(View.GONE);
//                                    }
            waitingForVerifyCL.setVisibility(View.VISIBLE);

            if(pendingAllRoom.size() <= 2 ){
                waitingForVerify_loadMore.setVisibility(View.GONE); // Load More Button

                for(int i = 0; i < pendingAllRoom.size(); i++){
                    if(pendingAllRoom.size() == 1){
                        pendingRoom.add(pendingAllRoom.get(i));
                    }
                    else{
//                                                if(pendingRoom.get(i) != null){
                        pendingRoom.add(pendingAllRoom.get(i));
//                                                }
                    }
                }
            }
            else{
                waitingForVerify_loadMore.setVisibility(View.VISIBLE); // Load More Button

                //if not on back press
                if(!waitVerifyOnBackPress){
                    //purposely show 2 result only
                    for(int i=0; i < 2; i++){
                        if(pendingAllRoom.get(i) != null){
                            pendingRoom.add(pendingAllRoom.get(i));
                        }
                    }
                }
                //show more than 2 result when onbackpress
                else{
                    pendingRoom.clear();
                    pendingRoom.addAll(pendingAllRoom);
                    startToHostAdapter.notifyDataSetChanged();
                }
            }
            if(pendingAllRoom.size() != 0){
                waitingForVerifyGV.setVisibility(View.VISIBLE);
            }
            else{
//                                        emptyCL.setVisibility(View.VISIBLE);
                waitingForVerifyCL.setVisibility(View.GONE);
            }

            waitingForVerifyGV.setLayoutManager(new GridLayoutManager(parentActivity, 2));
            waitingForVerifyGV.setNestedScrollingEnabled(false);
            waitingForVerifyGV.setAdapter(waitingVerificationAdapter);
//                                    setWaitForVerifyAdapter(savedInstanceState);
        }
        else{
            waitingForVerifyCL.setVisibility(View.GONE);
        }
    }
    private void publishDisplayRoom(ArrayList<Room>publishedAllRoom){
        if (publishedAllRoom.size() != 0) {
            publishedRoom.clear();
            publishGV.requestLayout();
//            publishedAdapter.notifyDataSetChanged();
            publishCL.setVisibility(View.VISIBLE);

            if (publishedAllRoom.size() <= 2) {
                publishedRoom.clear();
                publish_loadMore.setVisibility(View.GONE); // Load More Button

                for (int i = 0; i < publishedAllRoom.size(); i++) {
                    if (publishedAllRoom.size() == 1) {
                        publishedRoom.add(publishedAllRoom.get(i));
                    } else {
                        if (publishedAllRoom.get(i) != null) {
                            publishedRoom.add(publishedAllRoom.get(i));
                        }
                    }
                }
            } else {
                publish_loadMore.setVisibility(View.VISIBLE); // Load More Button
                if(!publishedOnBackPress){
                    if(btnIsDown_publised){
                        publishedRoom.clear();
                        publishedRoom.addAll(publishedAllRoom);
                        publishedAdapter.notifyDataSetChanged();
                    }
                    else{
                        publishedRoom.clear();
                        for (int i = 0; i < 2; i++) {

                            if (finalRooms.get(i) != null) {
                                publishedRoom.add(publishedAllRoom.get(i));
                            }
                        }
                    }
                }
                else{
                    publishedRoom.clear();
                    publishedRoom.addAll(publishedAllRoom);
                    publishedAdapter.notifyDataSetChanged();
                }
            }
            publishedAdapter = new HostStayForManageRecyclerGridAdapter(parentActivity, publishedRoom, TYPE_PUBLISHED);
            publishGV.setLayoutManager(new GridLayoutManager(parentActivity, 2));
            publishGV.setNestedScrollingEnabled(false);
            publishedAdapter.notifyDataSetChanged();
            publishGV.setAdapter(publishedAdapter);
            publishGV.requestLayout();
//                                    setPublishedAdapter(savedInstanceState);
        } else {
            publishCL.setVisibility(View.GONE);
        }
    }

    private void unPublishDisplayRoom(ArrayList<Room> unpublishedAllRoom){
        if (unpublishedAllRoom.size() != 0) {
            unPublishCL.setVisibility(View.VISIBLE);

            if (unpublishedAllRoom.size() <= 2) {
                unPublish_loadMore.setVisibility(View.GONE); // Load More Button

                for (int i = 0; i < unpublishedAllRoom.size(); i++) {
                    if (unpublishedAllRoom.size() == 1) {
                        unpublishedRoom.add(unpublishedAllRoom.get(i));
                    } else {
                        if (unpublishedAllRoom.get(i) != null) {
                            unpublishedRoom.add(unpublishedAllRoom.get(i));
                        }
                    }
                }
            } else {
                unPublish_loadMore.setVisibility(View.VISIBLE); // Load More Button
                if(!unpublishedOnBackPress){
                    if(btnIsDown_unpublised){
                        unpublishedRoom.clear();
                        unpublishedRoom.addAll(unpublishedAllRoom);
                        unPublishedAdapter.notifyDataSetChanged();
                    }
                    else{
                        for (int i = 0; i < 2; i++) {

                            if (unpublishedAllRoom.get(i) != null) {
                                unpublishedRoom.add(unpublishedAllRoom.get(i));
                            }
                        }
                    }
                }
                else{
                    unpublishedRoom.clear();
                    unpublishedRoom.addAll(unpublishedAllRoom);
                    unPublishedAdapter.notifyDataSetChanged();
                }
            }
            unPublishGV.setLayoutManager(new GridLayoutManager(parentActivity, 2));
            unPublishGV.setNestedScrollingEnabled(false);
            unPublishGV.setAdapter(unPublishedAdapter);
//                                    setPublishedAdapter(savedInstanceState);
        } else {
            unPublishCL.setVisibility(View.GONE);
        }
    }
    private void rejectDisplayRoom(ArrayList<Room> rejectedAllRoom){
        if (rejectedAllRoom.size() != 0) {
            rejectCL.setVisibility(View.VISIBLE);

            if (rejectedAllRoom.size() <= 2) {
                rejected_loadMore.setVisibility(View.GONE); // Load More Button

                for (int i = 0; i < rejectedAllRoom.size(); i++) {
                    if (rejectedAllRoom.size() == 1) {
                        rejectedRoom.add(rejectedAllRoom.get(i));
                    } else {
                        if (rejectedAllRoom.get(i) != null) {
                            rejectedRoom.add(rejectedAllRoom.get(i));
                        }
                    }
                }
            } else {
                rejected_loadMore.setVisibility(View.VISIBLE); // Load More Button

                if(!rejectedOnBackPress){
                    if(btnIsDown_rejected){
                        rejectedRoom.clear();
                        rejectedRoom.addAll(rejectedAllRoom);
                        rejectedAdapter.notifyDataSetChanged();
                    }
                    else{
                        for (int i = 0; i < 2; i++) {

                            if (rejectedAllRoom.get(i) != null) {
                                rejectedRoom.add(rejectedAllRoom.get(i));
                            }
                        }
                    }
                }
                else{
                    rejectedRoom.clear();
                    rejectedRoom.addAll(rejectedAllRoom);
                    rejectedAdapter.notifyDataSetChanged();
                }

            }
            rejectedGV.setLayoutManager(new GridLayoutManager(parentActivity, 2));
            rejectedGV.setNestedScrollingEnabled(false);
            rejectedGV.setAdapter(rejectedAdapter);
//                                    setPublishedAdapter(savedInstanceState);
        } else {
            rejectCL.setVisibility(View.GONE);
        }
    }

    private boolean isFirstTime() {
        if (firstTime == null) {
            SharedPreferences mPreferences = parentActivity.getSharedPreferences("first_time_firestore", Context.MODE_PRIVATE);
            firstTime = mPreferences.getBoolean("firstTime_firestore", true);
            if (firstTime) {
                SharedPreferences.Editor editor = mPreferences.edit();
                editor.putBoolean("firstTime_firestore", false);
                editor.apply();
            }
        }
        else{
            firstTime = false;
        }

        return firstTime;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(listenerRegistration1 != null){
            listenerRegistration1.remove();
        }
    }
}
