package com.vidatechft.yupa.hostFragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.GalleryViewPagerAdapter;
import com.vidatechft.yupa.classes.Blog;
import com.vidatechft.yupa.classes.Room;

public class GalleryViewPagerFragment extends Fragment {
    public static final String TAG = GalleryViewPagerFragment.class.getName();
    private MainActivity parentActivity;
    private Room room;
    private Blog blog;
    private int position;

    public static GalleryViewPagerFragment newInstance(MainActivity parentActivity, String lastToolBarTitle, Room room, int position) {
        GalleryViewPagerFragment fragment = new GalleryViewPagerFragment();
        fragment.parentActivity = parentActivity;
        fragment.room = room;
        fragment.position = position;
        return fragment;
    }

    public static GalleryViewPagerFragment newInstance(MainActivity parentActivity, String lastToolBarTitle, Blog blog, int position) {
        GalleryViewPagerFragment fragment = new GalleryViewPagerFragment();
        fragment.parentActivity = parentActivity;
        fragment.blog = blog;
        fragment.position = position;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.fragment_gallery_view_pager, container, false);
        parentActivity.hideToolbar();

        if(room != null){
            ViewPager viewPager = view.findViewById(R.id.galleryViewPager);
            GalleryViewPagerAdapter galleryViewPagerAdapter = new GalleryViewPagerAdapter(parentActivity,parentActivity.getApplicationContext(),room);
            viewPager.setAdapter(galleryViewPagerAdapter);
            viewPager.setCurrentItem(position);
            viewPager.setOffscreenPageLimit(1);
        }
        if(blog != null){
            ViewPager viewPager = view.findViewById(R.id.galleryViewPager);
            GalleryViewPagerAdapter galleryViewPagerAdapter = new GalleryViewPagerAdapter(parentActivity,parentActivity.getApplicationContext(),blog);
            viewPager.setAdapter(galleryViewPagerAdapter);
            viewPager.setCurrentItem(position);
            viewPager.setOffscreenPageLimit(1);
        }

        return view;
    }

    @Override
    public void onResume() {
        parentActivity.hideToolbar();
        parentActivity.hideBtmNav();
        parentActivity.selectHotel();
        parentActivity.setToolbarTitle(parentActivity.getString(R.string.host_an_accommodation));
        super.onResume();
    }

    @Override
    public void onPause() {
        parentActivity.showToolbar();
        parentActivity.showBtmNav();
        super.onPause();
    }
}
