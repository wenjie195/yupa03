package com.vidatechft.yupa.hostFragments;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;
import com.theartofdev.edmodo.cropper.CropImage;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.Accommodation;
import com.vidatechft.yupa.classes.Host;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.utilities.FilePath;
import com.vidatechft.yupa.utilities.GeneralFunction;
import com.vidatechft.yupa.utilities.KeyboardUtilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;
import static com.vidatechft.yupa.classes.Room.ACCOMMODATION_DETIALS;
import static com.vidatechft.yupa.classes.Room.ACCOMMODATION_DETIALS_ALL;
import static com.vidatechft.yupa.classes.Room.UPLOAD_ROOM_DETIALS;
import static com.vidatechft.yupa.classes.Room.UPLOAD_ROOM_DETIALS_ALL;
import static com.vidatechft.yupa.utilities.GeneralFunction.GlideImageSetting;

public class UploadRoomPhotoFragment extends Fragment implements View.OnClickListener, View.OnTouchListener {
    public static final String TAG = UploadRoomPhotoFragment.class.getName();
    private MainActivity parentActivity;
    private View view;
    private ConstraintLayout uploadRoomPhotoWrapCL1,uploadRoomPhotoWrapCL2,uploadRoomPhotoWrapCL3,uploadRoomPhotoWrapCL4,
            uploadRoomPhotoWrapCL5,uploadRoomPhotoWrapCL6,uploadRoomPhotoWrapCL7,uploadRoomPhotoWrapCL8;
    private int imageButton = 0;
    private ImageButton upload_room_1, upload_room_2,upload_room_3,upload_room_4,upload_room_5,upload_room_6,upload_room_7,
            upload_room_8;
    private ImageButton editNameButton1, editNameButton2, editNameButton3,editNameButton4, editNameButton5, editNameButton6,
            editNameButton7,editNameButton8;
    private EditText roomName1, roomName2, roomName3, roomName4, roomName5, roomName6, roomName7, roomName8;
    private String filePath1,filePath2,filePath3,filePath4,filePath5,filePath6,filePath7,filePath8;
    private ScrollView uploadRoomPhotoSV;
    private Button btn_next_uploadRoomPhotoFragment;
    private String getroomName1,getroomName2,getroomName3,getroomName4,getroomName5,getroomName6,getroomName7,getroomName8;
    private String noAvailable = "no available";
    private String value;
    private int count;
    private String setPushId1,setPushId2,setPushId3,setPushId4,setPushId5,setPushId6,setPushId7,setPushId8,getPushId1,getPushId2,getPushId3,
            getPushId4,getPushId5,getPushId6,getPushId7,getPushId8;
    private DocumentReference ref1,ref2,ref3,ref4,ref5,ref6,ref7,ref8;
    private int goToUploadAccommodationFragment;
    private boolean checkRoomName = false;
    private Boolean room1,room2,room3,room4,room5,room6,room7,room8;
    private List<String>roomUrl = new ArrayList<>();
    private List<String>roomName = new ArrayList<>();
    private List<ImageButton> imageViews = new ArrayList<>();
    private List<String> filepaths = new ArrayList<>();
    private List<String> filepathString = new ArrayList<>();
    private List<EditText> editTexts = new ArrayList<>();
    private int bedroomQT;
    private int countBedroomQT = 0;
    private boolean isFromDatabase = false;
    private String documentPushId;
    private List<String>roomPicPushIdFromFirestore = new ArrayList<>();
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private boolean isSubmit = false;
    private int bedroomQTY;
    private boolean room1Click,room2Click,room3Click,room4Click,room5Click,room6Click,room7Click,room8Click;
    private List<Boolean> roomClicks = new ArrayList<>();
    private List<Integer> positions = new ArrayList<>();
    private boolean isNewPushId1,isNewPushId2,isNewPushId3,isNewPushId4,isNewPushId5,isNewPushId6,isNewPushId7,isNewPushId8 = false;
    private List<Boolean> isNewPushIds = new ArrayList<>();
    private boolean isGoCropPhoto = false;
    private int isOnbackPressed = 0;
    private boolean isEditProfile = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static UploadRoomPhotoFragment newInstance(MainActivity parentActivity) {
        UploadRoomPhotoFragment fragment = new UploadRoomPhotoFragment();
        fragment.parentActivity = parentActivity;
        return fragment;
    }

    public static UploadRoomPhotoFragment newInstance(MainActivity parentActivity, List<String>roomUrl, List<String>roomName, boolean isFromDatabase, String documentPushId, List<String> roomPicPushIdFromFirestore, int bedroomQTY, boolean isEditProfile) {
        UploadRoomPhotoFragment fragment = new UploadRoomPhotoFragment();
        fragment.parentActivity = parentActivity;
        fragment.roomUrl = roomUrl;
        fragment.roomName = roomName;
        fragment.isFromDatabase = isFromDatabase;
        fragment.documentPushId = documentPushId;
        fragment.roomPicPushIdFromFirestore = roomPicPushIdFromFirestore;
        fragment.bedroomQTY = bedroomQTY;
        fragment.isEditProfile = isEditProfile;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_upload_room_photo, container, false);

        //initiate the view
        initiateView(view);

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode  == RESULT_OK) {
            if (requestCode == 158) {
                if (data == null) {
                    //no data present
                    return;
                }
            }
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    Uri resultUri = result.getUri();
                    if(imageButton == 1){
                            this.filePath1 = FilePath.getPath(parentActivity, resultUri);
                            GlideImageSetting(parentActivity, resultUri,upload_room_1);
                    }

                    if(imageButton == 2){
                            this.filePath2 = FilePath.getPath(parentActivity, resultUri);
                            GlideImageSetting(parentActivity, resultUri,upload_room_2);
                    }

                    if(imageButton == 3){
                            this.filePath3 = FilePath.getPath(parentActivity, resultUri);
                            GlideImageSetting(parentActivity, resultUri,upload_room_3);
                    }

                    if(imageButton == 4){
                            this.filePath4 = FilePath.getPath(parentActivity, resultUri);
                            GlideImageSetting(parentActivity, resultUri,upload_room_4);
                    }

                    if(imageButton == 5){
                            this.filePath5 = FilePath.getPath(parentActivity, resultUri);
                            GlideImageSetting(parentActivity, resultUri,upload_room_5);
                    }

                    if(imageButton == 6){
                            this.filePath6 = FilePath.getPath(parentActivity, resultUri);
                            GlideImageSetting(parentActivity, resultUri,upload_room_6);
                    }

                    if(imageButton == 7){
                            this.filePath7 = FilePath.getPath(parentActivity, resultUri);
                            GlideImageSetting(parentActivity, resultUri,upload_room_7);
                    }

                    if(imageButton == 8){
                            this.filePath8 = FilePath.getPath(parentActivity, resultUri);
                            GlideImageSetting(parentActivity, resultUri,upload_room_8);
                    }

                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Exception error = result.getError();
                }
            }

            if (requestCode == CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE) {
                if (ContextCompat.checkSelfPermission(parentActivity, Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_DENIED) {
                    ActivityCompat.requestPermissions(parentActivity, new String[]{Manifest.permission.CAMERA}, requestCode);

                }
            }
        }
    }

    private void getBedroomQuantityFromFirestore(){
        if(roomUrl != null){
//            bedroomQT = countBedroomQT;
            if (bedroomQTY != 0){

                if(bedroomQTY >=8){
                    uploadRoomPhotoWrapCL1.setVisibility(View.VISIBLE);
                    uploadRoomPhotoWrapCL2.setVisibility(View.VISIBLE);
                    uploadRoomPhotoWrapCL3.setVisibility(View.VISIBLE);
                    uploadRoomPhotoWrapCL4.setVisibility(View.VISIBLE);
                    uploadRoomPhotoWrapCL5.setVisibility(View.VISIBLE);
                    uploadRoomPhotoWrapCL6.setVisibility(View.VISIBLE);
                    uploadRoomPhotoWrapCL7.setVisibility(View.VISIBLE);
                    uploadRoomPhotoWrapCL8.setVisibility(View.VISIBLE);
                }
                else if(bedroomQTY >=7){
                    uploadRoomPhotoWrapCL1.setVisibility(View.VISIBLE);
                    uploadRoomPhotoWrapCL2.setVisibility(View.VISIBLE);
                    uploadRoomPhotoWrapCL3.setVisibility(View.VISIBLE);
                    uploadRoomPhotoWrapCL4.setVisibility(View.VISIBLE);
                    uploadRoomPhotoWrapCL5.setVisibility(View.VISIBLE);
                    uploadRoomPhotoWrapCL6.setVisibility(View.VISIBLE);
                    uploadRoomPhotoWrapCL7.setVisibility(View.VISIBLE);
                }
                else if(bedroomQTY >=6){
                    uploadRoomPhotoWrapCL1.setVisibility(View.VISIBLE);
                    uploadRoomPhotoWrapCL2.setVisibility(View.VISIBLE);
                    uploadRoomPhotoWrapCL3.setVisibility(View.VISIBLE);
                    uploadRoomPhotoWrapCL4.setVisibility(View.VISIBLE);
                    uploadRoomPhotoWrapCL5.setVisibility(View.VISIBLE);
                    uploadRoomPhotoWrapCL6.setVisibility(View.VISIBLE);
                }
                else if(bedroomQTY >=5){
                    uploadRoomPhotoWrapCL1.setVisibility(View.VISIBLE);
                    uploadRoomPhotoWrapCL2.setVisibility(View.VISIBLE);
                    uploadRoomPhotoWrapCL3.setVisibility(View.VISIBLE);
                    uploadRoomPhotoWrapCL4.setVisibility(View.VISIBLE);
                    uploadRoomPhotoWrapCL5.setVisibility(View.VISIBLE);
                }
                else if(bedroomQTY >=4){
                    uploadRoomPhotoWrapCL1.setVisibility(View.VISIBLE);
                    uploadRoomPhotoWrapCL2.setVisibility(View.VISIBLE);
                    uploadRoomPhotoWrapCL3.setVisibility(View.VISIBLE);
                    uploadRoomPhotoWrapCL4.setVisibility(View.VISIBLE);
                }
                else if(bedroomQTY >=3){
                    uploadRoomPhotoWrapCL1.setVisibility(View.VISIBLE);
                    uploadRoomPhotoWrapCL2.setVisibility(View.VISIBLE);
                    uploadRoomPhotoWrapCL3.setVisibility(View.VISIBLE);
                }
                else if(bedroomQTY >=2){
                    uploadRoomPhotoWrapCL1.setVisibility(View.VISIBLE);
                    uploadRoomPhotoWrapCL2.setVisibility(View.VISIBLE);
                }
                else{
                    uploadRoomPhotoWrapCL1.setVisibility(View.VISIBLE);
                }
            }
            else{
                Toast.makeText(parentActivity, "no data save", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void getBedroomQuantityFromJson(){
        //https://www.dev2qa.com/android-sharedpreferences-save-load-java-object-example/
            try{
                SharedPreferences sharedPreferences = parentActivity.getSharedPreferences(ACCOMMODATION_DETIALS, MODE_PRIVATE);

                // Get saved string data in it.
                String userInfoListJsonString = sharedPreferences.getString(ACCOMMODATION_DETIALS_ALL, "");

                // Create Gson object and translate the json string to related java object array.
                Gson gson = new Gson();

                Room userInfoDtoArray = gson.fromJson(userInfoListJsonString, Room.class);

                // Loop the UserInfoDTO array and print each UserInfoDTO data in android monitor as debug log.
                // Get each user info in dto.
                Room userInfoDto = userInfoDtoArray;
                //for append the word use stringbuffer
                StringBuffer userInfoBuf = new StringBuffer();
                userInfoBuf.append("BedroomQuantity : ");
                userInfoBuf.append(userInfoDto.bedroomQuantity);

                // Print debug log in android monitor console.Debug info tag is USER_INFO_LIST_TAG.
                Log.d(ACCOMMODATION_DETIALS_ALL, userInfoBuf.toString());

                bedroomQT = userInfoDto.bedroomQuantity;

                if (bedroomQT != 0){
                    if(bedroomQT>=8){
                        uploadRoomPhotoWrapCL1.setVisibility(View.VISIBLE);
                        uploadRoomPhotoWrapCL2.setVisibility(View.VISIBLE);
                        uploadRoomPhotoWrapCL3.setVisibility(View.VISIBLE);
                        uploadRoomPhotoWrapCL4.setVisibility(View.VISIBLE);
                        uploadRoomPhotoWrapCL5.setVisibility(View.VISIBLE);
                        uploadRoomPhotoWrapCL6.setVisibility(View.VISIBLE);
                        uploadRoomPhotoWrapCL7.setVisibility(View.VISIBLE);
                        uploadRoomPhotoWrapCL8.setVisibility(View.VISIBLE);
                    }
                    else if(bedroomQT>=7){
                        uploadRoomPhotoWrapCL1.setVisibility(View.VISIBLE);
                        uploadRoomPhotoWrapCL2.setVisibility(View.VISIBLE);
                        uploadRoomPhotoWrapCL3.setVisibility(View.VISIBLE);
                        uploadRoomPhotoWrapCL4.setVisibility(View.VISIBLE);
                        uploadRoomPhotoWrapCL5.setVisibility(View.VISIBLE);
                        uploadRoomPhotoWrapCL6.setVisibility(View.VISIBLE);
                        uploadRoomPhotoWrapCL7.setVisibility(View.VISIBLE);
                    }
                    else if(bedroomQT>=6){
                        uploadRoomPhotoWrapCL1.setVisibility(View.VISIBLE);
                        uploadRoomPhotoWrapCL2.setVisibility(View.VISIBLE);
                        uploadRoomPhotoWrapCL3.setVisibility(View.VISIBLE);
                        uploadRoomPhotoWrapCL4.setVisibility(View.VISIBLE);
                        uploadRoomPhotoWrapCL5.setVisibility(View.VISIBLE);
                        uploadRoomPhotoWrapCL6.setVisibility(View.VISIBLE);
                    }
                    else if(bedroomQT>=5){
                        uploadRoomPhotoWrapCL1.setVisibility(View.VISIBLE);
                        uploadRoomPhotoWrapCL2.setVisibility(View.VISIBLE);
                        uploadRoomPhotoWrapCL3.setVisibility(View.VISIBLE);
                        uploadRoomPhotoWrapCL4.setVisibility(View.VISIBLE);
                        uploadRoomPhotoWrapCL5.setVisibility(View.VISIBLE);
                    }
                    else if(bedroomQT>=4){
                        uploadRoomPhotoWrapCL1.setVisibility(View.VISIBLE);
                        uploadRoomPhotoWrapCL2.setVisibility(View.VISIBLE);
                        uploadRoomPhotoWrapCL3.setVisibility(View.VISIBLE);
                        uploadRoomPhotoWrapCL4.setVisibility(View.VISIBLE);
                    }
                    else if(bedroomQT>=3){
                        uploadRoomPhotoWrapCL1.setVisibility(View.VISIBLE);
                        uploadRoomPhotoWrapCL2.setVisibility(View.VISIBLE);
                        uploadRoomPhotoWrapCL3.setVisibility(View.VISIBLE);
                    }
                    else if(bedroomQT>=2){
                        uploadRoomPhotoWrapCL1.setVisibility(View.VISIBLE);
                        uploadRoomPhotoWrapCL2.setVisibility(View.VISIBLE);
                    }
                    else{
                        uploadRoomPhotoWrapCL1.setVisibility(View.VISIBLE);
                    }
                }
                else{
                    Toast.makeText(parentActivity, "no data save", Toast.LENGTH_SHORT).show();
                }
            }
            catch (Exception e){
                Toast.makeText(parentActivity, e.toString(), Toast.LENGTH_SHORT).show();
                Log.e("", e.toString());
            }
        }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.upload_room_1){
            room1Click = true;
            isGoCropPhoto = true;
            if(GeneralFunction.isStorageAndCameraPermitted(parentActivity.getApplicationContext(),parentActivity)) {
                GeneralFunction.showFileChooser(parentActivity,this);
                imageButton = 1;
            }
            KeyboardUtilities.hideSoftKeyboard(parentActivity);
        }
        if(v.getId() == R.id.upload_room_2){
            room2Click = true;
            isGoCropPhoto = true;
            if(GeneralFunction.isStorageAndCameraPermitted(parentActivity.getApplicationContext(),parentActivity)) {
                GeneralFunction.showFileChooser(parentActivity,this);
                imageButton = 2;
            }
            KeyboardUtilities.hideSoftKeyboard(parentActivity);
        }
        if(v.getId() == R.id.upload_room_3){
            room3Click = true;
            isGoCropPhoto = true;
            if(GeneralFunction.isStorageAndCameraPermitted(parentActivity.getApplicationContext(),parentActivity)) {
                GeneralFunction.showFileChooser(parentActivity,this);
                imageButton = 3;
            }
            KeyboardUtilities.hideSoftKeyboard(parentActivity);
        }
        if(v.getId() == R.id.upload_room_4){
            room4Click = true;
            isGoCropPhoto = true;
            if(GeneralFunction.isStorageAndCameraPermitted(parentActivity.getApplicationContext(),parentActivity)) {
                GeneralFunction.showFileChooser(parentActivity,this);
                imageButton = 4;
            }
            KeyboardUtilities.hideSoftKeyboard(parentActivity);
        }
        if(v.getId() == R.id.upload_room_5){
            room5Click = true;
            isGoCropPhoto = true;
            if(GeneralFunction.isStorageAndCameraPermitted(parentActivity.getApplicationContext(),parentActivity)) {
                GeneralFunction.showFileChooser(parentActivity,this);
                imageButton = 5;
            }
            KeyboardUtilities.hideSoftKeyboard(parentActivity);
        }
        if(v.getId() == R.id.upload_room_6){
            room6Click = true;
            isGoCropPhoto = true;
            if(GeneralFunction.isStorageAndCameraPermitted(parentActivity.getApplicationContext(),parentActivity)) {
                GeneralFunction.showFileChooser(parentActivity,this);
                imageButton = 6;
            }
            KeyboardUtilities.hideSoftKeyboard(parentActivity);
        }
        if(v.getId() == R.id.upload_room_7){
            room7Click = true;
            isGoCropPhoto = true;
            if(GeneralFunction.isStorageAndCameraPermitted(parentActivity.getApplicationContext(),parentActivity)) {
                GeneralFunction.showFileChooser(parentActivity,this);
                imageButton = 7;
            }
            KeyboardUtilities.hideSoftKeyboard(parentActivity);
        }
        if(v.getId() == R.id.upload_room_8){
            room8Click = true;
            isGoCropPhoto = true;
            if(GeneralFunction.isStorageAndCameraPermitted(parentActivity.getApplicationContext(),parentActivity)) {
                GeneralFunction.showFileChooser(parentActivity,this);
                imageButton = 8;
            }
            KeyboardUtilities.hideSoftKeyboard(parentActivity);
        }

        if(v.getId() == R.id.editNameButton1){
            GeneralFunction.editRoomNameFunc(roomName1,parentActivity);
        }

        if(v.getId() == R.id.editNameButton2){
            GeneralFunction.editRoomNameFunc(roomName2,parentActivity);
        }

        if(v.getId() == R.id.editNameButton3){
            GeneralFunction.editRoomNameFunc(roomName3,parentActivity);
        }

        if(v.getId() == R.id.editNameButton4){
            GeneralFunction.editRoomNameFunc(roomName4,parentActivity);
        }

        if(v.getId() == R.id.editNameButton5){
            GeneralFunction.editRoomNameFunc(roomName5,parentActivity);
        }

        if(v.getId() == R.id.editNameButton6){
            GeneralFunction.editRoomNameFunc(roomName6,parentActivity);
        }

        if(v.getId() == R.id.editNameButton7){
            GeneralFunction.editRoomNameFunc(roomName7,parentActivity);
        }

        if(v.getId() == R.id.editNameButton8){
            GeneralFunction.editRoomNameFunc(roomName8,parentActivity);
        }

        if(v.getId() == R.id.btn_next_uploadRoomPhotoFragment){
            isSubmit =true;
            goToUploadAccommodationFragment = 0;
            submitUploadRoomDetails();
        }
    }

    private void initiateView (View view){

        //button
        btn_next_uploadRoomPhotoFragment = view.findViewById(R.id.btn_next_uploadRoomPhotoFragment);

        //scrollView layout
        uploadRoomPhotoSV = view.findViewById(R.id.uploadRoomPhotoSV);

        //constraint layout
        uploadRoomPhotoWrapCL1 = view.findViewById(R.id.uploadRoomPhotoWrapCL1);
        uploadRoomPhotoWrapCL2 = view.findViewById(R.id.uploadRoomPhotoWrapCL2);
        uploadRoomPhotoWrapCL3 = view.findViewById(R.id.uploadRoomPhotoWrapCL3);
        uploadRoomPhotoWrapCL4 = view.findViewById(R.id.uploadRoomPhotoWrapCL4);
        uploadRoomPhotoWrapCL5 = view.findViewById(R.id.uploadRoomPhotoWrapCL5);
        uploadRoomPhotoWrapCL6 = view.findViewById(R.id.uploadRoomPhotoWrapCL6);
        uploadRoomPhotoWrapCL7 = view.findViewById(R.id.uploadRoomPhotoWrapCL7);
        uploadRoomPhotoWrapCL8 = view.findViewById(R.id.uploadRoomPhotoWrapCL8);

        //ImageButton
        upload_room_1 = view.findViewById(R.id.upload_room_1);
        upload_room_2 = view.findViewById(R.id.upload_room_2);
        upload_room_3 = view.findViewById(R.id.upload_room_3);
        upload_room_4 = view.findViewById(R.id.upload_room_4);
        upload_room_5 = view.findViewById(R.id.upload_room_5);
        upload_room_6 = view.findViewById(R.id.upload_room_6);
        upload_room_7 = view.findViewById(R.id.upload_room_7);
        upload_room_8 = view.findViewById(R.id.upload_room_8);

        editNameButton1 = view.findViewById(R.id.editNameButton1);
        editNameButton2 = view.findViewById(R.id.editNameButton2);
        editNameButton3 = view.findViewById(R.id.editNameButton3);
        editNameButton4 = view.findViewById(R.id.editNameButton4);
        editNameButton5 = view.findViewById(R.id.editNameButton5);
        editNameButton6 = view.findViewById(R.id.editNameButton6);
        editNameButton7 = view.findViewById(R.id.editNameButton7);
        editNameButton8 = view.findViewById(R.id.editNameButton8);

        //EditText
        roomName1 = view.findViewById(R.id.roomName1);
        roomName2 = view.findViewById(R.id.roomName2);
        roomName3 = view.findViewById(R.id.roomName3);
        roomName4 = view.findViewById(R.id.roomName4);
        roomName5 = view.findViewById(R.id.roomName5);
        roomName6 = view.findViewById(R.id.roomName6);
        roomName7 = view.findViewById(R.id.roomName7);
        roomName8 = view.findViewById(R.id.roomName8);

        //onClickListener
        upload_room_1.setOnClickListener(this);
        upload_room_2.setOnClickListener(this);
        upload_room_3.setOnClickListener(this);
        upload_room_4.setOnClickListener(this);
        upload_room_5.setOnClickListener(this);
        upload_room_6.setOnClickListener(this);
        upload_room_7.setOnClickListener(this);
        upload_room_8.setOnClickListener(this);
        btn_next_uploadRoomPhotoFragment.setOnClickListener(this);

        editNameButton1.setOnClickListener(this);
        editNameButton2.setOnClickListener(this);
        editNameButton3.setOnClickListener(this);
        editNameButton4.setOnClickListener(this);
        editNameButton5.setOnClickListener(this);
        editNameButton6.setOnClickListener(this);
        editNameButton7.setOnClickListener(this);
        editNameButton8.setOnClickListener(this);

        //onTouchListener
        uploadRoomPhotoSV.setOnTouchListener(this);

        room1 = null;
        room2 = null;
        room3 = null;
        room4 = null;
        room5 = null;
        room6 = null;
        room7 = null;
        room8 = null;

        roomClicks.add(room1Click);
        roomClicks.add(room2Click);
        roomClicks.add(room3Click);
        roomClicks.add(room4Click);
        roomClicks.add(room5Click);
        roomClicks.add(room6Click);
        roomClicks.add(room7Click);
        roomClicks.add(room8Click);

        for(int i = 0; i<roomClicks.size(); i++){
            roomClicks.set(i, false);
        }
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if(v.getId() == R.id.uploadRoomPhotoSV){
            KeyboardUtilities.hideSoftKeyboard(parentActivity);

        }
        return false;
    }

    private void submitUploadRoomDetails(){

            if(uploadRoomPhotoWrapCL1.getVisibility() != View.GONE ){
                if(filePath1 == null){
                    if(!isFromDatabase) {
                        GeneralFunction.checkAndReturnString(filePath1, parentActivity.getString(R.string.error_please_upload_a_photo_room1), parentActivity);
                    }
                }
                 if(roomName1.getText().toString().trim().equals("")){
                    GeneralFunction.openMessageNotificationDialog(parentActivity,"Please enter room name 1",R.drawable.notice_bad,"invalid_string");
                    checkRoomName = false;
                    return;
                }
                else{
                    getroomName1 = roomName1.getText().toString();
                }
            }
            else{
                filePath1 = noAvailable;
                getroomName1 = noAvailable;
            }

            if(uploadRoomPhotoWrapCL2.getVisibility() != View.GONE ){
                if(filePath2 == null){
                    if(!isFromDatabase){
                        GeneralFunction.checkAndReturnString(filePath2,parentActivity.getString(R.string.error_please_upload_a_photo_room2),parentActivity);
                    }
                }
                if(roomName2.getText().toString().trim().equals("")){
                    GeneralFunction.openMessageNotificationDialog(parentActivity,"Please enter room name 2",R.drawable.notice_bad,"invalid_string");
                    checkRoomName = false;
                    return;
                }
                else{
                    getroomName2 = roomName2.getText().toString();
                }
            }
            else{
                filePath2 = noAvailable;
                getroomName2 = noAvailable;
            }

            if(uploadRoomPhotoWrapCL3.getVisibility() != View.GONE){
                if(filePath3 == null){
                        GeneralFunction.checkAndReturnString(filePath3,parentActivity.getString(R.string.error_please_upload_a_photo_room3),parentActivity);

                }
                else if(roomName3.getText().toString().trim().equals("")){
                    GeneralFunction.openMessageNotificationDialog(parentActivity,"Please enter room name 3",R.drawable.notice_bad,"invalid_string");
                    checkRoomName = false;
                    return;
                }
                else{
                    getroomName3 = roomName3.getText().toString();
                }
            }
            else{
                filePath3 = noAvailable;
                getroomName3 = noAvailable;
            }

            if(uploadRoomPhotoWrapCL4.getVisibility() != View.GONE){
                if(filePath4 == null){
                    if(!isFromDatabase){
                        GeneralFunction.checkAndReturnString(filePath4,parentActivity.getString(R.string.error_please_upload_a_photo_room4),parentActivity);
                    }
                }
                if(roomName4.getText().toString().trim().equals("")){
                    GeneralFunction.openMessageNotificationDialog(parentActivity,"Please enter room name 4",R.drawable.notice_bad,"invalid_string");
                    checkRoomName = false;
                    return;
                }
                else{
                    getroomName4 = roomName4.getText().toString();
                }
            }
            else{
                filePath4 = noAvailable;
                getroomName4 = noAvailable;
            }

            if(uploadRoomPhotoWrapCL5.getVisibility() != View.GONE){
                if(filePath5 == null ){
                    if(!isFromDatabase){
                        GeneralFunction.checkAndReturnString(filePath5,parentActivity.getString(R.string.error_please_upload_a_photo_room5),parentActivity);
                    }
                }
                if(roomName5.getText().toString().trim().equals("")){
                    GeneralFunction.openMessageNotificationDialog(parentActivity,"Please enter room name 5",R.drawable.notice_bad,"invalid_string");                    checkRoomName = false;
                    return;
                }
                else{
                    getroomName5 = roomName5.getText().toString();
                }
            }
            else{
                filePath5 = noAvailable;
                getroomName5 = noAvailable;
            }

            if(uploadRoomPhotoWrapCL6.getVisibility() != View.GONE){
                if(filePath6 == null ){
                    if(!isFromDatabase){
                        GeneralFunction.checkAndReturnString(filePath6,parentActivity.getString(R.string.error_please_upload_a_photo_room6),parentActivity);
                    }
                }
                if(roomName6.getText().toString().trim().equals("")){
                    GeneralFunction.openMessageNotificationDialog(parentActivity,"Please enter room name 6",R.drawable.notice_bad,"invalid_string");
                    checkRoomName = false;
                    return;
                }
                else{
                    getroomName6 = roomName6.getText().toString();
                }
            }
            else{
                filePath6 = noAvailable;
                getroomName6 = noAvailable;
            }

            if(uploadRoomPhotoWrapCL7.getVisibility() != View.GONE){
                if(filePath7 == null){
                    if(!isFromDatabase){
                        GeneralFunction.checkAndReturnString(filePath7,parentActivity.getString(R.string.error_please_upload_a_photo_room7),parentActivity);
                    }
                }
                if(roomName7.getText().toString().trim().equals("")){
                    GeneralFunction.openMessageNotificationDialog(parentActivity,"Please enter room name 7",R.drawable.notice_bad,"invalid_string");
                    checkRoomName = false;
                    return;
                }
                else{
                    getroomName7 = roomName7.getText().toString();
                }

            }
            else{
                filePath7 = noAvailable;
                getroomName7 = noAvailable;
            }
            if(uploadRoomPhotoWrapCL8.getVisibility() != View.GONE){
                if(filePath8 == null){
                    if(!isFromDatabase){
                        GeneralFunction.checkAndReturnString(filePath8,parentActivity.getString(R.string.error_please_upload_a_photo_room8),parentActivity);
                    }
                }
                if(roomName8.getText().toString().trim().equals("")){
                    GeneralFunction.openMessageNotificationDialog(parentActivity,"Please enter room name 8",R.drawable.notice_bad,"invalid_string");
                    checkRoomName = false;
                    return;
                }
                else{
                    getroomName8 = roomName8.getText().toString();
                }

            }
            else{
                filePath8 = noAvailable;
                getroomName8 = noAvailable;
            }

            if(!checkRoomName){
                if(isValidInput(getroomName1,getroomName2,getroomName3,getroomName4,getroomName5,getroomName6,getroomName7,
                        getroomName8)){
                    if(filePath1!=null && filePath2!=null &&filePath3!=null && filePath4!=null && filePath5!=null && filePath6!=null &&
                            filePath7!=null && filePath8!=null){

                        Room room = new Room(filePath1,filePath2,filePath3,filePath4,filePath5,filePath6,filePath7,filePath8,
                                getroomName1,getroomName2,getroomName3,getroomName4,getroomName5,getroomName6,getroomName7,getroomName8);
                        if(!isFromDatabase){
                            GeneralFunction.saveHostSharedPreferences(parentActivity, room, Room.UPLOAD_ROOM_DETIALS_ALL,Room.UPLOAD_ROOM_DETIALS);
                        }
                        Log.d("successful", room.toString());
                        parentActivity.initializeProgressIndicator(parentActivity.getString(R.string.host_upload_room),false,this);
                        parentActivity.controlProgressIndicator(true,this);
                        uploadRoomPicToFirestore(getroomName1,getroomName2,getroomName3,getroomName4,getroomName5,getroomName6,getroomName7,
                                getroomName8);
                    }
                }
                else {
                    Log.d("failed", "Failed Save to Shared Preferences");
                }
            }





    }

    private boolean isValidInput(String roomName1, String roomName2, String roomName3,String roomName4, String roomName5,String roomName6
            ,String roomName7, String roomName8){
        return  GeneralFunction.checkAndReturnString(roomName1,parentActivity.getString(R.string.error_please_enter_room_name),parentActivity)
                ||
                GeneralFunction.checkAndReturnString(roomName2,parentActivity.getString(R.string.error_please_enter_room_name),parentActivity)
                ||
                GeneralFunction.checkAndReturnString(roomName3,parentActivity.getString(R.string.error_please_enter_room_name),parentActivity)
                ||
                GeneralFunction.checkAndReturnString(roomName4,parentActivity.getString(R.string.error_please_enter_room_name),parentActivity)
                ||
                GeneralFunction.checkAndReturnString(roomName5,parentActivity.getString(R.string.error_please_enter_room_name),parentActivity)
                ||
                GeneralFunction.checkAndReturnString(roomName6,parentActivity.getString(R.string.error_please_enter_room_name),parentActivity)
                ||
                GeneralFunction.checkAndReturnString(roomName7,parentActivity.getString(R.string.error_please_enter_room_name),parentActivity)
                ||
                GeneralFunction.checkAndReturnString(roomName8,parentActivity.getString(R.string.error_please_enter_room_name),parentActivity);

    }
    private void goToUploadAccommodationPhotoFragement() {
//        goToUploadAccommodationFragment = 0;
//        count = 0;
        isGoCropPhoto = false;
        isSubmit = false;
        if(goToUploadAccommodationFragment!=0){
            bedroomQT = 0;
            countBedroomQT = 0;
            UploadAccommodationPhotoFragement uploadAccommodationPhotoFragement = UploadAccommodationPhotoFragement.newInstance(parentActivity, isFromDatabase,documentPushId, isEditProfile);
            GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, uploadAccommodationPhotoFragement).addToBackStack(UploadAccommodationPhotoFragement.TAG).commit();
            showMessageNotificationDialog();

        }
    }

    private void uploadRoomPicToFirestore(String getroomName1,String getroomName2,String getroomName3,String getroomName4,String getroomName5,String getroomName6,String getroomName7,
                                          String getroomName8){

        if(!isFromDatabase){
            value = GeneralFunction.getDocumentPath(parentActivity,Room.ACCOMMODATION_DETIALS,Room.DIR_DOCUMENT, "");
            //get the share preference from the shared preferences
            getPushId1 = GeneralFunction.getDocumentPath(parentActivity,Room.ACCOMMODATION_DETIALS,Room.DIR_ROOM_1,"");
            getPushId2 = GeneralFunction.getDocumentPath(parentActivity,Room.ACCOMMODATION_DETIALS,Room.DIR_ROOM_2,"");
            getPushId3 = GeneralFunction.getDocumentPath(parentActivity,Room.ACCOMMODATION_DETIALS,Room.DIR_ROOM_3,"");
            getPushId4 = GeneralFunction.getDocumentPath(parentActivity,Room.ACCOMMODATION_DETIALS,Room.DIR_ROOM_4,"");
            getPushId5 = GeneralFunction.getDocumentPath(parentActivity,Room.ACCOMMODATION_DETIALS,Room.DIR_ROOM_5,"");
            getPushId6 = GeneralFunction.getDocumentPath(parentActivity,Room.ACCOMMODATION_DETIALS,Room.DIR_ROOM_6,"");
            getPushId7 = GeneralFunction.getDocumentPath(parentActivity,Room.ACCOMMODATION_DETIALS,Room.DIR_ROOM_7,"");
            getPushId8 = GeneralFunction.getDocumentPath(parentActivity,Room.ACCOMMODATION_DETIALS,Room.DIR_ROOM_8,"");
        }

        //Add a new document with a generated ID
        if(getPushId1 != null && !getPushId1.trim().equals("")){
            isNewPushIds.add(isNewPushId1);
            ref1 = GeneralFunction.getFirestoreInstance()
                    .collection(Accommodation.URL_ACCOMMODATION)
                    .document(value)
                    .collection(Room.URL_ROOM)
                    .document(getPushId1);
        }
        else{
            if(!isFromDatabase){
                ref1 = GeneralFunction.getFirestoreInstance()
                        .collection(Accommodation.URL_ACCOMMODATION)
                        .document(value)
                        .collection(Room.URL_ROOM)
                        .document();
                setPushId1 = ref1.getId();
                GeneralFunction.saveDocumentPath(parentActivity,Room.ACCOMMODATION_DETIALS,Room.DIR_ROOM_1,setPushId1);
            }
            else{
                if(uploadRoomPhotoWrapCL1.getVisibility() == View.VISIBLE){
                    isNewPushId1 = true;
                    isNewPushIds.add(isNewPushId1);
                    ref1 = GeneralFunction.getFirestoreInstance()
                            .collection(Accommodation.URL_ACCOMMODATION)
                            .document(value)
                            .collection(Room.URL_ROOM)
                            .document();
                }
            }
        }

        if(getPushId2 != null && !getPushId2.trim().equals("")){
            isNewPushIds.add(isNewPushId2);
            ref2 =  GeneralFunction.getFirestoreInstance()
                    .collection(Accommodation.URL_ACCOMMODATION)
                    .document(value)
                    .collection(Room.URL_ROOM)
                    .document(getPushId2);
        }
        else{
            if(!isFromDatabase){
                ref2 = GeneralFunction.getFirestoreInstance()
                        .collection(Accommodation.URL_ACCOMMODATION)
                        .document(value)
                        .collection(Room.URL_ROOM)
                        .document();
                setPushId2 = ref2.getId();
                GeneralFunction.saveDocumentPath(parentActivity,Room.ACCOMMODATION_DETIALS,Room.DIR_ROOM_2,setPushId2);
            }
            else{
                if(uploadRoomPhotoWrapCL2.getVisibility() == View.VISIBLE) {
                    isNewPushId2 = true;
                    isNewPushIds.add(isNewPushId2);
                    ref2 = GeneralFunction.getFirestoreInstance()
                            .collection(Accommodation.URL_ACCOMMODATION)
                            .document(value)
                            .collection(Room.URL_ROOM)
                            .document();
                }
            }
        }

        if(getPushId3 != null && !getPushId3.trim().equals("")){
            isNewPushIds.add(isNewPushId3);
            ref3 =  GeneralFunction.getFirestoreInstance()
                    .collection(Accommodation.URL_ACCOMMODATION)
                    .document(value)
                    .collection(Room.URL_ROOM)
                    .document(getPushId3);
        }
        else{
            if(!isFromDatabase){
                ref3 = GeneralFunction.getFirestoreInstance()
                        .collection(Accommodation.URL_ACCOMMODATION)
                        .document(value)
                        .collection(Room.URL_ROOM)
                        .document();
                setPushId3 = ref3.getId();
                GeneralFunction.saveDocumentPath(parentActivity,Room.ACCOMMODATION_DETIALS,Room.DIR_ROOM_3,setPushId3);
            }
            else{
                if(uploadRoomPhotoWrapCL3.getVisibility() == View.VISIBLE) {
                    isNewPushId3 = true;
                    isNewPushIds.add(isNewPushId3);
                    ref3 = GeneralFunction.getFirestoreInstance()
                            .collection(Accommodation.URL_ACCOMMODATION)
                            .document(value)
                            .collection(Room.URL_ROOM)
                            .document();
                }
            }
        }

        if(getPushId4 != null && !getPushId4.trim().equals("")){
            isNewPushIds.add(isNewPushId4);
            ref4 =  GeneralFunction.getFirestoreInstance()
                    .collection(Accommodation.URL_ACCOMMODATION)
                    .document(value)
                    .collection(Room.URL_ROOM)
                    .document(getPushId4);
        }

        else{
            if(!isFromDatabase){
                ref4 = GeneralFunction.getFirestoreInstance()
                        .collection(Accommodation.URL_ACCOMMODATION)
                        .document(value)
                        .collection(Room.URL_ROOM)
                        .document();
                setPushId4 = ref4.getId();
                GeneralFunction.saveDocumentPath(parentActivity,Room.ACCOMMODATION_DETIALS,Room.DIR_ROOM_4,setPushId4);
            }
            else{
                if(uploadRoomPhotoWrapCL4.getVisibility() == View.VISIBLE) {
                    isNewPushId4 = true;
                    isNewPushIds.add(isNewPushId4);
                    ref4 = GeneralFunction.getFirestoreInstance()
                            .collection(Accommodation.URL_ACCOMMODATION)
                            .document(value)
                            .collection(Room.URL_ROOM)
                            .document();
                }
            }
        }

        if(getPushId5 != null && !getPushId5.trim().equals("")){
            isNewPushIds.add(isNewPushId5);
            ref5 = GeneralFunction.getFirestoreInstance()
                    .collection(Accommodation.URL_ACCOMMODATION)
                    .document(value)
                    .collection(Room.URL_ROOM)
                    .document(getPushId5);
        }
        else{
            if(!isFromDatabase){
                ref5 = GeneralFunction.getFirestoreInstance()
                        .collection(Accommodation.URL_ACCOMMODATION)
                        .document(value)
                        .collection(Room.URL_ROOM)
                        .document();
                setPushId5 = ref5.getId();
                GeneralFunction.saveDocumentPath(parentActivity,Room.ACCOMMODATION_DETIALS,Room.DIR_ROOM_5,setPushId5);
            }
            else{
                if(uploadRoomPhotoWrapCL5.getVisibility() == View.VISIBLE) {
                    isNewPushId5 = true;
                    isNewPushIds.add(isNewPushId5);
                    ref5 = GeneralFunction.getFirestoreInstance()
                            .collection(Accommodation.URL_ACCOMMODATION)
                            .document(value)
                            .collection(Room.URL_ROOM)
                            .document();
                }
            }
        }

        if(getPushId6 != null && !getPushId6.trim().equals("")){
            isNewPushIds.add(isNewPushId6);
            ref6 =  GeneralFunction.getFirestoreInstance()
                    .collection(Accommodation.URL_ACCOMMODATION)
                    .document(value)
                    .collection(Room.URL_ROOM)
                    .document(getPushId6);
        }

        else{
            if(!isFromDatabase){
                ref6 = GeneralFunction.getFirestoreInstance()
                        .collection(Accommodation.URL_ACCOMMODATION)
                        .document(value)
                        .collection(Room.URL_ROOM)
                        .document();
                setPushId6 = ref6.getId();
                GeneralFunction.saveDocumentPath(parentActivity,Room.ACCOMMODATION_DETIALS,Room.DIR_ROOM_6,setPushId6);
            }
            else{
                if(uploadRoomPhotoWrapCL6.getVisibility() == View.VISIBLE) {
                    isNewPushId6 = true;
                    isNewPushIds.add(isNewPushId6);
                    ref6 = GeneralFunction.getFirestoreInstance()
                            .collection(Accommodation.URL_ACCOMMODATION)
                            .document(value)
                            .collection(Room.URL_ROOM)
                            .document();
                }
            }
        }

        if(getPushId7 != null && !getPushId7.trim().equals("")){
            isNewPushIds.add(isNewPushId7);
            ref7 = GeneralFunction.getFirestoreInstance()
                    .collection(Accommodation.URL_ACCOMMODATION)
                    .document(value)
                    .collection(Room.URL_ROOM)
                    .document(getPushId7);
        }
        else{
            if(!isFromDatabase){
                ref7 = GeneralFunction.getFirestoreInstance()
                        .collection(Accommodation.URL_ACCOMMODATION)
                        .document(value)
                        .collection(Room.URL_ROOM)
                        .document();
                setPushId7 = ref7.getId();
                GeneralFunction.saveDocumentPath(parentActivity,Room.ACCOMMODATION_DETIALS,Room.DIR_ROOM_7,setPushId7);
            }
            else{
                if(uploadRoomPhotoWrapCL7.getVisibility() == View.VISIBLE) {
                    isNewPushId7 = true;
                    isNewPushIds.add(isNewPushId7);
                    ref7 = GeneralFunction.getFirestoreInstance()
                            .collection(Accommodation.URL_ACCOMMODATION)
                            .document(value)
                            .collection(Room.URL_ROOM)
                            .document();
                }
            }
        }


        if(getPushId8 != null && !getPushId8.trim().equals("")){
            isNewPushIds.add(isNewPushId8);
            ref8 =  GeneralFunction.getFirestoreInstance()
                    .collection(Accommodation.URL_ACCOMMODATION)
                    .document(value)
                    .collection(Room.URL_ROOM)
                    .document(getPushId8);
        }
        else{
            if(!isFromDatabase){
                ref8 = GeneralFunction.getFirestoreInstance()
                        .collection(Accommodation.URL_ACCOMMODATION)
                        .document(value)
                        .collection(Room.URL_ROOM)
                        .document();
                setPushId8 = ref8.getId();
                GeneralFunction.saveDocumentPath(parentActivity,Room.ACCOMMODATION_DETIALS,Room.DIR_ROOM_8,setPushId8);
            }
            else{
                if(uploadRoomPhotoWrapCL8.getVisibility() == View.VISIBLE) {
                    isNewPushId8 = true;
                    isNewPushIds.add(isNewPushId8);
                    ref8 = GeneralFunction.getFirestoreInstance()
                            .collection(Accommodation.URL_ACCOMMODATION)
                            .document(value)
                            .collection(Room.URL_ROOM)
                            .document();
                }
            }
        }

//        setPushId = ref.getId();

        if(getroomName1 != null && !TextUtils.isEmpty(getroomName1) && !getroomName1.equals(noAvailable)&& filePath1 != null){
            HashMap<String,Object> roomPicDetails = new HashMap<>();
            roomPicDetails.put(Room.ROOM_NAME,getroomName1);
            uploadRoom1Pic(roomPicDetails, ref1);
            count++;
        }
        if(getroomName2 != null && !TextUtils.isEmpty(getroomName2) && !getroomName2.equals(noAvailable)&& filePath2 != null){
            HashMap<String,Object> roomPicDetails = new HashMap<>();
            roomPicDetails.put(Room.ROOM_NAME,getroomName2);
            uploadRoom2Pic(roomPicDetails, ref2);
            count++;
        }
        if(getroomName3 != null && !TextUtils.isEmpty(getroomName3) && !getroomName3.equals(noAvailable)&& filePath3 != null){
            HashMap<String,Object> roomPicDetails = new HashMap<>();
            roomPicDetails.put(Room.ROOM_NAME,getroomName3);
            uploadRoom3Pic(roomPicDetails, ref3);
            count++;
        }
        if(getroomName4 != null && !TextUtils.isEmpty(getroomName4) && !getroomName4.equals(noAvailable)&& filePath4 != null){
            HashMap<String,Object> roomPicDetails = new HashMap<>();
            roomPicDetails.put(Room.ROOM_NAME,getroomName4);
            uploadRoom4Pic(roomPicDetails, ref4);
            count++;
        }
        if(getroomName5 != null && !TextUtils.isEmpty(getroomName5) && !getroomName5.equals(noAvailable)&& filePath5 != null){
            HashMap<String,Object> roomPicDetails = new HashMap<>();
            roomPicDetails.put(Room.ROOM_NAME,getroomName5);
            uploadRoom5Pic(roomPicDetails, ref5);
            count++;
        }
        if(getroomName6 != null && !TextUtils.isEmpty(getroomName6) && !getroomName6.equals(noAvailable)&& filePath6 != null){
            HashMap<String,Object> roomPicDetails = new HashMap<>();
            roomPicDetails.put(Room.ROOM_NAME,getroomName6);
            uploadRoom6Pic(roomPicDetails, ref6);
            count++;
        }
        if(getroomName7 != null && !TextUtils.isEmpty(getroomName7) && !getroomName7.equals(noAvailable)&& filePath7 != null){
            HashMap<String,Object> roomPicDetails = new HashMap<>();
            roomPicDetails.put(Room.ROOM_NAME,getroomName7);
            uploadRoom7Pic(roomPicDetails, ref7);
            count++;
        }
        if(getroomName8 != null && !TextUtils.isEmpty(getroomName8) && !getroomName8.equals(noAvailable)&& filePath8 != null){
            HashMap<String,Object> roomPicDetails = new HashMap<>();
            roomPicDetails.put(Room.ROOM_NAME,getroomName8);
            uploadRoom8Pic(roomPicDetails, ref8);
            count++;
        }

    }

    private void uploadRoom1Pic(final HashMap<String,Object> roomMap, final DocumentReference roomRef){
        roomMap.put(Accommodation.ACCOMMODATION_POSITION, 1);
        if(filePath1 != null && !isFromDatabase){
            final StorageReference ref = GeneralFunction.getFirebaseStorageInstance().child(Room.URL_ROOM+"/"+parentActivity.uid+"/"+Room.URL_UPLOADED_ROOMS +"/"+value+"/"+getroomName1);

            UploadTask uploadTask = ref.putBytes(GeneralFunction.getCompressedBitmap(parentActivity,filePath1));
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                    GeneralFunction.handleUploadError(parentActivity,UploadRoomPhotoFragment.this,R.string.employer_err_job_posting_job,TAG);
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri downloadUri) {
                            goToUploadAccommodationFragment++;
                            if(downloadUri != null){
                                roomMap.put(Room.URL_ROOM_PIC, downloadUri.toString());
                                setRoomDetails(roomRef,roomMap,1);
                            }
                        }
                    });
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    // + 10 is because i set the progress to 10 when it finished saving the initial firebase data
                    // * 80 is because i want the progress to go to maximum 90% only when the image is finished uploaded
                    // so that the final 10% is set when the firebase updates the imageUrl data as the final step
                    int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * 80) + 10;
                    parentActivity.updateProgress((float) totalProgress,UploadRoomPhotoFragment.this);
                }
            });//end addOnprogresslistener
        }
        else{
            if(filePath1 != null && !URLUtil.isHttpsUrl(filePath1)){
                final StorageReference ref = GeneralFunction.getFirebaseStorageInstance().child(Room.URL_ROOM+"/"+parentActivity.uid+"/"+Room.URL_UPLOADED_ROOMS +"/"+value+"/"+getroomName1);

                UploadTask uploadTask = ref.putBytes(GeneralFunction.getCompressedBitmap(parentActivity,filePath1));
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        GeneralFunction.handleUploadError(parentActivity,UploadRoomPhotoFragment.this,R.string.employer_err_job_posting_job,TAG);
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri downloadUri) {
                                goToUploadAccommodationFragment++;
                                if(downloadUri != null){
                                    roomMap.put(Room.URL_ROOM_PIC, downloadUri.toString());
                                    setRoomDetails(roomRef,roomMap,1);
                                }
                            }
                        });
                    }
                }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        // + 10 is because i set the progress to 10 when it finished saving the initial firebase data
                        // * 80 is because i want the progress to go to maximum 90% only when the image is finished uploaded
                        // so that the final 10% is set when the firebase updates the imageUrl data as the final step
                        int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * 80) + 10;
                        parentActivity.updateProgress((float) totalProgress,UploadRoomPhotoFragment.this);
                    }
                });//end addOnprogresslistener
            }
            else{
                goToUploadAccommodationFragment++;
                setRoomDetails(roomRef,roomMap,1);
            }
        }
    }
    private void uploadRoom2Pic(final HashMap<String,Object> roomMap, final DocumentReference roomRef) {
        roomMap.put(Accommodation.ACCOMMODATION_POSITION, 2);
        if(filePath2 != null && !isFromDatabase){
            final StorageReference ref = GeneralFunction.getFirebaseStorageInstance().child(Room.URL_ROOM+"/"+parentActivity.uid+"/"+Room.URL_UPLOADED_ROOMS +"/"+value+"/"+getroomName2);

            UploadTask uploadTask = ref.putBytes(GeneralFunction.getCompressedBitmap(parentActivity,filePath2));
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                    GeneralFunction.handleUploadError(parentActivity,UploadRoomPhotoFragment.this,R.string.employer_err_job_posting_job,TAG);
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri downloadUri) {
                            goToUploadAccommodationFragment++;
                            if(downloadUri != null){
                                roomMap.put(Room.URL_ROOM_PIC, downloadUri.toString());
                                setRoomDetails(roomRef,roomMap,2);
                            }
                        }
                    });
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    // + 10 is because i set the progress to 10 when it finished saving the initial firebase data
                    // * 80 is because i want the progress to go to maximum 90% only when the image is finished uploaded
                    // so that the final 10% is set when the firebase updates the imageUrl data as the final step
                    int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * 80) + 10;
                    parentActivity.updateProgress((float) totalProgress,UploadRoomPhotoFragment.this);
                }
            });//end addOnprogresslistener
        }
        else{
            if(filePath2 != null && !URLUtil.isHttpsUrl(filePath2)){
                final StorageReference ref = GeneralFunction.getFirebaseStorageInstance().child(Room.URL_ROOM+"/"+parentActivity.uid+"/"+Room.URL_UPLOADED_ROOMS +"/"+value+"/"+getroomName2);

                UploadTask uploadTask = ref.putBytes(GeneralFunction.getCompressedBitmap(parentActivity,filePath2));
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        GeneralFunction.handleUploadError(parentActivity,UploadRoomPhotoFragment.this,R.string.employer_err_job_posting_job,TAG);
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri downloadUri) {
                                goToUploadAccommodationFragment++;
                                if(downloadUri != null){
                                    roomMap.put(Room.URL_ROOM_PIC, downloadUri.toString());
                                    setRoomDetails(roomRef,roomMap,2);
                                }
                            }
                        });
                    }
                }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        // + 10 is because i set the progress to 10 when it finished saving the initial firebase data
                        // * 80 is because i want the progress to go to maximum 90% only when the image is finished uploaded
                        // so that the final 10% is set when the firebase updates the imageUrl data as the final step
                        int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * 80) + 10;
                        parentActivity.updateProgress((float) totalProgress,UploadRoomPhotoFragment.this);
                    }
                });//end addOnprogresslistener
            }
            else{
                goToUploadAccommodationFragment++;
                setRoomDetails(roomRef,roomMap,2);
            }
        }

    }
    private void uploadRoom3Pic(final HashMap<String,Object> roomMap, final DocumentReference roomRef) {
        roomMap.put(Accommodation.ACCOMMODATION_POSITION, 3);
        if(filePath3 != null && !isFromDatabase ){
            final StorageReference ref = GeneralFunction.getFirebaseStorageInstance().child(Room.URL_ROOM+"/"+parentActivity.uid+"/"+Room.URL_UPLOADED_ROOMS +"/"+value+"/"+getroomName3);

            UploadTask uploadTask = ref.putBytes(GeneralFunction.getCompressedBitmap(parentActivity,filePath3));
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                    GeneralFunction.handleUploadError(parentActivity,UploadRoomPhotoFragment.this,R.string.employer_err_job_posting_job,TAG);
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri downloadUri) {
                            goToUploadAccommodationFragment++;
                            if(downloadUri != null){
                                roomMap.put(Room.URL_ROOM_PIC, downloadUri.toString());
                                setRoomDetails(roomRef,roomMap,3);
                            }
                        }
                    });
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    // + 10 is because i set the progress to 10 when it finished saving the initial firebase data
                    // * 80 is because i want the progress to go to maximum 90% only when the image is finished uploaded
                    // so that the final 10% is set when the firebase updates the imageUrl data as the final step
                    int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * 80) + 10;
                    parentActivity.updateProgress((float) totalProgress,UploadRoomPhotoFragment.this);
                }
            });//end addOnprogresslistener
        }
        else{
            if(filePath3 != null && !URLUtil.isHttpsUrl(filePath3)){
                final StorageReference ref = GeneralFunction.getFirebaseStorageInstance().child(Room.URL_ROOM+"/"+parentActivity.uid+"/"+Room.URL_UPLOADED_ROOMS +"/"+value+"/"+getroomName3);
                UploadTask uploadTask = ref.putBytes(GeneralFunction.getCompressedBitmap(parentActivity,filePath3));
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        GeneralFunction.handleUploadError(parentActivity,UploadRoomPhotoFragment.this,R.string.employer_err_job_posting_job,TAG);
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri downloadUri) {
                                goToUploadAccommodationFragment++;
                                if(downloadUri != null){
                                    roomMap.put(Room.URL_ROOM_PIC, downloadUri.toString());
                                    setRoomDetails(roomRef,roomMap,3);
                                }
                            }
                        });
                    }
                }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        // + 10 is because i set the progress to 10 when it finished saving the initial firebase data
                        // * 80 is because i want the progress to go to maximum 90% only when the image is finished uploaded
                        // so that the final 10% is set when the firebase updates the imageUrl data as the final step
                        int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * 80) + 10;
                        parentActivity.updateProgress((float) totalProgress,UploadRoomPhotoFragment.this);
                    }
                });//end addOnprogresslistener
            }
            else{
                goToUploadAccommodationFragment++;
                setRoomDetails(roomRef,roomMap,3);
            }
        }

    }
    private void uploadRoom4Pic(final HashMap<String,Object> roomMap, final DocumentReference roomRef) {
        roomMap.put(Accommodation.ACCOMMODATION_POSITION, 4);
        if(filePath4 != null && !isFromDatabase ){

            final StorageReference ref = GeneralFunction.getFirebaseStorageInstance().child(Room.URL_ROOM+"/"+parentActivity.uid+"/"+Room.URL_UPLOADED_ROOMS+"/"+value+"/"+getroomName4);

            UploadTask uploadTask = ref.putBytes(GeneralFunction.getCompressedBitmap(parentActivity,filePath4));
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                    GeneralFunction.handleUploadError(parentActivity,UploadRoomPhotoFragment.this,R.string.employer_err_job_posting_job,TAG);
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri downloadUri) {
                            goToUploadAccommodationFragment++;
                            if(downloadUri != null){
                                roomMap.put(Room.URL_ROOM_PIC, downloadUri.toString());
                                setRoomDetails(roomRef,roomMap,4);
                            }
                        }
                    });
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    // + 10 is because i set the progress to 10 when it finished saving the initial firebase data
                    // * 80 is because i want the progress to go to maximum 90% only when the image is finished uploaded
                    // so that the final 10% is set when the firebase updates the imageUrl data as the final step
                    int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * 80) + 10;
                    parentActivity.updateProgress((float) totalProgress,UploadRoomPhotoFragment.this);
                }
            });//end addOnprogresslistener
        }
        else{
            if(filePath4 != null && !URLUtil.isHttpsUrl(filePath4)){
                final StorageReference ref = GeneralFunction.getFirebaseStorageInstance().child(Room.URL_ROOM+"/"+parentActivity.uid+"/"+Room.URL_UPLOADED_ROOMS+"/"+value+"/"+getroomName4);

                UploadTask uploadTask = ref.putBytes(GeneralFunction.getCompressedBitmap(parentActivity,filePath4));
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        GeneralFunction.handleUploadError(parentActivity,UploadRoomPhotoFragment.this,R.string.employer_err_job_posting_job,TAG);
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri downloadUri) {
                                goToUploadAccommodationFragment++;
                                if(downloadUri != null){
                                    roomMap.put(Room.URL_ROOM_PIC, downloadUri.toString());
                                    setRoomDetails(roomRef,roomMap,4);
                                }
                            }
                        });
                    }
                }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        // + 10 is because i set the progress to 10 when it finished saving the initial firebase data
                        // * 80 is because i want the progress to go to maximum 90% only when the image is finished uploaded
                        // so that the final 10% is set when the firebase updates the imageUrl data as the final step
                        int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * 80) + 10;
                        parentActivity.updateProgress((float) totalProgress,UploadRoomPhotoFragment.this);
                    }
                });//end addOnprogresslistener
            }
            else{
                goToUploadAccommodationFragment++;
                setRoomDetails(roomRef,roomMap,4);
            }
        }
    }
    private void uploadRoom5Pic(final HashMap<String,Object> roomMap, final DocumentReference roomRef) {
        roomMap.put(Accommodation.ACCOMMODATION_POSITION, 5);
        if(filePath5 != null && !isFromDatabase){
            final StorageReference ref = GeneralFunction.getFirebaseStorageInstance().child(Room.URL_ROOM+"/"+parentActivity.uid+"/"+Room.URL_UPLOADED_ROOMS+"/"+value+"/"+getroomName5);

            UploadTask uploadTask = ref.putBytes(GeneralFunction.getCompressedBitmap(parentActivity,filePath5));
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                    GeneralFunction.handleUploadError(parentActivity,UploadRoomPhotoFragment.this,R.string.employer_err_job_posting_job,TAG);
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri downloadUri) {
                            goToUploadAccommodationFragment++;
                            if(downloadUri != null){
                                roomMap.put(Room.URL_ROOM_PIC, downloadUri.toString());
                                setRoomDetails(roomRef,roomMap,5);
                            }
                        }
                    });
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    // + 10 is because i set the progress to 10 when it finished saving the initial firebase data
                    // * 80 is because i want the progress to go to maximum 90% only when the image is finished uploaded
                    // so that the final 10% is set when the firebase updates the imageUrl data as the final step
                    int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * 80) + 10;
                    parentActivity.updateProgress((float) totalProgress,UploadRoomPhotoFragment.this);
                }
            });//end addOnprogresslistener
        }
        else{
            if(filePath5 != null && !URLUtil.isHttpsUrl(filePath5)){
                final StorageReference ref = GeneralFunction.getFirebaseStorageInstance().child(Room.URL_ROOM+"/"+parentActivity.uid+"/"+Room.URL_UPLOADED_ROOMS+"/"+value+"/"+getroomName5);

                UploadTask uploadTask = ref.putBytes(GeneralFunction.getCompressedBitmap(parentActivity,filePath5));
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        GeneralFunction.handleUploadError(parentActivity,UploadRoomPhotoFragment.this,R.string.employer_err_job_posting_job,TAG);
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri downloadUri) {
                                goToUploadAccommodationFragment++;
                                if(downloadUri != null){
                                    roomMap.put(Room.URL_ROOM_PIC, downloadUri.toString());
                                    setRoomDetails(roomRef,roomMap,5);
                                }
                            }
                        });
                    }
                }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        // + 10 is because i set the progress to 10 when it finished saving the initial firebase data
                        // * 80 is because i want the progress to go to maximum 90% only when the image is finished uploaded
                        // so that the final 10% is set when the firebase updates the imageUrl data as the final step
                        int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * 80) + 10;
                        parentActivity.updateProgress((float) totalProgress,UploadRoomPhotoFragment.this);
                    }
                });//end addOnprogresslistener
            }
            else{
                goToUploadAccommodationFragment++;
                setRoomDetails(roomRef,roomMap,5);
            }
        }
    }
    private void uploadRoom6Pic(final HashMap<String,Object> roomMap, final DocumentReference roomRef) {
        roomMap.put(Accommodation.ACCOMMODATION_POSITION, 6);
        if(filePath6 != null && !isFromDatabase){
            final StorageReference ref = GeneralFunction.getFirebaseStorageInstance().child(Room.URL_ROOM+"/"+parentActivity.uid+"/"+Room.URL_UPLOADED_ROOMS+"/"+value+"/"+getroomName6);

            UploadTask uploadTask = ref.putBytes(GeneralFunction.getCompressedBitmap(parentActivity,filePath6));
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                    GeneralFunction.handleUploadError(parentActivity,UploadRoomPhotoFragment.this,R.string.employer_err_job_posting_job,TAG);
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri downloadUri) {
                            goToUploadAccommodationFragment++;
                            if(downloadUri != null){
                                roomMap.put(Room.URL_ROOM_PIC, downloadUri.toString());
                                setRoomDetails(roomRef,roomMap,6);
                            }
                        }
                    });
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    // + 10 is because i set the progress to 10 when it finished saving the initial firebase data
                    // * 80 is because i want the progress to go to maximum 90% only when the image is finished uploaded
                    // so that the final 10% is set when the firebase updates the imageUrl data as the final step
                    int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * 80) + 10;
                    parentActivity.updateProgress((float) totalProgress,UploadRoomPhotoFragment.this);
                }
            });//end addOnprogresslistener
        }
        else{
            if(filePath6 != null && !URLUtil.isHttpsUrl(filePath6)){
                final StorageReference ref = GeneralFunction.getFirebaseStorageInstance().child(Room.URL_ROOM+"/"+parentActivity.uid+"/"+Room.URL_UPLOADED_ROOMS+"/"+value+"/"+getroomName6);

                UploadTask uploadTask = ref.putBytes(GeneralFunction.getCompressedBitmap(parentActivity,filePath6));
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        GeneralFunction.handleUploadError(parentActivity,UploadRoomPhotoFragment.this,R.string.employer_err_job_posting_job,TAG);
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri downloadUri) {
                                goToUploadAccommodationFragment++;
                                if(downloadUri != null){
                                    roomMap.put(Room.URL_ROOM_PIC, downloadUri.toString());
                                    setRoomDetails(roomRef,roomMap,6);
                                }
                            }
                        });
                    }
                }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        // + 10 is because i set the progress to 10 when it finished saving the initial firebase data
                        // * 80 is because i want the progress to go to maximum 90% only when the image is finished uploaded
                        // so that the final 10% is set when the firebase updates the imageUrl data as the final step
                        int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * 80) + 10;
                        parentActivity.updateProgress((float) totalProgress,UploadRoomPhotoFragment.this);
                    }
                });//end addOnprogresslistener
            }
            else{
                goToUploadAccommodationFragment++;
                setRoomDetails(roomRef,roomMap,6);
            }
        }
    }
    private void uploadRoom7Pic(final HashMap<String,Object> roomMap, final DocumentReference roomRef) {
        roomMap.put(Accommodation.ACCOMMODATION_POSITION, 7);
        if(filePath7 != null &&!isFromDatabase){
            final StorageReference ref = GeneralFunction.getFirebaseStorageInstance().child(Room.URL_ROOM+"/"+parentActivity.uid+"/"+Room.URL_UPLOADED_ROOMS+"/"+value+"/"+getroomName7);

            UploadTask uploadTask = ref.putBytes(GeneralFunction.getCompressedBitmap(parentActivity,filePath7));
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                    GeneralFunction.handleUploadError(parentActivity,UploadRoomPhotoFragment.this,R.string.employer_err_job_posting_job,TAG);
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri downloadUri) {
                            goToUploadAccommodationFragment++;
                            if(downloadUri != null){
                                roomMap.put(Room.URL_ROOM_PIC, downloadUri.toString());
                                setRoomDetails(roomRef,roomMap,7);
                            }
                        }
                    });
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    // + 10 is because i set the progress to 10 when it finished saving the initial firebase data
                    // * 80 is because i want the progress to go to maximum 90% only when the image is finished uploaded
                    // so that the final 10% is set when the firebase updates the imageUrl data as the final step
                    int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * 80) + 10;
                    parentActivity.updateProgress((float) totalProgress,UploadRoomPhotoFragment.this);
                }
            });//end addOnprogresslistener
        }
        else{
            if(filePath7 != null && !URLUtil.isHttpsUrl(filePath7)){
                final StorageReference ref = GeneralFunction.getFirebaseStorageInstance().child(Room.URL_ROOM+"/"+parentActivity.uid+"/"+Room.URL_UPLOADED_ROOMS+"/"+value+"/"+getroomName7);

                UploadTask uploadTask = ref.putBytes(GeneralFunction.getCompressedBitmap(parentActivity,filePath7));
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        GeneralFunction.handleUploadError(parentActivity,UploadRoomPhotoFragment.this,R.string.employer_err_job_posting_job,TAG);
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri downloadUri) {
                                goToUploadAccommodationFragment++;
                                if(downloadUri != null){
                                    roomMap.put(Room.URL_ROOM_PIC, downloadUri.toString());
                                    setRoomDetails(roomRef,roomMap,7);
                                }
                            }
                        });
                    }
                }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        // + 10 is because i set the progress to 10 when it finished saving the initial firebase data
                        // * 80 is because i want the progress to go to maximum 90% only when the image is finished uploaded
                        // so that the final 10% is set when the firebase updates the imageUrl data as the final step
                        int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * 80) + 10;
                        parentActivity.updateProgress((float) totalProgress,UploadRoomPhotoFragment.this);
                    }
                });//end addOnprogresslistener
            }
            else{
                goToUploadAccommodationFragment++;
                setRoomDetails(roomRef,roomMap,7);
            }
        }
    }
    private void uploadRoom8Pic(final HashMap<String,Object> roomMap, final DocumentReference roomRef) {
        roomMap.put(Accommodation.ACCOMMODATION_POSITION, 8);
        if(filePath8 != null && !isFromDatabase){
            final StorageReference ref = GeneralFunction.getFirebaseStorageInstance().child(Room.URL_ROOM+"/"+parentActivity.uid+"/"+Room.URL_UPLOADED_ROOMS+"/"+value+"/"+getroomName8);

            UploadTask uploadTask = ref.putBytes(GeneralFunction.getCompressedBitmap(parentActivity,filePath8));
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                    GeneralFunction.handleUploadError(parentActivity,UploadRoomPhotoFragment.this,R.string.employer_err_job_posting_job,TAG);
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri downloadUri) {
                            goToUploadAccommodationFragment++;
                            if(downloadUri != null){
                                roomMap.put(Room.URL_ROOM_PIC, downloadUri.toString());
                                setRoomDetails(roomRef,roomMap,8);
                            }
                        }
                    });
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    // + 10 is because i set the progress to 10 when it finished saving the initial firebase data
                    // * 80 is because i want the progress to go to maximum 90% only when the image is finished uploaded
                    // so that the final 10% is set when the firebase updates the imageUrl data as the final step
                    int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * 80) + 10;
                    parentActivity.updateProgress((float) totalProgress,UploadRoomPhotoFragment.this);
                }
            });//end addOnprogresslistener
        }
        else{
            if(filePath8 != null && !URLUtil.isHttpsUrl(filePath8)){
                final StorageReference ref = GeneralFunction.getFirebaseStorageInstance().child(Room.URL_ROOM+"/"+parentActivity.uid+"/"+Room.URL_UPLOADED_ROOMS+"/"+value+"/"+getroomName8);

                UploadTask uploadTask = ref.putBytes(GeneralFunction.getCompressedBitmap(parentActivity,filePath8));
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        GeneralFunction.handleUploadError(parentActivity,UploadRoomPhotoFragment.this,R.string.employer_err_job_posting_job,TAG);
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri downloadUri) {
                                goToUploadAccommodationFragment++;
                                if(downloadUri != null){
                                    roomMap.put(Room.URL_ROOM_PIC, downloadUri.toString());
                                    setRoomDetails(roomRef,roomMap,8);
                                }
                            }
                        });
                    }
                }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        // + 10 is because i set the progress to 10 when it finished saving the initial firebase data
                        // * 80 is because i want the progress to go to maximum 90% only when the image is finished uploaded
                        // so that the final 10% is set when the firebase updates the imageUrl data as the final step
                        int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * 80) + 10;
                        parentActivity.updateProgress((float) totalProgress,UploadRoomPhotoFragment.this);
                    }
                });//end addOnprogresslistener
            }
            else{
                goToUploadAccommodationFragment++;
                setRoomDetails(roomRef,roomMap,8);
            }
        }
    }

    private void setRoomDetails(final DocumentReference roomRef, final HashMap<String,Object> roomMap, int position){
        if(isFromDatabase && countBedroomQT==bedroomQTY){
            roomRef.update(roomMap).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    if(goToUploadAccommodationFragment == count){
                        GeneralFunction.handleUploadSuccessTommy(parentActivity,UploadRoomPhotoFragment.this,R.string.upload_room_successful,TAG);
                        goToUploadAccommodationPhotoFragement();
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(parentActivity,e.toString(),Toast.LENGTH_SHORT).show();
                    GeneralFunction.handleUploadError(parentActivity,UploadRoomPhotoFragment.this,R.string.upload_room_err_upload,TAG);
                }
            });
        }
        else{
            if(!isFromDatabase){
                roomRef.set(roomMap).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        if(goToUploadAccommodationFragment == count){
                            GeneralFunction.handleUploadSuccessTommy(parentActivity,UploadRoomPhotoFragment.this,R.string.upload_room_successful,TAG);
                            goToUploadAccommodationPhotoFragement();
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        GeneralFunction.handleUploadError(parentActivity,UploadRoomPhotoFragment.this,R.string.upload_room_err_upload,TAG);
                    }
                });
            }
            else{
                if(isNewPushIds != null){
                        if(isNewPushIds.get(position-1)){
                            roomRef.set(roomMap).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    if(goToUploadAccommodationFragment == count){
                                        GeneralFunction.handleUploadSuccessTommy(parentActivity,UploadRoomPhotoFragment.this,R.string.upload_room_successful,TAG);
                                        goToUploadAccommodationPhotoFragement();
                                    }
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    GeneralFunction.handleUploadError(parentActivity,UploadRoomPhotoFragment.this,R.string.upload_room_err_upload,TAG);
                                }
                            });
                        }
                        else{
                            roomRef.update(roomMap).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    if(goToUploadAccommodationFragment == count){
                                        GeneralFunction.handleUploadSuccessTommy(parentActivity,UploadRoomPhotoFragment.this,R.string.upload_room_successful,TAG);
                                        goToUploadAccommodationPhotoFragement();
                                    }
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    GeneralFunction.handleUploadError(parentActivity,UploadRoomPhotoFragment.this,R.string.upload_room_err_upload,TAG);
                                }
                            });
                        }
                }
            }
        }
    }

    private void showMessageNotificationDialog (){

            if(count == 1){
                GeneralFunction.openMessageNotificationDialog(parentActivity,getString(R.string.host_successful_upload_room), R.drawable.notice_good,TAG);
                count = 0;
            }
            if(count == 2){
                GeneralFunction.openMessageNotificationDialog(parentActivity,getString(R.string.host_successful_upload_room), R.drawable.notice_good,TAG);
                count = 0;
            }
            if(count == 3){
                GeneralFunction.openMessageNotificationDialog(parentActivity,getString(R.string.host_successful_upload_room), R.drawable.notice_good,TAG);
                count = 0;
            }
            if(count == 4){
                GeneralFunction.openMessageNotificationDialog(parentActivity,getString(R.string.host_successful_upload_room), R.drawable.notice_good,TAG);
                count = 0;
            }
            if(count == 5){
                GeneralFunction.openMessageNotificationDialog(parentActivity,getString(R.string.host_successful_upload_room), R.drawable.notice_good,TAG);
                count = 0;
            }
            if(count == 6){
                GeneralFunction.openMessageNotificationDialog(parentActivity,getString(R.string.host_successful_upload_room), R.drawable.notice_good,TAG);
                count = 0;
            }
            if(count == 7){
                GeneralFunction.openMessageNotificationDialog(parentActivity,getString(R.string.host_successful_upload_room), R.drawable.notice_good,TAG);
                count = 0;
            }
            if(count == 8){
                GeneralFunction.openMessageNotificationDialog(parentActivity,getString(R.string.host_successful_upload_room), R.drawable.notice_good,TAG);
                count = 0;
            }
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.selectHotel();
        parentActivity.setToolbarTitle(parentActivity.getString(R.string.host_an_accommodation));
        if(!isSubmit && !isGoCropPhoto){
            if(isFromDatabase){
                isGoCropPhoto= false;
                GeneralFunction.clearGlideCacheMemory(parentActivity);
                if(roomUrl.size()!=0 && isOnbackPressed == 0){
                    callFirestore();
                }
                else{
                    if(!room1Click && !room2Click && !room3Click && !room4Click && !room5Click && !room6Click && !room7Click && !room8Click && isOnbackPressed ==1){
                        callFirestore();
                        isOnbackPressed = 0;
                    }
                    else{
                        callFirestore();
                    }
                }

            }
            else{
                isGoCropPhoto= false;
                //get the bedroom quantity from the previous accommodation detail
                getBedroomQuantityFromJson();
                //set the value into the shared preferences
                setSharedPreferences();
            }
        }


    }

    private void setSharedPreferences(){
        try{
            SharedPreferences sharedPreferences = parentActivity.getSharedPreferences(UPLOAD_ROOM_DETIALS_ALL, MODE_PRIVATE);

            // Get saved string data in it.
            String userInfoListJsonString = sharedPreferences.getString(UPLOAD_ROOM_DETIALS, "");

            // Create Gson object and translate the json string to related java object array.
            Gson gson = new Gson();

            Room userInfoDtoArray = gson.fromJson(userInfoListJsonString, Room.class);

            // Loop the UserInfoDTO array and print each UserInfoDTO data in android monitor as debug log.
            // Get each user info in dto.
            Room userInfoDto = userInfoDtoArray;

            if(userInfoDto != null){

                if (uploadRoomPhotoWrapCL1.getVisibility() == View.VISIBLE){
                    GeneralFunction.showPicFromFilePath(upload_room_1, userInfoDto.imgRoom1);
                    roomName1.setText(userInfoDto.roomName1);
                    filePath1 = userInfoDto.imgRoom1;
                }

                if (uploadRoomPhotoWrapCL2.getVisibility() == View.VISIBLE){
                    GeneralFunction.showPicFromFilePath(upload_room_2, userInfoDto.imgRoom2);
                    roomName2.setText(userInfoDto.roomName2);
                    filePath2 = userInfoDto.imgRoom2;
                }

                if (uploadRoomPhotoWrapCL3.getVisibility() == View.VISIBLE){
                    GeneralFunction.showPicFromFilePath(upload_room_3, userInfoDto.imgRoom3);
                    roomName3.setText(userInfoDto.roomName3);
                    filePath3 = userInfoDto.imgRoom3;
                }

                if (uploadRoomPhotoWrapCL4.getVisibility() == View.VISIBLE){
                    GeneralFunction.showPicFromFilePath(upload_room_4, userInfoDto.imgRoom4);
                    roomName4.setText(userInfoDto.roomName4);
                    filePath4 = userInfoDto.imgRoom4;
                }

                if (uploadRoomPhotoWrapCL5.getVisibility() == View.VISIBLE){
                    GeneralFunction.showPicFromFilePath(upload_room_5, userInfoDto.imgRoom5);
                    roomName5.setText(userInfoDto.roomName5);
                    filePath5 = userInfoDto.imgRoom5;
                }

                if (uploadRoomPhotoWrapCL6.getVisibility() == View.VISIBLE){
                    GeneralFunction.showPicFromFilePath(upload_room_6, userInfoDto.imgRoom6);
                    roomName6.setText(userInfoDto.roomName6);
                    filePath6 = userInfoDto.imgRoom6;
                }
                if (uploadRoomPhotoWrapCL7.getVisibility() == View.VISIBLE){
                    GeneralFunction.showPicFromFilePath(upload_room_7, userInfoDto.imgRoom7);
                    roomName7.setText(userInfoDto.roomName7);
                    filePath7 = userInfoDto.imgRoom7;
                }

                if (uploadRoomPhotoWrapCL8.getVisibility() == View.VISIBLE){
                    GeneralFunction.showPicFromFilePath(upload_room_8, userInfoDto.imgRoom8);
                    roomName8.setText(userInfoDto.roomName8);
                    filePath8 = userInfoDto.imgRoom8;
                }
            }
        }
        catch(Exception e){
            Crashlytics.logException(e);
        }
    }
    private void retrieveFromFirestore(){
        if(roomUrl!= null){
            imageViews.add(upload_room_1);
            imageViews.add(upload_room_2);
            imageViews.add(upload_room_3);
            imageViews.add(upload_room_4);
            imageViews.add(upload_room_5);
            imageViews.add(upload_room_6);
            imageViews.add(upload_room_7);
            imageViews.add(upload_room_8);

            filepathString.add(filePath1);
            filepathString.add(filePath2);
            filepathString.add(filePath3);
            filepathString.add(filePath4);
            filepathString.add(filePath5);
            filepathString.add(filePath6);
            filepathString.add(filePath7);
            filepathString.add(filePath8);


            countBedroomQT = 0;
            for(int i= 0; i<roomUrl.size(); i ++){ //loop
                countBedroomQT++;
                GlideImageSetting(parentActivity, Uri.parse(roomUrl.get(i)),imageViews.get(i));
            }
            if(countBedroomQT != 0 && roomPicPushIdFromFirestore != null){
                if(countBedroomQT == 1){
                    filePath1 = roomUrl.get(0);
                    getPushId1 = roomPicPushIdFromFirestore.get(0);
                }
                if(countBedroomQT == 2){
                    filePath1 = roomUrl.get(0);
                    filePath2 = roomUrl.get(1);
                    getPushId1 = roomPicPushIdFromFirestore.get(0);
                    getPushId2 = roomPicPushIdFromFirestore.get(1);
                }
                if(countBedroomQT == 3){
                    filePath1 = roomUrl.get(0);
                    filePath2 = roomUrl.get(1);
                    filePath3 = roomUrl.get(2);
                    getPushId1 = roomPicPushIdFromFirestore.get(0);
                    getPushId2 = roomPicPushIdFromFirestore.get(1);
                    getPushId3 = roomPicPushIdFromFirestore.get(2);
                }
                if(countBedroomQT == 4){
                    filePath1 = roomUrl.get(0);
                    filePath2 = roomUrl.get(1);
                    filePath3 = roomUrl.get(2);
                    filePath4 = roomUrl.get(3);
                    getPushId1 = roomPicPushIdFromFirestore.get(0);
                    getPushId2 = roomPicPushIdFromFirestore.get(1);
                    getPushId3 = roomPicPushIdFromFirestore.get(2);
                    getPushId4 = roomPicPushIdFromFirestore.get(3);
                }
                if(countBedroomQT == 5){
                    filePath1 = roomUrl.get(0);
                    filePath2 = roomUrl.get(1);
                    filePath3 = roomUrl.get(2);
                    filePath4 = roomUrl.get(3);
                    filePath5 = roomUrl.get(4);
                    getPushId1 = roomPicPushIdFromFirestore.get(0);
                    getPushId2 = roomPicPushIdFromFirestore.get(1);
                    getPushId3 = roomPicPushIdFromFirestore.get(2);
                    getPushId4 = roomPicPushIdFromFirestore.get(3);
                    getPushId5 = roomPicPushIdFromFirestore.get(4);
                }
                if(countBedroomQT == 6){
                    filePath1 = roomUrl.get(0);
                    filePath2 = roomUrl.get(1);
                    filePath3 = roomUrl.get(2);
                    filePath4 = roomUrl.get(3);
                    filePath5 = roomUrl.get(4);
                    filePath6 = roomUrl.get(5);
                    getPushId1 = roomPicPushIdFromFirestore.get(0);
                    getPushId2 = roomPicPushIdFromFirestore.get(1);
                    getPushId3 = roomPicPushIdFromFirestore.get(2);
                    getPushId4 = roomPicPushIdFromFirestore.get(3);
                    getPushId5 = roomPicPushIdFromFirestore.get(4);
                    getPushId6 = roomPicPushIdFromFirestore.get(5);
                }
                if(countBedroomQT == 7) {
                    filePath1 = roomUrl.get(0);
                    filePath2 = roomUrl.get(1);
                    filePath3 = roomUrl.get(2);
                    filePath4 = roomUrl.get(3);
                    filePath5 = roomUrl.get(4);
                    filePath6 = roomUrl.get(5);
                    filePath7 = roomUrl.get(6);
                    getPushId1 = roomPicPushIdFromFirestore.get(0);
                    getPushId2 = roomPicPushIdFromFirestore.get(1);
                    getPushId3 = roomPicPushIdFromFirestore.get(2);
                    getPushId4 = roomPicPushIdFromFirestore.get(3);
                    getPushId5 = roomPicPushIdFromFirestore.get(4);
                    getPushId6 = roomPicPushIdFromFirestore.get(5);
                    getPushId7 = roomPicPushIdFromFirestore.get(6);
                }
                if(countBedroomQT == 8){
                    filePath1 = roomUrl.get(0);
                    filePath2 = roomUrl.get(1);
                    filePath3 = roomUrl.get(2);
                    filePath4 = roomUrl.get(3);
                    filePath5 = roomUrl.get(4);
                    filePath6 = roomUrl.get(5);
                    filePath7 = roomUrl.get(6);
                    filePath8 = roomUrl.get(7);
                    getPushId1 = roomPicPushIdFromFirestore.get(0);
                    getPushId2 = roomPicPushIdFromFirestore.get(1);
                    getPushId3 = roomPicPushIdFromFirestore.get(2);
                    getPushId4 = roomPicPushIdFromFirestore.get(3);
                    getPushId5 = roomPicPushIdFromFirestore.get(4);
                    getPushId6 = roomPicPushIdFromFirestore.get(5);
                    getPushId7 = roomPicPushIdFromFirestore.get(6);
                    getPushId8 = roomPicPushIdFromFirestore.get(7);
                }
            }
        }
        if(roomName != null){
            editTexts.add(roomName1);
            editTexts.add(roomName2);
            editTexts.add(roomName3);
            editTexts.add(roomName4);
            editTexts.add(roomName5);
            editTexts.add(roomName6);
            editTexts.add(roomName7);
            editTexts.add(roomName8);
            for(int i= 0; i<roomName.size(); i ++){ //loop
                editTexts.get(i).setText(roomName.get(i));
            }
        }

        if(isFromDatabase && documentPushId != null){
            value = documentPushId;
        }
    }

    private void callFirestore(){
        //get room url and document id
        CollectionReference roomRef = db.collection(Accommodation.URL_ACCOMMODATION).document(documentPushId).collection(Room.URL_ROOM);
        roomRef.orderBy(Accommodation.ACCOMMODATION_POSITION, Query.Direction.ASCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.w(TAG, "Listen failed.", e);
                    return;
                }
                String source = queryDocumentSnapshots != null && queryDocumentSnapshots.getMetadata().hasPendingWrites()
                        ? "Local" : "Server";
                Log.d(TAG,source);
                if (queryDocumentSnapshots != null) {
                    roomUrl.clear();
                    roomName.clear();
                    queryDocumentSnapshots.getMetadata().isFromCache();
                    queryDocumentSnapshots.getMetadata().hasPendingWrites();
                    for(int i = 0; i<queryDocumentSnapshots.size(); i++) {
                        roomUrl.add(queryDocumentSnapshots.toObjects(Room.class).get(i).urlRoomPic);
                        roomName.add(queryDocumentSnapshots.toObjects(Room.class).get(i).roomName);
                    }
                    roomUrl.size();
                    roomName.size();
                    for (DocumentSnapshot documentReference : queryDocumentSnapshots.getDocuments()) {
                        roomPicPushIdFromFirestore.add(documentReference.getId());
                        positions.add(documentReference.toObject(Room.class).getRoomPosition());
                    }
                    if(!isSubmit){
                        roomPicPushIdFromFirestore.size();
                        retrieveFromFirestore();
                        getBedroomQuantityFromFirestore();
                    }
//                    else{
//                        retrieveFromFirestore();
//                        getBedroomQuantityFromFirestore();
//                    }
                }
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        countBedroomQT = 0;
        goToUploadAccommodationFragment = 0;
        roomUrl.clear();
        positions.clear();
        filepathString.clear();
        imageViews.clear();
        editTexts.clear();
        roomName.clear();
        isSubmit=false;
        roomPicPushIdFromFirestore.clear();
        if(room1Click||room2Click||room3Click||room4Click||room5Click||room6Click||room7Click||room8Click){
            countBedroomQT = 0;
        }else{
            isOnbackPressed = 1;
            countBedroomQT = 0;
            goToUploadAccommodationFragment = 0;
            roomUrl.clear();
            positions.clear();
            filepathString.clear();
            imageViews.clear();
            editTexts.clear();
            roomName.clear();
            isSubmit=false;
            roomPicPushIdFromFirestore.clear();
            for(int i = 0; i<isNewPushIds.size(); i++){
                isNewPushIds.set(i, false);
            }
            isNewPushIds.clear();
            for(int i = 0; i<roomClicks.size(); i++){
                roomClicks.set(i, false);
            }
        }

    }
}
