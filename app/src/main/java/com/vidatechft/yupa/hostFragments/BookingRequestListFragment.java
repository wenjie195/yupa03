package com.vidatechft.yupa.hostFragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.BookingRequestListRecyclerAdapter;
import com.vidatechft.yupa.classes.BookingHistory;
import com.vidatechft.yupa.classes.User;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.HashMap;

public class BookingRequestListFragment extends Fragment{
    public static final String TAG = BookingRequestListFragment.class.getName();
    private MainActivity parentActivity;
    private ArrayList<BookingHistory> bookingHistories = new ArrayList<>();

    private RecyclerView bookingRequestRV;

    private HashMap<String,User> userMap = new HashMap<>();
    private int downloadCount = 0;

    private TextView noTV;
    private String errorTxt;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static BookingRequestListFragment newInstance(MainActivity parentActivity, ArrayList<BookingHistory> bookingHistories) {
        BookingRequestListFragment fragment = new BookingRequestListFragment();
        fragment.parentActivity = parentActivity;
        fragment.bookingHistories = bookingHistories;
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_booking_request_list, container, false);

        errorTxt = parentActivity.getString(R.string.booking_result_error_loading);

        bookingRequestRV = view.findViewById(R.id.bookingRequestRV);
        noTV = view.findViewById(R.id.noTV);

        if(bookingHistories != null && !bookingHistories.isEmpty()){
            parentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(bookingHistories.get(0).room != null && bookingHistories.get(0).room.accommodationName != null){
                        TextView bookingRequestTitleTV = view.findViewById(R.id.bookingRequestTitleTV);
                        bookingRequestTitleTV.setText(String.format(parentActivity.getString(R.string.bookingRequestHouseTV),bookingHistories.get(0).room.accommodationName));
                    }

                    for(final BookingHistory bookingHistory : bookingHistories){
                        if(bookingHistory != null && bookingHistory.userId != null){
                            if(userMap.get(bookingHistory.userId) != null){
                                downloadCount++;
                                handleDownloadAllFinished();
                            }else{
                                GeneralFunction.getFirestoreInstance()
                                        .collection(User.URL_USER)
                                        .document(bookingHistory.userId)
                                        .get()
                                        .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                            @Override
                                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                downloadCount++;
                                                if(GeneralFunction.hasErrorDoc(parentActivity,task,TAG)){
                                                    handleDownloadAllFinished();
                                                    return;
                                                }

                                                if(task.getResult().exists()){
                                                    User tempUser = task.getResult().toObject(User.class);
                                                    if(tempUser != null){
                                                        userMap.put(bookingHistory.userId,tempUser);
                                                    }
                                                }

                                                handleDownloadAllFinished();
                                            }
                                        });
                            }
                        }else{
                            downloadCount++;
                            handleDownloadAllFinished();
                        }
                    }
                }
            });
        }

        return view;
    }

    private boolean ifEmptyThenGoBack(){
        if(bookingHistories != null && !bookingHistories.isEmpty()){
            return false;
        }else{
//            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_occured),R.drawable.notice_bad,TAG);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    parentActivity.onBackPressed();
                }
            },200);
            return true;
        }
    }

    private void handleDownloadAllFinished(){
        if(downloadCount == bookingHistories.size()){
            for(int i = 0; i < bookingHistories.size(); i++){
                BookingHistory tempBookingHistory = bookingHistories.get(i);
                if(tempBookingHistory != null && tempBookingHistory.userId != null){
                    bookingHistories.get(i).user = userMap.get(tempBookingHistory.userId);
                }else{
                    bookingHistories.remove(i);
                }
            }

            downloadCount = 0;
            if(bookingHistories.isEmpty()){
                noTV.setText(errorTxt);
                noTV.setVisibility(View.VISIBLE);
            }else{
                noTV.setVisibility(View.GONE);
                setAdapter();
            }
        }
    }

    private void setAdapter(){
        BookingRequestListRecyclerAdapter bookingRequestListAdapter = new BookingRequestListRecyclerAdapter(parentActivity, BookingRequestListFragment.this, bookingHistories);
        bookingRequestRV.setAdapter(bookingRequestListAdapter);
        bookingRequestRV.setLayoutManager(new LinearLayoutManager(parentActivity, LinearLayoutManager.VERTICAL, false));
    }

    public void removeBookingRequest(int position){
        bookingHistories.remove(position);
        if(bookingRequestRV != null && bookingRequestRV.getAdapter() != null){
            bookingRequestRV.getAdapter().notifyItemRemoved(position);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.selectHotel();
        parentActivity.setToolbarTitle(parentActivity.getString(R.string.host_booking_request));

        ifEmptyThenGoBack();
    }
}
