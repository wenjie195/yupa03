package com.vidatechft.yupa.hostFragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.Accommodation;
import com.vidatechft.yupa.classes.Amenity;
import com.vidatechft.yupa.classes.Description;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.classes.Rules;
import com.vidatechft.yupa.classes.Setting;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class SettingFragment extends Fragment implements View.OnClickListener{
    public static final String TAG = SettingFragment.class.getName();
    private MainActivity parentActivity;
    private Button btn_myApproval, btn_payment,btn_guestMessage_yes,btn_guestMessage_no,btn_uploadLicense_yes,btn_uploadLicense_no,
            btn_femaleOnly,btn_maleOnly,btn_boostAccommodation_yes,btn_boostAccommodation_no,btn_provideDiscount_yes,btn_provideDiscount_no;
    private EditText guestMoveInSPN,discountForSPN,qualifyGetSPN;
    private ConstraintLayout constraintLayoutSettingSpinnerItem;
    private ListView listViewSettingSpinnerItem;
    private boolean checked = false;
    private Button btn_publish,btn_saveAsDraft_Setting;
    private DocumentReference settingRef,draftRef;
    private String value;
    private String setSettingDocumentPath, getSettingDocumentPath;
    private boolean isSaveDraft = false;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private String documentPushId;
    private boolean isFromDatabase;
    private List<Setting> settingDataset = new ArrayList<>();
    private List<Button> buttons = new ArrayList<>();
    private List<EditText> editTexts = new ArrayList<>();
    private String settingPushId;
    private boolean isEditProfile = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static SettingFragment newInstance(MainActivity parentActivity) {
        SettingFragment fragment = new SettingFragment();
        fragment.parentActivity = parentActivity;
        return fragment;
    }

    public static SettingFragment newInstance(MainActivity parentActivity, String documentPushId, boolean isFromDatabase, boolean isEditProfile) {
        SettingFragment fragment = new SettingFragment();
        fragment.parentActivity = parentActivity;
        fragment.documentPushId = documentPushId;
        fragment.isFromDatabase = isFromDatabase;
        fragment.isEditProfile = isEditProfile;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_setting, container, false);

        //button
        btn_myApproval = view.findViewById(R.id.btn_myApproval);
        btn_payment = view.findViewById(R.id.btn_payment);
        btn_guestMessage_yes = view.findViewById(R.id.btn_guestMessage_yes);
        btn_guestMessage_no = view.findViewById(R.id.btn_guestMessage_no);
        btn_uploadLicense_yes = view.findViewById(R.id.btn_uploadLicense_yes);
        btn_uploadLicense_no = view.findViewById(R.id.btn_uploadLicense_no);
        btn_femaleOnly = view.findViewById(R.id.btn_femaleOnly);
        btn_maleOnly = view.findViewById(R.id.btn_maleOnly);
        btn_boostAccommodation_yes = view.findViewById(R.id.btn_boostAccommodation_yes);
        btn_boostAccommodation_no = view.findViewById(R.id.btn_boostAccommodation_no);
        btn_provideDiscount_yes = view.findViewById(R.id.btn_provideDiscount_yes);
        btn_provideDiscount_no = view.findViewById(R.id.btn_provideDiscount_no);
        btn_publish = view.findViewById(R.id.btn_publish);
        btn_saveAsDraft_Setting = view.findViewById(R.id.btn_saveAsDraft_Setting);

        //editText
        guestMoveInSPN = view.findViewById(R.id.guestMoveInSPN);
        discountForSPN = view.findViewById(R.id.discountForSPN);
        qualifyGetSPN = view.findViewById(R.id.qualifyGetSPN);

        //constraintLayout
        constraintLayoutSettingSpinnerItem = view.findViewById(R.id.constraintLayoutSpinnerItem);

        //ListView
        listViewSettingSpinnerItem = view.findViewById(R.id.listViewSettingSpinnerItem);

        //listener
        btn_myApproval.setOnClickListener(this);
        btn_payment.setOnClickListener(this);
        btn_guestMessage_yes.setOnClickListener(this);
        btn_guestMessage_no.setOnClickListener(this);
        btn_uploadLicense_yes.setOnClickListener(this);
        btn_uploadLicense_no.setOnClickListener(this);
        btn_femaleOnly.setOnClickListener(this);
        btn_maleOnly.setOnClickListener(this);
        btn_boostAccommodation_yes.setOnClickListener(this);
        btn_boostAccommodation_no.setOnClickListener(this);
        btn_provideDiscount_yes.setOnClickListener(this);
        btn_provideDiscount_no.setOnClickListener(this);
        btn_publish.setOnClickListener(this);
        btn_saveAsDraft_Setting.setOnClickListener(this);

        guestMoveInSPN.setOnClickListener(this);
        discountForSPN.setOnClickListener(this);
        qualifyGetSPN.setOnClickListener(this);

        constraintLayoutSettingSpinnerItem.setOnClickListener(this);

        if(!isFromDatabase){
            setSharedPreferences();
        }
        if(isEditProfile){
            btn_publish.setText(R.string.btn_saveChanges);
            btn_saveAsDraft_Setting.setVisibility(View.GONE);
        }

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_myApproval:
                buttonClickableAtOnce(btn_myApproval,btn_payment,true);
                break;
            case R.id.btn_payment:
                buttonClickableAtOnce(btn_payment,btn_myApproval,true);
                break;
            case R.id.btn_guestMessage_yes:
                buttonClickableAtOnce(btn_guestMessage_yes,btn_guestMessage_no,true);
                break;
            case R.id.btn_guestMessage_no:
                buttonClickableAtOnce(btn_guestMessage_no,btn_guestMessage_yes,true);
                break;
            case R.id.btn_uploadLicense_yes:
                buttonClickableAtOnce(btn_uploadLicense_yes,btn_uploadLicense_no,true);
                break;
            case R.id.btn_uploadLicense_no:
                buttonClickableAtOnce(btn_uploadLicense_no,btn_uploadLicense_yes,true);
                break;
            case R.id.btn_femaleOnly:
                buttonClickableTwice(btn_femaleOnly,btn_maleOnly);
                break;
            case R.id.btn_maleOnly:
                buttonClickableTwice(btn_maleOnly,btn_femaleOnly);
                break;
            case R.id.btn_boostAccommodation_yes:
                buttonClickableAtOnce(btn_boostAccommodation_yes,btn_boostAccommodation_no,true);
                break;
            case R.id.btn_boostAccommodation_no:
                buttonClickableAtOnce(btn_boostAccommodation_no,btn_boostAccommodation_yes,true);
                break;
            case R.id.btn_provideDiscount_yes:
                buttonClickableAtOnce(btn_provideDiscount_yes,btn_provideDiscount_no,true);
                break;
            case R.id.btn_provideDiscount_no:
                buttonClickableAtOnce(btn_provideDiscount_no,btn_provideDiscount_yes,true);
                break;
            case R.id.guestMoveInSPN:
                GeneralFunction.slideUpAnimationForListView(parentActivity,constraintLayoutSettingSpinnerItem, listViewSettingSpinnerItem,R.array.guest_move_in,guestMoveInSPN);
                MainActivity.isOpen = true;
                break;
            case R.id.discountForSPN:
                GeneralFunction.slideUpAnimationForListView(parentActivity,constraintLayoutSettingSpinnerItem, listViewSettingSpinnerItem,R.array.guest_get_discount,discountForSPN);
                MainActivity.isOpen = true;
                break;
            case R.id.qualifyGetSPN:
                GeneralFunction.slideUpAnimationForListView(parentActivity,constraintLayoutSettingSpinnerItem, listViewSettingSpinnerItem,R.array.guest_qualify_get,qualifyGetSPN);
                MainActivity.isOpen = true;
                break;
            case R.id.constraintLayoutSpinnerItem:
                GeneralFunction.slideDownAnimationForListView(parentActivity,constraintLayoutSettingSpinnerItem);
                break;
            case R.id.btn_publish:
                isSaveDraft = false;
                submitSettingDetails();
                break;
            case R.id.btn_saveAsDraft_Setting:
                isSaveDraft = true;
                submitSettingDetails();
                break;
        }
    }

    private void buttonClickableAtOnce(Button btnTrue, Button btnFalse, boolean isChecked){

        if(!isChecked){
            //set selection
            btnFalse.setSelected(true);
            btnTrue.setSelected(false);
            //set false background
            btnFalse.setBackground(parentActivity.getResources().getDrawable(R.drawable.rounded_shape_button_done));
            btnFalse.setTextColor(parentActivity.getResources().getColor(R.color.white));
//            btnFalse.setColorFilter(Color.argb(255, 255, 255, 255)); // White Tint
            //set true background
            btnTrue.setBackground(parentActivity.getResources().getDrawable(R.drawable.rounded_acctype_white_bg_border_black));
            btnTrue.setTextColor(parentActivity.getResources().getColor(R.color.black));
        }
        else{
            //set selection
            btnTrue.setSelected(true);
            btnFalse.setSelected(false);
            //set true background
            btnTrue.setBackground(parentActivity.getResources().getDrawable(R.drawable.rounded_shape_button_done));
            btnTrue.setTextColor(parentActivity.getResources().getColor(R.color.white));
//            btnTrue.setColorFilter(Color.argb(255, 255, 255, 255)); // White Tint
            //set false background
            btnFalse.setBackground(parentActivity.getResources().getDrawable(R.drawable.rounded_acctype_white_bg_border_black));
            btnFalse.setTextColor(parentActivity.getResources().getColor(R.color.black));
        }
    }


    private void buttonClickableTwice(Button btnTrue, Button btnFalse){

        if(btnTrue.isSelected()){
            btnTrue.setSelected(false);
            btnTrue.setBackground(getResources().getDrawable(R.drawable.rounded_acctype_white_bg_border_black));
            btnTrue.setTextColor(getResources().getColor(R.color.black));

            btnFalse.setSelected(false);
            btnFalse.setBackground(getResources().getDrawable(R.drawable.rounded_acctype_white_bg_border_black));
            btnFalse.setTextColor(getResources().getColor(R.color.black));
        }
        else{
            btnTrue.setSelected(true);
            btnTrue.setBackground(getResources().getDrawable(R.drawable.rounded_shape_button_done));
            btnTrue.setTextColor(getResources().getColor(R.color.white));

            btnFalse.setSelected(false);
            btnFalse.setBackground(getResources().getDrawable(R.drawable.rounded_acctype_white_bg_border_black));
            btnFalse.setTextColor(getResources().getColor(R.color.black));
        }
    }

    private boolean isValidInput(String getGuestMoveIn,String getGuestDiscount, String getGuestQualify){
        return true;
//        return  GeneralFunction.checkAndReturnString(getGuestMoveIn,parentActivity.getString(R.string.error_please_choose_guest_move_in),parentActivity)
//                &&
//                GeneralFunction.checkAndReturnString(getGuestDiscount,parentActivity.getString(R.string.error_please_choose_guest_discount),parentActivity)
//                &&
//                GeneralFunction.checkAndReturnString(getGuestQualify,parentActivity.getString(R.string.error_please_choose_guest_qualify),parentActivity);
    }

    private void submitSettingDetails(){
        String getGuestMoveIn = guestMoveInSPN.getText().toString().trim();
        String getGuestDiscount = discountForSPN.getText().toString().trim();
        String getGuestQualify = qualifyGetSPN.getText().toString().trim();

//        if(!btn_myApproval.isSelected() && !btn_payment.isSelected()){
//            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_choose_approval),R.drawable.notice_good,TAG);
//            checked = false;
//            return;
//        }
//
//        else if(!btn_guestMessage_yes.isSelected() && !btn_guestMessage_no.isSelected()){
//            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_choose_should_message),R.drawable.notice_good,TAG);
//            checked = false;
//            return;
//        }
//
//        else if(!btn_uploadLicense_yes.isSelected() && !btn_uploadLicense_no.isSelected()){
//            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_choose_should_upload_license),R.drawable.notice_good,TAG);
//            checked = false;
//            return;
//        }
//        //to set the validation for btn male and female
////        else if(!btn_femaleOnly.isSelected() && !btn_maleOnly.isSelected()){
////            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_choose_guest_gender),R.drawable.notice_good,TAG);
////            checked = false;
////            return;
////        }
//
//        else if(!btn_boostAccommodation_yes.isSelected() && !btn_boostAccommodation_no.isSelected()){
//            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_choose_boost_accommodation),R.drawable.notice_good,TAG);
//            checked = false;
//            return;
//        }
//
//        else if(!btn_provideDiscount_yes.isSelected() && !btn_provideDiscount_no.isSelected()){
//            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_choose_discount),R.drawable.notice_good,TAG);
//            checked = false;
//            return;
//        }

        if(!checked){
            if(isValidInput(getGuestMoveIn,getGuestDiscount,getGuestQualify)){
                HashMap<String, Object> settingMap = new HashMap<>();
                settingMap.put(Setting.GUEST_MOVE_IN, getGuestMoveIn);
                settingMap.put(Setting.GUEST_GET_DISCOUNTS, getGuestDiscount);
                settingMap.put(Setting.GUEST_GET_QUALIFY, getGuestQualify);
                if(btn_myApproval.isSelected()){
                    settingMap.put(Setting.approvalType, Setting.myApproval);
                }
                if (btn_payment.isSelected()){
                    settingMap.put(Setting.approvalType, Setting.payment);
                }
                settingMap.put(Setting.shouldGuestMessageYou, btn_guestMessage_yes.isSelected());
                settingMap.put(Setting.shouldGuestUploadIdentity, btn_uploadLicense_yes.isSelected());
                if(btn_femaleOnly.isSelected()){
                    settingMap.put(Setting.guestGender, Setting.guestFemale);
                }
                if (btn_maleOnly.isSelected()){
                    settingMap.put(Setting.guestGender, Setting.guestMale);
                }
                if(!btn_maleOnly.isSelected() && !btn_femaleOnly.isSelected()){
                    settingMap.put(Setting.guestGender, Setting.guestBoth);
                }
                settingMap.put(Setting.wantBoostAccommodation, btn_boostAccommodation_yes.isSelected());
                settingMap.put(Setting.provideDiscount, btn_provideDiscount_yes.isSelected());
                settingMap.put(Setting.DATE_UPDATED, FieldValue.serverTimestamp());
                if(settingDataset != null){
                    for(Setting setting : settingDataset){
                        if(setting!=null){
                            if(setting.dateCreated == null){
                                settingMap.put(Setting.DATE_CREATED, FieldValue.serverTimestamp());
                            }
                        }
                    }
                }

                if(!isEditProfile){
                    if(isSaveDraft){
                        settingMap.put(Setting.IS_DRAFT, true);
                    }
                    else{
                        settingMap.put(Setting.IS_DRAFT, false);
                    }
                }

                if(!isFromDatabase){
                    GeneralFunction.saveAccommodationHashMapSharedPreferences(parentActivity,settingMap, Setting.SETTING_DETAILS,Setting.SETTING_DETAILS);
                }

                parentActivity.initializeProgressIndicator(parentActivity.getString(R.string.employer_job_posting),false,this);
                parentActivity.controlProgressIndicator(true,this);
                //initiate save to database
                saveRulesDetialsToDatabase(settingMap);
            }

        }
    }

    private void setSharedPreferences(){
        try{
            SharedPreferences sharedPreferences = parentActivity.getSharedPreferences(Setting.SETTING_DETAILS, MODE_PRIVATE);

            // Get saved string data in it.
            String userInfoListJsonString = sharedPreferences.getString(Setting.SETTING_DETAILS, "");

            // Create Gson object and translate the json string to related java object array.
            Gson gson = new Gson();

            Setting userInfoDtoArray = gson.fromJson(userInfoListJsonString, Setting.class);

            // Loop the UserInfoDTO array and print each UserInfoDTO data in android monitor as debug log.
            // Get each user info in dto.
            Setting userInfoDto = userInfoDtoArray;

            if(userInfoDto != null){
                guestMoveInSPN.setText(userInfoDto.guestMoveIn);
                discountForSPN.setText(userInfoDto.guestGetDiscount);
                qualifyGetSPN.setText(userInfoDto.guestGetQualify);
            }

            //to get the bundle of value in sharedpreference with GOOGLE_MAP_APY_KEY
            Type type = new TypeToken<Map<String, Object>>(){}.getType();
            Map<String, Object> storeSettingStringMap = gson.fromJson(sharedPreferences.getString(Setting.SETTING_DETAILS,""), type);

            if(storeSettingStringMap != null){
                //store value into string
                String approvalType = storeSettingStringMap.get(Setting.approvalType).toString().trim();
                String gender = storeSettingStringMap.get(Setting.guestGender).toString().trim();

                if(approvalType.equals(Setting.myApproval)){
                    buttonClickableAtOnce(btn_myApproval,btn_payment,true);
                }
                if(approvalType.equals(Setting.payment)){
                    buttonClickableAtOnce(btn_payment,btn_myApproval,true);
                }

                if(gender.equals(Setting.guestMale)){
                    buttonClickableAtOnce(btn_maleOnly,btn_femaleOnly,true);
                }
                if(gender.equals(Setting.guestFemale)){
                    buttonClickableAtOnce(btn_femaleOnly,btn_maleOnly,true);
                }
            }

            Map<String, Boolean> storeSettingBooleanMap = gson.fromJson(sharedPreferences.getString(Setting.SETTING_DETAILS,""), type);
            if(storeSettingBooleanMap != null){
                //set button here
                btn_guestMessage_yes.setSelected(storeSettingBooleanMap.get(Setting.shouldGuestMessageYou));
                btn_uploadLicense_yes.setSelected(storeSettingBooleanMap.get(Setting.shouldGuestUploadIdentity));
                btn_boostAccommodation_yes.setSelected(storeSettingBooleanMap.get(Setting.wantBoostAccommodation));
                btn_provideDiscount_yes.setSelected(storeSettingBooleanMap.get(Setting.provideDiscount));

                //set button for whether it true or false by compare
                buttonClickableAtOnce(btn_guestMessage_yes,btn_guestMessage_no,storeSettingBooleanMap.get(Setting.shouldGuestMessageYou));
                buttonClickableAtOnce(btn_uploadLicense_yes,btn_uploadLicense_no,storeSettingBooleanMap.get(Setting.shouldGuestUploadIdentity));
                buttonClickableAtOnce(btn_boostAccommodation_yes,btn_boostAccommodation_no,storeSettingBooleanMap.get(Setting.wantBoostAccommodation));
                buttonClickableAtOnce(btn_provideDiscount_yes,btn_provideDiscount_no,storeSettingBooleanMap.get(Setting.provideDiscount));

            }
        }
        catch(Exception e){
            Crashlytics.logException(e);
        }
    }

    private void saveRulesDetialsToDatabase(HashMap<String, Object> settingMap){
        HashMap<String,Object> draftMap = new HashMap<>();
        //initiate get path
        if(!isFromDatabase){
            value = GeneralFunction.getDocumentPath(parentActivity, Room.ACCOMMODATION_DETIALS,Room.DIR_DOCUMENT, "");
            getSettingDocumentPath = GeneralFunction.getDocumentPath(parentActivity, Setting.SETTING_DETAILS,Setting.DIR_SETTING,"");
        }

        if(getSettingDocumentPath != null && !getSettingDocumentPath.trim().equals("")){
            settingRef = GeneralFunction.getFirestoreInstance()
                    .collection(Accommodation.URL_ACCOMMODATION)
                    .document(value)
                    .collection(Setting.URL_SETTING)
                    .document(getSettingDocumentPath);
        }
        else{
            settingRef = GeneralFunction.getFirestoreInstance()
                    .collection(Accommodation.URL_ACCOMMODATION)
                    .document(value)
                    .collection(Setting.URL_SETTING)
                    .document();

            setSettingDocumentPath = settingRef.getId();
            if(!isFromDatabase){
                GeneralFunction.saveDocumentPath(parentActivity, Setting.SETTING_DETAILS,Setting.DIR_SETTING,setSettingDocumentPath);
            }
        }
        if(!isEditProfile){
            if(isSaveDraft){
                draftMap.put(Setting.IS_DRAFT, true);
                draftMap.put(Room.STATUS, null);
                GeneralFunction.getFirestoreInstance()
                        .collection(Accommodation.URL_ACCOMMODATION)
                        .document(value)
                        .update(draftMap).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        clearSharePreference();
                        Toast.makeText(parentActivity,"Successful save as draft!", Toast.LENGTH_SHORT).show();
                    }
                });

            }
            else{
                draftMap.put(Setting.IS_DRAFT, false);
                draftMap.put(Room.STATUS, Room.STATUS_PENDING);
                GeneralFunction.getFirestoreInstance()
                        .collection(Accommodation.URL_ACCOMMODATION)
                        .document(value)
                        .update(draftMap).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        clearSharePreference();
                    }
                });
            }
        }


        setSettingDetails(settingRef,settingMap);
    }

    private void setSettingDetails(DocumentReference settingRef, HashMap<String,Object> settingMap){
       if(isFromDatabase){
           settingRef.update(settingMap).addOnSuccessListener(new OnSuccessListener<Void>() {
               @Override
               public void onSuccess(Void aVoid) {
                   GeneralFunction.handleUploadSuccess(parentActivity,SettingFragment.this,R.string.setting_upload_success,TAG);
                   goToUploadRoomPhotoFragment();
               }
           }).addOnFailureListener(new OnFailureListener() {
               @Override
               public void onFailure(@NonNull Exception e) {
                   GeneralFunction.handleUploadError(parentActivity,SettingFragment.this,R.string.setting_err_upload,TAG);
               }
           });
       }
       else{
           settingRef.set(settingMap).addOnSuccessListener(new OnSuccessListener<Void>() {
               @Override
               public void onSuccess(Void aVoid) {
                   GeneralFunction.handleUploadSuccess(parentActivity,SettingFragment.this,R.string.setting_upload_success,TAG);
                   goToUploadRoomPhotoFragment();
               }
           }).addOnFailureListener(new OnFailureListener() {
               @Override
               public void onFailure(@NonNull Exception e) {
                   GeneralFunction.handleUploadError(parentActivity,SettingFragment.this,R.string.setting_err_upload,TAG);
               }
           });
       }
    }
    private void goToUploadRoomPhotoFragment() {
        HostAnAccommodationFragment hostAnAccommodationFragment = HostAnAccommodationFragment.newInstance(parentActivity);
        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, hostAnAccommodationFragment).addToBackStack(HostAnAccommodationFragment.TAG).commit();
    }
    private void clearSharePreference(){
        GeneralFunction.clearSharedPreferencesWithType(parentActivity,Room.ACCOMMODATION_DETIALS);
        GeneralFunction.clearSharedPreferencesWithType(parentActivity, Amenity.AMENITY_DETAILS);
        GeneralFunction.clearSharedPreferencesWithType(parentActivity, Description.URL_DESCRIPTION);
        GeneralFunction.clearSharedPreferencesWithType(parentActivity, Rules.RULES_DETAILS);
        GeneralFunction.clearSharedPreferencesWithType(parentActivity, Setting.SETTING_DETAILS);
        GeneralFunction.clearSharedPreferencesWithType(parentActivity, Room.UPLOAD_ROOM_DETIALS_ALL);
        GeneralFunction.clearSharedPreferencesWithType(parentActivity, Room.ACCOMMODATION_DETIALS_ALL);
        GeneralFunction.clearSharedPreferencesWithType(parentActivity, Accommodation.UPLOAD_ACCOMMODATION_DETAILS_ALL);
    }

    private void retrieveFromFirestore(){
        final CollectionReference rulesRef = db.collection(Accommodation.URL_ACCOMMODATION).document(documentPushId).collection(Setting.URL_SETTING);
        rulesRef.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.w(TAG, "Listen failed.", e);
                    return;
                }
                String source = queryDocumentSnapshots != null && queryDocumentSnapshots.getMetadata().hasPendingWrites()
                        ? "Local" : "Server";
                Log.d(TAG,source);

                if(queryDocumentSnapshots != null){
                    if(!queryDocumentSnapshots.isEmpty()){

                        for(QueryDocumentSnapshot queryDocumentSnapshot : queryDocumentSnapshots){
                            settingDataset.add(new Setting(parentActivity,queryDocumentSnapshot));
                        }

                        for(DocumentSnapshot documentSnapshot : queryDocumentSnapshots){
                            settingPushId = documentSnapshot.getId();
                        }

                        settingDataset.size();

                        setSettingDetails(settingDataset);

                    }
                }
            }
        });
    }

    private void setSettingDetails(List<Setting> data){
        value = documentPushId;
        if(settingPushId != null){
            getSettingDocumentPath = settingPushId;
        }

        //clear before use ensure it cant loop many
//        editTexts.clear();
//        editTexts.add(guestMoveInSPN);
//        editTexts.add(discountForSPN);
//        editTexts.add(qualifyGetSPN);
//
//        buttons.clear();
//        buttons.add(btn_myApproval);
//        buttons.add(btn_guestMessage_yes);
//        buttons.add(btn_uploadLicense_yes);
//        buttons.add(btn_femaleOnly);
//        buttons.add(btn_boostAccommodation_yes);
//        buttons.add(btn_provideDiscount_yes);

        if(data != null){

            for(Setting text : data) {
                if(text == null || text.type == null){
                    continue;
                }

                guestMoveInSPN.setText(text.guestMoveIn);
                discountForSPN.setText(text.guestGetDiscount);
                qualifyGetSPN.setText(text.guestGetQualify);

                //set button for whether it true or false by compare
                if(text.type.equals(Setting.myApproval)){
                    buttonClickableAtOnce(btn_myApproval,btn_payment,true);
                }
                if(text.type.equals(Setting.payment)){
                    buttonClickableAtOnce(btn_payment,btn_myApproval,true);
                }

                if(text.gender.equals(Setting.guestMale)){
                    buttonClickableAtOnce(btn_maleOnly,btn_femaleOnly,true);
                }
                if(text.gender.equals(Setting.guestFemale)){
                    buttonClickableAtOnce(btn_femaleOnly,btn_maleOnly,true);
                }
                buttonClickableAtOnce(btn_guestMessage_yes, btn_guestMessage_no, text.shouldMessageYou);
                buttonClickableAtOnce(btn_uploadLicense_yes, btn_uploadLicense_no, text.shouldUploadIdentity);
                buttonClickableAtOnce(btn_boostAccommodation_yes, btn_boostAccommodation_no, text.boost);
                buttonClickableAtOnce(btn_provideDiscount_yes, btn_provideDiscount_no, text.discount);
            }
        }
        else{
            Toast.makeText(parentActivity, "Data is null", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if(isFromDatabase){
            retrieveFromFirestore();
        }
        parentActivity.selectHotel();
        parentActivity.setToolbarTitle(parentActivity.getString(R.string.host_an_accommodation));
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
