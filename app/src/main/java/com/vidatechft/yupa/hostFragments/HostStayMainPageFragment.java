package com.vidatechft.yupa.hostFragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.MainSearchFragment.SearchHostRoomDateFragment;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.AmenityShowAdapter;
import com.vidatechft.yupa.adapter.GalleryAdapter;
import com.vidatechft.yupa.adapter.ReviewAdapter;
import com.vidatechft.yupa.adapter.RulesShowAdapter;
import com.vidatechft.yupa.bookHostRoomFragment.EnterGuestDetailsFragment;
import com.vidatechft.yupa.classes.Accommodation;
import com.vidatechft.yupa.classes.Amenity;
import com.vidatechft.yupa.classes.BookingHistory;
import com.vidatechft.yupa.classes.CurrencyRate;
import com.vidatechft.yupa.classes.Description;
import com.vidatechft.yupa.classes.Favourite;
import com.vidatechft.yupa.classes.Host;
import com.vidatechft.yupa.classes.Payment;
import com.vidatechft.yupa.classes.Rating;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.classes.Rules;
import com.vidatechft.yupa.classes.Search;
import com.vidatechft.yupa.classes.Setting;
import com.vidatechft.yupa.classes.User;
import com.vidatechft.yupa.travelKit.TravelKitReceiptLayout;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;
import com.vidatechft.yupa.utilities.WorkaroundMapFragment;
import com.vidatechft.yupa.utilities.YupaRecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nullable;

import static com.vidatechft.yupa.utilities.GeneralFunction.GlideImageSetting;

public class HostStayMainPageFragment extends Fragment implements GeneralFunction.VolleyCallback, OnMapReadyCallback, View.OnClickListener {
    public static final String TAG = HostStayMainPageFragment.class.getName();
    private MainActivity parentActivity;
    private String lastToolbarTitle;
    private Room oriRoom;
    private String oriDescPushId;
    private BookingHistory bookingHistory;
    private Payment payment;
    private boolean isReceipt = false;

    private ConstraintLayout showCoverPicCL;
    private ImageButton favIB;
    private ImageView showCoverPicIV;
    private TextView accNameTV,accLotTV,currencyTypeTV,currencyValueTV,roomTypeTV,guestNumTV,bedRoomQTY,singleBedQTY,queenSizeBedQTY,kingSizeBedQTY,roomDetailsTV;
    private Button btnBookNowMain;
    private Place place;
    private List<Amenity> amenityDataset = new ArrayList<>();
    private AmenityShowAdapter amenityShowAdapter;
    private RulesShowAdapter rulesShowAdapterBooleanAllow, rulesShowAdapterString,rulesShowAdapterBooleanNotAllow, promotionShowAdapter;
    private GalleryAdapter galleryAdapter;
    private YupaRecyclerView amenityRV;
    private ArrayList<CheckBox> checkBoxes;
    private Amenity amenity;
    private ListenerRegistration mainAmenityListener,mainGalleryListener, mainAccommodationGalleryListener, mainFavListener, mainUserListener,mainReviewListener;
    private LinearLayout sectionCL4;
    private TextView amenityViewAllTV;
    private boolean isShowAll = false;
    private GoogleMap map;
    private ViewGroup infoWindow;
    private GeoPoint currentGeoPoint;
    private ScrollView hostStayMainPageSV;
    private ImageView infoIV;
    private TextView infoAccName,infoAdd;
    private ImageButton repositionIB;
    private TextView accAddressTV,openMapTV;
    public boolean not_first_time_showing_info_window = false;
    private Rules rulesBool,rulesString;
    private Setting settingString;
    private boolean isDateCreated = false;
    private boolean isLiked = false;
    private boolean favClicked = false;
    private Host host = new Host();
    private Favourite favourite;
    private SupportMapFragment mapFragment;
    private ReviewAdapter reviewAdapter;
    private List<Rating> mRating = new ArrayList<>();

    private ArrayList<Room> galleryFullList = new ArrayList<>();

    //for when the previous fragment is from search
    private TextView viewMoreTV;
    private Search search;

    public static HostStayMainPageFragment newInstance(MainActivity parentActivity, String lastToolbarTitle, Room oriRoom, String oriDescPushId) {
        HostStayMainPageFragment fragment = new HostStayMainPageFragment();
        fragment.parentActivity = parentActivity;
        fragment.lastToolbarTitle = lastToolbarTitle;
        fragment.oriRoom = oriRoom;
        fragment.oriDescPushId = oriDescPushId;
        fragment.oriRoom.id = oriDescPushId;
        return fragment;
    }

    public static HostStayMainPageFragment newInstance(MainActivity parentActivity, String lastToolbarTitle, Room oriRoom, String oriDescPushId, Search search) {
        HostStayMainPageFragment fragment = new HostStayMainPageFragment();
        fragment.parentActivity = parentActivity;
        fragment.lastToolbarTitle = lastToolbarTitle;
        fragment.oriRoom = oriRoom;
        fragment.oriDescPushId = oriDescPushId;
        fragment.oriRoom.id = oriDescPushId;
        fragment.search = search;
        return fragment;
    }

    //paid/booked homestays
    public static HostStayMainPageFragment newInstance(MainActivity parentActivity, String lastToolbarTitle, Room oriRoom, String oriDescPushId, BookingHistory bookingHistory, Payment payment, boolean isReceipt) {
        HostStayMainPageFragment fragment = new HostStayMainPageFragment();
        fragment.parentActivity = parentActivity;
        fragment.lastToolbarTitle = lastToolbarTitle;
        fragment.oriRoom = oriRoom;
        fragment.oriDescPushId = oriDescPushId;
        fragment.oriRoom.id = oriDescPushId;
        fragment.bookingHistory = bookingHistory;
        fragment.payment = payment;
        fragment.isReceipt = isReceipt;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_host_stay_main_page, container, false);
        initiateVariable(view);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = (SupportMapFragment)  HostStayMainPageFragment.this.getChildFragmentManager().findFragmentById(R.id.accMapFGM);
        if(mapFragment != null){
            mapFragment.getMapAsync(HostStayMainPageFragment.this);
        }

        return view;
    }

    private void initiateVariable(final View v){
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //ConstraintLayout
                showCoverPicCL = v.findViewById(R.id.showCoverPicCL);
                //ImageButton
                favIB = v.findViewById(R.id.favIB);
                repositionIB = v.findViewById(R.id.repositionIB);
                //ImageView
                showCoverPicIV = v.findViewById(R.id.showCoverPicIV);
                //TextView
                accNameTV = v.findViewById(R.id.accNameTV);
                accLotTV = v.findViewById(R.id.accLotTV);
                currencyTypeTV = v.findViewById(R.id.currencyTypeTV);
                currencyValueTV = v.findViewById(R.id.currencyValueTV);
                roomTypeTV = v.findViewById(R.id.roomTypeTV);
                guestNumTV = v.findViewById(R.id.guestNumTV);
                bedRoomQTY = v.findViewById(R.id.bedRoomQTY);
                singleBedQTY = v.findViewById(R.id.singleBedQTY);
                queenSizeBedQTY = v.findViewById(R.id.queenSizeBedQTY);
                kingSizeBedQTY = v.findViewById(R.id.kingSizeBedQTY);
                roomDetailsTV = v.findViewById(R.id.roomDetailsTV);
                viewMoreTV = v.findViewById(R.id.viewMoreTV);
                amenityViewAllTV = v.findViewById(R.id.amenityViewAllTV);
                accAddressTV = v.findViewById(R.id.accAddressTV);
                openMapTV = v.findViewById(R.id.openMapTV);
                //Button
                btnBookNowMain = v.findViewById(R.id.btnBookNowMain);
                btnBookNowMain.setOnClickListener(HostStayMainPageFragment.this);
                //ScrollView
                hostStayMainPageSV = v.findViewById(R.id.hostStayMainPageSV);

                //Constraint Layout
                sectionCL4 = v.findViewById(R.id.sectionCL4);

                favIB.setOnClickListener(HostStayMainPageFragment.this);
                ImageView backIV = v.findViewById(R.id.backIV);
                backIV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        parentActivity.onBackPressed();
                    }
                });

                final RecyclerView galleryRV = v.findViewById(R.id.galleryRV);
                galleryAdapter = new GalleryAdapter(parentActivity,null, galleryFullList);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(parentActivity, LinearLayoutManager.HORIZONTAL,false);
                galleryRV.setAdapter(galleryAdapter);
                galleryRV.setLayoutManager(linearLayoutManager);

                if(oriRoom != null){
                    if(oriRoom.urlOutlook != null){
                        GlideImageSetting(parentActivity,Uri.parse(oriRoom.urlOutlook),showCoverPicIV);
                    }
                    if(oriRoom.accommodationName != null){
                        accNameTV.setText(oriRoom.accommodationName);
                    }
                    if(oriRoom.location != null){
                        accLotTV.setText(oriRoom.location);
                    }
                    if(oriRoom.accommodationGeoPoint != null){
                        currentGeoPoint = oriRoom.accommodationGeoPoint;
//                        final GeneralFunction.VolleyCallback volleyCallback = new GeneralFunction.VolleyCallback() {
//                            @Override
//                            public void onSuccessResponse(HashMap result) {
//
//                            }
//                        };
//                        GeneralFunction.getStateNameByCoordinates(parentActivity,oriRoom.accommodationGeoPoint, volleyCallback);
                       String cityName = GeneralFunction.getStateNameByCoordinates(parentActivity,oriRoom.accommodationGeoPoint);
                        accLotTV.setText(cityName);

                    }
                    if(oriRoom.currencyType != null && oriRoom.currencyValue != null){
                        String localCurrencyCode = GeneralFunction.getCurrencyCode(parentActivity);
                        String price;
                        try{
                            price = GeneralFunction.getDecimalFormat(2).format(CurrencyRate.getUserLocalPrice(parentActivity,localCurrencyCode,oriRoom.currencyType,oriRoom.currencyValue));
                        }catch (Exception e){
                            Log.e(TAG,e.getMessage());
                            price = String.valueOf(oriRoom.currencyValue);
                        }
                        currencyTypeTV.setText(localCurrencyCode);
                        currencyValueTV.setText(price);
                    }

                    if(oriRoom.roomType != null){
                        switch (oriRoom.roomType){
                            case Room.TYPE_ROOM_ENTIRE_PLACE:
                                roomTypeTV.setText(parentActivity.getString(R.string.btn_entirePlace));
                                break;
                            case Room.TYPE_ROOM_PRIVATE:
                                roomTypeTV.setText(parentActivity.getString(R.string.btn_privateRoom));
                                break;
                            case Room.TYPE_ROOM_SHARE:
                                roomTypeTV.setText(parentActivity.getString(R.string.btn_shareRoom));
                                break;
                            default:
                                roomTypeTV.setText(oriRoom.roomType);
                                break;
                        }
                    }
                    if(oriRoom.guestQuantity != null){
                        String guestQTY = String.valueOf(oriRoom.guestQuantity)+" "+Room.GUEST;
                        guestNumTV.setText(guestQTY);
                    }
                    if(oriRoom.bedroomQuantity != null){
                        bedRoomQTY.setText(String.valueOf(oriRoom.bedroomQuantity));
                    }
                    if(oriRoom.singleSizeBedQuantity != null){
                        singleBedQTY.setText(String.valueOf(oriRoom.singleSizeBedQuantity));
                    }
                    if(oriRoom.queenSizeBedQuantity != null){
                        queenSizeBedQTY.setText(String.valueOf(oriRoom.queenSizeBedQuantity));
                    }
                    if(oriRoom.kingSizeBedQuantity != null){
                        kingSizeBedQTY.setText(String.valueOf(oriRoom.kingSizeBedQuantity));
                    }
                    if(oriRoom.address != null){
                        accAddressTV.setText(oriRoom.address);
                    }
                    openMapTV.setOnClickListener(new DebouncedOnClickListener(500) {
                        @Override
                        public void onDebouncedClick(View v) {
                            if(oriRoom.accommodationGeoPoint != null){
                                GeneralFunction.openMapApplicationViaGps(parentActivity,oriRoom.accommodationGeoPoint.getLatitude(),oriRoom.accommodationGeoPoint.getLongitude(),oriRoom.accommodationName);
                            }
                        }
                    });

                }


//                if(oriDescPushId != null){
//                    if(oriDescPushId.descriptionAccommodation != null){
//                        roomDetailsTV.setText(oriDescPushId.descriptionAccommodation);
//                    }
//                }

                //*********************************for receipt usage only START*********************************
                initReceipt(v);
                //*********************************for receipt usage only END*********************************

            }
        });
    }

    private void initReceipt(final View v){
        if(!isReceipt || bookingHistory == null || bookingHistory.paymentId == null){
            v.findViewById(R.id.bookingDetailsLL).setVisibility(View.GONE);
            return;
        }

        favIB.setVisibility(View.GONE);
        btnBookNowMain.setVisibility(View.GONE);
        if(payment != null){
            populateReceipt(v,payment);
        }else{
            GeneralFunction.getFirestoreInstance()
                    .collection(Payment.PAYMENT_RESULT_URL)
                    .document(bookingHistory.paymentId)
                    .get()
                    .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            if(documentSnapshot != null && documentSnapshot.exists()){
                                Payment tempPayment = Payment.getSnapshotToPayment(parentActivity,documentSnapshot);
                                populateReceipt(v,tempPayment);
                            }
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            e.printStackTrace();
                            GeneralFunction.openMessageNotificationDialog(parentActivity,e.getMessage(),R.drawable.notice_bad,TAG);
                        }
                    });
        }
    }

    private void populateReceipt(View v,Payment tempPayment){
        if(tempPayment != null){
            //homestay receipt
            TextView receiptFirstNameTV = v.findViewById(R.id.receiptFirstNameTV);
            TextView receiptLastNameTV = v.findViewById(R.id.receiptLastNameTV);
            TextView receiptEmailTV = v.findViewById(R.id.receiptEmailTV);
            TextView receiptCountryTV = v.findViewById(R.id.receiptCountryTV);
            TextView receiptContactTV = v.findViewById(R.id.receiptContactTV);
            TextView receiptTotalGuestTV = v.findViewById(R.id.receiptTotalGuestTV);
            TextView receiptCheckinTV = v.findViewById(R.id.receiptCheckinTV);
            TextView receiptCheckoutTV = v.findViewById(R.id.receiptCheckoutTV);
            TextView receiptTotalPriceTV = v.findViewById(R.id.receiptTotalPriceTV);

            if(bookingHistory.firstName != null){
                receiptFirstNameTV.setText(bookingHistory.firstName);
            }

            if(bookingHistory.lastName != null){
                receiptLastNameTV.setText(bookingHistory.lastName);
            }

            if(bookingHistory.email != null){
                receiptEmailTV.setText(bookingHistory.email);
            }

            if(bookingHistory.country != null){
                receiptCountryTV.setText(bookingHistory.country);
            }

            if(bookingHistory.contactNo != null){
                String contactNo = bookingHistory.contactNo;
                if(bookingHistory.countryCode != null){
                    contactNo = bookingHistory.countryCode + " " + contactNo;
                }

                receiptContactTV.setText(contactNo);
            }

            receiptTotalGuestTV.setText(String.valueOf(bookingHistory.totalGuest));

            if(bookingHistory.checkinDate != null){
                receiptCheckinTV.setText(GeneralFunction.formatDateToString(bookingHistory.checkinDate));
            }

            if(bookingHistory.checkoutDate != null){
                receiptCheckoutTV.setText(GeneralFunction.formatDateToString(bookingHistory.checkoutDate));
            }

            if(tempPayment.paymentAmount != null && tempPayment.paymentAmount.grandTotal != null && tempPayment.paymentAmount.currencyCode != null){
                String totalPrice = tempPayment.paymentAmount.currencyCode + GeneralFunction.getDecimalFormat(2).format(tempPayment.paymentAmount.grandTotal);
                receiptTotalPriceTV.setText(totalPrice);
            }

            v.findViewById(R.id.bookingDetailsLL).setVisibility(View.VISIBLE);

            //travel kit receipt
            TravelKitReceiptLayout tkReceiptLayout = v.findViewById(R.id.tkReceiptLayout);
            tkReceiptLayout.initLayout(parentActivity,HostStayMainPageFragment.this, tempPayment.id, tempPayment,tkReceiptLayout);
        }
    }

    private void favSetAndGet(){

        mainFavListener = GeneralFunction.getFirestoreInstance()
                .collection(Favourite.URL_FAVOURITE)
                .whereEqualTo(Favourite.TYPE,Favourite.FAV_TYPE_ACC)
                .whereEqualTo(Favourite.TARGET_ID, oriDescPushId)
                .whereEqualTo(Favourite.USER_ID, parentActivity.uid)
                .limit(1)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w(TAG, "Listen failed.", e);
                            return;
                        }

                        String source = queryDocumentSnapshots != null && queryDocumentSnapshots.getMetadata().hasPendingWrites()
                                ? "Local" : "Server";
                        Log.d(TAG,source);

                        if(queryDocumentSnapshots != null){
                            if(!queryDocumentSnapshots.isEmpty()){
                                for (QueryDocumentSnapshot snapshot : queryDocumentSnapshots) {
                                    favourite = snapshot.toObject(Favourite.class);
                                    host.id = snapshot.getId();

                                    host.isFavourite = favourite.isLiked != null && favourite.isLiked;

                                    if(host.isFavourite){
                                        favIB.setImageDrawable(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.ic_love_whole_red,R.color.red));
                                    }else{
                                        favIB.setImageDrawable(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.ic_love,R.color.white));
                                    }
                                }

                            }
                        }
                    }
                });

    }


    @Override
    public void onSuccessResponse(HashMap result) {
        Toast.makeText(parentActivity,result.toString(),Toast.LENGTH_SHORT).show();
    }

    private void getDescription(){
        final ArrayList<String> description = new ArrayList<>();
        GeneralFunction.getFirestoreInstance()
                .collection(Accommodation.URL_ACCOMMODATION)
                .document(oriDescPushId)
                .collection(Description.URL_DESCRIPTION)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w(TAG, "Listen failed.", e);
                            return;
                        }
                        description.clear();

                        if(queryDocumentSnapshots != null){
                            if(!queryDocumentSnapshots.isEmpty()){
                                for(QueryDocumentSnapshot queryDocumentSnapshot : queryDocumentSnapshots){
                                    Description desc = new Description(parentActivity,queryDocumentSnapshot);
                                    description.add(desc.descriptionAccommodation);
                                    roomDetailsTV.setText(desc.descriptionAccommodation);

                                    GeneralFunction.makeTextViewResizableWithButtonMethod(parentActivity, roomDetailsTV, 10,10, parentActivity.getString(R.string.view_more), true,viewMoreTV);

                                    //read more feature
                                    //from here https://stackoverflow.com/questions/19099296/set-text-view-ellipsize-and-add-view-more-at-end
                                    //can use this if want to use HTML version, because it won't go to next line in HTML mode since no <p> or something, so it looks ugly, so cant use this yet. Maybe can use on blog 1
//                                    GeneralFunction.makeTextViewResizable(parentActivity, roomDetailsTV, 3, parentActivity.getString(R.string.view_more), true);

                                }
                            }
                        }
                    }
                });
    }

    private void getAmenity(final View v){

        final YupaRecyclerView amenityRV = v.findViewById(R.id.amenityRV);
        final ArrayList<Amenity> amenityList = new ArrayList<>();
        mainAmenityListener = GeneralFunction.getFirestoreInstance()
                .collection(Accommodation.URL_ACCOMMODATION)
                .document(oriDescPushId)
                .addSnapshotListener(new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot snapshot, @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w(TAG, "Listen failed.", e);
                            return;
                        }

                        if(snapshot != null) {
                            if (snapshot.exists()) {
                                Map<String, Object> oriData = snapshot.getData();
                                if(oriData != null){
                                    amenity = Amenity.toClass(oriData, parentActivity);
                                }

                                HashMap<String,Object> amenityData = Amenity.toMap(amenity,parentActivity);
                                amenityData.size();
                                final List<String> limitAmenityData = new ArrayList<>();
                                limitAmenityData.clear();
                                final List<String> tempAmenityDataAll =  FromValueGetKey(amenityData, true);
                                tempAmenityDataAll.size();

                                if(tempAmenityDataAll.size() > 0 ){
                                    if(tempAmenityDataAll.size() >6){
                                        //try to get 6 value
                                        for(int i= 0; i<6; i++){
                                            limitAmenityData.add(tempAmenityDataAll.get(i));
                                        }
                                        amenityViewAllTV.setVisibility(View.VISIBLE);
                                    }
                                    else{
                                        limitAmenityData.addAll(tempAmenityDataAll);
                                    }

                                    amenityShowAdapter = new AmenityShowAdapter(parentActivity,null,tempAmenityDataAll, limitAmenityData, false);
                                    amenityRV.setNestedScrollingEnabled(false);
                                    amenityRV.setAdapter(amenityShowAdapter);
                                    amenityRV.setLayoutManager(new GridLayoutManager(parentActivity, 2));
                                }
                                else{
                                    sectionCL4.setVisibility(View.GONE);
                                }

                                amenityViewAllTV.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if(amenityViewAllTV.getText().equals(getString(R.string.amenityShowAll)) && !isShowAll){

                                            isShowAll = true;
                                            amenityViewAllTV.setText(R.string.amenityShowLess);
                                            amenityShowAdapter = new AmenityShowAdapter(parentActivity,null,tempAmenityDataAll, limitAmenityData, true);
                                            amenityRV.setNestedScrollingEnabled(false);
                                            amenityRV.setAdapter(amenityShowAdapter);
                                            amenityRV.setLayoutManager(new GridLayoutManager(parentActivity, 2));
                                        }
                                        else{
                                            if(amenityViewAllTV.getText().equals(getString(R.string.amenityShowLess)) && isShowAll){
                                                isShowAll = false;
                                                amenityViewAllTV.setText(R.string.amenityShowAll);
                                                amenityShowAdapter = new AmenityShowAdapter(parentActivity,null,tempAmenityDataAll, limitAmenityData, false);
                                                amenityRV.setNestedScrollingEnabled(false);
                                                amenityRV.setAdapter(amenityShowAdapter);
                                                amenityRV.setLayoutManager(new GridLayoutManager(parentActivity, 2));
                                            }
                                        }

                                    }
                                });
                            }
                        }
                    }
                });
    }

    @Override
    public void onPause() {
        super.onPause();
        parentActivity.showToolbar();
        parentActivity.showBtmNav();

        if(mainFavListener != null){
            mainFavListener.remove();
        }
        if(mainAmenityListener != null){
            mainAmenityListener.remove();
        }
        if(mainGalleryListener != null){
            mainGalleryListener.remove();
        }
        if(mainAccommodationGalleryListener != null){
            mainAccommodationGalleryListener.remove();
        }
        if(mainUserListener != null){
            mainUserListener.remove();
        }
        if(mainReviewListener != null){
            mainReviewListener.remove();
        }
        mapFragment.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                parentActivity.setToolbarTitle(lastToolbarTitle);
                if(mainAmenityListener != null){
                    mainAmenityListener.remove();
                }
                if(mainGalleryListener != null){
                    mainGalleryListener.remove();
                }
                if(mainAccommodationGalleryListener != null){
                    mainAccommodationGalleryListener.remove();
                }
                if(mainFavListener != null){
                    mainFavListener.remove();
                }
                if(mainUserListener != null){
                    mainUserListener.remove();
                }
                if(mainReviewListener != null){
                    mainReviewListener.remove();
                }
                mapFragment.onDestroy();
            }
        });
    }

    private static ArrayList<String> FromValueGetKey(Map<String, Object> map, Boolean value) {
        Set<Map.Entry<String, Object>> set = map.entrySet();
        ArrayList<String> arr = new ArrayList<String>();
        for (Map.Entry<String, Object> aSet : set) {
            Map.Entry entry = (Map.Entry) aSet;
            if (entry.getValue() != null) {
                String s = (String) entry.getKey();
                arr.add(s);
            }
        }
        return arr;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        setupInfoWindow(map);
        populateMap();
    }

    private void setupInfoWindow(GoogleMap mMap){
        // Add a marker in Sydney and move the camera
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(infoWindow == null){
                    infoWindow = (ViewGroup)getLayoutInflater().inflate(R.layout.info_window_google_map_marker_with_image, infoWindow);
                }

                //declare variable below
                infoIV = infoWindow.findViewById(R.id.infoIV);
                infoAccName = infoWindow.findViewById(R.id.infoAccName);
                infoAdd = infoWindow.findViewById(R.id.infoAdd);

//                if(oriRoom.urlOutlook != null){
//                    try {
//                        GlideImageSettingShrinkImage(parentActivity,Uri.parse(oriRoom.urlOutlook),infoIV);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }

                HostStayMainPageFragment.this.map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                    @Override
                    public View getInfoWindow(Marker marker) {
                        if(oriRoom != null){
                            if(oriRoom.accommodationName != null){
                                infoAccName.setText(oriRoom.accommodationName);
                                infoAccName.requestLayout();
                            }

                            if(oriRoom.address != null){
                                infoAdd.setText(oriRoom.address);
                                infoAdd.requestLayout();
                            }

                            if(oriRoom.urlOutlook != null){
                                GeneralFunction.GlideImageSettingShrinkImage(parentActivity,Uri.parse(oriRoom.urlOutlook),infoIV,marker);
                            }
                        }
                        if(oriRoom != null){
                            return infoWindow;
                        }
                        else{
                            return null;
                        }
                    }

                    @Override
                    public View getInfoContents(final Marker marker) {
                      return null;
                    }
                });
            }
        });

    }

    private void populateMap(){
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((WorkaroundMapFragment) getChildFragmentManager().findFragmentById(R.id.accMapFGM))
                        .setListener(new WorkaroundMapFragment.OnTouchListener() {
                            @Override
                            public void onTouch()
                            {
                                hostStayMainPageSV.requestDisallowInterceptTouchEvent(true);
                            }
                        });
                map.clear();
                map.getUiSettings().setZoomControlsEnabled(true);
                    if(currentGeoPoint != null){
                        LatLng current = new LatLng(currentGeoPoint.getLatitude(),currentGeoPoint.getLongitude());
                        map.addMarker(new MarkerOptions()
                                .title(parentActivity.getString(R.string.you_are_here))
                                .position(current)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_gps)))
                                .setZIndex(0.1f);

                        final CameraPosition cameraPosition = new CameraPosition.Builder()
                                .target(current)
                                .zoom(15)
                                .bearing(0)
                                .tilt(0)
                                .build();

                        map.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                        repositionIB.setOnClickListener(new DebouncedOnClickListener(500) {
                            @Override
                            public void onDebouncedClick(View v) {
                                map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                            }
                        });

//                        CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
//                                current, Job.JOB_ZOOM_IN_LEVEL);//the bigger the number, the deeper it goes
//                        map.animateCamera(location);
                    }



            }
        });
    }

    private void getGallery(View v){
        final ArrayList<Room> galleryList = new ArrayList<>();
        mainGalleryListener = GeneralFunction.getFirestoreInstance()
                .collection(Accommodation.URL_ACCOMMODATION)
                .document(oriDescPushId)
                .collection(Room.URL_ROOM)
                .orderBy(Room.POSITION, Query.Direction.ASCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w(TAG, "Listen failed.", e);
                            return;
                        }
                        galleryList.clear();
                        if(queryDocumentSnapshots != null){

                            if(!queryDocumentSnapshots.isEmpty()){
                                for(QueryDocumentSnapshot queryDocumentSnapshot : queryDocumentSnapshots){
                                    Room tempGalleryList = Room.snapshotToRoom(parentActivity,queryDocumentSnapshot);
                                    galleryList.add(tempGalleryList);
                                }
                            }
                        }
                        galleryFullList.addAll(galleryList);
                        galleryAdapter.notifyDataSetChanged();
                    }
                });
    }

    private void getAccommodationGallery(View v){
        final ArrayList<Room> galleryList = new ArrayList<>();
        mainAccommodationGalleryListener = GeneralFunction.getFirestoreInstance()
                .collection(Accommodation.URL_ACCOMMODATION)
                .document(oriDescPushId)
                .collection(Accommodation.URL_ACCOMMODATION)
                .orderBy(Room.POSITION, Query.Direction.ASCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w(TAG, "Listen failed.", e);
                            return;
                        }
                        galleryList.clear();
                        if(queryDocumentSnapshots != null){
                            if(!queryDocumentSnapshots.isEmpty()){
                                for(QueryDocumentSnapshot queryDocumentSnapshot : queryDocumentSnapshots){
                                    Room tempGalleryList = Room.snapshotToRoom(parentActivity,queryDocumentSnapshot);
                                    if(tempGalleryList != null){
                                        try{
                                            tempGalleryList.roomName = (String) queryDocumentSnapshot.get(Accommodation.ACCOMMODATION_NAME);
                                            tempGalleryList.urlRoomPic = (String) queryDocumentSnapshot.get(Accommodation.URL_ACCOMMODATION_PIC);
                                        }catch (Exception exception){
                                            tempGalleryList.roomName = null;
                                            tempGalleryList.urlRoomPic = null;
                                        }
                                        galleryList.add(tempGalleryList);
                                    }
                                }
                            }
                        }
                        galleryFullList.addAll(galleryList);
                        galleryAdapter.notifyDataSetChanged();
                    }
                });
    }

    private void getRules(final View v){
        final RecyclerView rulesRV = v.findViewById(R.id.rulesRV);
        final RecyclerView allowRV = v.findViewById(R.id.allowRV);
        final RecyclerView notAllowRV = v.findViewById(R.id.notAllowRV);

        GeneralFunction.getFirestoreInstance()
                .collection(Accommodation.URL_ACCOMMODATION)
                .document(oriDescPushId)
                .collection(Rules.URL_RULES)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w(TAG, "Listen failed.", e);
                            return;
                        }
                        if(queryDocumentSnapshots != null){
                            if(!queryDocumentSnapshots.isEmpty()){

                                for(QueryDocumentSnapshot queryDocumentSnapshot : queryDocumentSnapshots){
                                    Map<String, Object> oriData = queryDocumentSnapshot.getData();
                                    if(oriData != null){
                                        rulesBool = Rules.mapToClassBoolean(oriData, parentActivity);
                                        rulesString = Rules.mapToClassString(oriData,parentActivity);
                                    }
                                }
                                HashMap<String,Object> rulesDataBool = Rules.classToMapBoolean(rulesBool);
                                final HashMap<String,Object> rulesDataString = Rules.classToMapString(rulesString);
                                rulesDataBool.size();
                                final List<String> rulesBoolDataTrue = getQuerysnapshotKeyAndValue(rulesDataBool,true, true);
                                final List<String> rulesBoolDataFalse = getQuerysnapshotKeyAndValue(rulesDataBool,true, false);
//                                final List<String> rulesStringData = getQuerysnapshotKeyAndValue(rulesDataString,false);

                                getSetting(v,rulesDataString, rulesBoolDataTrue, rulesBoolDataFalse,rulesRV,allowRV,notAllowRV);

                            }
                        }
                    }
                });
    }

    private void getSetting(final View v, final HashMap<String,Object> settingData, final List<String> rulesBoolDataTrue, final List<String> rulesBoolDataFalse, final RecyclerView rulesRV, final RecyclerView allowRV, final RecyclerView notAllowRV){
        final RecyclerView promotionRV = v.findViewById(R.id.promotionRV);
        GeneralFunction.getFirestoreInstance()
                .collection(Accommodation.URL_ACCOMMODATION)
                .document(oriDescPushId)
                .collection(Setting.URL_SETTING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w(TAG, "Listen failed.", e);
                            return;
                        }
                        final HashMap<String,Object> settingData2 = new HashMap<>();
                        if(queryDocumentSnapshots != null){
                            if(!queryDocumentSnapshots.isEmpty()){
                                for(QueryDocumentSnapshot queryDocumentSnapshot : queryDocumentSnapshots){
                                    Map<String,Object> oriSetting = queryDocumentSnapshot.getData();
                                    Setting setting = Setting.mapToClassString(oriSetting,parentActivity,0);
                                    HashMap<String,Object> tempSettingData = Setting.classToMapString(setting,0);

                                    Setting setting2 = Setting.mapToClassString(oriSetting,parentActivity,1);
                                    HashMap<String,Object> tempSettingData2 = Setting.classToMapString(setting2,1);
                                    settingData.putAll(tempSettingData);
                                    settingData2.putAll(tempSettingData2);
                                }

                                parentActivity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        //show rules part
                                        rulesShowAdapterString = new RulesShowAdapter(parentActivity,null,rulesBoolDataTrue,rulesBoolDataFalse,settingData, false, null,false);
                                        rulesRV.setNestedScrollingEnabled(false);
                                        rulesRV.setAdapter(rulesShowAdapterString);
                                        rulesRV.setLayoutManager(new GridLayoutManager(parentActivity, 1));

                                        //show allow part
                                        rulesShowAdapterBooleanAllow = new RulesShowAdapter(parentActivity,null,rulesBoolDataTrue,rulesBoolDataFalse,settingData, true, true,false);
                                        allowRV.setNestedScrollingEnabled(false);
                                        allowRV.setAdapter(rulesShowAdapterBooleanAllow);
                                        allowRV.setLayoutManager(new GridLayoutManager(parentActivity, 1));

                                        //show not allow part
                                        rulesShowAdapterBooleanNotAllow = new RulesShowAdapter(parentActivity,null,rulesBoolDataTrue,rulesBoolDataFalse,settingData, true, false,false);
                                        notAllowRV.setNestedScrollingEnabled(false);
                                        notAllowRV.setAdapter(rulesShowAdapterBooleanNotAllow);
                                        notAllowRV.setLayoutManager(new GridLayoutManager(parentActivity, 1));

                                        //show promotion
                                        List<String> tempSetingData2 = new ArrayList<>();
                                        for(Map.Entry<String, Object> entry: settingData2.entrySet()) {
                                            String key = entry.getKey();
                                            Object value = entry.getValue();

                                            String guestGetDiscount = null;
                                            String guestGetQualify = null;
                                            if(key.equals(Setting.GUEST_GET_DISCOUNTS)){
                                                guestGetDiscount = value.toString();
                                                tempSetingData2.add(guestGetDiscount);
                                            }

                                            if(key.equals(Setting.GUEST_GET_QUALIFY)){
                                                guestGetQualify = value.toString();
                                                tempSetingData2.add(guestGetQualify);
                                            }
                                        }

                                        LinearLayout sectionCL11 = v.findViewById(R.id.sectionCL11);
                                        if(tempSetingData2.get(0) != null && !tempSetingData2.get(0).trim().isEmpty() && tempSetingData2.get(1) != null && !tempSetingData2.get(1).trim().isEmpty()){
                                            List<String> settingDataFinal2 = Collections.singletonList(tempSetingData2.get(0) +" and get "+ tempSetingData2.get(1));
                                            promotionShowAdapter = new RulesShowAdapter(parentActivity,null,rulesBoolDataTrue,rulesBoolDataFalse,settingDataFinal2, false, false,true);
                                            promotionRV.setNestedScrollingEnabled(false);
                                            promotionRV.setAdapter(promotionShowAdapter);
                                            promotionRV.setLayoutManager(new GridLayoutManager(parentActivity, 1));
                                            sectionCL11.setVisibility(View.VISIBLE);
                                        }else{
                                            sectionCL11.setVisibility(View.GONE);
                                        }

                                    }
                                });
                            }
                        }
                    }
                });
    }

    private List<String> getQuerysnapshotKeyAndValue(HashMap<String, Object> meMap,boolean isBoolMethod, boolean isGetTrueValue){
        List<String> ruleStringValue = new ArrayList<>();
        if(isBoolMethod){
            if(isGetTrueValue){
                for (Object key : meMap.keySet()) {
                    Boolean value = (Boolean) meMap.get(key);
                    if(value.equals(true)){
                        ruleStringValue.add(key.toString());
                    }
                }
                return ruleStringValue;
            }
            else{
                for (Object key : meMap.keySet()) {
                    Boolean value = (Boolean) meMap.get(key);
                    if(value.equals(false)){
                        ruleStringValue.add(key.toString());
                    }
                }
                return ruleStringValue;
            }

        }
        else{
            for (Object key : meMap.keySet()) {
                String value = (String) meMap.get(key);
                ruleStringValue.add(value);

            }
            return ruleStringValue;
        }
    }

    private void getRating(View v){
        if(GeneralFunction.isGuest()){
            v.findViewById(R.id.sectionCL12).setVisibility(View.GONE);
            return;
        }

        final ImageView reviewProfilePicIV = v.findViewById(R.id.reviewProfilePicIV);
        final RatingBar reviewRB = v.findViewById(R.id.reviewRB);
        final Map<String,Object> userList = new HashMap<>();

        mainReviewListener = GeneralFunction.getFirestoreInstance()
                .collection(Accommodation.URL_ACCOMMODATION)
                .document(oriDescPushId)
                .collection(Rating.URL_RATING)
                .document(parentActivity.uid)
                .addSnapshotListener(new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot snapshot, @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w(TAG, "Listen failed.", e);
                            return;
                        }
                        if(snapshot != null){
                            if(snapshot.exists()){
                                final Rating rating = new Rating(parentActivity,snapshot);
                                if(rating.rating != 0.0f){
                                    reviewRB.setRating(rating.rating.floatValue());
                                }
                                if(rating.review != null){

                                }
                                if(rating.dateCreated != null){
                                    isDateCreated = true;
                                }

                            }
                        }
                    }
                });

        //get review current user profile
        mainUserListener = GeneralFunction.getFirestoreInstance().collection(User.URL_USER)
                .document(parentActivity.uid)
                .addSnapshotListener(new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot snapshot, @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w(TAG, "Listen failed.", e);
                            return;
                        }
                        userList.clear();
                        if(snapshot != null){
                            if(snapshot.exists()){
                                final User user = new User(parentActivity,snapshot);
                                final List<User> userList1 = new ArrayList<>();
                                userList1.add(user);
                                if(user.profilePicUrl != null){
                                    userList.put(User.PROFILE_PIC_URL,user.profilePicUrl);
                                    GeneralFunction.GlideCircleImageSetting(parentActivity,Uri.parse(user.profilePicUrl),reviewProfilePicIV);
                                }
                                reviewRB.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                                    @Override
                                    public void onRatingChanged(final RatingBar ratingBar, float rating, boolean fromUser) {
                                        if(fromUser){
                                            Map<String, Object> rateMap = new HashMap<>();

                                            if(ratingBar.getRating() != 0.0f){
                                                rateMap.put(Rating.RATING, ratingBar.getRating());
                                                if(!isDateCreated){
                                                    rateMap.put(Rating.DATE_CREATED,FieldValue.serverTimestamp());
                                                }
                                                rateMap.put(Rating.DATE_UPDATED,FieldValue.serverTimestamp());
                                            }

                                            DocumentReference rateRef = GeneralFunction.getFirestoreInstance()
                                                    .collection(Accommodation.URL_ACCOMMODATION)
                                                    .document(oriDescPushId)
                                                    .collection(Rating.URL_RATING)
                                                    .document(parentActivity.uid);

                                            if(rateMap.size() != 0){
                                                if(isDateCreated){
                                                    rateRef.update(rateMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            if(task.isSuccessful()){

                                                            }
                                                        }
                                                    }).addOnFailureListener(new OnFailureListener() {
                                                        @Override
                                                        public void onFailure(@NonNull Exception e) {
                                                            if(e != null){
                                                                Toast.makeText(parentActivity,e.toString(),Toast.LENGTH_SHORT).show();
                                                            }
                                                        }
                                                    });
                                                    RatingFragment fragment = RatingFragment.newInstance(parentActivity,ratingBar.getRating(),user,oriDescPushId, isDateCreated);
                                                    GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, fragment).addToBackStack(RatingFragment.TAG).commit();
                                                }
                                                else{
                                                    rateRef.set(rateMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            if(task.isSuccessful()){
                                                                RatingFragment fragment = RatingFragment.newInstance(parentActivity,ratingBar.getRating(),user,oriDescPushId, isDateCreated);
                                                                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, fragment).addToBackStack(RatingFragment.TAG).commit();
                                                            }
                                                        }
                                                    }).addOnFailureListener(new OnFailureListener() {
                                                        @Override
                                                        public void onFailure(@NonNull Exception e) {
                                                            if(e != null){
                                                                Toast.makeText(parentActivity,e.toString(),Toast.LENGTH_SHORT).show();
                                                            }
                                                        }
                                                    });
//                                                    RatingFragment fragment = RatingFragment.newInstance(parentActivity,ratingBar.getRating(),user,oriDescPushId, isDateCreated);
//                                                    GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, fragment).addToBackStack(RatingFragment.TAG).commit();
                                                }
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    }
                });


    }

    private void getReview(final View v){
        final RecyclerView reviewRV = v.findViewById(R.id.reviewRV);
        final List<String> documentPushId = new ArrayList<>();
        final List<Rating> cc = new ArrayList<>();
        final Map<String, Rating> reviewList = new HashMap<>();
        final Accommodation accommodation = new Accommodation();

         GeneralFunction.getFirestoreInstance()
                .collection(Accommodation.URL_ACCOMMODATION)
                .document(oriDescPushId)
                .collection(Rating.URL_RATING)
                .orderBy(Rating.DATE_CREATED, Query.Direction.DESCENDING)
                .limit(2)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if(task.isSuccessful()){

                            documentPushId.clear();
                            reviewList.clear();
                            cc.clear();
                            if(task != null){

                                if(task.getResult() != null){
                                    for (final QueryDocumentSnapshot queryDocumentSnapshot : task.getResult()){
                                        final Rating rating = new Rating(parentActivity, queryDocumentSnapshot);
                                        reviewList.put(queryDocumentSnapshot.getId(),rating);
                                        //todo document id
                                        accommodation.id  = oriDescPushId;
                                        reviewList.size();
                                        GeneralFunction.getFirestoreInstance()
                                                .collection(User.URL_USER)
                                                .document(queryDocumentSnapshot.getId())
                                                .get()
                                                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                        if(task.isSuccessful()){
                                                            final User user = new User(parentActivity, task.getResult());
                                                            if(reviewList.get(user.id) != null){
                                                                reviewList.get(user.id).user = user;
                                                            }

                                                            GeneralFunction.getFirestoreInstance()
                                                                    .collection(Favourite.URL_FAVOURITE)
                                                                    .whereEqualTo(Favourite.ACCOMMODATION_ID, oriDescPushId)
                                                                    .whereEqualTo(Favourite.TARGET_ID, queryDocumentSnapshot.getId())
                                                                    .whereEqualTo(Favourite.TYPE, Favourite.FAV_TYPE_RATING)
                                                                    .whereEqualTo(Favourite.USER_ID, parentActivity.uid)
                                                                    .get()
                                                                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                                                        @Override
                                                                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                                            if(task.isSuccessful()){
                                                                                if(!task.getResult().isEmpty()){
//                                                                                for(QueryDocumentSnapshot queryDocumentSnapshot1 : task.getResult()){
                                                                                    reviewList.get(user.id).favourite = new Favourite(parentActivity, task.getResult());
//                                                                                    favourite = queryDocumentSnapshot1.toObject(Favourite.class);
//                                                                                    reviewList.get(user.id).favourite.isLiked = favourite.isLiked;
                                                                                    rating.favourite = reviewList.get(user.id).favourite;
//                                                                                }
                                                                                }

                                                                                cc.add(reviewList.get(user.id));
                                                                                cc.size();
                                                                                mRating.add(reviewList.get(user.id));

                                                                                parentActivity.runOnUiThread(new Runnable() {
                                                                                    @Override
                                                                                    public void run() {
                                                                                        reviewAdapter = new ReviewAdapter(parentActivity,cc,accommodation);
                                                                                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(parentActivity);
                                                                                        reviewRV.setAdapter(reviewAdapter);
                                                                                        reviewAdapter.notifyDataSetChanged();
                                                                                        //when in scrollview prevent laggy
                                                                                        reviewRV.setNestedScrollingEnabled(false);
                                                                                        reviewRV.setHasFixedSize(true);
                                                                                        reviewRV.setLayoutManager(linearLayoutManager);
                                                                                        reviewRV.setVisibility(View.VISIBLE);

                                                                                        LinearLayout sectionCL13 = v.findViewById(R.id.sectionCL13);
                                                                                        if(reviewAdapter.getItemCount() > 0){
                                                                                            sectionCL13.setVisibility(View.VISIBLE);
                                                                                        }else{
                                                                                            sectionCL13.setVisibility(View.GONE);
                                                                                        }
                                                                                    }
                                                                                });
                                                                            }
                                                                        }
                                                                    });
                                                        }
                                                    }
                                                }).addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                if(e!=null){
                                                    Toast.makeText(parentActivity,e.toString(),Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        GeneralFunction.Toast(parentActivity,e.toString());
                    }
                });

        v.findViewById(R.id.reviewViewAll).setOnClickListener(new DebouncedOnClickListener(500) {
            @Override
            public void onDebouncedClick(View v) {
                Fragment reviewViewAllFragment = ReviewViewAllFragment.newInstance(parentActivity, oriDescPushId);
                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, reviewViewAllFragment).addToBackStack(ReviewViewAllFragment.TAG).commit();
            }
        });
    }

    private void bookNow(){
        if(search != null && search.checkin_date > 0 && search.checkout_date > 0){
            parentActivity.initializeLoadingDialog(parentActivity.getString(R.string.checking_availability),false);
            parentActivity.controlLoadingDialog(true,parentActivity);

            GeneralFunction.getFirestoreInstance()
                    .collection(BookingHistory.URL_BOOKING_HISTORY)
                    .whereEqualTo(BookingHistory.TARGET_ID,oriRoom.id)
                    .whereEqualTo(BookingHistory.ACTION_TYPE,BookingHistory.ACTION_TYPE_ACCEPTED)
                    .whereGreaterThanOrEqualTo(BookingHistory.CHECKOUT_DATE,search.checkin_date)
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if(GeneralFunction.hasError(parentActivity,task,TAG)){
                                return;
                            }

                            boolean isClashed = false;

                            if(task.getResult() != null && !task.getResult().isEmpty()){
                                for(DocumentSnapshot bkHistorySnapshot : task.getResult().getDocuments()){
                                    BookingHistory tempBookingHistory = bkHistorySnapshot.toObject(BookingHistory.class);
                                    if (tempBookingHistory != null && tempBookingHistory.checkinDate != null && tempBookingHistory.checkoutDate != null) {
                                        isClashed = GeneralFunction.isSearchedDateClashedWithOtherBookedDates(
                                                tempBookingHistory.checkinDate,tempBookingHistory.checkoutDate,search.checkin_date,search.checkout_date);
                                        if(isClashed){
                                            break;
                                        }
                                    }
                                }
                            }

                            parentActivity.controlLoadingDialog(false,parentActivity);
                            if(!isClashed){
                                Fragment enterGuestDetailsFragment = EnterGuestDetailsFragment.newInstance(parentActivity,search,oriRoom);
                                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, enterGuestDetailsFragment).addToBackStack(EnterGuestDetailsFragment.TAG).commit();
                            }else{
                                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.date_clashed_please_select_another_date),R.drawable.notice_bad,TAG);
                            }
                        }
                    });
        }else{
            Fragment searchHostRoomDateFragment = SearchHostRoomDateFragment.newInstance(parentActivity, new Search(), oriRoom);
            GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, searchHostRoomDateFragment).addToBackStack(SearchHostRoomDateFragment.TAG).commit();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.favIB:
                if(GeneralFunction.isGuest()){
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_guest_restrict_usage),R.drawable.notice_bad,TAG);
                    return;
                }
                handleLikeAction(false);
                break;
            case R.id.btnBookNowMain:
                if(GeneralFunction.isGuest()){
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_guest_restrict_usage),R.drawable.notice_bad,TAG);
                    return;
                }
                bookNow();
                break;
//            case R.id.btn_addToFav:
//                if(GeneralFunction.isGuest()){
//                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_guest_restrict_usage),R.drawable.notice_bad,TAG);
//                    return;
//                }
//                handleLikeAction(true);
//                break;
        }
    }

    private void handleLikeAction(boolean forceLike){
        DocumentReference favRef;

        if(host == null || host.isFavourite == null ||host.id == null){
            favRef = GeneralFunction.getFirestoreInstance()
                    .collection(Favourite.URL_FAVOURITE)
                    .document();

            favourite = new Favourite();

            host.id = favRef.getId();

            host.isFavourite = true;
        }else{
            favRef = GeneralFunction.getFirestoreInstance()
                    .collection(Favourite.URL_FAVOURITE)
                    .document(host.id);

            host.isFavourite = !host.isFavourite;
        }

        if(forceLike){
            host.isFavourite = true;
        }

        favourite.userId = parentActivity.uid;
        favourite.targetId = oriDescPushId;
        favourite.type = Favourite.FAV_TYPE_ACC;
        favourite.isLiked = host.isFavourite;
        HashMap<String,Object> favMap = Favourite.toMap(favourite);

        if(favourite.dateCreated == null){
            favMap.put(Favourite.DATE_CREATED, FieldValue.serverTimestamp());
        }
        favMap.put(Favourite.DATE_UPDATED, FieldValue.serverTimestamp());

        favRef.set(favMap);

        if(favourite.isLiked){
            favIB.setImageDrawable(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.ic_love_whole_red,R.color.red));
        }else{
            favIB.setImageDrawable(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.ic_love,R.color.white));
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapFragment.onLowMemory();
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.selectHotel();
        parentActivity.hideToolbar();
        parentActivity.hideBtmNav();

        View v = getView();
        if(v != null){
            favSetAndGet();
            getDescription();
            getAmenity(v);
            getGallery(v);
            getAccommodationGallery(v);
            getRules(v);
            getRating(v);
            getReview(v);
        }

        mapFragment.onResume();
    }

    //needs this to prevent duplicate id of map and autocomplete that is making the app crash
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        MapFragment f = (MapFragment) parentActivity.getFragmentManager()
                .findFragmentById(R.id.accMapFGM);
        if (f != null)
            parentActivity.getFragmentManager().beginTransaction().remove(f).commit();
    }
}
