package com.vidatechft.yupa.hostFragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.BookHomestayDetails;
import com.vidatechft.yupa.classes.BookingHistory;
import com.vidatechft.yupa.classes.User;
import com.vidatechft.yupa.employerFragments.PTJobSwipeFragment;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.HashMap;

public class BookingRequestFragment extends Fragment implements View.OnClickListener{
    public static final String TAG = BookingRequestFragment.class.getName();
    private MainActivity parentActivity;
    private BookingRequestListFragment parentFragment;
    private BookingHistory bookingHistory;
    private int position;

    private TextView bookingRequestHouseTV,nameTV,liveInTV,joinedTV,bookingDateTV,totalGuestTV,travelPurposeTV,firstNameTV,lastNameTV,emailTV,countryTV,contactTV,drivingLicenseTV;
    private ImageView profileIV,drivingLicenseIV,receiptIV;
    private LinearLayout receiptLL;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static BookingRequestFragment newInstance(MainActivity parentActivity, BookingRequestListFragment parentFragment, BookingHistory bookingHistory, int position) {
        BookingRequestFragment fragment = new BookingRequestFragment();
        fragment.parentActivity = parentActivity;
        fragment.parentFragment = parentFragment;
        fragment.bookingHistory = bookingHistory;
        fragment.position = position;
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_booking_request, container, false);
        if(bookingHistory == null || bookingHistory.user == null || bookingHistory.checkinDate == null || bookingHistory.checkoutDate == null || bookingHistory.targetId == null){
            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_getting_booking_history_details_of_homestay),R.drawable.notice_bad,TAG);
            parentActivity.onBackPressed();
            return view;
        }

        initView(view);
        if(bookingHistory != null){
            if(bookingHistory.room != null && bookingHistory.room.accommodationName != null){
                bookingRequestHouseTV.setText(String.format(parentActivity.getString(R.string.bookingRequestHouseTV),bookingHistory.room.accommodationName));
            }

            if(bookingHistory.user != null){
                populateUserDetails();
            }

            populateHomestayBookingDetails();
        }


        return view;
    }

    private void initView(View view){
        profileIV = view.findViewById(R.id.profileIV);
        nameTV = view.findViewById(R.id.nameTV);
        liveInTV = view.findViewById(R.id.liveInTV);
        joinedTV = view.findViewById(R.id.joinedTV);

        bookingDateTV = view.findViewById(R.id.bookingDateTV);
        totalGuestTV = view.findViewById(R.id.totalGuestTV);
        travelPurposeTV = view.findViewById(R.id.travelPurposeTV);
        firstNameTV = view.findViewById(R.id.firstNameTV);
        lastNameTV = view.findViewById(R.id.lastNameTV);
        emailTV = view.findViewById(R.id.emailTV);
        countryTV = view.findViewById(R.id.countryTV);
        contactTV = view.findViewById(R.id.contactTV);
        drivingLicenseIV = view.findViewById(R.id.drivingLicenseIV);
        drivingLicenseTV = view.findViewById(R.id.drivingLicenseTV);
        bookingRequestHouseTV = view.findViewById(R.id.bookingRequestHouseTV);

        receiptLL = view.findViewById(R.id.receiptLL);
        receiptIV = view.findViewById(R.id.receiptIV);

        Button approveBtn = view.findViewById(R.id.approveBtn);
        Button declineBtn = view.findViewById(R.id.declineBtn);
        approveBtn.setOnClickListener(this);
        declineBtn.setOnClickListener(this);
    }

    private void populateUserDetails(){
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                User user = bookingHistory.user;
                if(user.profilePicUrl != null){
                    GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(user.profilePicUrl),profileIV);
                }

                if(user.name != null){
                    nameTV.setText(user.name);
                }

                if(user.country != null){
                    if(user.state != null && !user.state.isEmpty()){
                        liveInTV.setText(String.format(parentActivity.getString(R.string.user_live_in_both), user.state, user.country));
                    }else{
                        liveInTV.setText(String.format(parentActivity.getString(R.string.user_live_in_country), user.country));
                    }
                }

                if(user.dateCreated != null){
                    joinedTV.setText(String.format(parentActivity.getString(R.string.user_joined_in), GeneralFunction.getSdf().format(user.dateCreated.toDate().getTime())));
                }
            }
        });
    }

    private void populateHomestayBookingDetails(){
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if(bookingHistory.checkinDate != null && bookingHistory.checkoutDate != null){
                    String dateString;
                    if(bookingHistory.checkinDate.equals(bookingHistory.checkoutDate)){
                        dateString = GeneralFunction.formatDateToString(bookingHistory.checkinDate);
                    }else{
                        dateString = GeneralFunction.formatDateToStringWithSlash(bookingHistory.checkinDate) + " - " + GeneralFunction.formatDateToStringWithSlash(bookingHistory.checkoutDate);
                    }
                    bookingDateTV.setText(dateString);
                }else{
                    bookingDateTV.setText(parentActivity.getString(R.string.error_occured));
                }

                totalGuestTV.setText(String.valueOf(bookingHistory.totalGuest));

                if(bookingHistory.purpose != null){
                    travelPurposeTV.setText(bookingHistory.purpose);
                }

                if(bookingHistory.firstName != null){
                    firstNameTV.setText(bookingHistory.firstName);
                }

                if(bookingHistory.lastName != null){
                    lastNameTV.setText(bookingHistory.lastName);
                }

                if(bookingHistory.email != null){
                    emailTV.setText(bookingHistory.email);
                }

                if(bookingHistory.country != null){
                    countryTV.setText(bookingHistory.country);
                }

                if(bookingHistory.contactNo != null){
                    contactTV.setText(bookingHistory.contactNo);
                }

                if(bookingHistory.licenseUrl != null){
                    GeneralFunction.GlideImageSetting(parentActivity,Uri.parse(bookingHistory.licenseUrl),drivingLicenseIV);
                    drivingLicenseTV.setVisibility(View.GONE);
                }else{
                    drivingLicenseTV.setVisibility(View.VISIBLE);
                }

                if(bookingHistory.paymentReceiptUrl != null && bookingHistory.isPaid != null && bookingHistory.isPaid){
                    GeneralFunction.GlideImageSetting(parentActivity,Uri.parse(bookingHistory.paymentReceiptUrl),receiptIV);
                    receiptLL.setVisibility(View.VISIBLE);
                }else{
                    receiptLL.setVisibility(View.GONE);
                }

            }
        });
    }

    private void updateHomestayBooking(boolean isApproved){
        final HashMap<String,Object> updateMap = new HashMap<>();
        parentActivity.initializeLoadingDialog(parentActivity.getString(R.string.updating),false);
        parentActivity.controlLoadingDialog(true,parentActivity);

        updateMap.put(BookHomestayDetails.DATE_UPDATED, FieldValue.serverTimestamp());
//        updateMap.put(BookHomestayDetails.REMARK, "some remark");
        if(isApproved){
            updateMap.put(BookHomestayDetails.ACTION_TYPE, BookingHistory.ACTION_TYPE_ACCEPTED);
            GeneralFunction.getFirestoreInstance()
                    .collection(BookingHistory.URL_BOOKING_HISTORY)
                    .whereEqualTo(BookingHistory.TARGET_ID,bookingHistory.targetId)
                    .whereEqualTo(BookingHistory.ACTION_TYPE,BookingHistory.ACTION_TYPE_ACCEPTED)
                    .whereGreaterThanOrEqualTo(BookingHistory.CHECKOUT_DATE,bookingHistory.checkinDate)
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if(GeneralFunction.hasError(parentActivity,task,TAG)){
                                return;
                            }

                            boolean isClashed = false;

                            if(task.getResult() != null && !task.getResult().isEmpty()){
                                for(DocumentSnapshot bkHistorySnapshot : task.getResult().getDocuments()){
                                    BookingHistory tempBookingHistory = bkHistorySnapshot.toObject(BookingHistory.class);
                                    if (tempBookingHistory != null && tempBookingHistory.checkinDate != null && tempBookingHistory.checkoutDate != null) {
                                        isClashed = GeneralFunction.isSearchedDateClashedWithOtherBookedDates(
                                                tempBookingHistory.checkinDate,tempBookingHistory.checkoutDate,bookingHistory.checkinDate,bookingHistory.checkoutDate);
                                        if(isClashed){
                                            break;
                                        }
                                    }
                                }
                            }

                            if(!isClashed){
                                updateIntoDatabase(updateMap);
                            }else{
                                parentActivity.controlLoadingDialog(false,parentActivity);
                                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.approve_booking_request_clashes),R.drawable.notice_bad,TAG);
                            }
                        }
                    });

        }else{
            updateMap.put(BookHomestayDetails.ACTION_TYPE, BookingHistory.ACTION_TYPE_REJECTED);
            updateIntoDatabase(updateMap);
        }
    }

    private void updateIntoDatabase(final HashMap<String,Object> updateMap){
        GeneralFunction.getFirestoreInstance()
                .collection(BookHomestayDetails.URL_BOOK_GUEST)
                .document(bookingHistory.id)
                .update(updateMap)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        parentActivity.controlLoadingDialog(false,parentActivity);

                        if(!GeneralFunction.hasErrorDocOnComplete(parentActivity,task,TAG) && updateMap.get(BookHomestayDetails.ACTION_TYPE) != null){
                            if(updateMap.get(BookHomestayDetails.ACTION_TYPE).equals(BookingHistory.ACTION_TYPE_ACCEPTED)){
                                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.approved_success),R.drawable.notice_good,TAG);
                            }else if(updateMap.get(BookHomestayDetails.ACTION_TYPE).equals(BookingHistory.ACTION_TYPE_REJECTED)){
                                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.reject_success),R.drawable.notice_good,TAG);
                            }
                            parentFragment.removeBookingRequest(position);
                        }

                        Fragment fragment = parentActivity.getSupportFragmentManager().findFragmentById(R.id.mainFL);
                        if(fragment instanceof BookingRequestFragment){
                            parentActivity.onBackPressed();
                        }
                    }
                });
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.selectHotel();
        parentActivity.setToolbarTitle(parentActivity.getString(R.string.host_booking_request));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.approveBtn:
                updateHomestayBooking(true);
                break;
            case R.id.declineBtn:
                updateHomestayBooking(false);
                break;
        }
    }
}
