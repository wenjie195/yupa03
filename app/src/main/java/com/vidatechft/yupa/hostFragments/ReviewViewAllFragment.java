package com.vidatechft.yupa.hostFragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.ReviewAdapter;
import com.vidatechft.yupa.classes.Accommodation;
import com.vidatechft.yupa.classes.Favourite;
import com.vidatechft.yupa.classes.Rating;
import com.vidatechft.yupa.classes.User;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ReviewViewAllFragment extends Fragment {
    public static final String TAG = ReviewViewAllFragment.class.getName();
    private MainActivity parentActivity;
    private String loadingTxt,emptyTxt,errorTxt;
    private ReviewAdapter reviewAdapter;
    private ListenerRegistration reviewListener;
    private TextView noReviewTV;
    private ArrayList<Rating> ratingList = new ArrayList<>();
    private String currentAccPushID;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static ReviewViewAllFragment newInstance(MainActivity parentActivity, String currentAccPushID) {
        ReviewViewAllFragment fragment = new ReviewViewAllFragment();

        fragment.parentActivity = parentActivity;
        fragment.currentAccPushID = currentAccPushID;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_review_view_all, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.review_all));

                loadingTxt = parentActivity.getString(R.string.review_loading_other);
                emptyTxt = parentActivity.getString(R.string.review_empty_other);
                errorTxt = parentActivity.getString(R.string.review_err_loading_other);

                noReviewTV = view.findViewById(R.id.noReviewTV);
                noReviewTV.setText(loadingTxt);
                noReviewTV.setVisibility(View.VISIBLE);

                if(ratingList.size() <= 0){
                    getRating(view);
                }else{
                    noReviewTV.setVisibility(View.GONE);
                }

            }
        });

        return view;
    }

    private void getRating(final View v){

        final RecyclerView reviewRV = v.findViewById(R.id.reviewRV);
        final List<String> documentPushId = new ArrayList<>();
        final List<Rating> cc = new ArrayList<>();
        final Map<String, Rating> reviewList = new HashMap<>();
        final Accommodation accommodation = new Accommodation();

        reviewListener = GeneralFunction.getFirestoreInstance()
                .collection(Accommodation.URL_ACCOMMODATION)
                .document(currentAccPushID)
                .collection(Rating.URL_RATING)
                .orderBy(Rating.DATE_CREATED, Query.Direction.DESCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                        if(e != null){
                            GeneralFunction.Toast(parentActivity, e.toString());
                            noReviewTV.setText(errorTxt);
                            noReviewTV.setVisibility(View.VISIBLE);
                            return;
                        }
                        documentPushId.clear();
                        reviewList.clear();
                        cc.clear();
                        if(queryDocumentSnapshots != null){
                            if(!queryDocumentSnapshots.isEmpty()){
                                noReviewTV.setVisibility(View.GONE);
                                for (final QueryDocumentSnapshot queryDocumentSnapshot : queryDocumentSnapshots){
                                    final Rating rating = new Rating(parentActivity, queryDocumentSnapshot);
                                    reviewList.put(queryDocumentSnapshot.getId(),rating);
                                    //todo document id
                                    accommodation.id  = currentAccPushID;
                                    reviewList.size();
                                    GeneralFunction.getFirestoreInstance()
                                            .collection(User.URL_USER)
                                            .document(queryDocumentSnapshot.getId())
                                            .get()
                                            .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                                @Override
                                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                    if(task.isSuccessful()){
                                                        final User user = new User(parentActivity, task.getResult());
                                                        if(reviewList.get(user.id) != null){
                                                            reviewList.get(user.id).user = user;
                                                        }

                                                        GeneralFunction.getFirestoreInstance()
                                                                .collection(Favourite.URL_FAVOURITE)
                                                                .whereEqualTo(Favourite.ACCOMMODATION_ID, currentAccPushID)
                                                                .whereEqualTo(Favourite.TARGET_ID, queryDocumentSnapshot.getId())
                                                                .whereEqualTo(Favourite.TYPE, Favourite.FAV_TYPE_RATING)
                                                                .whereEqualTo(Favourite.USER_ID, parentActivity.uid)
                                                                .get()
                                                                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                                                    @Override
                                                                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                                        if(task.isSuccessful()){
                                                                            if(!task.getResult().isEmpty()){
//                                                                                for(QueryDocumentSnapshot queryDocumentSnapshot1 : task.getResult()){
                                                                                reviewList.get(user.id).favourite = new Favourite(parentActivity, task.getResult());
//                                                                                    favourite = queryDocumentSnapshot1.toObject(Favourite.class);
//                                                                                    reviewList.get(user.id).favourite.isLiked = favourite.isLiked;
//                                                                                }
                                                                            }

                                                                            cc.add(reviewList.get(user.id));
                                                                            cc.size();

                                                                            parentActivity.runOnUiThread(new Runnable() {
                                                                                @Override
                                                                                public void run() {
                                                                                    reviewAdapter = new ReviewAdapter(parentActivity,cc,accommodation);
                                                                                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(parentActivity);
                                                                                    reviewRV.setAdapter(reviewAdapter);
                                                                                    reviewAdapter.notifyDataSetChanged();
                                                                                    //when in scrollview prevent laggy
                                                                                    reviewRV.setNestedScrollingEnabled(false);
                                                                                    reviewRV.setHasFixedSize(true);
                                                                                    reviewRV.setLayoutManager(linearLayoutManager);
                                                                                    reviewRV.setVisibility(View.VISIBLE);
                                                                                }
                                                                            });
                                                                        }
                                                                    }
                                                                }).addOnFailureListener(new OnFailureListener() {
                                                            @Override
                                                            public void onFailure(@NonNull Exception e) {
                                                                Toast.makeText(parentActivity,e.toString(),Toast.LENGTH_SHORT).show();
                                                                noReviewTV.setText(errorTxt);
                                                                noReviewTV.setVisibility(View.VISIBLE);
                                                            }
                                                        });
                                                    }
                                                }
                                            }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            if(e!=null){
                                                noReviewTV.setText(errorTxt);
                                                noReviewTV.setVisibility(View.VISIBLE);
                                                Toast.makeText(parentActivity,e.toString(),Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                                }
                            }
                            else{
                                noReviewTV.setText(emptyTxt);
                                noReviewTV.setVisibility(View.VISIBLE);
                            }
                        }
                        reviewListener.remove();
                    }
                });

    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(reviewListener != null){
                    reviewListener.remove();
                }
            }
        });
    }
}
