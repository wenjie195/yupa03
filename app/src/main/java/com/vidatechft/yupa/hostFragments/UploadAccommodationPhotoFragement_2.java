package com.vidatechft.yupa.hostFragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vidatechft.yupa.R;

public class UploadAccommodationPhotoFragement_2 extends Fragment {
    public static final String TAG = UploadAccommodationPhotoFragement_2.class.getName();
    private AppCompatActivity parentActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static UploadAccommodationPhotoFragement_2 newInstance(AppCompatActivity parentActivity) {
        UploadAccommodationPhotoFragement_2 fragment = new UploadAccommodationPhotoFragement_2();
        fragment.parentActivity = parentActivity;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_upload_accommodation_photo_2_fragement, container, false);
    }
}
