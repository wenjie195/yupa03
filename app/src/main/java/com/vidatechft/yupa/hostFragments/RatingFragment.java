package com.vidatechft.yupa.hostFragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.Accommodation;
import com.vidatechft.yupa.classes.Rating;
import com.vidatechft.yupa.classes.User;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;
import com.vidatechft.yupa.utilities.KeyboardUtilities;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;


public class RatingFragment extends Fragment {

    public static final String TAG = RatingFragment.class.getName();
    private MainActivity parentActivity;
    private float ratingScore;
    private User user;
    private ImageView profileImageView;
    private TextView reviewProfileNameTV;
    private RatingBar reviewRB;
    private EditText reviewET;
    private Button reviewSubmitBTN;
    private String oriDescPushId;
    private boolean isDateCreated = false;
    private ListenerRegistration mainReviewListener;


    public static RatingFragment newInstance(MainActivity parentActivity, float ratingScore, User user, String oriDescPushId, boolean isDateCreated) {
        RatingFragment fragment = new RatingFragment();
        fragment.parentActivity = parentActivity;
        fragment.ratingScore = ratingScore;
        fragment.user = user;
        fragment.oriDescPushId = oriDescPushId;
        fragment.isDateCreated = isDateCreated;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_rating, container, false);

        profileImageView = view.findViewById(R.id.reviewProfilePicIV);
        reviewProfileNameTV = view.findViewById(R.id.reviewProfileNameTV);
        reviewRB = view.findViewById(R.id.reviewRB);
        reviewET = view.findViewById(R.id.reviewET);
        reviewSubmitBTN = view.findViewById(R.id.reviewSubmitBTN);

        if(ratingScore != 0.0){
            reviewRB.setRating(ratingScore);
        }
        if(user != null){
            if(user.profilePicUrl != null){
                GeneralFunction.GlideCircleImageSetting(parentActivity,Uri.parse(user.profilePicUrl),profileImageView);
            }
            if(user.name != null){
                reviewProfileNameTV.setText(user.name);
            }

        }
        reviewSubmitBTN.setOnClickListener(new DebouncedOnClickListener(500) {
            @Override
            public void onDebouncedClick(View v) {
                submitReview();
            }
        });


        return view;
    }

    private void getRating(){
        mainReviewListener = GeneralFunction.getFirestoreInstance()
                .collection(Accommodation.URL_ACCOMMODATION)
                .document(oriDescPushId)
                .collection(Rating.URL_RATING)
                .document(parentActivity.uid)
                .addSnapshotListener(new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot snapshot, @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w(TAG, "Listen failed.", e);
                            return;
                        }
                        if(snapshot != null){
                            if(snapshot.exists()){
                                final Rating rating = new Rating(parentActivity,snapshot);
                                if(rating.rating != 0.0f){
                                    reviewRB.setRating(rating.rating.floatValue());
                                }
                                if(rating.review != null){
                                    reviewET.setText(rating.review);
                                    reviewSubmitBTN.setText("Update");
                                }
                            }
                        }
                    }
                });
    }

    private void setRating(){
        reviewRB.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(final RatingBar ratingBar, float rating, boolean fromUser) {
                if(fromUser && oriDescPushId != null){
                    Map<String, Object> rateMap = new HashMap<>();

                    if(ratingBar.getRating() != 0.0f){
                        rateMap.put(Rating.RATING, ratingBar.getRating());
                        rateMap.put(Rating.DATE_UPDATED,FieldValue.serverTimestamp());
                    }

                    DocumentReference rateRef = GeneralFunction.getFirestoreInstance()
                            .collection(Accommodation.URL_ACCOMMODATION)
                            .document(oriDescPushId)
                            .collection(Rating.URL_RATING)
                            .document(parentActivity.uid);

                    if(rateMap.size() != 0){
                        rateRef.update(rateMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()){

                                }
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                if(e != null){
                                    Toast.makeText(parentActivity,e.toString(),Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }
                }
            }
        });
    }

    private void submitReview(){
        if(reviewET.getText() != null && !TextUtils.isEmpty(reviewET.getText().toString().trim())){
            Map<String, Object> reviewMap = new HashMap<>();
            reviewMap.put(Rating.REVIEW,reviewET.getText().toString().trim());
            reviewMap.put(Rating.DATE_UPDATED,FieldValue.serverTimestamp());
            DocumentReference reviewRef = GeneralFunction.getFirestoreInstance()
                    .collection(Accommodation.URL_ACCOMMODATION)
                    .document(oriDescPushId)
                    .collection(Rating.URL_RATING)
                    .document(parentActivity.uid);

            reviewRef.update(reviewMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){
                        parentActivity.onBackPressed();
                        KeyboardUtilities.hideSoftKeyboard(parentActivity);
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(parentActivity,e.toString(), Toast.LENGTH_SHORT).show();

                }
            });
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        getRating();
        setRating();
        parentActivity.selectHotel();
        parentActivity.setToolbarTitle(parentActivity.getString(R.string.host_give_rating));
    }

    @Override
    public void onPause() {
        super.onPause();
        if(mainReviewListener != null){
            mainReviewListener.remove();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mainReviewListener != null){
            mainReviewListener.remove();
        }
    }
}
