package com.vidatechft.yupa.hostFragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.vidatechft.yupa.CalendaUtilities.MySelectorDecorator;
import com.vidatechft.yupa.CalendaUtilities.TodayDecorator;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.PublishAccommodationAdapter;
import com.vidatechft.yupa.classes.Accommodation;
import com.vidatechft.yupa.classes.BookHomestayDetails;
import com.vidatechft.yupa.classes.BookingHistory;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.utilities.GeneralFunction;
import com.vidatechft.yupa.utilities.MillisecondsToDate;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class PublishAccommodationStatusViewAllFragment extends Fragment {
    public static final String TAG = PublishAccommodationStatusViewAllFragment.class.getName();
    private MainActivity parentActivity;
    private MaterialCalendarView calendarView;
    private CalendarDay todayDate;
    private String mAccommodationID;
    private RecyclerView upcomingGuestRV;
    private PublishAccommodationAdapter publishAccommodationAdapter;
    private List<BookingHistory> bookHomestayDetails;
    private String guestType;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static PublishAccommodationStatusViewAllFragment newInstance(MainActivity parentActivity, List<BookingHistory> bookHomestayDetails, String guestType) {
        PublishAccommodationStatusViewAllFragment fragment = new PublishAccommodationStatusViewAllFragment();
        fragment.parentActivity = parentActivity;
        fragment.bookHomestayDetails = bookHomestayDetails;
        fragment.guestType = guestType;
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_publish_accommodation_status_view_all, container, false);
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                upcomingGuestRV = view.findViewById(R.id.upcomingGuestRV);
                publishAccommodationAdapter = new PublishAccommodationAdapter(parentActivity,PublishAccommodationStatusViewAllFragment.this,bookHomestayDetails,guestType);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(parentActivity);
                upcomingGuestRV.setAdapter(publishAccommodationAdapter);
                publishAccommodationAdapter.notifyDataSetChanged();
                //when in scrollview prevent laggy
                upcomingGuestRV.setNestedScrollingEnabled(false);
                upcomingGuestRV.setHasFixedSize(true);
                upcomingGuestRV.setLayoutManager(linearLayoutManager);
                upcomingGuestRV.setVisibility(View.VISIBLE);

            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                View view = getView();
                if(view != null){

                }
                parentActivity.selectHotel();
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.host_room_details));
            }
        });
    }

}
