package com.vidatechft.yupa.hostFragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.Accommodation;
import com.vidatechft.yupa.classes.Description;
import com.vidatechft.yupa.classes.Host;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.classes.Rules;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

import static android.content.Context.MODE_PRIVATE;

public class DescriptionFragment extends Fragment implements View.OnClickListener {
    public static final String TAG = DescriptionFragment.class.getName();
    private MainActivity parentActivity;
    private EditText descriptET;
    private DocumentReference descriptRef;
    private String getPushId;
    private String setDocumentId, getDocumentId;
    private Button btn_next_detailsFragment;
    private String documentPushId;
    private boolean isFromDatabase = false;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private boolean isEditProfile = false;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    public static DescriptionFragment newInstance(MainActivity parentActivity) {
        DescriptionFragment fragment = new DescriptionFragment();
        fragment.parentActivity = parentActivity;
        return fragment;
    }

    public static DescriptionFragment newInstance(MainActivity parentActivity, String documentPushId, boolean isFromDatabase,boolean isEditProfile) {
        DescriptionFragment fragment = new DescriptionFragment();
        fragment.parentActivity = parentActivity;
        fragment.documentPushId = documentPushId;
        fragment.isFromDatabase = isFromDatabase;
        fragment.isEditProfile = isEditProfile;
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_description, container, false);

        //editText
        descriptET = view.findViewById(R.id.descriptET);

        //button
        btn_next_detailsFragment = view.findViewById(R.id.btn_next_detailsFragment);
        btn_next_detailsFragment.setOnClickListener(this);
        if(!isFromDatabase){
            setSharedPreferences();
        }
        return view;
    }

    private void goToSettingsFragement() {
        SettingFragment settingFragment = SettingFragment.newInstance(parentActivity, documentPushId, isFromDatabase,isEditProfile);
        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, settingFragment).addToBackStack(SettingFragment.TAG).commit();
    }

    private void submitDetaildToDatabase(){
        String getDescription = descriptET.getText().toString().trim();

        if(isValidInput(getDescription)){
            parentActivity.initializeProgressIndicator(parentActivity.getString(R.string.host_upload_room),false,this);
            parentActivity.controlProgressIndicator(true,this);
            HashMap<String,Object> descriptMap = new HashMap<>();
            descriptMap.put(Description.DESCRIPTION_ACCOMMODATION, getDescription);

            if(!isFromDatabase){
                //initiate get path
                getPushId = GeneralFunction.getDocumentPath(parentActivity,Room.ACCOMMODATION_DETIALS,Room.DIR_DOCUMENT, "");
                getDocumentId = GeneralFunction.getDocumentPath(parentActivity,Description.URL_DESCRIPTION,Description.DIR_DESCRIPTION,"");
            }

          if(getDocumentId != null && !getDocumentId.trim().equals("")){
              descriptRef = GeneralFunction.getFirestoreInstance()
                      .collection(Accommodation.URL_ACCOMMODATION)
                      .document(getPushId)
                      .collection(Description.URL_DESCRIPTION)
                      .document(getDocumentId);
          }
          else{
              descriptRef = GeneralFunction.getFirestoreInstance()
                      .collection(Accommodation.URL_ACCOMMODATION)
                      .document(getPushId)
                      .collection(Description.URL_DESCRIPTION)
                      .document();

              setDocumentId = descriptRef.getId();
              if(!isFromDatabase){
                  GeneralFunction.saveDocumentPath(parentActivity,Description.URL_DESCRIPTION,Description.DIR_DESCRIPTION,setDocumentId);
              }
          }
          if(!isFromDatabase){
              GeneralFunction.saveAccommodationHashMapSharedPreferences(parentActivity,descriptMap, Description.URL_DESCRIPTION,Description.URL_DESCRIPTION);
          }
            setDescriptionDetails(descriptRef,descriptMap);
        }
    }

    private void setDescriptionDetails(DocumentReference descriptRef, HashMap<String,Object> descriptMap){
        if(isFromDatabase){
            descriptRef.update(descriptMap).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    goToSettingsFragement();
                    GeneralFunction.handleUploadSuccess(parentActivity,DescriptionFragment.this,R.string.description_upload_success,TAG);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    GeneralFunction.handleUploadError(parentActivity,DescriptionFragment.this,R.string.description_err_upload,TAG);
                }
            });
        }
        else{
            descriptRef.set(descriptMap).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    goToSettingsFragement();
                    GeneralFunction.handleUploadSuccess(parentActivity,DescriptionFragment.this,R.string.description_upload_success,TAG);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    GeneralFunction.handleUploadError(parentActivity,DescriptionFragment.this,R.string.description_err_upload,TAG);
                }
            });
        }
    }
    private boolean isValidInput(String getDecript){
        return  GeneralFunction.checkAndReturnString(getDecript,parentActivity.getString(R.string.error_please_write_some_description),parentActivity);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_next_detailsFragment:
                submitDetaildToDatabase();
                break;
        }
    }

    private void setSharedPreferences(){
        try{
            SharedPreferences sharedPreferences = parentActivity.getSharedPreferences(Description.URL_DESCRIPTION, MODE_PRIVATE);

            // Get saved string data in it.
            String userInfoListJsonString = sharedPreferences.getString(Description.URL_DESCRIPTION, "");

            // Create Gson object and translate the json string to related java object array.
            Gson gson = new Gson();

            Description userInfoDtoArray = gson.fromJson(userInfoListJsonString, Description.class);

            // Loop the UserInfoDTO array and print each UserInfoDTO data in android monitor as debug log.
            // Get each user info in dto.
            Description userInfoDto = userInfoDtoArray;

            if(userInfoDto != null){
                descriptET.setText(userInfoDto.descriptionAccommodation);
            }

        }
        catch(Exception e){
            Crashlytics.logException(e);
        }
    }

    private void retrieveFromFirestore(){
        CollectionReference descriptionRef = db.collection(Accommodation.URL_ACCOMMODATION).document(documentPushId).collection(Description.URL_DESCRIPTION);
        descriptionRef.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.w(TAG, "Listen failed.", e);
                    return;
                }
                String source = queryDocumentSnapshots != null && queryDocumentSnapshots.getMetadata().hasPendingWrites()
                        ? "Local" : "Server";
                Log.d(TAG,source);

                if(queryDocumentSnapshots != null){
                    if(!queryDocumentSnapshots.isEmpty()){
                        String description = null;
                        String id = null;
                        for(QueryDocumentSnapshot queryDocumentSnapshot : queryDocumentSnapshots){
                            Map<String, Object> data = queryDocumentSnapshot.getData();
                            description = data.get(Description.DESCRIPTION_ACCOMMODATION).toString();

                            id = queryDocumentSnapshot.getId();
                        }
                        setDescriptionDetails(description, id);
                    }
                }
            }
        });
    }

    private void setDescriptionDetails(String description, String id){
        getPushId = documentPushId;
        getDocumentId = id;
        if(description!=null){
            descriptET.setText(description);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(isFromDatabase){
            retrieveFromFirestore();
        }

        parentActivity.selectHotel();
        parentActivity.setToolbarTitle(parentActivity.getString(R.string.host_an_accommodation));
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
