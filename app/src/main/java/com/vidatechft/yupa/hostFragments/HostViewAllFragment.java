package com.vidatechft.yupa.hostFragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.HostStayRecyclerGridAdapter;
import com.vidatechft.yupa.classes.Accommodation;
import com.vidatechft.yupa.classes.Host;
import com.vidatechft.yupa.classes.Itinerary;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.classes.Setting;
import com.vidatechft.yupa.utilities.GeneralFunction;
import com.vidatechft.yupa.utilities.YupaRecyclerView;

import java.util.ArrayList;
import java.util.List;

public class HostViewAllFragment extends Fragment{
    public static final String TAG = HostViewAllFragment.class.getName();
    private MainActivity parentActivity;
    private Query queryRef;

    private ArrayList<Room> roomList = new ArrayList<>();
    private RecyclerView hostRV;
    private HostStayRecyclerGridAdapter hostAdapter;

    private ListenerRegistration hostListener;

    private String loadingTxt,emptyTxt,errorTxt;
    private TextView noHostTv;
    private ArrayList<String> documentPushId = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static HostViewAllFragment newInstance(MainActivity parentActivity, ArrayList<Room> roomList, ArrayList<String> documentPushId, Query queryRef) {
        HostViewAllFragment fragment = new HostViewAllFragment();

        fragment.parentActivity = parentActivity;
        if(roomList == null){
            fragment.roomList = new ArrayList<>();
        }else{
            fragment.roomList = roomList;
        }

        if(documentPushId == null){
            fragment.documentPushId = new ArrayList<>();
        }else{
            fragment.documentPushId = documentPushId;
        }

        fragment.queryRef = queryRef;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_host_stay_view_all, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.showBtmNav();

                loadingTxt = parentActivity.getString(R.string.host_room_loading_other);
                emptyTxt = parentActivity.getString(R.string.host_room_empty_other);
                errorTxt = parentActivity.getString(R.string.host_room_err_loading_other);

                hostRV = view.findViewById(R.id.hostRV);
                hostRV.setNestedScrollingEnabled(false);
                noHostTv = view.findViewById(R.id.noHostTV);

                if(roomList.size() <= 0){
                    getHostStay();
                }else{
                    noHostTv.setVisibility(View.GONE);
                }

                setUpGridAdapter();
            }
        });

        return view;
    }

    private void setUpGridAdapter(){

        hostAdapter = new HostStayRecyclerGridAdapter(parentActivity,this, roomList,documentPushId,false);

        hostRV.setAdapter(hostAdapter);
        hostRV.setLayoutManager(new GridLayoutManager(parentActivity, 2));
    }

    private void getHostStay(){
        //todo the recycler view needs to have lazy load

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                noHostTv.setText(loadingTxt);
                noHostTv.setVisibility(View.VISIBLE);
            }
        });

        Query ref;
        if(queryRef != null){
            ref = queryRef;
        }else{
            ref = GeneralFunction.getFirestoreInstance()
                    .collection(Accommodation.URL_ACCOMMODATION)
                    .whereEqualTo(Room.STATUS, Room.STATUS_APPROVED)
                    .whereEqualTo(Room.IS_DRAFT, false)
                    .whereEqualTo(Room.IS_PUBLISH, true)
                    .orderBy(Itinerary.LIKE_COUNT, Query.Direction.DESCENDING)
                    .orderBy(Itinerary.DATE_UPDATED, Query.Direction.DESCENDING);
        }

        hostListener = ref
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot snapshots, @javax.annotation.Nullable final FirebaseFirestoreException e) {
                        if (e != null) {
                            parentActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.w(TAG, "Listen failed.", e);
                                    noHostTv.setText(errorTxt);
                                    noHostTv.setVisibility(View.VISIBLE);
                                }
                            });
                            return;
                        }

                        roomList.clear();
                        documentPushId.clear();

                        if (snapshots != null && !snapshots.isEmpty()) {
                            for (QueryDocumentSnapshot snapshot : snapshots) {
                                Room tempRoom = Room.snapshotToRoom(parentActivity, snapshot);
                                roomList.add(tempRoom);
                                documentPushId.add(snapshot.getId());
                            }

                            parentActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    hostAdapter.notifyDataSetChanged();
                                    noHostTv.setVisibility(View.GONE);
                                }
                            });
                        } else {
                            parentActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    roomList.clear();
                                    documentPushId.clear();
                                    hostAdapter.notifyDataSetChanged();

                                    noHostTv.setText(emptyTxt);
                                    noHostTv.setVisibility(View.VISIBLE);
                                }
                            });
                        }
                    }
                });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(hostListener != null){
                    hostListener.remove();
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.setToolbarTitle(parentActivity.getString(R.string.host_stay_all));
        parentActivity.selectHotel();
    }
}
