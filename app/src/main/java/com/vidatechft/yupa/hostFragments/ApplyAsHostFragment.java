package com.vidatechft.yupa.hostFragments;
import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ScrollView;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.Host;
import com.vidatechft.yupa.utilities.Config;
import com.vidatechft.yupa.utilities.FilePath;
import com.vidatechft.yupa.utilities.GeneralFunction;

import static android.app.Activity.RESULT_OK;
import static com.vidatechft.yupa.utilities.GeneralFunction.GlideImageSetting;
import static com.vidatechft.yupa.utilities.GeneralFunction.getFirebaseStorageInstance;


public class ApplyAsHostFragment extends Fragment implements View.OnClickListener{
    public static final String TAG = ApplyAsHostFragment.class.getName();
    private MainActivity parentActivity;
    private EditText hostNameRT, hostIcET,emailET,contactNumberET, addressET;
    private ImageButton uploadLicenseIB;
    private Button applyAsHostSubmitBTN;
    private View view;
    private ScrollView scrollView;
    private String filePath2;
    private int imageButton = 0;
    private ProgressDialog progressDialog;
    private String uid;
    private GeoPoint geoPoint;
    private Place place;
    private final FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    private String documentPushId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        parentActivity = (MainActivity) getActivity();
        GeneralFunction.getFirestoreInstance();
        if(firebaseUser != null){
            uid= firebaseUser.getUid();
        }
    }

    public static ApplyAsHostFragment newInstance(MainActivity parentActivity) {
        ApplyAsHostFragment fragment = new ApplyAsHostFragment();
        fragment.parentActivity = parentActivity;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_apply_as_host, container, false);
        scrollView = view.findViewById(R.id.applyAsHostSV);
        //Textview
        hostNameRT = view.findViewById(R.id.hostNameET);
        hostIcET = view.findViewById(R.id.hostIcET);
        emailET = view.findViewById(R.id.emailET);
        contactNumberET = view.findViewById(R.id.contactNumberET);
        addressET = view.findViewById(R.id.addressET);
        //Imagebutton
        uploadLicenseIB = view.findViewById(R.id.uploadLicenseIB);
        //button
        applyAsHostSubmitBTN = view.findViewById(R.id.applyAsHostSubmitBTN);
        //listener
        uploadLicenseIB.setOnClickListener(this);
        addressET.setOnClickListener(this);
        applyAsHostSubmitBTN.setOnClickListener(this);
        progressDialog = new ProgressDialog(parentActivity);

        return view;
    }

    private void applyAsHost(){
        FirebaseFirestore db =  GeneralFunction.getFirestoreInstance();
        String name = hostNameRT.getText().toString().trim();
        String email = emailET.getText().toString().trim();
        String nric = hostIcET.getText().toString();
        String address = addressET.getText().toString();
        String contactNo = contactNumberET.getText().toString().trim();


        if(isValidInput(name,email,nric,address,contactNo) && filePath2 != null && geoPoint != null && uid != null ){
            Host host = new Host(name,nric,address,contactNo,email, geoPoint, uid);

            // Add a new document with a generated ID
            final DocumentReference hostRef = db.collection(Host.URL_HOST).document(parentActivity.uid);
            documentPushId = hostRef.getId();
            hostRef.set(host)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            parentActivity.onBackPressed();
                            GeneralFunction.openMessageNotificationDialog(parentActivity,getResources().getString(R.string.host_successful_apply_as_host),R.drawable.notice_good,TAG);

                            Log.d(TAG, "DocumentSnapshot successfully written!");
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            GeneralFunction.openMessageNotificationDialog(parentActivity,e.toString(),R.drawable.notice_bad,TAG);
                            Log.w(TAG, "Error adding document", e);
                        }
                    });
            uploadImage();
        }
        else{
            Log.e(parentActivity.getLocalClassName(),"Upload Failed");
        }
    }

    private boolean isValidInput(String name, String email,String ic,String address, String contactNo){
        return  GeneralFunction.checkAndReturnString(name,parentActivity.getString(R.string.error_please_enter_name),parentActivity)
                &&
                GeneralFunction.checkAndReturnString(email,parentActivity.getString(R.string.error_please_enter_email),parentActivity)
                &&
                GeneralFunction.isValidEmail(email,parentActivity)
                &&
                GeneralFunction.checkAndReturnString(ic,parentActivity.getString(R.string.error_please_enter_ic),parentActivity)
                &&
                GeneralFunction.checkAndReturnString(address,parentActivity.getString(R.string.error_please_enter_address),parentActivity)
                &&
                GeneralFunction.checkAndReturnString(contactNo,parentActivity.getString(R.string.error_please_enter_contact),parentActivity);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.uploadLicenseIB:
                if(GeneralFunction.isStorageAndCameraPermitted(parentActivity.getApplicationContext(),parentActivity)){
                    GeneralFunction.showFileChooser(parentActivity,this);
                    imageButton = 2;
                }
                break;

            case R.id.applyAsHostSubmitBTN:
                applyAsHost();
                break;

            case R.id.addressET:
                GeneralFunction.openPlacePicker(parentActivity,ApplyAsHostFragment.this);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode  == RESULT_OK) {
            if (requestCode == 158) {
                if (data == null) {
                    //no data present
                    return;
                }
            }

            if (requestCode == Config.REQUEST_PLACE_PICKER) {
                place = PlacePicker.getPlace(parentActivity, data);

                if (place != null && place.getAddress() != null && !place.getAddress().equals("")) {
                    addressET.setText(place.getAddress().toString());
                    geoPoint = new GeoPoint(place.getLatLng().latitude,place.getLatLng().longitude);

//                    GeneralFunction.setLocationNameForPlace(parentActivity, place, workspaceTV);
                } else {
                    GeneralFunction.openMessageNotificationDialog(parentActivity, parentActivity.getString(R.string.error_place_picker), R.drawable.notice_bad, TAG);
                }
            }

            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    Uri resultUri = result.getUri();
                    if(imageButton == 2){
                        this.filePath2 = FilePath.getPath(parentActivity, resultUri);
                        GlideImageSetting(parentActivity, resultUri,uploadLicenseIB);
                    }

                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Exception error = result.getError();
                }
            }

            if (requestCode == CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE) {
                if (ContextCompat.checkSelfPermission(parentActivity, Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_DENIED) {
                    ActivityCompat.requestPermissions(parentActivity, new String[]{Manifest.permission.CAMERA}, requestCode);

                }
            }
        }
    }

    private void uploadImage(){
        if(filePath2 != null )
        {
            progressDialog.setTitle("Uploading...Please Wait...");
            progressDialog.show();
            final StorageReference ref = getFirebaseStorageInstance().child(Host.URL_HOST+"/"+uid+"/"+Host.URL_HOST_DETAILS+"/"+Host.URL_HOST_DRIVER_LICENSE_PIC);

            UploadTask uploadTask = ref.putBytes(GeneralFunction.getCompressedBitmap(parentActivity,filePath2));
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                    progressDialog.dismiss();
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    // taskSnapshot.getMetadata() contains file metadata such as size, content-type, etc.
                    // ...
                    progressDialog.setTitle("Uploaded Successful ...");
                    progressDialog.dismiss();
                    ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri downloadUri) {
                            //Do what you want with the url
                            if(downloadUri != null && documentPushId != null){
                                GeneralFunction.getFirestoreInstance().collection(Host.URL_HOST).document(documentPushId).update(Host.DRIVER_LICENSE_PIC_URL,downloadUri.toString());
                            }
                        }
                    });
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.selectHotel();
        parentActivity.setToolbarTitle(parentActivity.getString(R.string.nav_apply_host));
    }
}