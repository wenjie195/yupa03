package com.vidatechft.yupa.hostFragments;

import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.Accommodation;
import com.vidatechft.yupa.classes.Amenity;
import com.vidatechft.yupa.classes.Host;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import static android.content.Context.MODE_PRIVATE;

public class SelectAvailableAmenityFragment extends Fragment implements View.OnClickListener {
    public static final String TAG = SelectAvailableAmenityFragment.class.getName();
    private MainActivity parentActivity;
    private ArrayList<CheckBox> checkBoxes;
    private DocumentReference amenityRef;
    private Button btn_next_selectAvailableAmenityFragment;
    private String value;
    private String setAemenityDocumentPath, getAmenityDocumentPath;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private boolean isFromDatabase = false;
    private String documentPushId, amenityPushId;
    private Map<String, Object> combineValue = new HashMap<>();
    private List<Amenity>amenityDataset = new ArrayList<>();
    private List<String>amenityDataset2 = new ArrayList<>();
    private Amenity amenity;
    private Room room;
    private boolean isEditProfile = false;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        }


    public static SelectAvailableAmenityFragment newInstance(MainActivity parentActivity) {
        SelectAvailableAmenityFragment fragment = new SelectAvailableAmenityFragment();
        fragment.parentActivity = parentActivity;
        return fragment;
    }

    public static SelectAvailableAmenityFragment newInstance(MainActivity parentActivity, boolean isFromDatabase, String documentPushId, boolean isEditProfile) {
        SelectAvailableAmenityFragment fragment = new SelectAvailableAmenityFragment();
        fragment.parentActivity = parentActivity;
        fragment.isFromDatabase = isFromDatabase;
        fragment.documentPushId = documentPushId;
        fragment.isEditProfile = isEditProfile;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_select_available_amenity, container, false);
        // Inflate the layout for this fragment
        LinearLayout selectAvailableAmenityLL = view.findViewById(R.id.selectAvailableAmenityLL);

        setupAmenitiesList(selectAvailableAmenityLL);
        btn_next_selectAvailableAmenityFragment = view.findViewById(R.id.btn_next_selectAvailableAmenityFragment);
        btn_next_selectAvailableAmenityFragment.setOnClickListener(this);
        if(!isFromDatabase){
            setSharedPreferences();

        }
        return view;
    }

    private void setupAmenitiesList(LinearLayout selectAvailableAmenityLL) {
        checkBoxes = new ArrayList<>();
        final String[] earnList = parentActivity.getResources().getStringArray(R.array.host_select_amenity_list);
        for (String earning : earnList) {
            CheckBox checkBox = (CheckBox) CheckBox.inflate(parentActivity, R.layout.custom_checkbox, null);
            checkBox.setText(earning.trim());
            selectAvailableAmenityLL.addView(checkBox);
            checkBoxes.add(checkBox);
        }
    }

    private void saveDetailsToDatabase(){
        HashMap<String,Boolean> amenityMap = new HashMap<>();
        parentActivity.initializeProgressIndicator(parentActivity.getString(R.string.amenity_uploading),false,this);
        parentActivity.controlProgressIndicator(true,this);

        for(CheckBox checkBox : checkBoxes){
            //for lower api cannot use max api 24
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                combineValue.remove(checkBox.getText().toString().trim(), true);
//            }
            if(checkBox.isChecked()){
                combineValue.put(checkBox.getText().toString().trim(),true);
            }
        }
        //initiate get path
        if(!isFromDatabase){
            value = GeneralFunction.getDocumentPath(parentActivity, Room.ACCOMMODATION_DETIALS,Room.DIR_DOCUMENT, "");
            getAmenityDocumentPath = GeneralFunction.getDocumentPath(parentActivity,Amenity.AMENITY_DETAILS,Amenity.DIR_AMENITY,"");
        }

            amenityRef = GeneralFunction.getFirestoreInstance()
                    .collection(Accommodation.URL_ACCOMMODATION)
                    .document(value);

        if(!isFromDatabase){
            GeneralFunction.saveAccommodationHashMapSharedPreferencesBoolean(parentActivity, combineValue, Amenity.AMENITY_DETAILS,Amenity.AMENITY_DETAILS);
        }

        setAmenityDetails(amenityRef, combineValue);

    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_next_selectAvailableAmenityFragment){
            saveDetailsToDatabase();
        }
    }

    private void setAmenityDetails(DocumentReference amenityRef, Map<String, Object> amenityMap){
        amenityRef.set(amenityMap).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                goToRuleFragement();
                GeneralFunction.handleUploadSuccess(parentActivity,SelectAvailableAmenityFragment.this,R.string.amenity_upload_success,TAG);
                }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                GeneralFunction.handleUploadError(parentActivity,SelectAvailableAmenityFragment.this,R.string.amenity_err_upload,TAG);
            }
        });
    }

    private void setSharedPreferences(){
        try{
            SharedPreferences sharedPreferences = parentActivity.getSharedPreferences(Amenity.AMENITY_DETAILS, MODE_PRIVATE);

            Type type = new TypeToken<Map<String, Boolean>>(){}.getType();
            Gson gson = new Gson();
            Map<String, Boolean> storedAmenityMap = gson.fromJson(sharedPreferences.getString(Amenity.AMENITY_DETAILS,""), type);

            for(CheckBox checkBox : checkBoxes){
                checkBox.setChecked(storedAmenityMap.get(checkBox.getText().toString().trim()));
            }
        }
        catch(Exception e){
            Crashlytics.logException(e);
        }
    }

    private void goToRuleFragement() {
        RulesFragment rulesFragment = RulesFragment.newInstance(parentActivity, documentPushId, isFromDatabase,isEditProfile);
        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, rulesFragment).addToBackStack(RulesFragment.TAG).commit();
    }

    private void retrieveFromFirestore(){
        if(documentPushId == null && !isFromDatabase){
            documentPushId = GeneralFunction.getDocumentPath(parentActivity,Room.ACCOMMODATION_DETIALS,Room.DIR_DOCUMENT, "");
        }
        final DocumentReference amenityRef = db.collection(Accommodation.URL_ACCOMMODATION).document(documentPushId);
        amenityRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot snapshot, @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.w(TAG, "Listen failed.", e);
                    return;
                }
                String source = snapshot != null && snapshot.getMetadata().hasPendingWrites()
                        ? "Local" : "Server";
                Log.d(TAG,source);

                if (snapshot != null) {
                    combineValue.clear();
                    if(snapshot.exists()){
                        Map<String, Object> oriData = snapshot.getData();
                        if(oriData != null){
                            //convert to class
                            room = Room.toClass(oriData,parentActivity);
                        }
                        //convert back to map
                        HashMap<String,Object> roomData = Room.toMap(room);
                        roomData.size();
                        //assign value into here
                        combineValue = roomData;
                        Amenity amenity = new Amenity(parentActivity,snapshot);
                        if(amenity.amenityScope != null){
                            for(CheckBox checkBox : checkBoxes){
                                if((amenity.amenityScope.contains(checkBox.getText().toString()))){
                                    checkBox.setChecked(true);
                                }
                            }
                        }
                        if(isFromDatabase){
                            setAmenityDetails();
                        }
                    }
                }
            }
        });

    }

    private void setAmenityDetails(){
       value = documentPushId;
       getAmenityDocumentPath = amenityPushId;
    }

    @Override
    public void onResume() {
        super.onResume();
//        getDataFirst(); // is not from database
        retrieveFromFirestore();
        parentActivity.selectHotel();
        parentActivity.setToolbarTitle(parentActivity.getString(R.string.host_an_accommodation));
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
