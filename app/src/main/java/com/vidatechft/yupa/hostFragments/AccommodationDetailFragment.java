package com.vidatechft.yupa.hostFragments;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;
import com.theartofdev.edmodo.cropper.CropImage;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.Accommodation;
import com.vidatechft.yupa.classes.GlideApp;
import com.vidatechft.yupa.classes.Host;
import com.vidatechft.yupa.classes.PlaceDetailsGoogleApi.PlaceAddress;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.retrofitNetwork.UnifyPlaceAddress;
import com.vidatechft.yupa.utilities.Config;
import com.vidatechft.yupa.utilities.FilePath;
import com.vidatechft.yupa.utilities.GeneralFunction;
import com.vidatechft.yupa.utilities.KeyboardUtilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;
import static com.vidatechft.yupa.classes.Room.ACCOMMODATION_DETIALS;
import static com.vidatechft.yupa.classes.Room.ACCOMMODATION_DETIALS_ALL;
import static com.vidatechft.yupa.classes.Room.URL_ROOM_PROOF;
import static com.vidatechft.yupa.utilities.GeneralFunction.GlideImageSetting;

public class AccommodationDetailFragment extends Fragment implements View.OnClickListener,View.OnTouchListener {
    public static final String TAG = HostAnAccommodationFragment.class.getName();
    private MainActivity parentActivity;
    private RadioButton btn_entirePlace, btn_privateRoom, btn_shareRoom;
    private View view;
    private ImageButton btn_edit, btn_upload_accommodation, electricBillIB;
    private EditText hostET, currencyPerNightET, addressAccommodationET;
    private ImageView btn_cover_photo;
    private String filePath, getRoomType, getElectricBillPath;
    private EditText bathroomSPN, guestMaxSPN, bedroomSPN, singleBedSizeSPN, queenBedSizeSPN, kingBedSizeSPN, currencyTV, lotDetailsET;
    private ScrollView accommodation_detailSV;
    private ConstraintLayout rentDetailCL;
    private Button btn_next_accommodationDetailFragment;
    private ListView listViewSpinnerItem;
    private ConstraintLayout constraintLayoutSpinnerItem;
    private boolean isEntire = true;
    public static boolean isOpen;
    private ViewGroup viewGroup;
    private int imageButton = 0;
    private ProgressDialog progressDialog;
    private Place place;
    public String setPushId, setElectricBillPushId;
    private GeoPoint getGeoPoint;
    private String getPushId, getElectricBillPushId;
    private Integer getBedQuantity;
    private DocumentReference roomRef, electricBillRef;
    private int position = 0;
    private Room room;
    private String fragmentType;
    private String documentPushId, electricBillPushId;
    private boolean isFromDatabase = false;
    private boolean uploadOutlook, uploadElectricBill;
    private boolean isDateCreated = false;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private List<String> roomUrl = new ArrayList<String>();
    private List<String> roomPicPushIdFromFirestore = new ArrayList<>();
    private List<String> roomName = new ArrayList<String>();
    private boolean isBtnUploadClick = false;
    private boolean isBtnUploadElectricClick = false;
    private boolean isImgReplaceFromDatabase = false;
    private boolean isNewAddress = false;
    private boolean isEditProfile = false;

    private PlaceAddress placeAddress;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static AccommodationDetailFragment newInstance(MainActivity parentActivity, Room room, int position, String fragmentType, String documentPushId, String electricBillPushId, boolean isFromDatabase, boolean isEditProfile) {
        AccommodationDetailFragment fragment = new AccommodationDetailFragment();
        fragment.parentActivity = parentActivity;
        fragment.room = room;
        fragment.position = position;
        fragment.fragmentType = fragmentType;
        fragment.documentPushId = documentPushId;
        fragment.electricBillPushId = electricBillPushId;
        fragment.isFromDatabase = isFromDatabase;
        fragment.isEditProfile = isEditProfile;
//        fragment.lastToolbarTitle = lastToolbarTitle;
        return fragment;
    }

    public static AccommodationDetailFragment newInstance(MainActivity parentActivity) {
        AccommodationDetailFragment fragment = new AccommodationDetailFragment();
        fragment.parentActivity = parentActivity;
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_accommodation_detail, container, false);
        //constraint layout
        rentDetailCL = view.findViewById(R.id.rentDetailCL);
        //scrollview
        accommodation_detailSV = view.findViewById(R.id.accommodation_detailSV);
        //radiobutton
        btn_entirePlace = view.findViewById(R.id.btn_entirePlace);
        btn_privateRoom = view.findViewById(R.id.btn_privateRoom);
        btn_shareRoom = view.findViewById(R.id.btn_shareRoom);
        btn_edit = view.findViewById(R.id.btn_edit);
        btn_upload_accommodation = view.findViewById(R.id.btn_upload_accommodation);
        btn_cover_photo = view.findViewById(R.id.btn_cover_photo);
        //button
        btn_next_accommodationDetailFragment = view.findViewById(R.id.btn_next_accommodationDetailFragment);
        //Textview
        currencyTV = view.findViewById(R.id.currencyTV);
        //Image button
        electricBillIB = view.findViewById(R.id.electricBillIB);
        //spinner
        guestMaxSPN = view.findViewById(R.id.guestMaxSPN);
        bedroomSPN = view.findViewById(R.id.bedroomSPN);
        singleBedSizeSPN = view.findViewById(R.id.singleBedSizeSPN);
        queenBedSizeSPN = view.findViewById(R.id.queenBedSizeSPN);
        kingBedSizeSPN = view.findViewById(R.id.kingBedSizeSPN);
        bathroomSPN = view.findViewById(R.id.bathroomSPN);

        //edittext
        hostET = view.findViewById(R.id.hostET);
        currencyPerNightET = view.findViewById(R.id.currencyPerNightET);
        addressAccommodationET = view.findViewById(R.id.addressAccommodationET);
        lotDetailsET = view.findViewById(R.id.lotDetailsET);

        //listview
        listViewSpinnerItem = view.findViewById(R.id.listViewSpinnerItem);

        //constraint layout
        constraintLayoutSpinnerItem = view.findViewById(R.id.constraintLayoutSpinnerItem);

        viewGroup = view.findViewById(R.id.accommodation_detailSV);

        //onClick listener
        btn_entirePlace.setOnClickListener(this);
        btn_privateRoom.setOnClickListener(this);
        btn_shareRoom.setOnClickListener(this);
        btn_edit.setOnClickListener(this);
        btn_upload_accommodation.setOnClickListener(this);
        constraintLayoutSpinnerItem.setOnClickListener(this);
        currencyTV.setOnClickListener(this);
        btn_next_accommodationDetailFragment.setOnClickListener(this);
        currencyPerNightET.setOnClickListener(this);
        bathroomSPN.setOnClickListener(this);
        guestMaxSPN.setOnClickListener(this);
        bedroomSPN.setOnClickListener(this);
        singleBedSizeSPN.setOnClickListener(this);
        queenBedSizeSPN.setOnClickListener(this);
        kingBedSizeSPN.setOnClickListener(this);
        electricBillIB.setOnClickListener(this);
        addressAccommodationET.setOnClickListener(this);

        accommodation_detailSV.setOnTouchListener(this);

        progressDialog = new ProgressDialog(parentActivity);
        if (!isFromDatabase) {
            setSharedPreferences();
        }
        uploadElectricBill = false;
        uploadOutlook = false;

        return view;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_entirePlace:
                btn_entirePlace.setBackgroundResource(R.drawable.rounded_shape_button_done);
                btn_entirePlace.setTextColor(getResources().getColor(R.color.white));
                btn_privateRoom.setBackgroundResource(R.drawable.rounded_acctype_white_bg_border_black);
                btn_privateRoom.setTextColor(getResources().getColor(R.color.black));
                btn_shareRoom.setBackgroundResource(R.drawable.rounded_acctype_white_bg_border_black);
                btn_shareRoom.setTextColor(getResources().getColor(R.color.black));
                setSpinnerArrayEntirePlace();
                getRoomType = Room.TYPE_ROOM_ENTIRE_PLACE;
                GeneralFunction.slideUpAnimation(parentActivity, rentDetailCL);
                GeneralFunction.clearForm(viewGroup);
                isEntire = true;
                break;
            case R.id.btn_privateRoom:
                btn_entirePlace.setBackgroundResource(R.drawable.rounded_acctype_white_bg_border_black);
                btn_entirePlace.setTextColor(getResources().getColor(R.color.black));
                btn_privateRoom.setBackgroundResource(R.drawable.rounded_shape_button_done);
                btn_privateRoom.setTextColor(getResources().getColor(R.color.white));
                btn_shareRoom.setBackgroundResource(R.drawable.rounded_acctype_white_bg_border_black);
                btn_shareRoom.setTextColor(getResources().getColor(R.color.black));
                getRoomType = Room.TYPE_ROOM_PRIVATE;
                GeneralFunction.slideUpAnimation(parentActivity, rentDetailCL);
                GeneralFunction.clearForm(viewGroup);
                setSpinnerArrayNotEntirePlace();
                isEntire = false;
                break;
            case R.id.btn_shareRoom:
                btn_entirePlace.setBackgroundResource(R.drawable.rounded_acctype_white_bg_border_black);
                btn_entirePlace.setTextColor(getResources().getColor(R.color.black));
                btn_privateRoom.setBackgroundResource(R.drawable.rounded_acctype_white_bg_border_black);
                btn_privateRoom.setTextColor(getResources().getColor(R.color.black));
                btn_shareRoom.setBackgroundResource(R.drawable.rounded_shape_button_done);
                btn_shareRoom.setTextColor(getResources().getColor(R.color.white));
                getRoomType = Room.TYPE_ROOM_SHARE;
                GeneralFunction.slideUpAnimation(parentActivity, rentDetailCL);
                GeneralFunction.clearForm(viewGroup);
                setSpinnerArrayNotEntirePlace();
                isEntire = false;
                break;

            case R.id.constraintLayoutSpinnerItem:
                GeneralFunction.slideDownAnimationForListView(parentActivity, constraintLayoutSpinnerItem);
                break;

            case R.id.btn_edit:
                hostET.requestFocus();
                if (hostET.hasFocus()) {
//             hostET.setSelection(hostET.getText().length());
                    hostET.setCursorVisible(true);
                    hostET.selectAll();
                } else {
                    hostET.setCursorVisible(false);
                }
                KeyboardUtilities.showSoftKeyboard(parentActivity, hostET);
                break;

            case R.id.btn_upload_accommodation:
                if (GeneralFunction.isStorageAndCameraPermitted(parentActivity.getApplicationContext(), parentActivity)) {
                    GeneralFunction.showFileChooserRatio169(parentActivity, this);
                }
                if (isFromDatabase) {
                    isBtnUploadClick = true;
                }
                imageButton = 1;
                break;

            case R.id.electricBillIB:
                if (GeneralFunction.isStorageAndCameraPermitted(parentActivity.getApplicationContext(), parentActivity)) {
                    GeneralFunction.showFileChooser(parentActivity, this);
                }
                if (isFromDatabase) {
                    isBtnUploadElectricClick = true;
                }
                imageButton = 2;
                break;


            case R.id.btn_next_accommodationDetailFragment:
                submitAccommodation();
                break;

            case R.id.currencyPerNightET:
//                GeneralFunction.slideUpAnimationForListView(parentActivity,listViewSpinnerItem);
                break;


            case R.id.currencyTV:
                GeneralFunction.slideUpAnimationForListView(parentActivity, constraintLayoutSpinnerItem, listViewSpinnerItem, R.array.currency_array, currencyTV);
                MainActivity.isOpen = true;
                break;

            case R.id.guestMaxSPN:
                if (isEntire) {
                    GeneralFunction.slideUpAnimationForListView(parentActivity, constraintLayoutSpinnerItem, listViewSpinnerItem, R.array.guestAllow_array, guestMaxSPN);
                } else {
                    GeneralFunction.slideUpAnimationForListView(parentActivity, constraintLayoutSpinnerItem, listViewSpinnerItem, R.array.guestAllow_notEntire_array, guestMaxSPN);
                }
                MainActivity.isOpen = true;
                break;

            case R.id.bedroomSPN:
                if (isEntire) {
                    GeneralFunction.slideUpAnimationForListView(parentActivity, constraintLayoutSpinnerItem, listViewSpinnerItem, R.array.bathroom_array, bedroomSPN);
                } else {
                    GeneralFunction.slideUpAnimationForListView(parentActivity, constraintLayoutSpinnerItem, listViewSpinnerItem, R.array.bathroom_notEntire_array, bedroomSPN);
                }
                MainActivity.isOpen = true;
                break;

            case R.id.singleBedSizeSPN:
                if (isEntire) {
                    GeneralFunction.slideUpAnimationForListView(parentActivity, constraintLayoutSpinnerItem, listViewSpinnerItem, R.array.singleSize_array, singleBedSizeSPN);
                } else {
                    GeneralFunction.slideUpAnimationForListView(parentActivity, constraintLayoutSpinnerItem, listViewSpinnerItem, R.array.singleSize_notEntire_array, singleBedSizeSPN);
                }
                MainActivity.isOpen = true;
                break;

            case R.id.queenBedSizeSPN:
                if (isEntire) {
                    GeneralFunction.slideUpAnimationForListView(parentActivity, constraintLayoutSpinnerItem, listViewSpinnerItem, R.array.queenSize_array, queenBedSizeSPN);
                } else {
                    GeneralFunction.slideUpAnimationForListView(parentActivity, constraintLayoutSpinnerItem, listViewSpinnerItem, R.array.queenSize_notEntire_array, queenBedSizeSPN);
                }
                MainActivity.isOpen = true;
                break;

            case R.id.kingBedSizeSPN:
                if (isEntire) {
                    GeneralFunction.slideUpAnimationForListView(parentActivity, constraintLayoutSpinnerItem, listViewSpinnerItem, R.array.kingSize_array, kingBedSizeSPN);
                } else {
                    GeneralFunction.slideUpAnimationForListView(parentActivity, constraintLayoutSpinnerItem, listViewSpinnerItem, R.array.kingSize_notEntire_array, kingBedSizeSPN);
                }
                MainActivity.isOpen = true;
                break;

            case R.id.bathroomSPN:
                if (isEntire) {
                    GeneralFunction.slideUpAnimationForListView(parentActivity, constraintLayoutSpinnerItem, listViewSpinnerItem, R.array.bathroom_array, bathroomSPN);
                } else {
                    GeneralFunction.slideUpAnimationForListView(parentActivity, constraintLayoutSpinnerItem, listViewSpinnerItem, R.array.bathroom_notEntire_array, bathroomSPN);
                }
                MainActivity.isOpen = true;
                break;

            case R.id.addressAccommodationET:
                GeneralFunction.openPlacePicker(parentActivity, AccommodationDetailFragment.this);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == 158) {
                if (data == null) {
                    //no data present
                    return;
                }
            }
            if (requestCode == Config.REQUEST_PLACE_PICKER) {
                place = PlacePicker.getPlace(parentActivity, data);

                if (place != null && place.getAddress() != null && !place.getAddress().equals("")) {
                    isNewAddress = true;
                    addressAccommodationET.setText(place.getAddress().toString());
                    getGeoPoint = new GeoPoint(place.getLatLng().latitude, place.getLatLng().longitude);

//                    GeneralFunction.setLocationNameForPlace(parentActivity, place, workspaceTV);
                } else {
                    GeneralFunction.openMessageNotificationDialog(parentActivity, parentActivity.getString(R.string.error_place_picker), R.drawable.notice_bad, TAG);
                }
            }
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    Uri resultUri = result.getUri();

                    if (imageButton == 1) {
                        isImgReplaceFromDatabase = true;
                        this.filePath = FilePath.getPath(parentActivity, resultUri);
                        GlideImageSetting(parentActivity, resultUri, btn_cover_photo);
                    }
                    if (imageButton == 2) {
                        isImgReplaceFromDatabase = true;
                        this.getElectricBillPath = FilePath.getPath(parentActivity, resultUri);
                        GlideImageSetting(parentActivity, resultUri, electricBillIB);
                    }


                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Exception error = result.getError();
                }
            }

            if (requestCode == CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE) {
                if (ContextCompat.checkSelfPermission(parentActivity, Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_DENIED) {
                    ActivityCompat.requestPermissions(parentActivity, new String[]{Manifest.permission.CAMERA}, requestCode);

                }
            }
        }
    }

    private void setSpinnerArrayEntirePlace() {
        bedroomSPN.setEnabled(true);
        bedroomSPN.setClickable(true);
        bedroomSPN.setHint("");
    }

    private void setSpinnerArrayNotEntirePlace() {
        bedroomSPN.setEnabled(false);
        bedroomSPN.setClickable(false);
        bedroomSPN.setText(Room.NOT_AVAILABLE);
        bedroomSPN.setHint(Room.NOT_AVAILABLE);
    }

    private void submitAccommodation() {
        String getLot = lotDetailsET.getText().toString();
        String getAddress = addressAccommodationET.getText().toString();
//        if(getGeoPoint == null){
//            getGeoPoint = new GeoPoint(place.getLatLng().latitude,place.getLatLng().longitude);
//        }
        String getAccommodationName = hostET.getText().toString();
        String getCurrencyType = currencyTV.getText().toString();
        Double getCurrencyValue = Double.parseDouble(currencyPerNightET.getText().toString());
        Integer getGuestQT = Integer.parseInt(guestMaxSPN.getText().toString());
        Integer getBedroomQT = Integer.parseInt(bedroomSPN.getText().toString());
        Integer getSingleSizeBedQT = Integer.parseInt(singleBedSizeSPN.getText().toString());
        Integer getQueenSizeBedQT = Integer.parseInt(queenBedSizeSPN.getText().toString());
        Integer getKingSizeBedQT = Integer.parseInt(kingBedSizeSPN.getText().toString());
        Integer getBathRoomQT = Integer.parseInt(bathroomSPN.getText().toString());

        if (GeneralFunction.checkAndReturnImageView(parentActivity, btn_cover_photo, R.drawable.ic_camera, R.string.error_please_upload_a_photo, isFromDatabase)) {
            if (getGeoPoint == null) {
                GeneralFunction.openMessageNotificationDialog(parentActivity, "Please enter the accommodation address", R.drawable.notice_bad, "invalid_string");
            } else if (GeneralFunction.checkAndReturnImageButton(parentActivity, electricBillIB, R.drawable.bg_placeholder_new, R.string.error_please_upload_electric_bill, isFromDatabase)) {

                if (isValidInput(getRoomType, getLot, getAccommodationName, getCurrencyType, getCurrencyValue, getGuestQT, getBedroomQT, getSingleSizeBedQT,
                        getQueenSizeBedQT, getKingSizeBedQT, getBathRoomQT) && getGeoPoint.getLatitude() != 0 && getGeoPoint.getLongitude() != 0) {

                    Room room = new Room(filePath, getRoomType, getAccommodationName, getLot, getAddress, getGeoPoint, getElectricBillPath, getCurrencyType, getCurrencyValue, getGuestQT, getBedroomQT, getSingleSizeBedQT,
                            getQueenSizeBedQT, getKingSizeBedQT, getBathRoomQT);

                    if (!isFromDatabase) {
                        GeneralFunction.saveHostSharedPreferences(parentActivity, room, Room.ACCOMMODATION_DETIALS, Room.ACCOMMODATION_DETIALS_ALL);
                    }
                    Log.d("successful", room.toString());
                    parentActivity.initializeProgressIndicator(parentActivity.getString(R.string.host_upload_detail), false, this);
                    parentActivity.controlProgressIndicator(true, this);

                    saveAccommodationDetailToFirestore(filePath, getRoomType, getAccommodationName, getLot, getGeoPoint, getAddress, getElectricBillPath, getCurrencyType, getCurrencyValue, getGuestQT, getBedroomQT, getSingleSizeBedQT,
                            getQueenSizeBedQT, getKingSizeBedQT, getBathRoomQT);
                    checkBedroomQuantity();

                } else {
                    Log.d("failed", "Failed Save to Shared Preferences");
                }
            }
        }
    }

    private boolean isValidInput(String getRoomType, String getLot, String getAccommodationName, String getCurrencyType, Double getCurrencyValue, Integer getGuestQT, Integer getBedroomQT
            , Integer getSingleSizeBedQT, Integer getQueenSizeBedQT, Integer getKingSizeBedQT, Integer getBathRoomQT) {
        return GeneralFunction.checkAndReturnString(getRoomType, parentActivity.getString(R.string.error_please_choose_room_type), parentActivity)
                &&
                GeneralFunction.checkAndReturnString(getLot, parentActivity.getString(R.string.error_please_enter_lot_details), parentActivity)
                &&
                GeneralFunction.checkAndReturnString(getAccommodationName, parentActivity.getString(R.string.error_please_enter_accommodation_name), parentActivity)
                &&
                GeneralFunction.checkAndReturnString(getCurrencyType, parentActivity.getString(R.string.error_please_select_currency_type), parentActivity)
                &&
                GeneralFunction.checkAndReturnDouble(getCurrencyValue, parentActivity.getString(R.string.error_please_enter_currency_value), parentActivity)
                &&
                GeneralFunction.checkAndReturnRoomQTY(getGuestQT, parentActivity.getString(R.string.error_please_select_guest_quantity), parentActivity)
                &&
                GeneralFunction.checkAndReturnRoomQTY(getBedroomQT, parentActivity.getString(R.string.error_please_select_bedroom_quantity), parentActivity)
                &&
                GeneralFunction.checkAndReturnBedQTY(getSingleSizeBedQT, parentActivity.getString(R.string.error_please_select_singlebed_quantity), parentActivity)
                &&
                GeneralFunction.checkAndReturnBedQTY(getQueenSizeBedQT, parentActivity.getString(R.string.error_please_select_queenbed_quantity), parentActivity)
                &&
                GeneralFunction.checkAndReturnBedQTY(getKingSizeBedQT, parentActivity.getString(R.string.error_please_select_kingbed_quantity), parentActivity)
                &&
                GeneralFunction.checkAndReturnRoomQTY(getBathRoomQT, parentActivity.getString(R.string.error_please_select_bathroom_quantity), parentActivity);

    }

    private void goToUploadRoomPhotoFragment() {
        if (uploadOutlook && uploadElectricBill) {
            GeneralFunction.openMessageNotificationDialog(parentActivity, getString(R.string.host_successful_next), R.drawable.notice_good, TAG);
            if (getRoomType.equals(Room.TYPE_ROOM_ENTIRE_PLACE)) {
                if (!isFromDatabase) {
                    UploadRoomPhotoFragment uploadRoomPhotoFragment = UploadRoomPhotoFragment.newInstance(parentActivity);
                    GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, uploadRoomPhotoFragment).addToBackStack(UploadRoomPhotoFragment.TAG).commit();
                    GeneralFunction.handleUploadSuccessTommy(parentActivity, AccommodationDetailFragment.this, R.string.employer_job_upload_success, TAG);
                    uploadOutlook = false;
                    uploadElectricBill = false;
                } else {
                    if(!getBedQuantity.equals("0")){
                        int bedroomQTY = Integer.parseInt(bedroomSPN.getText().toString());
                        UploadRoomPhotoFragment uploadRoomPhotoFragment = UploadRoomPhotoFragment.newInstance(parentActivity, roomUrl, roomName, isFromDatabase, documentPushId, roomPicPushIdFromFirestore, bedroomQTY, isEditProfile);
                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, uploadRoomPhotoFragment).addToBackStack(UploadRoomPhotoFragment.TAG).commit();
                        GeneralFunction.handleUploadSuccessTommy(parentActivity, AccommodationDetailFragment.this, R.string.employer_job_upload_success, TAG);
                        uploadOutlook = false;
                        uploadElectricBill = false;
                    }
                    else{
                        UploadAccommodationPhotoFragement uploadAccommodationPhotoFragement = UploadAccommodationPhotoFragement.newInstance(parentActivity, isFromDatabase,documentPushId, isEditProfile);
                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, uploadAccommodationPhotoFragement).addToBackStack(UploadAccommodationPhotoFragement.TAG).commit();
                        GeneralFunction.handleUploadSuccessTommy(parentActivity, AccommodationDetailFragment.this, R.string.employer_job_upload_success, TAG);
                        uploadOutlook = false;
                        uploadElectricBill = false;
                    }
                }

            } else {
                UploadAccommodationPhotoFragement uploadAccommodationPhotoFragement = UploadAccommodationPhotoFragement.newInstance(parentActivity, isFromDatabase, documentPushId, isEditProfile);
                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, uploadAccommodationPhotoFragement).addToBackStack(UploadAccommodationPhotoFragement.TAG).commit();
                GeneralFunction.handleUploadSuccessTommy(parentActivity, AccommodationDetailFragment.this, R.string.employer_job_upload_success, TAG);
                uploadOutlook = false;
                uploadElectricBill = false;
            }
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (v.getId() == R.id.accommodation_detailSV) {
            KeyboardUtilities.hideSoftKeyboard(parentActivity);
        }
        return false;
    }

    private void saveAccommodationDetailToFirestore(String outlook, String roomType, String accommodationName, String lot, GeoPoint getGeopoint, String address, String electricBillPath, String currencyType, Double currencyValue, Integer guestQT, Integer bedroomQT, Integer singleSizeBedQT,
                                                    Integer queenSizeBedQT, Integer kingSizeBedQT, Integer bathRoomQT) {
        final HashMap<String, Object> roomMap = new HashMap<>();
        roomMap.put(Room.HOST_ID, parentActivity.uid);
        roomMap.put(Room.ROOM_TYPE, roomType);
        roomMap.put(Room.ACCOMMODATION_NAME, accommodationName);
        roomMap.put(Room.ACCOMMODATION_LOT_DETAILS, lot);
        roomMap.put(Room.ACCOMMODATION_ADDRESS, address);
        roomMap.put(Room.ACCOMMODATION_GEOPOINT, getGeopoint);
        roomMap.put(Room.CURRENCY_TYPE, currencyType);
        roomMap.put(Room.CURRENCY_VALUE, currencyValue);
        roomMap.put(Room.GUEST_QUANTITY, guestQT);
        roomMap.put(Room.BEDROOM_QUANTITY, bedroomQT);
        roomMap.put(Room.SINGLE_SIZE_BED_QUANTITY, singleSizeBedQT);
        roomMap.put(Room.QUEEN_SIZE_BED_QUANTITY, queenSizeBedQT);
        roomMap.put(Room.KING_SIZE_BED_QUANTITY, kingSizeBedQT);
        roomMap.put(Room.BATHROOM_QUANTITY, bathRoomQT);
//        roomMap.put(Room.IS_APPROVED, false);
        if (!isDateCreated) {
            roomMap.put(Room.DATE_CREATED, FieldValue.serverTimestamp());
        }
        roomMap.put(Room.DATE_UPDATED, FieldValue.serverTimestamp());

        if (!isFromDatabase) {
            getPushId = GeneralFunction.getDocumentPath(parentActivity, Room.ACCOMMODATION_DETIALS, Room.DIR_DOCUMENT, "");
            getElectricBillPushId = GeneralFunction.getDocumentPath(parentActivity, Room.ACCOMMODATION_DETIALS, Room.DIR_ELECTRIC_BILL, "");
        }

        if(place == null && placeAddress == null){
            GeneralFunction.handleUploadError(parentActivity,AccommodationDetailFragment.this,R.string.error_occured_getting_place_details,TAG);
        }else{
            if(placeAddress != null && place == null){
                proceedAfterPlaceAddress(roomMap);
            }else{
                if(place.getId() != null){
                    new UnifyPlaceAddress(parentActivity, place.getId(), new UnifyPlaceAddress.OnUnifyComplete() {
                        @Override
                        public void onUnifySuccess(PlaceAddress placeAddress) {
                            parentActivity.updateProgress(40,AccommodationDetailFragment.this);
                            roomMap.put(Room.ACCOMMODATION_PLACE_ADDRESS,PlaceAddress.getHashmap(placeAddress,place.getId()));
                            roomMap.put(Room.ACCOMMODATION_PLACE_INDEX,PlaceAddress.getPlaceIndex(placeAddress));
                            proceedAfterPlaceAddress(roomMap);
                        }

                        @Override
                        public void onUnifyFailed(Exception e) {
                            GeneralFunction.handleUploadError(parentActivity,AccommodationDetailFragment.this,R.string.error_occured_getting_place_details,TAG);
                            e.printStackTrace();
                        }
                    });
                }else{
                    GeneralFunction.handleUploadError(parentActivity,AccommodationDetailFragment.this,R.string.error_occured_getting_place_details,TAG);
                }
            }
        }

    }//end saveAccommodationDetailtoFirestore

    private void proceedAfterPlaceAddress(HashMap<String, Object> roomMap){
        //Add a new document with a generated ID
        if (getPushId != null && !getPushId.trim().equals("")) {
            roomRef = GeneralFunction.getFirestoreInstance()
                    .collection(Accommodation.URL_ACCOMMODATION)
                    .document(getPushId);

            uploadOutlookPic(roomMap, getPushId);
            uploadOutlook = true;
        } else {
            roomRef = GeneralFunction.getFirestoreInstance()
                    .collection(Accommodation.URL_ACCOMMODATION)
                    .document();
            //save the push id into shared preferences
            setPushId = roomRef.getId();
            if (!isFromDatabase) {
                //save accommodation details into shared preferences
                GeneralFunction.saveDocumentPath(parentActivity, Room.ACCOMMODATION_DETIALS, Room.DIR_DOCUMENT, setPushId);
            }

            uploadOutlookPic(roomMap, setPushId);
            uploadOutlook = true;
        }
    }

    private void saveAccommodationElectricBillToFirestore() {
        HashMap<String, Object> roomPicMap = new HashMap<>();
        roomPicMap.put(Room.NAME, Room.ELECTRIC_BILL);
        if (!isDateCreated) {
            roomPicMap.put(Room.DATE_CREATED, FieldValue.serverTimestamp());
        }

        if (!isFromDatabase) {
            getPushId = GeneralFunction.getDocumentPath(parentActivity, Room.ACCOMMODATION_DETIALS, Room.DIR_DOCUMENT, "");
            getElectricBillPushId = GeneralFunction.getDocumentPath(parentActivity, Room.ACCOMMODATION_DETIALS, Room.DIR_ELECTRIC_BILL, "");
        }

        if (getPushId != null && !getPushId.equals("")) {
            if (getElectricBillPushId != null && !getElectricBillPushId.trim().equals("")) {
                electricBillRef = GeneralFunction.getFirestoreInstance()
                        .collection(Accommodation.URL_ACCOMMODATION)
                        .document(getPushId)
                        .collection(Room.URL_ROOM_PROOF)
                        .document(getElectricBillPushId);

                uploadElectricBillPic(roomPicMap, electricBillRef, getElectricBillPushId);
                uploadElectricBill = true;
            } else {
                electricBillRef = GeneralFunction.getFirestoreInstance()
                        .collection(Accommodation.URL_ACCOMMODATION)
                        .document(getPushId)
                        .collection(Room.URL_ROOM_PROOF)
                        .document();

                setElectricBillPushId = electricBillRef.getId();
                //save accommodation details into shared preferences
                if (!isFromDatabase) {
                    GeneralFunction.saveDocumentPath(parentActivity, Room.ACCOMMODATION_DETIALS, Room.DIR_ELECTRIC_BILL, setElectricBillPushId);

                }

                uploadElectricBillPic(roomPicMap, electricBillRef, setElectricBillPushId);
                uploadElectricBill = true;
            }
        }
    }

    private void setRoomDetails(final DocumentReference roomRef, HashMap<String, Object> roomMap) {
        if (isFromDatabase) {
            roomRef.update(roomMap).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    if (uploadOutlook) {
                        saveAccommodationElectricBillToFirestore();
                    }
                    if (uploadOutlook && uploadElectricBill) {
                        goToUploadRoomPhotoFragment();
                    }

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    GeneralFunction.handleUploadError(parentActivity, AccommodationDetailFragment.this, R.string.host_err_upload, TAG);
                }
            });
        } else {
            roomRef.set(roomMap).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    if (uploadOutlook) {
                        saveAccommodationElectricBillToFirestore();
                    }
                    if (uploadOutlook && uploadElectricBill) {
                        goToUploadRoomPhotoFragment();
                    }

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    GeneralFunction.handleUploadError(parentActivity, AccommodationDetailFragment.this, R.string.host_err_upload, TAG);
                }
            });
        }

    }

    private void uploadOutlookPic(final HashMap<String, Object> roomMap, final String pushId) {
        if (filePath != null && !isFromDatabase) {
            final StorageReference ref = GeneralFunction.getFirebaseStorageInstance().child(Room.URL_ROOM + "/" + parentActivity.uid + "/" + Room.URL_OUTLOOK + "/" + pushId + "/" + Room.URL_ROOM_OUTLOOK_PIC);

            UploadTask uploadTask = ref.putBytes(GeneralFunction.getCompressedBitmap(parentActivity, filePath));

            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                    GeneralFunction.handleUploadError(parentActivity, AccommodationDetailFragment.this, R.string.employer_err_job_posting_job, TAG);
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                    ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri downloadUri) {
                            uploadOutlook = true;
                            if (downloadUri != null) {
                                roomMap.put(Room.URL_OUTLOOK, downloadUri.toString());
                                setRoomDetails(roomRef, roomMap);
                            }
                            isBtnUploadClick = false;
                            isImgReplaceFromDatabase = false;
                        }
                    });
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    // + 10 is because i set the progress to 10 when it finished saving the initial firebase data
                    // * 80 is because i want the progress to go to maximum 90% only when the image is finished uploaded
                    // so that the final 10% is set when the firebase updates the imageUrl data as the final step
//                        int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * 80) + 10;
//                        parentActivity.updateProgress((float) totalProgress,AccommodationDetailFragment.this);
                    //custom make it because I want to upload 2 time and it was not correct way
                    int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * 80) + 10;
                    parentActivity.updateProgress((float) totalProgress, AccommodationDetailFragment.this);
                }
            });
        } else {
            if (filePath != null && isFromDatabase) {
                final StorageReference ref = GeneralFunction.getFirebaseStorageInstance().child(Room.URL_ROOM + "/" + parentActivity.uid + "/" + Room.URL_OUTLOOK + "/" + pushId + "/" + Room.URL_ROOM_OUTLOOK_PIC);

                UploadTask uploadTask = ref.putBytes(GeneralFunction.getCompressedBitmap(parentActivity, filePath));

                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        GeneralFunction.handleUploadError(parentActivity, AccommodationDetailFragment.this, R.string.employer_err_job_posting_job, TAG);
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                        ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri downloadUri) {
                                uploadOutlook = true;
                                if (downloadUri != null) {
                                    roomMap.put(Room.URL_OUTLOOK, downloadUri.toString());
                                    setRoomDetails(roomRef, roomMap);
                                }
                                isBtnUploadClick = false;
                                isImgReplaceFromDatabase = false;
                            }
                        });
                    }
                }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * 80) + 10;
                        parentActivity.updateProgress((float) totalProgress, AccommodationDetailFragment.this);
                    }
                });
            } else {
                isBtnUploadClick = false;
                isImgReplaceFromDatabase = false;
                setRoomDetails(roomRef, roomMap);
            }
        }
    }//end uploadphoto()

    private void uploadElectricBillPic(final HashMap<String, Object> roomMap, final DocumentReference electricBillRef, final String pushId) {
        if (getElectricBillPath != null && !isFromDatabase) {

            final StorageReference ref = GeneralFunction.getFirebaseStorageInstance().child(Room.URL_ROOM + "/" + parentActivity.uid + "/" + Room.URL_ELECTRIC_BILL + "/" + pushId + "/" + Room.URL_ROOM_ELECTRIC_BILL_PIC);

            UploadTask uploadTask = ref.putBytes(GeneralFunction.getCompressedBitmap(parentActivity, getElectricBillPath));
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                    GeneralFunction.handleUploadError(parentActivity, AccommodationDetailFragment.this, R.string.employer_err_job_posting_job, TAG);
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri downloadUri) {
                            uploadElectricBill = true;
                            if (downloadUri != null) {
                                roomMap.put(Room.URL_ELECTRIC_BILL, downloadUri.toString());
                                setRoomDetails(electricBillRef, roomMap);
                            }
                            isBtnUploadElectricClick = false;
                            isImgReplaceFromDatabase = false;
                        }
                    });

                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * 80) + 10;
                    parentActivity.updateProgress((float) totalProgress, AccommodationDetailFragment.this);
                }
            });//end addOnprogresslistener
        } else {
            if (getElectricBillPath != null && isFromDatabase) {

                final StorageReference ref = GeneralFunction.getFirebaseStorageInstance().child(Room.URL_ROOM + "/" + parentActivity.uid + "/" + Room.URL_ELECTRIC_BILL + "/" + pushId + "/" + Room.URL_ROOM_ELECTRIC_BILL_PIC);

                UploadTask uploadTask = ref.putBytes(GeneralFunction.getCompressedBitmap(parentActivity, getElectricBillPath));
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        GeneralFunction.handleUploadError(parentActivity, AccommodationDetailFragment.this, R.string.employer_err_job_posting_job, TAG);
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri downloadUri) {
                                uploadElectricBill = true;
                                if (downloadUri != null) {
                                    roomMap.put(Room.URL_ELECTRIC_BILL, downloadUri.toString());
                                    setRoomDetails(electricBillRef, roomMap);
                                }
                                isBtnUploadElectricClick = false;
                                isImgReplaceFromDatabase = false;
                            }
                        });

                    }
                }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * 80) + 10;
                        parentActivity.updateProgress((float) totalProgress, AccommodationDetailFragment.this);
                    }
                });//end addOnprogresslistener
            } else {
                isBtnUploadElectricClick = false;
                isImgReplaceFromDatabase = false;
                setRoomDetails(electricBillRef, roomMap);
            }
        }
    }

    private void setSharedPreferences() {
        try {
            SharedPreferences sharedPreferences = parentActivity.getSharedPreferences(ACCOMMODATION_DETIALS, MODE_PRIVATE);

            // Get saved string data in it.
            String userInfoListJsonString = sharedPreferences.getString(ACCOMMODATION_DETIALS_ALL, "");

            // Create Gson object and translate the json string to related java object array.
            Gson gson = new Gson();

            Room userInfoDtoArray = gson.fromJson(userInfoListJsonString, Room.class);

            // Loop the UserInfoDTO array and print each UserInfoDTO data in android monitor as debug log.
            // Get each user info in dto.
            Room userInfoDto = userInfoDtoArray;

            if (userInfoDto != null) {
                if (userInfoDto.urlOutlook != null) {
                    GeneralFunction.showPicFromFilePath(btn_cover_photo, userInfoDto.urlOutlook);
                    filePath = userInfoDto.urlOutlook;
                }

                if (userInfoDto.urlElectricBill != null) {
                    GeneralFunction.showPicFromFilePath(electricBillIB, userInfoDto.urlElectricBill);
                    getElectricBillPath = userInfoDto.urlElectricBill;
                }

                if (userInfoDto.roomType != null) {
                    setRadioButtonWithSharePreference(userInfoDto.roomType);
                    getRoomType = userInfoDto.roomType;
                }

                if (userInfoDto.accommodationGeoPoint != null) {
                    getGeoPoint = userInfoDto.accommodationGeoPoint;
//                    Log.d("dsfsdf",getGeoPoint.toString());
                }

                hostET.setText(userInfoDto.accommodationName);
                lotDetailsET.setText(userInfoDto.location);
                addressAccommodationET.setText(userInfoDto.address);
                bathroomSPN.setText(userInfoDto.bathroomQuantity);
                bedroomSPN.setText(userInfoDto.bedroomQuantity);
                //to gt bedroom quantity
                getBedQuantity = userInfoDto.bedroomQuantity;
                currencyTV.setText(userInfoDto.currencyType);
                currencyPerNightET.setText(String.valueOf(userInfoDto.currencyValue));
                guestMaxSPN.setText(userInfoDto.guestQuantity);
                kingBedSizeSPN.setText(userInfoDto.kingSizeBedQuantity);
                queenBedSizeSPN.setText(userInfoDto.queenSizeBedQuantity);
                singleBedSizeSPN.setText(userInfoDto.singleSizeBedQuantity);
                queenBedSizeSPN.setText(userInfoDto.queenSizeBedQuantity);

            }
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
    }

    private void setRadioButtonWithSharePreference(String type) {
        if (type != null && !type.trim().equals("")) {
            rentDetailCL.setVisibility(View.VISIBLE);
            //java.lang.illegalstateexception: fragment not attached to a context.
            //because the activity is not attach yet so cannot use getResource
            //use isadded()
            //https://stackoverflow.com/questions/6870325/android-compatibility-package-fragment-not-attached-to-activity
            if (isAdded()) {
                if (type.equals(Room.TYPE_ROOM_ENTIRE_PLACE)) {
                    btn_entirePlace.setBackgroundResource(R.drawable.rounded_shape_button_done);
                    btn_entirePlace.setTextColor(getResources().getColor(R.color.white));
                }
                if (type.equals(Room.TYPE_ROOM_PRIVATE)) {
                    btn_privateRoom.setBackgroundResource(R.drawable.rounded_shape_button_done);
                    btn_privateRoom.setTextColor(getResources().getColor(R.color.white));
                }
                if (type.equals(Room.TYPE_ROOM_SHARE)) {
                    btn_shareRoom.setBackgroundResource(R.drawable.rounded_shape_button_done);
                    btn_shareRoom.setTextColor(getResources().getColor(R.color.white));
                }
            }
        } else {
            rentDetailCL.setVisibility(View.GONE);
        }
    }

    private void checkBedroomQuantity() {
        if(getBedQuantity == null){
            getBedQuantity = Integer.parseInt(bedroomSPN.getText().toString());

        }
        if (getBedQuantity != 0) {
            if (Integer.parseInt(bedroomSPN.getText().toString()) != getBedQuantity) {
//                Toast.makeText(parentActivity,"no equal",Toast.LENGTH_SHORT).show();
                GeneralFunction.clearSharedPreferencesWithType(parentActivity, Room.UPLOAD_ROOM_DETIALS_ALL);
            }
        }
    }

    private void retrieveFromFirestore() {

        DocumentReference outlookRef = db.collection(Accommodation.URL_ACCOMMODATION).document(documentPushId);
        outlookRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@javax.annotation.Nullable DocumentSnapshot documentSnapshot, @javax.annotation.Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Toast.makeText(parentActivity, String.valueOf(e), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (documentSnapshot != null) {
                    Room room = documentSnapshot.toObject(Room.class);
                    String accommodationOutlook = documentSnapshot.toObject(Room.class).getUrlOutlook();
                    String acccommodationName = room.getAccommodationName();
                    String roomType = room.getRoomType();
                    String lotDetails = room.getLocation();
                    String address = room.getAddress();
                    try{
                        placeAddress = PlaceAddress.getMapToClass((HashMap<String, Object>) documentSnapshot.get(Room.ACCOMMODATION_PLACE_ADDRESS));
                    }catch (Exception exception){
                        exception.printStackTrace();
                    }
//            String electricBill = room.getUrlElectricBill();
                    String currencyType = room.getCurrencyType();
                    Double currencyValue = room.getCurrencyValue();
                    Integer guestQT = room.getGuestQuantity();
                    Integer bedroomQT = room.getBedroomQuantity();
                    final Integer singleSizeQT = room.getSingleSizeBedQuantity();
                    Integer queenSizeQT = room.getQueenSizeBedQuantity();
                    Integer kingSizeQT = room.getKingSizeBedQuantity();
                    Integer bathroomQT = room.getBathroomQuantity();
                    Timestamp dateCreated = room.getDateCreated();
                    GeoPoint geoPoint = documentSnapshot.getGeoPoint(Room.ACCOMMODATION_GEOPOINT);

                    hostET.setText(acccommodationName);

                    setRadioButtonWithSharePreference(roomType);
                    if (getRoomType == null) {
                        getRoomType = roomType;
                    }
                    lotDetailsET.setText(lotDetails);
                    if (!isNewAddress) {
                        addressAccommodationET.setText(address);
                        getGeoPoint = geoPoint;
                    } else {
                        isNewAddress = false;
                    }

//                    if(getGeoPoint == null){
//                        getGeoPoint = geoPoint;
//                    }


                    currencyTV.setText(currencyType);
                    currencyPerNightET.setText(String.valueOf(currencyValue));
                    guestMaxSPN.setText(String.valueOf(guestQT));
                    if (bedroomQT.toString().equals(Room.NOT_AVAILABLE)) {
                        setSpinnerArrayNotEntirePlace();
                    } else {
                        bedroomSPN.setText(String.valueOf(bedroomQT));
                    }
                    singleBedSizeSPN.setText(String.valueOf(singleSizeQT));
                    queenBedSizeSPN.setText(String.valueOf(queenSizeQT));
                    kingBedSizeSPN.setText(String.valueOf(kingSizeQT));
                    bathroomSPN.setText(String.valueOf(bathroomQT));

                    if (dateCreated != null) {
                        isDateCreated = true;
                    }
                    GlideImageSetting(parentActivity, Uri.parse(accommodationOutlook), btn_cover_photo);

                }
            }
        });

        if (isFromDatabase && getPushId == null && getElectricBillPushId == null) {
            getPushId = documentPushId;
            getElectricBillPushId = electricBillPushId;

            //get room url and document id
            CollectionReference roomRef = db.collection(Accommodation.URL_ACCOMMODATION).document(documentPushId).collection(Room.URL_ROOM);
            roomRef.orderBy(Accommodation.ACCOMMODATION_POSITION, Query.Direction.ASCENDING)
                    .addSnapshotListener(new EventListener<QuerySnapshot>() {
                        @Override
                        public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                            if (e != null) {
                                Log.w(TAG, "Listen failed.", e);
                                return;
                            }
                            String source = queryDocumentSnapshots != null && queryDocumentSnapshots.getMetadata().hasPendingWrites()
                                    ? "Local" : "Server";
                            Log.d(TAG, source);
                            if (queryDocumentSnapshots != null) {
//                        queryDocumentSnapshots.getMetadata().isFromCache();
//                        queryDocumentSnapshots.getMetadata().hasPendingWrites();
                                for (int i = 0; i < queryDocumentSnapshots.size(); i++) {
                                    roomUrl.add(queryDocumentSnapshots.toObjects(Room.class).get(i).urlRoomPic);
                                    roomName.add(queryDocumentSnapshots.toObjects(Room.class).get(i).roomName);
                                }
                                roomUrl.size();
                                roomName.size();
                                for (DocumentSnapshot documentReference : queryDocumentSnapshots.getDocuments()) {
                                    roomPicPushIdFromFirestore.add(documentReference.getId());
                                }
                                roomPicPushIdFromFirestore.size();
                            }
                        }
                    });

        }
        if (documentPushId != null && electricBillPushId != null) {
            DocumentReference electricBillRef = db.collection(Accommodation.URL_ACCOMMODATION).document(documentPushId).collection(Room.URL_ROOM_PROOF).document(electricBillPushId);
            electricBillRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
                @Override
                public void onEvent(@javax.annotation.Nullable DocumentSnapshot documentSnapshot, @javax.annotation.Nullable FirebaseFirestoreException e) {
                    if (e != null) {
                        Toast.makeText(parentActivity, String.valueOf(e), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (documentSnapshot != null) {
                        Room room = documentSnapshot.toObject(Room.class);
                        if (room != null) {
                            String electricBill = room.getUrlElectricBill();
                            GlideImageSetting(parentActivity, Uri.parse(electricBill), electricBillIB);
                        }
                    }
                }
            });

        }
    }

    @Override
    public void onResume() {
        super.onResume();

        parentActivity.selectHotel();
        parentActivity.setToolbarTitle(parentActivity.getString(R.string.host_an_accommodation));

        if (isFromDatabase) {
            if (isNewAddress || isBtnUploadClick && isImgReplaceFromDatabase || isBtnUploadElectricClick && isImgReplaceFromDatabase) {
                GeneralFunction.clearGlideCacheMemory(parentActivity);
            } else {
                if ((!isBtnUploadClick && !isImgReplaceFromDatabase) || (!isBtnUploadElectricClick && !isImgReplaceFromDatabase)) {
                    GeneralFunction.clearGlideCacheMemory(parentActivity);
                    //todo got error here
                    getElectricBill();
                    retrieveFromFirestore();
                } else {
                    getElectricBill();
                    retrieveFromFirestore();
                }
            }
        } else {
            isFromDatabase = false;
            setSharedPreferences();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private void getElectricBill(){
        if(room != null){
            if(room.id != null){
                documentPushId = room.id;
                GeneralFunction.getFirestoreInstance()
                        .collection(Accommodation.URL_ACCOMMODATION)
                        .document(room.id)
                        .collection(URL_ROOM_PROOF)
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                if(task.isSuccessful()){
                                    for(QueryDocumentSnapshot queryDocumentSnapshot : task.getResult()){
                                        Room room = queryDocumentSnapshot.toObject(Room.class);
                                        if(room.urlElectricBill != null){
                                            GlideImageSetting(parentActivity, Uri.parse(room.urlElectricBill), electricBillIB);
                                        }
                                        getElectricBillPushId = queryDocumentSnapshot.getId();
                                    }
                                }
                            }
                        });
            }
        }
    }
}