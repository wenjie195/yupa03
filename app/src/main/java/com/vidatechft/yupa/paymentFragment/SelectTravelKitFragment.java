package com.vidatechft.yupa.paymentFragment;

import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.MainSearchFragment.SearchHostRoomDateFragment;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.SelectTravelKtAdapter;
import com.vidatechft.yupa.classes.BookingHistory;
import com.vidatechft.yupa.classes.CurrencyRate;
import com.vidatechft.yupa.classes.Favourite;
import com.vidatechft.yupa.classes.Itinerary;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.classes.Search;
import com.vidatechft.yupa.classes.TravelKit;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;

import static com.vidatechft.yupa.utilities.GeneralFunction.getDecimalFormat;

public class SelectTravelKitFragment extends Fragment implements View.OnClickListener {
    public static final String TAG = SelectTravelKitFragment.class.getName();
    private MainActivity parentActivity;
    private TravelKit travelKit;
    private BookingHistory bookingHistory;
    private Itinerary itinerary;
    private Search search;

    private RecyclerView selectTravelKitRV;
    private ListenerRegistration getSelectTravelKitListener;
    private HashMap<String,Boolean> selectedTravelKitMap = new HashMap<>();
    public HashMap<String,HashMap<String,Object>> allBuyTravelKitMap = new HashMap<>();
    public HashMap<String,HashMap<String,Object>> allBuyTravelKitMapPrice = new HashMap<>();
    private HashMap<String,Object> travelKitCheckBoxMap = new HashMap<>();
    private HashMap<String,HashMap<String,Long>> quantityMap = new HashMap<>();
    private Button btnBuyNow;
    private ArrayList<TravelKit> buyNowList = new ArrayList<>();
    private ArrayList<TravelKit> allTravelKitList = new ArrayList<>();
    private ArrayList<String> selectedAddOn = new ArrayList<>();
    private Double traveKitPrice = 0.0;

//    display accmmodation and itnerary details
    private ConstraintLayout itiCL,accCL;
    private ImageView itiIV,accIV;
    private TextView itiNameTV, itiPriceTV, accNameTV, accDateTV, accPriceTV;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static SelectTravelKitFragment newInstance(MainActivity parentActivity, TravelKit travelKit, BookingHistory bookingHistory) {
        SelectTravelKitFragment fragment = new SelectTravelKitFragment();

        fragment.parentActivity = parentActivity;
        fragment.travelKit = travelKit;
        fragment.bookingHistory = bookingHistory;

        return fragment;
    }

    public static SelectTravelKitFragment newInstance(MainActivity parentActivity, TravelKit travelKit, Itinerary itinerary, Search search) {
        SelectTravelKitFragment fragment = new SelectTravelKitFragment();

        fragment.parentActivity = parentActivity;
        fragment.travelKit = travelKit;
        fragment.itinerary = itinerary;
        fragment.search = search;

        return fragment;
    }

    public static SelectTravelKitFragment newInstance(MainActivity parentActivity,HashMap<String,Boolean> selectedTravelKitMap,HashMap<String,Object> travelKitCheckBoxMap ) {
        SelectTravelKitFragment fragment = new SelectTravelKitFragment();

        fragment.parentActivity = parentActivity;
//        fragment.allBuyTravelKitMap = allBuyTravelKitMap;
//        fragment.allBuyTravelKitMapPrice = allBuyTravelKitMapPrice;
        fragment.selectedTravelKitMap = selectedTravelKitMap;
        fragment.travelKitCheckBoxMap = travelKitCheckBoxMap;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_select_travel_kit, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.hideBtmNav();
                selectTravelKitRV = view.findViewById(R.id.selectTravelKitRV);
                btnBuyNow = view.findViewById(R.id.btnBuyNow);
                btnBuyNow.setOnClickListener(SelectTravelKitFragment.this);

                if(itinerary != null){
                    setupItineraryLayout(view);
                }

                if(bookingHistory != null && bookingHistory.room != null){
                    setupAccommodationLayout(view);
                }
                getTravelKitDetail();
            }
        });

        return view;
    }

    private void setupItineraryLayout(View view){
        itiCL = view.findViewById(R.id.itiCL);
        itiIV = view.findViewById(R.id.itiIV);
        itiNameTV = view.findViewById(R.id.itiNameTV);
        itiPriceTV = view.findViewById(R.id.itiPriceTV);

        if(itinerary.itineraryPicUrl != null && !itinerary.itineraryPicUrl.trim().isEmpty()){
            GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(itinerary.itineraryPicUrl),itiIV);
        }else{
            GeneralFunction.GlideImageSettingOwnDrawable(parentActivity, R.drawable.ic_not_found,itiIV);
        }

        String title = GeneralFunction.getItineraryTitle(itinerary);
        if(title == null){
            title = "???";
        }
        itiNameTV.setText(title);

        String price = "???";
        if(itinerary.currencyCode != null && itinerary.price != null){
            price = GeneralFunction.getItineraryPrice(parentActivity,itinerary,null);
//            price = itinerary.currencyCode + " " + GeneralFunction.getDecimalFormat(2).format(itinerary.price);
        }
        itiPriceTV.setText(price);

        itiCL.setVisibility(View.VISIBLE);
    }

    private void setupAccommodationLayout(View view){
        accCL = view.findViewById(R.id.accCL);
        accIV = view.findViewById(R.id.accIV);
        accNameTV = view.findViewById(R.id.accNameTV);
        accDateTV = view.findViewById(R.id.accDateTV);
        accPriceTV = view.findViewById(R.id.accPriceTV);

        final Room room = bookingHistory.room;

        if(room.urlOutlook != null && !room.urlOutlook.trim().isEmpty()){
            GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(room.urlOutlook),accIV);
        }else{
            GeneralFunction.GlideImageSettingOwnDrawable(parentActivity, R.drawable.ic_not_found,accIV);
        }

        String title = "???";
        if(room.accommodationName != null){
            title = room.accommodationName;
        }
        accNameTV.setText(title);

        String date = "???";
        if(bookingHistory.checkinDate != null && bookingHistory.checkoutDate != null){
            if(bookingHistory.checkinDate.longValue() == bookingHistory.checkoutDate.longValue()){
                date = GeneralFunction.formatDateToStringWithSlash(bookingHistory.checkinDate);
            }else{
                date = GeneralFunction.formatDateToStringWithSlash(bookingHistory.checkinDate) + " - " + GeneralFunction.formatDateToStringWithSlash(bookingHistory.checkoutDate);
            }
        }
        accDateTV.setText(date);

        String price = "???";
        if(room.currencyType != null && room.currencyValue != null){
            price = GeneralFunction.getHostStayPrice(parentActivity,room);
//            price = room.currencyType + " " + GeneralFunction.getDecimalFormat(2).format(room.currencyValue);
        }
        accPriceTV.setText(price);

        accCL.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPause() {
        super.onPause();
        parentActivity.showBtmNav();
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.hideBtmNav();
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.booking_buy_travel_kit_toolbar));
            }
        });
    }

    private void getTravelKitDetail (){
        final ArrayList<TravelKit> travelKitsList = new ArrayList<>();
        getSelectTravelKitListener = GeneralFunction.getFirestoreInstance().collection(TravelKit.URL_TRAVELKIT)
                .whereEqualTo(TravelKit.IS_PUBLISH, true)
                .whereEqualTo(TravelKit.STATUS, TravelKit.STATUS_APPROVED)
                .orderBy(TravelKit.ORDER,Query.Direction.ASCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                        if(e != null){
                            Log.e(TAG,e.toString());
                            GeneralFunction.openMessageNotificationDialog(parentActivity,e.toString(),R.drawable.notice_bad,TAG);
                            return;
                        }
                        travelKitsList.clear();
                        allTravelKitList.clear();
                        if(queryDocumentSnapshots != null){
                            if(!queryDocumentSnapshots.isEmpty()){
                                for(QueryDocumentSnapshot queryDocumentSnapshot : queryDocumentSnapshots){
                                    TravelKit tempTravelKit = TravelKit.getTravelKit(parentActivity,queryDocumentSnapshot);
//                                    if(tempTravelKit != null && tempTravelKit.isAddOn){
//                                        tempAllAddOnList.add(tempTravelKit);
//                                    }
//                                    else{
//                                        travelKitsList.add(tempTravelKit);
//                                    }
                                    travelKitsList.add(tempTravelKit);
                                }
                                allTravelKitList.addAll(travelKitsList);
                            }
                            SelectTravelKtAdapter selectTravelKtAdapter = new SelectTravelKtAdapter(parentActivity,SelectTravelKitFragment.this,travelKitsList, selectedTravelKitMap,travelKitCheckBoxMap);
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(parentActivity);
                            selectTravelKitRV.setNestedScrollingEnabled(false);
                            selectTravelKitRV.setLayoutManager(linearLayoutManager);
                            selectTravelKitRV.setAdapter(selectTravelKtAdapter);

                            if(travelKit != null && travelKit.id != null && !travelKit.id.trim().isEmpty()){
                                for(TravelKit tempTravelKit : travelKitsList){
                                    if(tempTravelKit.id != null && TextUtils.equals(tempTravelKit.id,travelKit.id)){
                                        Fragment buyTravelKitFragment = BuyTravelKitFragment.newInstance(parentActivity,SelectTravelKitFragment.this,tempTravelKit,null,selectedTravelKitMap,travelKitCheckBoxMap);
                                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, buyTravelKitFragment).addToBackStack(BuyTravelKitFragment.TAG).commit();
                                        travelKit = null;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(getSelectTravelKitListener != null){
            getSelectTravelKitListener.remove();
        }
    }

    public void setQuantityMap(HashMap<String,HashMap<String,Long>> quantityMap){
        this.quantityMap = quantityMap;
    }

    public HashMap<String,HashMap<String,Long>> getQuantityMap(){
        return quantityMap;
    }

    private void buyNow(){
        if(GeneralFunction.isGuest()){
            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_guest_restrict_usage),R.drawable.notice_bad,TAG);
            return;
        }

        selectedAddOn.clear();
        buyNowList.clear();
        for(TravelKit tempTravelKit : allTravelKitList){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                if(Objects.equals(selectedTravelKitMap.get(tempTravelKit.name), true)){
                    buyNowList.add(tempTravelKit);
                }
            }
            else{
                try{
                    if(selectedTravelKitMap.get(tempTravelKit.name).equals(true)){
                        buyNowList.add(tempTravelKit);
                    }
                }
                catch (Exception e){
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_occured),R.drawable.notice_bad,TAG);
                }
            }
        }
        if(travelKitCheckBoxMap != null){
            for(Map.Entry<String,Object> entry : travelKitCheckBoxMap.entrySet()){
                String key = entry.getKey();
                Object value = entry.getValue();

                if(value.equals(true)){
                    selectedAddOn.add(key);
                }
            }
        }
        if(allBuyTravelKitMapPrice != null){
            for (Map.Entry<String, HashMap<String, Object>> map: allBuyTravelKitMapPrice.entrySet()) {
                traveKitPrice = 0.0;
                String key = map.getKey();
                Object value = map.getValue();
                for(Map.Entry<String, Object> price : map.getValue().entrySet()){
                    traveKitPrice = traveKitPrice + Double.parseDouble(String.valueOf(price.getValue()));
                }
            }
        }

        //this is to format the buy now list to include only selected product list and remove unselected products and pass to the payment for easy data manipulation
        ArrayList<TravelKit> formattedBuyNowList = new ArrayList<>();

        for(TravelKit tempTK : buyNowList){
            if(tempTK != null && tempTK.name != null && !tempTK.name.trim().isEmpty() && tempTK.productList != null){
                if(tempTK.name != null && quantityMap.get(tempTK.name) != null){
                    tempTK.quantityMap = quantityMap.get(tempTK.name);
                }
                formattedBuyNowList.add(tempTK);

                Iterator productIt = formattedBuyNowList.get(formattedBuyNowList.size() - 1).productList.entrySet().iterator();
                while (productIt.hasNext()) {
                    Map.Entry pair = (Map.Entry) productIt.next();
                    try{
                        if(allBuyTravelKitMapPrice == null || allBuyTravelKitMapPrice.get(tempTK.name) == null || allBuyTravelKitMapPrice.get(tempTK.name).get(pair.getKey().toString()) == null){
                            productIt.remove();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                        productIt.remove();
                    }
                }
            }
        }

        if(bookingHistory != null){
            Fragment paymentSummaryFragmentNo = PaymentSummaryFragment.newInstance(parentActivity,bookingHistory,itinerary,formattedBuyNowList);
            GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, paymentSummaryFragmentNo).addToBackStack(PaymentSummaryFragment.TAG).commit();
        }else{
            if(search != null){
                Fragment paymentSummaryFragmentNo = PaymentSummaryFragment.newInstance(parentActivity,bookingHistory,itinerary,formattedBuyNowList,search);
                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, paymentSummaryFragmentNo).addToBackStack(PaymentSummaryFragment.TAG).commit();
            }else{
                if(itinerary == null && formattedBuyNowList.isEmpty()){
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.booking_travel_kit_empty_err),R.drawable.notice_bad,TAG);
                    return;
                }

                Fragment searchHostRoomDateFragment = SearchHostRoomDateFragment.newInstance(parentActivity, bookingHistory,itinerary,formattedBuyNowList,search);
                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, searchHostRoomDateFragment).addToBackStack(SearchHostRoomDateFragment.TAG).commit();
            }

        }

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnBuyNow:
                buyNow();
                break;
        }
    }
}
