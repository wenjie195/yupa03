package com.vidatechft.yupa.paymentFragment;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.GeoPoint;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.BookingHistory;
import com.vidatechft.yupa.classes.EHailingBooking;
import com.vidatechft.yupa.utilities.Config;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

public class EHailingOrTaxiBookingFragment extends Fragment implements View.OnClickListener, DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {
    public static final String TAG = EHailingOrTaxiBookingFragment.class.getName();
    private MainActivity parentActivity;
    private Button btn_skip,btn_bookALift;
    private String type;
    private TextView arrivalDateTV,arrivalTimeTV,pickUpLocationTV;
    private DatePickerDialog datePickerDialog;
    private TimePickerDialog timePickerDialog;
    private int dobDay, dobMonth, dobYear;
    private SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");
    private SimpleDateFormat datetimeFormat = new SimpleDateFormat("dd MMM yyyyhh:mma");
    private Calendar calendar = new GregorianCalendar();
    private Place place;
    private GeoPoint getGeoPoint;
    private BookingHistory bookingHistory;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static EHailingOrTaxiBookingFragment newInstance(MainActivity parentActivity, String type, BookingHistory bookingHistory) {
        EHailingOrTaxiBookingFragment fragment = new EHailingOrTaxiBookingFragment();
        fragment.parentActivity = parentActivity;
        fragment.type = type;
        fragment.bookingHistory = bookingHistory;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_e_hailing_or_taxi_booking, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.hideBtmNav();
                //Button
                btn_skip = view.findViewById(R.id.btn_skip);
                btn_bookALift = view.findViewById(R.id.btn_bookALift);
                //TextView
                arrivalDateTV = view.findViewById(R.id.arrivalDateTV);
                arrivalTimeTV = view.findViewById(R.id.arrivalTimeTV);
                pickUpLocationTV = view.findViewById(R.id.pickUpLocationTV);


                //Listener
                btn_skip.setOnClickListener(EHailingOrTaxiBookingFragment.this);
                btn_bookALift.setOnClickListener(EHailingOrTaxiBookingFragment.this);
                arrivalDateTV.setOnClickListener(EHailingOrTaxiBookingFragment.this);
                arrivalTimeTV.setOnClickListener(EHailingOrTaxiBookingFragment.this);
                pickUpLocationTV.setOnClickListener(EHailingOrTaxiBookingFragment.this);


                datePickerDialog = GeneralFunction.getDatePickerDialog(parentActivity,EHailingOrTaxiBookingFragment.this);
                timePickerDialog = GeneralFunction.getTimePickerDialog(parentActivity,EHailingOrTaxiBookingFragment.this);
            }
        });

        return view;
    }



    @Override
    public void onPause() {
        super.onPause();
        parentActivity.showBtmNav();
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.mode_of_transport_toolbar));
                parentActivity.hideBtmNav();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_skip:
                parentActivity.goToHome();
                break;
            case R.id.btn_bookALift:
                submitBookingDetail();
                break;
            case R.id.arrivalDateTV:
                datePickerDialog.show();
                break;
            case R.id.arrivalTimeTV:
                timePickerDialog.show();
                break;
            case R.id.pickUpLocationTV:
                GeneralFunction.openPlacePicker(parentActivity, EHailingOrTaxiBookingFragment.this);
                break;
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        dobDay = dayOfMonth;
        dobMonth = month + 1;
        dobYear = year;
        calendar.set(year,month,dayOfMonth);

        parentActivity.runOnUiThread(new Runnable() {
            public void run() {
                arrivalDateTV.setText(format.format(calendar.getTime()));
            }
        });
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        Calendar cal = new GregorianCalendar();
        cal.set(1994,11,13,0,0,0);
        cal.set(Calendar.HOUR_OF_DAY,hourOfDay);
        cal.set(Calendar.MINUTE,minute);
        arrivalTimeTV.setText(GeneralFunction.formatDateToTime(cal.getTime()));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == Config.REQUEST_PLACE_PICKER) {
                place = PlacePicker.getPlace(parentActivity, data);

                if (place != null && place.getAddress() != null && !place.getAddress().equals("")) {
                    pickUpLocationTV.setText(place.getAddress().toString());
                    getGeoPoint = new GeoPoint(place.getLatLng().latitude, place.getLatLng().longitude);

//                    GeneralFunction.setLocationNameForPlace(parentActivity, place, workspaceTV);
                } else {
                    GeneralFunction.openMessageNotificationDialog(parentActivity, parentActivity.getString(R.string.error_place_picker), R.drawable.notice_bad, TAG);
                }
            }
        }
    }

    private void submitBookingDetail(){
        Map<String,Object> hailingMap = new HashMap<>();
        String arrivalDate = arrivalDateTV.getText().toString().trim();
        String arrivalTime = arrivalTimeTV.getText().toString().trim();
        String pickUpLocation = pickUpLocationTV.getText().toString().trim();

        if(isValidInput(arrivalDate,arrivalTime,pickUpLocation) && isValidGeopoint(getGeoPoint) && type != null && bookingHistory != null){
            parentActivity.initializeLoadingDialog(parentActivity.getString(R.string.e_hailing_or_taxi_booking),false);
            parentActivity.controlLoadingDialog(true,parentActivity);

            Date mDate = null;
            try {
                mDate = datetimeFormat.parse(arrivalDateTV.getText().toString() + arrivalTimeTV.getText().toString());
                long datetimeInMilliseconds = mDate.getTime();

                //Push in map
                hailingMap.put(EHailingBooking.PICKUP_TIME, datetimeInMilliseconds);
                hailingMap.put(EHailingBooking.TYPE, type);
                hailingMap.put(EHailingBooking.PICKUP_NAME, pickUpLocation);
                hailingMap.put(EHailingBooking.PICKUP_GPS, getGeoPoint);
                hailingMap.put(EHailingBooking.USER_ID, parentActivity.uid);
                hailingMap.put(EHailingBooking.BOOKING_HISTORY_ID, bookingHistory.id);
                hailingMap.put(EHailingBooking.DATE_CREATED, FieldValue.serverTimestamp());
                hailingMap.put(EHailingBooking.DATE_UPDATED, FieldValue.serverTimestamp());

                GeneralFunction.getFirestoreInstance()
                        .collection(EHailingBooking.URL_E_HAILING_BOOKING)
                        .document()
                        .set(hailingMap)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()){
                                    parentActivity.controlLoadingDialog(false,parentActivity);
                                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getResources().getString(R.string.e_hailing_or_taxi_successful_book_a_lift),R.drawable.notice_good,TAG);
                                }
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        parentActivity.controlLoadingDialog(false,parentActivity);
                        GeneralFunction.openMessageNotificationDialog(parentActivity,e.toString(),R.drawable.notice_bad,TAG);
                    }
                });



            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

    }

    private boolean isValidInput(String arrivalDate, String arrivalTime, String pickUpLocationTV){
        return  GeneralFunction.checkAndReturnString(arrivalDate,parentActivity.getString(R.string.e_hailing_or_taxi_error_arrival_date),parentActivity)
                &&
                GeneralFunction.checkAndReturnString(arrivalTime,parentActivity.getString(R.string.e_hailing_or_taxi_error_arrival_time),parentActivity)
                &&
                GeneralFunction.checkAndReturnString(pickUpLocationTV,parentActivity.getString(R.string.e_hailing_or_taxi_error_pick_up_location),parentActivity);
    }

    private boolean isValidGeopoint(GeoPoint geoPoint){

        if(geoPoint == null){
            String errorMsg = parentActivity.getResources().getString(R.string.e_hailing_or_taxi_error_geopoint);
            GeneralFunction.openMessageNotificationDialog(parentActivity,errorMsg,R.drawable.notice_bad,"invalid_geopoint");
            return false;
        }

        return true;
    }
}
