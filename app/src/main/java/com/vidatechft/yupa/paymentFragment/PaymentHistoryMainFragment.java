package com.vidatechft.yupa.paymentFragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.PaymentHistoryRecyclerGridAdapter;
import com.vidatechft.yupa.classes.Accommodation;
import com.vidatechft.yupa.classes.Payment;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class PaymentHistoryMainFragment extends Fragment{
    public static final String TAG = PaymentHistoryMainFragment.class.getName();
    private MainActivity parentActivity;

    private RecyclerView paymentHistoryRV;
    private TextView noTV;

    private ListenerRegistration listener;

    private HashMap<String,Room> roomMap = new HashMap<>();
    private ArrayList<Payment> paymentList = new ArrayList<>();
    private PaymentHistoryRecyclerGridAdapter adapter;

    private String loadingTxt,emptyTxt,errorTxt;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static PaymentHistoryMainFragment newInstance(MainActivity parentActivity) {
        PaymentHistoryMainFragment fragment = new PaymentHistoryMainFragment();

        fragment.parentActivity = parentActivity;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_payment_history_main, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.showBtmNav();

                loadingTxt = parentActivity.getString(R.string.payment_history_loading);
                emptyTxt = parentActivity.getString(R.string.payment_history_empty);
                errorTxt = parentActivity.getString(R.string.payment_history_error);

                paymentHistoryRV = view.findViewById(R.id.paymentHistoryRV);
                noTV = view.findViewById(R.id.noTV);
            }
        });

        return view;
    }

    private void getPaymentHistory(){
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                noTV.setText(loadingTxt);
                noTV.setVisibility(View.VISIBLE);
            }
        });

        setUpGridAdapter();

        listener = GeneralFunction.getFirestoreInstance()
                .collection(Payment.PAYMENT_RESULT_URL)
                .whereEqualTo(Payment.USER_ID,parentActivity.uid)
                .orderBy(Payment.DATE_UPDATED, Query.Direction.DESCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot snapshots, @javax.annotation.Nullable final FirebaseFirestoreException e) {
                        if (e != null) {
                            parentActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    e.printStackTrace();
                                    noTV.setVisibility(View.VISIBLE);
                                    noTV.setText(errorTxt);
                                }
                            });
                            return;
                        }

                        paymentList.clear();
                        if (snapshots != null && !snapshots.isEmpty()) {
                            for (QueryDocumentSnapshot snapshot : snapshots) {
                                Payment tempPayment = Payment.getSnapshotToPayment(parentActivity,snapshot);
                                if(tempPayment != null){
                                    paymentList.add(tempPayment);
                                }
                            }
                        }

                        onAllDownloadFinish();
                    }
                });
    }

    private void onAllDownloadFinish(){
        boolean hasFinishedDownload = true;
        for(Payment tempPayment : paymentList){
            if(tempPayment.bookingHistory != null){
                if(tempPayment.bookingHistory.targetId != null && tempPayment.bookingHistory.room == null){
                    if(roomMap.get(tempPayment.bookingHistory.targetId) != null){
                        tempPayment.bookingHistory.room = roomMap.get(tempPayment.bookingHistory.targetId);
                    }else{
                        hasFinishedDownload = false;
                        getRoom(tempPayment.bookingHistory.targetId);
                    }
                }
            }
        }

        if(hasFinishedDownload){
            setUpGridAdapter();
            if(paymentList.isEmpty()){
                noTV.setVisibility(View.VISIBLE);
                noTV.setText(emptyTxt);
            }else{
                noTV.setVisibility(View.GONE);
            }
        }
    }

    private void setUpGridAdapter(){
        if(adapter == null){
            adapter = new PaymentHistoryRecyclerGridAdapter(parentActivity,this, paymentList);
        }

        paymentHistoryRV.setAdapter(adapter);
        paymentHistoryRV.setLayoutManager(new GridLayoutManager(parentActivity, 2));
        adapter.notifyDataSetChanged();
    }

    private void getRoom(final String roomId){
        GeneralFunction.getFirestoreInstance()
                .collection(Accommodation.URL_ACCOMMODATION)
                .document(roomId)
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if(documentSnapshot != null && documentSnapshot.exists()){
                            Room tempRoom = Room.snapshotToRoom(parentActivity,documentSnapshot);
                            if(tempRoom != null){
                                roomMap.put(roomId,tempRoom);
                                onAllDownloadFinish();
                                return;
                            }
                        }

                        Room room = new Room();
                        room.id = roomId;
                        roomMap.put(roomId,room);
                        onAllDownloadFinish();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        e.printStackTrace();
//                        GeneralFunction.openMessageNotificationDialog(parentActivity,e.getMessage(),R.drawable.notice_bad,TAG);
                        Room room = new Room();
                        room.id = roomId;
                        roomMap.put(roomId,room);
                        onAllDownloadFinish();
                    }
                });
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.nav_payment_history));
                parentActivity.selectHighlights();
            }
        });

        if(paymentList.size() <= 0){
            getPaymentHistory();
        }else{
            onAllDownloadFinish();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(listener != null){
            listener.remove();
        }
    }
}
