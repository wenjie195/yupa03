//package com.vidatechft.yupa.paymentFragment;
//
//import android.graphics.Paint;
//import android.net.Uri;
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
//import android.text.Html;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.google.android.gms.tasks.OnCompleteListener;
//import com.google.android.gms.tasks.Task;
//import com.google.firebase.firestore.DocumentSnapshot;
//import com.google.firebase.firestore.Query;
//import com.google.firebase.firestore.QuerySnapshot;
//import com.vidatechft.yupa.R;
//import com.vidatechft.yupa.activities.MainActivity;
//import com.vidatechft.yupa.classes.BookingHistory;
//import com.vidatechft.yupa.classes.CurrencyRate;
//import com.vidatechft.yupa.classes.TravelKit;
//import com.vidatechft.yupa.utilities.GeneralFunction;
//
//public class BuyTravelKitFragmentOld extends Fragment implements View.OnClickListener {
//    public static final String TAG = BuyTravelKitFragmentOld.class.getName();
//    private MainActivity parentActivity;
//    private TravelKit travelKit;
//    private BookingHistory bookingHistory;
//
//    private TextView questionTV,originalPriceTV,discountedPriceTV,perSetTV,descTV;
//    private ImageView travelKitIV;
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//    }
//
//    public static BuyTravelKitFragmentOld newInstance(MainActivity parentActivity, TravelKit travelKit, BookingHistory bookingHistory) {
//        BuyTravelKitFragmentOld fragment = new BuyTravelKitFragmentOld();
//
//        fragment.parentActivity = parentActivity;
//        fragment.travelKit = travelKit;
//        fragment.bookingHistory = bookingHistory;
//
//        return fragment;
//    }
//
//    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
//        final View view = inflater.inflate(R.layout.fragment_buy_travel_kit, container, false);
//
//        parentActivity.runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                parentActivity.hideBtmNav();
//                if(travelKit != null){
//                    populateTravelKitDetails(view);
//                }else{
//                    parentActivity.initializeLoadingDialog(parentActivity.getString(R.string.booking_travel_kit_loading),false);
//                    parentActivity.controlLoadingDialog(true,parentActivity);
//                    getPrivateKitDetails(view);
//                }
//            }
//        });
//
//        return view;
//    }
//
//    private void populateTravelKitDetails(final View view){
//        parentActivity.runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                if(travelKit == null){
//                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_occured),R.drawable.notice_bad,TAG);
//                    parentActivity.onBackPressed();
//                    return;
//                }
//
//                questionTV = view.findViewById(R.id.questionTV);
//                originalPriceTV = view.findViewById(R.id.originalPriceTV);
//                discountedPriceTV = view.findViewById(R.id.discountedPriceTV);
//                descTV = view.findViewById(R.id.descTV);
//                travelKitIV = view.findViewById(R.id.travelKitIV);
//                perSetTV = view.findViewById(R.id.perSetTV);
//                TextView yesTV = view.findViewById(R.id.yesTV);
//                TextView noTV = view.findViewById(R.id.noTV);
//
//                String question = "";
//                String desc = "";
//                String originalPriceStr = "";
//                String discountedPriceStr = "";
//
//                if(travelKit.name != null){
//                    if(travelKit.discount != null && travelKit.discount > 0){
//                        question = String.format(parentActivity.getString(R.string.booking_travel_kit_promo_discount),travelKit.name, GeneralFunction.getDecimalFormat(2).format(travelKit.discount));
//                    }else{
//                        question = String.format(parentActivity.getString(R.string.booking_travel_kit_promo_normal),travelKit.name);
//                    }
//
//                    if(travelKit.desc != null){
//                        desc = String.format(parentActivity.getString(R.string.booking_travel_kit_desc),travelKit.name) + travelKit.desc;
//                    }else{
//                        desc = String.format(parentActivity.getString(R.string.booking_travel_kit_desc),travelKit.name) + parentActivity.getString(R.string.booking_travel_kit_no_desc);
//                    }
//                }else{
//                    if(travelKit.discount != null && travelKit.discount > 0){
//                        question = String.format(parentActivity.getString(R.string.booking_travel_kit_promo_discount),parentActivity.getString(R.string.booking_travel_kit_kit),GeneralFunction.getDecimalFormat(2).format(travelKit.discount));
//                    }else{
//                        question = String.format(parentActivity.getString(R.string.booking_travel_kit_promo_normal),parentActivity.getString(R.string.booking_travel_kit_kit));
//                    }
//
//                    if(travelKit.desc != null){
//                        desc = String.format(parentActivity.getString(R.string.booking_travel_kit_desc),parentActivity.getString(R.string.booking_travel_kit_this_kit)) + travelKit.desc;
//                    }else{
//                        desc = String.format(parentActivity.getString(R.string.booking_travel_kit_desc),parentActivity.getString(R.string.booking_travel_kit_this_kit)) + parentActivity.getString(R.string.booking_travel_kit_no_desc);
//                    }
//                }
//
//                if(travelKit.originalPrice != null && travelKit.currencyCode != null){
//                    String localCurrencyCode = GeneralFunction.getCurrencyCode(parentActivity);
//                    double originalPrice = CurrencyRate.getUserLocalPrice(parentActivity,localCurrencyCode,travelKit.currencyCode,travelKit.originalPrice);
//                    if(travelKit.discount != null && travelKit.discount > 0){
//                        originalPriceStr = localCurrencyCode + " " + GeneralFunction.getDecimalFormat(2).format(originalPrice);
//                        discountedPriceStr = localCurrencyCode + " " + GeneralFunction.getDecimalFormat(2).format(originalPrice - (originalPrice * travelKit.discount / 100));
//                    }else{
//                        discountedPriceStr = localCurrencyCode + " " +GeneralFunction.getDecimalFormat(2).format(originalPrice);
//                    }
//                }else{
//                    perSetTV.setVisibility(View.GONE);
//                    discountedPriceStr = parentActivity.getString(R.string.free);
//                }
//
//                questionTV.setText(Html.fromHtml(question));
//                originalPriceTV.setText(originalPriceStr);
//                discountedPriceTV.setText(discountedPriceStr);
//                descTV.setText(Html.fromHtml(desc));
//
//                if(!originalPriceStr.isEmpty()){
//                    originalPriceTV.setPaintFlags(originalPriceTV.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//                }
//
//                if(travelKit.coverPhotoUrl != null){
//                    GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(travelKit.coverPhotoUrl),travelKitIV);
//                }else{
//                    GeneralFunction.GlideImageSettingOwnDrawable(parentActivity,R.drawable.ic_not_found,travelKitIV);
//                }
//
//                yesTV.setOnClickListener(BuyTravelKitFragmentOld.this);
//                noTV.setOnClickListener(BuyTravelKitFragmentOld.this);
//                view.findViewById(R.id.btnLL).setVisibility(View.VISIBLE);
//                view.findViewById(R.id.contentSV).setVisibility(View.VISIBLE);
//            }
//        });
//    }
//
//    private void getPrivateKitDetails(final View view){
//        GeneralFunction.getFirestoreInstance()
//            .collection(TravelKit.URL_TRAVELKIT)
//            .whereEqualTo(TravelKit.IS_PUBLISH,true)
//            .whereEqualTo(TravelKit.STATUS,TravelKit.STATUS_APPROVED)
//            .orderBy(TravelKit.DATE_UPDATED, Query.Direction.DESCENDING)
//            .limit(1)
//            .get()
//            .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
//                @Override
//                public void onComplete(@NonNull Task<QuerySnapshot> task) {
//                    parentActivity.controlLoadingDialog(false,parentActivity);
//                    if(GeneralFunction.hasError(parentActivity,task,TAG)){
//                        return;
//                    }
//
//                    QuerySnapshot snapshots = task.getResult();
//
//                    if(snapshots != null && !snapshots.isEmpty()){
//                        for(DocumentSnapshot snapshot : snapshots){
//                            travelKit = TravelKit.getTravelKit(parentActivity,snapshot);
//                        }
//                    }
//
//                    populateTravelKitDetails(view);
//                }
//            });
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        parentActivity.runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                parentActivity.setToolbarTitle(parentActivity.getString(R.string.booking_buy_travel_kit_toolbar));
//            }
//        });
//    }
//
//    @Override
//    public void onClick(View v) {
//        switch (v.getId()){
//            case R.id.yesTV:
//                if(GeneralFunction.isGuest()){
//                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_guest_restrict_usage),R.drawable.notice_bad,TAG);
//                    return;
//                }
//                Fragment paymentSummaryFragmentYes = PaymentSummaryFragment.newInstance(parentActivity,travelKit,bookingHistory);
//                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, paymentSummaryFragmentYes).addToBackStack(PaymentSummaryFragment.TAG).commit();
//                break;
//            case R.id.noTV:
//                //if booking history is null means it comes from main activity or any other place  but didnt plan on booking accommodation, so can just go back
//                if(bookingHistory == null){
//                    parentActivity.onBackPressed();
//                }else{
//                    if(GeneralFunction.isGuest()){
//                        GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_guest_restrict_usage),R.drawable.notice_bad,TAG);
//                        return;
//                    }
//                    Fragment paymentSummaryFragmentNo = PaymentSummaryFragment.newInstance(parentActivity,null,bookingHistory);
//                    GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, paymentSummaryFragmentNo).addToBackStack(PaymentSummaryFragment.TAG).commit();
//                }
//                break;
//        }
//    }
//}
