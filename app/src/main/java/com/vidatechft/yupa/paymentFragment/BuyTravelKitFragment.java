package com.vidatechft.yupa.paymentFragment;

import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.CheckboxRecyclerGridAdapter;
import com.vidatechft.yupa.adapter.CheckboxTravelKitAdapter;
import com.vidatechft.yupa.classes.BookingHistory;
import com.vidatechft.yupa.classes.CurrencyRate;
import com.vidatechft.yupa.classes.TravelKit;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static com.vidatechft.yupa.utilities.GeneralFunction.getDecimalFormat;

public class BuyTravelKitFragment extends Fragment implements View.OnClickListener {
    public static final String TAG = BuyTravelKitFragment.class.getName();
    private MainActivity parentActivity;
    private TravelKit travelKit;
    private BookingHistory bookingHistory;
    private TextView yesTV, noTV;
    private TextView questionTV,originalPriceTV,discountedPriceTV,perSetTV,descTV;
    private ImageView travelKitIV;
    private HashMap<String,Boolean> selectedTravelKitMap;
    private RecyclerView otherKitRV;
    private CheckboxTravelKitAdapter checkboxTravelKitAdapter;
    private HashMap<String,Object> travelKitCheckBoxMap = new HashMap<>();
    private HashMap<String,Object> tempAllBuyTravelKitMapPrice = new HashMap<>();
    private HashMap<String,Object> tempCheckBoxMap = new HashMap<>();
    private TextView totalPriceTV;
    private Double defPrice = 0.0;
    private String localCurrencyCode;
    private SelectTravelKitFragment selectTravelKitFragment;
    private String thisTravelKit;
    private HashMap<String,Object> thisTravelKitCheckBoxMap = new HashMap<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static BuyTravelKitFragment newInstance(MainActivity parentActivity, TravelKit travelKit, BookingHistory bookingHistory) {
        BuyTravelKitFragment fragment = new BuyTravelKitFragment();

        fragment.parentActivity = parentActivity;
        fragment.travelKit = travelKit;
        fragment.bookingHistory = bookingHistory;

        return fragment;
    }

    public static BuyTravelKitFragment newInstance(MainActivity parentActivity, SelectTravelKitFragment selectTravelKitFragment, TravelKit travelKit, BookingHistory bookingHistory, HashMap<String,Boolean> selectedTravelKitMap, HashMap<String,Object> travelKitCheckBoxMap) {
        BuyTravelKitFragment fragment = new BuyTravelKitFragment();

        fragment.parentActivity = parentActivity;
        fragment.travelKit = travelKit;
        fragment.selectTravelKitFragment = selectTravelKitFragment;
        fragment.bookingHistory = bookingHistory;
        fragment.selectedTravelKitMap = selectedTravelKitMap;
        fragment.travelKitCheckBoxMap = travelKitCheckBoxMap;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_buy_travel_kit, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.hideBtmNav();
                if(travelKit != null){
                    populateTravelKitDetails(view);
                }else{
//                    parentActivity.initializeLoadingDialog(parentActivity.getString(R.string.booking_travel_kit_loading),false);
//                    parentActivity.controlLoadingDialog(true,parentActivity);
//                    getPrivateKitDetails(view);
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getResources().getString(R.string.booking_travel_kit_no_desc),R.drawable.notice_bad,TAG);
                    parentActivity.onBackPressed();
                }

                isSelectedTravelKit();
            }
        });

        return view;
    }

    private void populateTravelKitDetails(final View view){
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(travelKit == null){
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_occured),R.drawable.notice_bad,TAG);
                    parentActivity.onBackPressed();
                    return;
                }

                questionTV = view.findViewById(R.id.questionTV);
                originalPriceTV = view.findViewById(R.id.originalPriceTV);
                discountedPriceTV = view.findViewById(R.id.discountedPriceTV);
                descTV = view.findViewById(R.id.descTV);
                travelKitIV = view.findViewById(R.id.travelKitIV);
                perSetTV = view.findViewById(R.id.perSetTV);
                yesTV = view.findViewById(R.id.yesTV);
                noTV = view.findViewById(R.id.noTV);
                otherKitRV = view.findViewById(R.id.otherKit);
                totalPriceTV = view.findViewById(R.id.totalPriceTV);

                if(localCurrencyCode == null){
                    localCurrencyCode = GeneralFunction.getCurrencyCode(parentActivity);
                }

                if(localCurrencyCode == null){
                    localCurrencyCode = "MYR";
                }

                String s = localCurrencyCode + "0.00";
                totalPriceTV.setText(s);

                String question = "";
                String desc = "";
                String originalPriceStr = "";
                String discountedPriceStr = "";
//                getAllOtherKit();
                if(travelKit.name != null){
                    thisTravelKit = travelKit.name;
                    if(travelKit.discount != null && travelKit.discount > 0){
                        question = String.format(parentActivity.getString(R.string.booking_travel_kit_promo_discount),travelKit.name, getDecimalFormat(2).format(travelKit.discount));
                    }else{
                        question = String.format(parentActivity.getString(R.string.booking_travel_kit_promo_normal),travelKit.name);
                    }

//                    if(travelKit.desc != null){
//                        desc = String.format(parentActivity.getString(R.string.booking_travel_kit_desc),travelKit.name) + travelKit.desc;
//                    }else{
//                        desc = String.format(parentActivity.getString(R.string.booking_travel_kit_desc),travelKit.name) + parentActivity.getString(R.string.booking_travel_kit_no_desc);
//                    }
                }else{
                    if(travelKit.discount != null && travelKit.discount > 0){
                        question = String.format(parentActivity.getString(R.string.booking_travel_kit_promo_discount),parentActivity.getString(R.string.booking_travel_kit_kit),getDecimalFormat(2).format(travelKit.discount));
                    }else{
                        question = String.format(parentActivity.getString(R.string.booking_travel_kit_promo_normal),parentActivity.getString(R.string.booking_travel_kit_kit));
                    }

//                    if(travelKit.desc != null){
//                        desc = String.format(parentActivity.getString(R.string.booking_travel_kit_desc),parentActivity.getString(R.string.booking_travel_kit_this_kit)) + travelKit.desc;
//                    }else{
//                        desc = String.format(parentActivity.getString(R.string.booking_travel_kit_desc),parentActivity.getString(R.string.booking_travel_kit_this_kit)) + parentActivity.getString(R.string.booking_travel_kit_no_desc);
//                    }
                }

                if(travelKit.originalPrice != null && travelKit.currencyCode != null){
                    String localCurrencyCode = GeneralFunction.getCurrencyCode(parentActivity);
                    double originalPrice = CurrencyRate.getUserLocalPrice(parentActivity,localCurrencyCode,travelKit.currencyCode,travelKit.originalPrice);
                    if(travelKit.originalPrice == 0){
                        discountedPriceStr = parentActivity.getString(R.string.free);
                    }
                    else{
                        if(travelKit.discount != null && travelKit.discount > 0){
                            originalPriceStr = localCurrencyCode + " " + getDecimalFormat(2).format(originalPrice);
                            discountedPriceStr = localCurrencyCode + " " + getDecimalFormat(2).format(originalPrice - (originalPrice * travelKit.discount / 100));
                        }else{
                            discountedPriceStr = localCurrencyCode + " " +getDecimalFormat(2).format(originalPrice);
                        }
                    }
                }else{
                    perSetTV.setVisibility(View.GONE);
                    discountedPriceStr = parentActivity.getString(R.string.free);
                }

                questionTV.setText(Html.fromHtml(question));
                originalPriceTV.setText(originalPriceStr);
                discountedPriceTV.setText(discountedPriceStr);
                descTV.setText(Html.fromHtml(desc));

                if(!originalPriceStr.isEmpty()){
                    originalPriceTV.setPaintFlags(originalPriceTV.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                }

                if(travelKit.coverPhotoUrl != null){
                    GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(travelKit.coverPhotoUrl),travelKitIV);
                }else{
                    GeneralFunction.GlideImageSettingOwnDrawable(parentActivity,R.drawable.ic_not_found,travelKitIV);
                }

                if(travelKit.productList != null){
                    final ArrayList<CheckBox> allTravelKitCheckboxList = new ArrayList<>();
                    final ArrayList<Double> allTravelKitPriceList = new ArrayList<>();
                    int count = 1;
                    for (Map.Entry<String, Double> entry : travelKit.productList.entrySet())
                    {
                        String key = entry.getKey();
                        Object value = entry.getValue();
                        String free = "Free";
                        if(value.toString().equals("0")){
                            desc += count +". "+ key + "<br>";
                        }
                        else{
                            desc += count +". "+ key + "<br>";
                        }

                        count++;
                    }
                    count = 1;
//                    descTV.setText(Html.fromHtml(String.format(parentActivity.getString(R.string.booking_travel_kit_desc),parentActivity.getString(R.string.booking_travel_kit_this_kit))+"<br>"+ desc));
                    String tempCurrencyCode = "MYR";
                    if(localCurrencyCode != null){
                        tempCurrencyCode = localCurrencyCode;
                    }
                    descTV.setText(Html.fromHtml(String.format(parentActivity.getString(R.string.booking_travel_kit_desc),parentActivity.getString(R.string.booking_travel_kit_this_kit),tempCurrencyCode)));
//                    descTV.setText(parentActivity.getString(R.string.booking_travel_kit_desc));
                    if(travelKit.productOrder != null && !travelKit.productOrder.isEmpty()){
                        for(int i = 0; i < travelKit.productOrder.size(); i++){
                            String productName = travelKit.productOrder.get(i);
                            CheckBox checkBox = (CheckBox) CheckBox.inflate(parentActivity,R.layout.custom_checkbox,null);
                            checkBox.setText(productName);
                            allTravelKitCheckboxList.add(checkBox);
                            allTravelKitPriceList.add(travelKit.productList.get(productName));
                        }
                    }else{
                        for (Map.Entry<String, Double> entry : travelKit.productList.entrySet()) {
                            String key = entry.getKey();
                            Double value = entry.getValue();
                            CheckBox checkBox = (CheckBox) CheckBox.inflate(parentActivity,R.layout.custom_checkbox,null);
                            checkBox.setText(key.trim());
                            allTravelKitCheckboxList.add(checkBox);
                            allTravelKitPriceList.add(value);
                        }
                    }

                    checkboxTravelKitAdapter = new CheckboxTravelKitAdapter(parentActivity,allTravelKitCheckboxList, allTravelKitPriceList, travelKit, BuyTravelKitFragment.this,localCurrencyCode);
                    otherKitRV.setNestedScrollingEnabled(false);
                    otherKitRV.setAdapter(checkboxTravelKitAdapter);
                    otherKitRV.setLayoutManager(new StaggeredGridLayoutManager(1,StaggeredGridLayoutManager.VERTICAL));


                }

                if(selectedTravelKitMap != null){
                    for (Map.Entry<String, Boolean> map: selectedTravelKitMap.entrySet()){
                        if(travelKit != null){
                            if(travelKit.name.contains(map.getKey()) && map.getValue().equals(true)){
                                thisTravelKit = travelKit.name;
                            }
                        }
                    }
                }


                if(selectTravelKitFragment.allBuyTravelKitMap != null && selectTravelKitFragment.allBuyTravelKitMap.size() != 0){
                    for (Map.Entry<String, HashMap<String, Object>> map: selectTravelKitFragment.allBuyTravelKitMap.entrySet()){
                        if(thisTravelKit != null){
                            if(map.getKey().equals(thisTravelKit)){
                                for(Map.Entry<String, Object> checkBox : map.getValue().entrySet()){
                                    String checkboxKey = checkBox.getKey();
                                    Object checkboxValue = checkBox.getValue();
                                    thisTravelKitCheckBoxMap.put(checkboxKey,checkboxValue);
                                }
                            }
                        }
                    }
                }

                if(checkboxTravelKitAdapter.getCheckBoxes() != null){

                    for(CheckBox addOn:checkboxTravelKitAdapter.getCheckBoxes())
                    {
                        if(thisTravelKitCheckBoxMap != null){
                            if(thisTravelKitCheckBoxMap.containsKey(addOn.getText().toString())){

                            }
//                            for (Map.Entry<String, Object> map: thisTravelKitCheckBoxMap.entrySet()){
//                                if(map.getKey().contains(addOn.getText().toString()) && map.getValue().equals(true)){
//                                    addOn.setChecked(true);
//                                }
//                            }
                        }
                    }
                }


                if(travelKit.productRemark != null){

                }

                yesTV.setOnClickListener(BuyTravelKitFragment.this);
                noTV.setOnClickListener(BuyTravelKitFragment.this);
                view.findViewById(R.id.btnLL).setVisibility(View.VISIBLE);
                view.findViewById(R.id.contentSV).setVisibility(View.VISIBLE);

                populateCheckBoxDetail(checkboxTravelKitAdapter);
            }
        });
    }

    private void getPrivateKitDetails(final View view){
        final ArrayList<CheckBox> allTravelKitCheckBoxList = new ArrayList<>();
        final ArrayList<Double> allTravelKitPriceList = new ArrayList<>();
        final ArrayList<TravelKit> allTravelKitList = new ArrayList<>();
        GeneralFunction.getFirestoreInstance()
            .collection(TravelKit.URL_TRAVELKIT)
            .whereEqualTo(TravelKit.IS_PUBLISH,true)
            .whereEqualTo(TravelKit.STATUS,TravelKit.STATUS_APPROVED)
            .orderBy(TravelKit.DATE_UPDATED, Query.Direction.DESCENDING)
            .limit(1)
            .get()
            .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    parentActivity.controlLoadingDialog(false,parentActivity);
                    if(GeneralFunction.hasError(parentActivity,task,TAG)){
                        return;
                    }

                    QuerySnapshot snapshots = task.getResult();

                    if(snapshots != null){
                        allTravelKitCheckBoxList.clear();
                        allTravelKitPriceList.clear();
                        allTravelKitList.clear();
                        if(!snapshots.isEmpty()){
                            for(DocumentSnapshot snapshot : snapshots){
                                travelKit = TravelKit.getTravelKit(parentActivity,snapshot);
                                allTravelKitList.add(travelKit);
                                for (Map.Entry<String, Double> entry : travelKit.productList.entrySet())
                                {
                                    String key = entry.getKey();
                                    Object value = entry.getValue();
                                    CheckBox checkBox = (CheckBox) CheckBox.inflate(parentActivity,R.layout.custom_checkbox,null);
                                    checkBox.setText(key.trim());
                                    allTravelKitCheckBoxList.add(checkBox);
                                    allTravelKitPriceList.add((Double) value);
                                }
                            }
                        }

                        checkboxTravelKitAdapter = new CheckboxTravelKitAdapter(parentActivity,allTravelKitCheckBoxList,allTravelKitPriceList,allTravelKitList,BuyTravelKitFragment.this,localCurrencyCode);
                        otherKitRV.setNestedScrollingEnabled(false);
                        otherKitRV.setAdapter(checkboxTravelKitAdapter);
                        otherKitRV.setLayoutManager(new StaggeredGridLayoutManager(1,StaggeredGridLayoutManager.VERTICAL));
                        populateCheckBoxDetail(checkboxTravelKitAdapter);
                    }

                    populateTravelKitDetails(view);
                }
            });
    }

    @Override
    public void onPause() {
        super.onPause();
        parentActivity.showBtmNav();
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.hideBtmNav();
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.booking_buy_travel_kit_toolbar));
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.yesTV:
                if(GeneralFunction.isGuest()){
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_guest_restrict_usage),R.drawable.notice_bad,TAG);
                    return;
                }
//                Fragment paymentSummaryFragmentYes = PaymentSummaryFragment.newInstance(parentActivity,travelKit,bookingHistory);
//                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, paymentSummaryFragmentYes).addToBackStack(PaymentSummaryFragment.TAG).commit();
                storeTravelKitDetail();

                break;
            case R.id.noTV:
                //if booking history is null means it comes from main activity or any other place  but didnt plan on booking accommodation, so can just go back
                if(bookingHistory == null){
                    parentActivity.onBackPressed();
                }else{
                    if(GeneralFunction.isGuest()){
                        GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_guest_restrict_usage),R.drawable.notice_bad,TAG);
                        return;
                    }
//                    Fragment paymentSummaryFragmentNo = PaymentSummaryFragment.newInstance(parentActivity,bookingHistory,null,null,null);
//                    GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, paymentSummaryFragmentNo).addToBackStack(PaymentSummaryFragment.TAG).commit();
                }
                break;
        }
    }

    private void storeTravelKitDetail(){
        if(yesTV.getText().equals(parentActivity.getResources().getString(R.string.addToCart))){
            yesTV.setText(parentActivity.getResources().getString(R.string.removeFromCart));
        }
        else{
            yesTV.setText(parentActivity.getResources().getString(R.string.addToCart));
        }
        travelKitCheckBoxMap.clear();
        for(CheckBox travelKitCB : checkboxTravelKitAdapter.getCheckBoxes())
        {
            if(travelKitCB != null && travelKitCB.isChecked()){
                travelKitCheckBoxMap.put(travelKitCB.getText().toString().trim(),true);
            }
        }
        if(selectedTravelKitMap != null){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                if(!Objects.equals(selectedTravelKitMap.get(travelKit.name), true)){
                    selectedTravelKitMap.put(travelKit.name, true);
                }
                else{
                    selectedTravelKitMap.put(travelKit.name, false);
                }
            }
            else{
                try{
                    if(selectedTravelKitMap.get(travelKit.name) != null && selectedTravelKitMap.get(travelKit.name)){
                        selectedTravelKitMap.put(travelKit.name, false);
                    }
                    else{
                        selectedTravelKitMap.put(travelKit.name, true);
                    }
                }
                catch(Exception e){
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_occured),R.drawable.notice_bad,TAG);
                }
            }
        }

        if(travelKit.productList != null){
            for (Map.Entry<String, Double> entry : travelKit.productList.entrySet())
            {
                String key = entry.getKey();
                Double value = entry.getValue();

                if(travelKitCheckBoxMap != null){
                    for(Map.Entry<String, Object> checkBoxEntry : travelKitCheckBoxMap.entrySet()){
                        if(key.equals(checkBoxEntry.getKey()) && checkBoxEntry.getValue().equals(true)){
                            String convertedPrice = getDecimalFormat(2).format(CurrencyRate.getUserLocalPrice(parentActivity,GeneralFunction.getCurrencyCode(parentActivity),travelKit.currencyCode,value));
                            tempAllBuyTravelKitMapPrice.put(key,Double.parseDouble(String.valueOf(convertedPrice)));
                            tempCheckBoxMap.put(key,checkBoxEntry.getValue());
                        }
                    }
                }
            }
        }

        selectTravelKitFragment.allBuyTravelKitMap.put(travelKit.name, tempCheckBoxMap);
        selectTravelKitFragment.allBuyTravelKitMapPrice.put(travelKit.name,tempAllBuyTravelKitMapPrice);
        SelectTravelKitFragment.newInstance(parentActivity, selectedTravelKitMap,travelKitCheckBoxMap);
//        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, selectTravelKitFragment).addToBackStack(SelectTravelKitFragment.TAG).commit();

        if(!yesTV.getText().equals(parentActivity.getResources().getString(R.string.addToCart))){
            parentActivity.onBackPressed();
        }
    }

    private void isSelectedTravelKit(){
        if(selectedTravelKitMap != null){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                if(!Objects.equals(selectedTravelKitMap.get(travelKit.name), true)){
                    yesTV.setText(parentActivity.getResources().getString(R.string.addToCart));
                }
                else{
                    yesTV.setText(parentActivity.getResources().getString(R.string.removeFromCart));
                }
            }
            else{
                try{
                    if(selectedTravelKitMap.get(travelKit.name) != null && selectedTravelKitMap.get(travelKit.name)){
                        yesTV.setText(parentActivity.getResources().getString(R.string.removeFromCart));
                    }else{
                        yesTV.setText(parentActivity.getResources().getString(R.string.addToCart));
                    }
                }catch (Exception e){
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_occured),R.drawable.notice_bad,TAG);
                    yesTV.setText(parentActivity.getResources().getString(R.string.addToCart));
                }
            }
        }
    }

    private void populateCheckBoxDetail(CheckboxTravelKitAdapter checkboxTravelKitAdapter){
        if(thisTravelKitCheckBoxMap != null){
            if(checkboxTravelKitAdapter.getCheckBoxes() != null){

                for(CheckBox addOn:checkboxTravelKitAdapter.getCheckBoxes())
                {
                    if(thisTravelKitCheckBoxMap.containsKey(addOn.getText().toString()))
                    {
                        addOn.setChecked(true);
                    }
                }
            }
        }
    }

    public void setTravelKitTotalPrice(Double totalPrice, boolean isAdd){

        if(isAdd){
            defPrice = defPrice + totalPrice;
        }
        else{
            defPrice = defPrice - totalPrice;
        }
        String mTotalPrice = localCurrencyCode + " " + getDecimalFormat(2).format(defPrice);

        totalPriceTV.setText(mTotalPrice);
    }

    public void setQuantityMap(HashMap<String,HashMap<String,Long>> quantityMap){
        selectTravelKitFragment.setQuantityMap(quantityMap);
    }

    public HashMap<String,HashMap<String,Long>> getQuantityMap(){
        return selectTravelKitFragment.getQuantityMap();
    }
}
