package com.vidatechft.yupa.paymentFragment;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.Group;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TimePicker;

import com.braintreepayments.api.BraintreeFragment;
import com.braintreepayments.api.PayPal;
import com.braintreepayments.api.dropin.DropInActivity;
import com.braintreepayments.api.dropin.DropInRequest;
import com.braintreepayments.api.dropin.DropInResult;
import com.braintreepayments.api.exceptions.InvalidArgumentException;
import com.braintreepayments.api.interfaces.BraintreeCancelListener;
import com.braintreepayments.api.interfaces.BraintreeErrorListener;
import com.braintreepayments.api.interfaces.PaymentMethodNonceCreatedListener;
import com.braintreepayments.api.models.PayPalAccountNonce;
import com.braintreepayments.api.models.PayPalRequest;
import com.braintreepayments.api.models.PaymentMethodNonce;
import com.braintreepayments.api.models.PostalAddress;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QuerySnapshot;
import com.tsongkha.spinnerdatepicker.DatePicker;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.BookingHistory;
import com.vidatechft.yupa.classes.CurrencyRate;
import com.vidatechft.yupa.classes.Itinerary;
import com.vidatechft.yupa.classes.Payment;
import com.vidatechft.yupa.classes.PaymentAmount;
import com.vidatechft.yupa.classes.Plan;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.classes.Search;
import com.vidatechft.yupa.classes.TravelKit;
import com.vidatechft.yupa.itinerary.AddNewPlanFragment;
import com.vidatechft.yupa.utilities.Config;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TimeZone;

//todo maybe when he clicks buy now in paypal api, i update the status thing to pending, and to cancel if until onDestroy he still havent clicked on the buy now button
public class PaymentSummaryFragment extends Fragment implements View.OnClickListener, com.tsongkha.spinnerdatepicker.DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {
    public static final String TAG = PaymentSummaryFragment.class.getName();
    private MainActivity parentActivity;
    private BookingHistory bookingHistory;
    private Itinerary itinerary;
    private ArrayList<TravelKit> selectedTKs;
    private Room room;
    private Search search;

    private TextView accNameTV,checkinTV,checkoutTV,rateTV,roomTypeTV;
    private TextView noofNightTV,totalNightCurrencyTV,totalNightPriceTV;
    private TextView itineraryTitleTV,itineraryCurrencyTV,itineraryPriceTV;
    private TextView taxCurrencyTV,taxPriceTV;
    private TextView totalCurrencyTV,totalPriceTV;
    private TextView stayCostTV;
    private TableRow nightTR,itineraryTR;
    private TableLayout priceTL;
    private Group groupAcc;

    //pickup views
    private ConstraintLayout pickupCL;
    private TextView pickupDateTV,pickupTimeTV;
    private EditText pickupLocationET;
    private RadioButton pickupAirportRB,pickupHotelRB;
    private int pickupDay, pickupMonth, pickupYear, pickupHour, pickupMin;

    private String localCurrencyCode;
    private DocumentReference itiRef;
    private boolean needDeleteCreatedItinerary = false;
    private boolean hasAlreadyCreatedItinerary = false;
    private boolean hasGoneThroughPaymentProcess = false;

    private double[] allPrice = {0,0,0,0,0,0,0};
    private HashMap<String,Double> allTravelKitPrice = new HashMap<>();
    /*
            0 = rate per night ***CURRENCY CONVERSION WILL OCCUR HERE***
            1 = total night
            2 = total accommodation night * the rate per night
            3 = itineraries price
            4 = travel kit price ***CURRENCY CONVERSION WILL OCCUR HERE***
            5 = service tax of all items combined
            6 = final total price
        */

    //*********PAYMENT GATEWAY VARIABLES**************/
    //BRAINTREE DROP IN VARIABLES
    private ListenerRegistration clientTokenListener,paymentResultListener;
    private String clientToken = null;
    private Double amount = 0.0;

    //DIRECT PAYPAL VARIABLES
    BraintreeFragment mBraintreeFragment;
    private PaymentMethodNonceCreatedListener nonceCreatedListener;
    private BraintreeCancelListener braintreeCancelListener;
    private BraintreeErrorListener braintreeErrorListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static PaymentSummaryFragment newInstance(MainActivity parentActivity, BookingHistory bookingHistory, Itinerary itinerary, ArrayList<TravelKit> selectedTKs) {
        PaymentSummaryFragment fragment = new PaymentSummaryFragment();

        fragment.parentActivity = parentActivity;
        fragment.selectedTKs = selectedTKs;
        fragment.bookingHistory = bookingHistory;
        if(itinerary != null){
            //this is to clone a new object because if i changed the variable or itinerary like below, the original 1 will also changed because of pointer
            //Java is always pass-by-value. Unfortunately, when we pass the value of an object, we are passing the reference to it
            fragment.itinerary = new Itinerary(itinerary);
        }
        if(bookingHistory != null){
            fragment.room = bookingHistory.room;
        }

        return fragment;
    }

    public static PaymentSummaryFragment newInstance(MainActivity parentActivity, BookingHistory bookingHistory, Itinerary itinerary, ArrayList<TravelKit> selectedTKs, Search search) {
        PaymentSummaryFragment fragment = new PaymentSummaryFragment();

        fragment.parentActivity = parentActivity;
        fragment.selectedTKs = selectedTKs;
        fragment.bookingHistory = bookingHistory;
        if(itinerary != null){
            //this is to clone a new object because if i changed the variable or itinerary like below, the original 1 will also changed because of pointer
            //Java is always pass-by-value. Unfortunately, when we pass the value of an object, we are passing the reference to it
            fragment.itinerary = new Itinerary(itinerary);
            fragment.itinerary.departDate = search.checkin_date;
            fragment.itinerary.returnDate = search.checkout_date;
        }
        if(bookingHistory != null){
            fragment.room = bookingHistory.room;
        }
        fragment.search = search;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_payment_summary, container, false);

        getPaymentClientToken();

        if(localCurrencyCode == null){
            localCurrencyCode = GeneralFunction.getCurrencyCode(parentActivity);
        }

        //full backend transaction example is from here https://developers.braintreepayments.com/reference/request/transaction/sale/node
        //this codes supports USD and MYR with default MYR
//        if(TextUtils.equals(localCurrencyCode,"USD") || TextUtils.equals(localCurrencyCode,"MYR")){
//
//        }else{
//            localCurrencyCode = "MYR";
//        }

        //now make it only able to pay in MYR, the backend still support both USD and MYR but the app doesnt give any chance to pay in USD
        //to support USD too uncomment the codes above else just force it to MYR with code below
        localCurrencyCode = "MYR";

        if(itinerary != null && itiRef == null){
            itiRef = GeneralFunction.getFirestoreInstance()
                        .collection(Itinerary.URL_ITINERARY)
                        .document();
        }

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.hideBtmNav();
                if(bookingHistory == null && selectedTKs == null){
                    promptErrorAndGoBack(null);
                    return;
                }

                accNameTV = view.findViewById(R.id.accNameTV);
                checkinTV = view.findViewById(R.id.checkinTV);
                checkoutTV = view.findViewById(R.id.checkoutTV);
                rateTV = view.findViewById(R.id.rateTV);
                roomTypeTV = view.findViewById(R.id.roomTypeTV);
                noofNightTV = view.findViewById(R.id.noOfDayTV);
                totalNightCurrencyTV = view.findViewById(R.id.totalNightCurrencyTV);
                totalNightPriceTV = view.findViewById(R.id.totalNightPriceTV);
                itineraryTitleTV = view.findViewById(R.id.itineraryTitleTV);
                itineraryCurrencyTV = view.findViewById(R.id.itineraryCurrencyTV);
                itineraryPriceTV = view.findViewById(R.id.itineraryPriceTV);
                taxCurrencyTV = view.findViewById(R.id.taxCurrencyTV);
                taxPriceTV = view.findViewById(R.id.taxPriceTV);
                totalCurrencyTV = view.findViewById(R.id.totalCurrencyTV);
                totalPriceTV = view.findViewById(R.id.totalPriceTV);
                stayCostTV = view.findViewById(R.id.stayCostTV);
                nightTR = view.findViewById(R.id.nightTR);
                itineraryTR = view.findViewById(R.id.itineraryTR);
                priceTL = view.findViewById(R.id.priceTL);
                groupAcc = view.findViewById(R.id.groupAcc);

                calculateAndPopulateAllTravelKitsAndAddons();
                calculateAllPrice();
                populateDetails();

                TextView confirmTV = view.findViewById(R.id.confirmTV);
                confirmTV.setOnClickListener(PaymentSummaryFragment.this);
            }
        });

        setupPickupDetails(view);

        return view;
    }

    private void setupPickupDetails(View view){
        pickupCL = view.findViewById(R.id.pickupCL);
        pickupDateTV = view.findViewById(R.id.pickupDateTV);
        pickupTimeTV = view.findViewById(R.id.pickupTimeTV);
        pickupLocationET = view.findViewById(R.id.pickupLocationET);
        pickupAirportRB = view.findViewById(R.id.pickupAirportRB);
        pickupHotelRB = view.findViewById(R.id.pickupHotelRB);

        pickupDateTV.setOnClickListener(this);
        pickupTimeTV.setOnClickListener(this);
        if(itinerary != null || (selectedTKs != null && !selectedTKs.isEmpty())){
            pickupCL.setVisibility(View.VISIBLE);
        }else{
            pickupCL.setVisibility(View.GONE);
        }
    }

    private void getPaymentClientToken(){
        parentActivity.initializeLoadingDialog(parentActivity.getString(R.string.please_wait),false);
        parentActivity.controlLoadingDialog(true,parentActivity);

        GeneralFunction.getFirestoreInstance()
                .collection(Payment.TRIGGER_CLIENT_TOKEN_URL)
                .document(parentActivity.uid)
                .delete()
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        //FAILED: ERROR GETTING CLIENT TOKEN
                        parentActivity.controlLoadingDialog(false,parentActivity);
                        parentActivity.onBackPressed();
                        GeneralFunction.openMessageNotificationDialog(parentActivity,e.getMessage(),R.drawable.notice_bad,TAG);
                    }
                }).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        HashMap<String,Object> triggerMap = new HashMap<>();
                        triggerMap.put("trigger",true);
                        triggerMap.put("dateCreated",FieldValue.serverTimestamp());

                        GeneralFunction.getFirestoreInstance()
                                .collection(Payment.TRIGGER_CLIENT_TOKEN_URL)
                                .document(parentActivity.uid)
                                .set(triggerMap)
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        //FAILED: ERROR GETTING CLIENT TOKEN
                                        parentActivity.controlLoadingDialog(false,parentActivity);
                                        parentActivity.onBackPressed();
                                        GeneralFunction.openMessageNotificationDialog(parentActivity,e.getMessage(),R.drawable.notice_bad,TAG);
                                    }
                                }).addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        getClientToken();
                                    }
                                });
                    }
                });
    }

    private void getClientToken(){
        clientTokenListener = GeneralFunction.getFirestoreInstance()
                .collection(Payment.GET_CLIENT_TOKEN_URL)
                .document(parentActivity.uid)
                .addSnapshotListener(new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable DocumentSnapshot documentSnapshot, @javax.annotation.Nullable FirebaseFirestoreException e) {
                        if(documentSnapshot != null && documentSnapshot.exists()){
                            Payment payment = Payment.getSnapshotToPayment(parentActivity,documentSnapshot);

                            if(payment != null){
                                new Handler().postDelayed(new Runnable(){
                                    public void run(){
                                        parentActivity.controlLoadingDialog(false,parentActivity);
                                    }
                                }, 2000);

                                GeneralFunction.getFirestoreInstance()
                                        .collection(Payment.GET_CLIENT_TOKEN_URL)
                                        .document(parentActivity.uid)
                                        .delete();

                                if(payment.errorMsg != null){
                                    //FAILED: ERROR GETTING CLIENT TOKEN WITH ERROR MESSAGE
                                    parentActivity.onBackPressed();
                                    GeneralFunction.openMessageNotificationDialog(parentActivity,payment.errorMsg,R.drawable.notice_bad,TAG);
                                }else{
                                    if(payment.clientToken != null){
                                        clientToken = payment.clientToken;
                                    }else{
                                        //FAILED: ERROR GETTING CLIENT TOKEN
                                        parentActivity.onBackPressed();
                                        GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.payment_client_token_failed),R.drawable.notice_bad,TAG);
                                    }
                                }
                            }
                        }
                    }
                });
    }

    private void continuePaymentByDeletingPreviousFirstIfExists(final DropInResult result){
        parentActivity.initializeLoadingDialog(parentActivity.getString(R.string.processing_payment),false);
        parentActivity.controlLoadingDialog(true,parentActivity);

        GeneralFunction.getFirestoreInstance()
                .collection(Payment.CONTINUE_PAYMENT_URL)
                .document(parentActivity.uid)
                .delete()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        //have some delay after delete before setting the payment continue
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Payment payment = new Payment();
                                payment.describeContents = result.describeContents();
                                payment.deviceData = result.getDeviceData();
                                payment.paymentMethodNonce = result.getPaymentMethodNonce();
                                payment.paymentMethodType = result.getPaymentMethodType();
                                continueToPayment(payment);
                            }
                        },1);//was 500, now no need because will put function for both update and create in cloud functions
                    }
                });
    }

    private void continueToPayment(Payment payment){
        hasGoneThroughPaymentProcess = true;

        final Calendar localTimestamp = new GregorianCalendar();
        payment.clientToken = clientToken;
        payment.amount = amount;
        payment.localTimestamp = localTimestamp.getTimeInMillis();

        if(search != null){
            payment.departDate = search.checkin_date;
            payment.returnDate = search.checkout_date;
        }

        if(bookingHistory != null){
            payment.bookingHistory = bookingHistory;
            payment.departDate = bookingHistory.checkinDate;
            payment.returnDate = bookingHistory.checkoutDate;
        }
        if(itinerary != null){
            payment.itinerary = itinerary;
        }
        if(selectedTKs != null){
            payment.travelKits = selectedTKs;
        }

        if(itinerary != null || (selectedTKs != null && !selectedTKs.isEmpty())){
            Calendar tempCal = GeneralFunction.getDefaultUtcCalendar();
            tempCal.set(pickupYear,pickupMonth - 1,pickupDay,pickupHour,pickupMin);

            payment.pickupLocationName = pickupLocationET.getText().toString().trim();
            payment.pickupTime = tempCal.getTimeInMillis();
            if(pickupHotelRB.isChecked()){
                payment.pickupLocationType = Payment.PICKUP_LOCATION_TYPE_HOTEL;
            }else{
                payment.pickupLocationType = Payment.PICKUP_LOCATION_TYPE_AIRPORT;
            }
        }

        PaymentAmount paymentAmount = new PaymentAmount();
        paymentAmount.roomEach = allPrice[0];
        paymentAmount.roomNoofNights = allPrice[1];
        paymentAmount.roomTotal = allPrice[2];
        paymentAmount.itinerary = allPrice[3];
        paymentAmount.travelKitMap = allTravelKitPrice;
        paymentAmount.serviceAndTax = allPrice[5];
        paymentAmount.grandTotal = allPrice[6];
        paymentAmount.currencyCode = localCurrencyCode;
        payment.paymentAmount = paymentAmount;
        payment.isReadByAdmin = false;
        //create a log file for each payment process in cloud function to store in firestore, paymentLog -> uid -> logId -> log
        //actually the log file already exists in the cloud function console log. so i think no need purposely store another one is firestore since i can search in the cloud function console also

        GeneralFunction.getFirestoreInstance()
                .collection(Payment.CONTINUE_PAYMENT_URL)
                .document(parentActivity.uid)
                .set(payment)
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        parentActivity.controlLoadingDialog(false,parentActivity);
                        GeneralFunction.openMessageNotificationDialog(parentActivity,e.getMessage(),R.drawable.notice_bad,TAG);
                    }
                })
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        paymentResultListener = GeneralFunction.getFirestoreInstance()
                                .collection(Payment.PAYMENT_RESULT_URL)
                                .whereEqualTo(Payment.LOCAL_TIMESTAMP,localTimestamp.getTimeInMillis())
                                .whereEqualTo(Payment.USER_ID,parentActivity.uid)
                                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                                    @Override
                                    public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                                        if(queryDocumentSnapshots != null && (queryDocumentSnapshots.getMetadata().isFromCache() || queryDocumentSnapshots.isEmpty())){
                                            return;
                                        }

                                        if(GeneralFunction.hasError(parentActivity,e,TAG)){
                                            //FAILED: SOMETHING WRONG WITH FIRESTORE
                                            handleOnPaymentResultFinished();
                                            return;
                                        }

                                        try{
                                            for(DocumentSnapshot documentSnapshot : queryDocumentSnapshots.getDocuments()){
                                                if(documentSnapshot != null && documentSnapshot.getString(Payment.ERROR_MSG) != null){
                                                    //FAILED: PAYMENT RESULT RETURNS ERROR MESSAGE
                                                    GeneralFunction.openMessageNotificationDialog(parentActivity, documentSnapshot.getString(Payment.ERROR_MSG), R.drawable.notice_bad, TAG);
                                                    handleOnPaymentResultFinished();
                                                    return;
                                                }

                                                if(documentSnapshot != null
                                                        && documentSnapshot.get(Payment.PAYMENT_METHOD_NONCE) != null
//                                                        && documentSnapshot.get(Payment.PAYMENT_METHOD_TYPE) != null
                                                        && documentSnapshot.get(Payment.CLIENT_TOKEN) != null
                                                        && documentSnapshot.get(Payment.PAYMENT_AMOUNT) != null
                                                        && documentSnapshot.get(Payment.AMOUNT) != null
                                                        && documentSnapshot.getString(Payment.STATUS) != null
                                                        && documentSnapshot.getString(Payment.USER_ID) != null){
                                                    switch (documentSnapshot.getString(Payment.STATUS)){
                                                        case Payment.STATUS_PENDING:
                                                            //STATUS IS PENDING
                                                            break;
                                                        case Payment.STATUS_SUCCESS:
                                                            //STATUS IS SUCCESS
                                                            GeneralFunction.openMessageNotificationDialog(parentActivity, parentActivity.getString(R.string.processing_payment_success), R.drawable.notice_good, TAG);
                                                            handleOnPaymentResultFinished();
                                                            parentActivity.goToHome();
                                                            break;
                                                        case Payment.STATUS_FAILED:
                                                            //STATUS IS FAILED
                                                            GeneralFunction.openMessageNotificationDialog(parentActivity, parentActivity.getString(R.string.processing_payment_failed), R.drawable.notice_bad, TAG);
                                                            handleOnPaymentResultFinished();
                                                            break;
                                                        default:
                                                            //FAILED: STATUS IS UNKNOWN OTHERS
                                                            GeneralFunction.openMessageNotificationDialog(parentActivity, parentActivity.getString(R.string.processing_payment_unknown_failed), R.drawable.notice_bad, TAG);
                                                            handleOnPaymentResultFinished();
                                                            break;
                                                    }
                                                }else{
                                                    //FAILED: PAYMENT RESULT DATA RETURNED IS INCOMPLETE
                                                    GeneralFunction.openMessageNotificationDialog(parentActivity, parentActivity.getString(R.string.processing_payment_failed), R.drawable.notice_bad, TAG);
                                                    handleOnPaymentResultFinished();
                                                }
                                            }
                                        }catch (Exception exception) {
                                            //FAILED: CANT PROCESS THE PAYMENT RESULT RETURNED
                                            exception.printStackTrace();
                                            GeneralFunction.openMessageNotificationDialog(parentActivity, parentActivity.getString(R.string.processing_payment_failed), R.drawable.notice_bad, TAG);
                                            handleOnPaymentResultFinished();
                                        }
                                    }
                                });
                    }
                });
    }

    private void handleOnPaymentResultFinished(){
        parentActivity.controlLoadingDialog(false,parentActivity);
        if(paymentResultListener != null){
            paymentResultListener.remove();
        }
    }

    private void calculateAndPopulateAllTravelKitsAndAddons(){
        //***********TRAVEL KIT - (4)**********************
        allPrice[4] = 0;

        LayoutInflater layoutInflater = (LayoutInflater) parentActivity.getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if((selectedTKs != null && !selectedTKs.isEmpty())){

            int rowIndex = 2; //already have the nightTR and itineraryTR, so start from 2;

            for(TravelKit travelKit : selectedTKs){
                View tkSummaryView = layoutInflater.inflate(R.layout.fragment_travel_kits_payment_summary, null);
                TextView kitNameTV = tkSummaryView.findViewById(R.id.kitNameTV);

                if(travelKit != null && travelKit.productList != null && travelKit.currencyCode != null){
                    if(travelKit.name != null){
                        kitNameTV.setText(travelKit.name);
                    }else{
                        kitNameTV.setText("???");
                    }

                    priceTL.addView(tkSummaryView,rowIndex);
                    rowIndex++;

                    //THIS IS FOR LOOPING TO DISPLAY AND CALCULATE THE PRICE OF EACH TRAVEL KIT'S SELECTED PRODUCTS
                    Iterator productIt = travelKit.productList.entrySet().iterator();
                    Double thisTravelKitTotalPrice = 0.0;

                    while (productIt.hasNext()) {
                        View tkProductSummaryView = layoutInflater.inflate(R.layout.fragment_travel_kits_product_payment_summary, null);

                        TextView productNameTV = tkProductSummaryView.findViewById(R.id.productNameTV);
                        TextView productCurrencyTV = tkProductSummaryView.findViewById(R.id.productCurrencyTV);
                        TextView productPriceTV = tkProductSummaryView.findViewById(R.id.productPriceTV);

                        Map.Entry pair = (Map.Entry) productIt.next();
                        String productName = pair.getKey().toString();
                        Double productPrice = CurrencyRate.getUserLocalPrice(parentActivity,localCurrencyCode,travelKit.currencyCode,(Double) pair.getValue()); //converted to local price

//                            //this is for discount if got, but for now dont let yet
//                            if(travelKit.discount != null && travelKit.discount > 0){
//                                productPrice = productPrice - (productPrice * travelKit.discount / 100);
//                            }

                        int quantity = 1;
                        if(travelKit.quantityMap != null && travelKit.quantityMap.get(productName) != null){
                            try {
                                quantity =  travelKit.quantityMap.get(productName).intValue();
                            }catch (Exception e){
                                Crashlytics.logException(e);
                            }
                        }
                        productPrice = productPrice * quantity;
                        thisTravelKitTotalPrice += productPrice;

                        productName += " (x" + String.valueOf(quantity) + ")";
                        productNameTV.setText(productName);
                        productCurrencyTV.setText(localCurrencyCode);
                        productPriceTV.setText(GeneralFunction.getDecimalFormat(2).format(productPrice));

                        priceTL.addView(tkProductSummaryView,rowIndex);
                        rowIndex++;
                    }

                    allTravelKitPrice.put(travelKit.name,thisTravelKitTotalPrice);
                    allPrice[4] += thisTravelKitTotalPrice;
                }
            }
        }
    }

    private void populateDetails(){
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(bookingHistory != null){
                    if(bookingHistory.id == null || bookingHistory.id.isEmpty() || room == null || room.id == null || room.id.isEmpty()){
                        GeneralFunction.openMessageNotificationDialog(parentActivity,"no id, mayb is because of search hostroom fragment?",R.drawable.notice_bad,TAG);
                        promptErrorAndGoBack(null);
                        return;
                    }

                    if(room.accommodationName != null){
                        accNameTV.setText(room.accommodationName);
                    }else{
                        accNameTV.setText("???");
                    }

                    if(allPrice[1] > 1){
                        noofNightTV.setText(String.format( parentActivity.getString(R.string.noof_nights),(int)allPrice[1] ) );
                    }else{
                        noofNightTV.setText(String.format( parentActivity.getString(R.string.noof_night),(int)allPrice[1] ) );
                    }

                    if(room.roomType != null){
                        switch (room.roomType){
                            case Room.TYPE_ROOM_ENTIRE_PLACE:
                                roomTypeTV.setText(parentActivity.getString(R.string.btn_entirePlace));
                                break;
                            case Room.TYPE_ROOM_PRIVATE:
                                roomTypeTV.setText(parentActivity.getString(R.string.btn_privateRoom));
                                break;
                            case Room.TYPE_ROOM_SHARE:
                                roomTypeTV.setText(parentActivity.getString(R.string.btn_shareRoom));
                                break;
                            default:
                                roomTypeTV.setText("???");
                                break;
                        }
                    }else{
                        roomTypeTV.setText("???");
                    }
                    stayCostTV.setText(parentActivity.getString(R.string.stay_cost));
                    groupAcc.setVisibility(View.VISIBLE);
                }else if(itinerary != null){
                    if(itinerary.id == null || itinerary.id.isEmpty()){
                        GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_occured),R.drawable.notice_bad,TAG);
                        promptErrorAndGoBack(null);
                        return;
                    }

                    if(itinerary.tripName != null && !itinerary.tripName.trim().isEmpty()){
                        itineraryTitleTV.setText(itinerary.tripName);
                    }else{
                        itineraryTitleTV.setText("???");
                    }

                    stayCostTV.setText(parentActivity.getString(R.string.itinerary_cost));
                    groupAcc.setVisibility(View.GONE);
                }
                else{
                    stayCostTV.setText(parentActivity.getString(R.string.travel_kit_cost));
                    groupAcc.setVisibility(View.GONE);
                }

                String rateStr = localCurrencyCode + " " + GeneralFunction.getDecimalFormat(2).format(allPrice[0]);
                rateTV.setText(rateStr);

                totalNightCurrencyTV.setText(localCurrencyCode);
                itineraryCurrencyTV.setText(localCurrencyCode);
                taxCurrencyTV.setText(localCurrencyCode);
                totalCurrencyTV.setText(localCurrencyCode);

                totalNightPriceTV.setText(GeneralFunction.getDecimalFormat(2).format(allPrice[2]));
                itineraryPriceTV.setText(GeneralFunction.getDecimalFormat(2).format(allPrice[3]));
                taxPriceTV.setText(GeneralFunction.getDecimalFormat(2).format(allPrice[5]));
                totalPriceTV.setText(GeneralFunction.getDecimalFormat(2).format( allPrice[6]));
            }
        });
    }

    private void calculateAllPrice(){
        /*
            0 = rate per night ***CURRENCY CONVERSION WILL OCCUR HERE***
            1 = total night
            2 = total accommodation night * the rate per night
            3 = itineraries price
            4 = travel kit price ***CURRENCY CONVERSION WILL OCCUR HERE***
            5 = service tax of all items combined
            6 = final total price
        */
        //***********TRAVEL KIT - (0,1,2)**********************

        if(bookingHistory != null && bookingHistory.checkinDate != null && bookingHistory. checkoutDate != null && room != null && room.currencyValue != null && room.currencyType != null){
            try{
                allPrice[0] = CurrencyRate.getUserLocalPrice(parentActivity,localCurrencyCode,room.currencyType,room.currencyValue);
            }catch (Exception e){
                Crashlytics.log("The price of room id: " + room.id + " is NOT A NUMBER! in payment summary");
                promptErrorAndGoBack(parentActivity.getString(R.string.error_occured_failed_retrieve_price));
                return;
            }

            allPrice[1] = GeneralFunction.getNoofDaysInDate(bookingHistory.checkinDate,bookingHistory.checkoutDate);
            if(allPrice[1] > 1){
                allPrice[1] = allPrice[1] - 1;
            }
            allPrice[2] = allPrice[0] * allPrice[1];

            checkinTV.setText(GeneralFunction.formatDateToString(bookingHistory.checkinDate));
            checkoutTV.setText(GeneralFunction.formatDateToString(bookingHistory.checkoutDate));

            nightTR.setVisibility(View.VISIBLE);
        }else{
            nightTR.setVisibility(View.GONE);
        }

        //***********ITINERARIES - (3)**********************
        if(itinerary != null && itinerary.price != null && itinerary.currencyCode != null){
            allPrice[3] = CurrencyRate.getUserLocalPrice(parentActivity,localCurrencyCode,itinerary.currencyCode,itinerary.price);

            itineraryTR.setVisibility(View.VISIBLE);
        }else{
            itineraryTR.setVisibility(View.GONE);
        }

        //***********TRAVEL KIT - (4)**********************
        //HAS CHANGED LOCATION. IT IS LOCATED AT THE PREVIOUS FUNCTION BEFORE THIS FUNCTION EXECUTES
//        if(travelKit != null && travelKit.originalPrice != null && travelKit.currencyCode != null){
//            double originalPrice = CurrencyRate.getUserLocalPrice(parentActivity,localCurrencyCode,travelKit.currencyCode,travelKit.originalPrice);
//            if(travelKit.discount != null && travelKit.discount > 0){
//                allPrice[4] = originalPrice - (originalPrice * travelKit.discount / 100);
//            }else{
//                allPrice[4] = originalPrice;
//            }
//        }

        //***********TAX - (5)**********************
        allPrice[5] = (allPrice[2] + allPrice[3] + allPrice[4]) * 10 / 100; //10% SST? thats why i put total price * 10 / 100
        allPrice[5] = 0; //They say no need so i make it 0 only

        //***********TOTAL - (6)********************
        allPrice[6] = allPrice[2] + allPrice[3] + allPrice[4] + allPrice[5];
    }

    private void promptErrorAndGoBack(String errorMsg){
        if(errorMsg == null || errorMsg.isEmpty()){
            errorMsg = parentActivity.getString(R.string.error_occured);
        }

        //it will crash if i didnt delay the on back pressed because the fragment is still running
        GeneralFunction.openMessageNotificationDialog(parentActivity,errorMsg,R.drawable.notice_bad,TAG);
        try{
            new Handler().postDelayed(new Runnable(){
                public void run(){
                    parentActivity.onBackPressed();
                }
            }, 1000);
        }catch (Exception e){
            Crashlytics.logException(e);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        parentActivity.showBtmNav();
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.hideBtmNav();
                if(bookingHistory != null){
                    parentActivity.setToolbarTitle(parentActivity.getString(R.string.booking_action_bar2));
                }else{
                    parentActivity.setToolbarTitle(parentActivity.getString(R.string.booking_action_bar2_Kit));
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        //got check for status because if he request from admin, status wont be null and he can take his time to purchase the itinerary
        //if he buy directly from admin, status will be null, so must delete if didnt plan to pay
        //if he request 1 then he can slowly pay, don't delete
        if(itinerary != null && itinerary.quotationStatus == null && itiRef != null && !hasGoneThroughPaymentProcess && needDeleteCreatedItinerary){
            itiRef.delete();
        }

        if(clientTokenListener != null){
            clientTokenListener.remove();
        }

        if(paymentResultListener != null){
            paymentResultListener.remove();
        }

        if(mBraintreeFragment != null){
            if(nonceCreatedListener != null){
                mBraintreeFragment.removeListener(nonceCreatedListener);
            }
            if(braintreeCancelListener != null){
                mBraintreeFragment.removeListener(braintreeCancelListener);
            }
            if(braintreeErrorListener != null){
                mBraintreeFragment.removeListener(braintreeErrorListener);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Config.REQUEST_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                DropInResult result = data.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT);
                // use the result to update your UI and send the payment method nonce to your server
                continuePaymentByDeletingPreviousFirstIfExists(result);
            } else if (resultCode == Activity.RESULT_CANCELED) {
                // the user canceled
            } else {
                // handle errors here, an exception may be available in
                Exception error = (Exception) data.getSerializableExtra(DropInActivity.EXTRA_ERROR);
                GeneralFunction.openMessageNotificationDialog(parentActivity,error.getMessage(),R.drawable.notice_bad,TAG);
            }
        }
    }

    private void startPaymentProcess(double totalPrice){
        DropInRequest dropInRequest = new DropInRequest()
                .clientToken(clientToken);
        amount = totalPrice;
        startActivityForResult(dropInRequest.getIntent(parentActivity), Config.REQUEST_PAYMENT);
    }

    private void setupDirectPaypal(double totalPrice){
        parentActivity.initializeLoadingDialog(parentActivity.getString(R.string.redirecting_to_paypal_payment),false);
        parentActivity.controlLoadingDialog(true,parentActivity);

        try {
            mBraintreeFragment = BraintreeFragment.newInstance(parentActivity, clientToken);

        } catch (InvalidArgumentException e) {
            GeneralFunction.openMessageNotificationDialog(parentActivity,e.getMessage(),R.drawable.notice_bad,TAG);
        }

        nonceCreatedListener = new PaymentMethodNonceCreatedListener() {
            @Override
            public void onPaymentMethodNonceCreated(PaymentMethodNonce paymentMethodNonce) {
//                String nonce = paymentMethodNonce.getNonce();
//                DropInResult result = data.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT);
                // use the result to update your UI and send the payment method nonce to your server
                continuePaymentByDeletingPreviousFirstIfExistsPaypal(paymentMethodNonce);
            }
        };
        braintreeCancelListener = new BraintreeCancelListener() {
            @Override
            public void onCancel(int requestCode) {
                parentActivity.controlLoadingDialog(false,parentActivity);
            }
        };
        braintreeErrorListener = new BraintreeErrorListener() {
            @Override
            public void onError(Exception error) {
                parentActivity.controlLoadingDialog(false,parentActivity);
                GeneralFunction.openMessageNotificationDialog(parentActivity,error.getMessage(),R.drawable.notice_bad,TAG);
                error.printStackTrace();
            }
        };

        mBraintreeFragment.addListener(nonceCreatedListener);
        mBraintreeFragment.addListener(braintreeCancelListener);
        mBraintreeFragment.addListener(braintreeErrorListener);

        amount = totalPrice;
        PayPalRequest request = new PayPalRequest(String.valueOf(totalPrice))
                .currencyCode(localCurrencyCode)
                .intent(PayPalRequest.INTENT_AUTHORIZE);
        PayPal.requestOneTimePayment(mBraintreeFragment, request);
    }
    private void continuePaymentByDeletingPreviousFirstIfExistsPaypal(final PaymentMethodNonce paymentMethodNonce){
        parentActivity.controlLoadingDialog(false,parentActivity);
        parentActivity.initializeLoadingDialog(parentActivity.getString(R.string.processing_payment),false);
        parentActivity.controlLoadingDialog(true,parentActivity);

        GeneralFunction.getFirestoreInstance()
                .collection(Payment.CONTINUE_PAYMENT_URL)
                .document(parentActivity.uid)
                .delete()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        //have some delay after delete before setting the payment continue
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Payment payment = new Payment();

                                payment.describeContents = paymentMethodNonce.describeContents();
//                                payment.deviceData = paymentMethodNonce.getDeviceData();
                                HashMap<String,Object> nonceMap = new HashMap<>();
                                nonceMap.put("nonce",paymentMethodNonce.getNonce());
                                if (paymentMethodNonce instanceof PayPalAccountNonce) {
                                    PayPalAccountNonce payPalAccountNonce = (PayPalAccountNonce)paymentMethodNonce;

                                    nonceMap.put("email",payPalAccountNonce.getEmail());
                                    nonceMap.put("firstName",payPalAccountNonce.getFirstName());
                                    nonceMap.put("lastName",payPalAccountNonce.getLastName());
                                    nonceMap.put("phone",payPalAccountNonce.getPhone());
                                    nonceMap.put("billingAddress",payPalAccountNonce.getBillingAddress());
                                    nonceMap.put("shippingAddress",payPalAccountNonce.getShippingAddress());
                                }

                                payment.paymentMethodNonce = nonceMap;
//                                payment.paymentMethodType = paymentMethodNonce.getPaymentMethodType();
                                continueToPayment(payment);
                            }
                        },500);
                    }
                });
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        Calendar cal = new GregorianCalendar();
        cal.set(Calendar.HOUR_OF_DAY,hourOfDay);
        cal.set(Calendar.MINUTE,minute);

        pickupHour = hourOfDay;
        pickupMin = minute;

        pickupTimeTV.setText(GeneralFunction.formatDateToTime(cal.getTime()));
    }

    public void showHourPicker() {
        final Calendar myCalender = new GregorianCalendar();
        int hour = myCalender.get(Calendar.HOUR_OF_DAY);
        int minute = myCalender.get(Calendar.MINUTE);

        if(pickupHour > 0 || pickupMin > 0){
            hour = pickupHour;
            minute = pickupMin;
        }

        TimePickerDialog timePickerDialog = new TimePickerDialog(parentActivity, android.R.style.Theme_Holo_Light_Dialog_NoActionBar, PaymentSummaryFragment.this, hour, minute, false);
        if(timePickerDialog.getWindow() != null){
            timePickerDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            timePickerDialog.show();
        }
    }

    private void showDate(int year, int monthOfYear, int dayOfMonth, int spinnerTheme) {
        if(pickupDay > 0 || pickupMonth > 0 || pickupYear > 0){
            year = pickupYear;
            monthOfYear = pickupMonth - 1;
            dayOfMonth = pickupDay;
        }

        new SpinnerDatePickerDialogBuilder()
                .context(parentActivity)
                .callback(PaymentSummaryFragment.this)
                .spinnerTheme(spinnerTheme)
                .defaultDate(year, monthOfYear, dayOfMonth)
                .build()
                .show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        final Calendar calendar = new GregorianCalendar(year, monthOfYear, dayOfMonth);
        pickupDay = dayOfMonth;
        pickupMonth = monthOfYear + 1;
        pickupYear = year;
        pickupDateTV.setText(GeneralFunction.formatDateToString(calendar.getTimeInMillis()));
        pickupTimeTV.requestFocus();
    }

    private boolean validatePickupDetails(){
        if(itinerary != null || (selectedTKs != null && !selectedTKs.isEmpty())){
            if(pickupDateTV.getText().toString().trim().isEmpty()){
                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.pick_up_date_err),R.drawable.notice_bad,TAG);
                return false;
            }

            if(pickupTimeTV.getText().toString().trim().isEmpty()){
                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.pick_up_time_err),R.drawable.notice_bad,TAG);
                return false;
            }

            Calendar tempCal = GeneralFunction.getDefaultUtcCalendar();
            tempCal.set(pickupYear,pickupMonth - 1,pickupDay,pickupHour,pickupMin);
            Calendar currentUserCal = new GregorianCalendar();
            currentUserCal.set(Calendar.MILLISECOND,0);
            currentUserCal.setTimeZone(TimeZone.getTimeZone("UTC"));
//            pickup date must have 24 hour more than current time
            if(currentUserCal.getTimeInMillis() + (24 * 60 * 60 * 1000) > tempCal.getTimeInMillis()){
                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.pick_up_time_must_more_than_24_hour_err),R.drawable.notice_bad,TAG);
                return false;
            }

            if(pickupLocationET.getText().toString().trim().isEmpty()){
                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.pick_up_location_name_err),R.drawable.notice_bad,TAG);
                return false;
            }

            if(!pickupAirportRB.isChecked() && !pickupHotelRB.isChecked()){
                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.pick_up_location_name_type_err),R.drawable.notice_bad,TAG);
                return false;
            }

            return true;

        }else{
            return true;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.confirmTV:
                if(totalPriceTV.getText().toString().isEmpty()){
                    promptErrorAndGoBack(parentActivity.getString(R.string.error_occured_failed_calculate_final_price));
                    return;
                }

                double totalPrice;
                try {
                    totalPrice = Double.parseDouble(totalPriceTV.getText().toString());
                }catch (Exception e){
                    Crashlytics.log("failed to convert total price to double in payment summary");
                    promptErrorAndGoBack(parentActivity.getString(R.string.error_occured_failed_calculate_final_price));
                    return;
                }

                if(totalPrice < 0){
                    Crashlytics.log("failed getting total price from text view in payment summary");
                    promptErrorAndGoBack(parentActivity.getString(R.string.error_occured_failed_calculate_final_price));
                    return;
                }

                final double finalTotalPrice = totalPrice;

                if(!validatePickupDetails()){
                    return;
                }

                //start the payment gateway UI stuff
                if(clientToken != null && !clientToken.isEmpty()){

                    if(itinerary != null){

                        if(itiRef == null){
                            itiRef = GeneralFunction.getFirestoreInstance()
                                    .collection(Itinerary.URL_ITINERARY)
                                    .document();
                        }

                        //for quotation typed itinerary purchase
                        if(itinerary.quotationStatus != null && TextUtils.equals(itinerary.quotationStatus,Itinerary.STATUS_DONE)){
                            itiRef = GeneralFunction.getFirestoreInstance()
                                    .collection(Itinerary.URL_ITINERARY)
                                    .document(itinerary.id);
                            hasAlreadyCreatedItinerary = true;
                        }else if(itinerary.isPaid != null && !itinerary.isPaid){
                            itiRef = GeneralFunction.getFirestoreInstance()
                                    .collection(Itinerary.URL_ITINERARY)
                                    .document(itinerary.id);
                            hasAlreadyCreatedItinerary = true;
                        }

                        if(!hasAlreadyCreatedItinerary){
                            itinerary.likeCount = (long) 0;
                            itinerary.isPublic = false;
                            itinerary.isPublish = false;
                            itinerary.isPaid = null;
                            itinerary.isFromAdmin = true;
                            itinerary.dateCreated = null;
                            itinerary.dateUpdated = null;
                            itinerary.userId = parentActivity.uid;
                            itinerary.oriItiId = itinerary.id;
                            itinerary.id = itiRef.getId();

                            parentActivity.initializeLoadingDialog(parentActivity.getString(R.string.itinerary_progress_edit_itinerary),false);
                            parentActivity.controlLoadingDialog(true,parentActivity);

                            itiRef.set(itinerary).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    parentActivity.controlLoadingDialog(false,parentActivity);

                                    if(itinerary != null){
                                        if(itinerary.planList != null && itinerary.planList.size() > 0){
                                            CollectionReference plansRef = GeneralFunction.getFirestoreInstance()
                                                    .collection(Itinerary.URL_ITINERARY)
                                                    .document(itinerary.id)
                                                    .collection(Plan.URL_PLAN);
                                            for(int i = 0; i < itinerary.planList.size(); i++) {
                                                int key = itinerary.planList.keyAt(i);
                                                ArrayList<Plan> plans = itinerary.planList.get(key);

                                                for(Plan plan : plans){
                                                    plan.dateCreated = null;
                                                    plansRef.document().set(plan);
                                                }
                                            }
                                        }
                                    }

                                    if(!GeneralFunction.hasErrorDocOnComplete(parentActivity,task,TAG)){
                                        needDeleteCreatedItinerary = true;
                                        hasAlreadyCreatedItinerary = true;
//                                        startPaymentProcess(finalTotalPrice);
                                        setupDirectPaypal(finalTotalPrice);
                                    }else{
                                        hasAlreadyCreatedItinerary = false;
                                        promptErrorAndGoBack(parentActivity.getString(R.string.error_occured_failed_create_itinerary));
                                    }
                                }
                            });
                        }else{
//                            startPaymentProcess(finalTotalPrice);
                            setupDirectPaypal(finalTotalPrice);
                        }
                    }else{
//                        startPaymentProcess(finalTotalPrice);
                        setupDirectPaypal(finalTotalPrice);
                    }


                }else{
                    //FAILED: ERROR GETTING CLIENT TOKEN
                    parentActivity.onBackPressed();
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.payment_client_token_failed),R.drawable.notice_bad,TAG);
                }

//                //todo need some modification
//                //for example the type saved in mode of transport fragment is saved as type rental into car rental collection but not as bike or car etc
//                //car rental didnt save booking history id
//                //the if else below should be if booking history is null no need save booking history id lo else save the travel kit id only
//                //if both got then save both
//                //because now if dont hsve booking, when user just want buy travel kit, he cant go to transport page
//                if(bookingHistory != null){
//                    ModeOfTransportFragment modeOfTransportFragment = ModeOfTransportFragment.newInstance(parentActivity,bookingHistory);
//                    GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, modeOfTransportFragment).addToBackStack(ModeOfTransportFragment.TAG).commit();
//                }

                break;
            case R.id.pickupDateTV:
                Calendar cal = GeneralFunction.getDefaultUtcCalendar();
                showDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), R.style.DatePickerSpinner);
                break;
            case R.id.pickupTimeTV:
                showHourPicker();
                break;
        }
    }
}
