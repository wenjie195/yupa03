package com.vidatechft.yupa.paymentFragment;

import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.BookingHistory;
import com.vidatechft.yupa.classes.EHailingBooking;
import com.vidatechft.yupa.classes.TravelKit;
import com.vidatechft.yupa.utilities.GeneralFunction;

public class ModeOfTransportFragment extends Fragment implements View.OnClickListener {
    public static final String TAG = ModeOfTransportFragment.class.getName();
    private MainActivity parentActivity;
    private Button btn_skip;
    private ConstraintLayout carRentalCL,taxiCL,eHailingCL,motorBikeRentalCL,bicycleRentalCL;
    private BookingHistory bookingHistory;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static ModeOfTransportFragment newInstance(MainActivity parentActivity, BookingHistory bookingHistory) {
        ModeOfTransportFragment fragment = new ModeOfTransportFragment();
        fragment.parentActivity = parentActivity;
        fragment.bookingHistory = bookingHistory;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_mode_of_transport, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.hideBtmNav();
                //Button
                btn_skip = view.findViewById(R.id.btn_skip);
                //Constraint Layout
                carRentalCL = view.findViewById(R.id.carRentalCL);
                taxiCL = view.findViewById(R.id.taxiCL);
                eHailingCL = view.findViewById(R.id.eHailingCL);
                motorBikeRentalCL = view.findViewById(R.id.motorBikeRentalCL);
                bicycleRentalCL = view.findViewById(R.id.bicycleRentalCL);

                //Listener
                btn_skip.setOnClickListener(ModeOfTransportFragment.this);
                carRentalCL.setOnClickListener(ModeOfTransportFragment.this);
                taxiCL.setOnClickListener(ModeOfTransportFragment.this);
                eHailingCL.setOnClickListener(ModeOfTransportFragment.this);
                motorBikeRentalCL.setOnClickListener(ModeOfTransportFragment.this);
                bicycleRentalCL.setOnClickListener(ModeOfTransportFragment.this);
            }
        });

        return view;
    }



    @Override
    public void onPause() {
        super.onPause();
        parentActivity.showBtmNav();
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.mode_of_transport_toolbar));
                parentActivity.hideBtmNav();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_skip:
                parentActivity.goToHome();
                break;
            case R.id.carRentalCL:
                CarRentalFragment carRentalFragment1 = CarRentalFragment.newInstance(parentActivity, EHailingBooking.TYPE_RENTAL, bookingHistory);
                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, carRentalFragment1).addToBackStack(CarRentalFragment.TAG).commit();
                break;
            case R.id.taxiCL:
                EHailingOrTaxiBookingFragment eHailingOrTaxiBookingFragment2 = EHailingOrTaxiBookingFragment.newInstance(parentActivity, EHailingBooking.TYPE_E_HAILING, bookingHistory);
                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, eHailingOrTaxiBookingFragment2).addToBackStack(EHailingOrTaxiBookingFragment.TAG).commit();
                break;
            case R.id.eHailingCL:
                EHailingOrTaxiBookingFragment eHailingOrTaxiBookingFragment3 = EHailingOrTaxiBookingFragment.newInstance(parentActivity, EHailingBooking.TYPE_E_HAILING, bookingHistory);
                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, eHailingOrTaxiBookingFragment3).addToBackStack(EHailingOrTaxiBookingFragment.TAG).commit();
                break;
            case R.id.motorBikeRentalCL:
                CarRentalFragment carRentalFragment2 = CarRentalFragment.newInstance(parentActivity, EHailingBooking.TYPE_RENTAL, bookingHistory);
                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, carRentalFragment2).addToBackStack(CarRentalFragment.TAG).commit();
                break;
            case R.id.bicycleRentalCL:
                CarRentalFragment carRentalFragment3 = CarRentalFragment.newInstance(parentActivity, EHailingBooking.TYPE_RENTAL, bookingHistory);
                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, carRentalFragment3).addToBackStack(CarRentalFragment.TAG).commit();
                break;
        }
    }
}
