package com.vidatechft.yupa.paymentFragment;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.CarRentalAdapter;
import com.vidatechft.yupa.adapter.NewCarRentalAdapter;
import com.vidatechft.yupa.classes.BookingHistory;
import com.vidatechft.yupa.classes.CarRental;
import com.vidatechft.yupa.classes.SelectableItem;
import com.vidatechft.yupa.classes.Transport;
import com.vidatechft.yupa.holder.SelectableViewHolder;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

public class CarRentalFragment extends Fragment implements View.OnClickListener, DatePickerDialog.OnDateSetListener, AdapterView.OnItemSelectedListener, SelectableViewHolder.OnItemSelectedListener {
    public static final String TAG = CarRentalFragment.class.getName();
    private MainActivity parentActivity;
    private Button btn_continue;
    private String type;
    private TextView startBookingLabelTV,startBookingDateTV,endBookingLabelTV,endBookingDateTV,noOfDayTV,locationLabelTV,locationTV;
    private DatePickerDialog datePickerDialog;
    private TimePickerDialog timePickerDialog;
    private int dobDay, dobMonth, dobYear;
    private SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");
    private SimpleDateFormat datetimeFormat = new SimpleDateFormat("dd MMM yyyyhh:mma");
    private Calendar calendar = new GregorianCalendar();
    private BookingHistory bookingHistory;
    private RecyclerView carRentalRV;
    private CarRentalAdapter carRentalAdapter;
    private Spinner locationSPN;
    private int buttonClick = 0;
    private NewCarRentalAdapter newCarRentalAdapter;
    private ArrayList<Transport>selectedTransportList  = new ArrayList<>();
    private Transport mTransport;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static CarRentalFragment newInstance(MainActivity parentActivity, String type, BookingHistory bookingHistory) {
        CarRentalFragment fragment = new CarRentalFragment();
        fragment.parentActivity = parentActivity;
        fragment.type = type;
        fragment.bookingHistory = bookingHistory;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_car_rental, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.hideBtmNav();

                //Button
                btn_continue = view.findViewById(R.id.btn_continue);
                //TextView
                startBookingLabelTV = view.findViewById(R.id.startBookingLabelTV);
                startBookingDateTV = view.findViewById(R.id.startBookingDateTV);
                endBookingLabelTV = view.findViewById(R.id.endBookingLabelTV);
                endBookingDateTV = view.findViewById(R.id.endBookingDateTV);
                noOfDayTV = view.findViewById(R.id.noOfDayTV);
                locationLabelTV = view.findViewById(R.id.locationLabelTV);
                locationTV = view.findViewById(R.id.locationTV);
                //Spinner
                locationSPN = view.findViewById(R.id.locationSPN);
                //RecycleView
                carRentalRV = view.findViewById(R.id.carRentalRV);
                //Listener
                startBookingLabelTV.setOnClickListener(CarRentalFragment.this);
                endBookingLabelTV.setOnClickListener(CarRentalFragment.this);
                locationTV.setOnClickListener(CarRentalFragment.this);
                btn_continue.setOnClickListener(CarRentalFragment.this);

                datePickerDialog = GeneralFunction.getDatePickerDialog(parentActivity,CarRentalFragment.this);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>
                        (parentActivity, android.R.layout.simple_spinner_item, parentActivity.getResources().getStringArray(R.array.country_list)); //selected item will look like a spinner set from XML
                spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                        .simple_spinner_dropdown_item);
                locationSPN.setAdapter(spinnerArrayAdapter);
                locationSPN.setOnItemSelectedListener(CarRentalFragment.this);
                getTransport();
            }
        });

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        parentActivity.showBtmNav();
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.mode_of_transport_toolbar));
                parentActivity.hideBtmNav();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_continue:
                submitTransportation();
                break;
            case R.id.startBookingLabelTV:
                buttonClick = 1;
                datePickerDialog.show();
                break;
            case R.id.endBookingLabelTV:
                buttonClick = 2;
                datePickerDialog.show();
                break;
            case R.id.locationTV:
                locationSPN.performClick();
                break;
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        dobDay = dayOfMonth;
        dobMonth = month + 1;
        dobYear = year;
        calendar.set(year,month,dayOfMonth);

        parentActivity.runOnUiThread(new Runnable() {
            public void run() {
                if(buttonClick != 0){
                    switch (buttonClick){
                        case 1:
                            startBookingDateTV.setText(format.format(calendar.getTime()));
                            break;
                        case 2:
                            endBookingDateTV.setText(format.format(calendar.getTime()));
                            break;
                    }
                }

                if(startBookingDateTV != null && endBookingDateTV != null){
                    String startDate = startBookingDateTV.getText().toString();
                    String endDate = endBookingDateTV.getText().toString();
                    Date myStartDate, myEndDate = null;
                    try {
                        myStartDate = format.parse(startDate);
                        myEndDate = format.parse(endDate);

                        long mStartDate = myStartDate.getTime();
                        long mEndDate = myEndDate.getTime();

                        long day =  GeneralFunction.getNoofDaysInDate(mStartDate,mEndDate);

                        if(day > 1){
                            noOfDayTV.setText(String.format(parentActivity.getString(R.string.itinerary_trip_days_num),day));
                        }else{
                            noOfDayTV.setText(String.format(parentActivity.getString(R.string.itinerary_trip_day_num),day));
                        }

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }


    private void getTransport(){
        final ArrayList<Transport> transportsList = new ArrayList<>();
        GeneralFunction.getFirestoreInstance()
                .collection(Transport.URL_TRANSPORT)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                        if(e != null){
                            GeneralFunction.openMessageNotificationDialog(parentActivity,e.toString(),R.drawable.notice_bad,TAG);
                            return;
                        }
                        transportsList.clear();
                        if(queryDocumentSnapshots != null){
                            if(!queryDocumentSnapshots.isEmpty()){
                                for (QueryDocumentSnapshot queryDocumentSnapshot : queryDocumentSnapshots){
                                    Transport transport = queryDocumentSnapshot.toObject(Transport.class);
                                    transport.id = queryDocumentSnapshot.getId();
                                    transportsList.add(transport);
                                }
                                carRentalAdapter = new CarRentalAdapter(parentActivity, bookingHistory, transportsList, new CarRentalAdapter.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(Transport transport, int pos, ConstraintLayout wholeLL) {

                                        carRentalAdapter.getWholeLLMap().get(pos).setSelected(true);
                                        selectedTransportList.clear();
                                        for(int i = 0; i< carRentalAdapter.getWholeLLMap().size(); i++){
                                            if(!carRentalAdapter.getWholeLLMap().get(i).isSelected()){
                                                carRentalAdapter.getWholeLLMap().get(i).setBackgroundColor(parentActivity.getResources().getColor(R.color.white));
                                            }
                                            else{
                                                carRentalAdapter.getWholeLLMap().get(i).setSelected(false);
                                                carRentalAdapter.getWholeLLMap().get(i).setActivated(true);
                                                carRentalAdapter.getWholeLLMap().get(i).setBackgroundColor(parentActivity.getResources().getColor(R.color.unread_notification));
                                            }
                                            if(carRentalAdapter.getWholeLLMap().get(i).isActivated()){
                                                selectedTransportList.add(transport);
                                                mTransport = transport;
                                            }
                                        }

                                    }
                                });
                                carRentalRV.setNestedScrollingEnabled(false);
                                carRentalRV.setAdapter(carRentalAdapter);
                                carRentalRV.setLayoutManager(new GridLayoutManager(parentActivity, 1));

//                                newCarRentalAdapter = new NewCarRentalAdapter(CarRentalFragment.this,transportsList,false);
//                                carRentalRV.setNestedScrollingEnabled(false);
//                                carRentalRV.setAdapter(newCarRentalAdapter);
//                                carRentalRV.setLayoutManager(new GridLayoutManager(parentActivity, 1));
                            }
                        }
                    }
                });

    }




    private boolean isValidInput(String arrivalDate, String arrivalTime, String pickUpLocationTV){
        return  GeneralFunction.checkAndReturnString(arrivalDate,parentActivity.getString(R.string.e_hailing_or_taxi_error_arrival_date),parentActivity)
                &&
                GeneralFunction.checkAndReturnString(arrivalTime,parentActivity.getString(R.string.e_hailing_or_taxi_error_arrival_time),parentActivity)
                &&
                GeneralFunction.checkAndReturnString(pickUpLocationTV,parentActivity.getString(R.string.e_hailing_or_taxi_error_pick_up_location),parentActivity);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Object item = parent.getItemAtPosition(position);
        if (item != null) {
            String country = locationSPN.getSelectedItem().toString();
            locationTV.setText(country);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        locationTV.setHint(R.string.car_rental_hint_select_country);
    }

    @Override
    public void onItemSelected(SelectableItem item) {
        List<Transport> selectedItems = newCarRentalAdapter.getSelectedItems();
        Snackbar.make(carRentalRV,"Selected item is "+item.id+
                ", Totally  selectem item count is "+selectedItems.size(),Snackbar.LENGTH_LONG).show();
    }

    private void submitTransportation(){
        HashMap<String,Object> transportationMap = new HashMap<>();
        String startDate = startBookingDateTV.getText().toString();
        String endDate = endBookingDateTV.getText().toString();
        String locationCountry = locationTV.getText().toString().trim();
        if(mTransport != null){
            try {
                if(isValidInput(startDate,endDate,locationCountry)){

                    Date mStartDate = format.parse(startDate);
                    Date mEndDate = format.parse(endDate);
                    //convert to utc format
                    Calendar cStartDate = convertDateToUTC(mStartDate);
                    Calendar cEndDate = convertDateToUTC(mEndDate);
                    long longStartDate = cStartDate.getTimeInMillis();
                    long longEndDate = cEndDate.getTimeInMillis();

                    transportationMap.put(CarRental.PICKUP_TIME, longStartDate);
                    transportationMap.put(CarRental.RETURN_TIME, longEndDate);
                    transportationMap.put(CarRental.LOCATION, locationCountry);
                    transportationMap.put(CarRental.TRANSPORT_ID, mTransport.id);
                    transportationMap.put(CarRental.USER_ID, parentActivity.uid);
                    transportationMap.put(CarRental.RENTAL_TYPE, type);
                    transportationMap.put(CarRental.DATE_CREATED, FieldValue.serverTimestamp());
                    transportationMap.put(CarRental.DATE_UPDATED, FieldValue.serverTimestamp());
                    //todo
                    DocumentReference carRentalRef = GeneralFunction.getFirestoreInstance()
                            .collection(CarRental.URL_CAR_RENTAL)
                            .document();
                    carRentalRef.set(transportationMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getResources().getString(R.string.e_hailing_or_taxi_successful_submit_car_rental_request),R.drawable.notice_good, TAG);
                                parentActivity.goToHome();
                            }
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    GeneralFunction.openMessageNotificationDialog(parentActivity,e.toString(),R.drawable.notice_bad,TAG);
                                }
                            });
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }else{
            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getResources().getString(R.string.e_hailing_or_taxi_error_transportation),R.drawable.notice_bad,TAG);
        }
    }

    private Calendar convertDateToUTC(Date myDate){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
        calendar.setTime(myDate);
        return calendar;
    }
}
