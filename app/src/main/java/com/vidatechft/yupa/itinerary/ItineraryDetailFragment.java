package com.vidatechft.yupa.itinerary;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.ItineraryPlanTabPagerAdapter;
import com.vidatechft.yupa.classes.Favourite;
import com.vidatechft.yupa.classes.Gallery;
import com.vidatechft.yupa.classes.Itinerary;
import com.vidatechft.yupa.classes.Payment;
import com.vidatechft.yupa.classes.PlaceDetailsGoogleApi.PlaceAddress;
import com.vidatechft.yupa.classes.Plan;
import com.vidatechft.yupa.classes.Search;
import com.vidatechft.yupa.gallery.CustomGalleryLayout;
import com.vidatechft.yupa.paymentFragment.SelectTravelKitFragment;
import com.vidatechft.yupa.travelKit.TravelKitReceiptLayout;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;

public class ItineraryDetailFragment extends Fragment implements View.OnClickListener{
    public static final String TAG = ItineraryDetailFragment.class.getName();
    private MainActivity parentActivity;
    private Itinerary itinerary;
    private Search search;
    private Payment payment;

    private ItineraryPlanTabPagerAdapter adapter;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private ImageView favIV;

    private ListenerRegistration listenerRegistration, favListener, customItiStatusListener;
    private SparseArray<ArrayList<Plan>> planList = new SparseArray<>();
    private Favourite favourite;
    private boolean doneGettingFavourite = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static ItineraryDetailFragment newInstance(MainActivity parentActivity, Itinerary itinerary, Search search) {
        ItineraryDetailFragment fragment = new ItineraryDetailFragment();

        fragment.parentActivity = parentActivity;
        fragment.itinerary = itinerary;
        fragment.search = search;

        return fragment;
    }

    public static ItineraryDetailFragment newInstance(MainActivity parentActivity, Itinerary itinerary, Payment payment) {
        ItineraryDetailFragment fragment = new ItineraryDetailFragment();

        fragment.parentActivity = parentActivity;
        fragment.itinerary = itinerary;
        fragment.payment = payment;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_itinerary_details, container, false);
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tabLayout = view.findViewById(R.id.tabLayout);
                viewPager = view.findViewById(R.id.pager);
                ImageView backIV = view.findViewById(R.id.backIV);
                favIV = view.findViewById(R.id.favIV);
                CustomGalleryLayout customGalleryLayout = view.findViewById(R.id.customGalleryLayout);
                TextView buyNowTV = view.findViewById(R.id.buyNowTV);
                TextView tripTV = view.findViewById(R.id.tripTV);
                TextView budgetTV = view.findViewById(R.id.budgetTV);
                TextView dateTV = view.findViewById(R.id.dateTV);
                LinearLayout dateLL = view.findViewById(R.id.dateLL);
                TextView durationTV = view.findViewById(R.id.durationTV);
                TextView placeTV = view.findViewById(R.id.placeTV);
                TextView cuisineTV = view.findViewById(R.id.cuisineTV);
                TextView activityTV = view.findViewById(R.id.activityTV);
                TextView introTV = view.findViewById(R.id.introTV);
                TextView addWishlistTV = view.findViewById(R.id.addWishlistTV);
                TextView editTV = view.findViewById(R.id.editTV);
                TravelKitReceiptLayout tkReceiptLayout = view.findViewById(R.id.tkReceiptLayout);

                buyNowTV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(GeneralFunction.isGuest()){
                            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_guest_restrict_usage),R.drawable.notice_bad,TAG);
                            return;
                        }

                        Fragment selectTravelKitFragment = SelectTravelKitFragment.newInstance(parentActivity,null,itinerary,search);
                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, selectTravelKitFragment).addToBackStack(SelectTravelKitFragment.TAG).commit();
                    }
                });
                backIV.setOnClickListener(ItineraryDetailFragment.this);
                favIV.setOnClickListener(ItineraryDetailFragment.this);
                addWishlistTV.setOnClickListener(ItineraryDetailFragment.this);
                editTV.setOnClickListener(ItineraryDetailFragment.this);

                if(itinerary.isFromAdmin != null && itinerary.isFromAdmin){
                    favIV.setVisibility(View.GONE);
                    addWishlistTV.setVisibility(View.GONE);
                    editTV.setVisibility(View.GONE);

                    if(itinerary.isPaid != null && itinerary.isPaid){
                        buyNowTV.setVisibility(View.GONE);
                    }else{
                        buyNowTV.setVisibility(View.VISIBLE);
                    }

                    if(itinerary.paymentId != null){
                        tkReceiptLayout.initLayout(parentActivity,ItineraryDetailFragment.this, itinerary.paymentId, payment,tkReceiptLayout);
                    }

                }

                if(itinerary != null){
                    ArrayList<Gallery> galleryList = new ArrayList<>();
                    if(itinerary.itineraryPicUrl != null){
                        Gallery coverGallery = new Gallery("","",itinerary.itineraryPicUrl);
                        galleryList.add(coverGallery);
                    }

                    if(itinerary.gallery != null && !itinerary.gallery.isEmpty()){
                        galleryList.addAll(itinerary.gallery);
                    }

                    customGalleryLayout.initLayout(parentActivity,ItineraryDetailFragment.this,customGalleryLayout,galleryList);

                    if(itinerary.tripName != null){
                        tripTV.setText(itinerary.tripName);
                    }else{
                        String title = GeneralFunction.getItineraryTitle(itinerary);
                        if(title != null){
                            tripTV.setText(title);
                        }else{
                            tripTV.setText("???");
                        }
                    }

                    String budget = "";

                    if(itinerary.price != null && itinerary.price > 0){
                        budget += GeneralFunction.getItineraryPrice(parentActivity,itinerary,null);
                    }else{
                        if(itinerary.minBudget != null ){
                            budget += GeneralFunction.getItineraryPrice(parentActivity,itinerary,itinerary.minBudget);
                        }

                        if(itinerary.maxBudget != null){
                            if(itinerary.minBudget != null){
                                if(!itinerary.maxBudget.equals(itinerary.minBudget)){
                                    budget += " - " + GeneralFunction.getItineraryPrice(parentActivity,itinerary,itinerary.maxBudget);
                                }
                            }else{
                                budget += GeneralFunction.getItineraryPrice(parentActivity,itinerary,itinerary.maxBudget);
                            }
                        }
                    }

                    if(!budget.isEmpty()){
                        budgetTV.setText(budget);
                    }

                    if(itinerary.isPublic != null && itinerary.isPublic && itinerary.isPublish != null && itinerary.isPublish && itinerary.oriItiId == null && itinerary.quotationStatus == null && itinerary.isPaid == null){
                        dateLL.setVisibility(View.GONE);
                    }else{
                        dateLL.setVisibility(View.VISIBLE);
                        if(itinerary.departDate != null && itinerary.returnDate != null){
                            Calendar departCal = new GregorianCalendar();
                            Calendar returnCal = new GregorianCalendar();

                            departCal.setTimeInMillis(itinerary.departDate);
                            returnCal.setTimeInMillis(itinerary.returnDate);

                            String date;
//                        if(departCal.get(Calendar.MONTH) == returnCal.get(Calendar.MONTH)){
//                            date = GeneralFunction.formatDateToStringOnlyDay(itinerary.departDate) + "-" + GeneralFunction.formatDateToStringWithoutYear(itinerary.returnDate);
//                        }else{
//                            date = GeneralFunction.formatDateToStringWithoutYear(itinerary.departDate) + "-" + GeneralFunction.formatDateToStringWithoutYear(itinerary.returnDate);
//                        }
                            if(departCal.get(Calendar.MONTH) == returnCal.get(Calendar.MONTH)){
                                if(departCal.get(Calendar.DAY_OF_MONTH) == returnCal.get(Calendar.DAY_OF_MONTH)){
                                    date = GeneralFunction.formatDateToStringWithoutYear(itinerary.departDate);
                                }else{
                                    date = GeneralFunction.formatDateToStringOnlyDay(itinerary.departDate) + "-" + GeneralFunction.formatDateToStringWithoutYear(itinerary.returnDate);
                                }
                            }else{
                                date = GeneralFunction.formatDateToStringWithoutYear(itinerary.departDate) + "-" + GeneralFunction.formatDateToStringWithoutYear(itinerary.returnDate);
                            }

                            dateTV.setText(date);
                        }
                    }

                    if(itinerary.durationHour != null && itinerary.durationHour > 0){
                        if(itinerary.durationHour > 1){
                            durationTV.setText(String.format(parentActivity.getString(R.string.itinerary_trip_hours_num),itinerary.durationHour));
                        }else{
                            durationTV.setText(String.format(parentActivity.getString(R.string.itinerary_trip_hour_num),itinerary.durationHour));
                        }
                    }else{
                        if(itinerary.noofDays != null){
                            if(itinerary.noofDays > 1){
                                durationTV.setText(String.format(parentActivity.getString(R.string.itinerary_trip_days_num),itinerary.noofDays));
                            }else{
                                durationTV.setText(String.format(parentActivity.getString(R.string.itinerary_trip_day_num),itinerary.noofDays));
                            }
                        }
                    }

                    if(itinerary.noofDays != null){
                        for(int day = 0; day < itinerary.noofDays; day++){
                            tabLayout.addTab(tabLayout.newTab().setText(parentActivity.getString(R.string.itinerary_trip_day) + " " + String.valueOf(day + 1)));
                        }

                        if(planList != null && planList.size() > 0 && adapter != null){
                            setupViewPager();
                        }else{
                            getAllItineraryPlans();
                        }
                    }

                    StringBuilder placeToVisit = new StringBuilder();
                    if(itinerary.placeAddressList != null && !itinerary.placeAddressList.isEmpty()){
                        for(HashMap<String,Object> placeAddressObject : itinerary.placeAddressList){
                            PlaceAddress placeAddress = PlaceAddress.getMapToClass(placeAddressObject);

                            if(placeAddress != null && placeAddress.placeName != null){
                                placeToVisit.append(placeAddress.placeName);
                                placeToVisit.append(", ");
                            }
                        }
                    }

                    if(placeToVisit.length() > 0){
                        placeToVisit.delete(placeToVisit.length() - 2, placeToVisit.length() - 1);//delete last two character which is the comma and space
                    }else{
                        if(itinerary.locationName != null){
                            placeToVisit.append(itinerary.locationName);
                        }else{
                            placeToVisit.append(parentActivity.getString(R.string.itinerary_no_place));
                        }
                    }

                    placeTV.setText(placeToVisit.toString());

                    if(itinerary.cuisineList != null && !itinerary.cuisineList.isEmpty()){
                        cuisineTV.setText(GeneralFunction.formatList(itinerary.cuisineList,true));
                    }

                    if(itinerary.activityList != null && !itinerary.activityList.isEmpty()){
                        activityTV.setText(GeneralFunction.formatList(itinerary.activityList,true));
                    }

                    if(itinerary.introMsg != null && !itinerary.introMsg.trim().isEmpty()){
                        TextView viewMoreTV = view.findViewById(R.id.viewMoreTV);
                        introTV.setText(itinerary.introMsg);
                        view.findViewById(R.id.introLabelTV).setVisibility(View.VISIBLE);
                        introTV.setVisibility(View.VISIBLE);
                        GeneralFunction.makeTextViewResizableWithButtonMethod(parentActivity, introTV, 10,10, parentActivity.getString(R.string.view_more), true,viewMoreTV);
                    }
                }
            }
        });
        return view;
    }

    private void getAllItineraryPlans(){
        initializePlanList();

        parentActivity.initializeLiveLoadingDialog(parentActivity.getString(R.string.itinerary_plan_loading),false);
        parentActivity.controlLiveLoadingDialog(true,parentActivity);

        listenerRegistration = GeneralFunction.getFirestoreInstance()
                                .collection(Itinerary.URL_ITINERARY)
                                .document(itinerary.id)
                                .collection(Plan.URL_PLAN)
                                .orderBy(Plan.START_TIME)
                                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                                    @Override
                                    public void onEvent(@javax.annotation.Nullable QuerySnapshot snapshots, @javax.annotation.Nullable final FirebaseFirestoreException e) {

                                        parentActivity.controlLiveLoadingDialog(false,parentActivity);

                                        if (e != null) {
                                            parentActivity.runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Log.w(TAG, "Listen failed.", e);
                                                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.itinerary_plan_err_loading),R.drawable.notice_bad,TAG);
                                                }
                                            });
                                            return;
                                        }

                                        initializePlanList();

                                        if (snapshots != null && !snapshots.isEmpty()) {
                                            for (QueryDocumentSnapshot snapshot : snapshots) {
                                                Plan plan = Plan.snapshotToPlan(parentActivity,snapshot);
                                                if(plan != null && plan.day != null && itinerary.noofDays > plan.day ){
                                                    planList.get(plan.day.intValue()).add(plan);
                                                }
                                            }

                                            if(adapter != null && !snapshots.getMetadata().isFromCache() && listenerRegistration != null){
                                                listenerRegistration.remove();
                                            }
                                        }

                                        setupAdapter();
                                    }
                                });

        GeneralFunction.getFirestoreInstance()
                .collection(Favourite.URL_FAVOURITE)
                .whereEqualTo(Favourite.TYPE,Favourite.FAV_TYPE_ITI)
                .whereEqualTo(Favourite.TARGET_ID,itinerary.id)
                .whereEqualTo(Favourite.USER_ID,parentActivity.uid)
                .orderBy(Favourite.DATE_UPDATED, Query.Direction.DESCENDING)
                .limit(1)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull final Task<QuerySnapshot> task) {
                        doneGettingFavourite = true;

                        if (task.getException() != null) {
                            parentActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.w(TAG, "Listen failed.", task.getException());
                                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.itinerary_plan_err_loading),R.drawable.notice_bad,TAG);
                                }
                            });

                            favIV.setVisibility(View.VISIBLE);

                            if(itinerary.isFromAdmin != null && itinerary.isFromAdmin){
                                favIV.setVisibility(View.GONE);
                            }
                            return;
                        }

                        if (task.getResult() != null && !task.getResult().isEmpty()) {
                            for (QueryDocumentSnapshot snapshot : task.getResult()) {
                                favourite = Favourite.snapshotToFavourite(parentActivity,snapshot);

                                itinerary.isFavourite = favourite != null && favourite.isLiked != null && favourite.isLiked;

                                if(itinerary.isFavourite){
                                    favIV.setImageDrawable(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.ic_love_whole_red,R.color.red));
                                }else{
                                    favIV.setImageDrawable(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.ic_love,R.color.white));
                                }
                            }
                        }

                        favIV.setVisibility(View.VISIBLE);

                        if(itinerary.isFromAdmin != null && itinerary.isFromAdmin){
                            favIV.setVisibility(View.GONE);
                        }
                    }
                });
    }

    private void initializePlanList(){
        planList.clear();
        for(int day = 0; day < itinerary.noofDays; day++){
            planList.put(day,new ArrayList<Plan>());
        }
    }

    private void setupAdapter(){
        if (!isAdded()) return;
        //to solve Fragment has not been attached yet.
        adapter = new ItineraryPlanTabPagerAdapter(parentActivity,ItineraryDetailFragment.this,ItineraryDetailFragment.this.getChildFragmentManager(), tabLayout.getTabCount(),itinerary,planList);

        setupViewPager();
    }

    private void setupViewPager(){

        if(itinerary != null && itinerary.noofDays > 0){
            if(itinerary.noofDays.intValue() > 7){
                viewPager.setOffscreenPageLimit(6);
            }else{
                viewPager.setOffscreenPageLimit(itinerary.noofDays.intValue() - 1);
            }

            itinerary.planList = planList;
            viewPager.setAdapter(adapter);
            viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

            tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    viewPager.setCurrentItem(tab.getPosition());
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });
        }
    }

    private void handleLikeAction(boolean forceLike){
        DocumentReference favRef;

        if(itinerary.isFavourite == null || favourite == null ||favourite.id == null){
            favRef = GeneralFunction.getFirestoreInstance()
                    .collection(Favourite.URL_FAVOURITE)
                    .document();

            favourite = new Favourite();

            favourite.id = favRef.getId();

            itinerary.isFavourite = true;
        }else{
            favRef = GeneralFunction.getFirestoreInstance()
                    .collection(Favourite.URL_FAVOURITE)
                    .document(favourite.id);

            itinerary.isFavourite = !itinerary.isFavourite;
        }

        if(forceLike){
            itinerary.isFavourite = true;
        }

        favourite.userId = parentActivity.uid;
        favourite.targetId = itinerary.id;
        favourite.type = Favourite.FAV_TYPE_ITI;
        favourite.isLiked = itinerary.isFavourite;
        HashMap<String,Object> favMap = Favourite.toMap(favourite);

        if(favourite.dateCreated == null){
            favMap.put(Favourite.DATE_CREATED, FieldValue.serverTimestamp());
        }
        favMap.put(Favourite.DATE_UPDATED, FieldValue.serverTimestamp());

        GeneralFunction.updateFavourite(parentActivity,favRef,favMap,favourite,TAG);

        if(favourite.isLiked){
            favIV.setImageDrawable(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.ic_love_whole_red,R.color.red));
        }else{
            favIV.setImageDrawable(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.ic_love,R.color.white));
        }

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onPause() {
        super.onPause();
        if(listenerRegistration != null){
            listenerRegistration.remove();
        }

        if(favListener != null){
            favListener.remove();
        }

        if(customItiStatusListener != null){
            customItiStatusListener.remove();
        }

        parentActivity.showToolbar();
        parentActivity.showBtmNav();
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.hideToolbar();
                parentActivity.hideBtmNav();

                parentActivity.setToolbarTitle(parentActivity.getString(R.string.itinerary_action_toolbar2));

                if(doneGettingFavourite){
                    favIV.setVisibility(View.VISIBLE);

                    if(favourite != null && favourite.isLiked != null && favourite.isLiked){
                        favIV.setImageDrawable(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.ic_love_whole_red,R.color.red));
                    }else{
                        favIV.setImageDrawable(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.ic_love,R.color.white));
                    }
                }else{
                    favIV.setImageDrawable(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.ic_love,R.color.white));
                }

                if(itinerary.isFromAdmin != null && itinerary.isFromAdmin){
                    favIV.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.backIV:
                parentActivity.onBackPressed();
                break;
            case R.id.favIV:
                if(GeneralFunction.isGuest()){
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_guest_restrict_usage),R.drawable.notice_bad,TAG);
                    return;
                }
                handleLikeAction(false);
                break;
            case R.id.addWishlistTV:
                if(GeneralFunction.isGuest()){
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_guest_restrict_usage),R.drawable.notice_bad,TAG);
                    return;
                }
                handleLikeAction(true);
                break;
            case R.id.editTV:
                if(GeneralFunction.isGuest()){
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_guest_restrict_usage),R.drawable.notice_bad,TAG);
                    return;
                }

                if(!TextUtils.equals(parentActivity.uid,itinerary.userId)){
                    parentActivity.initializeLoadingDialog(parentActivity.getString(R.string.itinerary_progress_edit_itinerary),false);
                    parentActivity.controlLoadingDialog(true,parentActivity);

                    itinerary.likeCount = (long) 0;
                    itinerary.isPublic = false;
                    itinerary.isPublish = false;
                    itinerary.isPaid = null;
                    itinerary.dateCreated = null;
                    itinerary.dateUpdated = null;
                    itinerary.userId = parentActivity.uid;
                    itinerary.oriItiId = itinerary.id;

                    DocumentReference ref = GeneralFunction.getFirestoreInstance()
                            .collection(Itinerary.URL_ITINERARY)
                            .document();
                    itinerary.id = ref.getId();

                    ref.set(itinerary).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            parentActivity.controlLoadingDialog(false,parentActivity);

                            if(itinerary != null){
                                if(itinerary.planList != null && itinerary.planList.size() > 0){
                                    CollectionReference plansRef = GeneralFunction.getFirestoreInstance()
                                            .collection(Itinerary.URL_ITINERARY)
                                            .document(itinerary.id)
                                            .collection(Plan.URL_PLAN);
                                    for(int i = 0; i < itinerary.planList.size(); i++) {
                                        int key = itinerary.planList.keyAt(i);
                                        ArrayList<Plan> plans = itinerary.planList.get(key);

                                        for(Plan plan : plans){
                                            plan.dateCreated = null;
                                            plansRef.document().set(plan);
                                        }
                                    }
                                }
                            }

                            if(!GeneralFunction.hasErrorDocOnComplete(parentActivity,task,TAG)){
                                Itinerary.goToManageItineraryOrPlan(parentActivity,itinerary);
                            }
                        }
                    });
                }else{
                    Itinerary.goToManageItineraryOrPlan(parentActivity,itinerary);
                }
                break;
        }
    }
}