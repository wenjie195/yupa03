package com.vidatechft.yupa.itinerary;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.ItineraryRecyclerGridAdapter;
import com.vidatechft.yupa.classes.Favourite;
import com.vidatechft.yupa.classes.Itinerary;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;

public class ItineraryViewAllFragment extends Fragment{
    public static final String TAG = ItineraryViewAllFragment.class.getName();
    private MainActivity parentActivity;
    private ArrayList<Itinerary> itiList = new ArrayList<>();
    private Query queryRef;

    private RecyclerView itiRV;
    private ItineraryRecyclerGridAdapter itiAdapter;

    private ListenerRegistration itiListener;

    private String loadingTxt,emptyTxt,errorTxt;
    private TextView noItiTV;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static ItineraryViewAllFragment newInstance(MainActivity parentActivity, ArrayList<Itinerary> itiList, Query queryRef) {
        ItineraryViewAllFragment fragment = new ItineraryViewAllFragment();

        fragment.parentActivity = parentActivity;
        if(itiList == null){
            fragment.itiList = new ArrayList<>();
        }else{
            fragment.itiList = itiList;
        }
        fragment.queryRef = queryRef;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_itinerary_view_all, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.showBtmNav();

                loadingTxt = parentActivity.getString(R.string.itinerary_loading_other);
                emptyTxt = parentActivity.getString(R.string.itinerary_empty_other);
                errorTxt = parentActivity.getString(R.string.itinerary_err_loading_other);

                itiRV = view.findViewById(R.id.itiRV);
                noItiTV = view.findViewById(R.id.noItiTV);

                setUpGridAdapter();

                if(itiList.size() <= 0){
                    getIti();
                }else{
                    noItiTV.setVisibility(View.GONE);
                }
            }
        });

        return view;
    }

    private void setUpGridAdapter(){

        itiAdapter = new ItineraryRecyclerGridAdapter(parentActivity,this, itiList,false);

        itiRV.setAdapter(itiAdapter);
        itiRV.setLayoutManager(new GridLayoutManager(parentActivity, 2));
    }

    private void getIti(){
        //todo the recycler view needs to have lazy load

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                noItiTV.setText(loadingTxt);
                noItiTV.setVisibility(View.VISIBLE);
            }
        });

        Query ref;
        if(queryRef != null){
            ref = queryRef;
        }else{
            ref = GeneralFunction.getFirestoreInstance()
                    .collection(Itinerary.URL_ITINERARY)
                    .whereEqualTo(Itinerary.IS_PUBLIC,true)
                    .whereEqualTo(Itinerary.IS_PUBLISH,true)
                    .orderBy(Itinerary.LIKE_COUNT, Query.Direction.DESCENDING)
                    .orderBy(Itinerary.DATE_UPDATED, Query.Direction.DESCENDING);
        }

        itiListener = ref
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot snapshots, @javax.annotation.Nullable final FirebaseFirestoreException e) {
                        if (e != null) {
                            parentActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.w(TAG, "Listen failed.", e);
                                    noItiTV.setText(errorTxt);
                                    noItiTV.setVisibility(View.VISIBLE);
                                }
                            });
                            return;
                        }

                        itiList.clear();

                        if (snapshots != null && !snapshots.isEmpty()) {
                            for (QueryDocumentSnapshot snapshot : snapshots) {
                                Itinerary tempIti = new Itinerary(parentActivity, snapshot);
                                itiList.add(tempIti);
                            }

                            parentActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    itiAdapter.notifyDataSetChanged();
                                    noItiTV.setVisibility(View.GONE);
                                }
                            });
                        } else {
                            parentActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    itiList.clear();
                                    itiAdapter.notifyDataSetChanged();

                                    noItiTV.setText(emptyTxt);
                                    noItiTV.setVisibility(View.VISIBLE);
                                }
                            });
                        }
                    }
                });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(itiListener != null){
                    itiListener.remove();
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.selectItinerary();
        parentActivity.setToolbarTitle(parentActivity.getString(R.string.itinerary_all));
    }
}
