package com.vidatechft.yupa.itinerary;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.GeoPoint;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.PlaceAutoCompleteAdapter;
import com.vidatechft.yupa.classes.Itinerary;
import com.vidatechft.yupa.classes.PlaceDetailsGoogleApi.PlaceAddress;
import com.vidatechft.yupa.classes.Plan;
import com.vidatechft.yupa.classes.PlanSuggestion;
import com.vidatechft.yupa.retrofitNetwork.UnifyPlaceAddress;
import com.vidatechft.yupa.utilities.Config;
import com.vidatechft.yupa.utilities.GeneralFunction;
import com.vidatechft.yupa.utilities.KeyboardUtilities;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Random;
import java.util.TimeZone;

import static android.app.Activity.RESULT_OK;

public class AddNewPlanFragment extends DialogFragment implements View.OnClickListener,
        TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener,
        GoogleApiClient.OnConnectionFailedListener,
        com.tsongkha.spinnerdatepicker.DatePickerDialog.OnDateSetListener,
        GoogleApiClient.ConnectionCallbacks{
    public static final String TAG = AddNewPlanFragment.class.getName();

    private MainActivity parentActivity;
    private Itinerary itinerary;
    private PlanSuggestion planSuggestion;
    private long day;
    private Plan oriPlan;
    private CheckBox taxiBookingCB,setReminderCB;
    private AppCompatEditText activityET;
    private AppCompatTextView startTimeTV;
    private AutoCompleteTextView locationATV;
    private ImageView activityCloseRightIV,locationCloseRightIV,startTimeCloseRightIV;
    private long startTime,reminderTime, reminderDateTime;
    private TimePickerDialog timePickerDialog;
    private DatePickerDialog datePickerDialog;
    private String placeId;
    private LinearLayout carLL,busLL,walkLL,hailLL;
    private TextView reminderDateTV, reminderTimeTV;
    private int button = 0;
    private LinearLayout dateTimeButtonLL;
    private Plan plan;
    private SimpleDateFormat datetimeFormat = new SimpleDateFormat("dd MMM yyyyhh:mma");
    private Boolean isNeedTaxi = null;
    private int eventId = 0;
    private GoogleApiClient googleApiClient;
    private int dobDay, dobMonth, dobYear, selectedHour, selectedMin;
    private SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public AddNewPlanFragment(){

    }

    public static AddNewPlanFragment newInstance(MainActivity parentActivity, Itinerary itinerary, long day, PlanSuggestion planSuggestion) {
        AddNewPlanFragment fragment = new AddNewPlanFragment();

        fragment.parentActivity = parentActivity;
        fragment.itinerary = itinerary;
        fragment.day = day;
        fragment.planSuggestion = planSuggestion;

        return fragment;
    }

    public static AddNewPlanFragment newInstance(MainActivity parentActivity, Itinerary itinerary, long day, Plan oriPlan) {
        AddNewPlanFragment fragment = new AddNewPlanFragment();

        fragment.parentActivity = parentActivity;
        fragment.itinerary = itinerary;
        fragment.day = day;
        fragment.oriPlan = oriPlan;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(true);
        getDialog().setCancelable(true);
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.rounded_white);
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        return inflater.inflate(R.layout.fragment_add_new_plan, container);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.showBtmNav();

                activityET = view.findViewById(R.id.activityET);
                locationATV = view.findViewById(R.id.locationATV);
                activityCloseRightIV = view.findViewById(R.id.activityCloseRightIV);
                locationCloseRightIV = view.findViewById(R.id.locationCloseRightIV);
                startTimeCloseRightIV = view.findViewById(R.id.startTimeCloseRightIV);
                startTimeTV = view.findViewById(R.id.startTimeTV);
                TextView confirmTV = view.findViewById(R.id.confirmTV);
                TextView cancelTV = view.findViewById(R.id.cancelTV);
                carLL = view.findViewById(R.id.carLL);
                busLL = view.findViewById(R.id.busLL);
                walkLL = view.findViewById(R.id.walkLL);
                hailLL = view.findViewById(R.id.hailLL);
                //CheckBox
                taxiBookingCB = view.findViewById(R.id.taxiBookingCB);
                setReminderCB = view.findViewById(R.id.setReminderCB);
                taxiBookingCB.setOnClickListener(AddNewPlanFragment.this);
                setReminderCB.setOnClickListener(AddNewPlanFragment.this);

                //TextView
                reminderDateTV = view.findViewById(R.id.reminderDateTV);
                reminderTimeTV = view.findViewById(R.id.reminderTimeTV);
                reminderDateTV.setOnClickListener(AddNewPlanFragment.this);
                reminderTimeTV.setOnClickListener(AddNewPlanFragment.this);
                //Constraint Layout
                dateTimeButtonLL = view.findViewById(R.id.dateTimeButtonLL);

                walkLL.setActivated(true);
                timePickerDialog = GeneralFunction.getTimePickerDialog(parentActivity,AddNewPlanFragment.this);
                datePickerDialog = GeneralFunction.getDatePickerDialog(parentActivity,AddNewPlanFragment.this);

                confirmTV.setOnClickListener(AddNewPlanFragment.this);
                cancelTV.setOnClickListener(AddNewPlanFragment.this);
                startTimeTV.setOnClickListener(AddNewPlanFragment.this);
                carLL.setOnClickListener(AddNewPlanFragment.this);
                busLL.setOnClickListener(AddNewPlanFragment.this);
                walkLL.setOnClickListener(AddNewPlanFragment.this);
                hailLL.setOnClickListener(AddNewPlanFragment.this);

                if(planSuggestion != null){
                    if(planSuggestion.activityName != null){
                        activityET.setText(planSuggestion.activityName);
                    }

//                    if(planSuggestion.time != null){
//                        startTime = planSuggestion.time.getTime();
//                        startTimeTV.setText(GeneralFunction.formatDateToTime(planSuggestion.time));
//                    }

                    if(planSuggestion.placeRecommendedResult != null && planSuggestion.placeRecommendedResult.name != null && planSuggestion.placeRecommendedResult.placeId != null && planSuggestion.placeRecommendedResult.geometry != null){
                        locationATV.setText(planSuggestion.placeRecommendedResult.name);
                        placeId = planSuggestion.placeRecommendedResult.placeId;
                    }

                }

                if(oriPlan != null){
                    populateOriPlan();
                }

                activityCloseRightIV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        activityET.setText("");
                    }
                });

                activityET.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        activityCloseIconHandler(s.toString());
                    }
                });

                startTimeCloseRightIV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(startTimeTV.getText().toString().isEmpty()){
                            button = 0;
//                            timePickerDialog.show();
                            showHourPicker();
                        }else{
                            startTimeTV.setText("");
                        }
                    }
                });

                startTimeTV.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        startTimeCloseIconHandler(s.toString());
                    }
                });

                setupAutoCompleteAdapter();
            }
        });

        activityCloseIconHandler(activityET.getText().toString());
        locationCloseIconHandler(locationATV.getText().toString());
        startTimeCloseIconHandler(startTimeTV.getText().toString());
    }

    private void setupAutoCompleteAdapter(){
        googleApiClient = new GoogleApiClient.Builder(parentActivity)
                .addApi(Places.GEO_DATA_API)
                .addConnectionCallbacks(parentActivity)
                .addOnConnectionFailedListener(AddNewPlanFragment.this)
                .build();
        MapsInitializer.initialize(parentActivity);
        googleApiClient.connect();

        final PlaceAutoCompleteAdapter placeAutoCompleteAdapter = new PlaceAutoCompleteAdapter(parentActivity, R.layout.adapter_place_auto_complete,
                googleApiClient, null, null);

        locationATV.setAdapter(placeAutoCompleteAdapter);

        locationATV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                final PlaceAutoCompleteAdapter.PlaceAutocomplete item = placeAutoCompleteAdapter.getItem(position);
                if(item == null){
                    Log.e(TAG, "Place Autocomplete is null");
                    return;
                }

                placeId = item.placeId.toString();
            }
        });

        locationATV.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                locationCloseIconHandler(s.toString());
            }
        });

        locationCloseRightIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locationATV.setText("");
            }
        });
    }

    private void activityCloseIconHandler(String s){
        if(s.isEmpty()){
            activityCloseRightIV.setImageResource(R.drawable.ic_activity);
        }else{
            activityCloseRightIV.setImageResource(R.drawable.ic_close_black_24dp);
        }
    }

    private void locationCloseIconHandler(String s){
        if(s.isEmpty()){
            locationCloseRightIV.setImageResource(R.drawable.ic_iti_gps);
            placeId = null;
        }else{
            locationCloseRightIV.setImageResource(R.drawable.ic_close_black_24dp);
        }
    }

    private void startTimeCloseIconHandler(String s){
        if(s.isEmpty()){
            startTimeCloseRightIV.setImageResource(R.drawable.ic_time);
            startTime = 0;
        }else{
            startTimeCloseRightIV.setImageResource(R.drawable.ic_close_black_24dp);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

        Log.v(TAG,connectionResult.toString());
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    private void populateOriPlan(){
        if(oriPlan == null){
            return;
        }

        if(oriPlan.activityName != null){
            activityET.setText(oriPlan.activityName);
        }

        if(oriPlan.placeAddress != null && oriPlan.placeAddress.placeName != null){
            locationATV.setText(oriPlan.placeAddress.placeName);
        }

        if(oriPlan.startTime != null){
            startTime = oriPlan.startTime;
            Calendar cal = new GregorianCalendar();
            cal.setTimeInMillis(startTime);
            startTimeTV.setText(GeneralFunction.formatDateToTime(cal.getTime()));
        }

        if(oriPlan.transportMode != null){
            switch (oriPlan.transportMode){
                case Plan.MODE_CAR:
                    activateCar();
                    break;
                case Plan.MODE_BUS:
                    activateBus();
                    break;
                case Plan.MODE_WALK:
                    activateWalk();
                    break;
                case Plan.MODE_HAIL:
                    activateHail();
                    break;
                default:
                    activateWalk();
                    break;
            }
        }

        if(oriPlan.isNeedTaxi != null && oriPlan.isNeedTaxi){
            taxiBookingCB.setChecked(true);
        }

        if(oriPlan.reminderDate != null && oriPlan.reminderDate > 0){
            setReminderCB.setChecked(true);
            reminderDateTV.setText(GeneralFunction.formatDateToString(oriPlan.reminderDate));
            reminderTimeTV.setText(GeneralFunction.formatDateToTimeFromMilisec(oriPlan.reminderDate));
            reminderDateTime = oriPlan.reminderDate;
            reminderTime = oriPlan.reminderDate;

            Animation fadeIn = AnimationUtils.loadAnimation(parentActivity, R.anim.fade_in);
            if (dateTimeButtonLL.getVisibility() == View.GONE) {
                dateTimeButtonLL.setVisibility(View.VISIBLE);
                dateTimeButtonLL.startAnimation(fadeIn);
            }
        }

    }

    private boolean isValidInput(){
        if(setReminderCB.isChecked() && dateTimeButtonLL.getVisibility() == View.VISIBLE){
            return  GeneralFunction.checkAndReturnString(activityET.getText().toString(),parentActivity.getString(R.string.itinerary_planning_err_activity),parentActivity)
                    &&
                    GeneralFunction.checkAndReturnString(reminderDateTV.getText().toString(),parentActivity.getString(R.string.itinerary_planning_err_reminder_date),parentActivity)
                    &&
                    GeneralFunction.checkAndReturnString(reminderTimeTV.getText().toString(),parentActivity.getString(R.string.itinerary_planning_err_reminder_time),parentActivity)
                    &&
                    GeneralFunction.checkAndReturnNum(startTime,parentActivity.getString(R.string.itinerary_planning_err_start_time),parentActivity);
        }
        else{
            return  GeneralFunction.checkAndReturnString(activityET.getText().toString(),parentActivity.getString(R.string.itinerary_planning_err_activity),parentActivity)
                    &&
                    GeneralFunction.checkAndReturnNum(startTime,parentActivity.getString(R.string.itinerary_planning_err_start_time),parentActivity);
        }

    }

    private void setPlanDetails(){
        String transportMode;

        if(carLL.isActivated()){
            transportMode = Plan.MODE_CAR;
        }else if(busLL.isActivated()){
            transportMode = Plan.MODE_BUS;
        }else if(walkLL.isActivated()){
            transportMode = Plan.MODE_WALK;
        }else if(hailLL.isActivated()){
            transportMode = Plan.MODE_HAIL;
        }else{
            transportMode = Plan.MODE_WALK;
        }

        isNeedTaxi = taxiBookingCB.isChecked();
        plan = new Plan();
        if(oriPlan != null){
            plan = oriPlan;
        }

        plan.activityName = activityET.getText().toString();
        plan.startTime = startTime;
        plan.transportMode = transportMode;
        plan.day = day;
        if(setReminderCB.isChecked() && dateTimeButtonLL.getVisibility() == View.VISIBLE) {
            try {
                plan.reminderDate = datetimeFormat.parse(reminderDateTV.getText().toString() + reminderTimeTV.getText().toString()).getTime();
            }
            catch (Exception e){
                Log.e(TAG,e.toString());
            }
        }
        plan.isNeedTaxi = isNeedTaxi;

        if(placeId != null){
            parentActivity.initializeLoadingDialog(parentActivity.getString(R.string.place_details_donwloading),false);
            parentActivity.controlLoadingDialog(true,parentActivity);

            new UnifyPlaceAddress(parentActivity, placeId, new UnifyPlaceAddress.OnUnifyComplete() {
                @Override
                public void onUnifySuccess(PlaceAddress placeAddress) {
                    parentActivity.controlLoadingDialog(false,parentActivity);
                    if(placeAddress != null){
                        plan.placeAddress = placeAddress;
                        plan.placeIndex = PlaceAddress.getPlaceIndex(placeAddress);
                        plan.gps = new GeoPoint(placeAddress.lat,placeAddress.lng);
                    }

                    continueSettingUpPlan();
                }

                @Override
                public void onUnifyFailed(Exception e) {
                    parentActivity.controlLoadingDialog(false,parentActivity);
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_occured_getting_place_details),R.drawable.notice_bad,TAG);
                    e.printStackTrace();
                }
            });
        }else{
            continueSettingUpPlan();
        }
    }

    private void continueSettingUpPlan(){
        DocumentReference ref;

        if(oriPlan != null){
            ref = GeneralFunction.getFirestoreInstance()
                    .collection(Itinerary.URL_ITINERARY)
                    .document(itinerary.id)
                    .collection(Plan.URL_PLAN)
                    .document(oriPlan.id);
        }else{
            ref = GeneralFunction.getFirestoreInstance()
                    .collection(Itinerary.URL_ITINERARY)
                    .document(itinerary.id)
                    .collection(Plan.URL_PLAN)
                    .document();
        }
        plan.id = ref.getId();

        if(setReminderCB.isChecked()){
            if(GeneralFunction.isCalendarPermitted(parentActivity.getApplicationContext(), parentActivity)){
                if(setCalendarReminder()){
                    setToDatabase(ref, true);
                }else{
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_failed_add_reminder_to_calendar),R.drawable.notice_bad,TAG);
                }
            }
        }else{
            setToDatabase(ref,false);
        }
    }

    private void setToDatabase(DocumentReference ref, boolean isSetReminder){
        parentActivity.initializeLoadingDialog(parentActivity.getString(R.string.itinerary_uploading_plan),true);
        parentActivity.controlLoadingDialog(true,parentActivity);
        if(isSetReminder){
            plan.eventId = eventId;
        }

        if(itinerary.quotationStatus != null && TextUtils.equals(itinerary.quotationStatus,Itinerary.STATUS_DONE)){
            itinerary.quotationStatus = null;
            itinerary.isFromAdmin = null;
            HashMap<String,Object> map = new HashMap<>();
            map.put(Itinerary.QUOTATION_STATUS,null);
            map.put(Itinerary.IS_FROM_ADMIN,null);
            GeneralFunction.getFirestoreInstance()
                    .collection(Itinerary.URL_ITINERARY)
                    .document(itinerary.id)
                    .update(map);
        }

        ref.set(plan)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        parentActivity.controlLoadingDialog(false,parentActivity);
                        if(task.isSuccessful()){
                            if(oriPlan != null){
                                dismiss();
                            }else{
                                clearEverything();
                            }
                            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.itinerary_upload_plan_success),R.drawable.notice_good,TAG);
                        }else{
                            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.itinerary_upload_plan_failed),R.drawable.notice_bad,TAG);
                            Log.e(TAG,String.valueOf(task.getException()));
                        }
                    }
                });
    }

    private void clearEverything(){
        activityET.setText("");
        locationATV.setText("");
        startTimeTV.setText("");
        taxiBookingCB.setChecked(false);
        setReminderCB.setChecked(false);
        reminderDateTV.setText("");
        reminderTimeTV.setText("");
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        Calendar cal = new GregorianCalendar();
        cal.set(Calendar.HOUR_OF_DAY,hourOfDay);
        cal.set(Calendar.MINUTE,minute);

        selectedHour = hourOfDay;
        selectedMin = minute;

        switch (button){
            case 0:
                cal.set(1994,11,13,hourOfDay,minute,0);
                startTime = cal.getTimeInMillis();
                startTimeTV.setText(GeneralFunction.formatDateToTime(cal.getTime()));
                break;
            case 1:
                reminderTime = cal.getTimeInMillis();
                reminderTimeTV.setText(GeneralFunction.formatDateToTime(cal.getTime()));
                break;
        }

    }

    private void activateCar(){
        carLL.setActivated(true);
        busLL.setActivated(false);
        walkLL.setActivated(false);
        hailLL.setActivated(false);
    }

    private void activateBus(){
        carLL.setActivated(false);
        busLL.setActivated(true);
        walkLL.setActivated(false);
        hailLL.setActivated(false);
    }

    private void activateWalk(){
        carLL.setActivated(false);
        busLL.setActivated(false);
        walkLL.setActivated(true);
        hailLL.setActivated(false);
    }

    private void activateHail(){
        carLL.setActivated(false);
        busLL.setActivated(false);
        walkLL.setActivated(false);
        hailLL.setActivated(true);
    }

    @Override
    public void onResume() {

        super.onResume();
        Window window = getDialog().getWindow();
//        window.setLayout(1000, 650);
        window.setGravity(Gravity.CENTER);
//        parentActivity.runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                parentActivity.setToolbarTitle(parentActivity.getString(R.string.itinerary_action_toolbar3));
//            }
//        });
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        KeyboardUtilities.hideSoftKeyboard(parentActivity);
    }

    @Override
    public void onDestroy() {
        if(googleApiClient != null){
            googleApiClient.disconnect();
        }
        super.onDestroy();
    }

    private void getCheckboxSelected(){
        if(setReminderCB.isChecked()){
            Animation fadeIn = AnimationUtils.loadAnimation(parentActivity, R.anim.fade_in);
            if (dateTimeButtonLL.getVisibility() == View.GONE) {
                dateTimeButtonLL.setVisibility(View.VISIBLE);
                dateTimeButtonLL.startAnimation(fadeIn);
            }
        }
        else{
            Animation fadeOut = AnimationUtils.loadAnimation(parentActivity, R.anim.fade_out);
            if (dateTimeButtonLL.getVisibility() == View.VISIBLE) {
                dateTimeButtonLL.startAnimation(fadeOut);
                fadeOut.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        dateTimeButtonLL.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

            }
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        dobDay = dayOfMonth;
        dobMonth = month + 1;
        dobYear = year;
        showSetDate(year,month,dayOfMonth);
    }

    public void showSetDate(int year,int month,int day) {
        Calendar cal = new GregorianCalendar();
        cal.set(year,month,day);
//        cal.set(Calendar.HOUR, hour)
        reminderDateTime = cal.getTime().getTime();
        reminderDateTV.setText(GeneralFunction.formatDateToString(cal.getTime().getTime()));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Config.REQUEST_CALENDAR_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && reminderDateTime > 0) {
                    setCalendarReminder();
                } else {
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.need_calendar_permission),R.drawable.notice_bad,TAG);
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private boolean setCalendarReminder(){
        if(plan == null || plan.activityName == null){
            return false;
        }

        ContentResolver cr = parentActivity.getContentResolver();
        ContentValues calEvent = new ContentValues();
        calEvent.put(CalendarContract.Events.CALENDAR_ID, 1); // XXX pick)
        if(plan.activityName != null){
            calEvent.put(CalendarContract.Events.TITLE, plan.activityName);
        }else{
            calEvent.put(CalendarContract.Events.TITLE, parentActivity.getString(R.string.itinerary_plan_route_unknown_activity_name));
        }
        calEvent.put(CalendarContract.Events.DTSTART, reminderTime);
        calEvent.put(CalendarContract.Events.DTEND, reminderTime + (60 * 1000));
        calEvent.put(CalendarContract.Events.HAS_ALARM, true);
        if(plan.placeAddress != null && plan.placeAddress.placeName != null){
            calEvent.put(CalendarContract.Events.EVENT_LOCATION,plan.placeAddress.placeName);
        }

//        calEvent.put(CalendarContract.Events.EVENT_TIMEZONE, CalendarContract.Calendars.CALENDAR_TIME_ZONE);//get utc time
        // found here https://stackoverflow.com/questions/15602311/adding-events-to-android-calendar-failing
        calEvent.put(CalendarContract.Events.EVENT_TIMEZONE, TimeZone.getDefault().getID());// get local date time


        //save an event
        //already checked permision
        @SuppressLint("MissingPermission") final Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, calEvent);

        if(uri == null || uri.getLastPathSegment() == null){
            return false;
        }
        int dbId = Integer.parseInt(uri.getLastPathSegment());

        //Now create a reminder and attach to the reminder
        ContentValues reminders = new ContentValues();
        reminders.put(CalendarContract.Reminders.EVENT_ID, dbId);
//        reminders.put(CalendarContract.Reminders.METHOD, CalendarContract.Reminders.METHOD_DEFAULT);
        reminders.put(CalendarContract.Reminders.METHOD, CalendarContract.Reminders.METHOD_ALERT);
        reminders.put(CalendarContract.Reminders.MINUTES, 0);
        eventId = dbId;
        //already checked permision
        @SuppressLint("MissingPermission") final Uri reminder = cr.insert(CalendarContract.Reminders.CONTENT_URI, reminders);


        if(reminder == null || reminder.getLastPathSegment() == null){
            return false;
        }
        int added = Integer.parseInt(reminder.getLastPathSegment());

        //this means reminder is added
        //show the reminder view in calendar to see
//        if(added > 0) {
//            Intent view = new Intent(Intent.ACTION_VIEW);
//            view.setData(uri); // enter the uri of the event not the reminder
//
//            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH){
//                view.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
//                        |Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP
//                        |Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_DOCUMENT);
//            }
//            else {
//                view.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
//                        Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP |
//                        Intent.FLAG_ACTIVITY_NO_HISTORY |
//                        Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
//            }
//            //view the event in calendar
//            startActivity(view);
//        }
        return added > 0;//return true if reminder is added
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.confirmTV:
                eventId = 0;
                if(!isValidInput()){
                    return;
                }
                setPlanDetails();
                break;
            case R.id.cancelTV:
//                parentActivity.onBackPressed();
                dismiss();
                break;
            case R.id.startTimeTV:
                button = 0;
//                timePickerDialog.show();
                showHourPicker();
                break;
            case R.id.carLL:
                activateCar();
                break;
            case R.id.busLL:
                activateBus();
                break;
            case R.id.walkLL:
                activateWalk();
                break;
            case R.id.hailLL:
                activateHail();
                break;
            case R.id.taxiBookingCB:
                break;
            case R.id.setReminderCB:
                getCheckboxSelected();
                break;
            case R.id.reminderDateTV:
//                datePickerDialog.show();
                Calendar cal = GeneralFunction.getDefaultUtcCalendar();
                showDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), R.style.DatePickerSpinner);
                break;
            case R.id.reminderTimeTV:
                button = 1;
//                timePickerDialog.show();
                showHourPicker();
                break;
        }
    }

    public void showHourPicker() {
        final Calendar myCalender = new GregorianCalendar();
        int hour = myCalender.get(Calendar.HOUR_OF_DAY);
        int minute = myCalender.get(Calendar.MINUTE);

        new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                if(view.isShown()){
                    myCalender.set(Calendar.HOUR_OF_DAY,hourOfDay);
                    myCalender.set(Calendar.MINUTE,minute);
                    switch (button){
                        case 0:
                            myCalender.set(1994,11,13,hourOfDay,minute,0);
                            startTime = myCalender.getTimeInMillis();
                            startTimeTV.setText(GeneralFunction.formatDateToTime(myCalender.getTime()));
                            break;
                        case 1:
                            reminderTime = myCalender.getTimeInMillis();
                            reminderTimeTV.setText(GeneralFunction.formatDateToTime(myCalender.getTime()));
                            break;
                    }
                }
            }
        };

        if(oriPlan != null && oriPlan.startTime != null && oriPlan.startTime > 0){
            Calendar tempCal = new GregorianCalendar();
            tempCal.setTimeInMillis(oriPlan.startTime);
            hour = tempCal.get(Calendar.HOUR_OF_DAY);
            minute = tempCal.get(Calendar.MINUTE);
        }

        if(selectedHour > 0 && selectedMin > 0){
            hour = selectedHour;
            minute = selectedMin;
        }

        TimePickerDialog timePickerDialog = new TimePickerDialog(parentActivity, android.R.style.Theme_Holo_Light_Dialog_NoActionBar, AddNewPlanFragment.this, hour, minute, false);
        if(timePickerDialog.getWindow() != null){
            timePickerDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            timePickerDialog.show();
        }
    }

    void showDate(int year, int monthOfYear, int dayOfMonth, int spinnerTheme) {
        if(dobDay > 0 && dobMonth > 0 && dobYear > 0){
            year = dobYear;
            monthOfYear = dobMonth - 1;
            dayOfMonth = dobDay;
        }
        new SpinnerDatePickerDialogBuilder()
                .context(parentActivity)
                .callback(AddNewPlanFragment.this)
                .spinnerTheme(spinnerTheme)
                .defaultDate(year, monthOfYear, dayOfMonth)
                .build()
                .show();
    }

    @Override
    public void onDateSet(com.tsongkha.spinnerdatepicker.DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        final Calendar calendar = new GregorianCalendar(year, monthOfYear, dayOfMonth);
        dobDay = dayOfMonth;
        dobMonth = monthOfYear + 1;
        dobYear = year;
        reminderDateTV.setText(format.format(calendar.getTime()));

        parentActivity.runOnUiThread(new Runnable() {
            public void run() {
                reminderDateTV.setText(format.format(calendar.getTime()));
                reminderTimeTV.requestFocus();
            }
        });
    }
}
