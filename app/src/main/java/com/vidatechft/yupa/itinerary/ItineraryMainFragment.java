package com.vidatechft.yupa.itinerary;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.ItineraryRecyclerGridAdapter;
import com.vidatechft.yupa.classes.Favourite;
import com.vidatechft.yupa.classes.Itinerary;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.Collections;

public class ItineraryMainFragment extends Fragment implements View.OnClickListener{
    public static final String TAG = ItineraryMainFragment.class.getName();
    private MainActivity parentActivity;

    private ArrayList<Itinerary> ownItiList = new ArrayList<>();
    private ArrayList<Itinerary> favItiList = new ArrayList<>();
    private RecyclerView ownItiRV,favItiRV;
    private ItineraryRecyclerGridAdapter ownItiAdapter,favItiAdapter;
    private TextView noOwnItiTV,noFavItiTV;

    private ListenerRegistration ownItiListener,favItiListener;

    private String loadingTxt,emptyTxt,errorTxt;
    private String loadingFavTxt,emptyFavTxt,errorFavTxt;
    private int favItiCount = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.itinerary_action_toolbar1));
            }
        });
    }

    public static ItineraryMainFragment newInstance(MainActivity parentActivity) {
        ItineraryMainFragment fragment = new ItineraryMainFragment();

        fragment.parentActivity = parentActivity;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_itinerary_main, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.showBtmNav();

                loadingTxt = parentActivity.getString(R.string.itinerary_loading);
                emptyTxt = parentActivity.getString(R.string.itinerary_empty);
                errorTxt = parentActivity.getString(R.string.itinerary_err_loading);

                loadingFavTxt = parentActivity.getString(R.string.itinerary_fav_loading);
                emptyFavTxt = parentActivity.getString(R.string.itinerary_fav_empty);
                errorFavTxt = parentActivity.getString(R.string.itinerary_fav_err_loading);

                TextView planTV = view.findViewById(R.id.planTV);
                ownItiRV = view.findViewById(R.id.ownItiRV);
                favItiRV = view.findViewById(R.id.favItiRV);
                noOwnItiTV = view.findViewById(R.id.noOwnItiTV);
                noFavItiTV = view.findViewById(R.id.noFavItiTV);

                planTV.setOnClickListener(ItineraryMainFragment.this);

                setUpGridAdapter();

                if(ownItiList.size() <= 0){
                    getOwnIti();
                }else{
                    noOwnItiTV.setVisibility(View.GONE);
                }

                if(favItiList.size() <= 0){
                    getFavIti();
                }else{
                    noFavItiTV.setVisibility(View.GONE);
                }
            }
        });

        return view;
    }

    private void setUpGridAdapter(){

        ownItiAdapter = new ItineraryRecyclerGridAdapter(parentActivity,this, ownItiList,true);

        ownItiRV.setNestedScrollingEnabled(false);
        ownItiRV.setAdapter(ownItiAdapter);
        ownItiRV.setLayoutManager(new GridLayoutManager(parentActivity, 2));

        //*******************************************************************************************************//

        favItiAdapter = new ItineraryRecyclerGridAdapter(parentActivity,this, favItiList,false,true);

        favItiRV.setNestedScrollingEnabled(false);
        favItiRV.setAdapter(favItiAdapter);
        favItiRV.setLayoutManager(new GridLayoutManager(parentActivity, 2));
    }

    private void getOwnIti(){
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                noOwnItiTV.setText(loadingTxt);
                noOwnItiTV.setVisibility(View.VISIBLE);
            }
        });

        ownItiListener = GeneralFunction.getFirestoreInstance()
                .collection(Itinerary.URL_ITINERARY)
                .whereEqualTo(Itinerary.USER_ID,parentActivity.uid)
                .orderBy(Itinerary.DATE_UPDATED, Query.Direction.DESCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot snapshots, @javax.annotation.Nullable final FirebaseFirestoreException e) {
                        if (e != null) {
                            parentActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.w(TAG, "Listen failed.", e);
                                    noOwnItiTV.setVisibility(View.VISIBLE);
                                    noOwnItiTV.setText(errorTxt);
                                }
                            });
                            return;
                        }

                        ownItiList.clear();

                        if (snapshots != null && !snapshots.isEmpty()) {
                            for (QueryDocumentSnapshot snapshot : snapshots) {
                                Itinerary tempIti = new Itinerary(parentActivity, snapshot);

                                //if he is paying for an itinerary and the payment is not done for 1 day already, will delete the itinerary
                                //got check status is null because if he directly buy from admin without requesting quotation, the status will be null
                                //if he request from admin, status wont be null and he can take his time to purchase the itinerary
                                //it will show something like "itinerary done, waiting for payment"
                                //this only applies for user purchasing an itinerary already pre made by the admins, doesnt apply for itinerary that is requesting for quotations
                                if(     tempIti.isFromAdmin != null &&
                                        tempIti.isFromAdmin &&
                                        tempIti.isPaid == null &&
                                        tempIti.paymentId == null &&
                                        tempIti.oriItiId != null &&
                                        tempIti.quotationStatus == null &&
                                        tempIti.dateCreated != null &&
                                        (tempIti.dateCreated.toDate().getTime() + (24 * 60 * 60 * 1000)) < System.currentTimeMillis() ){
                                    snapshot.getReference().delete();
                                }else{
                                    ownItiList.add(tempIti);
                                }
                            }

                            Collections.sort(ownItiList);
                            parentActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    noOwnItiTV.setVisibility(View.GONE);
                                    ownItiAdapter.notifyDataSetChanged();
                                }
                            });
                        } else {

                            //because firestore got cache so need to manually remove it
                            ownItiList.clear();
                            ownItiAdapter.notifyDataSetChanged();

                            noOwnItiTV.setVisibility(View.VISIBLE);
                            noOwnItiTV.setText(emptyTxt);
                        }
                    }
                });
    }

    private void getFavIti(){
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                noFavItiTV.setText(loadingFavTxt);
                noFavItiTV.setVisibility(View.VISIBLE);
            }
        });

        favItiListener = GeneralFunction.getFirestoreInstance()
                .collection(Favourite.URL_FAVOURITE)
                .whereEqualTo(Favourite.USER_ID,parentActivity.uid)
                .whereEqualTo(Favourite.TYPE,Favourite.FAV_TYPE_ITI)
                .whereEqualTo(Favourite.IS_LIKED, true)
                .orderBy(Favourite.DATE_UPDATED, Query.Direction.DESCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot snapshots, @javax.annotation.Nullable final FirebaseFirestoreException e) {
                        if (e != null) {
                            parentActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.w(TAG, "Listen failed.", e);
                                    noFavItiTV.setVisibility(View.VISIBLE);
                                    noFavItiTV.setText(errorFavTxt);
                                }
                            });
                            return;
                        }

                        favItiList.clear();

                        if (snapshots != null && !snapshots.isEmpty()) {
                            final int totalFavItiCount = snapshots.getDocuments().size();
                            for (QueryDocumentSnapshot snapshot : snapshots) {
                                final Favourite tempFavourite = Favourite.snapshotToFavourite(parentActivity,snapshot);

                                if(tempFavourite != null && tempFavourite.targetId != null && tempFavourite.isLiked){
                                    GeneralFunction.getFirestoreInstance()
                                            .collection(Itinerary.URL_ITINERARY)
                                            .document(tempFavourite.targetId)
                                            .get()
                                            .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                                @Override
                                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                    favItiCount++;

                                                    if(task.isSuccessful()){
                                                        Itinerary tempItinerary = new Itinerary(parentActivity,task.getResult());
                                                        tempItinerary.favourite = tempFavourite;
                                                        favItiList.add(tempItinerary);
                                                    }

                                                    if(favItiCount == totalFavItiCount){
                                                        favItiCount = 0;
                                                        parentActivity.runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                if(favItiList.isEmpty()){
                                                                    noFavItiTV.setText(emptyFavTxt);
                                                                    noFavItiTV.setVisibility(View.VISIBLE);
                                                                }else{
                                                                    noFavItiTV.setVisibility(View.GONE);
                                                                }
                                                            }
                                                        });
                                                        favItiAdapter.notifyDataSetChanged();
                                                    }
                                                }
                                            });
                                }else{
                                    favItiCount++;
                                }
                            }
                        } else {
                            //because firestore got cache so need to manually remove it
                            favItiList.clear();
                            favItiAdapter.notifyDataSetChanged();

                            noFavItiTV.setVisibility(View.VISIBLE);
                            noFavItiTV.setText(emptyFavTxt);
                        }
                    }
                });
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.itinerary_action_toolbar));
                parentActivity.selectItinerary();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(ownItiListener != null){
            ownItiListener.remove();
        }

        if(favItiListener != null){
            favItiListener.remove();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.planTV:
                Itinerary.goToManageItineraryOrPlan(parentActivity,null);
                break;
        }
    }
}
