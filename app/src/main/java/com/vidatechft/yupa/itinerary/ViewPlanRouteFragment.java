package com.vidatechft.yupa.itinerary;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.ui.IconGenerator;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.Plan;
import com.vidatechft.yupa.routeClasses.AbstractRouting;
import com.vidatechft.yupa.routeClasses.Route;
import com.vidatechft.yupa.routeClasses.RouteException;
import com.vidatechft.yupa.routeClasses.Routing;
import com.vidatechft.yupa.routeClasses.RoutingListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//copied the library codes from here https://github.com/jd-alexander/Google-Directions-Android
//that is inspired from here https://stackoverflow.com/questions/11745314/why-retrieving-google-directions-for-android-using-kml-data-is-not-working-anymo/11745316#11745316
//The current way is separate all the waypoints and call it 2 by 2 so will waste a lot of API calls if it has a lot of plans with GPS attached
//it can 1 shot calculate all the waypoints with 1 API call but then i dont know how to separate it locally and also it has maximum no of 8 waypoints per API call according to the Directions API documentation
//todo <DIRECTIONS API> that is why I want to save the routes into Shared Preferences and only recalculate the routes if there are any changes in that day's plan
//or try to use 1 shot call 1 query only and populates the whole route not call 1 by 1
//IT SEEMS LIKE THE NEW DIRECTIONS API WILL ONLY CHARGE HIGHER PRICE IF WAYPOINTS MORE THAN 10 (Maximum allowed number of waypoints per request: 25)
//in the documentation: A request to the Directions API or the Maps JavaScript API’s Directions Service that uses traffic information, more than 10 waypoints, and/or waypoints optimization. 0.01 USD per each
//from here https://developers.google.com/maps/documentation/directions/usage-and-billing?hl=en_US
public class ViewPlanRouteFragment extends Fragment implements OnMapReadyCallback,RoutingListener {
    public static final String TAG = ViewPlanRouteFragment.class.getName();
    private MainActivity parentActivity;
    private ArrayList<Plan> plans = new ArrayList<>();
    private int day;

    private GoogleMap googleMap;
    private ViewGroup infoWindow;
    private TextView activityDetailsTV;
    private ArrayList<Plan> plansWithGps = new ArrayList<>();
    List<LatLng> waypoints = new ArrayList<>();

    private List<Polyline> polylines = new ArrayList<>();
    private static final int[] colourResources = new int[]{R.color.pure_red,R.color.pure_lime,R.color.pure_yellow,R.color.pure_blue,R.color.pure_magenta,R.color.pure_aqua,R.color.pure_purple,R.color.pure_orange,R.color.pure_green,R.color.pure_pink};
    private final float POLYLINE_WIDTH_NORMAL = 15f;
    private final float POLYLINE_WIDTH_SELECTED = 20f;
    private final int Z_INDEX_LOWEST = 1;
    private final int Z_INDEX_MEDIUM = 2;
    private final int Z_INDEX_HIGHEST = 3;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static ViewPlanRouteFragment newInstance(MainActivity parentActivity, ArrayList<Plan> plans, int day) {
        ViewPlanRouteFragment fragment = new ViewPlanRouteFragment();

        fragment.parentActivity = parentActivity;
        fragment.plans = plans;
        fragment.day = day;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_view_plan_route, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.showBtmNav();

                SupportMapFragment mapFragment = (SupportMapFragment) ViewPlanRouteFragment.this.getChildFragmentManager()
                        .findFragmentById(R.id.viewPlanRouteGoogleMap);
                mapFragment.getMapAsync(ViewPlanRouteFragment.this);
            }
        });

        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;

        this.googleMap.getUiSettings().setMapToolbarEnabled(false);

//        if(polylines.size()>0) {
//            for (Polyline poly : polylines) {
//                poly.remove();
//            }
//        }
//        polylines = new ArrayList<>();

        if(polylines.size() <= 0){
            populateMap();
            setupInfoWindow();
        }

    }

    private void populateMap(){
        if(googleMap == null){
            return;
        }else{
            googleMap.clear();
        }

        if(plans == null || plans.isEmpty()){
            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.itinerary_plan_route_no_plans),R.drawable.notice_bad,TAG);
            return;
        }

        plansWithGps.clear();
        for(Plan plan : plans){
            if(plan.gps != null){
                plansWithGps.add(plan);
            }
        }

        if(plansWithGps.isEmpty()){
            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.itinerary_plan_route_no_plans_with_gps),R.drawable.notice_bad,TAG);
            return;
        }

        waypoints.clear();
        for(Plan planWithGps : plansWithGps){
            waypoints.add(new LatLng(planWithGps.gps.getLatitude(),planWithGps.gps.getLongitude()));
        }

        if(waypoints.isEmpty() || waypoints.size() <= 1){
            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.itinerary_plan_route_no_plans_with_gps),R.drawable.notice_bad,TAG);
            return;
        }

        parentActivity.initializeLoadingDialog(parentActivity.getString(R.string.itinerary_plan_route_loading_route),true);
        parentActivity.controlLoadingDialog(true,parentActivity);

        LatLngBounds.Builder builder = LatLngBounds.builder();
        for(int i = 0; i < waypoints.size(); i++){
            // -1 is because it is executed in pair(like [0,1],[1,2]...) so the last one has already been used by the last second index, so no need to run again
            if(i < waypoints.size() - 1){
                Routing routing = new Routing.Builder()
                        .travelMode(AbstractRouting.TravelMode.DRIVING)
                        .withListener(this)
                        .alternativeRoutes(true)
                        .waypoints(waypoints.get(i),waypoints.get(i + 1))
                        .key(parentActivity.getString(R.string.google_geo_api_key))
                        .indexId(i)
                        .build();
                routing.execute();
            }

            builder.include(waypoints.get(i));
        }

        googleMap.setOnPolylineClickListener(new GoogleMap.OnPolylineClickListener() {
            @Override
            public void onPolylineClick(Polyline polyline) {
                handlePolylineThickness(polyline);
                if(polyline.getTag() != null && !polyline.getTag().toString().isEmpty()){
                    Toast.makeText(parentActivity,polyline.getTag().toString(),Toast.LENGTH_LONG).show();
                }
            }
        });

        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                try{
                    int polylineId = Integer.parseInt(marker.getSnippet());
                    if(polylineId >= 0){
                        handlePolylineThickness(polylines.get(polylineId));
                    }
                }catch (Exception e){
                    Log.e(TAG,e.getMessage());
                }
                return false;
            }
        });

        final LatLngBounds bounds = builder.build();
        googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
    }

    private void handlePolylineThickness(Polyline polyline){
        for(Polyline tempPolyline : polylines){
            tempPolyline.setZIndex(Z_INDEX_LOWEST);
            tempPolyline.setWidth(POLYLINE_WIDTH_NORMAL);
        }
        polyline.setZIndex(Z_INDEX_MEDIUM);
        polyline.setWidth(POLYLINE_WIDTH_SELECTED);
    }

    private void setupInfoWindow(){
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                infoWindow = (ViewGroup)getLayoutInflater().inflate(R.layout.info_window_google_map_marker_for_iti_plans_route, null);
                activityDetailsTV = infoWindow.findViewById(R.id.activityDetailsTV);

                ViewPlanRouteFragment.this.googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                    @Override
                    public View getInfoWindow(final Marker marker) {
                        //the whole code in getInfoContents can be put inside here too (to eliminate the original paddings)
                        return null;
                    }

                    @Override
                    public View getInfoContents(final Marker marker) {
                        parentActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                activityDetailsTV.setText(marker.getTitle());
                            }
                        });

                        if(marker != null){
                            return infoWindow;
                        }else{
                            return null;
                        }
                    }
                });
            }
        });
    }

    @Override
    public void onRoutingFailure(RouteException e) {
        // The Routing request failed
        parentActivity.controlLoadingDialog(false,parentActivity);
        if(e != null) {
            GeneralFunction.openMessageNotificationDialog(parentActivity,"Error: " + e.getMessage(),R.drawable.notice_bad,TAG);
        }else {
            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_occured),R.drawable.notice_bad,TAG);
        }
    }

    @Override
    public void onRoutingStart() {
        // The Routing Request starts
    }

    @Override
    public void onRoutingCancelled() {
        Log.i(TAG, "Routing was cancelled.");
    }

    @Override
    public void onRoutingSuccess(List<Route> route, int shortestRouteIndex, int indexId) {
        parentActivity.controlLoadingDialog(false,parentActivity);

        //In case the routes is more than the colours's size
        int colorIndex = indexId % colourResources.length;

        String polylineTag = "";
        if(plansWithGps.get(indexId).placeAddress != null && plansWithGps.get(indexId).placeAddress.placeName != null && plansWithGps.get(indexId + 1) != null && plansWithGps.get(indexId + 1).placeAddress != null && plansWithGps.get(indexId + 1).placeAddress.placeName != null){
            polylineTag = String.format(parentActivity.getString(R.string.itinerary_plan_route_place_to_place),
                    plansWithGps.get(indexId).placeAddress.placeName,
                    plansWithGps.get(indexId + 1).placeAddress.placeName);
        }

        PolylineOptions polyOptions = new PolylineOptions();
        int routeColour;

        //if more than the size of colour, then use generated random colour instead
        if(indexId >= colourResources.length){
            routeColour = GeneralFunction.generateRandomColor(parentActivity, colourResources[colorIndex]);
        }else{
            routeColour = ContextCompat.getColor(parentActivity, colourResources[colorIndex]);
        }

        polyOptions.color(routeColour);
        polyOptions.width(POLYLINE_WIDTH_NORMAL);
        polyOptions.addAll(route.get(shortestRouteIndex).getPoints());
        Polyline polyline = googleMap.addPolyline(polyOptions);
        polyline.setClickable(true);
        polyline.setTag(polylineTag);
        polylines.add(polyline);

        if(indexId == 0){
            int lastIndexId = waypoints.size() - 1;
            int lastColorIndex = lastIndexId % colourResources.length;
            int lastMarkerColour;
            if(lastIndexId >= colourResources.length){
                lastMarkerColour = GeneralFunction.generateRandomColor(parentActivity, colourResources[lastColorIndex]);
            }else{
                lastMarkerColour = ContextCompat.getColor(parentActivity, colourResources[lastColorIndex]);
            }
            addNewMarker(lastIndexId,-1, lastMarkerColour);//this is for the final destination's marker
        }
        addNewMarker(indexId,polylines.size() - 1, routeColour);
    }

    private void addNewMarker(int indexId, int polylineId, int routeColour){
        Bitmap icon;
        try{
            int px = parentActivity.getResources().getDimensionPixelSize(R.dimen.extra_larger_padding);
            View markerView = ((LayoutInflater) parentActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.tv_plan_route_custom_marker, null);
            markerView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            markerView.layout(0, 0, px, px);
            markerView.setPadding(0,20,0,0);
            markerView.buildDrawingCache();
            TextView numberTV = markerView.findViewById(R.id.numberTV);
            numberTV.setBackground(GeneralFunction.getTintedRandomDrawable(parentActivity,R.drawable.circle_shape_for_map_marker,routeColour));
            Bitmap mDotMarkerBitmap = Bitmap.createBitmap(px, px, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(mDotMarkerBitmap);
            numberTV.setText(String.valueOf(indexId + 1));
            markerView.draw(canvas);
            icon = mDotMarkerBitmap;
        }catch (Exception e){
            Log.e(TAG,e.getMessage());
            TextView textView = new TextView(parentActivity);
            textView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            textView.setPadding(20,5,20,5);
            textView.setTextColor(Color.WHITE);
            textView.setText(String.valueOf(indexId + 1));
            IconGenerator generator = new IconGenerator(parentActivity);
            generator.setBackground(GeneralFunction.getTintedRandomDrawable(parentActivity,R.drawable.circle_shape_for_map_marker,routeColour));
            generator.setContentView(textView);
            icon = generator.makeIcon();
        }

        Date date = new Date();
        Plan tempPlan = plansWithGps.get(indexId);
        if(tempPlan.startTime != null){
            date.setTime(plansWithGps.get(indexId).startTime);
        }else{
            date.setTime(System.currentTimeMillis());
        }

        String activityName = parentActivity.getString(R.string.itinerary_plan_route_unknown_activity_name);
        if(tempPlan.activityName != null){
            activityName = tempPlan.activityName;
        }

        String locationName = parentActivity.getString(R.string.itinerary_plan_route_unknown_activity_location);
        if(tempPlan.placeAddress != null && tempPlan.placeAddress.placeName != null){
            locationName = tempPlan.placeAddress.placeName;
        }

        MarkerOptions markerOptions;
        markerOptions = new MarkerOptions().position(waypoints.get(indexId)).icon(BitmapDescriptorFactory.fromBitmap(icon));
        markerOptions.title(String.format(parentActivity.getString(R.string.itinerary_plan_route_info_window_details),GeneralFunction.formatDateToTime(date),activityName,locationName));
        markerOptions.snippet(String.valueOf(polylineId));
        markerOptions.zIndex(Z_INDEX_HIGHEST);

        googleMap.addMarker(markerOptions);
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(String.format(parentActivity.getString(R.string.itinerary_plan_route_toolbar),day + 1));
                parentActivity.selectItinerary();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        MapFragment f = (MapFragment) parentActivity.getFragmentManager()
                .findFragmentById(R.id.viewPlanRouteGoogleMap);
        if (f != null)
            parentActivity.getFragmentManager().beginTransaction().remove(f).commit();
    }
}
