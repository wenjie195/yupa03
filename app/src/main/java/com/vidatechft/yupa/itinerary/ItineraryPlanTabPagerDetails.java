package com.vidatechft.yupa.itinerary;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.ItineraryPlanRecyclerAdapter;
import com.vidatechft.yupa.adapter.ItineraryPlanRecyclerDetailsAdapter;
import com.vidatechft.yupa.classes.Itinerary;
import com.vidatechft.yupa.classes.Plan;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class ItineraryPlanTabPagerDetails extends Fragment{
    public static final String TAG = ItineraryPlanTabPagerDetails.class.getName();
    private MainActivity parentActivity;
    private Fragment parentFragment;
    private Itinerary itinerary;
    private int day;
    private ArrayList<Plan> plans = new ArrayList<>();

    private RecyclerView planRV;
    private ItineraryPlanRecyclerDetailsAdapter planAdapter;

    private SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static ItineraryPlanTabPagerDetails newInstance(MainActivity parentActivity, Fragment parentFragment, Itinerary itinerary, int day, ArrayList<Plan> plans) {
        ItineraryPlanTabPagerDetails fragment = new ItineraryPlanTabPagerDetails();

        fragment.parentActivity = parentActivity;
        fragment.parentFragment = parentFragment;
        fragment.itinerary = itinerary;
        fragment.day = day;
        fragment.plans = plans;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_itinerary_plan_day_pager_details, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                planRV = view.findViewById(R.id.planRV);
                TextView noPlanTV = view.findViewById(R.id.noPlanTV);

                if(plans != null && !plans.isEmpty()){
                    noPlanTV.setVisibility(View.GONE);
                }else{
                    noPlanTV.setVisibility(View.VISIBLE);
                }

                setupAdapter();
            }
        });

        return view;
    }

    private void setupAdapter(){
        planAdapter = new ItineraryPlanRecyclerDetailsAdapter(parentActivity,parentFragment,itinerary,day,plans);

        setupRecyclerView();
    }

    private void setupRecyclerView(){
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(parentActivity);
        planRV.setNestedScrollingEnabled(false);
        planRV.setAdapter(planAdapter);
        planRV.setLayoutManager(linearLayoutManager);
    }

    public void onDatasetChanged(ArrayList<Plan> plans){
        this.plans = plans;
    }

}
