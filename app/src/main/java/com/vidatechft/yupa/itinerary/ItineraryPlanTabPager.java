package com.vidatechft.yupa.itinerary;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FieldValue;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.ItineraryPlanRecyclerAdapter;
import com.vidatechft.yupa.adapter.PlanSuggestionRecyclerAdapter;
import com.vidatechft.yupa.classes.Itinerary;
import com.vidatechft.yupa.classes.PlaceDetailsGoogleApi.PlaceAddress;
import com.vidatechft.yupa.classes.PlaceDetailsGoogleApi.PlacesDetailsJson;
import com.vidatechft.yupa.classes.PlaceSearchRecommendedGoogleApi.PlaceRecommendedResult;
import com.vidatechft.yupa.classes.PlaceSearchRecommendedGoogleApi.PlaceSearchRecommendedResult;
import com.vidatechft.yupa.classes.Plan;
import com.vidatechft.yupa.classes.PlanSuggestion;
import com.vidatechft.yupa.paymentFragment.SelectTravelKitFragment;
import com.vidatechft.yupa.retrofitNetwork.PlaceAddressCallback;
import com.vidatechft.yupa.retrofitNetwork.RetrofitInstance;
import com.vidatechft.yupa.retrofitNetwork.SearchRecommendedPlaceCallback;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

public class ItineraryPlanTabPager extends Fragment implements View.OnClickListener{
    public static final String TAG = ItineraryPlanTabPager.class.getName();
    private MainActivity parentActivity;
    private ItineraryPlansContainerFragment parentFragment;
    private Itinerary itinerary;
    private int day;
    private ArrayList<Plan> plans = new ArrayList<>();

    private RecyclerView planRV;
    private ItineraryPlanRecyclerAdapter planAdapter;

    private View touchToCloseOptionsView;
    private ScrollView optionsSV;
    private AppCompatImageView optionsIV;
    private ConstraintLayout quotationCL,buyNowCL,addPlanCL,suggestionsCL,routeCL;
    private TextView quotationTV,buyNowTV;
    private AppCompatImageView quotationIV;

    //for quotation details
    private LinearLayout quotationDetailLL;
    private TextView quotationPriceTV,quotationTravelDateTV;

    private SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");

    private BroadcastReceiver broadcastReceiver;
    private boolean isEditing = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static ItineraryPlanTabPager newInstance(MainActivity parentActivity, ItineraryPlansContainerFragment parentFragment, Itinerary itinerary, int day, ArrayList<Plan> plans) {
        ItineraryPlanTabPager fragment = new ItineraryPlanTabPager();

        fragment.parentActivity = parentActivity;
        fragment.parentFragment = parentFragment;
        fragment.itinerary = itinerary;
        fragment.day = day;
        fragment.plans = plans;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_itinerary_plan_day_pager, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.showBtmNav();

                planRV = view.findViewById(R.id.planRV);
                TextView dateTV = view.findViewById(R.id.dateTV);
                TextView planDescTV = view.findViewById(R.id.planDescTV);
                ImageView arrowIV = view.findViewById(R.id.arrowIV);

                optionsIV = view.findViewById(R.id.optionsIV);
                optionsSV = view.findViewById(R.id.optionsSV);
                addPlanCL = view.findViewById(R.id.addPlanCL);
                suggestionsCL = view.findViewById(R.id.suggestionsCL);
                quotationCL = view.findViewById(R.id.quotationCL);
                quotationTV = view.findViewById(R.id.quotationTV);
                quotationIV = view.findViewById(R.id.quotationIV);
                routeCL = view.findViewById(R.id.routeCL);
                buyNowCL = view.findViewById(R.id.buyNowCL);
                buyNowTV = view.findViewById(R.id.buyNowTV);
                touchToCloseOptionsView = view.findViewById(R.id.touchToCloseOptionsView);

                //quotation details
                setupQuotationDetails(view);

                int plansWithGpsCount = 0;
                for(Plan tempPlan : plans){
                    if(tempPlan != null && tempPlan.gps != null){
                        plansWithGpsCount++;
                    }
                }

                if(plansWithGpsCount >= 2){
                    routeCL.setVisibility(View.VISIBLE);
                }else{
                    routeCL.setVisibility(View.GONE);
                }

                if(plans != null && !plans.isEmpty()){
                    planDescTV.setVisibility(View.GONE);
                    arrowIV.setVisibility(View.GONE);
                }else{
                    planDescTV.setVisibility(View.VISIBLE);
                    arrowIV.setVisibility(View.GONE);//todo because changed ui and the arrow will make ui weird, so let it gone for now
                }

                if(itinerary.departDate != null){
                    Calendar cal = new GregorianCalendar();
                    cal.setTimeInMillis(itinerary.departDate);
                    cal.add(Calendar.DATE, day);
                    dateTV.setText(format.format(cal.getTime()));
                }

                if(isEditing){
                    setupAdapter(true);
                }else{
                    setupAdapter(false);
                }

                broadcastReceiver = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, final Intent intent) {
                        if (intent != null && intent.getAction() != null && intent.getAction().equals(Plan.BROADCAST_ID)) {
                            String actionType = intent.getStringExtra(Plan.INTENT_EDIT_PLAN);

                            switch (actionType){
                                case Plan.INTENT_VALUE_SAVE:
                                    isEditing = false;
                                    planAdapter.setEditingMode(false);
                                    planAdapter.notifyDataSetChanged();
                                    break;
                                case Plan.INTENT_VALUE_EDIT:
                                    isEditing = true;
                                    planAdapter.setEditingMode(true);
                                    planAdapter.notifyDataSetChanged();
                                    break;
                            }
                        }
                    }
                };

                quotationIV.setImageDrawable(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.ic_direction,R.color.black));

                if( (itinerary.quotationStatus != null && TextUtils.equals(itinerary.quotationStatus,Itinerary.STATUS_PENDING)) || (itinerary.isPaid != null && itinerary.isPaid) || itinerary.paymentId != null ){
                    onQuotationSent(true);
                }else{
                    onQuotationSent(false);
                }

                if(itinerary.quotationStatus != null && TextUtils.equals(itinerary.quotationStatus,Itinerary.STATUS_DONE) ){
                    if((itinerary.isPaid != null && !itinerary.isPaid)){
                        addPlanCL.setVisibility(View.GONE);
                        suggestionsCL.setVisibility(View.GONE);
                        onQuotationSent(true);
                    }

                    buyNowCL.setVisibility(View.VISIBLE);
                    quotationCL.setBackground(parentActivity.getResources().getDrawable(R.drawable.square_black_border_btm));
                }

                String convertedLocalPrice = GeneralFunction.getItineraryPrice(parentActivity,itinerary,null);
                if(!convertedLocalPrice.isEmpty()){
                    convertedLocalPrice = buyNowTV.getText().toString() + " (" + convertedLocalPrice + ")";
                    buyNowTV.setText(convertedLocalPrice);
                }

                dateTV.setOnClickListener(ItineraryPlanTabPager.this);
                touchToCloseOptionsView.setOnClickListener(ItineraryPlanTabPager.this);
                optionsIV.setOnClickListener(ItineraryPlanTabPager.this);
                optionsSV.setOnClickListener(ItineraryPlanTabPager.this);
                addPlanCL.setOnClickListener(ItineraryPlanTabPager.this);
                suggestionsCL.setOnClickListener(ItineraryPlanTabPager.this);
                quotationCL.setOnClickListener(ItineraryPlanTabPager.this);
                routeCL.setOnClickListener(ItineraryPlanTabPager.this);
                buyNowCL.setOnClickListener(ItineraryPlanTabPager.this);
            }
        });

        return view;
    }

    private void setupQuotationDetails(View view){
        quotationDetailLL = view.findViewById(R.id.quotationDetailLL);
        quotationPriceTV = view.findViewById(R.id.quotationPriceTV);
        quotationTravelDateTV = view.findViewById(R.id.quotationTravelDateTV);

        if(itinerary != null && itinerary.currencyCode != null && itinerary.price != null && itinerary.isFromAdmin != null && itinerary.isFromAdmin){

        }else{
            quotationDetailLL.setVisibility(View.GONE);
            return;
        }

        if(itinerary.departDate != null && itinerary.returnDate != null){
            String travelDate = GeneralFunction.formatDateToString(itinerary.departDate) + " - " + GeneralFunction.formatDateToString(itinerary.returnDate);
            quotationTravelDateTV.setText(travelDate);
        }

        quotationPriceTV.setText(GeneralFunction.getItineraryPrice(parentActivity,itinerary,null));

        quotationDetailLL.setVisibility(View.VISIBLE);
    }

    public void disableBuyNowButton(){
        itinerary.quotationStatus = null;
        itinerary.isFromAdmin = null;
    }

    public void onDatasetChanged(ArrayList<Plan> plans){
        this.plans = plans;
    }

    private void setupAdapter(boolean isEditing){
        planAdapter = new ItineraryPlanRecyclerAdapter(parentActivity,this,itinerary,day,plans,isEditing);

        setupRecyclerView();
    }

    private void setupRecyclerView(){
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(parentActivity);
        planRV.setAdapter(planAdapter);
        planRV.setLayoutManager(linearLayoutManager);
    }

    private boolean handleOnBackPressed(boolean isBackPressed){
        if(optionsSV.getVisibility() == View.VISIBLE){
            optionsSV.setVisibility(View.GONE);
            touchToCloseOptionsView.setVisibility(View.GONE);
            optionsIV.setImageDrawable(parentActivity.getResources().getDrawable(R.drawable.ic_hamburger));
        }else{
            if(isBackPressed){
                return true;
            }else{
                optionsSV.setVisibility(View.VISIBLE);
                touchToCloseOptionsView.setVisibility(View.VISIBLE);
                optionsIV.setImageDrawable(parentActivity.getResources().getDrawable(R.drawable.baseline_close_white_18dp));
            }
        }
        return false;
    }

    private void onQuotationSent(boolean alreadySent){
        if(parentActivity.getThisUser() != null && parentActivity.getThisUser().isAdmin != null && parentActivity.getThisUser().isAdmin){
            quotationCL.setVisibility(View.GONE);
            if(routeCL.getVisibility() == View.GONE){
                suggestionsCL.setBackgroundColor(parentActivity.getResources().getColor(R.color.white));
            }else{
                routeCL.setBackgroundColor(parentActivity.getResources().getColor(R.color.white));
            }
            return;
        }

        if(alreadySent){
            quotationCL.setEnabled(false);
            quotationTV.setTypeface(null, Typeface.ITALIC);
            quotationTV.setTextColor(parentActivity.getResources().getColor(R.color.default_hint));
            quotationTV.setText(parentActivity.getString(R.string.itinerary_trip_quotation_sent));

            quotationIV.setImageDrawable(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.ic_direction,R.color.default_hint));

            buyNowCL.setVisibility(View.GONE);
            quotationCL.setBackgroundColor(parentActivity.getResources().getColor(R.color.white));
        }else{
            quotationCL.setEnabled(true);
            quotationTV.setText(parentActivity.getString(R.string.itinerary_trip_quotation));
            quotationTV.setTypeface(null, Typeface.NORMAL);
            quotationTV.setTextColor(parentActivity.getResources().getColor(R.color.black));

            quotationIV.setImageDrawable(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.ic_direction,R.color.black));
        }
    }

    private void requestQuotation(){
        HashMap<String,Object> quotationMap = new HashMap<>();
        quotationMap.put(Itinerary.IS_FROM_ADMIN,true);
        quotationMap.put(Itinerary.QUOTATION_STATUS,Itinerary.STATUS_PENDING);
        quotationMap.put(Itinerary.DATE_QUOTATION_REQUESTED, FieldValue.serverTimestamp());
        //todo dateQuotationRequested and dateQuotationAction and quotationRemark need to have both in app and the web
        //todo need think can submit again bo even after already replied by admin.
        //todo if can, is it only for when isPaid == null, if already paid, cannot request already, need make another 1
        //todo in web, must have tripname and price set, because if the itinerary created from app, it wont have price and might not have trip names. so if trip names == null get from locationName

        GeneralFunction.getFirestoreInstance()
                .collection(Itinerary.URL_ITINERARY)
                .document(itinerary.id)
                .update(quotationMap)
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        e.printStackTrace();
                        GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.itinerary_trip_quotation_failed),R.drawable.notice_bad,TAG);

                        onQuotationSent(false);
                    }
                });

        Toast.makeText(parentActivity,parentActivity.getString(R.string.itinerary_trip_quotation_sent),Toast.LENGTH_LONG).show();

        onQuotationSent(true);

        handleOnBackPressed(false);
    }

    @Override
    public void onResume() {
        if(broadcastReceiver != null){
            IntentFilter intentFilter = new IntentFilter(Plan.BROADCAST_ID);
            parentActivity.registerReceiver(broadcastReceiver, intentFilter);
        }

        //this code is to intercept on back pressed for this fragment //from here https://stackoverflow.com/questions/7992216/android-fragment-handle-back-button-press
        if(getView() != null){
            getView().setFocusableInTouchMode(true);
            getView().requestFocus();
            getView().setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey( View v, int keyCode, KeyEvent event ) {
                    if( event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_BACK ) {
                        if(parentFragment.hideSuggestionView(true) && handleOnBackPressed(true)){
                            parentActivity.onBackPressed();
                        }
                        return true;
                    }
                    return false;
                }
            });
        }

        super.onResume();
    }

    @Override
    public void onPause() {
        if(broadcastReceiver != null){
            parentActivity.unregisterReceiver(broadcastReceiver);
        }
        super.onPause();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.touchToCloseOptionsView:
                handleOnBackPressed(false);
                break;
            case R.id.optionsIV:
                handleOnBackPressed(false);
                break;
            case R.id.addPlanCL:
                handleOnBackPressed(false);
                if(parentActivity != null && !parentActivity.isFinishing() && GeneralFunction.isAppInForground(parentActivity.getApplicationContext())){
                    final FragmentManager fm = parentActivity.getSupportFragmentManager();
                    PlanSuggestion planSuggestion = null;
                    AddNewPlanFragment addNewPlanFragment = AddNewPlanFragment.newInstance(parentActivity,itinerary, day, planSuggestion);
                    addNewPlanFragment.show(fm, TAG);
                }
                break;
            case R.id.suggestionsCL:
                handleOnBackPressed(false);
                if(!parentFragment.showSuggestionView(day)){
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.itinerary_trip_suggestions_empty),R.drawable.notice_bad,TAG);
                }
                break;
            case R.id.routeCL:
                handleOnBackPressed(false);
                Fragment viewPlanRouteFragment = ViewPlanRouteFragment.newInstance(parentActivity,plans,day);
                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, viewPlanRouteFragment).addToBackStack(ViewPlanRouteFragment.TAG).commit();
                break;
            case R.id.quotationCL:
                if(itinerary != null && itinerary.id != null){
                    android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(parentActivity);
                    alertDialogBuilder.setTitle(parentActivity.getString(R.string.itinerary_trip_quotation_title));
                    alertDialogBuilder.setMessage(parentActivity.getString(R.string.itinerary_trip_quotation_desc));
                    alertDialogBuilder.setPositiveButton(parentActivity.getString(R.string.btn_yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            requestQuotation();
                            dialog.dismiss();
                        }
                    });

                    alertDialogBuilder.setNegativeButton(parentActivity.getString(R.string.btn_no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    android.support.v7.app.AlertDialog alertDialog  = alertDialogBuilder.create();
                    if( !( parentActivity.isFinishing() && alertDialog.isShowing() ) ){
                        alertDialog.show();
                    }
                }else{
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_occured),R.drawable.notice_bad,TAG);
                }

                break;
            case R.id.buyNowCL:
                if(itinerary.quotationStatus == null && itinerary.isFromAdmin == null){
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.itinerary_buy_now_error),R.drawable.notice_bad,TAG);
                    return;
                }

                Fragment selectTravelKitFragment = SelectTravelKitFragment.newInstance(parentActivity,null,itinerary,null);
                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, selectTravelKitFragment).addToBackStack(SelectTravelKitFragment.TAG).commit();
                break;
        }
    }
}
