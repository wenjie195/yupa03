package com.vidatechft.yupa.itinerary;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.gson.Gson;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.CheckboxRecyclerGridAdapter;
import com.vidatechft.yupa.adapter.ItineraryRecyclerGridAdapter;
import com.vidatechft.yupa.classes.Itinerary;
import com.vidatechft.yupa.utilities.Config;
import com.vidatechft.yupa.utilities.GeneralFunction;
import com.vidatechft.yupa.utilities.KeyboardUtilities;
import com.vidatechft.yupa.utilities.TagEditText;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

public class StartPlanItineraryMoreDetailsFragment extends Fragment implements View.OnClickListener{
    public static final String TAG = StartPlanItineraryMoreDetailsFragment.class.getName();
    private MainActivity parentActivity;
    private String lastToolbarTitle;
    private Itinerary oriItinerary;

    private Itinerary editedItinerary;
    private EditText minBudgetET,maxBudgetET,descET;
    private LinearLayout minBudgetLL,maxBudgetLL;
    private TextView minCurrencyTV,maxCurrencyTV;
    private CheckboxRecyclerGridAdapter cuisineCBAdapter, activityCBAdapter;

    private String currencyCode;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static StartPlanItineraryMoreDetailsFragment newInstance(MainActivity parentActivity, String lastToolbarTitle, Itinerary oriItinerary) {
        StartPlanItineraryMoreDetailsFragment fragment = new StartPlanItineraryMoreDetailsFragment();

        fragment.parentActivity = parentActivity;
        fragment.lastToolbarTitle = lastToolbarTitle;
        fragment.oriItinerary = oriItinerary;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_start_plan_itinerary_more_details, container, false);

        if(oriItinerary.currencyCode != null && !oriItinerary.currencyCode.isEmpty()){
            currencyCode = oriItinerary.currencyCode;
        }else{
            currencyCode = GeneralFunction.getCurrencyCode(parentActivity);
        }

        parentActivity.runOnUiThread(new Runnable() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public void run() {
                parentActivity.showBtmNav();

                TextView nextTV = view.findViewById(R.id.nextTV);
                minBudgetET = view.findViewById(R.id.minBudgetET);
                maxBudgetET = view.findViewById(R.id.maxBudgetET);
                minBudgetLL = view.findViewById(R.id.minBudgetLL);
                maxBudgetLL = view.findViewById(R.id.maxBudgetLL);
                minCurrencyTV = view.findViewById(R.id.minCurrencyTV);
                maxCurrencyTV = view.findViewById(R.id.maxCurrencyTV);
                descET = view.findViewById(R.id.descET);
                RecyclerView cuisineRV = view.findViewById(R.id.cuisineRV);
                RecyclerView activityRV = view.findViewById(R.id.activityRV);

                minCurrencyTV.setText(currencyCode);
                maxCurrencyTV.setText(currencyCode);
                setupCuisineList(cuisineRV);
                setupActivityList(activityRV);

                nextTV.setOnClickListener(StartPlanItineraryMoreDetailsFragment.this);
                minBudgetLL.setOnClickListener(StartPlanItineraryMoreDetailsFragment.this);
                maxBudgetLL.setOnClickListener(StartPlanItineraryMoreDetailsFragment.this);
                descET.setOnTouchListener(new View.OnTouchListener() {

                    public boolean onTouch(View v, MotionEvent event) {
                        if (descET.hasFocus()) {
                            v.getParent().requestDisallowInterceptTouchEvent(true);
                            switch (event.getAction() & MotionEvent.ACTION_MASK){
                                case MotionEvent.ACTION_SCROLL:
                                    v.getParent().requestDisallowInterceptTouchEvent(false);
                                    return true;
                            }
                        }
                        return false;
                    }
                });

                populateOriDetails();
            }
        });

        return view;
    }

    private void setupCuisineList(RecyclerView cuisineRV){
        ArrayList<CheckBox> cuisineCBs = new ArrayList<>();
        final String[] cuisineList = parentActivity.getResources().getStringArray(R.array.cuisine_list);
        for(String cuisine : cuisineList){
            CheckBox checkBox = (CheckBox) CheckBox.inflate(parentActivity,R.layout.custom_checkbox,null);
            checkBox.setText(cuisine.trim());
            cuisineCBs.add(checkBox);
        }
        cuisineCBAdapter = new CheckboxRecyclerGridAdapter(parentActivity,cuisineCBs);

        cuisineRV.setNestedScrollingEnabled(false);
        cuisineRV.setAdapter(cuisineCBAdapter);
        cuisineRV.setLayoutManager(new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL));
    }

    private void setupActivityList(RecyclerView activityRV){
        ArrayList<CheckBox> activityCBs = new ArrayList<>();
        final String[] activityList = parentActivity.getResources().getStringArray(R.array.editProfileHobby);
        for(String activity : activityList){
            CheckBox checkBox = (CheckBox) CheckBox.inflate(parentActivity,R.layout.custom_checkbox,null);
            checkBox.setText(activity.trim());
            activityCBs.add(checkBox);
        }
        activityCBAdapter = new CheckboxRecyclerGridAdapter(parentActivity,activityCBs);

        activityRV.setNestedScrollingEnabled(false);
        activityRV.setAdapter(activityCBAdapter);
        activityRV.setLayoutManager(new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL));
    }

    private void populateOriDetails(){
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Itinerary populateIti = null;
                if(oriItinerary != null){
                    populateIti = oriItinerary;
                }

                if(editedItinerary != null){
                    populateIti = editedItinerary;
                }

                if(populateIti != null){

                    if(populateIti.currencyCode != null && !populateIti.currencyCode.trim().isEmpty()){
                        minCurrencyTV.setText(populateIti.currencyCode);
                        maxCurrencyTV.setText(populateIti.currencyCode);
                    }

                    if(populateIti.minBudget != null){
                        String minBudgetStr = String.valueOf(populateIti.minBudget.intValue());
                        minBudgetET.setText(minBudgetStr);
                    }

                    if(populateIti.maxBudget != null){
                        String maxBudgetStr = String.valueOf(populateIti.maxBudget.intValue());
                        maxBudgetET.setText(maxBudgetStr);
                    }

                    if(populateIti.cuisineList != null && !populateIti.cuisineList.isEmpty()){
                        for(String cuisine : populateIti.cuisineList){
                            for(int i = 0; i < cuisineCBAdapter.getCheckBoxes().size(); i++){
                                CheckBox cuisineCB = cuisineCBAdapter.getCheckBoxes().get(i);
                                if(TextUtils.equals(cuisineCB.getText().toString().trim(),cuisine.trim())){
                                    cuisineCBAdapter.getCheckBoxes().get(i).setChecked(true);
                                    cuisineCBAdapter.notifyItemChanged(i);
                                }
                            }
                        }
                    }

                    if(populateIti.activityList != null && !populateIti.activityList.isEmpty()){
                        for(String activity : populateIti.activityList){
                            for(int i = 0; i < activityCBAdapter.getCheckBoxes().size(); i++){
                                CheckBox activityCB = activityCBAdapter.getCheckBoxes().get(i);
                                if(TextUtils.equals(activityCB.getText().toString().trim(),activity.trim())){
                                    activityCBAdapter.getCheckBoxes().get(i).setChecked(true);
                                    activityCBAdapter.notifyItemChanged(i);
                                }
                            }
                        }
                    }

                    if(populateIti.specialRequest != null){
                        descET.setText(populateIti.specialRequest);
                    }
                }
            }
        });
    }

    private boolean isValidInput(){
        if(oriItinerary == null){
            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.itinerary_err_set_detail),R.drawable.notice_bad,TAG);
            return false;
        }

        if(minBudgetET.getText().toString().trim().isEmpty()){
            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.itinerary_err_budget_min),R.drawable.notice_bad,TAG);
            return false;
        }

        if(maxBudgetET.getText().toString().trim().isEmpty()){
            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.itinerary_err_budget_max),R.drawable.notice_bad,TAG);
            return false;
        }

        return GeneralFunction.compareAndReturnNum(convertTextIntoDouble(minBudgetET.getText().toString()),convertTextIntoDouble(maxBudgetET.getText().toString()),parentActivity.getString(R.string.itinerary_err_budget_range),parentActivity);
    }

    private Double convertTextIntoDouble(String text){
        try{
            return Double.parseDouble(text);
        }catch (Exception e){
            return 0.0;
        }
    }

    private void setItineraryDetails(){
        if(!isValidInput()){
            return;
        }

        final HashMap<String,Object> itiMap = Itinerary.toMap(oriItinerary,false);

        itiMap.put(Itinerary.CURRENCY_CODE,currencyCode);
        itiMap.put(Itinerary.MIN_BUDGET,convertTextIntoDouble(minBudgetET.getText().toString()));
        itiMap.put(Itinerary.MAX_BUDGET,convertTextIntoDouble(maxBudgetET.getText().toString()));

        for(CheckBox cuisineCB : cuisineCBAdapter.getCheckBoxes()){
            if(cuisineCB != null && cuisineCB.isChecked()){
                itiMap.put(cuisineCB.getText().toString().trim(),true);
            }
        }

        for(CheckBox activityCB : activityCBAdapter.getCheckBoxes()){
            if(activityCB != null && activityCB.isChecked()){
                itiMap.put(activityCB.getText().toString().trim(),true);
            }
        }

        itiMap.put(Itinerary.SPECIAL_REQUEST, descET.getText().toString());

        if(oriItinerary.dateCreated == null){
            itiMap.put(Itinerary.DATE_CREATED, FieldValue.serverTimestamp());
        }

        itiMap.put(Itinerary.DATE_UPDATED, FieldValue.serverTimestamp());

        GeneralFunction.getFirestoreInstance()
                .collection(Itinerary.URL_ITINERARY)
                .document(oriItinerary.id)
                .set(itiMap);

        itiMap.put(Itinerary.ID, oriItinerary.id);
        oriItinerary = Itinerary.toClass(itiMap,parentActivity,oriItinerary.planList);

        Fragment itineraryPlansContainerFragment = ItineraryPlansContainerFragment.newInstance(parentActivity,oriItinerary);
        itineraryPlansContainerFragment.setTargetFragment(this, Config.INTENT_ITI_MORE_DETAIL_TO_CONTAINER);
        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, itineraryPlansContainerFragment).addToBackStack(ItineraryPlansContainerFragment.TAG).commit();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode==Config.INTENT_ITI_MORE_DETAIL_TO_CONTAINER && resultCode== Activity.RESULT_OK) {
            Gson gson = new Gson();
            String itiFromContainer = data.getStringExtra(Config.ITINERARY_INTENT_ID);
            oriItinerary = gson.fromJson(itiFromContainer, Itinerary.class);

            populateOriDetails();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        editedItinerary = new Itinerary();
        editedItinerary.minBudget = convertTextIntoDouble(minBudgetET.getText().toString());
        editedItinerary.maxBudget = convertTextIntoDouble(maxBudgetET.getText().toString());

        ArrayList<String> cuisineList = new ArrayList<>();
        for(CheckBox cuisineCB : cuisineCBAdapter.getCheckBoxes()){
            if(cuisineCB != null && cuisineCB.isChecked()){
                cuisineList.add(cuisineCB.getText().toString().trim());
            }
        }
        editedItinerary.cuisineList = cuisineList;

        ArrayList<String> activityList = new ArrayList<>();
        for(CheckBox activityCB : activityCBAdapter.getCheckBoxes()){
            if(activityCB != null && activityCB.isChecked()){
                activityList.add(activityCB.getText().toString().trim());
            }
        }
        editedItinerary.activityList = activityList;

        editedItinerary.specialRequest = descET.getText().toString();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if(getTargetFragment() != null){
            getTargetFragment().onActivityResult(
                    getTargetRequestCode(),
                    Activity.RESULT_OK,
                    new Intent().putExtra(Config.ITINERARY_INTENT_ID, (new Gson()).toJson(oriItinerary))
            );
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.setToolbarTitle(lastToolbarTitle);
        parentActivity.selectItinerary();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.nextTV:
                setItineraryDetails();
                break;
            case R.id.minBudgetLL:
                minBudgetET.performClick();
                minBudgetET.requestFocus();
                KeyboardUtilities.showSoftKeyboard(parentActivity,minBudgetET);
                minBudgetET.setSelection(minBudgetET.length());
                break;
            case R.id.maxBudgetLL:
                maxBudgetET.performClick();
                maxBudgetET.requestFocus();
                KeyboardUtilities.showSoftKeyboard(parentActivity,maxBudgetET);
                maxBudgetET.setSelection(maxBudgetET.length());
                break;
        }
    }
}
