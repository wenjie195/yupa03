package com.vidatechft.yupa.itinerary;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.ItineraryPlanTabPagerAdapter;
import com.vidatechft.yupa.adapter.PlanSuggestionRecyclerAdapter;
import com.vidatechft.yupa.classes.Itinerary;
import com.vidatechft.yupa.classes.PlaceDetailsGoogleApi.PlaceAddress;
import com.vidatechft.yupa.classes.PlaceSearchRecommendedGoogleApi.PlaceRecommendedResult;
import com.vidatechft.yupa.classes.PlaceSearchRecommendedGoogleApi.PlaceSearchRecommendedResult;
import com.vidatechft.yupa.classes.Plan;
import com.vidatechft.yupa.classes.PlanSuggestion;
import com.vidatechft.yupa.retrofitNetwork.RetrofitInstance;
import com.vidatechft.yupa.retrofitNetwork.SearchRecommendedPlaceCallback;
import com.vidatechft.yupa.utilities.Config;
import com.vidatechft.yupa.utilities.GeneralFunction;
import com.vidatechft.yupa.utilities.KeyboardUtilities;

import java.util.ArrayList;
import java.util.Date;

public class ItineraryPlansContainerFragment extends Fragment implements View.OnClickListener{
    public static final String TAG = ItineraryPlansContainerFragment.class.getName();
    private MainActivity parentActivity;
    private Itinerary itinerary;

    private ItineraryPlanTabPagerAdapter adapter;
    private ViewPager viewPager;
    private EditText tripET;
    private TextView tripTV;
    private LinearLayout editLL;
    private ImageView saveIV;
    private TabLayout tabLayout;

    private ListenerRegistration listenerRegistration;
    private SparseArray<ArrayList<Plan>> planList = new SparseArray<>();

    private boolean isEditing = false;

    private int tabPosition = 0;

    //suggestion
    public ConstraintLayout suggestionCL;
    private ImageView suggestionCloseIV;
    private RecyclerView suggestionRV;
    private PlanSuggestionRecyclerAdapter planSuggestionRecyclerAdapter;

    //to fix sometimes plan is empty after editing the original 1. this fix is to redownload the plan again only
    private Handler planHandler;
    private Runnable planRunnable;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static ItineraryPlansContainerFragment newInstance(MainActivity parentActivity, Itinerary itinerary) {
        ItineraryPlansContainerFragment fragment = new ItineraryPlansContainerFragment();

        fragment.parentActivity = parentActivity;
        fragment.itinerary = itinerary;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_itinerary_plan_dashboard_container, container, false);
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.showBtmNav();
                tabLayout = view.findViewById(R.id.tabLayout);
                editLL = view.findViewById(R.id.editLL);
                saveIV = view.findViewById(R.id.saveIV);
                ImageView editIV = view.findViewById(R.id.editIV);
                ImageView shareIV = view.findViewById(R.id.shareIV);
                tripET = view.findViewById(R.id.tripET);
                tripTV = view.findViewById(R.id.tripTV);
                viewPager = view.findViewById(R.id.pager);
                ImageView bgIV = view.findViewById(R.id.bgIV);

                //suggestion
                suggestionCL = view.findViewById(R.id.suggestionCL);
                suggestionCloseIV = view.findViewById(R.id.suggestionCloseIV);
                suggestionRV = view.findViewById(R.id.suggestionRV);

                if(itinerary != null){
                    if(itinerary.itineraryPicUrl != null){
                        //todo change the carousel2 drawable
                        GeneralFunction.GlideImageSettingOwnPlaceholder(parentActivity, Uri.parse(itinerary.itineraryPicUrl),bgIV,R.drawable.bg_carousel2);
                    }else{
                        GeneralFunction.GlideImageSettingOwnDrawable(parentActivity,R.drawable.bg_carousel2,bgIV);
                    }

                    if(itinerary.tripName != null){
                        tripET.setText(itinerary.tripName);
                        tripTV.setText(itinerary.tripName);
                    }else{
                        String title = GeneralFunction.getItineraryTitle(itinerary);
                        if(title != null){
                            tripET.setText(title);
                            tripTV.setText(title);
                        }else{
                            tripET.setText("???");
                            tripTV.setText("???");
                        }
                    }

                    if(itinerary.noofDays != null){
                        for(int day = 0; day < itinerary.noofDays; day++){
                            tabLayout.addTab(tabLayout.newTab().setText(parentActivity.getString(R.string.itinerary_trip_day) + " " + String.valueOf(day + 1)));
                        }

                        if(planList != null && planList.size() > 0 && adapter != null){
                            setupViewPager();
                        }else{
                            getAllItineraryPlans();
                        }
                    }

                }

                shareIV.setImageDrawable(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.ic_share,R.color.black));
                saveIV.setOnClickListener(ItineraryPlansContainerFragment.this);
                editIV.setOnClickListener(ItineraryPlansContainerFragment.this);
                shareIV.setOnClickListener(ItineraryPlansContainerFragment.this);
            }
        });
        setupPlanSuggestionFeature();
        return view;
    }

    public boolean showSuggestionView(int day){
        if(planSuggestionRecyclerAdapter == null || planSuggestionRecyclerAdapter.getItemCount() <= 0){
            return false;
        }

        planSuggestionRecyclerAdapter.setDay(day);
        suggestionCL.setVisibility(View.VISIBLE);
        return true;
    }

    public boolean hideSuggestionView(boolean isBackPressed){
        if(suggestionCL.getVisibility() != View.VISIBLE && isBackPressed){
            return true;
        }else{
            suggestionCL.setVisibility(View.GONE);
            return false;
        }
    }

    private void setupPlanSuggestionFeature(){
        final String[] planQueries = parentActivity.getResources().getStringArray(R.array.planSuggestions);
        final String[] planQueriesActivityName = parentActivity.getResources().getStringArray(R.array.planSuggestionsActivityName);
        final int queryPosition = GeneralFunction.generateRandomNumber(0,planQueries.length);

        final ArrayList<PlanSuggestion> fullSuggestion = new ArrayList<>();
        final ArrayList<PlanSuggestion> suggestions = new ArrayList<>();

        //this add null was to force the first one to allow user to create his own custom plan previously rather than using suggested places. Not used anymore
//        fullSuggestion.add(null);
//        suggestions.add(null);

        planSuggestionRecyclerAdapter = new PlanSuggestionRecyclerAdapter(parentActivity,suggestions,itinerary,0);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(parentActivity);
        linearLayoutManager.setOrientation(LinearLayout.VERTICAL);
        suggestionRV.setAdapter(planSuggestionRecyclerAdapter);
        suggestionRV.setLayoutManager(linearLayoutManager);

        String query = String.format(planQueries[queryPosition],"Japan");
        if(itinerary != null && itinerary.placeAddressList != null && itinerary.placeAddressList.get(0) != null){
            PlaceAddress placeAddress = PlaceAddress.getMapToClass(itinerary.placeAddressList.get(0));
            query = String.format(planQueries[queryPosition],PlaceAddress.getCombinedStates(placeAddress));
        }else{
            if(itinerary != null && itinerary.locationName != null){
                query = String.format(planQueries[queryPosition],itinerary.locationName);
            }
        }

        RetrofitInstance.searchRecommendedPlaceResultQueryCall(parentActivity,query)
                .enqueue(new SearchRecommendedPlaceCallback<PlaceSearchRecommendedResult>() {
                    @Override
                    public void onSearchPlaceSuccess(ArrayList<PlaceRecommendedResult> recommendedPlaces) {
                        for(PlaceRecommendedResult recommendedPlace : recommendedPlaces){
                            if(recommendedPlace != null && recommendedPlace.placeId != null){
                                PlanSuggestion planSuggestion = new PlanSuggestion();
                                planSuggestion.placeRecommendedResult = recommendedPlace;
                                if(planQueriesActivityName[queryPosition] != null){
                                    planSuggestion.activityName = String.format(planQueriesActivityName[queryPosition],recommendedPlace.name);
                                }else{
                                    planSuggestion.activityName = String.format(planQueriesActivityName[0],recommendedPlace.name);
                                }

                                fullSuggestion.add(planSuggestion);
                            }
                        }
                        suggestions.clear();
                        suggestions.addAll(fullSuggestion);

                        planSuggestionRecyclerAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onSearchPlaceFailure(Exception e) {
//                        GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.plan_suggestion_error_occured_getting_place_details),R.drawable.notice_bad,TAG);
                        e.printStackTrace();
                    }
                });

        suggestionCL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSuggestionView(false);
            }
        });

        suggestionCloseIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSuggestionView(false);
            }
        });
    }

    private void getAllItineraryPlans(){
        initializePlanList();

        parentActivity.initializeLiveLoadingDialog(parentActivity.getString(R.string.itinerary_plan_loading),false);
        parentActivity.controlLiveLoadingDialog(true,parentActivity);

        listenerRegistration = GeneralFunction.getFirestoreInstance()
                                .collection(Itinerary.URL_ITINERARY)
                                .document(itinerary.id)
                                .collection(Plan.URL_PLAN)
                                .orderBy(Plan.START_TIME)
                                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                                    @Override
                                    public void onEvent(@javax.annotation.Nullable QuerySnapshot snapshots, @javax.annotation.Nullable final FirebaseFirestoreException e) {
                                        parentActivity.controlLiveLoadingDialog(false,parentActivity);
                                        if (e != null) {
                                            parentActivity.runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Log.w(TAG, "Listen failed.", e);
                                                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.itinerary_plan_err_loading),R.drawable.notice_bad,TAG);
                                                }
                                            });
                                            return;
                                        }

                                        initializePlanList();

                                        if (snapshots != null && !snapshots.isEmpty()) {
                                            for (QueryDocumentSnapshot snapshot : snapshots) {
                                                Plan plan = Plan.snapshotToPlan(parentActivity,snapshot);
                                                if(plan != null && plan.day != null && itinerary.noofDays > plan.day ){
                                                    planList.get(plan.day.intValue()).add(plan);
                                                }
                                            }

                                            for (DocumentChange dc : snapshots.getDocumentChanges()) {
                                                switch (dc.getType()) {
                                                    case ADDED:

                                                    case MODIFIED:
                                                        if(adapter != null){
                                                            setupViewPager();//needs this to update the view
                                                        }else{
                                                            setupAdapter();
                                                        }
                                                        break;
                                                    case REMOVED:

                                                        break;
                                                }
                                            }
                                        }else{
                                            planHandler = new Handler();
                                            planRunnable = new Runnable() {
                                                public void run() {
                                                    redownloadPlanFromOriIti();
                                                }
                                            };
                                            planHandler.postDelayed(planRunnable,2000);
                                            setupAdapter();
                                        }
                                    }
                                });
    }

    private void redownloadPlanFromOriIti(){
        if(itinerary == null || itinerary.oriItiId == null){
            return;
        }

        GeneralFunction.getFirestoreInstance()
                .collection(Itinerary.URL_ITINERARY)
                .document(itinerary.oriItiId)
                .collection(Plan.URL_PLAN)
                .orderBy(Plan.START_TIME)
                .get()
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Listen failed.", e);
                        GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.itinerary_plan_err_loading),R.drawable.notice_bad,TAG);
                    }
                })
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot snapshots) {
                        if (snapshots != null && !snapshots.isEmpty()) {
                            CollectionReference plansRef = GeneralFunction.getFirestoreInstance()
                                    .collection(Itinerary.URL_ITINERARY)
                                    .document(itinerary.id)
                                    .collection(Plan.URL_PLAN);

                            for (QueryDocumentSnapshot snapshot : snapshots) {
                                Plan plan = Plan.snapshotToPlan(parentActivity,snapshot);
                                if(plan != null && plan.day != null && itinerary.noofDays > plan.day ){
                                    plan.dateCreated = null;
                                    plansRef.document(plan.id).set(plan);
                                }
                            }
                        }
                    }
                });
    }

    private void initializePlanList(){
        planList.clear();
        for(int day = 0; day < itinerary.noofDays; day++){
            planList.put(day,new ArrayList<Plan>());
        }
    }

    private void setupAdapter(){
        if (!isAdded()) return;
        //to solve Fragment has not been attached yet.

        itinerary.planList = planList;
        adapter = new ItineraryPlanTabPagerAdapter(parentActivity,ItineraryPlansContainerFragment.this,ItineraryPlansContainerFragment.this.getChildFragmentManager(), tabLayout.getTabCount(),itinerary,planList);

        setupViewPager();
    }

    private void setupViewPager(){

        if(itinerary.noofDays.intValue() > 7){
            viewPager.setOffscreenPageLimit(6);
        }else{
            viewPager.setOffscreenPageLimit(itinerary.noofDays.intValue() - 1);
        }

        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                tabPosition = tab.getPosition();
                if(planSuggestionRecyclerAdapter != null){
                    planSuggestionRecyclerAdapter.setDay(tabPosition);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        viewPager.setCurrentItem(tabPosition);
    }

    private void onEditingState(){
        Intent intent2 = new Intent();
        intent2.putExtra(Plan.INTENT_EDIT_PLAN,Plan.INTENT_VALUE_EDIT);
        intent2.setAction(Plan.BROADCAST_ID);
        parentActivity.sendBroadcast(intent2);

        editLL.setVisibility(View.GONE);
        saveIV.setVisibility(View.VISIBLE);

        tripET.setVisibility(View.VISIBLE);
        tripET.requestFocus();
    }

    private void onBackToNormalState(){
        Intent intent = new Intent();
        intent.putExtra(Plan.INTENT_EDIT_PLAN,Plan.INTENT_VALUE_SAVE);
        intent.setAction(Plan.BROADCAST_ID);
        parentActivity.sendBroadcast(intent);

        editLL.setVisibility(View.VISIBLE);
        saveIV.setVisibility(View.GONE);

        tripTV.setText(tripET.getText().toString());
        tripET.setVisibility(View.GONE);
        tripET.clearFocus();
        KeyboardUtilities.hideSoftKeyboard(parentActivity);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(listenerRegistration != null){
            listenerRegistration.remove();
        }

        if(getTargetFragment() != null){
            getTargetFragment().onActivityResult(
                    getTargetRequestCode(),
                    Activity.RESULT_OK,
                    new Intent().putExtra(Config.ITINERARY_INTENT_ID, (new Gson()).toJson(itinerary))
            );
        }

        if(planHandler != null && planRunnable != null){
            planHandler.removeCallbacks(planRunnable);
        }
    }

    @Override
    public void onResume() {
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.itinerary_action_toolbar2));
            }
        });

        if(isEditing){
            Intent intent2 = new Intent();
            intent2.putExtra(Plan.INTENT_EDIT_PLAN,Plan.INTENT_VALUE_EDIT);
            intent2.setAction(Plan.BROADCAST_ID);
            parentActivity.sendBroadcast(intent2);

            editLL.setVisibility(View.GONE);
            saveIV.setVisibility(View.VISIBLE);

            tripET.setVisibility(View.VISIBLE);
            tripET.requestFocus();
        }

        super.onResume();
    }

    private String generateShareContentFromPlans(){
        StringBuilder shareContent = new StringBuilder();

        String title = GeneralFunction.getItineraryTitle(itinerary);
        if(title == null){
            title = parentActivity.getString(R.string.itinerary_action_toolbar1);
        }

        shareContent.append(title);
        addNewLine(2,shareContent);

        if(itinerary.noofDays != null){
            for(int i = 0; i < itinerary.noofDays; i++){
                shareContent.append(parentActivity.getString(R.string.itinerary_trip_day));
                shareContent.append(" ");
                shareContent.append(i + 1);
                shareContent.append(": ");
                shareContent.append("\n");
                if(planList.get(i) != null && !planList.get(i).isEmpty()){
                    for(Plan tempPlan : planList.get(i)){
                        String activityName = "???";
                        String time = "00:00am";
                        if(tempPlan.activityName != null){
                            activityName = tempPlan.activityName;
                        }

                        if(tempPlan.startTime != null){
                            Date date = new Date();
                            date.setTime(tempPlan.startTime);
                            time = GeneralFunction.formatDateToTime(date);
                        }

                        shareContent.append(time);
                        shareContent.append(" - ");
                        shareContent.append(activityName);

                        if(tempPlan.placeAddress != null && tempPlan.placeAddress.placeName != null){
                            shareContent.append(" @ ");
                            shareContent.append(tempPlan.placeAddress.placeName);
                        }

                        addNewLine(1,shareContent);
                    }

                    shareContent.append("\n");
                }else{
                    shareContent.append(parentActivity.getString(R.string.empty));
                    addNewLine(2,shareContent);
                }
            }
        }else{
            shareContent.append(parentActivity.getString(R.string.empty));
        }

        try{
            ClipboardManager clipboard = (ClipboardManager) parentActivity.getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("Itinerary Plan Content", shareContent.toString());
            if(clipboard != null){
                clipboard.setPrimaryClip(clip);
            }
            Toast.makeText(parentActivity,parentActivity.getString(R.string.itinerary_copied_plan),Toast.LENGTH_LONG).show();
        }catch (Exception e){
            Log.e(TAG,"error copying string to clipboard");
        }

        return shareContent.toString();
    }

    private void addNewLine(int noofTimes, StringBuilder sb){
        for(int i = 0; i < noofTimes; i++){
            sb.append("\n");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.saveIV:
                isEditing = false;
                if(tripET.getText().toString().trim().isEmpty()){
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.itinerary_err_trip),R.drawable.notice_bad,TAG);
                }else{
                    onBackToNormalState();

                    GeneralFunction.getFirestoreInstance()
                            .collection(Itinerary.URL_ITINERARY)
                            .document(itinerary.id)
                            .update(Itinerary.TRIP_NAME, tripET.getText().toString());
                    itinerary.tripName = tripET.getText().toString();
                }
                break;
            case R.id.editIV:
                isEditing = true;
                onEditingState();
                break;
            case R.id.shareIV:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, generateShareContentFromPlans());
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.share_to)));
                break;
        }
    }
}