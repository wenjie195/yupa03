package com.vidatechft.yupa.itinerary;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatRadioButton;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;
import com.theartofdev.edmodo.cropper.CropImage;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.PlaceAutoCompleteAdapter;
import com.vidatechft.yupa.classes.Itinerary;
import com.vidatechft.yupa.classes.PlaceDetailsGoogleApi.PlaceAddress;
import com.vidatechft.yupa.classes.Plan;
import com.vidatechft.yupa.classes.Search;
import com.vidatechft.yupa.retrofitNetwork.UnifyPlaceAddress;
import com.vidatechft.yupa.utilities.Config;
import com.vidatechft.yupa.utilities.FilePath;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;

import static android.app.Activity.RESULT_OK;
import static com.vidatechft.yupa.utilities.GeneralFunction.GlideImageSetting;

public class StartPlanItineraryFragment extends Fragment implements View.OnClickListener,
        DatePickerDialog.OnDateSetListener,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks{
    public static final String TAG = StartPlanItineraryFragment.class.getName();
    private MainActivity parentActivity;
    private Itinerary oriItinerary;

    private TextView departureTV,returnTV;
    private AutoCompleteTextView locationNameATV;
    private AppCompatRadioButton privateRB,publicRB;
    private ImageButton uploadCoverIB;

    private boolean departClicked, returnClicked;
    private long departDate, returnDate;
    private String filePath;

    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");

    private GoogleApiClient googleApiClient;
    private String placeId;

    private DocumentReference ref;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.itinerary_action_toolbar2));
            }
        });
    }

    public static StartPlanItineraryFragment newInstance(MainActivity parentActivity, Itinerary oriItinerary) {
        StartPlanItineraryFragment fragment = new StartPlanItineraryFragment();

        fragment.parentActivity = parentActivity;
        fragment.oriItinerary = oriItinerary;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_start_plan_itinerary, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.showBtmNav();

                TextView nextTV = view.findViewById(R.id.nextTV);
                departureTV = view.findViewById(R.id.departureTV);
                returnTV = view.findViewById(R.id.returnTV);
                locationNameATV = view.findViewById(R.id.locationNameATV);
                privateRB = view.findViewById(R.id.privateRB);
                publicRB = view.findViewById(R.id.publicRB);
                uploadCoverIB = view.findViewById(R.id.uploadCoverIB);

                nextTV.setOnClickListener(StartPlanItineraryFragment.this);
                locationNameATV.setOnClickListener(StartPlanItineraryFragment.this);
                returnTV.setOnClickListener(StartPlanItineraryFragment.this);
                departureTV.setOnClickListener(StartPlanItineraryFragment.this);
                uploadCoverIB.setOnClickListener(StartPlanItineraryFragment.this);

                datePickerDialog = GeneralFunction.getDatePickerDialog(parentActivity,StartPlanItineraryFragment.this);
//                datePickerDialog.getDatePicker().setMinDate(new Date().getTime() - 1);

                if(oriItinerary != null){
                    Calendar calendar = new GregorianCalendar();

                    if(oriItinerary.locationName != null){
                        locationNameATV.setText(oriItinerary.locationName);
                    }

                    if(oriItinerary.departDate != null){
                        departDate = oriItinerary.departDate;
                        calendar.setTimeInMillis(departDate);
                        departureTV.setText(format.format(calendar.getTime()));
                    }

                    if(oriItinerary.returnDate != null){
                        returnDate = oriItinerary.returnDate;
                        calendar.setTimeInMillis(returnDate);
                        returnTV.setText(format.format(calendar.getTime()));
                    }

                    if(oriItinerary.isPublic != null){
                        if(oriItinerary.isPublic){
                            publicRB.setChecked(true);
                        }else{
                            privateRB.setChecked(true);
                        }
                    }else{
                        privateRB.setChecked(true);
                    }

                    if(oriItinerary.itineraryPicUrl != null){
                        GlideImageSetting(parentActivity,Uri.parse(oriItinerary.itineraryPicUrl),uploadCoverIB);
                    }

                }

                setupSearchFeature();
            }
        });

        return view;
    }

    private void setupSearchFeature(){
        googleApiClient = new GoogleApiClient.Builder(parentActivity)
                .addApi(Places.GEO_DATA_API)
                .addConnectionCallbacks(StartPlanItineraryFragment.this)
                .addOnConnectionFailedListener(StartPlanItineraryFragment.this)
                .build();
        MapsInitializer.initialize(parentActivity);
        googleApiClient.connect();

        final PlaceAutoCompleteAdapter placeAutoCompleteAdapter = new PlaceAutoCompleteAdapter(parentActivity, R.layout.adapter_place_auto_complete,
                googleApiClient, null, null);

        locationNameATV.setAdapter(placeAutoCompleteAdapter);

        locationNameATV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                final PlaceAutoCompleteAdapter.PlaceAutocomplete item = placeAutoCompleteAdapter.getItem(position);
                if(item == null){
                    Log.e(TAG, "Place Autocomplete is null");
                    return;
                }

                placeId = item.placeId.toString();
            }
        });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.v(TAG,connectionResult.toString());
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onDateSet(DatePicker view, final int year, final int month, final int dayOfMonth) {
        parentActivity.runOnUiThread(new Runnable() {
            public void run() {
                Calendar calendar = GeneralFunction.getDefaultUtcCalendar(year,month,dayOfMonth);

                if(departClicked){
                    departDate = calendar.getTimeInMillis();
                    departureTV.setText(format.format(calendar.getTime()));
                }else if(returnClicked){
                    returnDate = calendar.getTimeInMillis();
                    returnTV.setText(format.format(calendar.getTime()));
                }else{
                    departDate = calendar.getTimeInMillis();
                    departureTV.setText(format.format(calendar.getTime()));
                }

                departClicked = false;
                returnClicked = false;
            }
        });
    }

    private boolean isValidInput(){
        if(placeId == null && (oriItinerary == null || oriItinerary.placeAddressList == null || oriItinerary.placeAddressList.isEmpty()) ){
            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.itinerary_err_location),R.drawable.notice_bad,TAG);
            return false;
        }

        return  GeneralFunction.checkAndReturnNum(departDate,parentActivity.getString(R.string.itinerary_err_depart_date),parentActivity)
                &&
                GeneralFunction.checkAndReturnNum(returnDate,parentActivity.getString(R.string.itinerary_err_return_date),parentActivity)
                &&
                GeneralFunction.checkAndReturnDateRange(departDate,returnDate,parentActivity.getString(R.string.itinerary_err_date_range),parentActivity);
    }

    private void getPlaceDetails(){
        parentActivity.initializeProgressIndicator(parentActivity.getString(R.string.itinerary_uploading_cover), false, this);
        parentActivity.controlProgressIndicator(true, this);

        if(placeId != null){
            new UnifyPlaceAddress(parentActivity, placeId, new UnifyPlaceAddress.OnUnifyComplete() {
                @Override
                public void onUnifySuccess(PlaceAddress placeAddress) {
                    parentActivity.updateProgress(40,StartPlanItineraryFragment.this);
                    uploadItineraryPic(placeAddress);
                }

                @Override
                public void onUnifyFailed(Exception e) {
                    GeneralFunction.handleUploadError(parentActivity,StartPlanItineraryFragment.this,R.string.error_occured_getting_place_details,TAG);
                    e.printStackTrace();
                }
            });
        }else{
            uploadItineraryPic(null);
        }
    }

    private void uploadItineraryPic(final PlaceAddress placeAddress){
        if(filePath == null){
            parentActivity.controlProgressIndicator(false, this);
            setItineraryDetails(null, placeAddress);
            return;
        }

        final StorageReference storageReference = GeneralFunction.getFirebaseStorageInstance()
                .child(Itinerary.URL_ITINERARY)
                .child(parentActivity.uid)
                .child(ref.getId())
                .child(Itinerary.ITINERARY_PIC_URL);

        UploadTask uploadTask = storageReference.putBytes(GeneralFunction.getCompressedBitmap(parentActivity,filePath));

        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                GeneralFunction.handleUploadError(parentActivity,StartPlanItineraryFragment.this,R.string.itinerary_err_upload_pic,TAG);
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri downloadUri) {
                        if(downloadUri != null){
                            //close progress dialog
                            GeneralFunction.handleUploadSuccess(parentActivity,StartPlanItineraryFragment.this,R.string.itinerary_uploaded_cover_pic,TAG);
                            setItineraryDetails(downloadUri.toString(), placeAddress);
                        }else{
                            GeneralFunction.handleUploadError(parentActivity,StartPlanItineraryFragment.this,R.string.itinerary_err_upload_pic,TAG);
                        }
                    }
                });
            }
        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                // + 10 is because i set the progress to 10 when it finished saving the initial firebase data
                // * 80 is because i want the progress to go to maximum 90% only when the image is finished uploaded
                // so that the final 10% is set when the firebase updates the imageUrl data as the final step
                int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * 80) + 10;
                parentActivity.updateProgress((float) totalProgress,StartPlanItineraryFragment.this);
            }
        });
    }

    private void setItineraryDetails(String coverUrl, PlaceAddress placeAddress){
        HashMap<String,Object> itiMap = new HashMap<>();

        if(oriItinerary != null){
            itiMap = Itinerary.toMap(oriItinerary,true);

            if(!TextUtils.equals(parentActivity.uid,oriItinerary.userId)){
                itiMap.put(Itinerary.DATE_CREATED, FieldValue.serverTimestamp());
            }

            if(oriItinerary.itineraryPicUrl != null){
                itiMap.put(Itinerary.ITINERARY_PIC_URL, oriItinerary.itineraryPicUrl);
            }else{
                itiMap.put(Itinerary.ITINERARY_PIC_URL, null);
            }

            if(oriItinerary.locationName != null){
                itiMap.put(Itinerary.LOCATION_NAME,oriItinerary.locationName);
            }

            if(oriItinerary.address != null){
                itiMap.put(Itinerary.ADDRESS,oriItinerary.address);
            }

            if(oriItinerary.placeId != null){
                itiMap.put(Itinerary.PLACE_ID,oriItinerary.placeId);
            }

            if(oriItinerary.gps != null){
                itiMap.put(Itinerary.GPS,oriItinerary.gps);
            }

            if(oriItinerary.dateCreated == null){
                itiMap.put(Itinerary.DATE_CREATED, FieldValue.serverTimestamp());
            }

        }else{
            itiMap.put(Itinerary.DATE_CREATED, FieldValue.serverTimestamp());
        }

        itiMap.put(Itinerary.USER_ID, parentActivity.uid);
        if(placeId != null && !placeId.trim().isEmpty() && placeAddress != null){
            itiMap.put(Itinerary.ADDRESS,placeAddress.placeName);

            itiMap.put(Itinerary.PLACE_ID,placeId);

            ArrayList<GeoPoint> gpsList = new ArrayList<>();
            gpsList.add(new GeoPoint(placeAddress.lat,placeAddress.lng));
            itiMap.put(Itinerary.GPS,gpsList);

            ArrayList<HashMap<String,Object>> placeAddressList = new ArrayList<>();
            placeAddressList.add(PlaceAddress.getHashmap(placeAddress,placeId));

            itiMap.put(Itinerary.PLACE_ADDRESS_LIST,placeAddressList);
            itiMap.put(Itinerary.PLACE_INDEX,PlaceAddress.getPlaceIndex(placeAddress));
        }

        itiMap.put(Itinerary.LOCATION_NAME, locationNameATV.getText().toString().trim());
        itiMap.put(Itinerary.DEPART_DATE, departDate);
        itiMap.put(Itinerary.RETURN_DATE, returnDate);
        itiMap.put(Itinerary.NO_OF_DAYS, GeneralFunction.getNoofDaysInDate(departDate,returnDate));
        if(coverUrl != null){
            itiMap.put(Itinerary.ITINERARY_PIC_URL, coverUrl);
        }

//        old
//        itiMap.put(Itinerary.IS_PUBLIC, !privateRB.isChecked());

//        if(parentActivity.getThisUser() != null && parentActivity.getThisUser().isAdmin != null && parentActivity.getThisUser().isAdmin){
//            //only if this user is an admin, let him publish the itinerary
//            itiMap.put(Itinerary.IS_PUBLIC, true);
//            itiMap.put(Itinerary.IS_PUBLISH, true);
//        }else{
//            itiMap.put(Itinerary.IS_PUBLIC, false);
//            itiMap.put(Itinerary.IS_PUBLISH, false);
//        }

        itiMap.put(Itinerary.LIKE_COUNT, (long) 0);//to prevent it from being null, it wont be searchable if it is null
        //new 1: even if is admin, if use app, all will be unpublished
        itiMap.put(Itinerary.IS_PUBLIC, false);
        itiMap.put(Itinerary.IS_PUBLISH, false);

        itiMap.put(Itinerary.DATE_UPDATED, FieldValue.serverTimestamp());

        ref.set(itiMap);

        //id i put after i set because i dont want to set the id but i want to pass it to the next fragment
        itiMap.put(Itinerary.ID, ref.getId());

        if(oriItinerary != null){
            if(oriItinerary.planList == null){
                oriItinerary.planList = new SparseArray<>();
            }
            oriItinerary = Itinerary.toClass(itiMap,parentActivity,oriItinerary.planList);
        }else{
            oriItinerary = Itinerary.toClass(itiMap,parentActivity,new SparseArray<ArrayList<Plan>>());
        }

        if(oriItinerary.planList != null && oriItinerary.planList.size() > 0){
            CollectionReference plansRef = GeneralFunction.getFirestoreInstance()
                    .collection(Itinerary.URL_ITINERARY)
                    .document(oriItinerary.id)
                    .collection(Plan.URL_PLAN);
            for(int i = 0; i < oriItinerary.planList.size(); i++) {
                int key = oriItinerary.planList.keyAt(i);
                ArrayList<Plan> plans = oriItinerary.planList.get(key);

                for(Plan plan : plans){
                    plan.dateCreated = null;
                    plansRef.document().set(plan);
                }
            }
        }

        Fragment startPlanItineraryMoreDetailsFragment = StartPlanItineraryMoreDetailsFragment.newInstance(parentActivity,parentActivity.getString(R.string.itinerary_action_toolbar2),oriItinerary);
        startPlanItineraryMoreDetailsFragment.setTargetFragment(this, Config.INTENT_ITI_START_TO_MORE_DETAIL);
        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, startPlanItineraryMoreDetailsFragment).addToBackStack(StartPlanItineraryMoreDetailsFragment.TAG).commit();
    }

    private void initializeRef(){
        if(oriItinerary != null){
            ref = GeneralFunction.getFirestoreInstance()
                    .collection(Itinerary.URL_ITINERARY)
                    .document(oriItinerary.id);

            if(!TextUtils.equals(parentActivity.uid,oriItinerary.userId)){
                ref = GeneralFunction.getFirestoreInstance()
                        .collection(Itinerary.URL_ITINERARY)
                        .document();
            }
        }else{
            ref = GeneralFunction.getFirestoreInstance()
                    .collection(Itinerary.URL_ITINERARY)
                    .document();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                Uri resultUri = result.getUri();
                filePath = FilePath.getPath(parentActivity, resultUri);
                GlideImageSetting(parentActivity, resultUri,uploadCoverIB);
            }else if (requestCode == Config.INTENT_ITI_START_TO_MORE_DETAIL) {
                //this is for passing data back from start plan more detail fragments (need to setTargetFragment first though)
                Gson gson = new Gson();
                String itiFromContainer = data.getStringExtra(Config.ITINERARY_INTENT_ID);
                oriItinerary = gson.fromJson(itiFromContainer, Itinerary.class);
            }
//            else if (requestCode == CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE) {
//                if (ContextCompat.checkSelfPermission(parentActivity, Manifest.permission.CAMERA)
//                        == PackageManager.PERMISSION_DENIED) {
//                    ActivityCompat.requestPermissions(parentActivity, new String[]{Manifest.permission.CAMERA}, requestCode);
//                }
//            }else if (requestCode == Config.REQUEST_PLACE_PICKER) {
//                place = PlacePicker.getPlace(parentActivity, data);
//
//                if (place != null && place.getAddress() != null && !place.getAddress().equals("")) {
//                    GeneralFunction.setLocationCountryForPlace(place, locationNameATV);
//                } else {
//                    GeneralFunction.openMessageNotificationDialog(parentActivity, parentActivity.getString(R.string.error_place_picker), R.drawable.notice_bad, TAG);
//                }
//            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onResume() {
        super.onResume();

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.itinerary_action_toolbar1));
                parentActivity.selectItinerary();
            }
        });

        Calendar calendar = new GregorianCalendar();

        if(departDate != 0){
            calendar.setTimeInMillis(departDate);
            departureTV.setText(format.format(calendar.getTime()));
        }

        if(returnDate != 0){
            calendar.setTimeInMillis(returnDate);
            returnTV.setText(format.format(calendar.getTime()));
        }

        if(googleApiClient != null){
            googleApiClient.connect();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(googleApiClient != null){
            googleApiClient.disconnect();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.nextTV:
                if(isValidInput()){
                    initializeRef();
                    getPlaceDetails();
                }
                break;
//            case R.id.locationNameATV:
//                GeneralFunction.openPlacePicker(parentActivity,StartPlanItineraryFragment.this);
//                break;
            case R.id.departureTV:
                departClicked = true;
                datePickerDialog.show();
                break;
            case R.id.returnTV:
                returnClicked = true;
                datePickerDialog.show();
                break;
            case R.id.uploadCoverIB:
                GeneralFunction.showFileChooser(parentActivity,this);
                break;
        }
    }
}
