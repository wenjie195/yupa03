package com.vidatechft.yupa.tourBuddyFragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.PlaceDetailsGoogleApi.PlaceAddress;
import com.vidatechft.yupa.classes.TourBuddy;
import com.vidatechft.yupa.classes.User;
import com.vidatechft.yupa.retrofitNetwork.UnifyPlaceAddress;
import com.vidatechft.yupa.utilities.Config;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

import static android.app.Activity.RESULT_OK;

public class TourBuddyRegisterFragment extends Fragment
{
    public static final String TAG = TourBuddyRegisterFragment.class.getName();
    private MainActivity parentActivity;
    private Place place;
    private EditText placeStayedET;
    private GeoPoint getGeoPoint;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private TextView applyTourBuddyTV,backTV;

    public TourBuddyRegisterFragment(){}

    public static TourBuddyRegisterFragment newInstance(MainActivity parentActivity)
    {
        TourBuddyRegisterFragment fragment = new TourBuddyRegisterFragment();
        fragment.parentActivity = parentActivity;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater , ViewGroup container ,
            Bundle savedInstanceState)
    {
        View view =  inflater.inflate(R.layout.fragment_register_tour_buddy , container , false);

        //todo : View id`s
        findView(view);

        //todo : it means he`s pending and not accepted
        checkWhetherIfHeAlreadyApply();

        //todo : Place picker
        onClickListeners();

        return view;
    }

    private void checkWhetherIfHeAlreadyApply()
    {
        db.collection(TourBuddy.URL_TOUR_BUDDY).document(parentActivity.uid)
                .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>()
        {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task)
            {
                if (task.isSuccessful())
                {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists())
                    {
                        GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.tour_buddy_register_already),R.drawable.notice_bad,TAG);
                    }
                    else
                    {
                        //do nothing because first time apply doesnt have to show message
                    }
                }
                else
                {
                    //do nothing because first time apply doesnt have to show message
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(resultCode == RESULT_OK){
            if (requestCode == Config.REQUEST_PLACE_PICKER) {
                place = PlacePicker.getPlace(parentActivity, data);

                if (place != null && place.getAddress() != null && !place.getAddress().equals("")) {
                    placeStayedET.setText(place.getAddress().toString());
                    getGeoPoint = new GeoPoint(place.getLatLng().latitude , place.getLatLng().longitude);
                }
                else
                {
                    GeneralFunction.openMessageNotificationDialog(parentActivity, parentActivity.getString(R.string.error_place_picker), R.drawable.notice_bad, TAG);
                }
            }
        }
    }

    private void onClickListeners()
    {
        placeStayedET.setOnClickListener(new DebouncedOnClickListener(500)
        {
            @Override
            public void onDebouncedClick(View v)
            {
                GeneralFunction.openPlacePicker(parentActivity,TourBuddyRegisterFragment.this);
            }
        });
        applyTourBuddyTV.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(placeStayedET.getText().toString() != "" && place != null)
                {
                    saveDataIntoFirestore();
                }
                else
                {
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.tour_buddy_register_validation),R.drawable.notice_bad,TAG);
                }
            }
        });
        backTV.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                parentActivity.onBackPressed();
            }
        });
    }

    private void saveDataIntoFirestore()
    {
        final Map<String,Object> setTourBuddyData =  new HashMap<>();
        final DocumentReference docRef = db.collection(TourBuddy.URL_TOUR_BUDDY).document(parentActivity.uid);
        new UnifyPlaceAddress(parentActivity , place.getId() , new UnifyPlaceAddress.OnUnifyComplete()
        {
            @Override
            public void onUnifySuccess(PlaceAddress placeAddress)
            {

                if(getGeoPoint != null)
                {
                    setTourBuddyData.put(TourBuddy.TOUR_BUDDY_GET_CURRENT_LOCATION,getGeoPoint);
                }

                setTourBuddyData.put(TourBuddy.TOUR_BUDDY_ADDRESS,placeStayedET.getText().toString());
                setTourBuddyData.put(TourBuddy.TOUR_BUDDY_PLACE_ADDRESS,PlaceAddress.getHashmap(placeAddress,place.getId()));
                setTourBuddyData.put(TourBuddy.TOUR_BUDDY_PLACE_INDEX,PlaceAddress.getPlaceIndex(placeAddress));
                setTourBuddyData.put(TourBuddy.TOUR_BUDDY_USER_ID,parentActivity.uid);
                setTourBuddyData.put(TourBuddy.TOUR_BUDDY_DATE_CREATED,FieldValue.serverTimestamp());
                setTourBuddyData.put(TourBuddy.TOUR_BUDDY_DATE_UPDATED,FieldValue.serverTimestamp());
                setTourBuddyData.put(TourBuddy.TOUR_BUDDY_APPLY_STATUS,TourBuddy.TOUR_BUDDY_APPLY_STATUS_PENDING);


                saveIntoFireStore(docRef,setTourBuddyData);
            }

            @Override
            public void onUnifyFailed(Exception e)
            {

            }
        });
    }

    private void saveIntoFireStore(DocumentReference docRef , Map<String,Object> setTourBuddyData)
    {
        docRef.set(setTourBuddyData).addOnSuccessListener(new OnSuccessListener<Void>()
        {
            @Override
            public void onSuccess(Void aVoid)
            {
                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.tour_buddy_register_wait_admin_approve),R.drawable.notice_good,TAG);
            }
        }).addOnFailureListener(new OnFailureListener()
        {
            @Override
            public void onFailure(@NonNull Exception e)
            {
                GeneralFunction.openMessageNotificationDialog(parentActivity,e.getMessage(),R.drawable.notice_bad,TAG);
            }
        });

        setTourBuddyData.remove(TourBuddy.TOUR_BUDDY_APPLY_STATUS);
        setTourBuddyData.put(TourBuddy.TOUR_BUDDY_IS_AVAILABLE,false);

        GeneralFunction.getFirestoreInstance().collection(User.URL_USER)
                .document(parentActivity.uid)
                .update(TourBuddy.URL_TOUR_BUDDY_CLASS_NAME,setTourBuddyData);
    }

    private void findView(View view)
    {
        placeStayedET = view.findViewById(R.id.placeStayedET);
        applyTourBuddyTV = view.findViewById(R.id.applyTourBuddyTV);
        applyTourBuddyTV.setText("Apply For Tour Buddy");
        backTV = view.findViewById(R.id.backTV);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        parentActivity.setToolbarTitle(parentActivity.getString(R.string.tour_buddy_toolbar_tour_buddy_register));
        parentActivity.selectChat();
    }
}