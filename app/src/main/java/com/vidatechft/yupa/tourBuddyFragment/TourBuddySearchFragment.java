package com.vidatechft.yupa.tourBuddyFragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.GetAllAvailableTourBuddyAdapter;
import com.vidatechft.yupa.classes.PlaceDetailsGoogleApi.PlaceAddress;
import com.vidatechft.yupa.classes.TourBuddy;
import com.vidatechft.yupa.classes.User;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;

public class TourBuddySearchFragment extends Fragment
{
    public static final String TAG = TourBuddySearchFragment.class.getName();
    private MainActivity parentActivity;
    private User searchedUser;
    private EditText searchET;
    private Button searchBTN;
    private ImageView clearIV;
    private TextView displayMsgTV;
    private RecyclerView searchTourBuddyRV;

    private GetAllAvailableTourBuddyAdapter getAllAvailableTourBuddyAdapter;
    private ArrayList<User> getTourBuddy = new ArrayList<>();
    private ListenerRegistration searchListener;

    public TourBuddySearchFragment()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        parentActivity.setToolbarTitle(parentActivity.getString(R.string.tour_buddy_toolbar_search_tour_buddy));
        parentActivity.selectChat();
    }

    public static TourBuddySearchFragment newInstance(MainActivity parentActivity , User searchedUser)
    {
        TourBuddySearchFragment fragment = new TourBuddySearchFragment();
        fragment.parentActivity = parentActivity;
        fragment.searchedUser = searchedUser;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater , ViewGroup container ,
            Bundle savedInstanceState)
    {
        View view =  inflater.inflate(R.layout.fragment_search_tour_buddy , container , false);

        //todo : All of the UI views variables
        findViews(view);

        //todo : Setup grid adapter for display all tour buddy
        setUpGridAdapter();

        //todo : onClickListeners
        onClickSetup();

        if(getTourBuddy.size() <= 0)
        {
            displayMsgTV.setText("Loading all available Tour Buddy");
            searchTourBuddy();
        }
        else
        {
            displayMsgTV.setVisibility(View.GONE);
        }

        return view;
    }
    @Override
    public void onDestroy()
    {
        super.onDestroy();
        if (searchListener != null)
        {
            searchListener.remove();
            searchedUser = null;
        }
    }

    private void searchTourBuddy()
    {
        Query reference = GeneralFunction.getFirestoreInstance().collection(User.URL_USER);
        reference = reference.whereEqualTo(User.IS_TOUR_BUDDY,true);
        reference = reference.whereEqualTo(TourBuddy.URL_TOUR_BUDDY_CLASS_NAME +"."+TourBuddy.TOUR_BUDDY_IS_AVAILABLE,true);

        // place comparison
        if(searchedUser.placeAddress != null)
        {
            reference = PlaceAddress.getQueriedPlaceAddress(reference,searchedUser.placeAddress,TourBuddy.URL_TOUR_BUDDY_CLASS_NAME);
        }

        //gender Comparison
        if(searchedUser.gender != null)
        {
            if(searchedUser.gender ==  User.GENDER_MALE)
            {
                reference = reference.whereEqualTo(User.GENDER,User.GENDER_MALE);
            }
            else
            {
                reference = reference.whereEqualTo(User.GENDER,User.GENDER_FEMALE);
            }
        }

        // date Comparison
        if(searchedUser.dateSearchRange != null)
        {
            for(Map.Entry<String, Object> entry: searchedUser.dateSearchRange.entrySet())
            {
                reference =  reference.whereEqualTo(User.URL_USER_TOUR_BUDDY +"."+TourBuddy.TOUR_BUDDY_GET_ALL_AVAILABLE_DATES_INDEX +"."+ entry.getKey(),true);
            }
        }

        searchListener = reference.addSnapshotListener(new EventListener<QuerySnapshot>()
        {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots , @Nullable FirebaseFirestoreException e)
            {
                if(searchedUser == null){
                    return;
                }

                if(queryDocumentSnapshots!= null && !queryDocumentSnapshots.isEmpty())
                {
                    getTourBuddy.clear();
                    for(DocumentSnapshot tempUser:queryDocumentSnapshots)
                    {
                        User eachTourBuddy = new User(parentActivity,tempUser);
                        boolean addToList = true;

                        // Age Comparison
                        if(addToList && searchedUser.ageRangeMin >= 5 && searchedUser.ageRangeMax <= 105)
                        {
                            Calendar currentDate = GeneralFunction.getDefaultUtcCalendar();
                            int age = currentDate.get(Calendar.YEAR) - eachTourBuddy.dobYear;
                            if(age < searchedUser.ageRangeMin || age > searchedUser.ageRangeMax)
                            {
                                addToList = false;
                            }
                        }

                        //Hobby comparison
                        if(addToList && searchedUser.hobbyScope != null)
                        {
                            for(String searchedHobby : searchedUser.hobbyScope)
                            {
                                if(eachTourBuddy.hobbyScope != null && !eachTourBuddy.hobbyScope.contains(searchedHobby))
                                {
                                    addToList = false;
                                }
                            }
                        }

                        if(TextUtils.equals(eachTourBuddy.id,parentActivity.uid))
                        {
                            addToList = false;
                        }

                        if(addToList)
                        {
                            getTourBuddy.add(eachTourBuddy);
                        }
                    }

                    parentActivity.runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            getAllAvailableTourBuddyAdapter.notifyDataSetChanged();
                            displayMsgTV.setVisibility(View.GONE);
                        }
                    });
                }
                else
                {
                    displayMsgTV.setText("There are no Tour Buddy available at the moment");
                }
            }
        });
    }

    private void onClickSetup()
    {
        searchBTN.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                parentActivity.onBackPressed();
            }
        });

        searchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(count > 0){
                    clearIV.setVisibility(View.VISIBLE);
                }else{
                    clearIV.setVisibility(View.GONE);
                }

                searchedUser.searchNameOrContactOrEmail = s.toString();

                searchTourBuddy();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        clearIV.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                searchET.setText("");
            }
        });
    }

    private void setUpGridAdapter()
    {
        getAllAvailableTourBuddyAdapter = new GetAllAvailableTourBuddyAdapter(parentActivity,this,getTourBuddy,searchedUser);
        searchTourBuddyRV.setNestedScrollingEnabled(false);
        searchTourBuddyRV.setAdapter(getAllAvailableTourBuddyAdapter);
        searchTourBuddyRV.setLayoutManager(new GridLayoutManager(parentActivity, 3));

    }

    private void findViews(View view)
    {
        searchET = view.findViewById(R.id.searchET);
        searchBTN = view.findViewById(R.id.searchBTN);
        displayMsgTV = view.findViewById(R.id.displayMsgTV);
        searchTourBuddyRV = view.findViewById(R.id.searchTourBuddyRV);
        clearIV = view.findViewById(R.id.clearIV);
    }

}