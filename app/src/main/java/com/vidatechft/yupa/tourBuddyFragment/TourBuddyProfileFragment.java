package com.vidatechft.yupa.tourBuddyFragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.ListenerRegistration;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.PlaceDetailsGoogleApi.PlaceAddress;
import com.vidatechft.yupa.classes.TourBuddy;
import com.vidatechft.yupa.classes.User;
import com.vidatechft.yupa.retrofitNetwork.UnifyPlaceAddress;
import com.vidatechft.yupa.utilities.Config;
import com.vidatechft.yupa.utilities.CustomMaterialCalendarView;
import com.vidatechft.yupa.utilities.DebouncedOnClickListener;
import com.vidatechft.yupa.utilities.GeneralFunction;

import org.threeten.bp.Instant;
import org.threeten.bp.ZoneOffset;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

import static android.app.Activity.RESULT_OK;
import static com.vidatechft.yupa.utilities.CustomMaterialCalendarView.SELECTION_MODE_MULTIPLE_RANGE;

public class TourBuddyProfileFragment extends Fragment
{
    public static final String TAG = TourBuddyProfileFragment.class.getName();
    private MainActivity parentActivity;
    private ImageView profilePicIV;
    private Button buttonCL1BTN;
    private CustomMaterialCalendarView tourCalendarCMCV;
    private Switch toggleTourBuddySWTC;
    private TextView nameTV,liveInTV,joinedTV,currentLiveTV;
    private EditText placeStayedET;
    private Place place;
    private GeoPoint getGeoPoint;
    private ListenerRegistration userDetailsListeners;
    private Map<String,Object> tourBuddyData = new HashMap<>();
    private Map<String,Object> getAllTourBuddyAvailableDateCustomMaterialView = new HashMap<>();
    private HashMap<CalendarDay,Integer> populateDateMap = new HashMap<>();
    private boolean gotCheck = false;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();


    public TourBuddyProfileFragment()
    {
        // Required empty public constructor
    }

    public static TourBuddyProfileFragment newInstance(MainActivity parentActivity)
    {
        TourBuddyProfileFragment fragment = new TourBuddyProfileFragment();
        fragment.parentActivity = parentActivity;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        parentActivity.setToolbarTitle(parentActivity.getString(R.string.tour_buddy_toolbar_tour_buddy_profile));
        parentActivity.selectChat();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        if(userDetailsListeners != null)
        {
            userDetailsListeners.remove();
        }

        if(gotCheck)
        {
            db.collection(User.URL_USER).document(parentActivity.uid)
                    .update(TourBuddy.URL_TOUR_BUDDY_CLASS_NAME+"."+TourBuddy.TOUR_BUDDY_IS_AVAILABLE,true);
        }
        if(!gotCheck)
        {
            db.collection(User.URL_USER).document(parentActivity.uid)
                    .update(TourBuddy.URL_TOUR_BUDDY_CLASS_NAME+"."+TourBuddy.TOUR_BUDDY_IS_AVAILABLE,false);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater , ViewGroup container ,
            Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_tour_buddy_profile , container , false);

        //todo : findviews
        findViews(view);

        //todo : initializes Custom Material Calendar View
        initializeCustomMaterialCalendarView();

        //todo : set picture and name
        initializeUserDetails();

        //todo : toggle calendar to show or hide
        toggleCalendar();

        //todo : onclickListener
        onClickListeners();

        return view;
    }

    private void onClickListeners()
    {
        //todo : button update
        buttonCL1BTN.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                getData(tourBuddyData);
            }
        });

        //todo : if the user want to edit their place
        placeStayedET.setOnClickListener(new DebouncedOnClickListener(500)
        {
            @Override
            public void onDebouncedClick(View v)
            {
                GeneralFunction.openPlacePicker(parentActivity,TourBuddyProfileFragment.this);
            }
        });
    }

    private void getData(Map<String, Object> tourBuddyData)
    {
        boolean previouslyAsTourBuddy;
        final Map <String,Object> updateTourBuddyData =  new HashMap<>();
        final DocumentReference userTourBuddyRef = db.collection(User.URL_USER).document(parentActivity.uid);

        // todo : if he was a tour buddy before
        if(tourBuddyData != null && ! tourBuddyData.isEmpty())
        {
            previouslyAsTourBuddy = true;
        }
        // todo : if he was a first time user
        else
        {
            previouslyAsTourBuddy = false;
        }

        if(place != null)
        {
            getGeoPoint = new GeoPoint(place.getLatLng().latitude , place.getLatLng().longitude);
            unifyAddressPlace(getGeoPoint,previouslyAsTourBuddy,tourBuddyData,place,updateTourBuddyData,userTourBuddyRef);
        }
        else
        {
            if(previouslyAsTourBuddy)
            {
                getGeoPoint = (GeoPoint) tourBuddyData.get(TourBuddy.TOUR_BUDDY_GET_CURRENT_LOCATION);
                unifyAddressPlace(getGeoPoint,previouslyAsTourBuddy,tourBuddyData,place,updateTourBuddyData,userTourBuddyRef);
            }
            else
            {
                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.tour_buddy_error_place),R.drawable.notice_bad,TAG);
            }
        }
    }

    private void unifyAddressPlace(
            final GeoPoint getGeoPoint,
            final boolean previouslyAsTourBuddy,
            final Map<String, Object> tourBuddyData,
            final Place place,
            final Map<String, Object> updateTourBuddyData,
            final DocumentReference userTourBuddyRef)
    {
        if(place != null)
        {
            new UnifyPlaceAddress(parentActivity , place.getId() , new UnifyPlaceAddress.OnUnifyComplete()
            {
                @Override
                public void onUnifySuccess(PlaceAddress placeAddress)
                {
                    updateTourBuddyData.put(TourBuddy.TOUR_BUDDY_ADDRESS,placeStayedET.getText().toString());
                    updateTourBuddyData.put(TourBuddy.TOUR_BUDDY_PLACE_ADDRESS,PlaceAddress.getHashmap(placeAddress,place.getId()));
                    updateTourBuddyData.put(TourBuddy.TOUR_BUDDY_PLACE_INDEX,PlaceAddress.getPlaceIndex(placeAddress));
                    updateTourBuddyData.put(TourBuddy.TOUR_BUDDY_GET_CURRENT_LOCATION,getGeoPoint);
                    updateTourBuddyData.put(TourBuddy.TOUR_BUDDY_DATE_UPDATED,FieldValue.serverTimestamp());

                    if(previouslyAsTourBuddy)
                    {
                        updateTourBuddyData.put(TourBuddy.TOUR_BUDDY_DATE_CREATED,tourBuddyData.get(TourBuddy.TOUR_BUDDY_DATE_CREATED));
                    }
                    else
                    {
                        updateTourBuddyData.put(TourBuddy.TOUR_BUDDY_DATE_CREATED,FieldValue.serverTimestamp());
                    }

                    getCustomDate(userTourBuddyRef,updateTourBuddyData);
                }

                @Override
                public void onUnifyFailed(Exception e)
                {

                }
            });
        }
        else
        {
            if(previouslyAsTourBuddy)
            {
                updateTourBuddyData.put(TourBuddy.TOUR_BUDDY_ADDRESS,placeStayedET.getText().toString());
                updateTourBuddyData.put(TourBuddy.TOUR_BUDDY_PLACE_ADDRESS,tourBuddyData.get(TourBuddy.TOUR_BUDDY_PLACE_ADDRESS));
                updateTourBuddyData.put(TourBuddy.TOUR_BUDDY_PLACE_INDEX,tourBuddyData.get(TourBuddy.TOUR_BUDDY_PLACE_INDEX));
                updateTourBuddyData.put(TourBuddy.TOUR_BUDDY_GET_CURRENT_LOCATION,getGeoPoint);
                updateTourBuddyData.put(TourBuddy.TOUR_BUDDY_DATE_UPDATED,FieldValue.serverTimestamp());
                updateTourBuddyData.put(TourBuddy.TOUR_BUDDY_DATE_CREATED,tourBuddyData.get(TourBuddy.TOUR_BUDDY_DATE_CREATED));

                getCustomDate(userTourBuddyRef , updateTourBuddyData);
            }
        }
    }

    private void getCustomDate(DocumentReference userTourBuddyRef , Map<String, Object> updateTourBuddyData)
    {
        final Map <String,Object> setDatesForInterface = new HashMap<>();
        final Map <String,Object> setDatesForSearching = new HashMap<>();
        HashMap<CalendarDay,Integer> multiRangeDayMap = tourCalendarCMCV.getSelectedMultiRangeDay();

        for (Map.Entry<CalendarDay, Integer> entry : multiRangeDayMap.entrySet())
        {
            long getFormattedTime = GeneralFunction.getDefaultUtcCalendar(
                    entry.getKey().getYear(),
                    entry.getKey().getMonth() - 1, //need - 1 because the android Calendar class's month default starts at 0
                    entry.getKey().getDay())
                    .getTime().getTime();
            setDatesForInterface.put(String.valueOf(getFormattedTime),entry.getValue());
            setDatesForSearching.put(String.valueOf(getFormattedTime),true);
        }

        updateTourBuddyData.put(TourBuddy.TOUR_BUDDY_GET_ALL_AVAILABLE_DATES,setDatesForInterface);
        updateTourBuddyData.put(TourBuddy.TOUR_BUDDY_GET_ALL_AVAILABLE_DATES_INDEX,setDatesForSearching);
        updateTourBuddyData.put(TourBuddy.TOUR_BUDDY_IS_AVAILABLE,true);
        
        updateDataInsideFirestore(updateTourBuddyData,userTourBuddyRef);
    }

    private void updateDataInsideFirestore(Map<String,Object> updateTourBuddyData , DocumentReference userTourBuddyRef)
    {
        userTourBuddyRef.update(TourBuddy.URL_TOUR_BUDDY_CLASS_NAME,updateTourBuddyData)
        .addOnSuccessListener(new OnSuccessListener<Void>()
        {
            @Override
            public void onSuccess(Void aVoid)
            {
                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.tour_buddy_edit_successful),R.drawable.notice_good,TAG);
            }
        })
        .addOnFailureListener(new OnFailureListener()
        {
            @Override
            public void onFailure(@NonNull Exception e)
            {
                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.tour_buddy_edit_fail),R.drawable.notice_bad,TAG);
            }
        });
    }

    //todo : If he was a tour buddy before
    private void getTourBuddyData(User currentUser)
    {
        if(currentUser.tourBuddy != null)
        {
            tourBuddyData = currentUser.tourBuddy;

            if (tourBuddyData.containsKey(TourBuddy.TOUR_BUDDY_GET_ALL_AVAILABLE_DATES))
            {
                getAllTourBuddyAvailableDateCustomMaterialView = (Map<String, Object>) tourBuddyData.get(TourBuddy.TOUR_BUDDY_GET_ALL_AVAILABLE_DATES);

                for (Map.Entry<String, Object> currentKey : getAllTourBuddyAvailableDateCustomMaterialView.entrySet())
                {
                    String key = currentKey.getKey();
                    String value = currentKey.getValue().toString();

                    populateDateMap.put(CalendarDay.from(Instant.ofEpochMilli(Long.parseLong(key)).atZone(ZoneOffset.UTC).toLocalDate()) , Integer.parseInt(String.valueOf(value)));
                }
                tourCalendarCMCV.setNewAllSelectedDate(parentActivity , populateDateMap);
            }
            if (tourBuddyData.containsKey(TourBuddy.TOUR_BUDDY_ADDRESS))
            {
                placeStayedET.setText(tourBuddyData.get(TourBuddy.TOUR_BUDDY_ADDRESS).toString());
            }

            if(tourBuddyData.containsKey(TourBuddy.TOUR_BUDDY_IS_AVAILABLE))
            {
                gotCheck = (boolean) tourBuddyData.get(TourBuddy.TOUR_BUDDY_IS_AVAILABLE);
                toggleTourBuddySWTC.setChecked(gotCheck);
                if(!gotCheck)
                {
                    gotCheck = false;
                    setViewGoneOrVisible(View.GONE);
                }
                else
                {
                    gotCheck = true;
                    setViewGoneOrVisible(View.VISIBLE);
                }

            }
            if (!tourBuddyData.containsKey(TourBuddy.TOUR_BUDDY_IS_AVAILABLE))
            {
                gotCheck = false;
                toggleTourBuddySWTC.setChecked(gotCheck);
                setViewGoneOrVisible(View.GONE);
            }
        }
        else
        {
            tourBuddyData = null;
        }
    }

    private void initializeUserDetails()
    {
        userDetailsListeners =
            GeneralFunction.getFirestoreInstance()
            .collection(User.URL_USER).document(parentActivity.uid)
            .addSnapshotListener(new EventListener<DocumentSnapshot>()
            {
                @Override
                public void onEvent(@Nullable DocumentSnapshot documentSnapshot , @Nullable FirebaseFirestoreException e)
                {
                    if(documentSnapshot != null && documentSnapshot.exists())
                    {
                        User currentUser = new User(parentActivity,documentSnapshot);

                        // todo : display user profile
                        putUserDetailsIntoUI(currentUser);

                        // todo : display Tour Buddy data
                        getTourBuddyData(currentUser);
                    }
                    else
                    {
                        GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.tour_buddy_server_error),R.drawable.notice_bad,TAG);
                    }
                }
            });
    }

    private void putUserDetailsIntoUI(User currentUser)
    {
        if(currentUser != null)
        {
            if(currentUser.profilePicUrl != null && currentUser.profilePicUrl != "")
            {
                GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(currentUser.profilePicUrl),profilePicIV);
            }
            if(currentUser.name != null)
            {
                nameTV.setText(currentUser.name);
            }

            if(currentUser.country != null)
            {
                if(currentUser.state != null && !currentUser.state.isEmpty())
                {
                    liveInTV.setText(String.format(parentActivity.getString(R.string.user_live_in_both), currentUser.state, currentUser.country));
                }
                else
                {
                    liveInTV.setText(String.format(parentActivity.getString(R.string.user_live_in_country), currentUser.country));
                }
            }

            if(currentUser.dateCreated != null)
            {
                joinedTV.setText(String.format(parentActivity.getString(R.string.user_joined_in), GeneralFunction.getSdf().format(currentUser.dateCreated.toDate().getTime())));
            }
        }
    }

    private void toggleCalendar()
    {
        toggleTourBuddySWTC.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if(buttonView.isChecked())
                {
                    gotCheck = true;
                    setViewGoneOrVisible(View.VISIBLE);

                }
                else
                {
                    gotCheck = false;
                    setViewGoneOrVisible(View.GONE);

                }
            }
        });


    }

    private void setViewGoneOrVisible(int viewVisibility)
    {
        tourCalendarCMCV.setVisibility(viewVisibility);
        buttonCL1BTN.setVisibility(viewVisibility);
        currentLiveTV.setVisibility(viewVisibility);
        placeStayedET.setVisibility(viewVisibility);
    }

    private void initializeCustomMaterialCalendarView()
    {
        tourCalendarCMCV.setContext(parentActivity);
        tourCalendarCMCV.setSelectionMode(SELECTION_MODE_MULTIPLE_RANGE);
        tourCalendarCMCV.hideButtons();
        tourCalendarCMCV.changeSetDateTextOrHideSetDate(parentActivity.getString(R.string.tour_buddy_edit_text_tour_calendar),false);
        tourCalendarCMCV.setTextSizeAndTextStyle(Float.parseFloat("20"),false);
    }

    private void findViews(View view)
    {
        profilePicIV = view.findViewById(R.id.profilePicIV);
        buttonCL1BTN = view.findViewById(R.id.buttonCL1BTN);
        toggleTourBuddySWTC = view.findViewById(R.id.toggleTourBuddySWTC);
        tourCalendarCMCV = view.findViewById(R.id.tourCalendarCMCV);
        nameTV = view.findViewById(R.id.nameTV);
        liveInTV = view.findViewById(R.id.liveInTV);
        joinedTV = view.findViewById(R.id.joinedTV);
        placeStayedET = view.findViewById(R.id.placeStayedET);
        currentLiveTV = view.findViewById(R.id.currentLiveTV);


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(resultCode == RESULT_OK)
        {
            if (requestCode == Config.REQUEST_PLACE_PICKER)
            {
                place = PlacePicker.getPlace(parentActivity, data);

                if (place != null && place.getAddress() != null && !place.getAddress().equals(""))
                {
                    placeStayedET.setText(place.getAddress().toString());
                }
                else
                {
                    GeneralFunction.openMessageNotificationDialog(parentActivity, parentActivity.getString(R.string.error_place_picker), R.drawable.notice_bad, TAG);
                }
            }
        }
    }

}
