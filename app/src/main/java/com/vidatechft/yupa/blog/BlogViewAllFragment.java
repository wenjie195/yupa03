package com.vidatechft.yupa.blog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.BlogRecyclerGridAdapter;
import com.vidatechft.yupa.classes.Blog;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;

public class BlogViewAllFragment extends Fragment{
    public static final String TAG = BlogViewAllFragment.class.getName();
    private MainActivity parentActivity;
    private ArrayList<Blog> blogs = new ArrayList<>();
    private Query queryRef;

    private RecyclerView blogRV;
    private BlogRecyclerGridAdapter blogAdapter;

    private ListenerRegistration blogListener;

    private String loadingTxt,emptyTxt,errorTxt;
    private TextView noBlogTV;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static BlogViewAllFragment newInstance(MainActivity parentActivity, ArrayList<Blog> blogs, Query queryRef) {
        BlogViewAllFragment fragment = new BlogViewAllFragment();

        fragment.parentActivity = parentActivity;
        if(blogs == null){
            fragment.blogs = new ArrayList<>();
        }else{
            fragment.blogs = blogs;
        }
        fragment.queryRef = queryRef;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_blog_view_all, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.showBtmNav();

                loadingTxt = parentActivity.getString(R.string.blog_loading_other);
                emptyTxt = parentActivity.getString(R.string.blog_empty_other);
                errorTxt = parentActivity.getString(R.string.blog_err_loading_other);

                blogRV = view.findViewById(R.id.blogRV);
                noBlogTV = view.findViewById(R.id.noBlogTV);

                setUpGridAdapter();

                if(blogs.size() <= 0){
                    getBlog();
                }else{
                    noBlogTV.setVisibility(View.GONE);
                }
            }
        });

        return view;
    }

    private void setUpGridAdapter(){
        blogAdapter = new BlogRecyclerGridAdapter(parentActivity, blogs,false);

        blogRV.setAdapter(blogAdapter);
        blogRV.setLayoutManager(new GridLayoutManager(parentActivity, 2));
    }

    private void getBlog(){
        //todo the recycler view needs to have lazy load
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                noBlogTV.setText(loadingTxt);
                noBlogTV.setVisibility(View.VISIBLE);
            }
        });

        Query ref;
        if(queryRef != null){
            ref = queryRef;
        }else{
            ref = GeneralFunction.getFirestoreInstance()
                    .collection(Blog.URL_BLOG)
                    .whereEqualTo(Blog.IS_PUBLISH, true)
                    .whereEqualTo(Blog.IS_DRAFT, false)
                    .orderBy(Blog.DATE_UPDATED, Query.Direction.DESCENDING);
        }

        blogListener = ref
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot snapshots, @javax.annotation.Nullable final FirebaseFirestoreException e) {
                        if (e != null) {
                            parentActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.w(TAG, "Listen failed.", e);
                                    noBlogTV.setText(errorTxt);
                                    noBlogTV.setVisibility(View.VISIBLE);
                                }
                            });
                            return;
                        }

                        blogs.clear();

                        if (snapshots != null && !snapshots.isEmpty()) {
                            for (QueryDocumentSnapshot snapshot : snapshots) {
                                Blog tempBlog = Blog.snapshotToBlog(parentActivity,snapshot);
                                if(tempBlog != null){
                                    blogs.add(tempBlog);
                                }
                            }

                            parentActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    blogAdapter.notifyDataSetChanged();
                                    noBlogTV.setVisibility(View.GONE);
                                }
                            });
                        } else {
                            parentActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    blogs.clear();
                                    blogAdapter.notifyDataSetChanged();

                                    noBlogTV.setText(emptyTxt);
                                    noBlogTV.setVisibility(View.VISIBLE);
                                }
                            });
                        }
                    }
                });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(blogListener != null){
                    blogListener.remove();
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.all_blogs));
                parentActivity.selectChat();
            }
        });
    }
}
