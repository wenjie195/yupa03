package com.vidatechft.yupa.blog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.BlogRecyclerGridAdapter;
import com.vidatechft.yupa.classes.Blog;
import com.vidatechft.yupa.dialogFragments.BlogUploadCoverDialogFragment;
import com.vidatechft.yupa.dialogFragments.ShowImageDialogFragment;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;

public class BlogDashboardFragment extends Fragment implements View.OnClickListener{
    public static final String TAG = BlogDashboardFragment.class.getName();
    private MainActivity parentActivity;

    private ArrayList<Blog> draftBlogs = new ArrayList<>();
    private ArrayList<Blog> publishBlogs = new ArrayList<>();
    private ArrayList<Blog> unpublishBlogs = new ArrayList<>();
    private RecyclerView draftRV,publishRV,unpublishRV;
    private LinearLayout draftLL,publishLL,unpublishLL;
    private BlogRecyclerGridAdapter draftAdapter,publishAdapter,unpublishAdapter;

    private ArrayList<Blog> allBlogs = new ArrayList<>();
    private ListenerRegistration blogListener;

    private String loadingTxt,emptyTxt,errorTxt;
    private TextView noBlogTV;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static BlogDashboardFragment newInstance(MainActivity parentActivity) {
        BlogDashboardFragment fragment = new BlogDashboardFragment();

        fragment.parentActivity = parentActivity;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_blog_dashboard, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.showBtmNav();

                loadingTxt = parentActivity.getString(R.string.blog_loading);
                emptyTxt = parentActivity.getString(R.string.blog_empty);
                errorTxt = parentActivity.getString(R.string.blog_err_loading);

                draftLL = view.findViewById(R.id.draftLL);
                draftRV = view.findViewById(R.id.draftRV);
                publishLL = view.findViewById(R.id.publishLL);
                publishRV = view.findViewById(R.id.publishRV);
                unpublishLL = view.findViewById(R.id.unpublishLL);
                unpublishRV = view.findViewById(R.id.unpublishRV);
                noBlogTV = view.findViewById(R.id.noBlogTV);
                TextView addBlogTV = view.findViewById(R.id.addBlogTV);

                addBlogTV.setOnClickListener(BlogDashboardFragment.this);

                setUpGridAdapter();

                if(allBlogs.size() <= 0){
                    getAllBlogs();
                }else{
                    if(draftBlogs.isEmpty()){
                        draftLL.setVisibility(View.GONE);
                    }else{
                        draftLL.setVisibility(View.VISIBLE);
                    }

                    if(publishBlogs.isEmpty()){
                        publishLL.setVisibility(View.GONE);
                    }else{
                        publishLL.setVisibility(View.VISIBLE);
                    }

                    if(unpublishBlogs.isEmpty()){
                        unpublishLL.setVisibility(View.GONE);
                    }else{
                        unpublishLL.setVisibility(View.VISIBLE);
                    }

                    noBlogTV.setVisibility(View.GONE);
                }
            }
        });

        return view;
    }

    private void setUpGridAdapter(){
        draftAdapter = new BlogRecyclerGridAdapter(parentActivity, draftBlogs,true);

        draftRV.setAdapter(draftAdapter);
        draftRV.setNestedScrollingEnabled(false);
        draftRV.setLayoutManager(new GridLayoutManager(parentActivity, 2));

        publishAdapter = new BlogRecyclerGridAdapter(parentActivity, publishBlogs,true);

        publishRV.setAdapter(publishAdapter);
        publishRV.setNestedScrollingEnabled(false);
        publishRV.setLayoutManager(new GridLayoutManager(parentActivity, 2));

        unpublishAdapter = new BlogRecyclerGridAdapter(parentActivity, unpublishBlogs,true);

        unpublishRV.setAdapter(unpublishAdapter);
        unpublishRV.setNestedScrollingEnabled(false);
        unpublishRV.setLayoutManager(new GridLayoutManager(parentActivity, 2));
    }

    private void getAllBlogs(){
        //todo the recycler view needs to have lazy load
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                noBlogTV.setText(loadingTxt);
                noBlogTV.setVisibility(View.VISIBLE);
            }
        });

        blogListener = GeneralFunction.getFirestoreInstance()
                .collection(Blog.URL_BLOG)
                .whereEqualTo(Blog.USER_ID, parentActivity.uid)
                .orderBy(Blog.DATE_UPDATED, Query.Direction.DESCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot snapshots, @javax.annotation.Nullable final FirebaseFirestoreException e) {
                        if (e != null) {
                            parentActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.w(TAG, "Listen failed.", e);
                                    noBlogTV.setText(errorTxt);
                                    noBlogTV.setVisibility(View.VISIBLE);
                                }
                            });
                            return;
                        }

                        allBlogs.clear();
                        draftBlogs.clear();
                        publishBlogs.clear();
                        unpublishBlogs.clear();

                        if (snapshots != null && !snapshots.isEmpty()) {
                            for (QueryDocumentSnapshot snapshot : snapshots) {
                                Blog tempBlog = Blog.snapshotToBlog(parentActivity,snapshot);
                                if(tempBlog != null){
                                    allBlogs.add(tempBlog);
                                    if(tempBlog.isDraft != null && tempBlog.isDraft){
                                        draftBlogs.add(tempBlog);
                                    }else if(tempBlog.isPublish != null){
                                        if(tempBlog.isPublish){
                                            publishBlogs.add(tempBlog);
                                        }else{
                                            unpublishBlogs.add(tempBlog);
                                        }
                                    }
                                }
                            }

                            parentActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if(draftBlogs.isEmpty()){
                                        draftLL.setVisibility(View.GONE);
                                    }else{
                                        draftLL.setVisibility(View.VISIBLE);
                                    }

                                    if(publishBlogs.isEmpty()){
                                        publishLL.setVisibility(View.GONE);
                                    }else{
                                        publishLL.setVisibility(View.VISIBLE);
                                    }

                                    if(unpublishBlogs.isEmpty()){
                                        unpublishLL.setVisibility(View.GONE);
                                    }else{
                                        unpublishLL.setVisibility(View.VISIBLE);
                                    }

                                    draftAdapter.notifyDataSetChanged();
                                    publishAdapter.notifyDataSetChanged();
                                    unpublishAdapter.notifyDataSetChanged();

                                    if(draftBlogs.isEmpty() && publishBlogs.isEmpty() && unpublishBlogs.isEmpty()){
                                        noBlogTV.setText(emptyTxt);
                                        noBlogTV.setVisibility(View.VISIBLE);
                                    }else{
                                        noBlogTV.setVisibility(View.GONE);
                                    }
                                }
                            });
                        } else {
                            parentActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    allBlogs.clear();

                                    draftBlogs.clear();
                                    publishBlogs.clear();
                                    unpublishBlogs.clear();

                                    draftAdapter.notifyDataSetChanged();
                                    publishAdapter.notifyDataSetChanged();
                                    unpublishAdapter.notifyDataSetChanged();

                                    draftLL.setVisibility(View.GONE);
                                    publishLL.setVisibility(View.GONE);
                                    unpublishLL.setVisibility(View.GONE);

                                    noBlogTV.setText(emptyTxt);
                                    noBlogTV.setVisibility(View.VISIBLE);
                                }
                            });
                        }
                    }
                });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(blogListener != null){
                    blogListener.remove();
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentActivity.setToolbarTitle(parentActivity.getString(R.string.blog_dashboard));
                parentActivity.selectChat();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.addBlogTV:
                Fragment addBlogFragment = AddBlogFragment.newInstance(parentActivity,null);
                GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, addBlogFragment).addToBackStack(AddBlogFragment.TAG).commit();
                break;
        }
    }
}
