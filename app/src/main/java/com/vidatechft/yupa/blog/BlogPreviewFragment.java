package com.vidatechft.yupa.blog;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.Blog;
import com.vidatechft.yupa.dialogFragments.BlogUploadCoverDialogFragment;
import com.vidatechft.yupa.utilities.FilePath;
import com.vidatechft.yupa.utilities.GeneralFunction;

import net.dankito.richtexteditor.android.RichTextEditor;
import net.dankito.richtexteditor.callback.GetCurrentHtmlCallback;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;

import static android.app.Activity.RESULT_OK;

//todo make uploaded image smaller
public class BlogPreviewFragment extends Fragment implements View.OnClickListener,AdapterView.OnItemSelectedListener{
    public static final String TAG = BlogPreviewFragment.class.getName();
    private MainActivity parentActivity;
    private Blog blog;

    private RichTextEditor editor;
    private EditText titleET,categoriesET;
    private Spinner categoriesSPN;
    private Button btn_publish;
    private boolean isCategorySelected = false;

    private ArrayList<String> uploadedUrlList = new ArrayList<>();
    private int uploadEachPhotoCount = 0;
    private boolean uploadHasError = false;
    private ImageView coverPhotoIV;
    private String coverUrl;
    private boolean isPublishMessage = false;
    private BlogPreviewFragment blogPreviewFragment;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static BlogPreviewFragment newInstance(MainActivity parentActivity,Blog blog) {
        BlogPreviewFragment fragment = new BlogPreviewFragment();
        fragment.parentActivity = parentActivity;
        fragment.blog = blog;
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_blog_preview, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                editor = view.findViewById(R.id.editor);
                ConstraintLayout buttonCL = view.findViewById(R.id.buttonCL);
                btn_publish = view.findViewById(R.id.btn_publish);
                Button btn_saveAsDraft = view.findViewById(R.id.btn_saveAsDraft);
                titleET = view.findViewById(R.id.titleET);
                categoriesET = view.findViewById(R.id.categoriesET);
                categoriesSPN = view.findViewById(R.id.categoriesSPN);

                coverPhotoIV = view.findViewById(R.id.coverPhotoIV);

                editor.setOnClickListener(BlogPreviewFragment.this);
                coverPhotoIV.setOnClickListener(BlogPreviewFragment.this);
                categoriesET.setOnClickListener(BlogPreviewFragment.this);

                editor.setEditorFontSize(16);
//                editor.setPadding((4 * (int) getResources().getDisplayMetrics().density));
                editor.setInputEnabled(false);
                editor.enterViewingMode();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    editor.setNestedScrollingEnabled(false);
                }

                if(blog == null || blog.id == null || blog.content == null){
                    buttonCL.setVisibility(View.GONE);
                }else{
                    initCategorySpinner();

                    if(blog.title != null){
                        titleET.setText(blog.title);
                    }

                    if(blog.category != null){
                        for(int i = 0; i < categoriesSPN.getAdapter().getCount(); i++){
                            if(TextUtils.equals(blog.category,categoriesSPN.getAdapter().getItem(i).toString())){
                                categoriesSPN.setSelection(i);
                                isCategorySelected = true;
                                break;
                            }
                        }
                    }

                    editor.setHtml(blog.content);

                    setPublishBtnText();

                    buttonCL.setVisibility(View.VISIBLE);
                    btn_publish.setOnClickListener(BlogPreviewFragment.this);
                    btn_saveAsDraft.setOnClickListener(BlogPreviewFragment.this);
                }
                if(blog.coverPhoto != null){
                    GeneralFunction.GlideImageSetting(parentActivity,Uri.parse(blog.coverPhoto),coverPhotoIV);
                }

                view.findViewById(R.id.btn_back).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        parentActivity.onBackPressed();
                    }
                });
            }
        });

        return view;
    }

    //*****************************************************INIT THE CATEGORY SPINNERS START************************************************************/
    private void initCategorySpinner(){
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(parentActivity, android.R.layout.simple_spinner_item, parentActivity.getResources().getStringArray(R.array.blogCategories)){
            @Override
            public boolean isEnabled(int position){
                return position != 0;
            }

            @Override
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent)
            {
                View spinnerview = super.getDropDownView(position, convertView, parent);

                if(position == 0) {
                    isCategorySelected = false;
                    //Set the disable spinner item color fade .
                    categoriesET.setTextColor(Color.parseColor("#bcbcbb"));
                }
                else {
                    isCategorySelected = true;
                    categoriesET.setTextColor(Color.BLACK);

                }
                return spinnerview;
            }
        }; //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categoriesSPN.setAdapter(spinnerArrayAdapter);
        categoriesSPN.setSelection(0);
        categoriesSPN.setOnItemSelectedListener(BlogPreviewFragment.this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Object item = parent.getItemAtPosition(position);
        if (item != null) {
            String country = categoriesSPN.getSelectedItem().toString();
            categoriesET.setText(country);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        categoriesET.setHint(R.string.choose_category);
    }

    private void setPublishBtnText(){
        if(blog.isPublish != null && blog.isPublish){
            btn_publish.setText(parentActivity.getString(R.string.unpublish));
        }else{
            btn_publish.setText(parentActivity.getString(R.string.publish));
        }
    }

    //*****************************************************INIT THE CATEGORY SPINNERS END************************************************************/

    private void extractFilePathsFromHtmlAndUpload(String htmlContent) {
        String mTitle = titleET.getText().toString().trim();
        String mCategory = categoriesET.getText().toString().trim();
        if(isValidInput(mTitle,mCategory)){
            blog.title = mTitle;
            blog.category = mCategory;

        }else{
            return;
        }

        parentActivity.initializeLoadingDialog(parentActivity.getString(R.string.add_blog_processing),false);
        parentActivity.controlLoadingDialog(true,parentActivity);

        String SEPERATOR = "<img src=\"";

        final ArrayList<String> listOfFilePathsToUpload = new ArrayList<>();
        String[] splitStr = htmlContent.split(SEPERATOR);
        //I start the int = 1 because the first one is not the one i want. inside wont have file paths
        for(int i = 1; i < splitStr.length; i++){
            String[] filePathSplitStr;
            filePathSplitStr = (splitStr[i].split("\" alt"));
            //must not contain http or https and must have storage/ to consider it as a file to upload
            //must include the symbol / because firebasestorage also got the word storage and also file names cant have the symbol /
            if(filePathSplitStr[0].contains("storage/") && !filePathSplitStr[0].contains("https://") && !filePathSplitStr[0].contains("http://")){
                listOfFilePathsToUpload.add(filePathSplitStr[0]);
            }
        }

        if(listOfFilePathsToUpload.isEmpty()){
            SEPERATOR = "<img class=\"resizable\" src=\"";

            String[] splitResizableStr = htmlContent.split(SEPERATOR);
            //I start the int = 1 because the first one is not the one i want. inside wont have file paths
            for(int i = 1; i < splitResizableStr.length; i++){
                String[] filePathSplitStr;
                filePathSplitStr = (splitResizableStr[i].split("\" alt"));
                if(filePathSplitStr[0].contains("storage/") && !filePathSplitStr[0].contains("https://") && !filePathSplitStr[0].contains("http://")){
                    listOfFilePathsToUpload.add(filePathSplitStr[0]);
                }
            }
        }

        uploadEachPhotoCount = 0;
        if(listOfFilePathsToUpload.isEmpty()){
            //if still empty means really no image
            publishBlog(false);
        }else{
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    parentActivity.setLoadingDialogText(String.format(parentActivity.getString(R.string.add_blog_uploading_blog_pictures),uploadEachPhotoCount,listOfFilePathsToUpload.size()));
                }
            }, 100);

            uploadedUrlList.clear();
            uploadHasError = false;
            for(int x = 0; x < listOfFilePathsToUpload.size(); x++){
                uploadedUrlList.add(null);
                uploadEachPhoto(listOfFilePathsToUpload.get(x),x,listOfFilePathsToUpload.size());
            }
        }

        if(coverUrl != null && btn_publish.getText() != parentActivity.getString(R.string.unpublish)){
            uploadCoverImage();
        }
    }

    private void uploadEachPhoto(final String filePath, final int position, final int size){
        if(filePath != null){
            final StorageReference ref = GeneralFunction.getFirebaseStorageInstance()
                    .child(Blog.URL_BLOG)
                    .child(parentActivity.uid)
                    .child(blog.id)
                    .child(String.valueOf(System.currentTimeMillis()) + "_" + String.valueOf(position));

            UploadTask uploadTask = ref.putBytes(GeneralFunction.getCompressedBitmap(parentActivity, filePath));

            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    handleEachUploadComplete(size);
                    uploadHasError = true;
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.add_blog_error_exceed_upload_size),R.drawable.notice_bad,TAG);
                    Log.e(TAG,exception.getMessage());
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                    ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri downloadUri) {
                            if (downloadUri != null) {
//                                if(position == 0){
//                                    blog.coverPhoto = downloadUri.toString();
//                                }

                                blog.content = blog.content.replace(filePath,downloadUri.toString());
                                uploadedUrlList.set(position,downloadUri.toString());
                                handleEachUploadComplete(size);
                            }
                        }
                    });
                }
            });
        }
    }

    private void handleEachUploadComplete(int size){
        uploadEachPhotoCount++;
        parentActivity.setLoadingDialogText(String.format(parentActivity.getString(R.string.add_blog_uploading_blog_pictures),uploadEachPhotoCount,size));

        if(uploadEachPhotoCount == size){
            boolean urlListHasError = false;

            for(String url : uploadedUrlList){
                if(url == null){
                    urlListHasError = true;
                    break;
                }
            }

            if(urlListHasError || uploadHasError){
                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.add_blog_error_upload),R.drawable.notice_bad,TAG);
                parentActivity.controlLoadingDialog(false,parentActivity);
            }else{
                publishBlog(true);
            }
        }
    }

    private void publishBlog(boolean hasPicture){
        if(hasPicture){
            parentActivity.setLoadingDialogText(parentActivity.getString(R.string.add_blog_uploading_blog_after_finished_picture));
        }else{
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    parentActivity.setLoadingDialogText(parentActivity.getString(R.string.add_blog_uploading_blog));
                }
            }, 100);
        }
        blog.isDraft = false;
        if(blog.isPublish != null && blog.isPublish){
            blog.isPublish = false;
        }else{
            blog.isPublish = true;
        }

        GeneralFunction.getFirestoreInstance()
                .collection(Blog.URL_BLOG)
                .document(blog.id)
                .set(blog).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        parentActivity.controlLoadingDialog(false,parentActivity);
                        if(GeneralFunction.hasErrorDocOnComplete(parentActivity,task,TAG)){
                            return;
                        }

                        if(blog.isPublish != null && blog.isPublish){
                            if(!isPublishMessage){
                                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getResources().getString(R.string.successful_publish_blog),R.drawable.notice_good,TAG);
                            }
                        }else{
                            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getResources().getString(R.string.successful_unpublish_blog),R.drawable.notice_good,TAG);
                        }

                        setPublishBtnText();
                    }
                });
    }

    private void saveAsDraft(){
        blog.title = titleET.getText().toString().trim();
        blog.category = categoriesET.getText().toString().trim();
        blog.isDraft = true;
        blog.isPublish = false;

        GeneralFunction.getFirestoreInstance()
                .collection(Blog.URL_BLOG)
                .document(blog.id)
                .set(blog).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        parentActivity.controlLoadingDialog(false,parentActivity);
                        if(GeneralFunction.hasErrorDocOnComplete(parentActivity,task,TAG)){
                            return;
                        }
                        Toast.makeText(parentActivity,parentActivity.getString(R.string.successful_save_as_draft_blog),Toast.LENGTH_SHORT).show();

                        setPublishBtnText();
                        if(coverUrl != null){
                            uploadCoverImage();
                        }
                    }
                });
    }

    //*****************************************************INPUT VALIDATION START************************************************************/
    private boolean isValidInput(String mTitle, String mCategory){
        return  GeneralFunction.checkAndReturnString(mTitle,parentActivity.getString(R.string.add_blog_error_title),parentActivity)
                &&
                checkSpinner(mCategory)
                &&
                checkImage();
    }

    private boolean checkImage(){
        if(blog.coverPhoto != null){
            return true;
        }
        else{
            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.blog_error_upload_cover_image),R.drawable.notice_bad,"invalid_string");
            return false;
        }
    }

    private boolean checkSpinner(String mCategory){
        if(mCategory != null && isCategorySelected){
            return true;
        }
        else{
            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.add_blog_error_category),R.drawable.notice_bad,"invalid_string");
            return false;
        }
    }
    //*****************************************************INPUT VALIDATION END************************************************************/

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.hideBtmNav();
        parentActivity.hideToolbar();
    }

    @Override
    public void onPause() {
        super.onPause();
        parentActivity.showBtmNav();
        parentActivity.showToolbar();
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_publish:
                extractFilePathsFromHtmlAndUpload(blog.content);
                break;
            case R.id.btn_saveAsDraft:
                saveAsDraft();
                break;
            case R.id.categoriesET:
                categoriesSPN.performClick();
                break;
            case R.id.coverPhotoIV:
                if (GeneralFunction.isStorageAndCameraPermitted(parentActivity.getApplicationContext(), parentActivity)) {
                    GeneralFunction.showFileChooserRatio169(parentActivity, this);
                }
                break;
        }
    }


    private void uploadCoverImage(){
        parentActivity.initializeProgressIndicator(parentActivity.getString(R.string.uploadEP_picture), false,blogPreviewFragment);
        parentActivity.controlProgressIndicator(true,BlogPreviewFragment.this);
        if(checkImage()){
            final StorageReference storeRef = GeneralFunction.getFirebaseStorageInstance().child(Blog.URL_BLOG + "/" + parentActivity.uid+"/"+ blog.id +"/"+ "cover_photo");
            UploadTask uploadTask = storeRef.putBytes(GeneralFunction.getCompressedBitmap(parentActivity,coverUrl));
//            UploadTask uploadTask = storeRef.putBytes(GeneralFunction.getCompressedBitmap(parentActivity, FilePath.getPath(parentActivity, Uri.parse(coverUrl))));
            uploadTask.addOnFailureListener(new OnFailureListener()
            {
                @Override
                public void onFailure(@NonNull Exception exception)
                {
                    GeneralFunction.openMessageNotificationDialog(parentActivity,"Error",R.drawable.notice_bad,TAG);

                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>()
            {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot)
                {
                    storeRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>()
                    {
                        @Override
                        public void onSuccess(Uri uri)
                        {
                            if(uri != null){
                                String coverPhotoUrl = uri.toString();
                                blog.coverPhoto = coverPhotoUrl;
                                HashMap<String, Object> blogMap = new HashMap<>();
                                blogMap.put(Blog.COVER_PHOTO, coverPhotoUrl);
                                blogMap.put(Blog.DATE_UPDATED, FieldValue.serverTimestamp());
                                if(blog.id != null){
                                    GeneralFunction.getFirestoreInstance()
                                            .collection(Blog.URL_BLOG)
                                            .document(blog.id)
                                            .update(blogMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            parentActivity.controlLoadingDialog(false,parentActivity);
                                            if(GeneralFunction.hasErrorDocOnComplete(parentActivity,task,TAG)){
                                                return;
                                            }
                                            coverUrl = null;
                                            isPublishMessage = false;
                                            parentActivity.controlProgressIndicator(false,BlogPreviewFragment.this);
                                            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getResources().getString(R.string.successful_publish_blog),R.drawable.notice_good,TAG);                                        }
                                    });
                                }
                            }
                        }
                    });
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    parentActivity.controlProgressIndicator(false,BlogPreviewFragment.this);
                    GeneralFunction.openMessageNotificationDialog(parentActivity,e.toString(),R.drawable.notice_bad,TAG);
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>()
            {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot)
                {
                    int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * 80) + 10;
                    parentActivity.updateProgress((float) totalProgress, BlogPreviewFragment.this);
                }
            });
        }
        else{
            parentActivity.controlProgressIndicator(false,BlogPreviewFragment.this);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode  == RESULT_OK) {
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                Uri resultUri = result.getUri();
                this.coverUrl = FilePath.getPath(parentActivity, resultUri);
                blog.coverPhoto = coverUrl;
                isPublishMessage = true;
                GeneralFunction.GlideImageSetting(parentActivity, resultUri,coverPhotoIV);
            }

            if (requestCode == CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE) {
                if (ContextCompat.checkSelfPermission(parentActivity, Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_DENIED) {
                    ActivityCompat.requestPermissions(parentActivity, new String[]{Manifest.permission.CAMERA}, requestCode);
                }
            }
        }
    }
}
