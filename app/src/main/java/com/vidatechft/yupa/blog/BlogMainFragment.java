package com.vidatechft.yupa.blog;

import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.database.core.Repo;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.DiscussionAdapter;
import com.vidatechft.yupa.adapter.HostRoomMapClusterListAdapter;
import com.vidatechft.yupa.adapter.RecommendedAdapter;
import com.vidatechft.yupa.classes.Blog;
import com.vidatechft.yupa.classes.BlogDiscussion;
import com.vidatechft.yupa.classes.Favourite;
import com.vidatechft.yupa.classes.Report;
import com.vidatechft.yupa.classes.User;
import com.vidatechft.yupa.dialogFragments.BlogBookmarkReportFragment;
import com.vidatechft.yupa.profileFragments.ProfileFragment;
import com.vidatechft.yupa.utilities.Config;
import com.vidatechft.yupa.utilities.GeneralFunction;
import com.vidatechft.yupa.utilities.KeyboardUtilities;

import net.dankito.richtexteditor.android.RichTextEditor;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import kotlin.Function;

public class BlogMainFragment extends Fragment implements View.OnClickListener{
    public static final String TAG = BlogMainFragment.class.getName();
    private MainActivity parentActivity;
    private Blog blog, tempBlog, tempBookmark;
    private ImageView blog_cover_image;
    private ImageView favIV,btn_like,btn_bookmark,btn_flag,profilePicIV,sendIV;
    private TextView nameTV,dateTV,bookmark_countTV,like_countTV;
    private Favourite favourite;
    private EditText discussionET;
    private RecommendedAdapter recommendedAdapter;
    private RecyclerView recommendedGalleryRV,discussionCommentRV;
    private ArrayList<BlogDiscussion>blogDiscussions = new ArrayList<>();
    private ListenerRegistration favListener,recommendedGalleryListener,discussionListener;
    private List<Boolean> isAllDownloaded = new ArrayList<>();
    private DiscussionAdapter discussionAdapter;
    private int count = 0;
    private ImageButton shareIB;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static BlogMainFragment newInstance(MainActivity parentActivity, Blog blog) {
        BlogMainFragment fragment = new BlogMainFragment();
        fragment.parentActivity = parentActivity;
        fragment.blog = blog;
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_blog_main, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //TextView
                TextView titleTV = view.findViewById(R.id.titleTV);
                bookmark_countTV = view.findViewById(R.id.bookmark_countTV);
                nameTV = view.findViewById(R.id.nameTV);
                like_countTV = view.findViewById(R.id.like_countTV);
                dateTV = view.findViewById(R.id.dateTV);
                //Rich Text Editor
                RichTextEditor editor = view.findViewById(R.id.editor);
                //ImageView
                ImageView blog_cover_image = view.findViewById(R.id.blog_cover_image);
                favIV = view.findViewById(R.id.favIV);
                btn_like = view.findViewById(R.id.btn_like);
                btn_bookmark = view.findViewById(R.id.btn_bookmark);
                btn_flag = view.findViewById(R.id.btn_flag);
                profilePicIV = view.findViewById(R.id.profilePicIV);
                sendIV = view.findViewById(R.id.sendIV);
                //EditText
                discussionET = view.findViewById(R.id.discussionET);
                //RecyclerView
                recommendedGalleryRV = view.findViewById(R.id.recommendedGalleryRV);
                discussionCommentRV = view.findViewById(R.id.discussionCommentRV);
                //ImageButton
                shareIB = view.findViewById(R.id.shareIB);

                //Onclick Listener
                favIV.setOnClickListener(BlogMainFragment.this);
                sendIV.setOnClickListener(BlogMainFragment.this);
                btn_like.setOnClickListener(BlogMainFragment.this);
                btn_bookmark.setOnClickListener(BlogMainFragment.this);
                btn_flag.setOnClickListener(BlogMainFragment.this);
                shareIB.setOnClickListener(BlogMainFragment.this);
                ImageView backIV = view.findViewById(R.id.backIV);
                backIV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        parentActivity.onBackPressed();
                    }
                });

                shareIB.setBackground(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.ic_share,R.color.white));

                tempBlog = new Blog();
                tempBookmark = new Blog();
                if(blog != null){
                    if(blog.title != null){
                        titleTV.setText(blog.title);
                    }
                    if(blog.coverPhoto != null){
                        GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(blog.coverPhoto),blog_cover_image);
                    }

                    editor.setEditorFontSize(16);
//                    editor.setPadding((4 * (int) getResources().getDisplayMetrics().density));
                    editor.setInputEnabled(false);
                    editor.enterViewingMode();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        editor.setNestedScrollingEnabled(false);
                    }

                    if(blog.content != null){
                        editor.setHtml(blog.content.replace("class=\"resizable\" ",""));
                    }
                    if(blog.userId != null){
                        GeneralFunction.getFirestoreInstance()
                                .collection(User.URL_USER)
                                .document(blog.userId)
                                .get()
                                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                        if(task.isSuccessful()){
                                            if(task.getResult() != null){
                                              final User tempUser = new User(parentActivity,task.getResult());
                                                String fullText = parentActivity.getResources().getString(R.string.blog_by) + " <b><font color=#000000>" + tempUser.name + "</font></b> " + parentActivity.getResources().getString(R.string.blog_contributor) ;
                                                nameTV.setText(Html.fromHtml(fullText));

                                              nameTV.setOnClickListener(new View.OnClickListener() {
                                                  @Override
                                                  public void onClick(View v) {
                                                      Fragment profileFragment = ProfileFragment.newInstance(parentActivity,tempUser,false);
                                                      GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, profileFragment).addToBackStack(ProfileFragment.TAG).commit();
                                                  }
                                              });
                                            }
                                        }
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                GeneralFunction.openMessageNotificationDialog(parentActivity,e.toString(),R.drawable.notice_bad,TAG);
                            }
                        });
                    }
                    if(blog.dateUpdated != null){
                        Calendar mCalendar = toCalendar(blog.dateUpdated.toDate());
                        String mDate = getCurrentDateInSpecificFormat(mCalendar);
                        dateTV.setText(mDate);
                    }
                    if(parentActivity.uid != null){
                        GeneralFunction.getFirestoreInstance()
                                .collection(User.URL_USER)
                                .document(parentActivity.uid)
                                .get()
                                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                        if(task.isSuccessful()){
                                            if(task.getResult() != null){
                                                User tempUser = task.getResult().toObject(User.class);
                                                if(tempUser != null){
                                                    if(tempUser.profilePicUrl != null){
                                                        GeneralFunction.GlideImageSetting(parentActivity, Uri.parse(tempUser.profilePicUrl),profilePicIV);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                GeneralFunction.openMessageNotificationDialog(parentActivity,e.toString(),R.drawable.notice_bad,TAG);
                            }
                        });
                    }
                    if(blog.likeCount != null){
                        String totalLikeCount = GeneralFunction.shortenNumbersFormat((long) blog.likeCount);
                        like_countTV.setText(totalLikeCount);
                    }

                    if(blog.bookmarkCount != null){
                        String totalLikeCount = GeneralFunction.shortenNumbersFormat((long) blog.bookmarkCount);
                        bookmark_countTV.setText(totalLikeCount);
                    }
                }

                if(GeneralFunction.isGuest()){
                    view.findViewById(R.id.userCommentCL).setVisibility(View.GONE);
                    view.findViewById(R.id.discussionTV).setVisibility(View.GONE);
                    view.findViewById(R.id.allCommentTV).setVisibility(View.GONE);
                }
            }
        });

        return view;
    }

    private void handleLikeAction(boolean forceLike){
        DocumentReference favRef;

        if(tempBlog == null || tempBlog.isFavourite == null ||tempBlog.id == null){
            favRef = GeneralFunction.getFirestoreInstance()
                    .collection(Favourite.URL_FAVOURITE)
                    .document();

            favourite = new Favourite();

            tempBlog.id = favRef.getId();

            tempBlog.isFavourite = true;
        }else{
            favRef = GeneralFunction.getFirestoreInstance()
                    .collection(Favourite.URL_FAVOURITE)
                    .document(tempBlog.id);

            tempBlog.isFavourite = !tempBlog.isFavourite;
        }

        if(forceLike){
            tempBlog.isFavourite = true;
        }

        favourite.userId = parentActivity.uid;
        favourite.targetId = blog.id;
        favourite.type = Favourite.FAV_TYPE_BLOG;
        favourite.isLiked = tempBlog.isFavourite;
        HashMap<String,Object> favMap = Favourite.toMap(favourite);

        if(favourite.dateCreated == null){
            favMap.put(Favourite.DATE_CREATED, FieldValue.serverTimestamp());
        }
        favMap.put(Favourite.DATE_UPDATED, FieldValue.serverTimestamp());

        favRef.set(favMap);

        if(favourite.isLiked){
            favIV.setImageDrawable(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.ic_love_whole_red,R.color.red));
            btn_like.setBackground(parentActivity.getResources().getDrawable(R.drawable.ic_like_color_primary));
            int likecount = Integer.parseInt(like_countTV.getText().toString()) + 1;
            like_countTV.setText(String.valueOf(likecount));
        }else{
            favIV.setImageDrawable(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.ic_love,R.color.white));
            btn_like.setBackground(parentActivity.getResources().getDrawable(R.drawable.ic_like_grey));
            int likecount = Integer.parseInt(like_countTV.getText().toString()) - 1;
            like_countTV.setText(String.valueOf(likecount));
        }
    }

    private void handleBookmarkAction(boolean forceLike){
        DocumentReference favRef;

        if(tempBookmark == null || tempBookmark.isFavourite == null ||tempBookmark.id == null){
            favRef = GeneralFunction.getFirestoreInstance()
                    .collection(Favourite.URL_FAVOURITE)
                    .document();

            favourite = new Favourite();

            tempBookmark.id = favRef.getId();

            tempBookmark.isFavourite = true;
        }else{
            favRef = GeneralFunction.getFirestoreInstance()
                    .collection(Favourite.URL_FAVOURITE)
                    .document(tempBookmark.id);

            tempBookmark.isFavourite = !tempBookmark.isFavourite;
        }

        if(forceLike){
            tempBookmark.isFavourite = true;
        }

        favourite.userId = parentActivity.uid;
        favourite.targetId = blog.id;
        favourite.type = Favourite.FAV_TYPE_BLOG_BOOKMARK;
        favourite.isLiked = tempBookmark.isFavourite;
        HashMap<String,Object> favMap = Favourite.toMap(favourite);

        if(favourite.dateCreated == null){
            favMap.put(Favourite.DATE_CREATED, FieldValue.serverTimestamp());
        }
        favMap.put(Favourite.DATE_UPDATED, FieldValue.serverTimestamp());

        favRef.set(favMap);
        
        if(favourite.isLiked){
            btn_bookmark.setBackground(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.graystar,R.color.pure_orange));
            int likecount = Integer.parseInt(bookmark_countTV.getText().toString()) + 1;
            bookmark_countTV.setText(String.valueOf(likecount));
            Toast.makeText(parentActivity,parentActivity.getResources().getString(R.string.blog_bookmarks_added),Toast.LENGTH_SHORT).show();
        }else{
            btn_bookmark.setBackground(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.graystar,R.color.gray));
            int likecount = Integer.parseInt(bookmark_countTV.getText().toString()) - 1;
            bookmark_countTV.setText(String.valueOf(likecount));
            Toast.makeText(parentActivity,parentActivity.getResources().getString(R.string.blog_bookmarks_removed),Toast.LENGTH_SHORT).show();
        }
    }

    private void favSetAndGet(){

        favListener = GeneralFunction.getFirestoreInstance()
                .collection(Favourite.URL_FAVOURITE)
                .whereEqualTo(Favourite.TYPE,Favourite.FAV_TYPE_BLOG)
                .whereEqualTo(Favourite.TARGET_ID, blog.id)
                .whereEqualTo(Favourite.USER_ID, parentActivity.uid)
                .limit(1)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w(TAG, "Listen failed.", e);
                            return;
                        }

                        String source = queryDocumentSnapshots != null && queryDocumentSnapshots.getMetadata().hasPendingWrites()
                                ? "Local" : "Server";
                        Log.d(TAG,source);

                        if(queryDocumentSnapshots != null){
                            if(!queryDocumentSnapshots.isEmpty()){
                                for (QueryDocumentSnapshot snapshot : queryDocumentSnapshots) {
                                    favourite = snapshot.toObject(Favourite.class);
                                    tempBlog.id = snapshot.getId();

                                    tempBlog.isFavourite = favourite.isLiked != null && favourite.isLiked;

                                    if(tempBlog.isFavourite){
                                        favIV.setImageDrawable(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.ic_love_whole_red,R.color.red));
                                        btn_like.setBackground(parentActivity.getResources().getDrawable(R.drawable.ic_like_color_primary));
                                    }else{
                                        favIV.setImageDrawable(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.ic_love,R.color.white));
                                        btn_like.setBackground(parentActivity.getResources().getDrawable(R.drawable.ic_like_grey));
                                    }
                                }

                            }
                        }
                    }
                });

    }

    private void getBookmark(){

        GeneralFunction.getFirestoreInstance()
                .collection(Favourite.URL_FAVOURITE)
                .whereEqualTo(Favourite.TYPE,Favourite.FAV_TYPE_BLOG_BOOKMARK)
                .whereEqualTo(Favourite.TARGET_ID, blog.id)
                .whereEqualTo(Favourite.USER_ID, parentActivity.uid)
                .limit(1)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                        if(e != null){
                            GeneralFunction.openMessageNotificationDialog(parentActivity,e.getMessage(),R.drawable.notice_bad,TAG);
                            return;
                        }
                        if(queryDocumentSnapshots != null){
                            if(!queryDocumentSnapshots.isEmpty()){
                                for (QueryDocumentSnapshot snapshot : queryDocumentSnapshots) {
                                    favourite = snapshot.toObject(Favourite.class);
                                    tempBookmark.id = snapshot.getId();

                                    tempBookmark.isFavourite = favourite.isLiked != null && favourite.isLiked;

                                    if(tempBookmark.isFavourite){
                                        btn_bookmark.setBackground(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.graystar,R.color.pure_orange));
                                    }else{
                                        btn_bookmark.setBackground(GeneralFunction.getTintedDrawable(parentActivity,R.drawable.graystar,R.color.gray));
                                    }
                                }

                            }
                        }
                    }
                });

    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.hideBtmNav();
        parentActivity.hideToolbar();

        favSetAndGet();
        getBookmark();
        getRecommended();
        getDiscussion();
//        populateLikeCount();
//        populateBookmarkCount();
    }

    @Override
    public void onPause() {
        super.onPause();
        parentActivity.showBtmNav();
        parentActivity.showToolbar();
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.favIV:
                if(GeneralFunction.isGuest()){
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_guest_restrict_usage),R.drawable.notice_bad,TAG);
                    return;
                }
                handleLikeAction(false);
                break;
            case R.id.btn_like:
                if(GeneralFunction.isGuest()){
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_guest_restrict_usage),R.drawable.notice_bad,TAG);
                    return;
                }
                handleLikeAction(false);
                break;
            case R.id.btn_bookmark:
                if(GeneralFunction.isGuest()){
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_guest_restrict_usage),R.drawable.notice_bad,TAG);
                    return;
                }
                handleBookmarkAction(false);
                break;
            case R.id.btn_flag:
                if(GeneralFunction.isGuest()){
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_guest_restrict_usage),R.drawable.notice_bad,TAG);
                    return;
                }
                checkReportSubmited();
                break;
            case R.id.sendIV:
                if(GeneralFunction.isGuest()){
                    GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.error_please_guest_restrict_usage),R.drawable.notice_bad,TAG);
                    return;
                }
                KeyboardUtilities.hideSoftKeyboard(parentActivity);
                submitDiscussion();
                break;
            case R.id.shareIB:
                shareBlog();
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(favListener != null){
            favListener.remove();
        }

        if(recommendedGalleryListener != null){
            recommendedGalleryListener.remove();
        }
        if(discussionListener != null){
            discussionListener.remove();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(favListener != null){
            favListener.remove();
        }

        if(recommendedGalleryListener != null){
            recommendedGalleryListener.remove();
        }
        if(discussionListener != null){
            discussionListener.remove();
        }
    }

    private void checkReportSubmited(){

        GeneralFunction.getFirestoreInstance()
                .collection(Report.URL_REPORT)
                .whereEqualTo(Report.USER_ID,parentActivity.uid)
                .whereEqualTo(Report.REPORT_TYPE,Report.REPORT_TYPE_BLOG_DISCUSSION)
                .whereEqualTo(Report.TARGET_ID, blog.id)
                .whereEqualTo(Report.IS_REVIEWED,false)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if(task.isSuccessful()){
                            if(task.getResult() != null){
                                if(!task.getResult().isEmpty()){
                                    for(QueryDocumentSnapshot queryDocumentSnapshot : task.getResult()){
                                        Report report = queryDocumentSnapshot.toObject(Report.class);
                                        if(report.isReviewed != null && !report.isReviewed){
                                            GeneralFunction.Toast(parentActivity,parentActivity.getString(R.string.submitted_report));
                                        }
                                    }
                                }
                                else{
                                    FragmentManager fm = parentActivity.getSupportFragmentManager();
                                    BlogBookmarkReportFragment reviewReportFragment = BlogBookmarkReportFragment.newInstance(parentActivity,blog);
                                    reviewReportFragment.show(fm, TAG);
                                }
                            }
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if(e != null){
                    GeneralFunction.Toast(parentActivity,e.toString());
                }
            }
        });
    }

    private String getCurrentDateInSpecificFormat(Calendar currentCalDate) {
        String dayNumberSuffix = getDayNumberSuffix(currentCalDate.get(Calendar.DAY_OF_MONTH));
        DateFormat dateFormat = new SimpleDateFormat("d'" + dayNumberSuffix + "' MMM yyyy");
        return dateFormat.format(currentCalDate.getTime());
    }

    private String getDayNumberSuffix(int day) {
        if (day >= 11 && day <= 13) {
            return "th";
        }
        switch (day % 10) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }

    public static Calendar toCalendar(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }

    private void getRecommended(){
        final ArrayList<Blog> blogList = new ArrayList<>();
        recommendedGalleryListener = GeneralFunction.getFirestoreInstance()
                .collection(Blog.URL_BLOG)
                .whereEqualTo(Blog.IS_PUBLISH, true)
                .whereEqualTo(Blog.IS_DRAFT, false)
                .orderBy(Blog.LIKE_COUNT, Query.Direction.DESCENDING)
                .orderBy(Blog.DATE_UPDATED, Query.Direction.DESCENDING)
                .limit(5)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w(TAG, "Listen failed.", e);
                            return;
                        }
                        blogList.clear();
                        if(queryDocumentSnapshots != null){

                            if(!queryDocumentSnapshots.isEmpty()){
                                for(QueryDocumentSnapshot queryDocumentSnapshot : queryDocumentSnapshots){
                                    Blog tempBlog = queryDocumentSnapshot.toObject(Blog.class);
                                    tempBlog.id = queryDocumentSnapshot.getId();
                                    if(!tempBlog.id.equals(blog.id)){
                                        blogList.add(tempBlog);
                                    }
                                }
                                blogList.size();
                            }
                            parentActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    recommendedAdapter = new RecommendedAdapter(parentActivity, blogList);
                                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(parentActivity, LinearLayoutManager.HORIZONTAL,false);
                                    recommendedGalleryRV.setAdapter(recommendedAdapter);
                                    recommendedGalleryRV.setLayoutManager(linearLayoutManager);
                                    recommendedGalleryRV.setNestedScrollingEnabled(false);
                                    recommendedAdapter.notifyDataSetChanged();
                                    recommendedGalleryRV.requestLayout();
                                }
                            });
                        }
                    }
                });
    }

    private void submitDiscussion(){
        String discussionText = discussionET.getText().toString().trim();
        HashMap<String, Object> discussionMap = new HashMap<>();
        if(isValidInput(discussionText)){
            discussionMap.put(BlogDiscussion.DESCRIPTION_DISCUSSION,discussionText);
            discussionMap.put(BlogDiscussion.USER_ID,parentActivity.uid);
            discussionMap.put(BlogDiscussion.BLOG_ID,blog.id);
            discussionMap.put(BlogDiscussion.TYPE,BlogDiscussion.TYPE_DISCUSSION);
            discussionMap.put(BlogDiscussion.IS_REVIEWED,false);
            discussionMap.put(BlogDiscussion.DATE_CREATED,FieldValue.serverTimestamp());
            discussionMap.put(BlogDiscussion.DATE_UPDATED,FieldValue.serverTimestamp());

            DocumentReference discussionRef = GeneralFunction.getFirestoreInstance()
                    .collection(BlogDiscussion.URL_BLOG_DISCUSSION)
                    .document();
            discussionRef.set(discussionMap)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            discussionET.setText("");
                            Toast.makeText(parentActivity, parentActivity.getString(R.string.blog_successful_submit_discussion), Toast.LENGTH_SHORT).show();
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    GeneralFunction.openMessageNotificationDialog(parentActivity,e.toString(),R.drawable.notice_bad,TAG);
                }
            });
        }
    }
    private boolean isValidInput(String discussion){
        return  GeneralFunction.checkAndReturnString(discussion,parentActivity.getString(R.string.blog_error_empty_discussion),parentActivity);

    }

    private void getDiscussion(){
        discussionListener = GeneralFunction.getFirestoreInstance()
                .collection(BlogDiscussion.URL_BLOG_DISCUSSION)
                .whereEqualTo(BlogDiscussion.BLOG_ID,blog.id)
                .whereEqualTo(BlogDiscussion.TYPE, BlogDiscussion.TYPE_DISCUSSION)
                .orderBy(BlogDiscussion.DATE_CREATED, Query.Direction.ASCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                        if(e != null){
                            Log.e(TAG,e.getMessage());
                            return;
                        }
                        if(queryDocumentSnapshots != null && !queryDocumentSnapshots.isEmpty()){
                            blogDiscussions.clear();
                            ArrayList<Task<QuerySnapshot>> getReports = new ArrayList<>();
                            ArrayList<Task<DocumentSnapshot>> getUsers = new ArrayList<>();
                            for(QueryDocumentSnapshot queryDocumentSnapshot : queryDocumentSnapshots){
                                BlogDiscussion tempBlogDiscussion = queryDocumentSnapshot.toObject(BlogDiscussion.class);
                                tempBlogDiscussion.id = queryDocumentSnapshot.getId();
                                blogDiscussions.add(tempBlogDiscussion);

                                Task<QuerySnapshot> getFavourite = GeneralFunction.getFirestoreInstance().collection(Favourite.URL_FAVOURITE)
                                        .whereEqualTo(Favourite.TARGET_ID, tempBlogDiscussion.id)
                                        .whereEqualTo(Favourite.TARGET_UID, tempBlogDiscussion.userId)
                                        .whereEqualTo(Favourite.USER_ID, parentActivity.uid)
                                        .whereEqualTo(Favourite.TYPE, Favourite.FAV_TYPE_BLOG_DISCUSSION)
                                        .get();
                                getReports.add(getFavourite);

                                Task<DocumentSnapshot> getUser = GeneralFunction.getFirestoreInstance().collection(User.URL_USER)
                                        .document(tempBlogDiscussion.userId)
                                        .get();

                                getUsers.add(getUser);
                            }
                            blogDiscussions.size();
                            initAllDownloaded();
                            getAllUser(getUsers);
                            getAllReport(getReports);
                        }
                    }
                });

    }

    private void getAllReport(ArrayList<Task<QuerySnapshot>> getReports){
        if(getReports.isEmpty()){
            handleOnAllDownloaded(0);
            return;
        }

        Task<List<QuerySnapshot>> allTasks = Tasks.whenAllSuccess(getReports);
        allTasks.addOnSuccessListener(new OnSuccessListener<List<QuerySnapshot>>() {
            @Override
            public void onSuccess(List<QuerySnapshot> querySnapshots) {
                for(int i = 0; i < querySnapshots.size(); i++){
                    QuerySnapshot querySnapshot = querySnapshots.get(i);

                    Favourite favourite = null;
                    for(int x = 0; x < querySnapshot.size(); x++){
                        DocumentSnapshot documentSnapshot = querySnapshot.getDocuments().get(x);

                        if(documentSnapshot != null && documentSnapshot.exists()){
                            favourite = Favourite.snapshotToFavourite(parentActivity,documentSnapshot);
                        }
                    }
                    blogDiscussions.get(i).favourite = favourite;
                }
                handleOnAllDownloaded(0);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                handleOnAllDownloaded(0);
                Log.e(TAG,e.getMessage());
            }
        });
    }

    private void getAllUser(final ArrayList<Task<DocumentSnapshot>> getUsers) {
        if(getUsers.isEmpty()){
            handleOnAllDownloaded(1);
            return;
        }
        Task<List<DocumentSnapshot>> userTasks = Tasks.whenAllSuccess(getUsers);
        userTasks.addOnSuccessListener(new OnSuccessListener<List<DocumentSnapshot>>() {
            @Override
            public void onSuccess(List<DocumentSnapshot> documentSnapshots) {

                for(DocumentSnapshot queryDocumentSnapshot : documentSnapshots){
                    User user = null;
                    if(queryDocumentSnapshot != null && queryDocumentSnapshot.exists()){
                        user = queryDocumentSnapshot.toObject(User.class);
                        if(user != null){
                            user.id = queryDocumentSnapshot.getId();
                            blogDiscussions.get(count).user = user;
                        }
                    }
                    count ++;
                }
                count = 0;
                handleOnAllDownloaded(1);

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                handleOnAllDownloaded(1);
                Log.e(TAG,e.getMessage());
            }
        });
    }


    private void handleOnAllDownloaded(int downloadTaskId){
        isAllDownloaded.set(downloadTaskId,true);
        for(Boolean isDownloaded : isAllDownloaded){
            if(!isDownloaded){
                return;
            }
        }
        blogDiscussions.size();
        discussionAdapter = new DiscussionAdapter(parentActivity,blogDiscussions,blog);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(parentActivity);
        discussionCommentRV.setNestedScrollingEnabled(false);
        discussionCommentRV.setLayoutManager(linearLayoutManager);
        discussionCommentRV.setAdapter(discussionAdapter);
        discussionAdapter.notifyDataSetChanged();
        //when in scrollview prevent laggy

    }

    private void initAllDownloaded(){
        isAllDownloaded.clear();
        isAllDownloaded.add(false);
        isAllDownloaded.add(false);
    }

//    private void populateLikeCount (){
//        GeneralFunction.getFirestoreInstance()
//                .collection(Favourite.URL_FAVOURITE)
//                .whereEqualTo(Favourite.TYPE, Favourite.FAV_TYPE_BLOG)
//                .whereEqualTo(Favourite.TARGET_ID, blog.id)
//                .whereEqualTo(Favourite.IS_LIKED, true)
//                .get()
//                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
//                    @Override
//                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
//                        if(queryDocumentSnapshots != null){
//                            if(!queryDocumentSnapshots.isEmpty()){
//                                int totalLike = queryDocumentSnapshots.size();
//                                String totalLikeCount = GeneralFunction.shortenNumbersFormat((long) totalLike);
//                                like_countTV.setText(totalLikeCount);
//                            }
//                        }
//                    }
//                }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//                GeneralFunction.openMessageNotificationDialog(parentActivity,e.toString(),R.drawable.notice_bad, TAG);
//            }
//        });
//    }
//

//    private void populateBookmarkCount (){
//        GeneralFunction.getFirestoreInstance()
//                .collection(Favourite.URL_FAVOURITE)
//                .whereEqualTo(Favourite.TYPE, Favourite.FAV_TYPE_BLOG_BOOKMARK)
//                .whereEqualTo(Favourite.TARGET_ID, blog.id)
//                .get()
//                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
//                    @Override
//                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
//                        if(queryDocumentSnapshots != null){
//                            if(!queryDocumentSnapshots.isEmpty()){
//                                int totalLike = queryDocumentSnapshots.size();
//                                String totalLikeCount = GeneralFunction.shortenNumbersFormat((long) totalLike);
//                                bookmark_countTV.setText(totalLikeCount);
//                            }
//                        }
//                    }
//                }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//                GeneralFunction.openMessageNotificationDialog(parentActivity,e.toString(),R.drawable.notice_bad, TAG);
//            }
//        });
//    }

    private void shareBlog(){
        parentActivity.initializeLoadingDialog(parentActivity.getString(R.string.generating_blog_deep_link),false);
        parentActivity.controlLoadingDialog(true,parentActivity);

        Uri urlLink = Uri.parse("https://yupa.asia/?" +
                Config.SP_DEEP_LINK_TARGET_ID + "=" + blog.id + "&" +
                Config.SP_DEEP_LINK_TYPE + "=" + Config.SP_DEEP_LINK_TYPE_BLOG);
                GeneralFunction.generateDeepLink(parentActivity, urlLink,parentActivity.getString(R.string.deep_link_blog))
                .addOnCompleteListener(new OnCompleteListener<ShortDynamicLink>() {
                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                        parentActivity.controlLoadingDialog(false,parentActivity);
                        if(task.getException() == null && task.getResult() != null && task.getResult().getShortLink() != null){
                            try{
                                String shortDeepLink = task.getResult().getShortLink().toString();

                                GeneralFunction.shareDeepLink(parentActivity,
                                        String.format(parentActivity.getString(R.string.get_this_app_to_see_blog),parentActivity.getString(R.string.app_name)),
                                        task.getResult().getShortLink());
                            }catch (Exception e){
                                GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.err_generating_my_profile_deep_link),R.drawable.notice_bad,TAG);
                                e.printStackTrace();
                                Crashlytics.logException(e);
                            }
                        }else{
                            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.err_generating_my_profile_deep_link),R.drawable.notice_bad,TAG);
                        }
                    }
                });
    }
}
