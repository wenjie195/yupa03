package com.vidatechft.yupa.blog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.classes.Blog;
import com.vidatechft.yupa.utilities.GeneralFunction;
import com.vidatechft.yupa.utilities.KeyboardUtilities;

import net.dankito.richtexteditor.android.RichTextEditor;
import net.dankito.richtexteditor.android.toolbar.AllCommandsEditorToolbar;
import net.dankito.richtexteditor.callback.GetCurrentHtmlCallback;
import net.dankito.utils.android.permissions.IPermissionsService;
import net.dankito.utils.android.permissions.PermissionsService;

import org.jetbrains.annotations.NotNull;

public class AddBlogFragment extends Fragment implements View.OnClickListener{
    public static final String TAG = AddBlogFragment.class.getName();
    private MainActivity parentActivity;
    private Blog blog;

    private RichTextEditor editor;
    private AllCommandsEditorToolbar bottomGroupedCommandsToolbar;
    private IPermissionsService permissionsService;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static AddBlogFragment newInstance(MainActivity parentActivity, Blog blog) {
        AddBlogFragment fragment = new AddBlogFragment();
        fragment.parentActivity = parentActivity;
        fragment.blog = blog;
        return fragment;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_add_blog, container, false);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //Editor
                editor = view.findViewById(R.id.editor);
                //Button
                Button btn_saveAsDraft = view.findViewById(R.id.btn_saveAsDraft);
                Button btn_preview = view.findViewById(R.id.btn_preview);

                btn_saveAsDraft.setOnClickListener(AddBlogFragment.this);
                btn_preview.setOnClickListener(AddBlogFragment.this);
                editor.setOnClickListener(AddBlogFragment.this);

                permissionsService = new PermissionsService(parentActivity);
                // this is needed if you like to insert images so that the user gets asked for permission to access external storage if needed
                // see also onRequestPermissionsResult() below
                editor.setPermissionsService(permissionsService);

                bottomGroupedCommandsToolbar = view.findViewById(R.id.editorToolbar);
                bottomGroupedCommandsToolbar.setEditor(editor);

                editor.setEditorFontSize(16);
//                editor.setPadding((4 * (int) getResources().getDisplayMetrics().density));
                editor.setOnTouchListener(new View.OnTouchListener() {

                    public boolean onTouch(View v, MotionEvent event) {
                        if (editor.hasFocus()) {
                            v.getParent().requestDisallowInterceptTouchEvent(true);
                            switch (event.getAction() & MotionEvent.ACTION_MASK){
                                case MotionEvent.ACTION_SCROLL:
                                    v.getParent().requestDisallowInterceptTouchEvent(false);
                                    return true;
                            }
                            InputMethodManager imm = (InputMethodManager)
                                    parentActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                            if (imm != null) {
                                imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
                            }
                        }
                        return false;
                    }
                });

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    editor.setNestedScrollingEnabled(false);
                }

                if(blog != null && blog.content != null){
                    editor.setHtml(blog.content);
                }
                // only needed if you allow to automatically download remote images
//                editor.setDownloadImageConfig(new DownloadImageConfig(DownloadImageUiSetting.AllowSelectDownloadFolderInCode,
//                        new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "downloaded_images")));

                view.findViewById(R.id.btn_back).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        parentActivity.onBackPressed();
                    }
                });
            }
        });

        return view;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsService.onRequestPermissionsResult(requestCode, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void saveBlog(final boolean saveAsDraft) {
        DocumentReference blogRef;
        if(blog != null && blog.id != null){
            blog.dateUpdated = null; //set it to null so when uploaded to firebase it will auto convert to latest timestamp
            blogRef = GeneralFunction.getFirestoreInstance()
                    .collection(Blog.URL_BLOG)
                    .document(blog.id);
        }else{
            blog = new Blog(parentActivity.uid);
            blogRef = GeneralFunction.getFirestoreInstance()
                    .collection(Blog.URL_BLOG)
                    .document();
            blog.id = blogRef.getId();
        }

        final DocumentReference FINAL_BLOG_REF = blogRef;

        KeyboardUtilities.hideSoftKeyboard(parentActivity);

        if(!saveAsDraft){
            parentActivity.initializeLoadingDialog(parentActivity.getString(R.string.blog_generating_preview),false);
            parentActivity.controlLoadingDialog(true,parentActivity);
        }

        editor.getCurrentHtmlAsync(new GetCurrentHtmlCallback() {
            @Override
            public void htmlRetrieved(@NotNull String html) {
                if(blog != null){
                    blog.content = html;

                    if(saveAsDraft){
                        blog.isDraft = true;
                        blog.isPublish = false;

                        FINAL_BLOG_REF.set(blog).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(GeneralFunction.hasErrorDocOnComplete(parentActivity,task,TAG)){
                                    return;
                                }

                                Toast.makeText(parentActivity,parentActivity.getString(R.string.successful_save_as_draft_blog),Toast.LENGTH_SHORT).show();
                            }
                        });
                    }else{
                        parentActivity.controlLoadingDialog(false,parentActivity);

                        //this is to prevent the user from resizing pictures in preview mode fragment
                        Blog previewBlog = blog;
                        previewBlog.content = blog.content.replace("class=\"resizable\" ","");
                        Fragment blogPreviewFragment = BlogPreviewFragment.newInstance(parentActivity, previewBlog);
                        GeneralFunction.getFragmentTransaction(parentActivity).replace(R.id.mainFL, blogPreviewFragment).addToBackStack(BlogPreviewFragment.TAG).commit();
                    }
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.hideBtmNav();
        parentActivity.hideToolbar();
    }

    @Override
    public void onPause() {
        super.onPause();
        editor.getCurrentHtmlAsync(new GetCurrentHtmlCallback() {
            @Override
            public void htmlRetrieved(@NotNull String html) {
                if(blog != null){
                    blog.content = html;
                }
            }
        });
        parentActivity.showBtmNav();
        parentActivity.showToolbar();
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_saveAsDraft:
                saveBlog(true);
                break;

            case R.id.btn_preview:
                saveBlog(false);
                break;

            case R.id.editor:
                // show keyboard right at start up
                editor.focusEditorAndShowKeyboardDelayed(500);
                break;
        }
    }
}
