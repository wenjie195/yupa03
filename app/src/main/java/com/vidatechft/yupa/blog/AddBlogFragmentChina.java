//package com.vidatechft.yupa.blog;
//
//import android.Manifest;
//import android.annotation.SuppressLint;
//import android.content.Intent;
//import android.content.pm.PackageManager;
//import android.graphics.Color;
//import android.net.Uri;
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.support.v4.app.ActivityCompat;
//import android.support.v4.app.Fragment;
//import android.support.v4.content.ContextCompat;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.MotionEvent;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.WindowManager;
//import android.widget.AdapterView;
//import android.widget.ArrayAdapter;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.Spinner;
//
//import com.google.android.gms.tasks.OnCompleteListener;
//import com.google.android.gms.tasks.OnFailureListener;
//import com.google.android.gms.tasks.OnSuccessListener;
//import com.google.android.gms.tasks.Task;
//import com.google.firebase.firestore.DocumentReference;
//import com.google.firebase.storage.OnProgressListener;
//import com.google.firebase.storage.StorageReference;
//import com.google.firebase.storage.UploadTask;
//import com.theartofdev.edmodo.cropper.CropImage;
//import com.vidatechft.yupa.R;
//import com.vidatechft.yupa.activities.MainActivity;
//import com.vidatechft.yupa.classes.Blog;
//import com.vidatechft.yupa.utilities.FilePath;
//import com.vidatechft.yupa.utilities.GeneralFunction;
//
//import net.dankito.richtexteditor.android.RichTextEditor;
//import net.dankito.richtexteditor.android.toolbar.AllCommandsEditorToolbar;
//import net.dankito.richtexteditor.callback.GetCurrentHtmlCallback;
//import net.dankito.utils.android.permissions.IPermissionsService;
//import net.dankito.utils.android.permissions.PermissionsService;
//
//import org.jetbrains.annotations.NotNull;
//
//import java.util.HashMap;
//
//import static android.app.Activity.RESULT_OK;
//import static com.vidatechft.yupa.utilities.GeneralFunction.GlideImageSetting;
//
//public class AddBlogFragmentChina extends Fragment implements View.OnClickListener,AdapterView.OnItemSelectedListener{
//    public static final String TAG = AddBlogFragmentChina.class.getName();
//    private MainActivity parentActivity;
//    private RichTextEditor editor;
//    private AllCommandsEditorToolbar bottomGroupedCommandsToolbar;
//    private IPermissionsService permissionsService;
//    private Button btn_done;
//    private EditText titleET,categoriesET;
//    private ImageView upload_coverIB;
//    private Spinner categoriesSPN;
//    private String coverPhotoFilePath, contentTV;
//    private boolean isCategorySelected = false;
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
//        parentActivity = (MainActivity) getActivity();
//    }
//
//    public static AddBlogFragmentChina newInstance(MainActivity parentActivity) {
//        AddBlogFragmentChina fragment = new AddBlogFragmentChina();
//        fragment.parentActivity = parentActivity;
//        return fragment;
//    }
//
//    @SuppressLint("ClickableViewAccessibility")
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
//        final View view = inflater.inflate(R.layout.fragment_add_blog, container, false);
//
//        parentActivity.runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                //Editor
//                editor = view.findViewById(R.id.editor);
//                //Button
//                btn_done = view.findViewById(R.id.btn_done);
//                //Edit Text
//                titleET = view.findViewById(R.id.titleET);
//                categoriesET = view.findViewById(R.id.categoriesET);
//                //ImageButton
//                upload_coverIB = view.findViewById(R.id.upload_coverIB);
//                //Spinner
//                categoriesSPN = view.findViewById(R.id.categoriesSPN);
//                //Listener
//                btn_done.setOnClickListener(AddBlogFragmentChina.this);
//                categoriesET.setOnClickListener(AddBlogFragmentChina.this);
//                upload_coverIB.setOnClickListener(AddBlogFragmentChina.this);
//
//                permissionsService = new PermissionsService(parentActivity);
//                // this is needed if you like to insert images so that the user gets asked for permission to access external storage if needed
//                // see also onRequestPermissionsResult() below
//                editor.setPermissionsService(permissionsService);
//
//                bottomGroupedCommandsToolbar = view.findViewById(R.id.editorToolbar);
//                bottomGroupedCommandsToolbar.setEditor(editor);
//
//                editor.setEditorFontSize(16);
//                editor.setPadding((4 * (int) getResources().getDisplayMetrics().density));
//                editor.setOnTouchListener(new View.OnTouchListener() {
//
//                    public boolean onTouch(View v, MotionEvent event) {
//                        if (editor.hasFocus()) {
//                            v.getParent().requestDisallowInterceptTouchEvent(true);
//                            switch (event.getAction() & MotionEvent.ACTION_MASK){
//                                case MotionEvent.ACTION_SCROLL:
//                                    v.getParent().requestDisallowInterceptTouchEvent(false);
//                                    return true;
//                            }
//                        }
//                        return false;
//                    }
//                });
//
//                // only needed if you allow to automatically download remote images
////                editor.setDownloadImageConfig(new DownloadImageConfig(DownloadImageUiSetting.AllowSelectDownloadFolderInCode,
////                        new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "downloaded_images")));
//
//
//                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(parentActivity, android.R.layout.simple_spinner_item, parentActivity.getResources().getStringArray(R.array.categories)){
//                    @Override
//                    public boolean isEnabled(int position){
//                        return position != 0;
//                    }
//
//                    @Override
//                    public View getDropDownView(int position, View convertView,ViewGroup parent)
//                    {
//                        View spinnerview = super.getDropDownView(position, convertView, parent);
//
//                        if(position == 0) {
//                            isCategorySelected = false;
//                            //Set the disable spinner item color fade .
//                            categoriesET.setTextColor(Color.parseColor("#bcbcbb"));
//                        }
//                        else {
//                            isCategorySelected = true;
//                            categoriesET.setTextColor(Color.BLACK);
//
//                        }
//                        return spinnerview;
//                    }
//                }; //selected item will look like a spinner set from XML
//                spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//                categoriesSPN.setAdapter(spinnerArrayAdapter);
//                categoriesSPN.setSelection(0);
//                categoriesSPN.setOnItemSelectedListener(AddBlogFragmentChina.this);
//            }
//        });
//
//        return view;
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        permissionsService.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//    }
//
//    // then when you want to do something with edited html
//    private void getHtmlString(final HashMap<String,Object>blogMap) {
//        editor.getCurrentHtmlAsync(new GetCurrentHtmlCallback() {
//
//            @Override
//            public void htmlRetrieved(@NotNull String html) {
//                blogMap.put(Blog.CONTENT,html);
//            }
//        });
//    }
//
////    private String saveHtml(String html) {
////        // ...
////       return html;
////    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        parentActivity.hideBtmNav();
//    }
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        parentActivity.showBtmNav();
//    }
//
//    @Override
//    public void onClick(View v) {
//        switch(v.getId()){
//            case R.id.btn_done:
//                submitBlog();
//                break;
//            case R.id.categoriesET:
//                categoriesSPN.performClick();
//                break;
//            case R.id.upload_coverIB:
//                if(GeneralFunction.isStorageAndCameraPermitted(parentActivity.getApplicationContext(),parentActivity)){
//                    GeneralFunction.showFileChooserRatio169(parentActivity,AddBlogFragmentChina.this);
//                }
//                break;
//        }
//    }
//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
//            CropImage.ActivityResult result = CropImage.getActivityResult(data);
//            if (resultCode == RESULT_OK) {
//                Uri resultUri = result.getUri();
//
//                this.coverPhotoFilePath = FilePath.getPath(parentActivity, resultUri);
//                GlideImageSetting(parentActivity, resultUri,upload_coverIB);
//
//
//
//            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
//                Exception error = result.getError();
//                Log.e(error.toString(),TAG);
//            }
//        }
//
//        if (requestCode == CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE) {
//            if (ContextCompat.checkSelfPermission(parentActivity, Manifest.permission.CAMERA)
//                    == PackageManager.PERMISSION_DENIED) {
//                ActivityCompat.requestPermissions(parentActivity, new String[]{Manifest.permission.CAMERA}, requestCode);
//            }
//        }
//    }
//
//    @Override
//    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//        Object item = parent.getItemAtPosition(position);
//        if (item != null) {
//            String country = categoriesSPN.getSelectedItem().toString();
//            categoriesET.setText(country);
//        }
//    }
//
//    @Override
//    public void onNothingSelected(AdapterView<?> parent) {
//        categoriesET.setHint(R.string.car_rental_hint_select_country);
//    }
//
//    private void submitBlog(){
//        HashMap<String,Object> blogMap = new HashMap<>();
//        String mTitle = titleET.getText().toString().trim();
//        String mCategory = categoriesET.getText().toString().trim();
//
//        editor.setHtml("<p>\u200Brrzrs</p><p><img style =\"transform: rotate(90deg);\" class=\"resizable\" src=\"/storage/emulated/0/DCIM/Camera/20180924_153739.jpg\" alt=\"/storage/emulated/0/DCIM/Camera/20180924_153739.jpg\"><img class=\"resizable\" src=\"/storage/emulated/0/DCIM/Camera/20180924_153739.jpg\" alt=\"/storage/emulated/0/DCIM/Camera/20180924_153739.jpg\"><img class=\"resizable\" src=\"/storage/emulated/0/DCIM/Camera/20180926_191733.jpg\" alt=\"/storage/emulated/0/DCIM/Camera/20180926_191733.jpg\"><img class=\"resizable\" src=\"/storage/emulated/0/DCIM/Camera/20180924_153739.jpg\" alt=\"/storage/emulated/0/DCIM/Camera/20180924_153739.jpg\"><br></p>");
//
////        if(isValidInput(mTitle,mCategory,coverPhotoFilePath)){
////            getHtmlString(blogMap);
////            blogMap.put(Blog.TITLE,mTitle);
////            blogMap.put(Blog.CATEGORY,mCategory);
////            blogMap.put(Blog.USER_ID,parentActivity.uid);
////            blogMap.put(Blog.DATE_CREATED, FieldValue.serverTimestamp());
////            blogMap.put(Blog.DATE_UPDATED, FieldValue.serverTimestamp());
////
////            DocumentReference blogRef = GeneralFunction.getFirestoreInstance()
////                    .collection(Blog.URL_BLOG)
////                    .document();
////
////            blogRef.getId();
////
////            parentActivity.initializeProgressIndicator(parentActivity.getString(R.string.host_upload_detail), false, this);
////            parentActivity.controlProgressIndicator(true, this);
////
////            uploadCoverPhoto(blogMap,coverPhotoFilePath, blogRef);
////
////        }
//
//    }
//
//    private boolean isValidInput(String mTitle, String mCategory, String coverPhotoFilePath){
//        return  GeneralFunction.checkAndReturnString(mTitle,parentActivity.getString(R.string.add_blog_error_title),parentActivity)
//                &&
//                checkSpinner(mCategory)
//                &&
//                GeneralFunction.checkImageViewHasDrawable(parentActivity,upload_coverIB,R.string.add_blog_error_cover_photo, coverPhotoFilePath);
//    }
//
//    private boolean checkSpinner(String mCategory){
//        if(mCategory!=null && isCategorySelected){
//            return true;
//        }
//        else{
//            GeneralFunction.openMessageNotificationDialog(parentActivity,parentActivity.getString(R.string.add_blog_error_category),R.drawable.notice_bad,"invalid_string");
//            return false;
//        }
//    }
//
//    private void uploadCoverPhoto(final HashMap<String,Object> blogMap, String filePath, final DocumentReference blogRef){
//        if(filePath != null){
//            final StorageReference ref = GeneralFunction.getFirebaseStorageInstance().child(Blog.URL_BLOG + "/" + blogRef.getId() + "/" + parentActivity.uid);
//
//            UploadTask uploadTask = ref.putBytes(GeneralFunction.getCompressedBitmap(parentActivity, filePath));
//
//            uploadTask.addOnFailureListener(new OnFailureListener() {
//                @Override
//                public void onFailure(@NonNull Exception exception) {
//                    // Handle unsuccessful uploads
//                    GeneralFunction.handleUploadError(parentActivity, AddBlogFragmentChina.this, R.string.employer_err_job_posting_job, TAG);
//                }
//            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
//                @Override
//                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
//
//                    ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
//                        @Override
//                        public void onSuccess(Uri downloadUri) {
//                            if (downloadUri != null) {
//                                blogMap.put(Blog.COVER_PHOTO, downloadUri.toString());
//                                setBlogDetail(blogRef, blogMap);
//                            }
//                        }
//                    });
//                }
//            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
//                @Override
//                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
//                    int totalProgress = (int) (GeneralFunction.getCurrentProgress(taskSnapshot) / 100 * 80) + 10;
//                    parentActivity.updateProgress((float) totalProgress, AddBlogFragmentChina.this);
//                }
//            });
//        }
//    }
//
//    private void setBlogDetail(DocumentReference blogRef, HashMap<String,Object>blogMap){
//        blogRef.set(blogMap).addOnCompleteListener(new OnCompleteListener<Void>() {
//            @Override
//            public void onComplete(@NonNull Task<Void> task) {
//                if(task.isSuccessful()){
//                    GeneralFunction.openMessageNotificationDialog(parentActivity,"Successful upload blog detail",R.drawable.notice_good,TAG);
//                    parentActivity.controlProgressIndicator(false,AddBlogFragmentChina.this);
//                }
//            }
//        }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//                parentActivity.controlProgressIndicator(false,AddBlogFragmentChina.this);
//                GeneralFunction.openMessageNotificationDialog(parentActivity,e.toString(),R.drawable.notice_bad,TAG);
//            }
//        });
//    }
//}
