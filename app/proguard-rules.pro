# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

# Keep native methods
-keepclassmembers class * {
    native <methods>;
}

-dontwarn javax.annotation.**
-dontwarn com.android.volley.toolbox.**

#tried to fix start here

-dontwarn android.support.v4.**
-keep class android.support.v4.app.** { *; }
-keep interface android.support.v4.app.** { *; }
-dontwarn android.support.v7.**
-keep class android.support.v7.** { *; }
-keep interface android.support.v7.** { *; }

#https://stackoverflow.com/questions/47941251/why-proguard-is-only-obfuscating-the-class-which-is-not-extending-anything

-keep class ** extends android.** {
    <fields>;
    <methods>;
}

# Keep serializable classes & fields
-keep class ** extends java.io.Serializable {
    <fields>;
}

# Keep - Applications. Keep all application classes, along with their 'main'
# methods.
-keepclasseswithmembers public class * {
    public static void main(java.lang.String[]);
}

# Also keep - Enumerations. Keep the special static methods that are required in
# enumeration classes.
-keepclassmembers enum  * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}


-assumenosideeffects class android.util.Log {
    public static *** d(...);
    public static *** e(...);
    public static *** w(...);
}

#https://www.guardsquare.com/en/proguard/manual/examples
#-keep public class com.vidatechft.foodee

#https://github.com/krschultz/android-proguard-snippets/blob/master/examples/proguard-project-app.pro
-keep class com.vidatechft.yupa.** { *; }

-keep class android.support.v7.widget.** { *; }

-ignorewarnings