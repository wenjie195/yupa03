package com.vidatechft.yupa.hostFragments;

import android.animation.ValueAnimator;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.GridLayoutAnimationController;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.QuerySnapshot;
import com.vidatechft.yupa.R;
import com.vidatechft.yupa.activities.MainActivity;
import com.vidatechft.yupa.adapter.HostAnAccommodationAdapter;
import com.vidatechft.yupa.adapter.PublishedAdapter;
import com.vidatechft.yupa.adapter.WaitingVerifyAdapter;
import com.vidatechft.yupa.classes.Room;
import com.vidatechft.yupa.classes.Setting;
import com.vidatechft.yupa.utilities.GeneralFunction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class HostAnAccommodationFragment extends Fragment implements AbsListView.OnScrollListener, View.OnClickListener{
    public static final String TAG = HostAnAccommodationFragment.class.getName();
    private MainActivity parentActivity;
    private GridView startHostGV, waitingForVerifyGV,publishGV;
    private String getPushId;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private HostAnAccommodationAdapter hostAnAccommodationAdapter;
    private WaitingVerifyAdapter waitingVerifyAdapter;
    private PublishedAdapter publishedAdapter;
    private int selectAccommodation = 0;
    private List<Room> startHostAllRooms = new ArrayList<>();
    private List<Room> startHostDisplayRooms = new ArrayList<>();
    private List<String> documentPushIdFromFirestore = new ArrayList<>();
    private List<String> electricBillPushIdFromFirestore = new ArrayList<>();
    private List<List<Room>> electricBillUrl = new ArrayList<List<Room>>();
    private List<GeoPoint> geoPoint = new ArrayList<>();

    private List<Room> waitVerifyAllRooms = new ArrayList<>();
    private List<Room> waitVerifyDisplayRooms = new ArrayList<>();
    private List<Room> publisedAllRooms = new ArrayList<>();
    private List<Room> publishedDisplayRooms = new ArrayList<>();
    private ImageButton startToHost_loadMore,waitingForVerify_loadMore,publish_loadMore;
    private boolean btnIsDown_startHost = false;
    private boolean btnIsDown_waitVerify = false;
    private boolean btnIsDown_publised = false;
    private boolean btn;
    private ConstraintLayout startHostCL,waitingForVerifyCL,publishCL;
    private Button btn_submitNewAccommodation;
    private HashMap<Integer, List<String>> getDocumentId;
    private boolean isFromFireStore = false;
    private ConstraintLayout emptyCL;


    public static HostAnAccommodationFragment newInstance(MainActivity parentActivity) {
        HostAnAccommodationFragment fragment = new HostAnAccommodationFragment();
        fragment.parentActivity = parentActivity;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPushId = GeneralFunction.getDocumentPath(parentActivity, Room.ACCOMMODATION_DETIALS,Room.DIR_DOCUMENT, "");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, final Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_host_an_accommodation, container, false);

        //constraint layout
        emptyCL = view.findViewById(R.id.emptyCL);

        //grid view
        startHostGV = view.findViewById(R.id.startHostGV);
        waitingForVerifyGV = view.findViewById(R.id.waitingForVerifyGV);
        publishGV = view.findViewById(R.id.publishGV);

        //image button
        startToHost_loadMore = view.findViewById(R.id.startToHost_loadMore);
        waitingForVerify_loadMore = view.findViewById(R.id.waitingForVerify_loadMore);
        publish_loadMore = view.findViewById(R.id.publish_loadMore);

        //button
        btn_submitNewAccommodation = view.findViewById(R.id.btn_submitNewAccommodation);

        //constraint layout
        startHostCL = view.findViewById(R.id.startHostCL);
        waitingForVerifyCL = view.findViewById(R.id.waitingForVerifyCL);
        publishCL = view.findViewById(R.id.publishCL);

        //listener
        startToHost_loadMore.setOnClickListener(this);
        waitingForVerify_loadMore.setOnClickListener(this);
        publish_loadMore.setOnClickListener(this);
        btn_submitNewAccommodation.setOnClickListener(this);

        startHostGV.requestDisallowInterceptTouchEvent(false);
        waitingForVerifyGV.requestDisallowInterceptTouchEvent(false);
        publishGV.requestDisallowInterceptTouchEvent(false);

        startHostGV.setOnScrollListener(this);
        waitingForVerifyGV.setOnScrollListener(this);
        publishGV.setOnScrollListener(this);

        if(startHostDisplayRooms.size() <= 0){
            showStartHost(savedInstanceState);
        }
        else{
            startHostCL.setVisibility(View.VISIBLE);
            if(startHostAllRooms.size() <= 2 ){
                startToHost_loadMore.setVisibility(View.GONE); // Load More Button
            }
            else{
                startToHost_loadMore.setVisibility(View.VISIBLE); // Load More Button
            }
            setStartToHostAdapter(savedInstanceState);
        }
        if(waitVerifyDisplayRooms.size() <= 0){
            showWaitingVerify(savedInstanceState);
        }
        else{
            waitingForVerifyCL.setVisibility(View.VISIBLE);
            if(waitVerifyDisplayRooms.size() <= 2 ){
                waitingForVerify_loadMore.setVisibility(View.GONE); // Load More Button
            }
            else{
                waitingForVerify_loadMore.setVisibility(View.VISIBLE); // Load More Button
            }
            setWaitForVerifyAdapter(savedInstanceState);
        }
        if(publishedDisplayRooms.size() <= 0){
            showPublishedVerify(savedInstanceState);
        }
        else{
            publishCL.setVisibility(View.VISIBLE);
            if(publishedDisplayRooms.size() <= 2 ){
                publish_loadMore.setVisibility(View.GONE); // Load More Button
            }
            else{
                publish_loadMore.setVisibility(View.VISIBLE); // Load More Button
            }
            setPublishedAdapter(savedInstanceState);
        }

        return view;
    }

    private void showStartHost(final Bundle savedInstanceState){
        final ProgressDialog progress = new ProgressDialog(parentActivity);
        progress.setCancelable(false);
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();
        CollectionReference cocRef = db.collection(Host.URL_HOST);
        cocRef.whereEqualTo(Room.HOST_ID, parentActivity.uid)
                .whereEqualTo(Setting.IS_DRAFT, true)
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                if(!queryDocumentSnapshots.isEmpty()){
                    startHostAllRooms = queryDocumentSnapshots.toObjects(Room.class);

                    if(startHostAllRooms.size() != 0){
                        startHostCL.setVisibility(View.VISIBLE);


                        if(startHostAllRooms.size() <= 2 ){
                            startToHost_loadMore.setVisibility(View.GONE); // Load More Button

                            for(int i = 0; i < startHostAllRooms.size(); i++){
                                if(startHostAllRooms.size() == 1){
                                    startHostDisplayRooms.add(startHostAllRooms.get(i));
                                }
                                else{
                                    if(startHostAllRooms.get(i) != null){
                                        startHostDisplayRooms.add(startHostAllRooms.get(i));
                                    }
                                }
                            }
                        }
                        else{
                            startToHost_loadMore.setVisibility(View.VISIBLE); // Load More Button

                            for(int i=0; i < 2; i++){

                                if(startHostAllRooms.get(i) != null){
                                    startHostDisplayRooms.add(startHostAllRooms.get(i));
                                }
                            }
                        }
                        setStartToHostAdapter(savedInstanceState);
                    }
                    else{
                        startHostCL.setVisibility(View.GONE);
                    }
                }
                CollectionReference cocRef = db.collection(Host.URL_HOST);
                cocRef.whereEqualTo(Room.HOST_ID, parentActivity.uid)
                        .whereEqualTo(Setting.IS_DRAFT, true)
                        .get()
                        .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                            @Override
                            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                                getDocumentId = new HashMap<Integer, List<String>>();

                                for(final DocumentSnapshot documentReference : queryDocumentSnapshots.getDocuments()){

                                    CollectionReference collectionReference = db.collection(Host.URL_HOST).document(documentReference.getId()).collection(Room.URL_ROOM_PROOF);
                                    collectionReference.get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                                        @Override
                                        public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                                            queryDocumentSnapshots.toObjects(Room.class);

                                            for(int i = 0; i < queryDocumentSnapshots.size(); i++){
                                                documentPushIdFromFirestore.add(i,documentReference.getId());
                                                electricBillUrl.add(i,queryDocumentSnapshots.toObjects(Room.class));
                                                geoPoint.add(i, documentReference.getGeoPoint(Room.ACCOMMODATION_GEOPOINT));
                                            }
                                            progress.dismiss();
                                            documentPushIdFromFirestore.size();
                                            electricBillUrl.size();
                                            //to loop and get electricbill push id
                                            for(DocumentSnapshot getElectricPushId : queryDocumentSnapshots.getDocuments()){
                                                for(int i = 0; i < queryDocumentSnapshots.size(); i++) {
                                                    electricBillPushIdFromFirestore.add(i, getElectricPushId.getId());
                                                }
                                            }

                                            electricBillPushIdFromFirestore.size();
                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Crashlytics.logException(e);
                                        }
                                    });
                                }
                            }
                        });
            }
        }).addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                QuerySnapshot document = task.getResult();
                boolean isEmpty = task.getResult().isEmpty();
                if (isEmpty) {
                    emptyCL.setVisibility(View.VISIBLE);
                    progress.dismiss();
                }
                else{
                    emptyCL.setVisibility(View.GONE);
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Crashlytics.logException(e);
            }
        });
    }

    private void showWaitingVerify(final Bundle savedInstanceState){
        CollectionReference cocRef = db.collection(Host.URL_HOST);
        cocRef.whereEqualTo(Room.HOST_ID, parentActivity.uid)
                .whereEqualTo(Setting.IS_DRAFT, false)
                .whereEqualTo(Room.STATUS, Room.STATUS_PENDING)
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        if(!queryDocumentSnapshots.isEmpty()){
                            waitVerifyAllRooms = queryDocumentSnapshots.toObjects(Room.class);

                            if(waitVerifyAllRooms.size() != 0) {
                                waitingForVerifyCL.setVisibility(View.VISIBLE);

                                if(waitVerifyAllRooms.size() <= 2 ){
                                    waitingForVerify_loadMore.setVisibility(View.GONE); // Load More Button

                                    for(int i = 0; i < waitVerifyAllRooms.size(); i++){
                                        if(waitVerifyAllRooms.size() == 1){
                                            waitVerifyDisplayRooms.add(waitVerifyAllRooms.get(i));
                                        }
                                        else{
                                            if(waitVerifyAllRooms.get(i) != null){
                                                waitVerifyDisplayRooms.add(waitVerifyAllRooms.get(i));
                                            }
                                        }
                                    }
                                }
                                else{
                                    waitingForVerify_loadMore.setVisibility(View.VISIBLE); // Load More Button

                                    for(int i=0; i < 2; i++){

                                        if(waitVerifyAllRooms.get(i) != null){
                                            waitVerifyDisplayRooms.add(waitVerifyAllRooms.get(i));
                                        }
                                    }
                                }
                                setWaitForVerifyAdapter(savedInstanceState);
                            }
                            else{
                                waitingForVerifyCL.setVisibility(View.GONE);
                            }

                        }
                    }
                });
    }

    private void showPublishedVerify(final Bundle savedInstanceState){
        CollectionReference cocRef = db.collection(Host.URL_HOST);
        cocRef.whereEqualTo(Room.HOST_ID, parentActivity.uid)
                .whereEqualTo(Setting.IS_DRAFT, false)
                .whereEqualTo(Room.STATUS, Room.STATUS_PUBLISHED)
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        if(!queryDocumentSnapshots.isEmpty()){
                            publisedAllRooms = queryDocumentSnapshots.toObjects(Room.class);

                            if(publisedAllRooms.size() != 0){
                                publishCL.setVisibility(View.VISIBLE);

                                if(publisedAllRooms.size() <= 2 ){
                                    publish_loadMore.setVisibility(View.GONE); // Load More Button

                                    for(int i = 0; i < publisedAllRooms.size(); i++){
                                        if(publisedAllRooms.size() == 1){
                                            publishedDisplayRooms.add(publisedAllRooms.get(i));
                                        }
                                        else{
                                            if(publisedAllRooms.get(i) != null){
                                                publishedDisplayRooms.add(publisedAllRooms.get(i));
                                            }
                                        }
                                    }
                                }
                                else{
                                    publish_loadMore.setVisibility(View.VISIBLE); // Load More Button

                                    for(int i=0; i < 2; i++){

                                        if(publisedAllRooms.get(i) != null){
                                            publishedDisplayRooms.add(publisedAllRooms.get(i));
                                        }
                                    }
                                }
                                setPublishedAdapter(savedInstanceState);
                            }
                            else{
                                publishCL.setVisibility(View.GONE);
                            }
                        }
                    }
                });
    }

    private void setStartToHostAdapter(Bundle savedInstanceState){
        hostAnAccommodationAdapter = new HostAnAccommodationAdapter(parentActivity.getApplicationContext(),parentActivity,savedInstanceState, startHostDisplayRooms);
        hostAnAccommodationAdapter.setAdapterView(startHostGV);
        Animation animation = AnimationUtils.loadAnimation(getContext(),R.anim.grid_item_anim);
        GridLayoutAnimationController controller = new GridLayoutAnimationController(animation, .2f, .2f);
        startHostGV.setLayoutAnimation(controller);
        startHostGV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                isFromFireStore = true;
                selectAccommodation = position;
                Room room = (Room) adapterView.getItemAtPosition(position);
                String getDocumentPushIdFromFirestore = documentPushIdFromFirestore.get(position);
                String getElectricBillPushIfFromFirestore = electricBillPushIdFromFirestore.get(position);

                if(room.getUrlElectricBill() == null){
                    List<Room> getUrlElectricBill =  electricBillUrl.get(position);
                    for (int i = 0; i<getUrlElectricBill.size(); i++){
                        String electricUrl =  getUrlElectricBill.get(i).getUrlElectricBill();
                        room.setUrlElectricBill(electricUrl);
                    }
                }
                if(room.getAccommodationGeoPoint() == null){
                    GeoPoint getGeoPoint = geoPoint.get(position);
                    room.setAccommodationGeoPoint(getGeoPoint);
                }

                Fragment fragment = AccommodationDetailFragment.newInstance(parentActivity,room,position,Room.TYPE_START_HOST,getDocumentPushIdFromFirestore,getElectricBillPushIfFromFirestore, isFromFireStore);
                FragmentManager fragmentManager = parentActivity.getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.mainFL, fragment).addToBackStack(AccommodationDetailFragment.TAG).commit();
//                Toast.makeText(parentActivity, roomName, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setWaitForVerifyAdapter(Bundle savedInstanceState){

        waitingVerifyAdapter = new WaitingVerifyAdapter(parentActivity.getApplicationContext(),parentActivity,savedInstanceState, waitVerifyDisplayRooms);
        waitingVerifyAdapter.setAdapterView(waitingForVerifyGV);
        Animation animation = AnimationUtils.loadAnimation(getContext(),R.anim.grid_item_anim);
        GridLayoutAnimationController controller = new GridLayoutAnimationController(animation, .2f, .2f);
        waitingForVerifyGV.setLayoutAnimation(controller);
        waitingForVerifyGV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Toast.makeText(parentActivity, String.valueOf(position), Toast.LENGTH_SHORT).show();
            }
        });
        waitingForVerifyGV.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(parentActivity, position, Toast.LENGTH_SHORT).show();
                return false;
            }
        });
    }

    private void setPublishedAdapter(Bundle savedInstanceState){

        publishedAdapter = new PublishedAdapter(parentActivity.getApplicationContext(),parentActivity,savedInstanceState, publishedDisplayRooms);
        publishedAdapter.setAdapterView(publishGV);
        Animation animation = AnimationUtils.loadAnimation(getContext(),R.anim.grid_item_anim);
        GridLayoutAnimationController controller = new GridLayoutAnimationController(animation, .2f, .2f);
        publishGV.setLayoutAnimation(controller);
        publishGV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                selectAccommodation = position;
            }
        });
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
//        if (startHostGV.getLastVisiblePosition() + 1 >= 2) {
//            startToHost_loadMore.setVisibility(View.VISIBLE); // Load More Button
//            startHostGV.setOnTouchListener(new View.OnTouchListener(){
//
//                @Override
//                public boolean onTouch(View v, MotionEvent event) {
//                    return event.getAction() == MotionEvent.ACTION_MOVE;
//                }
//
//            });
//            startToHost_loadMore.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                   expand(startHostGV,1000,1200);
//                }
//            });
//        }
//        else{
//            startToHost_loadMore.setVisibility(View.GONE); // Load More Button
//
//        }
    }

    public static void expand(final View v, int duration, int targetHeight) {

        int prevHeight  = v.getHeight();

        v.setVisibility(View.VISIBLE);
        ValueAnimator valueAnimator = ValueAnimator.ofInt(prevHeight, targetHeight);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                v.getLayoutParams().height = (int) animation.getAnimatedValue();
                v.requestLayout();
            }
        });
        valueAnimator.setInterpolator(new DecelerateInterpolator());
        valueAnimator.setDuration(duration);
        valueAnimator.start();
    }

    public static void collapse(final View v, int duration, int targetHeight) {
        int prevHeight  = v.getHeight();
        ValueAnimator valueAnimator = ValueAnimator.ofInt(prevHeight, targetHeight);
        valueAnimator.setInterpolator(new DecelerateInterpolator());
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                v.getLayoutParams().height = (int) animation.getAnimatedValue();
                v.requestLayout();
            }
        });
        valueAnimator.setInterpolator(new DecelerateInterpolator());
        valueAnimator.setDuration(duration);
        valueAnimator.start();
    }


    @Override
    public void onClick(View v) {

        FragmentTransaction ft = parentActivity.getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.fragment_enter, R.anim.fragment_exit, R.anim.fragment_pop_enter, R.anim.fragment_pop_exit);
        Fragment fragment = parentActivity.getSupportFragmentManager().findFragmentById(R.id.mainFL);
        if(v.getId() == R.id.startToHost_loadMore){

            if(!btnIsDown_startHost){
                btnIsDown_startHost = true;
                startHostDisplayRooms.clear();
                startHostDisplayRooms.addAll(startHostAllRooms);
                hostAnAccommodationAdapter.notifyDataSetChanged();
                //request layout to update the layout
                startHostGV.requestLayout();
                startToHost_loadMore.setImageResource(R.drawable.arrow_up_black);
            }
            else{
                btnIsDown_startHost = false;
                startHostDisplayRooms.clear();
                startHostGV.requestLayout();
                for(int i=0; i < 2; i++){
                    if(startHostAllRooms.get(i) != null){
                        startHostDisplayRooms.add(startHostAllRooms.get(i));
                        startToHost_loadMore.setVisibility(View.VISIBLE); // Load More Button
                    }
                    else{
                        startToHost_loadMore.setVisibility(View.GONE); // Load More Button
                    }
                }
                startToHost_loadMore.setImageResource(R.drawable.arrow_down_black);
            }
        }

        if(v.getId() == R.id.waitingForVerify_loadMore){
            if(!btnIsDown_waitVerify){
                btnIsDown_waitVerify = true;
                waitVerifyDisplayRooms.clear();
                waitVerifyDisplayRooms.addAll(waitVerifyAllRooms);
                waitingVerifyAdapter.notifyDataSetChanged();
                //request layout to update the layout
                waitingForVerifyGV.requestLayout();
                waitingForVerify_loadMore.setImageResource(R.drawable.arrow_up_black);
            }
            else{
                btnIsDown_waitVerify = false;
                waitVerifyDisplayRooms.clear();
                waitingForVerifyGV.requestLayout();
                for(int i=0; i < 2; i++){
                    if(waitVerifyAllRooms.get(i) != null){
                        waitVerifyDisplayRooms.add(waitVerifyAllRooms.get(i));
                        waitingForVerify_loadMore.setVisibility(View.VISIBLE); // Load More Button
                    }
                    else{
                        waitingForVerify_loadMore.setVisibility(View.GONE); // Load More Button
                    }
                }
                waitingForVerify_loadMore.setImageResource(R.drawable.arrow_down_black);
            }
        }

        if(v.getId() == R.id.publish_loadMore){
            if(!btnIsDown_publised){
                btnIsDown_publised = true;
                publishedDisplayRooms.clear();
                publishedDisplayRooms.addAll(publisedAllRooms);
                publishedAdapter.notifyDataSetChanged();
                //request layout to update the layout
                publishGV.requestLayout();
                publish_loadMore.setImageResource(R.drawable.arrow_up_black);
            }
            else{
                btnIsDown_publised = false;
                publishedDisplayRooms.clear();
                publishGV.requestLayout();
                for(int i=0; i < 2; i++){
                    if(publisedAllRooms.get(i) != null){
                        publishedDisplayRooms.add(publisedAllRooms.get(i));
                        publish_loadMore.setVisibility(View.VISIBLE); // Load More Button
                    }
                    else{
                        publish_loadMore.setVisibility(View.GONE); // Load More Button
                    }
                }
                publish_loadMore.setImageResource(R.drawable.arrow_down_black);
            }
        }

        if(v.getId() == R.id.btn_submitNewAccommodation){
            isFromFireStore = false;
            if(!(fragment instanceof AccommodationDetailFragment)){
                AccommodationDetailFragment accommodationDetailFragment = AccommodationDetailFragment.newInstance(parentActivity);
                ft.replace(R.id.mainFL, accommodationDetailFragment).addToBackStack(AccommodationDetailFragment.TAG);
                ft.commit();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        btn = btnIsDown_startHost;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(btn){
            startToHost_loadMore.setImageResource(R.drawable.arrow_up_black);
        }
        else{
            startToHost_loadMore.setImageResource(R.drawable.arrow_down_black);
        }
    }
}
